<?php require("globals.php"); ?>  
<?php

if($_SESSION['user']){
    
    if(isset($_SERVER['HTTP_REFERER']) && ($_SERVER['HTTP_REFERER']=="http://234tempname.com.s207745.gridserver.com/" || $_SERVER['HTTP_REFERER']=="http://234tempname.com.s207745.gridserver.com/index.php" )){
      echo"<script>window.location='dashboard.php';</script>";
    }
}
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 11;
    $_SESSION['titleadd'] = 'Best Vehicle Assessment';

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];
    
    unset($_SESSION['assessment_post']);
    unset($_SESSION['pos_post']);
    unset($_SESSION['searchplan_post']);
    unset($_SESSION['consult_post']);

    
    $errorinload = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        mysql_close($con);
    }else {$errorinload = 'Could not retrieve Assessment Information';}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php //require("headerend.php"); ?>
<script type="text/javascript">
    $(document).ready(function () {
		doyouprefer('<?php echo $vehicletype;?>');
		$("#form_assessment input:text").click(function (){
			var element = $(this);
			$("#notetext").val(element.val());
			$( "#dialog" ).dialog({
				modal: true,
				title: " ",
				close: function( event, ui ) {
					element.val($("#notetext").val());
					$("#dialog").dialog("destroy");
				}
			});
		});
                
                $("#showrequirfield").click(function (){
			$('#notshowfields').toggle();
		});
                
                
	});
	function vehiclechanged(val){
		if(val=='Auto'){
			$("#bodytype").html('<option value="Sedan" selected="selected">Sedan</option><option value="Coupe">Coupe</option><option value="Hatchback">Hatchback</option><option value="Convertible">Convertible</option>');
			
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
			
			//Do you prefer section
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").hide();
			$("#trtpackage").hide();
			
		}else if(val=='Minivan'){
			$("#bodytype").html('<option value="Standard" selected="selected">Standard</option>');
			
			$("#vehiclesize").html('<option value="Standard" selected="selected">Standard</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
			
			//Do you prefer section
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").hide();

		}else if(val=='SUV'){
			$("#bodytype").html('<option value="2 door" selected="selected">2 door</option><option value="4 door">4 door</option>');
			
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		
			//Do you prefer section
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").show();
			$("#trcrow").show();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").show();

		}else if(val=='Pickup'){
			$("#bodytype").html('<option value="4 door" selected="selected">4 door</option><option value="Ext Cab">Ext Cab</option><option value="Single Cab">Single Cab</option>');
			
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
			
			//Do you prefer section
			$("#trfrontstype").show();
			$("#trbedtype").show();
			$("#trrearwindow").show();
			$("#trbedliner").show();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").show();
			$("#trtpackage").show();

		}
		
	}
	
    function bodychanged(val){
		var vehicle_type = $('#vehicletype').val();
		console.log("Vehicle type: "+ vehicle_type);
		if(val=='Sedan' || val=='Coupe' || val=='Hatchback'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
			
		}else if(val=='Convertible'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='Standard'){
			$("#vehiclesize").html('<option value="Standard" selected="selected">Standard</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='2 door'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='4 door' && vehicle_type =='SUV' ){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='4 door' && vehicle_type =='Pickup' ){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
			
		}else if(val=='Ext Cab' || val=='Single Cab'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		}
	}
	
	function doyouprefer(val){
		if(val=='Auto'){
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").hide();
			$("#trtpackage").hide();
			
		}else if(val=='Minivan'){
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").hide();

		}else if(val=='SUV'){
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").show();
			$("#trcrow").show();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").show();

		}else if(val=='Pickup'){
			$("#trfrontstype").show();
			$("#trbedtype").show();
			$("#trrearwindow").show();
			$("#trbedliner").show();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").show();
			$("#trtpackage").show();
		}
	}
	
	function validateFormOnSubmit(){
        var reason = "";
        if(reason != "")
        {
            vbudget.focus();
            alert("Some fields need correction:"+'\n' + reason + '\n');
            return false;
        }
        return true;
    }
</script>
<style>
    .grideightcontainer table a {
    color: #ffffff;
    font-weight: normal;
}
   .tdactive {
        background-color: #92d050;
    }
	#content {margin:0 auto;}
	.assessment-vehicle {padding-left:0px;float: left;}
	.assess-title-video {padding-left: 15%;}
	.assess-title-all { margin-top: 2%; float:right;}
	.assessment-vehicle > h3 { color: #216ece; font-size: 18px; margin: 5px; padding-left: 10px; letter-spacing:0; text-transform:none;}
	.assess-title-video img { width: 20%;}
	.assess-title-video > span {padding-left: 5px; color: #216ece;}
	.assess-link a{color:#46a25a;}
	h1.subhead{margin:0;}
</style>



<script type="text/javascript">
$(document).ready(function(){
 $("#youtube1").click(function(){
                $.colorbox({href:"dashboard_video.php",scrolling:false,width: "90%"});
            });
            
            $("#step_2_video").click(function(){
                $.colorbox({href:"step_2_video.php",scrolling:false,width: "90%"});
            });
});
</script>
<div class="gridtwelve"></div>
<div id="content">
	<div class="grideightcontainer">            
        <?php 
        if (!isset($_SESSION['user'])) { ?>
           <div class="col-xs-12 col-sm-12 dashboard-title">
                    <h3>My Dashboard</h3>
            </div> 
        <div style="width: 100%">              
            <table class="table table-bordered secondmenu-table">
                <thead>
                    <tr>
                        <td align="center" class="tdactive"><a href="#"><span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">1</span>
                        </span>GET HELP WITH WHAT TO BUY</a></td>                                              
                        <td align="center"><a href="page-unavailable.php"><span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">2</span>
                        </span>CONSIDER & APPROVE</a></td>
                        <td align="center"><a href="page-unavailable2.php"><span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">3</span>
                        </span>INSPECT & TAKE DELIVERY</a></td>
                    </tr>
                </thead>   
            </table>
        </div>  
            <div class="col-xs-12 col-sm-12 dash-step2 " style="margin-bottom: 5px; margin-top: -20px;"> 
                  
            <!--<div class="col-xs-4 col-sm-4">
                
            </div> -->
            
            <div class="col-xs-6 col-sm-6 assess-title-all">
                <div class="assess-title-video" style="float:left;">
                     <span class="helpful-tools"> Other Helpful Links: </span> <br>
                     <a href="#" id="step_2_video"><img src="images/play.png"/> About Step 2</a>
                    </div>
                   
                    <div class="assess-link" style="float:right;">
                    <!--<a title="Newsweek"  href="researchvehicles.php">
                       Research Prices & Availability>
                    </a><br>-->
                    <a   href="consult.php">
                     Request Consultation>
                    </a><br>
                     <a   href="#">
                    24/7 Customer support>
                    </a><br>
                    </div>
            </div>
                
        </div>  
            
       <?php } ?>
            
          <!--  <div class="dashboardmenu">
         
            <div class="row placeholders">     
                
                 <div class="col-xs-6 col-md-4 placeholder">
                    <a href="consult.php"> 
                        <img id="dashboardimage" class="img-responsive"  alt="" src="images/request.png">
                    </a>
                </div> 
                
                 <div class="col-xs-6 col-md-4 placeholder">
                    <a title="Newsweek"  href="researchvehicles.php">
                        <img id="dashboardimage" class="img-responsive"  alt="" src="images/research.png">
                     </a>
                </div>
               
                <div class="col-xs-6 col-md-4 placeholder">
                    <a href="#">  
                      <img id="dashboardimage" class="img-responsive"  alt="" src="images/support.png">
                    </a>
                </div> 
            </div>
        </div>-->
        
        
		<br><br>
         <!--   <div class="grideightgrey">-->
               <?php /* <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;">* All fields required except Additional Information</p> */?>
        <!--        <div class="grideight" style="width: 97%; margin-top:-5px;">

        
       
                    <p class="blackfourteen">
                Quickly find information on specific autos, SUVs and minivans that are 1 to 6 years old.
                <form action="researchspecific.php" method="post">
                    <button type="submit" value="" class="med"><nobr>RESEARCH SPECIFIC VEHICLES</nobr></button>
                </form>
            </p>

            <p class="blackfourteen">
                Quickly sort all autos, SUVs and minivans that are 1 to 6 years old to find out which ones fall within your price range.
                <form action="researchprice.php" method="post">
                    <button type="submit" value="" class="med"><nobr>RESEARCH BY PRICE RANGES</nobr></button>
                </form>
            </p>
      
            <p class="blackfourteen">
                These vehicles can be found in a shorter time frame with greater selection of colors, options and mileage.
                <form action="excellent.php" method="post">
                    <button type="submit" value="" class="med"><nobr>RESEARCH EXCELLENT AVAILABILITY</nobr></button>
                </form>
            <div style="text-align:right;">
                
                <button type="button" value="" onclick="history.back();"class="med"><nobr>BACK to GET EXPERT ADVICE & PRICING</nobr></button>
            </div>
            </p>
      

                </div>--><!-- end greyeight-->
           <!-- </div>--><!-- grid eight container -->
       <!-- </form>-->
    <!--</div>--><!-- end grideightgrey-->
    <div class="clear"></div>
                    
     <div class="grideightgrey get-price-expert get-buybackground" style="margin-top:0px;">
               <?php /* <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;">* All fields required except Additional Information</p> */?>
                <div class="col-xs-12 col-sm-12">
                  
                   <h3 class="research-option">Research Vehicles</h3>
                
                      
              </div>
                    <div class="col-xs-12 col-sm-12 researchvi-option" style="text-align:right;">
                     <a href="get_price_and_expert_advice.php">Go back to GET HELP WITH WHAT TO BUY</a>
                      
                     </div>
               <div class="col-xs-12 col-sm-12 " style="margin-bottom: 5px;">
                   <div class="col-xs-6 col-sm-6 get-buybackground2" style="margin-bottom: 20px;">
                       <a class="button-default" style="text-align:center;" href="researchspecific.php">RESEARCH SPECIFIC VEHICLES</a>  
                    
                     <span class="button-default-span">Quickly find information on specific autos, SUVs and minivans that are 1 to 6 years old. </span>  
                     
                      <a class="button-default" style="text-align:center;" href="researchprice.php" >RESEARCH BY PRICE RANGES</a>
                      
                     <span class="button-default-span">Quickly sort all autos, SUVs and minivans that are 1 to 6 years old to find out which ones fall within your price range. </span>                       
                      <a class="button-default" style="text-align:center;" href="excellent.php">RESEARCH EXCELLENT AVAILABILITY</a>
                       
                     <span class="button-default-span">These vehicles can be found in a shorter time frame with greater selection of colors, options and mileage. </span>
                  
                     </div> 
               </div> 
             
               
            </div><!-- grid eight container -->
        </form>
    </div><!-- end grideightgrey-->
    
    
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>

<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
