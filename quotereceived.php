<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 2;
    $_SESSION['substate'] = 0;
    $_SESSION['titleadd'] = 'Current Market Study Received';

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];

    if(isset($_REQUEST['QuoteID']))
    {
        $quoteID = $_REQUEST['QuoteID'];
        unset($_REQUEST['QuoteID']);
    }

    $loaderror = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select QuoteType, Year, Make, Model, Style, MileageCeiling, EngineCylinders, Transmission,
            CabType, FrontSeatType, BedType, SeatMaterial, WheelDriveType, WheelCovers, RadioNeeded, RadioFM, RadioCassette, RadioCD, PowerDoors, PowerWindows,
            PowerSeats, HeatedSeats, AirConditioning, RemoteEntry, TractionControl, SecuritySystem, CruiseControl, Navigation, RearSlidingWindow, BedLiner, TowPackage,
            ColorCombination, SpecialRequests, Pickup, DeliverToCity, DeliverToState, LuggageRack, SunRoof from quoterequests where QuoteRequestID = ".$quoteID;
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $iquoteid = $quoteID;
            $quotetype = $row['QuoteType'];
            $iyear = $row['Year'];
            $imake = $row['Make'];
            $imodel = $row['Model'];
            $istyle = $row['Style'];
            $imileage = $row['MileageCeiling'];
            $iseattype = $row['SeatMaterial'];
            $iengine = $row['EngineCylinders'];
            $idrive = $row['WheelDriveType'];
            $itrans = $row['Transmission'];
            $iwheels = $row['WheelCovers'];
            $icab = $row['CabType'];
            $ifront = $row['FrontSeatType'];
            $ibedtype = $row['BedType'];
            $iradflex = $row['RadioNeeded'];
            $iradfm = $row['RadioFM'];
            $iradcas = $row['RadioCassette'];
            $iradcd = $row['RadioCD'];
            $ipdoor = $row['PowerDoors'];
            $ipwin = $row['PowerWindows'];
            $ipseat = $row['PowerSeats'];
            $iheat = $row['HeatedSeats'];
            $iair = $row['AirConditioning'];
            $iremote = $row['RemoteEntry'];
            $itraction = $row['TractionControl'];
            $isecure = $row['SecuritySystem'];
            $icruise = $row['CruiseControl'];
            $inavsys = $row['Navigation'];
            $irearwin = $row['RearSlidingWindow'];
            $ibed = $row['BedLiner'];
            $itow = $row['TowPackage'];
            $icolors = $row['ColorCombination'];
            $inotes = $row['SpecialRequests'];
            $idelorpick = $row['Pickup'];
            $idelcity = $row['DeliverToCity'];
            $idelstate = $row['DeliverToState'];
            $iluggage = $row['LuggageRack'];
            $isunroof = $row['SunRoof'];
        }
        else $loaderror = 'Could not find the specified quote request.';

        if($loaderror == 'false')
        {
            $query = "Select UpdateStatus,OrderType,Mileage,PricePoint,ImageFile,AdminNote,FirmQuoteID,LastUpdated,AddDepositAmount,AddSecondKeyLimit, AddNavDiscLimit, AddSellerRepairLimit, AddPurchaserRepairLimit, AddTireCopay, AddDelivery, WaitPeriod from firmquotes where QuoteRequestID=".$quoteID;
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                if($row[0] == 'Researching') $fstatus = 1;
                else $fstatus = 0;
                $fordertype = $row[1];
                $fmiles = $row[2];
                $fprice = $row[3];
                $fimage = $row[4];
                $fnote = $row[5];
                $firmquoteid = $row[6];
                $fupdated = $row[7];
                $fadddep = $row[8];
                $faddsec = $row[9];
                $faddnav = $row[10];
                $faddsel = $row[11];
                $faddpur = $row[12];
                $faddtire = $row[13];
                $fadddel = $row[14];
                $fwaitperiod = $row[15];

                $oquery = "select o.accepted, o.Expires, o.WaitPeriod, o.orderid from orders o,orderfor of,firmquotes f,quoterequests q where o.orderid=of.orderid and f.firmquoteid=of.firmquoteid and f.quoterequestid=q.quoterequestid and q.QuoteRequestID=".$quoteID;
                $oresult = mysql_query($oquery);
                $index = 0;
                if($orow = mysql_fetch_array($oresult)) $isordered = 'true';
                else $isordered = 'false';

                $wquery = "select w.accepted, w.Expires, w.watchid from watches w,watchfor wf,firmquotes f,quoterequests q where w.watchid=wf.watchid and f.firmquoteid=wf.firmquoteid and f.quoterequestid=q.quoterequestid and q.QuoteRequestID=".$quoteID;
                $wresult = mysql_query($wquery);
                if($wrow = mysql_fetch_array($wresult)) $isposted = 'true';
                else $isposted = 'false';
            }
            else $loaderror = 'Could not find the specified current market study.';
        }

        mysql_close($con);
    }
    $_SESSION['titleadd'] = $_SESSION['titleadd'].' - '.$iyear.' '.$imake.' '.$imodel.' '.$istyle;
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 150px;" >Current Market Study</h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0;"><a href="allfirmquotes.php">Go to existing Current Market Studies</a></p>
            <br />
<?php
    if($loaderror != 'false')
    {
        echo '<p style="margin: 0; font-size: 19px;">There was an error loading the page:</p>';
        echo '<p style="margin: 0; font-size: 19px;">'.$loaderror.'</p>';
    }
    else
    {
        echo '<p style="margin: 0; font-size: 19px;">'.$iyear.' '.$imake.' '.$imodel.' '.$istyle.'</p>';
        echo '<br />';
        if(isset($fimage))
        {
            if(!file_exists($fimage)) $imagefile = '';
            else $imagefile = $fimage;
        }

        if(!isset($imagefile) || (strlen($imagefile) < 1))
        {
            if($quotetype == 'Standard')
            {
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    // First fill the list of Makes...
                    mysql_select_db(DB_SERVER_DATABASE, $con);

                    $result = mysql_query("select v.ImageFile from vehicles v, makes m where m.makeid=v.makeid and m.name='".$imake."' and v.year=".$iyear." and v.model='".$imodel."' and v.style='".$istyle."'");
                    if($result && $row = mysql_fetch_array($result))
                    {
                        if(strlen($row[0]) > 0) $imagefile = $row[0];
                    }
                    else $imagefile = '';
                    mysql_close($con);
                }
                else $imagefile = '';
            }
        }

        if(isset($imagefile) && (strlen($imagefile) > 0))
        {
            $max_width = 500;
            $max_height = 500;
            echo '<center><img id="vehimage" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" /></center>';
            echo '<center><p class="blacktwelve">Actual year, make, model, and style</p></center>';
        }
        else echo '<center><p class="blacktwelve">*** No current vehicle image **</p></center>';
?>
        </div><!--end grideightgrey-->
        <h2 class="subhead">Vehicle Preferences</h2>
        <div class="grideightgrey">
            <table border="0" width="300" align="left">
                <tr>
                    <td><strong>Mileage Ceiling</strong></td>
                    <td><strong>Seat Material</strong></td>
                </tr>
                <tr valign="top">
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;"><?php if(isset($imileage)) echo number_format($imileage); ?></p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;"><?php if(isset($iseattype)) echo $iseattype; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Engine</strong></td>
                    <td><strong>Wheel Drive</strong></td>
                </tr>
                <tr valign="top">
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;"><?php if(isset($iengine)) echo $iengine.' Cylinders'; ?></p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;"><?php if(isset($idrive)) echo $idrive; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Transmission</strong></td>
                    <td><strong>Wheel Type</strong></td>
                </tr>
                <tr valign="top">
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;"><?php if(isset($itrans)) echo $itrans; ?></p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;"><?php if(isset($iwheels)) echo $iwheels; ?></p></td>
                </tr>
                <tr>
<?php
    if($quotetype == 'Pickup') echo '<td><strong>Cab Type</strong></td>';
    else echo '<td><strong>Sun Roof</strong></td>';
?>
                    <td><strong>Stereo</strong></td>
                </tr>
                <tr valign="top">
<?php
    if($quotetype == 'Pickup')
    {
        echo '<td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">';
        if(isset($icab)) echo $icab;
        echo '</p></td>';
    }
    else
    {
        echo '<td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">';
        if(isset($isunroof)) echo $isunroof;
        echo '</p></td>';
    }
    echo '<td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">';
    if($iradflex == 'No') echo 'Not Needed';
    else
    {
        if($iradflex == 'Flexible') echo 'Flexible ';
        if($iradfm == 1) echo ' (AM/FM';
        if($iradcas == 1)
        {
            if($iradfm == 1) echo ',CAS';
            else echo ' (CAS';
        }
        if($iradcd == 1)
        {
            if(($iradfm == 1) || ($iradcas == 1)) echo ',CD';
            else echo ' (CD';
        }
        if(($iradfm == 1) || ($iradcas == 1) || ($iradcd == 1)) echo ')';
    }
    echo '</p></td></tr>';

    if($quotetype == 'Pickup')
    {
        echo '<tr>';
        echo '<td><strong>Front Seat Type</strong></td>';
        echo '<td><strong>Bed Type</strong></td>';
        echo '</tr>';
        echo '<tr valign="top">';
        echo '<td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">';
        if(isset($ifront)) echo $ifront;
        echo '</p></td>';
        echo '<td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">';
        if(isset($ibedtype)) echo $ibedtype;
        echo '</p></td>';
        echo '</tr>';
    }
?>
            </table>
            <table border="0" width="280" align="right" valign="top" cellspacing="5">
                <tr>
                    <td width="147">Power Door Locks</td>
                    <td width="114"><span style="color: #06C;"><?php if(isset($ipdoor)) echo $ipdoor; ?></span></td>
                </tr>
                <tr>
                    <td>Power Windows</td>
                    <td><span style="color: #06C;"><?php if(isset($ipwin)) echo $ipwin; ?></span></td>
                </tr>
                <tr>
                    <td>Power Seats</td>
                    <td><span style="color: #06C;"><?php if(isset($ipseat)) echo $ipseat; ?></span></td>
                </tr>
                <tr>
                    <td>Heated Seats</td>
                    <td><span style="color: #06C;"><?php if(isset($iheat)) echo $iheat; ?></span></td>
                </tr>
                <tr>
                    <td width="147">Air Conditioning</td>
                    <td width="114"><span style="color: #06C;"><?php if(isset($iair)) echo $iair; ?></span></td>
                </tr>
                <tr>
                    <td>Remote Entry</td>
                    <td><span style="color: #06C;"><?php if(isset($iremote)) echo $iremote; ?></span></td>
                </tr>
                <tr>
                    <td>Traction Conrol</td>
                    <td><span style="color: #06C;"><?php if(isset($itraction)) echo $itraction; ?></span></td>
                </tr>
                <tr>
                    <td>Security System</td>
                    <td><span style="color: #06C;"><?php if(isset($isecure)) echo $isecure; ?></span></td>
                </tr>
                <tr>
                    <td width="147">Cruise Control</td>
                    <td width="114"><span style="color: #06C;"><?php if(isset($icruise)) echo $icruise; ?></span></td>
                </tr>
                <tr>
                    <td>Navigation</td>
                    <td><span style="color: #06C;"><?php if(isset($inavsys)) echo $inavsys; ?></span></td>
                </tr>
<?php
    if($quotetype == 'Pickup')
    {
        echo '<tr>';
        echo '<td>Rear Sliding Window</td>';
        echo '<td><span style="color: #06C;">';
        if(isset($irearwin)) echo $irearwin;
        echo '</span></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Bed Liner</td>';
        echo '<td><span style="color: #06C;">';
        if(isset($ibed)) echo $ibed;
        echo '</span></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Tow Package</td>';
        echo '<td><span style="color: #06C;">';
        if(isset($itow)) echo $itow;
        echo '</span></td>';
        echo '</tr>';
    }
    else
    {
        echo '<tr>';
        echo '<td>Luggage Rack</td>';
        echo '<td><span style="color: #06C;">';
        if(isset($iluggage)) echo $iluggage;
        echo '</span></td>';
        echo '</tr>';
    }
?>
            </table>
            <br clear="all" />
            <br />
            <h4 class="subhead" style="font-size:18px;">Additional Information</h4>
            <br />
            <table border="0" width="300" align="left">
                <tr>
                    <td><strong>Preferred color combinations:</strong></td>
                </tr>
                <tr>
                    <td><?php if(isset($icolors)) echo $icolors; ?></td>
                </tr>
            </table>
            <table border="0" width="300" align="right">
                <tr>
                    <td><strong>Special notes:</strong></td>
                </tr>
                <tr>
                    <td><?php if(isset($inotes)) echo $inotes; ?></td>
                </tr>
            </table>
            <br clear="all" />
            <h4 class="subhead" style="font-size:18px;">Delivery</h4>
<?php
    if(isset($idelorpick) && ($idelorpick == 1)) echo '<p>I will pick the vehicle up at my 1-800-vehicles dealer location.</p>';
    else echo '<p>Please deliver to:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$idelcity.', '.$idelstate.'</p>';
?>
            <h4 class="subhead" style="font-size:18px;">Quote(s)</h4>
<?php
            //<p class="blackeleven">(plus tax, title, license and co-pays)</p>
?>
            <table border="0" width="600">
                <tr>
                    <td><span style="color: #98c534;"><strong>Order Type</strong></span></td>
                    <td><strong>Received On</strong></td>
                    <td><strong>Estimated Mileage</strong></td>
                </tr>
                <tr valign="top">
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    // Adjust the display to something other than what is stored in the database...
    if(isset($fordertype))
    {
        $disptype = $fordertype;
        if($fordertype == 'No Order or Posting Available') $disptype = 'Cannot be Ordered or Posted';
        if($fordertype == 'Group') $disptype = 'Group Watchlist';
        echo $disptype;
    }
?>
                    </p></td>
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;"><?php if(isset($fupdated)) echo date_at_timezone('m/d/Y', 'EST', $fupdated); ?></p></td>
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($fordertype) && ($fordertype != 'No Order or Posting Available'))
    {
        if(isset($fmiles))
        {
            echo number_format($fmiles);
        }
        else echo 'Any';
    }
    else echo '?';
?>
                    </p></td>
                </tr>
<?php
    echo '<tr><td colspan="3"><p style="margin-top: 0;"><strong>Exclusive Search Period</strong>: &nbsp;';
    if(($fordertype != 'No Order or Posting Available') && ($fordertype != 'Group')  && isset($fwaitperiod)) echo number_format($fwaitperiod).' Days';
    elseif($fordertype == 'Group') echo 'None';
    else echo '?';
    echo '</p></td></tr>';

    if(isset($fordertype) && ($fordertype != 'No Order or Posting Available'))
    {
        if(isset($fprice))
        {
            //if($fadddel > 0)
            //{
                echo '<tr><td colspan="3"><span style="color: #98c534;"><strong>Pricing Summary</span></strong> (plus tax, title, license and any co-pays)</td></tr>';
                echo '<tr><td><strong>Vehicle</strong></td><td style="text-align:right;">$'.number_format($fprice).'</td><td>&nbsp;</td></tr>';
                echo '<tr><td><strong>Delivery</strong></td><td style="text-align:right;">$'.number_format($fadddel).'</td><td>&nbsp;</td></tr>';
                echo '<tr><td><strong>Price</strong></td><td style="text-align:right; color:#85c11b; font-size:14px;"><strong>$'.number_format($fadddel+$fprice).'</strong></td><td>&nbsp;</td></tr>';
            //}
            //else
            //{
            //    echo '<tr><td><strong>Price (without delivery)</strong></td><td style="color:#85c11b; font-size:14px;"><strong>$'.number_format($fprice).'</strong></td></tr>';
            //}
        }
        else echo '<tr><td><strong>Price (without delivery)</strong></td><td>Any</td></tr>';
    }
    else echo '<tr><td><strong>Price (without delivery)</strong></td><td>?</td></tr>';
?>
            </table>
<?php
    if(($fadddep > 0) || ($faddtir > 0) || ($faddsec > 0) || ($faddnav > 0) || ($faddsel > 0) || ($faddpur > 0))
    {
?>
            <h4 class="subhead" style="font-size:18px; width: 250px;">Additional Amounts & Co-Pays</h4>
            <p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">(in addition to amounts specified in Specific Vehicle Purchase Agreement)</p>
            <table border="0" width="400">
                <tr>
                    <td><strong>Deposit</strong></td>
                    <td><strong>Tire Copay</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($fadddep)) echo '$'.number_format($fadddep);
?>
                    </p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($faddtire)) echo '$'.number_format($faddtire);
?>
                    </p></td>
                </tr>
                <tr>
                    <td><strong>Second Key Limit</strong></td>
                    <td><strong>Nav Disc Limit</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($faddsec)) echo '$'.number_format($faddsec);
?>
                    </p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($faddnav)) echo '$'.number_format($faddnav);
?>
                    </p></td>
                </tr>
                <tr>
                    <td><strong>Seller Repair Limit</strong></td>
                    <td><strong>Purchaser Repair Limit</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($faddsel)) echo '$'.number_format($faddsel);
?>
                    </p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($faddpur)) echo '$'.number_format($faddpur);
?>
                    </p></td>
                </tr>
            </table>
<?php
    }
?>
<?php
    if(isset($fordertype) && ($fordertype == 'Group'))
    {
        if($isposted == 'true')
        {
            echo '<p>This vehicle has already been posted. &nbsp;To see the posting go to "<a href="watchlist.php">Watchlist Details</a>".  To see specific vehicles quoted (SVQs) do to "<a href="svqlist.php">SVQ List</a>".</p>';
        }
        else
        {
            echo "<p>To post this vehicle on the Group Watchlist, click on the 'Post To Watchlist' button. Your posting will be completed if approved by your Sales Representative.</p>";
            echo '<form action="posttogroupterms.php" method="post" name="orderform">';
            echo '<input type="hidden" value="'.$firmquoteid.'" name="FirmQuoteID[]" />';
            echo '<button type="submit" class="med">POST TO WATCHLIST</button>';
            echo '</form>';
        }
    }
    else if(isset($fordertype) && ($fordertype == 'No Order or Posting Available'))
    {
        echo '<p>This vehicle may not be ordered or posted. &nbsp;Your Sales Representative has the details on why there is an issue with providing a Firm Quote in this case.</p>';
    }
    else
    {
        if($isordered == 'true')
        {
            echo '<p>This vehicle has already been ordered. &nbsp;To see the order go to "<a href="orderdetails.php">Order Details</a>".  To see specific vehicles quoted go to "<a href="svqlist.php">All Specific Vehicle Quotes</a>".</p>';
        }
        else
        {
            echo "<p>To order this vehicle, simply click on the 'Place Order' button. You will be agreeing to the search agreement terms listed below. This order may be cancelled after 14 days as long as your vehicle has not been purchased on the wholesale market.</p>";
            echo '<form action="placeorderterms.php" method="post" name="orderform">';
            echo '<input type="hidden" value="'.$firmquoteid.'" name="FirmQuoteID[]" />';
            echo '<button type="submit" class="med">PLACE ORDER</button>';
            echo '</form>';
        }
    }
?>
        </div><!--grideightgrey-->
<?php
    if($fordertype == 'Standard')
    {
        echo '<h2 class="subhead" style="width: 235px;">Standard Order Agreement</h2>';
        $aname = 'Standard Order';
    }
    else if($fordertype == 'Special')
    {
        echo '<h2 class="subhead" style="width: 235px;">Special Order Agreement</h2>';
        $aname = 'Special Order';
    }
    else
    {
        echo '<h2 class="subhead" style="width: 235px;">Group Watchlist Agreement</h2>';
        $aname = 'Group Watchlist Posting';
    }
    $pname = 'Specific Vehicle Purchase';

    echo '<div class="grideight" style="padding:5px;">';

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select AgreementID from agreements where AgreementName='".$aname."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $aid = $row[0];

            $query = "select SectionName, SectionText from agreementsections where AgreementID=".$aid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $secname[$index] = $row[0];
                $sectext[$index] = $row[1];
                $index++;
            }
        }

        $query = "select AgreementID from agreements where AgreementName='".$pname."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $pid = $row[0];

            $query = "select SectionName, SectionText from agreementsections where AgreementID=".$pid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $psecname[$index] = $row[0];
                $psectext[$index] = $row[1];
                $index++;
            }
        }

        mysql_close($con);
    }

    $count = count($secname);
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($sectext[$index]);
        //echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($secname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($secname[$index]).'</strong> - '.$sectext[$index].'</p>';
    }

    echo '</div><!--end grideight-->';
    echo '<h2 class="subhead" style="width: 335px;">Specific Vehicle Purchase Agreement</h2>';
    echo '<div class="grideight" style="padding:5px;">';
    echo '<p class="blackfourteen" style="color: rgb(193, 133, 27); margin-left: 20px; margin-right: 125px;"><strong>Note: This is here to show you what the agreement looks like when you actually purchase a vehicle.  You are not bound to this until an order or posting is placed with us.</strong></p>';

    $count = count($psecname);
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($psectext[$index]);
        //echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($psecname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($psecname[$index]).'</strong> - '.$psectext[$index].'</p>';
    }
?>
<?php
/*
            <p class="blackfourteen"><strong>PURCHASE AGREEMENT</strong></p>
            <p class="blackfourteen" align="justify" >THIS STANDARD ORDER AGREEMENT, made online by the client listed above (Purchaser) and the 1-800-vehicles.com dealer (Seller) has been entered into for the purpose of finding Purchaser a pre-owned vehicle closely meeting the description given in the 'Firm Quote' or 'Firm Quotes' attached hereto.</p>
            <p class="blackfourteen"><strong>SEARCH TERM</strong></p>
            <p class="blackfourteen" align="justify" >This agreement commences upon the order placed by Purchaser and terminates upon the successful delivery of the vehicle or cancellation of the order. A full refund of Purchaser's $250.00 deposit will be returned within three business days of such cancellation. Seller shall be allowed up to two weeks (after the wholesale purchase has been made) to make the vehicle ready for inspection by Purchaser. IT IS UNDERSTOOD THAT THE &quot;CONDITION OF THE VEHICLE&quot; TERMS SHOWN BELOW ARE WHAT PURCHASER WILL BE EXPECTED TO ACCEPT ONCE A SPECIFIC VEHICLE HAS BEEN APPROVED. IT IS ALSO UNDERSTOOD THAT NO VEHICLE WILL BE PURCHASED FOR THE CLIENT WITHOUT HIS OR HER APPROVAL OF THE SPECIFIC VEHICLE.</p>
            <p class="blackfourteen"><strong>CONDITION OF THE VEHICLE</strong></p>
            <p class="blackfourteen" align="justify" >IN GENERAL- The vehicle delivered will be in excellent mechanical and cosmetic condition with no frame damage, flood damage or salvaged title. The vehicle will have all standard equipment, such as floormats, owner's manual, CD cartridge, wiper blades, spare tire, jack, etc., which will be in new or like new condition. Seller will provide a master key and remote (if applicable), and will reimburse Purchaser up to $75.00 towards a second key and remote if not provided. Seller will also reimburse Purchaser up to $75.00 towards any navigation disc that may be needed. The vehicle will have smooth riding tires with at least 4/32 inch of tread life remaining or Seller and Purchaser will split the cost of new tires (Firestone Tire products shall be the brand used for pricing). The vehicle will be professionally detailed, including door ding removal and touch up painting of small scratches (to the best of professional ability).</p>
            <p class="blackfourteen" align="justify" >SERVICE AND REPAIRS- Purchaser shall choose a mechanic to inspect the vehicle once it has been delivered. Seller shall pay up to $400.00 towards any service or repair needs recommended by this mechanic. Purchaser agrees to contribute up to $300.00 in addition to Seller's $400.00 (if necessary), but any amount above a total bill of $700.00 will be the responsibility of the Seller. If Seller chooses not to pay an amount over $700.00, Purchaser is not obligated to purchase that vehicle (deposit refunded). If any inspection fee is charged by this mechanic, it will be payed for by the Purchaser. Seller reserves the right to request a second opinion (by another mechanic or specialist of Purchaser's choosing) and may choose the lesser of the two estimates as the true mechanical need. This inspection would be payed for by the Seller. In addition, Seller agrees that if the first mechanic believes that the vehicle has an issue (such as undercarriage rust or poor service history) that would significantly increase the cost of future repairs (and the second mechanic or specialist holds the same belief), Purchaser is not obligated to purchase that vehicle (deposit refunded).</p>
            <p class="blackfourteen" align="justify" >COSMETIC- If Purchaser believes that the vehicle delivered has cosmetic wear and tear in excess of normal use (for vehicles of similar year, make, model and mileage), Seller will take corrective measures satisfactory to Purchaser or release Purchaser from any obligation for the purchase of that vehicle. Under such circumstances, Purchaser will allow Seller a reasonable opportunity to make such corrections. </p>
            <p class="blackfourteen"><strong>UNDERSTANDING</strong></p>
            <p class="blackfourteen" align="justify" >Purchaser understands that the vehicle ordered is being purchased by Seller specifically for him or her and that Seller has no desire or intention to inventory this vehicle for the purpose of resale. Purchaser understands that the vehicle ordered may or may not have service records. In addition, Purchaser understands that if he or she does not complete the purchase of the vehicle within three business days after delivery (for reasons other than those provided in this agreement), he or she will forfeit the deposit and could be billed for shipping expenses and transaction fees incurred by Seller. If the $250 deposit has not been received by Seller at the time of such an event, Purchaser may also be billed for the deposit amount by Seller.</p>
            <p class="blackfourteen"><strong>DEPOSIT AMOUNT: $250.00</strong></p>
            <p class="blackfourteen" align="justify" >The deposit will be applied to the purchase price of the vehicle ordered, unless this agreement is cancelled (deposit refunded) or Purchaser fails to perform as agreed (deposit kept).</p>
*/
?>
            <p class="blackfourteen" style="color:#85c11b;"><strong>THANK YOU FOR GIVING US AN OPPORTUNITY TO SERVE YOU! </strong></p>
<?php
    }
?>
        </div><!--end grideight-->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
