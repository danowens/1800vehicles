<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = "All Virtual Vehicles";
    $_SESSION['onloadfunction'] = 'initform()';
    //$_SESSION['ShowError'] .= $_POST['savebtn'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

                if(empty($_POST['Reason']))
                {
                    // purposefully empty
                }
                else if(($_POST['Reason'] == 'AddNew') || ((strlen($_POST['GroupID']) < 1) && ($_POST['Reason'] == 'EditExisting')))
        {
            $anquery = "insert into vehiclegroups (groupname) values ('New Group')";
                        if(!mysql_query($anquery, $con))
                        {
                            if(!isset($_SESSION['ShowError']))
                            {
                                $_SESSION['ShowError'] = '';
                            }
                               
                            $_SESSION['ShowError'] .= 'Could not add a new group.';
                        }
            else $_SESSION['LastGroup'] = mysql_insert_id($con);
        }
                else if($_POST['Reason'] == 'EditExisting')
        {
            $eequery = "update vehiclegroups set ";
            if(isset($_POST['groupname']) && (strlen($_POST['groupname']) > 0)) $eequery .= "groupname = '".$_POST['groupname']."'";
            else $eequery .= "groupname = 'Unknown'";
            $eequery .= ", visible = ".$_POST['statuslist'];
            $eequery .= ", year = ".$_POST['yearlist'];
            $eequery .= ", makeid = ".$_POST['makelist'];
            if(isset($_POST['modelname']) && (strlen($_POST['modelname']) > 0)) $eequery .= ", model = '".$_POST['modelname']."'";
            else $eequery .= ", model = NULL";
            if(isset($_POST['stylename']) && (strlen($_POST['stylename']) > 0)) $eequery .= ", style = '".$_POST['stylename']."'";
            else $eequery .= ", style = NULL";
            if(isset($_POST['lowmilestart']) && (strlen($_POST['lowmilestart']) > 0)) $eequery .= ", lowmileagestart = ".$_POST['lowmilestart'];
            else $eequery .= ", lowmileagestart = NULL";
            if(isset($_POST['lowmileend']) && (strlen($_POST['lowmileend']) > 0)) $eequery .= ", lowmileageend = ".$_POST['lowmileend'];
            else $eequery .= ", lowmileageend = NULL";
            if(isset($_POST['highmilestart']) && (strlen($_POST['highmilestart']) > 0)) $eequery .= ", highmileagestart = ".$_POST['highmilestart'];
            else $eequery .= ", highmileagestart = NULL";
            if(isset($_POST['highmileend']) && (strlen($_POST['highmileend']) > 0)) $eequery .= ", highmileageend = ".$_POST['highmileend'];
            else $eequery .= ", highmileageend = NULL";
            if(isset($_POST['lowpricestart']) && (strlen($_POST['lowpricestart']) > 0)) $eequery .= ", lowpricestart = ".$_POST['lowpricestart'];
            else $eequery .= ", lowpricestart = NULL";
            if(isset($_POST['lowpriceend']) && (strlen($_POST['lowpriceend']) > 0)) $eequery .= ", lowpriceend = ".$_POST['lowpriceend'];
            else $eequery .= ", lowpriceend = NULL";
            if(isset($_POST['highpricestart']) && (strlen($_POST['highpricestart']) > 0)) $eequery .= ", highpricestart = ".$_POST['highpricestart'];
            else $eequery .= ", highpricestart = NULL";
            if(isset($_POST['highpriceend']) && (strlen($_POST['highpriceend']) > 0)) $eequery .= ", highpriceend = ".$_POST['highpriceend'];
            else $eequery .= ", highpriceend = NULL";
            $eequery .= ", bestbuy = ".$_POST['bestbuy'];
            $eequery .= ", bodytype = '".$_POST['bodytype']."'";
            $eequery .= ", type = '".$_POST['typelist']."'";
            $eequery .= ", doors = ".$_POST['doorlist'];
            $eequery .= ", convertible = '".$_POST['convlist']."'";
            $eequery .= ", wheeldrive = '".$_POST['wheellist']."'";
            if(strlen($_POST['GroupID']) < 1) $useID = $_SESSION['LastGroup'];
            else $useID = $_POST['GroupID'];
            $eequery .= " where vehiclegroupid = ".$useID;
            if(!mysql_query($eequery, $con)) $_SESSION['ShowError'] .= 'Could not save the group.\n';//.$eequery;

            $_SESSION['LastGroup'] = $_POST['GroupID'];
            if($_POST['savebtn'] == 'view') header('Location: viewgroup.php');
        }
                else if($_POST['Reason'] == 'DeleteCurrent')
        {
            $anquery = "delete from vehiclegroups where vehiclegroupid = ".$_SESSION['LastGroup'];
            if(!mysql_query($anquery, $con)) $_SESSION['ShowError'] .= 'Could not delete the selected group.';
            else $_SESSION['LastGroup'] = 0;
        }

        $vgquery = "select vehiclegroupid, groupname from vehiclegroups order by groupname ASC";
        $vgresult = mysql_query($vgquery, $con);
                if($vgresult)
                {
                    $index = 0;
                    while($vgrow = mysql_fetch_array($vgresult))
                    {
                            $vgs[$index] = $vgrow[0];
                            $vgnames[$index] = $vgrow[1];
                            $index++;
                    }
                    if(!isset($_SESSION['LastGroup']) || ($_SESSION['LastGroup'] == 0))
                    {
                            $_SESSION['LastGroup'] = $vgs[0];
                    }
                }

        $yquery = "select distinct v.year from vehicles v order by 1 desc";
        $yresult = mysql_query($yquery, $con);
                if ($yresult)
                {
                    $index = 0;
                    while($yrow = mysql_fetch_array($yresult))
                    {
                            $allyears[$index] = $yrow[0];
                            $index++;
                    }
                }

        $mquery = "select makeid, name from makes m order by 2";
        $mresult = mysql_query($mquery, $con);
                if ($mresult)
                {
                    $index = 0;
                    while($mrow = mysql_fetch_array($mresult))
                    {
                            $makes[$index] = $mrow[0];
                            $makenames[$index] = $mrow[1];
                            $index++;
                    }
                }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script language="javascript" type="text/javascript">
<!--
    function findIndex(vArray, vValue)
    {
        for (var i=0; i < vArray.length; i++)
        {
            if(vArray[i].value == vValue)
            {
                return i;
            }
        }
        return 0;
    }

    function initform()
    {
        groupchanged();
    }

    var grouphttpObject = null;

    // Get the HTTP Object
    function getHTTPObject()
    {
        var xmlHttp = null;
        try
        {
            // Firefox, Opera, Safari
            xmlHttp = new XMLHttpRequest();
        }
        catch (e)
        {
            // Internet Explorer
            try
            {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e)
            {
                try
                {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e)
                {
                    alert('Your browser must be Firefox, Safari, Opera or IE 5 or higher');
                }
            }
        }
        return xmlHttp;
    }

    function groupchanged()
    {
        var vgrouplist = document.getElementById("grouplist");

        grouphttpObject = getHTTPObject();
        if (grouphttpObject != null)
        {
            grouphttpObject.open("GET", "ajaxgetgroup.php?Group=" + vgrouplist.options[vgrouplist.selectedIndex].value, true);
            grouphttpObject.send(null);
            grouphttpObject.onreadystatechange = groupReturned;
            vgrouplist.style.cursor = "wait";
        }
    }

    function groupReturned()
    {
        if(grouphttpObject.readyState == 4)
        {
            var vgrouplist = document.getElementById("grouplist");

            //alert(grouphttpObject.responseText);
            names = grouphttpObject.responseText.split('#');

            document.getElementById("groupname").value = names[0];
            document.getElementById("grouptitle").innerHTML = names[0];

            document.getElementById("GroupID").value = names[2];

            vStatus = document.getElementById("statuslist");
            vIndex = findIndex(vStatus.options, names[4]);
            vStatus.options[vIndex].selected = true;

            vMake = document.getElementById("makelist");
            vIndex = findIndex(vMake.options, names[6]);
            vMake.options[vIndex].selected = true;

            vYear = document.getElementById("yearlist");
            vIndex = findIndex(vYear.options, names[8]);
            vYear.options[vIndex].selected = true;

            document.getElementById("modelname").value = names[10];

            document.getElementById("stylename").value = names[12];

            vType = document.getElementById("typelist");
            vIndex = findIndex(vType.options, names[14]);
            vType.options[vIndex].selected = true;

            vDoors = document.getElementById("doorlist");
            vIndex = findIndex(vDoors.options, names[16]);
            vDoors.options[vIndex].selected = true;

            vConv = document.getElementById("convlist");
            vIndex = findIndex(vConv.options, names[18]);
            vConv.options[vIndex].selected = true;

            vWheel = document.getElementById("wheellist");
            vIndex = findIndex(vWheel.options, names[20]);
            vWheel.options[vIndex].selected = true;

            vBodyType = document.getElementById("bodytype");
            vIndex = findIndex(vBodyType.options, names[22]);
            vBodyType.options[vIndex].selected = true;

            document.getElementById("lowmilestart").value = names[28];

            document.getElementById("lowmileend").value = names[30];

            document.getElementById("highmilestart").value = names[32];

            document.getElementById("highmileend").value = names[34];

            document.getElementById("lowpricestart").value = names[36];

            document.getElementById("lowpriceend").value = names[38];

            document.getElementById("highpricestart").value = names[40];

            document.getElementById("highpriceend").value = names[42];

            vBestBuy = document.getElementById("bestbuy");
            vIndex = findIndex(vBestBuy.options, names[44]);
            vBestBuy.options[vIndex].selected = true;

            document.getElementById("numrows").value = names[50];

            vgrouplist.style.cursor = "default";
        }
    }

    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }

//-->
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width:275px;">Virtual Vehicle Groups</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: 0px;">
                <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
                <br/>
                <form action="allvehicleedit.php" method="post">
                    <input type="hidden" name="AddEditType" value="AddNew" />
                    <button type="submit" value="" class="med">Add New Vehicle</button>
                </form>
                <br/>
                <h4 class="subhead">Vehicle Groups</h4>
                <table border="0" width="600" cellspacing="10" style="margin-left:-10px;">
                    <tr><td>
                        <form action="virtualvehiclegroups.php" method="post">
                            <input type="hidden" name="Reason" value="AddNew" />
                            <button type="submit" value="" class="med">Add New Group</button>
                        </form>
                    </td>
                    <td>
                        <form action="virtualvehiclegroups.php" method="post">
                            <input type="hidden" name="Reason" value="DeleteCurrent" />
                            <button type="submit" value="" style="float:right" class="med">Delete this Group</button>
                        </form>
                    </td></tr>
                </table>
                <table border="0" width="600" cellspacing="10" style="margin-left:-10px;">
                    <tr valign="baseline">
                        <td width="150">Select a Saved Group</td>
                        <td width="450"><select style="width: 400px;" name="grouplist" id="grouplist" onchange="javascript:groupchanged();">
<?php
    $count = count($vgs);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$vgs[$i].'"';
        if(isset($_SESSION['LastGroup']) && ($_SESSION['LastGroup'] == $vgs[$i])) echo 'selected="selected"';
        echo '>'.$vgnames[$i].'</option>';
    }
?>
                            </select></td>
                    </tr>
                </table>
                <input type="text" id="numrows" name="numrows" style="width: 60px;border: none;" value="0" readonly="readonly" />
                Vehicles in Group (as of last save)
                <br/>
                <form action="virtualvehiclegroups.php" method="post">
                    <input type="hidden" name="Reason" value="EditExisting" />
                    <input type="hidden" name="GroupID" id="GroupID" value="<?php echo $_SESSION['LastGroup']; ?>" />

                    <h4 class="subhead" name="grouptitle" id="grouptitle">This Group</h4>
                    <table border="0" width="600" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td colspan="2">
                                <button type="submit" value="save" name="savebtn" id="savebtn" class="med">Save this Group</button>
                            </td><td colspan="2" align="right">
                                <button type="submit" value="view" name="savebtn" id="savebtn" class="med">Save and View</button>
                            </td>
                        </tr>
                        <tr valign="baseline">
                            <td width="100">Name</td>
                            <td width="150"><input type="text" style="width: 143px;" name="groupname" id="groupname"/></td>
                            <td width="100">Status</td>
                            <td width="150"><select style="width: 150px;" name="statuslist" id="statuslist">
                                <option value="NULL">&lt;Any&gt;</option>
                                <option value="1">Live</option>
                                <option value="0">Not Live</option>
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Year</td>
                            <td><select style="width: 150px;" name="yearlist" id="yearlist">
                                <option value="NULL">&lt;Any&gt;</option>
<?php
    $count = count($allyears);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$allyears[$i].'"';
        echo '>'.$allyears[$i].'</option>';
    }
?>
                            </select></td>
                            <td>Make</td>
                            <td><select style="width: 150px;" name="makelist" id="makelist">
                                <option value="NULL">&lt;Any&gt;</option>
<?php
    $count = count($makes);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$makes[$i].'"';
        echo '>'.$makenames[$i].'</option>';
    }
?>
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Model (contains)</td>
                            <td><input type="text" style="width: 143px;" name="modelname" id="modelname"/></td>
                            <td>Style (contains)</td>
                            <td><input type="text" style="width: 143px;" name="stylename" id="stylename"/></td>
                        </tr>
                        <tr valign="baseline">
                            <td>BB Price over (TODO)</td>
                            <td><input type="text" disabled="disabled" style="width: 143px;" name="bbpricestart" id="bbpricestart" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                            <td>and under</td>
                            <td><input type="text" disabled="disabled" style="width: 143px;" name="bbpriceend" id="bbpriceend" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Low Miles over</td>
                            <td><input type="text" style="width: 143px;" name="lowmilestart" id="lowmilestart" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                            <td>and under</td>
                            <td><input type="text" style="width: 143px;" name="lowmileend" id="lowmileend" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Avg Miles over</td>
                            <td><input type="text" style="width: 143px;" name="highmilestart" id="highmilestart" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                            <td>and under</td>
                            <td><input type="text" style="width: 143px;" name="highmileend" id="highmileend" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Avg Miles over</td>
                            <td><input type="text" style="width: 143px;" name="highmilestart" id="highmilestart" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                            <td>and under</td>
                            <td><input type="text" style="width: 143px;" name="highmileend" id="highmileend" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Low Mileage $ over</td>
                            <td><input type="text" style="width: 143px;" name="lowpricestart" id="lowpricestart" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                            <td>and under</td>
                            <td><input type="text" style="width: 143px;" name="lowpriceend" id="lowpriceend" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Avg Mileage $ over</td>
                            <td><input type="text" style="width: 143px;" name="highpricestart" id="highpricestart" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                            <td>and under</td>
                            <td><input type="text" style="width: 143px;" name="highpriceend" id="highpriceend" maxlength="7" onkeypress="javascript:return numbersonly(event);"/></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Type</td>
                            <td><select style="width: 150px;" name="typelist" id="typelist">
                                <option value="NULL">&lt;Any&gt;</option>
                                <option value="Auto">Auto</option>
                                <option value="MiniVan">MiniVan</option>
                                <option value="Pick-Up">Pick-Up</option>
                                <option value="SUV">SUV</option>
                            </select></td>
                            <td>Doors</td>
                            <td><select style="width: 150px;" name="doorlist" id="doorlist">
                                <option value="NULL">&lt;Any&gt;</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Convertible</td>
                            <td><select style="width: 150px;" name="convlist" id="convlist">
                                <option value="NULL">&lt;Any&gt;</option>
                                <option value="N/A">N/A</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select></td>
                            <td>Wheel Drive</td>
                            <td><select style="width: 150px;" name="wheellist" id="wheellist">
                                <option value="NULL">&lt;Any&gt;</option>
                                <option value="2">2</option>
                                <option value="4">4</option>
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Body Type</td>
                            <td><select style="width: 150px;" name="bodytype" id="bodytype">
                                <option value="NULL">&lt;Any&gt;</option>
                                <option value="N/A">N/A</option>
                                <option value="Hatchback">Hatchback</option>
                                <option value="Reg Cab">Reg Cab</option>
                                <option value="Ext Cab">Ext Cab</option>
                            </select></td>
                            <td>Exc. Availability</td>
                            <td><select style="width: 150px;" name="bestbuy" id="bestbuy">
                                <option value="NULL">&lt;Any&gt;</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select></td>
                        </tr>
                    </table>
                </form>
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
