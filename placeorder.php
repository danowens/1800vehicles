<?php require("globals.php"); ?>
<?php
    require_once(WEB_ROOT_PATH.'common/functions/string.php');
    $_SESSION['state'] = 2;
    $_SESSION['substate'] = 2;
    $_SESSION['titleadd'] = 'Place an Order';

    $marketneedid = $_SESSION['marketneedid'];
    $anyordered = 0;

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select q.QuoteRequestID, q.Year, q.Make, q.Model, q.Style, f.LastUpdated, q.QuoteType, f.OrderType, f.PricePoint, f.FirmQuoteID, f.LastUpdated, f.WaitPeriod from quoterequests q,firmquotes f where f.QuoteRequestID=q.QuoteRequestID and f.OrderType in ('Standard','Special') and f.visible=1 and f.UpdateStatus='Completed' and q.Visible=1 and q.MarketNeedID=".$marketneedid;
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $fquoteid[$index] = $row[0];
            $fyear[$index] = $row[1];
            $fmake[$index] = $row[2];
            $fmodel[$index] = $row[3];
            $fstyle[$index] = $row[4];
            $fupdated[$index] = $row[5];
            $fquotetype[$index] = $row[6];
            $fordertype[$index] = $row[7];
            $fprice[$index] = $row[8];
            $ffirmid[$index] = $row[9];
            $flastup[$index] = $row[10];
            $fwaitperiod[$index] = $row[11];

            // Make sure there is not already an order in for this marketneed...
            $oquery = "select o.Accepted from quoterequests q,firmquotes f,orderfor of,orders o where o.OrderID=of.OrderID and of.FirmQuoteID=f.FirmQuoteID and f.QuoteRequestID=q.QuoteRequestID and q.MarketNeedID=".$marketneedid;
            $oresult = mysql_query($oquery);
            if($orow = mysql_fetch_array($oresult))
            {
                $forderdate[$index] = $orow[0];
                $anyordered = 1;
            }

            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function checkselected()
    {
        var vnumbox = document.getElementById("NUMBOXES");
        var count = parseInt(vnumbox.value, 10);
        var selected = 0;
        for(var i = 0; i < count; i++)
        {
            var vboxname = 'sel'+i;
            var vnumbox = document.getElementById(vboxname);
            if(vnumbox.checked) selected++;
        }
        if(selected < 1)
        {
            alert('There were no quotes selected.  Please select the checkboxes next to the quote(s) to order and then try again.'+'\n');
            return false;
        }
        else return true;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead">Place An Order</h1>
        <div class="grideightgrey">
<?php
    $numchecks = 0;
    $count = count($ffirmid);
    if($count > 0)
    {
        echo '<form action="placeorderterms.php" method="post" onsubmit="javascript:return checkselected();">';
        echo '<table width="600" border="0" cellspacing="0" cellpadding="5">';
        echo '<tr>';
        echo '<td>&nbsp;</td>';
        //echo '<td width="50" align="center"><h3 class="greensub">No.</h3></td>';
        $plural = pluralize_noun($count, "CURRENT MARKET STUD", "IES", "Y");
        echo '<td width="175"><h3 class="greensub">'.$plural.'</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">STATUS</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">AVAILABILITY</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">ESP</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">PRICE</h3></td>';
        echo '</tr>';
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td width="5">';
            if(!isset($forderdate[$i]))
            {
                $days = round((strtotime(date_at_timezone('m/d/Y H:i:s', 'EST')) - strtotime(date_at_timezone('m/d/Y H:i:s', 'EST', $flastup[$i]))) / (60 * 60 * 24));
                if($days <= 30)
                {
                    echo '<input name="FirmQuoteID[]" id="sel'.$numchecks.'" type="checkbox" value="'.$ffirmid[$i].'" />';
                    $numchecks++;
                }
            }
            echo '</td>';
            //echo '<td align="center">';
            //echo '<label for="select3"><p class="greyeleven">';
            //echo str_pad($fquoteid[$i],5,"0",STR_PAD_LEFT);
            //echo '</p><br /></label>';
            //echo '</td>';
            echo '<td>';
            echo '<p class="formbluetext">';
            echo '<a href="quotereceived.php?QuoteID='.$fquoteid[$i].'">';
            echo $fyear[$i].' '.$fmake[$i].' '.$fmodel[$i].' '.$fstyle[$i];
            echo '</a>';
            echo '</p>';
            echo '<p class="greyeleven">';
            echo date_at_timezone('m/d/Y H:i:s T', 'EST', $fupdated[$i]);
            echo '</p>';
            echo '</td>';
            if(!isset($forderdate[$i])) echo '<td align="center"><p class="greyeleven">Received</p></td>';
            else echo '<td align="center"><p class="greyeleven">Ordered</p></td>';
            echo '<td align="center"><p class="greyeleven">'.$fordertype[$i].'</p></td>';
            echo '<td align="center"><p class="greyeleven">'.$fwaitperiod[$i].' Days</p></td>';
            echo '<td align="center"><p class="greyeleven">$'.number_format($fprice[$i]).'</p></td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '<input type="hidden" value="'.$numchecks.'" id="NUMBOXES"/>';
        echo '<br/>';
        echo '<table border="0" width="600" cellpadding="5">';
        echo '<tr><td colspan="2">*Your order may include Current Market Studies with Standard Order availability OR Special Order availability, not both together.  In some cases, your Sales Representative may be able to change the order availability on one or the other to get them both onto a single order. Your Exclusive Search Period (ESP) on your order will match the longest period of time on any included quotes.</td></tr>';
        echo '<tr>';
        if(($anyordered == 0) && ($numchecks > 0)) echo '<td><button type="submit" value="" class="med">BEGIN PLACING AN ORDER</button></td>';
        else echo '<tr><td colspan="2">There is already an active Order. To add a Current Market Study you have received to the Order, please contact your Sales Representative.</td></tr>';
        echo '</form>';
        //echo '<td><form action="allfirmquotes.php" method="post"><button type="submit" value="" class="med">CANCEL</button></form></td>';
        echo '</tr>';
        echo '</table>';
    }
    else
    {
        echo '<strong>You must request a quote before you can place an order.</strong>';
    }
?>
        </div><!-- end grideightgrey-->
    </div><!-- end grid eight container -->
    <br clear="all" />
</div>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
