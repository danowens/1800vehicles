<?php require("globals.php"); ?>

<?php
    $vehid = $_REQUEST['vehid'];

    if(!isset($vehid))
    {
?>


<script language=JavaScript>
    //alert('There was an error loading the details.  Please contact your sales representative for details on that vehicle.', 'Error on site');
    //window.close();
</script>
<?php
    }
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        // Get the Make ID since we need it...
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the details...
        $query = 'select makeid, year, model, style, doors, bodytype, convertible, wheeldrive, slidingdoors, type, imagefile from vehicles where vehicleid='.$vehid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $makeid = $row['makeid'];
            $year = $row['year'];
            $model = $row['model'];
            $style = $row['style'];
            $num_doors = $row['doors'];
            $body_type = $row['bodytype'];
            $convertible = $row['convertible'];
            $wheel_drive = $row['wheeldrive'];
            $sliding_doors = $row['slidingdoors'];
            $type = $row['type'];
            $vimage = $row['imagefile'];
        }
        else
        {
?>
<script language=JavaScript>
    //alert('There was an error loading the details.  Please contact your sales representative for details on that vehicle.', 'Error on site');
    //window.close();
</script>
<?php
        }

        // Get the make...
        $query = 'select name, origin from makes where makeid='.$makeid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $make = $row['name'];
            $origin = $row['origin'];
        }
        else
        {
?>
<script language=JavaScript>
    //alert('There was an error loading the details.  Please contact your sales representative for details on that vehicle.', 'Error on site');
    //window.close();
</script>
<?php
        }

        // First we retrieve the details for the high mileage side...
        $highquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=0 and vehicleid = ".$vehid;
        $result = mysql_query($highquery);
        if($result && $row = mysql_fetch_array($result))
        {
            $highbb = $row['bestbuy'];
            $highmilestart = $row['mileagestart'];
            $highmileend = $row['mileageend'];
            $highpricestart = $row['pricestart'];
            $highpriceend = $row['priceend'];
        }
        else
        {
?>
<script language=JavaScript>
    //alert('There was an error loading the details.  Please contact your sales representative for details on that vehicle.', 'Error on site');
    //window.close();
</script>
<?php
        }

        // Next we retrieve the details for the low mileage side...
        $lowquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=1 and vehicleid = ".$vehid;
        $result = mysql_query($lowquery);
        if($result && $row = mysql_fetch_array($result))
        {
            $lowbb = $row['bestbuy'];
            $lowmilestart = $row['mileagestart'];
            $lowmileend = $row['mileageend'];
            $lowpricestart = $row['pricestart'];
            $lowpriceend = $row['priceend'];
        }
        else
        {
?>
<script language=JavaScript>
    //alert('There was an error loading the details.  Please contact your sales representative for details on that vehicle.', 'Error on site');
    //window.close();
</script>
<?php
        }
        mysql_close($con);
    }

    switch($type)
    {
        case 'Auto':
            if($body_type == 'Hatchback')
            {
                if($num_doors > 3) $vehtype = '5 Door Hatchback';
                else $vehtype = '3 Door Hatchback';
            }
            elseif($convertible == 'Yes') $vehtype = $num_doors.' Door Convertible';
            else
            {
                if($num_doors == 4) $vehtype = '4 Door Sedan';
                elseif($num_doors == 2) $vehtype = '2 Door Coupe';
                else $vehtype = '5 Door Wagon';
            }
            break;
        case 'SUV':
            $vehtype = $num_doors.' Door '.$wheel_drive.'WD';
            break;
        case 'MiniVan':
            if($sliding_doors == 'Dual') $vehtype = 'Dual Sliding Doors';
            else $vehtype = 'Mini Van';
            break;
    }

    $_SESSION['titleadd'] = $year.' '.$make.' '.$model.' '.$style;
?>

<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<style>
    nav {
    display: none;
}
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border-top: none;
}
</style>
<div class="gridtwelve"></div>
<div id="content">
    
      
        <div class="grideightcontainer">
        <a href="JavaScript:self.close()"><img src="common/layout/close.gif" border="0" align="right" hspace="3" alt="Close" /></a>
        <a href="javascript:void()" onclick="javascript:window.print();"><img src="common/layout/print.gif" border="0" align="right" alt="Print this page." /></a>
        <div class="grideightgrey">
            <p style="font-size: 18px; margin-bottom: 0px; margin-left: 10px;">
                <?php echo $year.' '.$make.' '.$model.' '.$style; ?>
            </p>
            <p style="font-size: 14px; margin-top:0px; margin-left: 10px;">
<?php echo $origin.' '.$vehtype; ?>
            </p>
            <center>
<?php
    if(!file_exists($vimage)) $imagefile = '';
    else $imagefile = $vimage;

    if($imagefile)
    {
        $max_width = 550;
        $max_height = 550;
        echo '<img class="img-responsive" id="vehimage" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
    }
    else echo '** Image currently not available **';
?>
                <br />
                <br />
                Actual year, make, model and style
            </center>
            <br />
            <table class="table borderless details-box" bordercolor="#142c3c" border="1" cellpadding="0" cellspacing="0"  bgcolor="#ebebeb" align="center">
                <tr>
                    <td width="200" align="center" bgcolor="#142c3c" style="color:#FFF; font-size:13px;"><b>AVERAGE MILEAGE</b></td>
                    <td width="200" align="center" bgcolor="#142c3c" style="color:#FFF; font-size:13px;"><b>LOW MILEAGE</b></td>
                </tr>
                <tr>
                    <td width="400" colspan="2" align="center" bgcolor="#142c3c">
                        <table class="table borderless" border="0" cellpadding="0" cellspacing="0">
                            <!-- mileage -->
                            <tr valign="middle" style="font-size:13px; color:#142c3c;">
                                <td width="80" height="20" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($highmileend); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($highmilestart+(($highmileend-$highmilestart)/2)); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($highmilestart); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($lowmilestart+(($highmilestart-$lowmilestart)/2)); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($lowmilestart); ?>
                                </td>
                            </tr><!-- end mileage -->
                            <tr>
                                    <td height="3" width="40" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="38" bgcolor="#ebebeb" align="center"></td>
                                </tr><!-- end top line of hash marks -->
                                <!-- middle black bar -->
                                <tr>
                                    <td bgcolor="#ebebeb" height="2"></td>
                                    <td colspan="13" height="2" bgcolor="#142c3c"></td>
                                    <td bgcolor="#ebebeb" height="2"></td>
                                </tr><!-- end middle black bar -->
                                <!-- bottom line of hash marks -->
                                <tr>
                                    <td height="3" width="40" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="38" bgcolor="#ebebeb" align="center"></td>
                                </tr><!-- end bottom line of hash marks -->
                            <!-- excellent availability section -->
                            <tr valign="bottom">
                                <td height="20" bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                <td colspan="6" align="center" bgcolor="#ebebeb">
<?php if($highbb == 1) echo '<span class="excellent">Excellent Availability!</span>'; ?>
                                </td>
                                <td bgcolor="#142c3c"></td>
                                <td colspan="6" align="center" bgcolor="#ebebeb">
<?php if($lowbb == 1) echo '<span class="excellent">Excellent Availability!</span>'; ?>
                                </td>
                                <td height="20" bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                            </tr><!-- end excellent availability section -->
                            <!-- estimates -->
                            <tr style="font-size:13px; color:#142c3c;">
                                <td bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                <td class="bodybold" bgcolor="#ebebeb" align="center" colspan="6">
<?php
    if(($lowpricestart == $highpricestart) && ($lowpriceend == $highpriceend))
    {
        echo '$'.number_format($highpricestart).' to $'.number_format($highpricestart + (($highpriceend-$highpricestart)/2));
    }
    else if($highpricestart < 5000) echo 'Below $'.number_format($highpriceend);
    else echo '$'.number_format($highpricestart).' to $'.number_format($highpriceend);
?>
                                </td>
                                <td bgcolor="#142c3c"></td>
                                <td class="bodybold" bgcolor="#ebebeb" align="center" colspan="6">
<?php
    if(($lowpricestart == $highpricestart) && ($lowpriceend == $highpriceend))
    {
        echo '$'.number_format($lowpricestart + (($lowpriceend-$lowpricestart)/2)).' to $'.number_format($lowpriceend);
    }
    else if($lowpricestart < 5000) echo 'Below $'.number_format($lowpriceend);
    else echo '$'.number_format($lowpricestart).' to $'.number_format($lowpriceend);
?>
                                </td>
                                <td bgcolor="#ebebeb" height="20"><img src="common/layout/trans.gif" width="1" height="1"></td>
                            </tr>
                            <!-- end estimates -->
                        </table>
                    </td>
                </tr>
            </table>
            <p style="font-size:11px; color:#142c3c; margin-top:2px; margin-left:10px;" align="center">plus tax, title and license<p>
            <p align="center" style="margin-left:10px; color:#142c3c;">Prices continually fall, therefore an actual quote may be lower than the price ranges shown here.</p>
            <br />
            <center><table width="100%"><tr><td align="center">
             <?php
    if(isset($_SESSION['user'])){
    ?>
                            
                            <form action="favorites.php" method="post" name="saveform">
            <input type="hidden" value="<?php echo $vehid; ?>" name="AddVehicleID" />
            <button type="submit" class="med">SAVE TO FAVORITES</button>
            </form>
    <?php } ?>           
                            
            </td><td align="center">
            <form action=<?php echo '"quote.php?makeitem='.$make.'&yearitem='.$year.'&modelitem='.$model.'&styleitem='.$style.'&QuoteType=Standard"'; ?> method="post">
                <button type="submit" value="" class="med">REQUEST A CURRENT MARKET STUDY</button>
            </form>
            </td></tr></table></center>
        </div>    <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
       
  
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
