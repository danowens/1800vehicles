<div id="rotator">
    <div id="slider2">
        <ul>
            <li>
                <img src="common/layout/slide1.jpg" width="940" height="282" alt=""/>
                <span class="slidecontent">
                    <h1>Learn &amp; Research</h1>
                    <p>Learn how our revolutionary process will save you time, money and hassles.  Find information quickly on all 1-6 year old autos, SUVs, and minivans that are currently available on the national wholesale market.</p>
                </span>
            </li>
            <li>
                <img src="common/layout/slide2.jpg" width="940" height="282" alt=""/>
                <span class="slidecontent">
                    <h1>Consult &amp; Get Quotes</h1>
                    <p>Get free consulting from a 1-800-vehicles.com representative who is a professional car buyer, not a pushy salesperson. Request "Current Market Study" on vehicles that meet your specific needs and get the information you need to place an order (or post to our watch list).</p>
                </span>
            </li>
            <li>
                <img src="common/layout/slide3.jpg" width="940" height="282" alt=""/>
                <span class="slidecontent">
                    <h1><strong>Consider &amp; Approve</strong></h1>
                    <p>Relax while 1-800-vehicles.com searches the national wholesale market to find great vehicles for your consideration. Approve the perfect vehicle through our "Specific Vehicle Quote" process and our buying team will get to work!</p>
                </span>
            </li>
            <li>
                <img src="common/layout/slide4.jpg" width="940" height="282" alt=""/>
                <span class="slidecontent">
                    <h1>Inspect & Take Delivery</h1>
                    <p>Once your specific vehicle has been purchased at the wholesale level, it will be serviced and independently inspected. After you have test driven and approved the vehicle yourself, you will take delivery with confidence knowing you have a great car!</p>
                </span>
            </li>
        </ul>
    </div><!--end slider-->
    <div id="btngetstarted">
        <a href="learntheprocess.php" class="largegrn">Get Started!</a>
    </div><!--end btngetstarted-->
    <div class="clear">
    </div>
</div><!--end rotator-->
