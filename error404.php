<?php require("globals.php"); ?>
<?php
    $_SESSION['substate'] = -1;
    $_SESSION['titleadd'] = 'Error 404';
    $_SESSION['ignoreaccesscheck'] = 'true';
    $_SESSION['hidetabs'] = 'true';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
<center>
<table border="0" width="100%" cellpadding="0" cellspacing="5">
    <tr>
        <td align="center" >
            <font size="+1">File Not Found</font>
        </td>
    </tr>
</table>
<table border="0" width="90%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" width="100%">
            <font size="+1">
            The file you requested could not be found.<br>
            </font>
            <span class="boldtextdkgreen"><font size="+1">
                ~ The administrator has been alerted to this situation. ~<br>
            </font></span>
        </td>
    </tr>
    <tr>
        <td width="100%">
            <font size="+1">

            <br><br>
            <li>If you typed this address in by hand, the spelling or punctuation may be incorrect.<br>
            <li>If you reached this page from a bookmark, it may be outdated.<br><br>

            Please use your browser's <a href="javascript:history.back()">back</a> button to go to the previous page,<br>
            or you can access our home page
            <a href="index.php"><font size="+1">here</font></a>.<br><br>

            </font>
        </td>
    </tr>
</table>
</center>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
