<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 7;
    $_SESSION['titleadd'] = "Copy an Action Plan";

    $userid = $_SESSION['userid'];
    $uadmin = getuserprofile($userid, 'Administrator');
    $uterr = getuserprofile($userid, 'Teritory Admin');
    $ufran = getuserprofile($userid, 'Franchisee');
    $uops = getuserprofile($userid, 'Operations Manager');

    if(isset($_REQUEST['FranchiseeID']) && ($_REQUEST['FranchiseeID'] != -1))
    {
        $franid = $_REQUEST['FranchiseeID'];

        // Verify this user has access to this page...
        if(isinfranchisee($franid, $userid) == 'false')
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x010507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
        if(!(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true')))
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x010507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
    }
    else
    {
        $franid = -1;
        if(!(($uadmin == 'true') || ($uterr == 'true')))
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x010507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
    }

    if(isset($_POST['PostBack']))
    {
        $consp = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($consp)
        {
            mysql_select_db(DB_SERVER_DATABASE, $consp);

            // We will have to add all the Specific Plan Items now...
            $apid = $_POST['theplan'];

            $query = "select Name from actionplans where ActionPlanID=".$apid;
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                $query = "insert into actionplans (Name, CorporateAdded, FranchiseeID, Visible) values ('".$row[0]." (Copy)"."',0,".$franid.",1)";
                if(mysql_query($query, $consp))
                {
                    $newaid = mysql_insert_id($consp);

                    $acquery = "select Name, DisplayOrder, CustSee, ActionCategoryID from actioncategories where ActionPlanID=".$apid;
                    $acresult = mysql_query($acquery);
                    while($acresult && $acrow = mysql_fetch_array($acresult))
                    {
                        $scquery = "insert into actioncategories (ActionPlanID, Name, DisplayOrder, CustSee) values (".$newaid.",'".$acrow[0]."',".$acrow[1].",".$acrow[2].")";
                        if(mysql_query($scquery, $consp))
                        {
                            $catid = mysql_insert_id($consp);

                            $aiquery = "select Name, DisplayOrder, CustSee from actionitems where ActionCategoryID=".$acrow[3];
                            $airesult = mysql_query($aiquery);
                            while($airesult && $airow = mysql_fetch_array($airesult))
                            {
                                $siquery = "insert into actionitems (ActionCategoryID, Name, DisplayOrder, CustSee) values (".$catid.",'".$airow[0]."',".$airow[1].",".$airow[2].")";
                                mysql_query($siquery, $consp);
                            }
                        }
                    }
                }
            }
            mysql_close($consp);
        }

        header('Location: mydashboard.php#admintab');
        exit();
    }

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Only grab the ones that have data in the categories and items as well...
        if($franid != -1) $apquery = "select distinct ap.Name, ap.ActionPlanID from actionplans ap, actioncategories ac, actionitems ai where ap.ActionPlanID=ac.ActionPlanID and ai.ActionCategoryID=ac.ActionCategoryID and ap.Visible=1 and ((ap.CorporateAdded=1) or (ap.CorporateAdded=0 and ap.FranchiseeID=".$franid."))";
        else $apquery = "select distinct ap.Name, ap.ActionPlanID from actionplans ap, actioncategories ac, actionitems ai where ap.ActionPlanID=ac.ActionPlanID and ai.ActionCategoryID=ac.ActionCategoryID and ap.Visible=1";
        $apresult = mysql_query($apquery, $con);
        $index = 0;
        while($apresult && $aprow = mysql_fetch_array($apresult))
        {
            $actionname[$index] = $aprow[0];
            $actionid[$index] = $aprow[1];
            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>
<div id="content">
    <p><a href="mydashboard.php#admintab">Back to Dashboard</a></p>
    <div class="grideightcontainer">
        <h1 class="subhead">Copy Action Plan</h1>
        <div class="grideightgrey" style="color: rgb(20, 44, 60);">
            <form action="copyactionplan.php" onsubmit="javascript:return validateFormOnSubmit();" method="post">
                <input type="hidden" value="<?php echo $franid; ?>" name="FranchiseeID" />
                <input type="hidden" value="true" name="PostBack" />
                <p style="font-size: 18px; font-weight: bold; margin-top: 0pt;">Action Plan</p>
                <table style="margin-left: 5px;" align="left" border="0" cellpadding="5" width="430">
                    <tbody>
                        <tr>
                            <td width="200"><strong>Action Plan Name: </strong></td>
                            <td width="100">
                                <select id="theplan" name="theplan">
<?php
    $count = count($actionid);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$actionid[$i].'">'.$actionname[$i].'</option>';
    }
?>
                                </select>
                            </td>
                            <td width="130" align="right">
                                <button type="submit" value="" class="med"><nobr>SAVE A COPY OF THIS ACTION PLAN</nobr></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br clear="all" />
                <br />
            </form>
        </div><!--grideightgrey-->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
