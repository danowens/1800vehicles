<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 2;
    $_SESSION['substate'] = 5;
    $_SESSION['titleadd'] = 'Post to the Group Watchlist';

    $marketneedid = $_SESSION['marketneedid'];
    $anyposted = 0;

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select q.QuoteRequestID, q.Year, q.Make, q.Model, q.Style, f.LastUpdated, q.QuoteType, f.OrderType, f.PricePoint, f.FirmQuoteID, f.LastUpdated from Quoterequests q,firmquotes f where f.QuoteRequestID=q.QuoteRequestID and f.OrderType='Group' and f.visible=1 and f.UpdateStatus='Completed' and q.Visible=1 and q.MarketNeedID=".$marketneedid;
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $fquoteid[$index] = $row[0];
            $fyear[$index] = $row[1];
            $fmake[$index] = $row[2];
            $fmodel[$index] = $row[3];
            $fstyle[$index] = $row[4];
            $fupdated[$index] = $row[5];
            $fquotetype[$index] = $row[6];
            $fordertype[$index] = $row[7];
            $fprice[$index] = $row[8];
            $ffirmid[$index] = $row[9];
            $flastup[$index] = $row[10];

            // Adjust the display to something other than what is stored in the database...
            if($fordertype[$index] == 'Group') $fordertype[$index] = 'Group Watchlist';

            // Make sure there is not already an order in for this marketneed...
            $oquery = "select w.Accepted from quoterequests q,firmquotes f,watchfor wf,watches w where w.WatchID=wf.WatchID and wf.FirmQuoteID=f.FirmQuoteID and f.QuoteRequestID=q.QuoteRequestID and q.QuoteRequestID=".$fquoteid[$index];
            $oresult = mysql_query($oquery);
            if($orow = mysql_fetch_array($oresult))
            {
                $fwatchdate[$index] = $orow[0];
                $anyposted = 1;
            }

            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function checkselected()
    {
        var vnumbox = document.getElementById("NUMBOXES");
        var count = parseInt(vnumbox.value, 10);
        var selected = 0;
        for(var i = 0; i < count; i++)
        {
            var vboxname = 'sel'+i;
            var vnumbox = document.getElementById(vboxname);
            if(vnumbox.checked) selected++;
        }
        if(selected < 1)
        {
            alert('There were no quotes selected.  Please select the checkboxes next to the quote(s) to post and then try again.'+'\n');
            return false;
        }
        else return true;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 300px;"><nobr>Post to the Group Watchlist</nobr></h1>
        <div class="grideightgrey">
<?php
    $numchecks = 0;
    $count = count($ffirmid);
    if($count > 0)
    {
        echo '<form action="posttogroupterms.php" method="post" onsubmit="javascript:return checkselected();">';
        echo '<table width="600" border="0" cellspacing="0" cellpadding="5">';
        echo '<tr>';
        echo '<td>&nbsp;</td>';
        //echo '<td width="50" align="center"><h3 class="greensub">No.</h3></td>';
        $plural = pluralize_noun($count, "CURRENT MARKET STUD", "IES", "Y");
        echo '<td width="250"><h3 class="greensub">'.$plural.'</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">STATUS</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">AVAILABILITY</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">PRICE</h3></td>';
        echo '</tr>';
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td width="5">';
            if(!isset($fwatchdate[$i]))
            {
                echo '<input name="FirmQuoteID[]" id="sel'.$numchecks.'" type="checkbox" value="'.$ffirmid[$i].'" /><label for="FirmQuoteID[]">&nbsp;</label>';
                $numchecks++;
            }
            echo '</td>';
            //echo '<td align="center">';
            //echo '<label for="select3"><p class="greyeleven">';
            //echo str_pad($fquoteid[$i],5,"0",STR_PAD_LEFT);
            //echo '</p><br /></label>';
            //echo '</td>';
            echo '<td>';
            echo '<p class="formbluetext">';
            echo '<a href="quotereceived.php?QuoteID='.$fquoteid[$i].'">';
            echo $fyear[$i].' '.$fmake[$i].' '.$fmodel[$i].' '.$fstyle[$i];
            echo '</a>';
            echo '</p>';
            echo '<p class="greyeleven">';
            echo date_at_timezone('m/d/Y H:i:s T', 'EST', $fupdated[$i]);
            echo '</p>';
            echo '</td>';
            if(!isset($fwatchdate[$i])) echo '<td align="center"><p class="greyeleven">Received</p></td>';
            else echo '<td align="center"><p class="greyeleven">On Watchlist</p></td>';
            echo '<td align="center"><p class="greyeleven">'.$fordertype[$i].'</p></td>';
            echo '<td align="center"><p class="greyeleven">$'.number_format($fprice[$i]).'</p></td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '<input type="hidden" value="'.$numchecks.'" id="NUMBOXES"/>';
        echo '<br/>';
        echo '<table border="0" width="600" cellpadding="5">';
        echo '<tr><td colspan="2">*Your posting will be added to the Group Watchlist if approved by your Sales Representative.</td></tr>';
        echo '<tr>';
        if($numchecks > 0) echo '<td><button type="submit" value="" class="med">BEGIN POSTING TO THE GROUP WATCHLIST</button></td>';
        if($anyposted > 0) echo '<tr><td colspan="2">There is already an active Posting. You may add another Current Market Study you have received as another posting with approval from your Sales Representative.</td></tr>';
        //echo '<td><form action="allfirmquotes.php" method="post"><button type="submit" value="" class="med">CANCEL</button></form></td>';
        echo '</tr>';
        echo '</table>';
    }
    else
    {
        echo '<strong>You must request a quote before you can post to the group watchlist.</strong>';
    }
?>
        </div>
    </div><!-- end grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
