<?php
    // Functions that are used to display or work with images...

    // ScaledImageFile = Convert files to thumbnails or store with maximum dimensions or just display it
    // $file = string for name of input file
    // $mwidth = maximum width to allow for the file
    // $mheight = maximum height for the file
    // $tofile = string for name of file to write to OR NULL to write to screen
    // $blob = 'true' to get output image to store in a database BLOB field
    // $transparent = 'true' to make the image transparent by pixel at 0,0 color
    // Return = false for a problem OR true or the image itself otherwise
    function ScaledImageFile($file, $mwidth, $mheight, $tofile=NULL, $blob='false', $transparent='true')
    {
        $file = str_replace('\\', '/', $file); // Make sure the slashes are all forward facing...
        $source_pic = $file;
        $max_width = $mwidth;
        $max_height = $mheight;

        list($width, $height, $image_type) = getimagesize($file);

        ini_set('memory_limit', '200M');

        $fext = strtolower(end(explode(".", $file)));
        switch($fext)
        {
            case 'gif':
                $src = imagecreatefromgif($file);
                break;
            case 'jpg':
            case 'jpeg':
            case 'jpe':
                $src = imagecreatefromjpeg($file);
                break;
            case 'png':
                $src = imagecreatefrompng($file);
                break;
            default:
                return false;
                break;
        }
        //switch($image_type)
        //{
        //    case 1: $src = imagecreatefromgif($file); break;
        //    case 2: $src = imagecreatefromjpeg($file);  break;
        //    case 3: $src = imagecreatefrompng($file); break;
        //    default: return false;  break;
        //}
        //echo $file.'='.$image_type.' '.$width.' '.$height.'<br/>';

        $x_ratio = $max_width / $width;
        $y_ratio = $max_height / $height;

        if(($width <= $max_width) && ($height <= $max_height))
        {
            $tn_width = $width;
            $tn_height = $height;
        }
        elseif((($y_ratio * $height) <= $max_height) && ($width <= $max_width))
        {
            $tn_height = ceil($y_ratio * $height);
            $tn_width = ceil($y_ratio * $width);
        }
        elseif((($x_ratio * $width) <= $max_width) && ($height <= $max_height))
        {
            $tn_width = ceil($x_ratio * $width);
            $tn_height = ceil($x_ratio * $height);
        }
        else
        {
            $ratio = min($x_ratio, $y_ratio);
            $tn_width = ceil($ratio * $width);
            $tn_height = ceil($ratio * $height);
        }

        $tmp = imagecreatetruecolor($tn_width, $tn_height);

        // See if we need transparency...
        if($transparent == 'true')
        {
            // Check if this image is PNG or GIF first
            if(($image_type == 1) || ($image_type == 3))
            {
                $ci = imagecolorat($src, 0, 0);
                $clist = imagecolorsforindex($src, $ci);
                $transparent = imagecolorallocate($tmp, $clist['red'], $clist['green'], $clist['blue']);
                imagecolortransparent($tmp, $transparent);
                imagefilledrectangle($tmp, 0, 0, $tn_width, $tn_height, $transparent);
            }
        }
        imagecopyresampled($tmp,$src,0,0,0,0,$tn_width, $tn_height,$width,$height);
        imagedestroy($src);

        // Use the commented code to grab the output buffer when not saving to a file to store in a database BLOB object
        if($blob == 'true')
        {
            //echo 'Blob<br/>';
            ob_start();
            switch($fext)
            {
                case 'gif':
                    imagegif($tmp);
                    break;
                case 'jpg':
                case 'jpeg':
                case 'jpe':
                    imagejpeg($tmp, NULL, 100);
                    break;
                case 'png':
                    imagepng($tmp, NULL, 0);
                    break;
                default:
                    return false;
                    break;
            }
            //switch($image_type)
            //{
            //    case 1: imagegif($tmp); break;
            //    case 2: imagejpeg($tmp, NULL, 100);  break; // best quality
            //    case 3: imagepng($tmp, NULL, 0); break; // no compression
            //    default: return false; break;
            //}
            $final_image = ob_get_contents();
            ob_end_clean();
            imagedestroy($tmp);
            return $final_image;
        }
        else // Send to file name...or pass NULL to output to screen
        {
            if($tofile && (strlen($tofile)>0))
            {
                $ext = strtolower(end(explode(".", $tofile)));
                switch($ext)
                {
                    case 'gif':
                        imagegif($tmp, $tofile);
                        break;
                    case 'jpg':
                    case 'jpeg':
                    case 'jpe':
                        imagejpeg($tmp, $tofile, 100);
                        break;
                    case 'png':
                        imagepng($tmp, $tofile, 0);
                        break;
                    default:
                        imagedestroy($tmp);
                        return false;
                        break;
                }
                imagedestroy($tmp);
                return true;
            }
            else
            {
                // NULL, so going to the screen...
                switch($fext)
                {
                    case 'gif':
                        header('Content-Type: image/gif');
                        imagegif($tmp);
                        break;
                    case 'jpg':
                    case 'jpeg':
                    case 'jpe':
                        header('Content-Type: image/jpeg');
                        imagejpeg($tmp);
                        break;
                    case 'png':
                        header('Content-Type: image/png');
                        imagepng($tmp);
                        break;
                    default:
                        imagedestroy($tmp);
                        return false;
                        break;
                }
                //switch($image_type)
                //{
                //    case 1:
                //        header('Content-Type: image/gif');
                //        imagegif($tmp);
                //        break;
                //    case 2:
                //        header('Content-Type: image/jpeg');
                //        imagejpeg($tmp);
                //        break;
                //    case 3:
                //        header('Content-Type: image/png');
                //        imagepng($tmp);
                //        break;
                //    default:
                //        imagedestroy($tmp);
                //        return false;
                //        break;
                //}
                imagedestroy($tmp);
            }
        }

        return true;
    }
?>
