<?php
    // Functions that are used on the franchise dashboards...

    //require_once(WEB_ROOT_PATH."globals.php");

    // getunassignedleads = finds the list of leads for the user to assign...
    // $puserid = the ID of the user to get leads for
    // $pfranif = the ID of the franchise they are in
    // Return = List in a string of the UserIDs that match the right criteria
    function getunassignedleads($puserid, $pfranid)
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            // Get their franchise member id...
            $query = "select franchiseememberid from franchiseemembers where franchiseeid=".$pfranid." and userid=".$puserid;
            $result = mysql_query($query, $con);
            if($row = mysql_fetch_array($result))
            {
                $memberid = $row[0];

                // First see if they are restricted in any way...
                $query = "select zip from restricttozip where franchiseememberid=".$memberid;
                $result = mysql_query($query, $con);
                if($row = mysql_fetch_array($result))
                {
                    // Get the first one...
                    $zips[0] = $row[0];

                    // Now get the rest...
                    $index = 1;
                    while($row = mysql_fetch_array($result))
                    {
                        $zips[$index] = $row[0];
                        $index++;
                    }
                }
                else  // There are no restrictions on this member...so get the whole franchise list...
                {
                    $query = 'select zip from franchiseetozip where franchiseeid = '.$pfranid;
                    $result = mysql_query($query, $con);
                    $index = 0;
                    while($row = mysql_fetch_array($result))
                    {
                        $zips[$index] = $row[0];
                        $index++;
                    }
                }

                if(count($zips)>0)
                {
                    $ziplist = implode(",", $zips);

                    // Get the Potential Leads...
                    $query = "select u.userid, m.marketneedid from users u, useraddresses a, marketneeds m where m.userid=u.userid and m.seenbysales=0 and u.userid=a.userid and a.isprimary=1 and a.zip in (".$ziplist.")";
                    $result = mysql_query($query, $con);
                    $index = 0;
                    while($row = mysql_fetch_array($result))
                    {
                        $mquery = "select * from assignedreps where marketneedid=".$row[1];
                        $mresult = mysql_query($mquery, $con);
                        if(!$mrow = mysql_fetch_array($mresult))
                        {
                            $userids[$index] = $row[0];
                            $index++;
                        }
                    }
                }
            }

            mysql_close($con);
        }
        if(isset($userids)) return implode(",", $userids);
        else return "";
    }

    // getnonfranunassignedleads = finds the list of leads for the user to assign...
    // $puserid = the ID of the user to get leads for
    // Return = List in a string of the UserIDs that match the right criteria
    function getnonfranunassignedleads($puserid)
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            // Get all currently assigned ZIPs...
            $query = 'select zip from franchiseetozip';
            $result = mysql_query($query, $con);
            $index = 0;
            while($row = mysql_fetch_array($result))
            {
                $zips[$index] = $row[0];
                $index++;
            }

            if(count($zips)>0)
            {
                $ziplist = implode(",", $zips);

                // Get the Potential Leads...
                $query = "select u.userid, m.marketneedid from users u, useraddresses a, marketneeds m where m.userid=u.userid and m.seenbysales=0 and u.userid=a.userid and a.isprimary=1 and a.zip not in (".$ziplist.")";
                $result = mysql_query($query, $con);
                $index = 0;
                while($row = mysql_fetch_array($result))
                {
                    $mquery = "select * from assignedreps where marketneedid=".$row[1];
                    $mresult = mysql_query($mquery, $con);
                    if(!$mrow = mysql_fetch_array($mresult))
                    {
                        $userids[$index] = $row[0];
                        $index++;
                    }
                }
            }

            mysql_close($con);
        }
        if(isset($userids)) return implode(",", $userids);
        else return "";
    }

    // getassignedmarketneeds = finds the list of customers for the salesrep
    // each customer has multiple market needs and each market need has multiple assignedrep records
    //  the current sales rep is based on the most recent start field in the assignedrep for any market needs for this customer
    //  therefore if two market needs exist then both can have different salesreps but the last assigned rep record for any is the winner
    // $puserid = the ID of the salesrep to get customers for
    // Return = List in a string of the MarketNeedIDs that match the right criteria
    function getassignedmarketneeds($puserid, $activeonly = 0)
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            // issues with the old query
            //  1. if a rep WAS working on a market need then they could get it back here even if it has been reassigned to another rep
            //     solution need to get the latest assignedrep record for this market need
            //  2. showing all market needs even if inactive
            //     solution need to filter out inactive if requested with reasonable default
            //  3. TODO add back support for enddate on assignments
            //$query = "select m.MarketNeedID from AssignedReps a,MarketNeeds m where m.MarketNeedID=a.MarketNeedID and ((now() between a.startdate and a.enddate) or (a.enddate is null)) and a.UserRepID=".$puserid;

            $query = "select m.marketneedid, a.userrepid, m.active from assignedreps a, marketneeds m";
            $query .= "    where m.marketneedid=a.marketneedid and ((now() between a.startdate and a.enddate) or (a.enddate is null))";
            $query .= " and a.userrepid=".$puserid;
            $query .= " group by a.marketneedid ";
            $query .= " order by a.startdate desc";
            //$query .= " limit 1";

            $result = mysql_query($query, $con);
            $index = 0;
            while($row = mysql_fetch_array($result))
            {
                // respect activeonly flag
                $isactive = $row[2];
                if (!$activeonly || $isactive == '1') $needids[$index] = $row[0];
                $index++;
            }

            mysql_close($con);
        }

        if(isset($needids)) return implode(",", $needids);
        else return "";
    }

    // getavailablereps = finds the list of salesreps for the franchise at the specified ZIP...
    // $pfranid = the ID of the franchisee to get salesreps for
    // $zipcode = the ZIP the customer is in to get the available reps for
    // Return = List in a string of the UserIDs for the sales reps that match the right criteria
    function getavailablereps($pfranid, $zipcode)
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            // Get the list of members for the franchise...
            $query = "select franchiseememberid, userid from franchiseemembers where franchiseeid=".$pfranid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($row = mysql_fetch_array($result))
            {
                $memberid = $row[0];
                $ifuserid = $row[1];

                // Check whether they are sales reps or not...
                $upquery = "select profileid from userprofiles where userid = ".$ifuserid;
                $upresult = mysql_query($upquery);
                while($uprow = mysql_fetch_array($upresult))
                {
                    $pquery = "select name from profiles where profileid = ".$uprow['profileid'];
                    $presult = mysql_query($pquery);
                    if($prow = mysql_fetch_array($presult))
                    {
                        if($prow['name'] == 'Sales Representative')
                        {
                            // Make sure they are not restricted at all OR restricted to this zipcode...
                            $rquery = "select zip from restricttozip where franchiseememberid=".$memberid;
                            $rresult = mysql_query($rquery, $con);
                            if($rrow = mysql_fetch_array($rresult))
                            {
                                if($rrow[0] == $zipcode)
                                {
                                    $userids[$index] = $ifuserid;
                                    $index++;
                                }
                                else
                                {
                                    while($rrow = mysql_fetch_array($rresult))
                                    {
                                        if($rrow[0] == $zipcode)
                                        {
                                            $userids[$index] = $ifuserid;
                                            $index++;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $userids[$index] = $ifuserid;
                                $index++;
                            }
                        }
                    }
                }
            }

            mysql_close($con);
        }

        if(isset($userids)) return implode(",", $userids);
        else return "";
    }

    // isinfranchisee = finds the list of salesreps for the franchise at the specified ZIP...
    // $pfranid = the ID of the franchisee to get salesreps for
    // $puserid = the ID of the employee to check membership on
    // Return = 'true' if a member, 'false' if not...
    function isinfranchisee($pfranid, $puserid)
    {
        $ismember = 'false';
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            // Get the list of members for the franchise...
            $query = "select * from franchiseemembers where franchiseeid=".$pfranid." and userid=".$puserid;
            $result = mysql_query($query, $con);
            if($row = mysql_fetch_array($result)) $ismember = 'true';

            mysql_close($con);
        }

        return $ismember;
    }

    // isinfranchisee = finds the list of salesreps for the franchise at the specified ZIP...
    // $pfranid = the ID of the franchisee to get salesreps for
    // $puserid = the ID of the employee to check membership on
    // Return = 'true' if a member, 'false' if not...
    function franchiseenamenodb($pcon, $pfranid)
    {
        $return = 'false';

        // Get the list of members for the franchise...
        $query = "select name from franchisees where franchiseeid=".$pfranid;
        $result = mysql_query($query, $pcon);
        if($row = mysql_fetch_array($result)) $return = $row[0];

        return $return;
    }

    // franchiseeofzip = finds the franchisee handling this zip code
    // $pcon = the mysql connection
    // $pzipcode = the zipcode to find in a zipcode list per franchise
    // Return = franchiseID if zip code is in one, -1 if not...
    function franchiseeofzipnodb($pcon, $pzipcode)
    {
        $franid = 0;

        $query = "select franchiseeid from franchiseetozip where zip='".$pzipcode."'";
        $result = mysql_query($query, $pcon);
        if($row = mysql_fetch_array($result))
            $franid=$row[0];

        return $franid;
    }
    
    function get_admin_id()
    {
       
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $query = "SELECT UserID FROM `users` WHERE `Email` = 'dan@1800vs.com' ";

            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
		return $row['UserID'];		
                
            }
            else
            {
                return "";
            }

            mysql_close($con);
        }
       
    }
    
    
    
?>
