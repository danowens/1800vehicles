<?php

require_once(WEB_ROOT_PATH."common/functions/pricefunctions.php");

/**
 * Static class with functions related to vehicles.
 */
class Vehicle {

    public static $TYPES = array('Auto','MiniVan','Pick-Up','SUV');
    public static $TYPES_NEW = array('Auto','MiniVan','SUV');
    public static $CONVERTIBLE = array('N/A','No','Yes');
    public static $WHEEL_DRIVE_OPTIONS = array(
        'N/A' => 'N/A',
        '2'   => '2 Wheel Drive',
        '4'   => '4 Wheel Drive'
    );

    public static $BODY_TYPE_OPTIONS = array(
        'N/A'       => 'N/A',
        'Hatchback' => 'Hatchback',
        'Reg Cab'   => 'Regular Cab',
        'Ext Cab'   => 'Extended Cab',
    );

    public static $ORIGINS = array("Import", "Domestic");

    /**
     * @param $db object a DB object
     * @param $id int    the primary key of the vehicles table
     * @return Associative array of information about the vehicle
     */
    public static function getById($db, $id) {
        $id = $db->real_escape_string($id);
        $sql = "
            select
                vehicles.*,
                ifnull(low.BestBuy , 0) as lowbest,
                ifnull(high.BestBuy, 0) as highbest
            from
                vehicles
                left join vehicledata as low  on (vehicles.VehicleID = low .VehicleID and low .LowMiles = 1)
                left join vehicledata as high on (vehicles.VehicleID = high.VehicleID and high.LowMiles = 0)
            where
                vehicles.VehicleID = $id";
        $res = $db->query($sql);
        if (!$res) {
            return null;
        }
        $vehicle = $res->fetch_assoc();
        return $vehicle;
    }

    public static function getMakes($db, $with_origin=true) {
        if ($with_origin) {
            $sql = "select MakeID, concat(Name, ' (', Origin, ')') as name from makes order by name";
        } else {
            $sql = "select MakeID, Name as name from makes order by name";
        }
        return $db->query_fetch_all_key_value($sql);
    }

    /**
     * Add a make to the makes table.
     *
     * @param $db
     * @param $name   string the name of the make (e.g. "Toyota")
     * @param $origin string "Domestic" or "Foreign"
     * @return MakeID if created, 0 otherwise
     */
    public static function addMake($db, $name, $origin) {
        $name   = $db->real_escape_string($name);
        if (!in_array($origin, self::$ORIGINS)) {
            return 0;
        }
        // skipping the escape since we know $origin is one of the two valid values
        //$origin = $db->real_escape_string($origin);
        $sql = "insert into makes set Name = '$name', Origin = '$origin'";
        $res = $db->query($sql);
        if (!$res) {
            return 0;
        }
        return $db->insert_id;
    }

    /**
     * Create a new vehicle in the vehicles table and add price/mileage entries
     * in the vehiclesdata table.
     */
    public static function add($db, $values) {
        return self::addUpdate($db, $values);
    }

    public static function update($db, $vehicle_id, $values) {
        return self::addUpdate($db, $values, $vehicle_id);
    }

    private static function addUpdate($db, $values, $vehicle_id=null) {

        if (isset($values["highbest"])) {
            $highbest = (bool) $values["highbest"];
            unset($values["highbest"]);
        }
        if (isset($values["lowbest"])) {
            $lowbest = (bool) $values["lowbest"];
            unset($values["lowbest"]);
        }

        $db->query("start transaction");

        $set = $db->getsetfields($values);

        if (isset($vehicle_id)) {
            $sql = "update vehicles set $set where VehicleID = $vehicle_id";
        } else {
            $sql = "insert into vehicles set $set";
        }
        $res = $db->query($sql);
        if (!$res) {
            $db->query("rollback");
            return 0;
        }

        if (!isset($values['BlackBookAvg'])) {
            if (isset($vehicle_id)) {
                $db->query("commit");
                return $vehicle_id;
            }
            // refuse to create a vehicle without a price (and the associated vehicledata entries)
            return 0;
        }
        $bb_price = $values['BlackBookAvg'];
		$best_buy = array('lowbest'=> $lowbest, 'highbest'=>$highbest);
		//print_r($values);
        if (isset($vehicle_id)) {
            $error = setVehiclePrice($db, $vehicle_id, $bb_price);
		
			$error = setVehicleBestBuy($db, $vehicle_id, $lowbest,$highbest);
			$db->query("commit");
            if ($errror) {
                return 0;
            }
            return $vehicle_id;
        }

        $id = $db->insert_id;

        list($lowpricestart,  $lowpriceend, $highpricestart, $highpriceend) = getPrices($bb_price);

        if (!isset($lowbest)) {
            $lowbest = false;
        }

        list($low_miles, $average_miles, $high_miles) = self::getMileage($values['Year']);
        $low_values = array(
            'VehicleID'    => $id,
            'LowMiles'     => 1,
            'MileageStart' => $low_miles,
            'MileageEnd'   => $average_miles,
            'PriceStart'   => $lowpricestart,
            'PriceEnd'     => $lowpriceend,
            'BestBuy'      => $lowbest,
        );
        $set = $db->getsetfields($low_values);
        $sql = "insert into vehicledata set $set";
        $res = $db->query($sql);
        if (!$res) {
            $db->query("rollback");
            return 0;
        }

        if (!isset($highbest)) {
            $highbest = false;
        }
        $high_values = array(
            'VehicleID'    => $id,
            'LowMiles'     => 0,
            'MileageStart' => $average_miles,
            'MileageEnd'   => $high_miles,
            'PriceStart'   => $highpricestart,
            'PriceEnd'     => $highpriceend,
            'BestBuy'      => $highbest,
        );
        $set = $db->getsetfields($high_values);
        $sql = "insert into vehicledata set $set";
        $res = $db->query($sql);
        if (!$res) {
            $db->query("rollback");
            return 0;
        }

        $db->query("commit");
        return $id;
    }

    public static function getMileage($year) {
        $curr_year = date("Y");
        $diff = $curr_year - $year;
        if ($diff < 0) {
            $diff = 0;
        }
        if ($diff < 3) {
            $low = $diff*10000;
            $avg = $diff*15000;
        } else {
            // re-base the mileage at 3 years
            $low = 40000;
            $avg = 35000;
            if ($diff > 3) {
                $add = ($diff - 3)*15000;
                $low += $add;
                $avg += $add;
            }
        }
        return array($low, $avg, $avg+20000);
    }
    
    public static function add_internal_images($db, $values) {
      
        $set = $db->getsetfields($values);
        $sql = "insert into vehicleinternalimages set $set";  
        $res = $db->query($sql);
    }
    
    public static function get_internal_images($db, $vehicle_id) {
         $sql = "select * from vehicleinternalimages where vehilce_id = ".$vehicle_id;  
         $list=array();
        $res = $db->query($sql);
        if (!$res) {
            return null;
        }
       while($vehicle = $res->fetch_assoc()){
           $list[]=$vehicle;
       }
        
        return $list;
    }
    
}

?>
