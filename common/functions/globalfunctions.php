<?php
    // Functions that are used all over the system that should be defined once

    // escapestr = add backslashes to certain characters before sending in an SQL string
    // $str = the string to escape
    // Return = the escapped string
    function escapestr($str)
    {
        $search=array("\\","\0","\n","\r","\x1a","'",'"');
        $replace=array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
        return str_replace($search,$replace,$str);
    }

    // htmlifystr = convert from \r\n to <br/> for email output
    // $str = the string to convert
    // Return = the converted string
    function htmlifystr($str)
    {
        $search=array("\\r\\n","\'",'\"');
        $replace=array("<br/>","'", '"');
        return str_replace($search,$replace,$str);
    }

    // time_diff = determine the number of seconds between two dates in the format 'YYYY-MM-DD HH:MM:SS'
    // $dt1 = current or at least what is expected to the the later date
    // $dt2 = date to look at
    // Return = number of seconds between them (divide by 60 for min, 60*60 for hours, etc.)
    function time_diff($dt1,$dt2)
    {
        $y1 = substr($dt1,0,4);
        $m1 = substr($dt1,5,2);
        $d1 = substr($dt1,8,2);
        $h1 = substr($dt1,11,2);
        $i1 = substr($dt1,14,2);
        $s1 = substr($dt1,17,2);

        $y2 = substr($dt2,0,4);
        $m2 = substr($dt2,5,2);
        $d2 = substr($dt2,8,2);
        $h2 = substr($dt2,11,2);
        $i2 = substr($dt2,14,2);
        $s2 = substr($dt2,17,2);

        return (mktime($h1,$i1,$s1,$m1,$d1,$y1)-mktime($h2,$i2,$s2,$m2,$d2,$y2));
    }

    // timefromnow = how many minutes, hours, days, etc. until a date
    // $sec = seconds in the future
    // Return = string in the format of 'in 6 min', 'in 9 days', etc.
    function timefromnow($sec)
    {
        if($sec == 0) return 'now';
        if($sec < 60) // Within Seconds...
        {
            if($sec == 1) return 'in '.$sec.' second';
            return ' in '.$sec.' seconds';
        }
        elseif($sec < (60*60)) // Within Minutes...
        {
            $sec = round($sec/60);
            if($sec == 1) return 'in '.$sec.' minute';
            return 'in '.$sec.' minutes';
        }
        elseif($sec < (60*60*24)) // Within Hours...
        {
            $sec = round($sec/(60*60));
            if($sec == 1) return 'in '.$sec.' hour';
            return 'in '.$sec.' hours';
        }
        else
        {
            $sec = round($sec/(60*60*24));
            if($sec == 1) return 'in '.$sec.' day';
            return 'in '.$sec.' days';
        }
    }

    // timesincenow = how many minutes, hours, days, etc. have elapsed since a date
    // $datesince = date to look at in format: 'YYYY-MM-DD HH:MM:SS'
    // Return = string in the format of '6 min ago', '9 hours ago', etc.
    function timesincenow($datesince, $future = 0)
    {
        // cant this just use now() ?
        $funcon = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($funcon)
        {
            mysql_select_db(DB_SERVER_DATABASE, $funcon);

            $query = "select '".date_at_timezone('Y-m-d H:i:s', 'EST')."' from marketneeds where marketneedid=1";
            $result = mysql_query($query, $funcon);
            if($row = mysql_fetch_array($result))
            {
                $datenow = $row[0];
            }
            else $datenow = date_at_timezone('Y-m-d H:i:s', 'EST');
            mysql_close($funcon);
        }
        else $datenow = date();
        $sec = time_diff(date_at_timezone('Y-m-d H:i:s','EST', $datenow), date_at_timezone('Y-m-d H:i:s','EST',$datesince));
        if($sec < 0) {
            if ($future)
                return timefromnow(-1 * $sec);
            else
                return 'recently';
        }
        if($sec < 60) // Within Seconds...
        {
            if($sec == 1) return $sec.' second ago';
            return $sec.' seconds ago';
        }
        elseif($sec < (60*60)) // Within Minutes...
        {
            $sec = round($sec/60);
            if($sec == 1) return $sec.' minute ago';
            return $sec.' minutes ago';
        }
        elseif($sec < (60*60*24)) // Within Hours...
        {
            $sec = round($sec/(60*60));
            if($sec == 1) return $sec.' hour ago';
            return $sec.' hours ago';
        }
        else
        {
            $sec = round($sec/(60*60*24));
            if($sec == 1) return $sec.' day ago';
            return $sec.' days ago';
        }
    }

    // date_at_time zone = convert time to a new time zone...
    // $format = the format in date() function styling (will default to common format)
    // $locale = the timezone in which we want the datetime (will default to php timezone set in globals.php)
    // $timestamp = optional, will use current date and time if null is passed in
    // Return = string with properly formatted date with time in proper time zone...
    function date_at_timezone($format = null, $locale = null, $timestamp = null)
    {
        if(is_null($format)) $format = 'm/d/Y H:i:s T';
        if(is_null($locale)) $locale = date_default_timezone_get();
        if(is_null($timestamp)) $timestamp = date($format);

        $date = new DateTime($timestamp, new DateTimeZone(date_default_timezone_get()));
        $date->setTimezone(new DateTimeZone($locale));
        return $date->format($format);
    }

    // replaceagreementtags = replace the tags in the agreement strings with valid data
    // $instr = the string that may have tags in it
    // $inmneed = optional, uses marketneed of current user by default
    // Return = string with all allowed tags replaced...
    /*function replaceagreementtags($instr, $inmneed = -1)
    {
        $textdisp = $instr;

        $funcon = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($funcon)
        {
            mysql_select_db(DB_SERVER_DATABASE, $funcon);

            if($inmneed == -1) $inmneed =     $_SESSION['marketneedid'];

            $query = "select DepositRequired, DepositAmount, SecondKeyLimit, NavDiscLimit, SellerRepairLimit, PurchaserRepairLimit, PurchaseDepositAmount from MarketNeeds where MarketNeedID=".$inmneed;
            $result = mysql_query($query, $funcon);
            if($row = mysql_fetch_array($result))
            {
                $mndepreq = $row[0];
                $mndepamt = $row[1];
                $mnseckey = $row[2];
                $mnnavdisc = $row[3];
                $mnselrep = $row[4];
                $mnpurrep = $row[5];
                $mnpurdep = $row[6];
            }
            else
            {
                mysql_close($funcon);
                return "";
            }

            mysql_close($funcon);
        }

        while($pos = strpos($textdisp,'{deposit_amount}',0))
        {
            if($mndepreq == 1) $textdisp = substr($textdisp,0,$pos).number_format($mndepamt,2).substr($textdisp, $pos+16);
            else $textdisp = substr($textdisp,0,$pos).'0.00'.substr($textdisp, $pos+16);
        }
        while($pos = strpos($textdisp,'{second_key_limit}',0))
        {
            $textdisp = substr($textdisp,0,$pos).number_format($mnseckey,2).substr($textdisp, $pos+18);
        }
        while($pos = strpos($textdisp,'{nav_disc_limit}',0))
        {
            $textdisp = substr($textdisp,0,$pos).number_format($mnnavdisc,2).substr($textdisp, $pos+16);
        }
        while($pos = strpos($textdisp,'{seller_repair_limit}',0))
        {
            $textdisp = substr($textdisp,0,$pos).number_format($mnselrep,2).substr($textdisp, $pos+21);
        }
        while($pos = strpos($textdisp,'{purchaser_repair_limit}',0))
        {
            $textdisp = substr($textdisp,0,$pos).number_format($mnpurrep,2).substr($textdisp, $pos+24);
        }
        while($pos = strpos($textdisp,'{total_repair_limit}',0))
        {
            $textdisp = substr($textdisp,0,$pos).number_format(($mnselrep+$mnpurrep),2).substr($textdisp, $pos+20);
        }
        while($pos = strpos($textdisp,'{purchase_deposit_amount}',0))
        {
            $textdisp = substr($textdisp,0,$pos).number_format($mnpurdep,2).substr($textdisp, $pos+25);
        }
        return $textdisp;
    }*/

    // debugmsg = simple way to get messages sent to a special debug file if needed
    // $message = the text to send out
    // Return = none
    function debugmsg($message)
    {
        $myFile = $path = WEB_ROOT_PATH.'admin/debuglog.txt';
        if(file_exists($myFile)) $openas = 'a';
        else $openas = 'w';
        if($fh = fopen($myFile, $openas))
        {
            $out = date_at_timezone('m/d/Y H:i:s T', 'EST').': '.$message.'\n';
            fwrite($fh, $out);
            fclose($fh);
        }
    }
?>
