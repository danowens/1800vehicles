<?php

/**
 * Static functions related to users.
 */
class User {
    public static $SECURITY_QUESTIONS =  array(
        "firstelem"     => "What is the first elementary school you attended?",
        "mothersmaiden" => "What is your mother's maiden name?",
        "grandma"       => "What is your favorite grandmother's name?",
        "favpetsname"   => "What is your favorite pet's name?",
        "highschool"    => "What was the primary high school you attended?",
        "sports"        => "What is your favorite sport team's name?",
        "fathermid"     => "What is your father's middle name?",
        "mothermid"     => "What is your mother's middle name?",
        "spousemid"     => "What is your spouse's middle name?",
        "childmiddle"   => "What is your first child's middle name?",
        "birthplace"    => "What city were you born in?",
        "favteacher"    => "Who was your favorite teacher?",
        "firstjob"      => "What was your first job growing up?",
    );

    const PROFILE_CUSTOMER        = 'Customer';
    const PROFILE_ADMIN           = 'Administrator';
    const PROFILE_TERRITORY_ADMIN = 'Teritory Admin';
    const PROFILE_FRANCHISEE      = 'Franchisee';
    const PROFILE_OPS_MANAGER     = 'Operations Manager';
    const PROFILE_GENERAL_MANAGER = 'General Manager';
    const PROFILE_BUYER           = 'Researcher';
    const PROFILE_SALES_REP       = 'Sales Representative';

    /**
     * Given a login, which could be an email address, retrive relevant
     * information about the user.
     */
    public static function getUserByLogin($db, $login) {
        $login = $db->real_escape_string($login);
        $sql = "
            select
                u.UserID,
                u.FirstName,
                u.LastName,
                u.Email,
                l.Password,
                PWQuestion1, PWAnswer1
            from
                users u
                join userlogin l using (UserID)
            where
            (
                (l.UseEmail = 1 and u.Email = '$login')
                or
                (l.UseEmail = 0 and l.Login = '$login')
            )";
        $result = $db->query($sql);
        if (!$result) {
            return null;
        }
        $user = $result->fetch_array(MYSQLI_ASSOC);

        if (empty($user)) {
            return $user;
        }

        // get the profiles for the user
        $sql = "select
                    profiles.Name as profile,
                    1
                from
                    userprofiles
                    join profiles using (ProfileID)
                where
                    userprofiles.UserID = {$user['UserID']}";

        $profiles = $db->query_fetch_all_key_value($sql);

        // if nothing else, the user is a customer
        if (empty($profiles)) {
            $profiles[self::PROFILE_CUSTOMER] = 1;
        }
        $user['profiles'] = $profiles;

        return $user;
    }

}

?>
