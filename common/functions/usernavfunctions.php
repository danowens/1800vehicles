<?php

// Functions that are used to determine user state, capabilities, etc.
//require_once(WEB_ROOT_PATH."globals.php");
// currentstep = see how far the user has progressed in the current market need...
// Return = step level of the active market need
function currentstep() {
    if (!isset($_SESSION['marketneedid']))
        return 0;

    $marketneedid = $_SESSION['marketneedid'];

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Look to see if we are in step 4 first...
        $query = "select purchaseid from specificvehicles s, purchases p where p.status='Bought' and s.specificvehicleid=p.specificvehicleid and s.marketneedid = " . $marketneedid;
        $result = mysql_query($query);
        if ($result && $row = mysql_fetch_array($result)) {
            mysql_close($con);
            return 4;
        }

        // Next check if we are in step 3...
        $query = "select f.firmquoteid from quoterequests q,firmquotes f where f.quoterequestid=q.quoterequestid and q.marketneedid = " . $marketneedid;
        $result = mysql_query($query);
        if ($result && $row = mysql_fetch_array($result)) {
            $oquery = "select orderid from orderfor where firmquoteid = " . $row[0];
            $oresult = mysql_query($oquery);
            if ($orow = mysql_fetch_array($oresult)) {
                mysql_close($con);
                return 3;
            }
            $wquery = "select watchid from watchfor where firmquoteid = " . $row[0];
            $wresult = mysql_query($wquery);
            if ($wrow = mysql_fetch_array($wresult)) {
                mysql_close($con);
                return 3;
            }
        }

        // Finally check if we are in step 2...
        $query = "select assessmentid from assessments where marketneedid = " . $marketneedid;
        $result = mysql_query($query);
        if ($result && $row = mysql_fetch_array($result)) {
            mysql_close($con);
            return 2;
        }

        $query = "select consultationid from consultations where marketneedid = " . $marketneedid;
        $result = mysql_query($query);
        if ($result && $row = mysql_fetch_array($result)) {
            mysql_close($con);
            return 2;
        }
        $query = "select quoterequestid from quoterequests where visible=1 and marketneedid = " . $marketneedid;
        $result = mysql_query($query);
        if ($result && $row = mysql_fetch_array($result)) {
            mysql_close($con);
            return 2;
        }

        mysql_close($con);
    }
    return 1;
}

// getuserstep = finds the current step for the customer
// $puserid = the ID of the user to check against
// $pmarketid = optional...if not specified it will use the latest market need ID for the user...
// Return = -1 if not found or the step of the market need as appropriate...
function getuserstep($puserid, $pmarketid = -1) {
    $curstep = -1;
    if (!isset($puserid))
        return $curstep;

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        if ($pmarketid == -1) {
            $query = "select max(marketneedid) 'marketneedid' from marketneeds where userid = " . $puserid;
            $result = mysql_query($query);
            if ($row = mysql_fetch_array($result)) {
                $pmarketid = $row['marketneedid'];
            } else {
                mysql_close($con);
                return $curstep;
            }
        }

        // Look to see if we are in step 4 first...
        $query = "select purchaseid from specificvehicles s, purchases p where p.status='Bought' and s.specificvehicleid=p.specificvehicleid and s.marketneedid = " . $pmarketid;
        $result = mysql_query($query);
        if ($row = mysql_fetch_array($result)) {
            $curstep = 4;
        }

        if ($curstep == -1) {
            // Next check if we are in step 3...
            $query = "select f.firmquoteid from quoterequests q,firmquotes f where f.quoterequestid=q.quoterequestid and q.marketneedid = " . $pmarketid;
            $result = mysql_query($query);
            while ($row = mysql_fetch_array($result)) {
                $oquery = "select orderid from orderfor where firmquoteid = " . $row[0];
                $oresult = mysql_query($oquery);
                if ($orow = mysql_fetch_array($oresult)) {
                    $curstep = 3;
                    break;
                }

                $wquery = "select watchid from watchfor where firmquoteid = " . $row[0];
                $wresult = mysql_query($wquery);
                if ($wrow = mysql_fetch_array($wresult)) {
                    $curstep = 3;
                    break;
                }
            }
        }

        if ($curstep == -1) {
            // Finally check if we are in step 2...
            $query = "select assessmentid from assessments where marketneedid = " . $pmarketid;
            $result = mysql_query($query);

            $chkquery = "select SearchPlanID from searchplans where MarketNeedID = " . $pmarketid;
            $planresult = mysql_query($chkquery, $con);


            if ($row = mysql_fetch_array($result)) {
                $curstep = 2;
            } else if ($row = mysql_fetch_array($planresult)) {
                $curstep = 2;
            } else {
                $query = "select consultationid from consultations where marketneedid = " . $pmarketid;
                $result = mysql_query($query);
                if ($row = mysql_fetch_array($result)) {
                    $curstep = 2;
                } else {
                    $query = "select quoterequestid from quoterequests where visible=1 and marketneedid = " . $pmarketid;
                    $result = mysql_query($query);
                    if ($row = mysql_fetch_array($result)) {
                        $curstep = 2;
                    }
                }
            }
        }

        if ($curstep == -1)
            $curstep = 1;

        mysql_close($con);
    }

    return $curstep;
}

// getuserprofile = see whether the user has access to the specified responsibility level
// $puserid = the ID of the user to check against
// $respcheck = the responsibility to check against
//        Possible Values:
//            Customer
//            Administrator
//            Teritory Admin
//            Franchisee
//            Operations Manager
//            General Manager
//            Researcher
//            Sales Representative
// Return = 'true' or 'false' as appropriate...
function getuserprofile($puserid, $respcheck) {
    $isadmin = 'false';
    if (!isset($puserid))
        return $isadmin;

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select profileid from userprofiles where userid = " . $puserid;

        $result = mysql_query($query);
        if ($result && $row = mysql_fetch_array($result)) {

            $pquery = "select name from profiles where profileid = " . $row['profileid'];
            $presult = mysql_query($pquery);
            if ($prow = mysql_fetch_array($presult)) {
                if ($prow['name'] == $respcheck) {
                    $isadmin = 'true';
                } else {
                    while ($row = mysql_fetch_array($result)) {
                        $pquery = "select name from profiles where profileid = " . $row['profileid'];
                        $presult = mysql_query($pquery);
                        if ($prow = mysql_fetch_array($presult)) {
                            if ($prow['name'] == $respcheck) {
                                $isadmin = 'true';
                            }
                        }
                    }
                }
            }
        } else {
            // They have NO Profile, so we can make them a customer at least...
            $iquery = "insert into userprofiles (userid, profileid) values (" . $puserid . ",1)";
            $iresult = mysql_query($iquery);
            if ($iresult && $irow = mysql_fetch_array($iresult)) {
                if ($respcheck == 'Customer')
                    $isadmin = 'true';
            }
        }

        mysql_close($con);
    }
    return $isadmin;
}

// ismorethancust = see whether the user has access to the non-customer dashboard functions
// $puserid = the ID of the user to check against
// Return = 'true' = has access or 'false'= does not as appropriate...
function ismorethancust($puserid) {
    if (!isset($puserid))
        return 'false';
    if (getuserprofile($puserid, 'Administrator') == 'true')
        return 'true';
    if (getuserprofile($puserid, 'Teritory Admin') == 'true')
        return 'true';
    if (getuserprofile($puserid, 'Franchisee') == 'true')
        return 'true';
    if (getuserprofile($puserid, 'Operations Manager') == 'true')
        return 'true';
    if (getuserprofile($puserid, 'General Manager') == 'true')
        return 'true';
    if (getuserprofile($puserid, 'Researcher') == 'true')
        return 'true';
    if (getuserprofile($puserid, 'Sales Representative') == 'true')
        return 'true';
    return 'false';
}

// getuserlevel = see whether the user has access to the home office pages or not
// $puserid = the ID of the user to check against
// Return = 'H'='Home Office' or 'F'='Franchise Only' as appropriate...
function getuserlevel($puserid) {
    $isadmin = 'F';
    if (!isset($puserid))
        return $isadmin;

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select profileid from userprofiles where userid = " . $puserid;
        $result = mysql_query($query);
        if ($row = mysql_fetch_array($result)) {
            $query = "select homeorfran from profiles where profileid = " . $row['profileid'];
            $result = mysql_query($query);
            if ($row = mysql_fetch_array($result)) {
                if ($row['homeorfran'] == 'H')
                    $isadmin = 'H';
            }
        }

        mysql_close($con);
    }

    return $isadmin;
}

// getsalesrep = finds the current sales rep (if any) for the user
// $puserid = the ID of the user to check against
// $pmarketid = optional...if not specified it will use the latest market need ID for the user...
// $pcon = mysql connection
// Return = -1 if not found or the UserID of the SalesRep as appropriate...
function getsalesrepnodb($pcon, $puserid, $pmarketid = -1) {
    $repid = -1;
    if (!isset($puserid))
        return $repid;

    if ($pmarketid == '' || $pmarketid == -1) {
        $query = "select max(marketneedid) from marketneeds where userid = " . $puserid;
        $result = mysql_query($query, $pcon);
        if ($row = mysql_fetch_array($result)) {
            $pmarketid = $row[0];
        }
    }

    if ($pmarketid != -1) {
        $curdate = date_at_timezone('Y-m-d H:i:s', 'EST');
        $query = "select userrepid from assignedreps where marketneedid = " . $pmarketid . " and startdate < '" . $curdate . "' and (enddate is null or enddate > '" . $curdate . "') order by startdate desc";
        $result = mysql_query($query, $pcon);
        if ($result) {
            if ($row = mysql_fetch_array($result))
                $repid = $row[0];
        }
    }

    return $repid;
}

// getsalesrep = finds the current sales rep (if any) for the user
// $puserid = the ID of the user to check against
// $pmarketid = optional...if not specified it will use the latest market need ID for the user...
// Return = -1 if not found or the UserID of the SalesRep as appropriate...
function getsalesrep($puserid, $pmarketid = -1) {
    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $repid = getsalesrepnodb($con, $puserid, $pmarketid);

        mysql_close($con);
    }
    return $repid;
}

// isvalid = checks if the user and market need match...
// $puserid = the ID of the user to check against
// $pmarketid = the market need ID to check...
// Return = 0 if not found or not a match or 1 if it is a match and valid...
function isvalid($puserid, $pmarketid) {
    $valid = 0;
    if (!isset($puserid))
        return $valid;
    if (!isset($pmarketid))
        return $valid;

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select * from marketneeds where userid = " . $puserid . " and marketneedid = " . $pmarketid;
        $result = mysql_query($query);
        if ($row = mysql_fetch_array($result))
            $valid = 1;

        mysql_close($con);
    }
    return $valid;
}

// getuserimage = finds the image for the user passed in
// $puserid = the ID of the user to check against
// Return = file location of the image file
function getuserimage($puserid) {
    if (!isset($puserid))
        return "userimages/nopicture.jpg";

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the current User Information...
        $query = 'select imagefile from users where userid = ' . $puserid;
        $result = mysql_query($query, $con);
        if ($row = mysql_fetch_array($result)) {
            if (strlen($row[0]) > 0) {
                mysql_close($con);
                return $row[0];
            }
        }

        mysql_close($con);
    }
    return "userimages/nopicture.jpg";
}

// getuserfirstname = finds the first name for the user passed in
// $puserid = the ID of the user to check against
// Return = FirstName from the Users table
function getuserfirstname($puserid) {
    if (!isset($puserid))
        return "Unknown";

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the current User Information...
        $query = 'select firstname from users where userid = ' . $puserid;
        $result = mysql_query($query, $con);
        if ($row = mysql_fetch_array($result)) {
            if (strlen($row[0]) > 0) {
                mysql_close($con);
                return $row[0];
            }
        }

        mysql_close($con);
    }
    return "Unknown";
}

// getuserlastname = finds the first name for the user passed in
// $puserid = the ID of the user to check against
// Return = FirstName from the Users table
function getuserlastname($puserid) {
    if (!isset($puserid))
        return "Unknown";

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the current User Information...
        $query = 'select lastname from users where userid = ' . $puserid;
        $result = mysql_query($query, $con);
        if ($row = mysql_fetch_array($result)) {
            if (strlen($row[0]) > 0) {
                mysql_close($con);
                return $row[0];
            }
        }

        mysql_close($con);
    }
    return "Unknown";
}

// getuserlastname = finds the first name for the user passed in
// $puserid = the ID of the user to check against
// Return = FirstName from the Users table
function getuserlastnamenodb($pcon, $puserid) {
    if (!isset($puserid))
        return "Unknown";

    // Get the current User Information...
    $query = 'select lastname from users where userid = ' . $puserid;
    $result = mysql_query($query, $pcon);
    if ($row = mysql_fetch_array($result))
        if (strlen($row[0]) > 0)
            return $row[0];
    return "Unknown";
}

// getuserfullname = finds the full name for the user passed in
// $puserid = the ID of the user to check against
// $lastfirst = 'true' or 'false' to determine name order
// Return = FirstName from the Users table
function getuserfullname($puserid, $lastfirst = 'true') {
    if (!isset($puserid))
        return "Unknown";

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        if ($lastfirst == 'true')
            $order = "concat(lastname,', ',firstname) AS fullname";
        else
            $order = "concat(firstname,' ',lastname) AS fullname";

        // Get the current User Information...
        $query = 'select ' . $order . ' from users where userid = ' . $puserid;
        $result = mysql_query($query, $con);
        if ($row = mysql_fetch_array($result)) {
            if (strlen($row[0]) > 0) {
                mysql_close($con);
                return $row[0];
            }
        }

        mysql_close($con);
    }
    return "Unknown";
}

// getuserfullnamenodb = finds the full name for the user passed in with already opened connection
// $puserid = the ID of the user to check against
// $lastfirst = 'true' or 'false' to determine name order
// Return = FirstName from the Users table
function getuserfullnamenodb($pcon, $puserid, $lastfirst = 'true') {
    if (!isset($puserid))
        return "Unknown";

    if ($lastfirst == 'true')
        $order = "concat(lastname,', ',firstname) AS fullname";
    else
        $order = "concat(firstname,' ',lastname) AS fullname";

    // Get the current User Information...
    $query = 'select ' . $order . ' from users where userid = ' . $puserid;
    $result = mysql_query($query, $pcon);
    if ($row = mysql_fetch_array($result))
        if (strlen($row[0]) > 0)
            return $row[0];
    return "Unknown";
}

// getuserphone = finds the phone number for the user passed in
// $puserid = the ID of the user to check against
// $type = The Phone Number to retrieve if available
//        Can Be: Home, Cell, Work, Fax, Spouse, Other
// Return = Empty String or Phone Number like "(XXX) XXX-XXXX x XXXX"
function getuserphone($puserid, $type = 'Cell') {
    if (!isset($puserid))
        return "";

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = 'select n.phonenumber, n.extension, n.addinfo from usernumbers n, numbertypes t where t.numbertypeid=n.numbertypeid and n.userid=' . $puserid . ' and t.typename="' . $type . '"';
        $result = mysql_query($query, $con);
        if ($row = mysql_fetch_array($result)) {
            $number = '(' . substr($row[0], 0, 3) . ') ' . substr($row[0], 3, 3) . '-' . substr($row[0], 6, 4);
            if (!is_null($row[1]))
                $number .= ' x ' . $row[1];
            if (!is_null($row[2]))
                $number .= ' [' . $row[2] . ']';

            mysql_close($con);
            return $number;
        }

        mysql_close($con);
    }
    return "";
}

// getuserphonenodb = finds the phone number for the user passed in with already opened connection
// $puserid = the ID of the user to check against
// $type = The Phone Number to retrieve if available
//        Can Be: Home, Cell, Work, Fax, Spouse, Other
// Return = Empty String or Phone Number like "(XXX) XXX-XXXX x XXXX"
function getuserphonenodb($pcon, $puserid, $type = 'Cell') {
    if (!isset($puserid))
        return "";

    $query = 'select n.phonenumber, n.extension, n.addinfo from usernumbers n, numbertypes t where t.numbertypeid=n.numbertypeid and n.userid=' . $puserid . ' and t.typename="' . $type . '"';
    $result = mysql_query($query, $pcon);
    if ($row = mysql_fetch_array($result)) {
        $number = '(' . substr($row[0], 0, 3) . ') ' . substr($row[0], 3, 3) . '-' . substr($row[0], 6, 4);
        if (!is_null($row[1]))
            $number .= ' x ' . $row[1];
        if (!is_null($row[2]))
            $number .= ' [' . $row[2] . ']';
        return $number;
    }
    return "";
}

// getuseremail = finds the email address for the user passed in
// $puserid = the ID of the user to check against
// Return = Email from the Users table
function getuseremail($puserid) {
    if (!isset($puserid))
        return "Unknown";

    $emailcon = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($emailcon) {
        mysql_select_db(DB_SERVER_DATABASE, $emailcon);

        // Get the current User Information...
        $emailquery = 'select email from users where userid = ' . $puserid;
        $emailresult = mysql_query($emailquery, $emailcon);
        if ($emailrow = mysql_fetch_array($emailresult)) {
            if (strlen($emailrow[0]) > 0) {
                mysql_close($emailcon);
                return $emailrow[0];
            }
        }

        mysql_close($emailcon);
    }
    return "Unknown";
}

// getuseremailnodb = finds the email address for the user passed in with already opened connection
// $puserid = the ID of the user to check against
// Return = Email Address from the Users table
function getuseremailnodb($pcon, $puserid) {
    if (!isset($puserid))
        return "Unknown";

    // Get the current User Information...
    $emailquery = 'select email from users where userid = ' . $puserid;
    $emailresult = mysql_query($emailquery, $pcon);
    if ($emailrow = mysql_fetch_array($emailresult))
        if (strlen($emailrow[0]) > 0)
            return $emailrow[0];
    return "Unknown";
}

?>
