<?php

// Functions related to the price of vehicles

require_once(WEB_ROOT_PATH."globals.php");

/**
 * Set the price for a vehicle based on the blackbook price.
 *
 * @param $con         The DB connection to use
 * @param $vehicle_id  The primary key on the vehicles table.
 * @param $bb_price    The black book price
 * @param $hybrid      hubrid status
 * @param $size    	   size status
 
 Read more: http://www.lyricsbell.com/22da-zora-randhawa-fateh/#ixzz3uwA44VQB
 * @return error string on error, false otherwise.
 */
function setVehiclePrice($con, $vehicle_id, $bb_price, $hybrid, $size) {

   list($lowpricestart,  $lowpriceend, $highpricestart, $highpriceend) = getPrices($bb_price);
   
	
    //echo "bb_price $bb_price => lmarkup $lmarkup hmarkup $hmarkup<br />\n";
    //echo "lowpricestart $lowpricestart lowpriceend $lowpriceend<br />\n";
    //echo "highpricestart $highpricestart highpriceend $highpriceend<br />\n";

    $sql = "update vehicles set ";
    $sql .= "BlackBookAvg = ".$bb_price;
	$sql .= " ,";
	$sql .= " Hybrid = "."'$hybrid'";
	$sql .= " ,";
	$sql .= " Size = "."'$size'";
    $sql .= " where VehicleID = ".$vehicle_id;

	
	$result = mysql_query($sql);
	
    if(!$result) {
        return 'Error Updating the Vehicle! '.$sequery;
    } else {
        $sql = "update vehicledata set ";
        $sql .= "PriceStart = ".$lowpricestart.",";
        $sql .= "PriceEnd = ".$lowpriceend;
        $sql .= " where VehicleID = $vehicle_id";
        $sql .= "   and LowMiles  = 1";
		
		$result = mysql_query($sql);

        //echo "<pre>$sql</pre><br />\n";
        if(!$result) {
            return 'Error Updating the Vehicle Low Miles Data! '.$sql;
        } else {
            $sql = "update vehicledata set ";
            $sql .= "PriceStart = ".$highpricestart.",";
            $sql .= "PriceEnd = ".$highpriceend;
            $sql .= " where VehicleID = $vehicle_id";
            $sql .= "   and LowMiles  = 0";
            //echo "<pre>$sql</pre><br />\n";
			$result = mysql_query($sql);

            if(!$result) {
                return 'Error Updating the Vehicle High Miles Data! '.$sql;
            }
        }
    }

    return false;
}

function setVehicleBestBuy($con, $vehicle_id, $lowbest,$highbest) {

    //list($lowpricestart,  $lowpriceend, $highpricestart, $highpriceend) = getPrices($bb_price);

    //echo "bb_price $bb_price => lmarkup $lmarkup hmarkup $hmarkup<br />\n";
    //echo "lowpricestart $lowpricestart lowpriceend $lowpriceend<br />\n";
    //echo "highpricestart $highpricestart highpriceend $highpriceend<br />\n";
	
	if(empty($highbest)){
	$highbest='0';}else{
	$highbest=$highbest;
	}
	if(empty($lowbest)){
	$lowbest='0';}else{
	$lowbest=$lowbest;
	}
        $sql = "update vehicledata set ";
        $sql .= "BestBuy = ".$lowbest;
        $sql .= " where VehicleID = $vehicle_id";
        $sql .= "   and LowMiles  = 1";
		
      
        if(!$con->query($sql)) {
            return 'Error Updating the Vehicle Low Miles Data! '.$sql;
        } else {
            $sql = "update vehicledata set ";
            $sql .= "BestBuy = ".$highbest;
            $sql .= " where VehicleID = $vehicle_id";
            $sql .= "   and LowMiles  = 0";
           

            if(!$con->query($sql)) {
                return 'Error Updating the Vehicle High Miles Data! '.$sql;
            }
        }
  

    return false;
}

/**
 * Get the low and high price bounds based on the Black Book Price.
 */
function getPrices($bb_price) {
    list($lmarkup, $hmarkup)             = getMarkup($bb_price);
    list($lowpricestart,  $lowpriceend)  = getPriceBound($bb_price, $lmarkup);
    list($highpricestart, $highpriceend) = getPriceBound($bb_price, $hmarkup);
    return array($lowpricestart,  $lowpriceend, $highpricestart, $highpriceend);
}


/**
 * Given a black book price, return the high and low markup.
 */
function getMarkup($bb_price) {
    // calculate markup
    if ($bb_price < 20000) {
        $hmarkup = 3000;
        $lmarkup = 4500;
    } else if ($bb_price < 40000) {
        $hmarkup = 3500;
        $lmarkup = 5000;
    } else {
        $hmarkup = 3500;
        $lmarkup = 5500;
    }
    return array($lmarkup, $hmarkup);
}


/**
 * Given a black book price, calculate a price range
 */
function getPriceBound($bb_price, $markup) {
    // calculate price targets
    $target = $markup + $bb_price;

    // snap to ranges and intervals
    if ($target < 5000)
    {
        // 500 -> 0 .. 5,000
        $pricestart = 0;
        $interval = 5000;
    } else if ($target < 10000) {
        // 6,001 -> 6,000
        // 6,999 -> 6,000
        $pricestart = floor($target / 1000) * 1000;
        $interval = 1000;
    } else if ($target < 40000) {
        // 10,500 -> 10,000
        // 11,999 -> 10,000
        // 30,500 -> 30,000
        // 34,999 -> 34,000
        $pricestart = floor($target / 2000) * 2000;
        $interval = 2000;
    } else {
        // 50,500 -> 50,000
        // 54,999 -> 50,000
        $pricestart = floor($target / 5000) * 5000;
        $interval = 5000;
    }

    $priceend = $pricestart + $interval;
    return array($pricestart, $priceend);
}


?>
