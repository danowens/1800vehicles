<?php
    // Functions that are used to send emails...

    require_once(WEB_ROOT_PATH."globals.php");

    // sendmail = send an email to someone from the system
    // $to = string of email address to send to, like 'bob@test.com'
    // $subject = string for the email subject line
    // $body = text of the message which can contain HTML codes
    // $important = 'true' if the message should be flagged as high priority
    // Return = return of the built in mail() function
    function sendemail($to, $subject, $body, $important = 'false', $from = PRIMARY_EMAIL_ADDR, $smtpadd = EMAIL_SMTP_ADDR, $smtpport = EMAIL_SMTP_PORT) {
        
		if($_SESSION['disableemail'] == 'true') return TRUE;

        ini_set('SMTP', $smtpadd);
        ini_set('smtp_port',$smtpport);
        ini_set('sendmail_from', $from);
        //$headers .= "From: ".$from."\r\n";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "X-Mailer: PHP\r\n";
        $headers .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n";
        //$headers .= "Message-ID: <".microtime().">";
        if($important == "true")
        {
            $headers .= "X-Priority: 1 (Highest)\r\n";
            $headers .= "X-MSMail-Priority: High\r\n";
            $headers .= "Importance: High\r\n";
        }

        if($_SESSION['systemname'] != 'LIVE')
        {
            // Prepend a message to the beginning to let them know this was not a live site email...
            $body = '
                This email came from the '.$_SESSION['systemname'].' system, not the LIVE site.<br/>
                Please ignore this message as it was generated as a demonstration.<br/>
                <br/>
                <strong>Message:</strong><br/>
            '.$body;
        }

        return mail($to, $subject, $body, $headers);
    }

    // Get list of Admin emails and send them the message passed in
    function sendtoadmins($con, $body, $subject = '1800vehicles.com Admin Alert', $important = 'false')
    {
        $admquery = "select email from users where userid in (select userid from userprofiles where profileid = 2)";
        $admresult = mysql_query($admquery, $con);
        $admindex = 0;
        while($admrow = mysql_fetch_array($admresult))
        {
            $emails[$admindex] = $admrow[0];
            $admindex++;
        }
        if(isset($emails))
        {
            $emaillist = implode(",", $emails);
            return sendemail($emaillist, $subject, $body, $important, SYSTEM_EMAIL_ADDR);
        }
        else return FALSE;
    }

    // Creates the connection if needed before sending to the admins...
    function sendtoadminsnodb($pbody, $psubject = '1800vehicles.com Admin Alert', $pimportant = 'false')
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);
            sendtoadminsnodb($con, $pbody, $psubject, $pimportant);
            mysql_close($con);
        }
    }

    // Send the welcome message to the email address provided...
    function sendwelcome($con, $pemail, $plogin, $pphone, $pusername, $pzipcode)
    {
        $message = 'Thank you for registering with <a href="'.WEB_SERVER_NAME.'index.php">1-800-vehicles.com</a>!<br/><br/>';
        $message .= 'For your records, the login you entered was: ';
        $message .= $plogin;
        $message .= '<br/<br/><a href="'.WEB_SERVER_NAME.'index.php">1-800-vehicles.com</a> may contact you to see if you would like free consulting.<br/><br/>';
        $message .= 'To be sure you get our emails, ';
        $message .= 'you should add <a href="mailto://vehicles@1800vehicles.com">vehicles@1800vehicles.com</a> and ';
        $message .= '<a href="mailto://sales@1800vehicles.com">sales@1800vehicles.com</a> to the whitelist in your email system.<br/>';
        $message .= 'This will prevent our messages to you from being blocked by spam filters.<br/><br/>';
        $message .= "To change any of your personal information, log in and click on 'My Account'.<br/><br/>";
        $message .= "If you forget your password follow the 'Forgot Password' link in the Login Area to set a new one.";
        if(sendemail($pemail, 'Thank you for registering!', $message, 'false'))
        {
            $emailbody = 'A new user has registered: '.$pusername.'<br/>';
            if($plogin != $pemail) $emailbody .= 'Primary Login: '.$plogin.'<br/>';
            $emailbody .= 'Primary Email: '.$pemail.'<br/>Primary Phone: '.$pphone.'<br/>Zip Code: '.$pzipcode.'<br/>';
            return sendtoadmins($con, $emailbody, '1800vehicles.com New User');
        }
        else return FALSE;
    }

    // Post a Message to a User Dashboard...
    function posttodashboard($pcon, $userfromID, $usertoID, $text, $marketneedID = -1, $sendon = '0000-00-00 00:00:00', $sendemail = 'false')
    {
        $pamquery = "insert into messages (created, message, userfromid, usertoid, sendon";
        if($marketneedID != -1) $pamquery .= ", marketneedid";
        $pamquery .= ") values ('".date_at_timezone('Y-m-d H:i:s', 'EST')."','".escapestr($text)."',".$userfromID.",".$usertoID.",'".$sendon."'";
        if($marketneedID != -1) $pamquery .= ", ".$marketneedID;
        $pamquery .= ")";
        if(mysql_query($pamquery, $pcon))
        {
            if($sendemail != 'false')
            {
                $emquery = "select email from users where userid = ".$usertoID;
                $emresult = mysql_query($emquery, $con);
                if($emrow = mysql_fetch_array($emresult))
                {
                    return sendemail($emrow[0], '1800vehicles.com Update Message', $text, 'false', SYSTEM_EMAIL_ADDR);
                }
                else return FALSE;
            }
            else return TRUE;
        }
        return FALSE;
    }

    // Creates the connection if needed before posting the message...
    function posttodashboardnodb($puserfromID, $pusertoID, $ptext, $pmarketneedID = -1, $psendon = '0000-00-00 00:00:00', $psendemail = 'false')
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);
            posttodashboard($con, $puserfromID, $pusertoID, $ptext, $pmarketneedID, $psendon, $psendemail);
            mysql_close($con);
        }
    }
	
	//Sends email to customer in support section
	function send_email_support( $from, $name, $subject, $body, $to = SECONDARY_EMAIL_ADDR, $smtp_add = EMAIL_SMTP_ADDR, $smtp_port = EMAIL_SMTP_PORT, $smtp_host = EMAIL_SMTP_HOST, $smtp_pass = EMAIL_SMTP_PASS, $smp_sec_add = SECONDARY_EMAIL_ADDR ) {
		
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		//Tell PHPMailer to use SMTP
		$mail->isSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;
		//Ask for HTML-friendly debug output
		//$mail->Debugoutput = 'html';
		//Set the hostname of the mail server
		$mail->Host = $smtp_host;
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = $smtp_port;
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $smtp_add;
		//Password to use for SMTP authentication
		$mail->Password = $smtp_pass;
		//Set who the message is to be sent from
		$mail->setFrom($from, $name);
		//Set an alternative reply-to address
		$mail->addReplyTo($from, $name);
		//Set who the message is to be sent to
		$mail->addAddress($to, 'Ruben Lupian');
		//Set the subject line
		$mail->Subject = $subject;
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($body, dirname(__FILE__));
		//Replace the plain text body with one created manually
		$mail->AltBody = "";
		//Attach an image file
		//$mail->addAttachment('images/phpmailer_mini.png');

		//send the message, check for errors
		if (!$mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "Message has been sent successfully!";
		}				
	}
?>
