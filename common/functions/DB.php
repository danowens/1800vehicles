<?php

/**
 * Start on a singleton MySQLi wrapper.
 */
class DB {
    static private $dbs = array();

    private $conn;
    private $connected = false;

    public static function init($config="default") {
        if (isset($dbs[$config])) {
            return $dbs[$config];
        }
        //$con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        //mysql_select_db(DB_SERVER_DATABASE, $con);
        $db = new DB($config);
        $dbs[$config] = $db;
        return $db;
    }

    public function __contruct($config) {
        if ($config != "default" ) {
            throw new Exception("Only know one DB right now!");
            return;
        }
        $this->connected = false;
    }

    /**
     * Connect to the database (if not already). In general, you shouldn't need to call
     * this method directly.
     *
     * @param $force bool Optional, if set to true re-connects even if already connected.
     * @return bool true if connected, false otherwise.
     */
    public function connect($force=false) {
        if ($this->connected && !$force) {
            return true;
        }
        $this->conn = mysqli_init();

        $this->conn->real_connect(
            DB_SERVER_HOSTNAME,
            DB_SERVER_USERNAME,
            DB_SERVER_PASSWORD,
            DB_SERVER_DATABASE
        );
        if(!$this->conn || $this->conn->connect_errno){
            //if ($this->conn) {
            //    $this->conn->connect_error);
            //}
            throw new Exception("Could not connect to DB!");
            return false;
        }
        $this->connected = true;
        return true;
    }

    /**
     * Magic wrapper to call methods on the underlying MySQLi object.
     */
    public function __call($method, $args) {
        if (!$this->connected) {
            $this->connect();
        }
        if(method_exists($this->conn, $method)){
            return call_user_func_array(array($this->conn, $method), $args);
        } else {
            throw new Exception ("Invalid method $method for class ".__CLASS__);
        }
    }

    /**
     * Magic wrapper to get variables from the underlying MySQLi object.
     */
    public function __get($var_name) {
        if (!$this->connected) {
            $this->connect();
        }
        $val = $this->conn->$var_name;
        if ($val !== null) {
            return $this->conn->$var_name;
        }
        throw new Exception("Invalid variable $var_name for DB class");
    }

    /**
     * Wrapper to fetch all of the data for a query as a numeric array whose
     * elements are the rows.
     *
     * @param $sql string the SQL query to execute
     * @param $type int   Optional. The format of each row (defaults to associative arrays).
     */
    public function query_fetch_all($sql, $type=MYSQLI_ASSOC) {
        $res = $this->query($sql, MYSQLI_USE_RESULT);
        if (!$res) {
            return array();
        }
        $ret = array();
        while ($rec = $res->fetch_array($type)) {
            $ret[] = $rec;
        }
        return $ret;
    }

    /**
     * Helper function to fetch all results in one call and return an associative
     * array keyed by the first column with values of the second column.
     *
     * For example:
     *    $a = $db->query_fetch_all_key_value("select key, value from table");
     *
     * @param   string  $sql    The query to run
     * @return  array
     */
    public function query_fetch_all_key_value($sql) {

        $ret = array();
        $res = $this->query($sql, MYSQLI_USE_RESULT);

        if ($res) {

            $rec = $res->fetch_array(MYSQLI_NUM);
            if ($rec) {

                if(count($rec) != 2){
                    throw new Exception ("Expected exactly two fields in result set.");
                }

                do {
                    $ret[$rec[0]] = $rec[1];
                } while($rec = $res->fetch_array(MYSQLI_NUM));

            }

            $res->free_result();
        }

        return $ret;
    }

    /**
     * Get a "column = value" string for the given set of name value pairs.
     */
    public function getSetFields($values) {
        $arr = array();
        foreach ($values as $column => $value) {
            $backtick_column = "`" . $column . "`";
            $arr[] = "$backtick_column = '" . $this->real_escape_string($value) . "'";
        }
        return implode(",", $arr);
    }
}

$db = new DB();