<?php

    /**
     * Pluralizes a noun if necessary
     *
     * @param   int     $num            The number of X items.
     * @param   string  $word           The word for the item
     * @param   string  $plural_suffix  The suffix if plural
     * @return  string
     *
     * @test
     * echo String::pluralize_noun(4,'bus','es') . "\n";
     * echo String::pluralize_noun(1,'bus','es') . "\n";
     * echo String::pluralize_noun(3,'cat','s') . "\n";
     * echo String::pluralize_noun(1,'stud','ies', 'y') . "\n";
     * echo String::pluralize_noun(3,'stud','ies', 'y') . "\n";
     * @expects
     * buses
     * bus
     * cats
     * study
     * studies
     * @end
     */
    function pluralize_noun($num, $word, $plural_suffix, $singular_suffix="") {
        return ($num == 1) ? $word . $singular_suffix : $word . $plural_suffix;
    }

?>
