<style>
    nav {
    display: none;
}
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border-top: none;
}

#loginpanel tr td {
    border: 0px;
}
</style>



        <form method="post" action="login.php">
       
            <h1>Login</h1>
            <?php if($_SESSION['loginfailed']) { ?>
                <fieldset><div id='success_page'><p>Your login details are incorrect. Please check the username and password.</p></div></fieldset>
            <?php } ?>
                <table class="table borderless" id="loginpanel">                                
                
               
                <tr>
                    <td >
                        <label for="zip" >Username <span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <input type="text" name="user" id="username" class="form-control" size='100' value="" maxlength="50" required="" data-validation-required-message="Please enter username."/>
                    </td>                                                
                </tr>

                <tr>
                    <td>
                        <label for="email" >Password <span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <input type="password" name="pass" class="form-control"  id="password" value="" maxlength="50" required="" data-validation-required-message="Please enter password." />
                        
                    </td>                                                
                </tr>
                <tr>
                    <td align="center">

                        <span class="gridfour_reg">
                            <input type="submit" name="login" id="logint" value="Login" class="btn btn-primary">
                        </span>
                    </td>                                                
                </tr>

            </table>
        </form>
   