<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 13;
    $_SESSION['titleadd'] = 'Request a Current Market Study';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
         <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Request A Current Market Study</h1>
        <div class="grideightgrey">
            <div class="grideight">
                <p>Can't wait for a Current Market Study? Go to &quot;<a href="researchspecific.php">Research Specific Vehicles</a>&quot; for quick estimates.</p>
                <p>
                    Every effort will be made to complete your quote request within one business day. However, busy times can affect our ability to
                    reach that goal. Please call our sales department at 1-800-vehicles (834-4253) if you have not received your quote in a
                    timely fashion.
                </p>
                <ul>
                    <li style="color: #000000;">Requesting a Current Market Study does not obligate you in any way.</li>
                    <li style="color: #000000;">Current Market Study are good for 30 days.</li>
                </ul>
                <p>
                    PLEASE LIMIT YOU CURRENT MARKET STUDY REQUESTS TO A MAXIMUM OF THREE (3) AT A TIME.  This will allow your representative to be most accurate
                    in the availability estimates and price quotations.
                </p>
                <br/>
            </div>
        </div>
        <div class="grideightgrey">
            <p class="blackfourteen">
                For 1-6 year old autos, SUVs, and minivans
                <form action="quote.php" method="post">
                    <input type="hidden" value="Standard" name="QuoteType" />
                    <button type="submit" value="" class="med"><nobr>REQUEST A CURRENT MARKET STUDY</nobr></button>
                </form>
            </p>
            <br/>
            <p class="blackfourteen">
                For special requests or exotic cars
                <form action="quote.php" method="post">
                    <input type="hidden" value="Special" name="QuoteType" />
                    <button type="submit" value="" class="med"><nobr>REQUEST A SPECIAL CURRENT MARKET STUDY</nobr></button>
                </form>
            </p>
            <br/>
            <p class="blackfourteen">
                For pick-up trucks
                <form action="quote.php" method="post">
                    <input type="hidden" value="Pickup" name="QuoteType" />
                    <button type="submit" value="" class="med"><nobr>REQUEST A PICK-UP CURRENT MARKET STUDY</nobr></button>
                </form>
            </p>
        </div>
    </div><!-- grid eight container -->
<?php
    $_SESSION['hideteaser1'] = 'true';
    $_SESSION['hideteaser2'] = 'true';
    $_SESSION['hideteaser4'] = 'true';
    require("teaser.php");
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
