<?php require("globals.php"); ?>
<?php
    require_once(WEB_ROOT_PATH.'common/functions/string.php');
    $_SESSION['state'] = 2;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = 'Place an Order';

    $loaderror = 'false';
    if(!isset($_POST['FirmQuoteID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000204';
        header('Location: placeorder.php');
        exit();
    }

    $firmquoteids = implode(",", $_POST['FirmQuoteID']);

    $marketneedid = $_SESSION['marketneedid'];
    $srep = getsalesrep($userid, $marketneedid);

    $saveerror = 'false';
    $longestwait = -1;
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select q.QuoteRequestID, q.Year, q.Make, q.Model, q.Style, f.LastUpdated, q.QuoteType, f.OrderType, f.PricePoint, f.FirmQuoteID, f.WaitPeriod from quoterequests q,firmquotes f where f.QuoteRequestID=q.QuoteRequestID and f.FirmQuoteID in (".$firmquoteids.")";
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $fquoteid[$index] = $row[0];
            $fyear[$index] = $row[1];
            $fmake[$index] = $row[2];
            $fmodel[$index] = $row[3];
            $fstyle[$index] = $row[4];
            $fupdated[$index] = $row[5];
            $fquotetype[$index] = $row[6];
            $fordertype[$index] = $row[7];
            $fprice[$index] = $row[8];
            $ffirmid[$index] = $row[9];
            $fwaitperiod[$index] = $row[10];
            if($fwaitperiod[$index] > $longestwait) $longestwait = $fwaitperiod[$index];

            $index++;
        }

        $maxwait = 0;
        $count = count($fquoteid);
        for($i=0;$i<$count;$i++)
        {
            if($fwaitperiod[$i] > $maxwait) $maxwait = $fwaitperiod[$i];
        }

        $numquotes = count($fquoteid);
        if($numquotes > 0)
        {
            // Make sure there is not already an order in for this marketneed...
            $query = "select o.Accepted from quoterequests q,firmquotes f,orderfor of,orders o where o.OrderID=of.OrderID and of.FirmQuoteID=f.FirmQuoteID and f.QuoteRequestID=q.QuoteRequestID and q.MarketNeedID=".$marketneedid;
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                $saveerror = 'Order already placed on '.date_at_timezone('m/d/Y H:i:s T', 'EST', $row[0]);
            }
            else
            {
                //$query = "select DepositRequired,DepositAmount from marketneeds where MarketNeedID=".$marketneedid;
                //$result = mysql_query($query);
                //if($result && $row = mysql_fetch_array($result))
                //{
                //    if($row[0] == 1) $depamt = $row[1];
                //    else $depamt = 0;
                //}
                //else $depamt = 250;

                $query = "insert into orders (WaitPeriod,Accepted) values (".$maxwait.",'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(mysql_query($query, $con))
                {
                    $orderid = mysql_insert_id($con);

                    $query = "select Accepted from orders where OrderID=".$orderid;
                    $result = mysql_query($query);
                    if(!$result || !$row = mysql_fetch_array($result)) $saveerror = 'Could not create the order.';
                    else $orderdate = $row[0];

                    $vordered = '';

                    for($i=0;(($i<$numquotes) && ($saveerror == 'false'));$i++)
                    {
                        if($vordered != '') $vordered .= ' or ';
                        $vordered .= $fyear[$i].' '.$fmake[$i].' '.$fmodel[$i].' '.$fstyle[$i];

                        $query = "insert into orderfor (OrderID,FirmQuoteID) values (".$orderid.",".$ffirmid[$i].")";
                        if(!mysql_query($query, $con)) $saveerror = 'Could not create the order.';
                    }

                    if($saveerror == 'false')
                    {
                        // Add a Message Update when this happens...
                        posttodashboard($con, $userid, $userid, 'placed an <a href="'.WEB_SERVER_NAME.'orderdetails.php">Order</a> for a '.$vordered.'.',$marketneedid);

                        if($srep != -1)
                        {
                            // Add a Message Update when this happens...
                            posttodashboard($con, $userid, $srep, '<a href="'.WEB_SERVER_NAME.'salesrepactions.php?ForUserID='.$userid.'&MarketNeedID='.$marketneedid.'">'.$firstname.' '.$lastname.'</a> placed an Order for a '.$vordered.'.');

                            $message = 'Your customer has placed an order...</br>';
                            $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $userid, 'false').'</br>';
                            $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $userid).'</br>';
                            $message .= '&nbsp;&nbsp;Vehicle: &nbsp;'.$vordered.'</br>';
                            sendemail(getuseremailnodb($con, $srep), 'Customer Ordered!', $message, 'true');
                        }

                        $message = 'Your order has been received for the '.$vordered.'.</br>Thank you for ordering with <a href="http://www.1800vehicles.com">1800vehicles.com</a>, your sales representative will provide Specific Vehicle Quotes for you to review soon.';
                        sendemail(getuseremailnodb($con, $userid), 'Order Received', $message, 'false');

                        $message = 'A customer has placed an order...</br>';
                        $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $userid, 'false').'</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $userid).'</br>';
                        $message .= '&nbsp;&nbsp;Vehicle: &nbsp;'.$vordered.'</br>';
                        sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                    }
                }
                else $saveerror = 'Could not create the order.';
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
<?php
    if($saveerror != 'false')
    {
        echo '<h1 class="subhead" style="width: 300px;">Error on Order Creation</h1>';
        echo '<div class="grideightgrey">';
        echo '<p class="blackfourteen"><strong>There was an issue creating the order:</strong></p>';
        echo '<p class="blackfourteen">'.$saveerror.'</p>';
        echo '</div><!-- end grideightgrey-->';
    }
    else
    {
        $count = count($fquoteid);
        echo '<div class="grideightgrey">';
        echo '<center><p style="color:#216dce; font-size:15px; font-weight:bold; width:500px; margin: 10px auto;">Thank You for your order! Your vehicle was ordered on ';
        echo date_at_timezone('m/d/Y H:i:s T', 'EST', $orderdate).'.';
        echo '</p></center></div><!--end grideightgrey -->  ';
        echo '<h1 class="subhead">Order Placed</h1>';
        echo '<div class="grideightgrey">';
        echo '<table border="0" cellpadding="5" cellspacing="0" width="600">';
        echo '<tbody><tr>';
        //echo '<td align="center" width="50"><h3 class="greensub">No.</h3></td>';
            $plural = pluralize_noun($count, "CURRENT MARKET STUD", "IES", "Y");
        echo '<td width="250"><h3 class="greensub">'.$plural.'</h3></td>';
        echo '<td align="center" width="100"><h3 class="greensub">STATUS</h3></td>';
        echo '<td align="center" width="100"><h3 class="greensub">AVAILABILITY</h3></td>';
        echo '<td align="center" width="100"><h3 class="greensub">ESP</h3></td>';
        echo '<td align="center" width="100"><h3 class="greensub">PRICE</h3></td>';
        echo '<td width="95"><h3 class="greensub">&nbsp;</h3></td>';
        echo '</tr>';
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            //echo '<td align="center">';
            //echo '<label for="select3">';
            //echo '</label><p class="greyeleven">';
            //if(isset($fquoteid[$i])) echo str_pad($fquoteid[$i],5,"0",STR_PAD_LEFT);
            //echo '</p><br />';
            //echo '</td>';
            echo '<td><p class="formbluetext">';
            echo '<a href="quotereceived.php?QuoteID='.$fquoteid[$i].'">';
            echo $fyear[$i].' '.$fmake[$i].' '.$fmodel[$i].' '.$fstyle[$i];
            echo '</a>';
            echo '</p>';
            echo '<p class="greyeleven">';
            echo date_at_timezone('m/d/Y', 'EST', $fupdated[$i]);
            echo '</p></td>';
            echo '<td align="center"><p class="greyeleven">Received</p></td>';
            echo '<td align="center"><p class="greyeleven">';
            echo $fordertype[$i];
            echo '</p></td>';
            echo '<td align="center"><p class="greyeleven">'.$longestwait.' Days</p></td>';
            echo '<td align="center"><p class="greyeleven">$'.number_format($fprice[$i]).'</p></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';
        echo '</div><!-- end grideightgrey-->';
?>
</div><!-- end grid eight container -->
<?php
    if($fordertype[0] == 'Standard') echo '<h2 class="subhead" style="width: 235px;">Standard Order Agreement</h2>';
    else echo '<h2 class="subhead" style="width: 235px;">Special Order Agreement</h2>';
?>
<div class="grideight" style="width: 615px; margin-left: 20px;">
<?php
    if($fordertype[0] == 'Standard') $aname = 'Standard Order';
    else $aname = 'Special Order';
    $pname = 'Specific Vehicle Purchase';

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select AgreementID from agreements where AgreementName='".$aname."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $aid = $row[0];

            $query = "select SectionName, SectionText from agreementsections where AgreementID=".$aid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $secname[$index] = $row[0];
                $sectext[$index] = $row[1];
                $index++;
            }
        }

        $query = "select AgreementID from agreements where AgreementName='".$pname."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $pid = $row[0];

            $query = "select SectionName, SectionText from agreementsections where AgreementID=".$pid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $psecname[$index] = $row[0];
                $psectext[$index] = $row[1];
                $index++;
            }
        }

        mysql_close($con);
    }
    $count = count($secname);
    ini_set('display_errors','on');
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($sectext[$index]);
        //echo '<p class="blackfourteen"><strong>'.strtoupper($secname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen"><strong>'.strtoupper($secname[$index]).'</strong> - '.$sectext[$index].'</p>';
    }

    echo '</div><!--end grideight-->';
    echo '<h2 class="subhead" style="width: 335px;">Specific Vehicle Purchase Agreement</h2>';
    echo '<div class="grideight" style="padding:5px;">';

    $count = count($psecname);
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($psectext[$index]);
        //echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($psecname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($psecname[$index]).'</strong> - '.$psectext[$index].'</p>';
    }
?>
<p class="blackfourteen" style="color:#85c11b;"><strong>THANK YOU FOR GIVING US AN OPPORTUNITY TO SERVE YOU! </strong></p>
</div><!-- end grideight-->
<br clear="all" />
<?php
    }
?>
</div>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
