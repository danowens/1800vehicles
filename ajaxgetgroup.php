<?php require("globals.php"); ?>
<?php
    $rettext = '';

    $_SESSION['LastGroup'] = $_REQUEST['Group'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
        $query .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
        $query .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_REQUEST['Group'];
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $rettext = implode("#", array_values($row));

            $vquery = 'select count(*)';
            $vquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
            $vquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
            if(!is_null($row[2])) $vquery .= ' and v.Visible = '.$row[2];
            if(!is_null($row[3])) $vquery .= ' and v.MakeID = '.$row[3];
            if(!is_null($row[4])) $vquery .= " and v.Year = '".$row[4]."'";
            if(!is_null($row[5])) $vquery .= " and v.Model like '%".$row[5]."%'";
            if(!is_null($row[6])) $vquery .= " and v.Style like '%".$row[6]."%'";
            if(!is_null($row[7]) && (strlen($row[7]) > 0)) $vquery .= " and v.Type = '".$row[7]."'";
            if(!is_null($row[8])) $vquery .= " and v.Doors = '".$row[8]."'";
            if(!is_null($row[9]) && (strlen($row[9]) > 0)) $vquery .= " and v.Convertible = '".$row[9]."'";
            if(!is_null($row[10]) && (strlen($row[10]) > 0)) $vquery .= " and v.WheelDrive = '".$row[10]."'";
            if(!is_null($row[11]) && (strlen($row[11]) > 0)) $vquery .= " and v.BodyType = '".$row[11]."'";
            if(!is_null($row[14])) $vquery .= ' and dl.MileageStart > '.$row[14];
            if(!is_null($row[15])) $vquery .= ' and dl.MileageEnd < '.$row[15];
            if(!is_null($row[16])) $vquery .= ' and dh.MileageStart > '.$row[16];
            if(!is_null($row[17])) $vquery .= ' and dh.MileageEnd < '.$row[17];
            if(!is_null($row[18])) $vquery .= ' and dl.PriceStart > '.$row[18];
            if(!is_null($row[19])) $vquery .= ' and dl.PriceEnd < '.$row[19];
            if(!is_null($row[20])) $vquery .= ' and dh.PriceStart > '.$row[20];
            if(!is_null($row[21])) $vquery .= ' and dh.PriceEnd < '.$row[21];
            if(!is_null($row[22])) $vquery .= ' and (dl.BestBuy = '.$row[22].' or dh.BestBuy = '.$row[22].')';
            $vresult = mysql_query($vquery, $con);
            if($vresult && $vrow = mysql_fetch_array($vresult))
            {
                $rettext .= '#'.$vrow[0];
            }
            else
            {
                $rettext .= '#0';
            }
        }

        mysql_close($con);
    }

    // Send the response back...
    echo $rettext;
?>
