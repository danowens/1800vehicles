<?php
	require("globals.php"); 
	$current = $_POST['number']; 
    
	$userid = $_SESSION['userid'];
    $marketneedid = 0;

    $errorinload = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con){
        mysql_select_db(DB_SERVER_DATABASE, $con);
		
		if($current==1){
				//set default value
				$vehicletype			= 'Auto';
				$drivetrain				= 'Front Wheel Drive';
				$transmission			= 'Automatic';
				$mileagefrom			= '40000';
				$mileageto				= '50000';
				$mileageceiling			= '60000';
				$extlike				= '';
				$extdislike				= '';
				$intlike				= '';
				$intdislike				= '';
				$budgetfrom				= '30000';
				$budgetto				= '40000';
				$borrowmaxpayment		= 'Not Borrowing';
				$borrowdownpayment		= 'Not Borrowing';
				$vehicleneed			= 'Now';
				
				//What do you prefer start
				$frontstype				= 'Standard Bench';
				$bedtype				= 'Short Bed (about 6 ft.)';
				$leather				= 'Yes';
				$heatedseat				= 'Yes';
				$navigation				= 'Yes';
				$sunroof				= 'Yes';
				$alloywheels			= 'Yes';
				$rearwindow				= 'Yes';
				$bedliner				= 'Yes';
				$entertainmentsystem	= 'Yes';
				$thirdrs				= 'Yes';
				$crow					= 'Yes';
				$prhatch				= 'Yes';
				$backupcamera			= 'Yes';
				$tpackage				= 'Yes';
				//What do you prefer end
			
		}else{
			$vehicletype			= 'Auto';
			$drivetrain				= $_POST['drivetrain'];
			$transmission			= $_POST['transmission'];
			$mileagefrom			= $_POST['mileagefrom'];
			$mileageto				= $_POST['mileageto'];
			$mileageceiling			= $_POST['mileageceiling'];
			$extlike				= $_POST['extlike'];
			$extdislike				= $_POST['extdislike'];
			$intlike				= $_POST['intlike'];
			$intdislike				= $_POST['intdislike'];
			$budgetfrom				= $_POST['budgetfrom'];
			$budgetto				= $_POST['budgetto'];
			$borrowmaxpayment		= $_POST['borrowmaxpayment'];
			$borrowdownpayment		= $_POST['borrowdownpayment'];
			$vehicleneed			= $_POST['vehicleneed'];

			//What do you prefer start
			$frontstype				= $_POST['frontstype'];
			$bedtype				= $_POST['bedtype'];
			$leather				= $_POST['leather'];
			$heatedseat				= $_POST['heatedseat'];
			$navigation				= $_POST['navigation'];
			$sunroof				= $_POST['sunroof'];
			$alloywheels			= $_POST['alloywheels'];
			$rearwindow				= $_POST['rearwindow'];
			$bedliner				= $_POST['bedliner'];
			$entertainmentsystem	= $_POST['entertainmentsystem'];
			$thirdrs				= $_POST['thirdrs'];
			$crow					= $_POST['crow'];
			$prhatch				= $_POST['prhatch'];
			$backupcamera			= $_POST['backupcamera'];
			$tpackage				= $_POST['tpackage'];
			//What do you prefer end
		}
        //Get all vehicle years
		$yquery = "select distinct v.Year from vehicles v order by v.Year desc";
        $yresult = mysql_query($yquery, $con);
        $index = 0;
		$allyears = array();
		
        while($yrow = mysql_fetch_array($yresult))
        {
            $allyears[$index] = $yrow[0];
            $index++;
        }
		
		//Get all vehicle makes
		if($current==1){
			$mquery = "select distinct m.MakeID, m.Name from vehicles v, makes m where m.MakeID = v.MakeID and v.Year = '".$allyears[0]."' order by 2";
		}else{
			$mquery = "select distinct m.MakeID, m.Name from vehicles v, makes m where m.MakeID = v.MakeID and v.Year = '".$_POST['yearlist']."' order by 2";
		}
        $result = mysql_query($mquery, $con);
		
		$allmakes = array();
        while($result && $mrow = mysql_fetch_array($result))
        {
            $allmakes[] = $mrow;
        }
		
		//Get all vehicle model
		if($current==1){
			$modquery = "select distinct v.Model from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$allmakes[0]['MakeID']." and v.Year = '".$allyears[0]."' order by 1";
		}else{
			$modquery = "select distinct v.Model from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$_POST['makelist']." and v.Year = '".$_POST['yearlist']."' order by 1";
		}
		$result = mysql_query($modquery, $con);
		
		$allmodels = array();
		while($result && $row = mysql_fetch_array($result))
		{
			$allmodels[] = $row[0];
		}
		
		//Get all vehicle Style
		if($current==1){
			$styquery = "select distinct v.Style, v.VehicleID from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$allmakes[0]['MakeID']." and v.Year = '".$allyears[0]."' and v.Model = '".$allmodels[0]."' order by 1";
		}else{
			$styquery = "select distinct v.Style, v.VehicleID from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$_POST['makelist']." and v.Year = '".$_POST['yearlist']."' and v.Model = '".$_POST['modellist']."' order by 1";
		}
        $result = mysql_query($styquery, $con);
		
		$allstyles = array();
        $k = 0;
		while($result && $strow = mysql_fetch_array($result))
        {
			if($k==0){
				$vehicleid = $strow['VehicleID'];
			}else{
				$k = 1;
			}
            $allstyles[] = $strow;
        }
		
		//Update type of the vehicle based on Vehicle ID
		$query = "select Type from vehicles where VehicleID = '".$vehicleid."'";
		$result = mysql_query($query, $con);
		while($result && $row = mysql_fetch_array($result)){
			$vehicletype = $row['Type'];
		}
		
        mysql_close($con);
    }else {$errorinload = 'Could not retrieve Information';}

?>
	<div class="grideightgrey" style="padding: 0px !important;">
            
		<p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;"></p>
		<input type="hidden" id="vehicletype<?php echo $current?>" name="vehicletype<?php echo $current?>" value="<?php echo $vehicletype;?>" />
		<input type="hidden" id="specific<?php echo $current?>" name="specific<?php echo $current?>" value="0" />
		<div class="grideight" style="width: 95%; margin-top:-5px; margin-bottom: 0px;">
                    <table align="center" class="table researchspecific_table">
                       <?php if($current>1) {?> <tr><td colspan="2"><h3 class="helpful-tools">Your <?php echo $current?> Vehicle Spec<h3></td></tr><?php } ?>
				<tr id="selectyear<?php echo $current?>">
					<td style="width:30%"><strong>Year</strong></td>
					<td style="width:40%" >
						<select id="yearlist<?php echo $current;?>" name="yearlist<?php echo $current?>" onchange="yearchanged('<?php echo $current?>')">
							<?php
								$count = count($allyears);
								for($i = 0; $i < $count; $i++){
							?>
									<option value="<?php echo $allyears[$i]?>" <?php if(isset($_POST['yearlist']) && ($_POST['yearlist'] == $allyears[$i])){?> selected="selected"<?php }?>><?php echo $allyears[$i];?></option>
							<?php
								}
							?>
						</select>
					</td>
				</tr>
				<tr id="textyear<?php echo $current?>" style="display:none;">
					<td style="width:30%"><strong>Year</strong></td>
					<td style="width:40%" >
						<input class="no_popup" type="text" name="yeartext<?php echo $current;?>" id="yeartext<?php echo $current;?>" />
					</td>
				</tr>
				<tr id="selectmake<?php echo $current?>">
					<td><strong>Make</strong></td>
					<td align="left" >
						<select id="makelist<?php echo $current?>" name="makelist<?php echo $current?>" onchange="makechanged('<?php echo $current?>')">
							<?php
								$count = count($allmakes);
								for($i = 0; $i < $count; $i++){
							?>
									<option value="<?php echo $allmakes[$i]['MakeID']?>" <?php if(isset($_POST['makelist']) && ($_POST['makelist'] == $allmakes[$i]['MakeID'])){?> selected="selected"<?php }?> text_dis="<?php echo $allmakes[$i]['Name'];?>"><?php echo $allmakes[$i]['Name'];?></option>
							<?php
								}
							?>
						</select>
					</td> 
				</tr>
				<tr id="textmake<?php echo $current?>" style="display:none;">
					<td ><strong>Make</strong></td>
					<td align="left" >
						<input  class="no_popup" type="text" name="maketext<?php echo $current;?>" id="maketext<?php echo $current;?>" />
					</td>
				</tr>
				<tr id="selectmodel<?php echo $current?>">
					<td><strong>Model</strong></td>
					<td align="left" >
						<select id="modellist<?php echo $current?>" name="modellist<?php echo $current?>" onchange="modelchanged('<?php echo $current?>')">
							<?php
								$count = count($allmodels);
								for($i = 0; $i < $count; $i++){
							?>
									<option value="<?php echo $allmodels[$i]?>" <?php if(isset($_POST['modellist']) && ($_POST['modellist'] == $allmodels[$i])){?> selected="selected"<?php }?>><?php echo $allmodels[$i];?></option>
							<?php
								}
							?>
						</select>
					</td>
				</tr>
				<tr id="textmodel<?php echo $current?>" style="display:none;">
					<td ><strong>Model</strong></td>
					<td align="left" >
						<input class="no_popup" type="text" name="modeltext<?php echo $current;?>" id="modeltext<?php echo $current;?>" />
					</td>
				</tr>
				<tr id="selectstyle<?php echo $current?>">
					<td><strong>Style</strong></td>
					<td align="left" >
						<select id="stylelist<?php echo $current?>" name="stylelist<?php echo $current?>" onchange="stylechanged('<?php echo $current?>')">
							<?php
								$count = count($allstyles);
								for($i = 0; $i < $count; $i++){
							?>
									<option value="<?php echo $allstyles[$i]['VehicleID'].';'.$allstyles[$i]['Style'];?>" <?php if(isset($_POST['stylelist']) && ($_POST['stylelist'] == $allstyles[$i]['VehicleID'].';'.$allstyles[$i]['Style'])){?> selected="selected"<?php }?>><?php echo $allstyles[$i]['Style'];?></option>
							<?php
								}
							?>
						</select>
					</td>
				</tr>
				<tr id="textstyle<?php echo $current?>" style="display:none;">
					<td ><strong>Style</strong></td>
					<td align="left" >
						<input class="no_popup" type="text" name="styletext<?php echo $current;?>" id="styletext<?php echo $current;?>" />
					</td>
				</tr>
				
				<tr id="specificsection<?php echo $current?>"><td colspan="2"><strong>Not seeing the vehicle you want?<span style="color:blue; cursor:pointer;" onclick="javascript: specific('<?php echo $current?>', 1)" > Click here</span>.</strong></td></tr>
                                
				<tr id="vehiclesection<?php echo $current?>" style="display:none;"><td colspan="2"><strong>Want to select from the list? <span style="color:blue;cursor:pointer;" onclick="javascript: specific('<?php echo $current?>', 0)" >Click here</span>.</strong></td></tr>
                                <!--<tr style="text-align: center"><td colspan="2" ><button type="button" style="font-size:14px;" class="med view_price" value='<?php echo $current?>'>SEE EXAMPLE PICTURES & PRICE RANGES</button></td></tr>-->
				<!--<tr><td colspan="2">&nbsp;</td></tr>-->
				<tr>
					<td><strong>Other styles (or packages) you will consider</strong></td>
					<td align="left" >
						<input type="text" name="otherstyle<?php echo $current?>" id="otherstyle<?php echo $current?>" placeholder="Text box" value="<?php if(isset($_POST['otherstyle'])){ echo $_POST['otherstyle'];}?>">
					</td>
				</tr>
				<tr>
					<td><strong>Other year models you will consider:</strong></td>
					<td align="left" >
						<input type="text" name="otheryear<?php echo $current?>" id="otheryear<?php echo $current?>" placeholder="Text box" value="<?php if(isset($_POST['otheryear'])){ echo $_POST['otheryear'];}?>">
					</td>
				</tr>
				<tr>
					<td><strong>How soon do you need a vehicle?</strong></td>
					<td align="left" >
						<select name="vehicleneed<?php echo $current?>" id="vehicleneed<?php echo $current?>">
							<option value="Now" <?php if($vehicleneed == 'Now') {?> selected="selected" <?php }?>>Now!</option>
							<option value="1-2 weeks" <?php if($vehicleneed == '1-2 weeks') {?> selected="selected" <?php }?>>1-2 weeks</option>
							<option value="3 weeks is ok" <?php if($vehicleneed == '3 weeks is ok') {?> selected="selected" <?php }?>>3 weeks is ok</option>
							<option value="A month or more is ok" <?php if($vehicleneed == 'A month or more is ok') {?> selected="selected" <?php }?>>A month or more is ok</option>
						</select>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				 <tr><td colspan="2"><strong class="assessment_insideother">Mileage:</strong></td></tr>
				<tr>
					<td><strong>Mileage you are hoping for</strong></td>
					<td align="left" width="10%">
						<select name="mileagefrom<?php echo $current?>" id="mileagefrom<?php echo $current?>" class="width103">
							<option value="Flexible"<?php if($mileagefrom=='Small'){?> selected="selected" <?php }?>>Flexible</option>
							<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
								<option value="<?php echo $i;?>" <?php if($i==$mileagefrom){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
							<?php }?>
						</select>  TO  
                                                
                                                <select name="mileageto<?php echo $current?>" id="mileageto<?php echo $current?>" class="width100">
							<option value="Flexible"<?php if($mileageto=='Small'){?> selected="selected" <?php }?>>Flexible</option>
							<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
								<option value="<?php echo $i;?>" <?php if($i== $mileageto){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
							<?php }?>
						</select>
					</td>
					
						
					</td>
				</tr>
				<tr>
					<td><strong>Mileage Ceiling</strong></td>
					<td align="left" >
						<select name="mileageceiling<?php echo $current?>" id="mileageceiling<?php echo $current?>">
							<option value="Flexible"<?php if($mileageceiling=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
							<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
								<option value="<?php echo $i;?>" <?php if($i==$mileageceiling){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
							<?php }?>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Transmission</strong></td>
					<td align="left" >
						<select name="transmission<?php echo $current?>" id="transmission<?php echo $current?>">
							<option value="Automatic"<?php if($transmission=='Automatic'){?> selected="selected" <?php }?>>Automatic</option>
							<option value="Manual"<?php if($transmission=='Manual'){?> selected="selected" <?php }?>>Manual</option>
							<option value="Flexible"<?php if($transmission=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Drive Train</strong></td>
					<td align="left" >
						<select name="drivetrain<?php echo $current?>" id="drivetrain<?php echo $current?>">
						<?php if($vehicletype=='Auto'){?>
							<option value="Front Wheel Drive"<?php if($drivetrain=='Front Wheel Drive'){?> selected="selected" <?php }?>>Front Wheel Drive</option>
							<option value="Rear Wheel Drive"<?php if($drivetrain=='Rear Wheel Drive'){?> selected="selected" <?php }?>>Rear Wheel Drive</option>
							<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
						<?php }elseif($vehicletype=='Minivan' || $vehicletype == 'MiniVan'){?>
							<option value="Front Wheel Drive"<?php if($drivetrain=='Front Wheel Drive'){?> selected="selected" <?php }?>>Front Wheel Drive</option>
							<option value="4wd"<?php if($drivetrain=='4wd'){?> selected="selected" <?php }?>>4wd</option>
							<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
						<?php }elseif($vehicletype=='SUV' || $vehicletype == 'Pickup'){?>
							<option value="2wd"<?php if($drivetrain=='2wd'){?> selected="selected" <?php }?>>2wd</option>
							<option value="4wd"<?php if($drivetrain=='4wd'){?> selected="selected" <?php }?>>4wd</option>
							<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
						<?php }?>
						</select>
					</td>
				</tr>
						<tr><td><strong>Exterior colors you like </strong></td>
                                                <td align="left" >
								<input type="text" name="extlike<?php echo $current?>" id="extlike<?php echo $current?>" value="<?php echo $extlike?>" placeholder="Text box">
						</td>
                                                
                                                </tr>

						
						<tr><td ><strong>Interior colors you like</strong></td>
                                                    <td align="left" >
								<input type="text" name="intlike<?php echo $current?>" id="intlike<?php echo $current?>" value="<?php echo $intlike?>" placeholder="Text box">
							</td>
                                                </tr>
                               
<!--				<tr><td colspan="2">&nbsp;</td></tr>
                                     <tr><td colspan="2"><strong class="assessment_insideother">If Borrowing:</strong></td></tr>
				<tr>
					<td><strong>Maximum payment is</strong></td>
					<td align="left" >$
						<select name="borrowmaxpayment<?php echo $current?>" id="borrowmaxpayment<?php echo $current?>" class="width222">
							<option value="Not Applicable" <?php if($borrowmaxpayment == 'Not Applicable') {?> selected="selected" <?php }?>>Not Applicable</option>
							<option value="Not Borrowing" <?php if($borrowmaxpayment == 'Not Borrowing') {?> selected="selected" <?php }?>>Not Borrowing</option>
							<option value="Not Sure"<?php if($borrowmaxpayment == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
							<?php for($i=100; $i <= 2000; $i=$i+10){?>
								<option value="<?php echo $i;?>" <?php if($i==$borrowmaxpayment){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
							<?php }?>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Down payment is</strong></td>
					<td align="left" >$
						<select name="borrowdownpayment<?php echo $current?>" id="borrowdownpayment<?php echo $current?>" class="width222">
							<option value="Not Applicable" <?php if($borrowdownpayment == 'Not Applicable') {?> selected="selected" <?php }?>>Not Applicable</option>
							<option value="Not Borrowing" <?php if($borrowdownpayment == 'Not Borrowing') {?> selected="selected" <?php }?>>Not Borrowing</option>
							<option value="Not Sure" <?php if($borrowdownpayment == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
							<?php for($i=100; $i <= 2000; $i=$i+10){?>
								<option value="<?php echo $i;?>" <?php if($i==$borrowdownpayment){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
							<?php }?>
						</select>
					</td>
				</tr>						
				<tr><td colspan="2">&nbsp;</td></tr>-->
                                
				<tr><td colspan="2"><strong class="assessment_insideother">Do you prefer:</strong></td></tr>
				<tr id="trfrontstype<?php echo $current?>" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
					<td><strong>Front Seat Type</strong></td>
					<td align="left" >
						<select name="frontstype<?php echo $current?>" id="frontstype<?php echo $current?>">
							<option value="Standard Bench" <?php if($frontstype == 'Standard Bench') {?> selected="selected" <?php }?>>Standard Bench</option>
							<option value="Bench Warmrest" <?php if($frontstype == 'Bench Warmrest') {?> selected="selected" <?php }?>>Bench Warmrest</option>
							<option value="Bucket Seats" <?php if($frontstype == 'Bucket Seats') {?> selected="selected" <?php }?>>Bucket Seats</option>
							<option value="Flexible" <?php if($frontstype == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trbedtype<?php echo $current?>" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
					<td><strong>Bed Type</strong></td>
					<td align="left" >
						<select name="bedtype<?php echo $current?>" id="bedtype<?php echo $current?>">
							<option value="Short Bed (about 6 ft.)" <?php if($bedtype == 'Short Bed (about 6 ft.)') {?> selected="selected" <?php }?>>Short Bed (about 6 ft.)</option>
							<option value="Long Bed (about 8 ft.)" <?php if($bedtype == 'Long Bed (about 8 ft.)') {?> selected="selected" <?php }?>>Long Bed (about 8 ft.)</option>
							<option value="Extra Long Bed (about 10 ft.)" <?php if($bedtype == 'Extra Long Bed (about 10 ft.)') {?> selected="selected" <?php }?>>Extra Long Bed (about 10 ft.)</option>
							<option value="Mini Bed (about 4 ft.)" <?php if($bedtype == 'Mini Bed (about 4 ft.)') {?> selected="selected" <?php }?>>Mini Bed (about 4 ft.)</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Leather</strong></td>
					<td align="left" >
						<select name="leather<?php echo $current?>" id="leather<?php echo $current?>">
							<option value="Yes" <?php if($leather == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($leather == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($leather == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($leather == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Heated Seats</strong></td>
					<td align="left" >
						<select name="heatedseat<?php echo $current?>" id="heatedseat<?php echo $current?>">
							<option value="Yes" <?php if($heatedseat == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($heatedseat == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($heatedseat == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($heatedseat == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Navigation</strong></td>
					<td align="left" >
						<select name="navigation<?php echo $current?>" id="navigation<?php echo $current?>">
							<option value="Yes" <?php if($navigation == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($navigation == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($navigation == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($navigation == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Sunroof</strong></td>
					<td align="left" >
						<select name="sunroof<?php echo $current?>" id="sunroof<?php echo $current?>">
							<option value="Yes" <?php if($sunroof == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($sunroof == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($sunroof == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($sunroof == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Alloy Wheels</strong></td>
					<td align="left" >
						<select name="alloywheels<?php echo $current?>" id="alloywheels<?php echo $current?>">
							<option value="Yes" <?php if($alloywheels == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($alloywheels == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($alloywheels == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($alloywheels == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trrearwindow<?php echo $current?>" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
					<td><strong>Rear Sliding Window</strong></td>
					<td align="left" >
						<select name="rearwindow<?php echo $current?>" id="rearwindow<?php echo $current?>">
							<option value="Yes" <?php if($rearwindow == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($rearwindow == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($rearwindow == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($rearwindow == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trbedliner<?php echo $current?>" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
					<td><strong>Bed Liner</strong></td>
					<td align="left" >
						<select name="bedliner<?php echo $current?>" id="bedliner<?php echo $current?>">
							<option value="Yes" <?php if($bedliner == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($bedliner == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($bedliner == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($bedliner == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trentertainmentsystem<?php echo $current?>" <?php if($vehicletype == 'Pickup' || $vehicletype == 'Auto'){ ?> style="display:none;" <?php }?>>
					<td><strong>Entertainment System</strong></td>
					<td align="left" >
						<select name="entertainmentsystem<?php echo $current?>" id="entertainmentsystem<?php echo $current?>">
							<option value="Yes" <?php if($entertainmentsystem == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($entertainmentsystem == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($entertainmentsystem == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($entertainmentsystem == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trthirdrs<?php echo $current?>" <?php if($vehicletype != 'SUV'){ ?> style="display:none;" <?php }?>>
					<td><strong>Third Row Seating</strong></td>
					<td align="left" >
						<select name="thirdrs<?php echo $current?>" id="thirdrs<?php echo $current?>">
							<option value="Yes" <?php if($thirdrs == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($thirdrs == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($thirdrs == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($thirdrs == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trcrow<?php echo $current?>" <?php if($vehicletype != 'SUV'){ ?> style="display:none;" <?php }?>>
					<td><strong>Captain chairs center row  </strong></td>
					<td align="left" >
						<select name="crow<?php echo $current?>" id="crow<?php echo $current?>">
							<option value="Yes" <?php if($crow == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($crow == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($crow == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($crow == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trprhatch<?php echo $current?>" <?php if($vehicletype == 'Pickup' || $vehicletype == 'Auto'){ ?> style="display:none;" <?php }?>>
					<td><strong>Power Rear Hatch</strong></td>
					<td align="left" >
						<select name="prhatch<?php echo $current?>" id="prhatch<?php echo $current?>">
							<option value="Yes" <?php if($prhatch == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($prhatch == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($prhatch == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($prhatch == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trbackupcamera<?php echo $current?>" <?php if($vehicletype != 'Auto'){ ?> style="display:none;" <?php }?>>
					<td><strong>Backup Camera</strong></td>
					<td align="left" >
						<select name="backupcamera<?php echo $current?>" id="backupcamera<?php echo $current?>">
							<option value="Yes" <?php if($backupcamera == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($backupcamera == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($backupcamera == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($backupcamera == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
				<tr id="trtpackage<?php echo $current?>" <?php if($vehicletype == 'Minivan' || $vehicletype == 'MiniVan' || $vehicletype == 'Auto'){ ?> style="display:none;" <?php }?>>
					<td><strong>Tow package</strong></td>
					<td align="left" >
						<select name="tpackage<?php echo $current?>" id="tpackage<?php echo $current?>">
							<option value="Yes" <?php if($tpackage == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
							<option value="No" <?php if($tpackage == 'No') {?> selected="selected" <?php }?>>No</option>
							<option value="Would really like to have" <?php if($tpackage == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
							<option value="Flexible" <?php if($tpackage == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
						</select>
					</td>
				</tr>
                               <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
					<td><strong>Notes from Client</strong></td>
					<td align="left" >
						<input type="text" name="clientnote<?php echo $current?>" id="clientnote<?php echo $current?>" placeholder="Text box" value="<?php if(isset($_POST['clientnote'])){ echo $_POST['clientnote'];}?>">
					</td>
				</tr>
                               <tr><td colspan="2">&nbsp;</td></tr>
                               <!-- <tr><td colspan="2" align="center">
                                        
                                           <span style="width: 100%">
                                               <button type="button" class="med submit_form"><nobr>Continue to Complete Vehicle Spec</nobr></button>
                                           &nbsp;<strong>OR</strong>&nbsp; 
                                           <button type="button" class="blueonwhitehideshow showrequirfield" id="pos_page">Request a Price & Availability</button>
                                           </span>
                                       </td></tr> -->
                                   </table>  
                                 <table align="center" class="table researchspecific_table" id="notshowfields<?php echo $current?>" >                                
                                <tr><td colspan="2" class="assessment_insidetd_green" ><strong class="assessment_inside">NONE OF THESE FIELDS ARE REQUIRED</strong></td></tr>
						<tr><td style="width:30%"><strong>Exterior colors you don't like </strong></td>
                                                <td align="left" style="width:40%" >
								
                                                               <input type="text" name="extdislike<?php echo $current?>" id="extdislike<?php echo $current?>" value="<?php echo $extdislike?>" placeholder="Text box">
						</td>                                                
                                                </tr>

						<tr><td ><strong>Interior colors you don't like</strong></td>
                                                    <td align="left" >	
                                                               <input type="text" name="intdislike<?php echo $current?>" id="intdislike<?php echo $current?>" value="<?php echo $intdislike?>" placeholder="Text box">
							</td>
                                                </tr>
				<tr>
					<td><strong>Must Have</strong></td>
					<td align="left" >
						<input type="text" name="musthave<?php echo $current?>" id="musthave<?php echo $current?>" placeholder="Text box" value="<?php if(isset($_POST['musthave'])){ echo $_POST['musthave'];}?>">
					</td>
				</tr>
				<tr>
					<td><strong>Would really like to have</strong></td>
					<td align="left" >
						<input type="text" name="reallyhave<?php echo $current?>" id="reallyhave<?php echo $current?>" placeholder="Text box" value="<?php if(isset($_POST['reallyhave'])){ echo $_POST['reallyhave'];}?>">
					</td>
				</tr>
				<tr>
					<td><strong>Flexible On</strong></td>
					<td align="left" >
						<input type="text" name="flexible<?php echo $current?>" id="flexible<?php echo $current?>" placeholder="Text box" value="<?php if(isset($_POST['flexible'])){ echo $_POST['flexible'];}?>">
					</td>
				</tr>
				<tr>
					<td><strong>Do not want</strong></td>
					<td align="left" >
						<input type="text" name="notwant<?php echo $current?>" id="notwant<?php echo $current?>" placeholder="Text box" value="<?php if(isset($_POST['notwant'])){ echo $_POST['notwant'];}?>">
					</td>
				</tr>
                                <tr style="text-align: center"><td colspan="2" ><button type="button" style="font-size:14px;" class="med view_price" value='<?php echo $current?>'>SEE EXAMPLE PICTURES & PRICE RANGES FOR THIS VEHICLE</button></td></tr>
				
				
			</table>
		</div><!-- end greyeight-->
	</div><!-- grid eight container -->
	<br clear="all" />