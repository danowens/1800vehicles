<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 3;
    $_SESSION['substate'] = 1;
    $_SESSION['titleadd'] = 'Watchlist';

    $marketneedid = $_SESSION['marketneedid'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select q.quoterequestid, q.year, q.make, q.model, q.style, f.lastupdated, q.quotetype, f.ordertype, f.pricepoint, f.firmquoteid, f.lastupdated, w.accepted, f.imagefile from quoterequests q,firmquotes f,watchfor wf,watches w where w.watchid=wf.watchid and wf.firmquoteid=f.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=".$marketneedid;
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $fquoteid[$index] = $row[0];
            $fyear[$index] = $row[1];
            $fmake[$index] = $row[2];
            $fmodel[$index] = $row[3];
            $fstyle[$index] = $row[4];
            $fupdated[$index] = $row[5];
            $fquotetype[$index] = $row[6];
            $fordertype[$index] = $row[7];
            $fprice[$index] = $row[8];
            $ffirmid[$index] = $row[9];
            $flastup[$index] = $row[10];
            $fwatchdate[$index] = $row[11];
            $fimage[$index] = $row[12];

            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 300px;">View My Watchlist Details</h1>
        <div class="grideightgrey">
<?php
    if($index < 1)
    {
        echo '<p class="blackfourteen" style="color:#142c3c;">* You do not have anything Posted to the Watchlist at this time.</p>';
    }
    else
    {
        $currentquote = -1;
        $count = count($fwatchdate);
        for($i=0;$i<$count;$i++)
        {
            if($currentquote != $fquoteid[$i])
            {
                if($currentquote != -1)
                {
                    echo '</td>';
                    echo '</tr>';
                    echo '</table><br/>';
                }

                $currentquote = $fquoteid[$i];

                $imagefile = '';
                if(isset($fimage[$i]))
                {
                    if(!file_exists($fimage[$i])) $imagefile = '';
                    else $imagefile = $fimage[$i];
                }

                if(!isset($imagefile) || (strlen($imagefile) < 1))
                {
                    if($fquotetype[$i] == 'Standard')
                    {
                        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                        if($con)
                        {
                            // First fill the list of Makes...
                            mysql_select_db(DB_SERVER_DATABASE, $con);

                            $result = mysql_query("select v.imagefile from vehicles v, makes m where m.makeid=v.makeid and m.name='".$fmake[$i]."' and v.year=".$fyear[$i]." and v.model='".$fmodel[$i]."' and v.style='".$fstyle[$i]."'");
                            if($result && $row = mysql_fetch_array($result))
                            {
                                if(strlen($row[0]) > 0) $imagefile = $row[0];
                            }
                            else $imagefile = '';
                            mysql_close($con);
                        }
                        else $imagefile = '';
                    }
                }

                if(isset($imagefile) && (strlen($imagefile) > 0))
                {
                    $max_width = 500;
                    $max_height = 500;
                    echo '<center>';
                    echo '<img id="vehimage" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" style="margin-top:10px;" />';
                    echo '</center>';
                    echo '<br />';
                    echo '<br />';
                }
                //else echo '** No Image Available **';

                echo '<table border="0" width="550" cellspacing="5" style="margin-left:35px; font-size:14px">';
                echo '<tr>';
                echo '<td><strong>Order Type</strong></td>';
                echo '<td><span style="color: #85c11b;">Group Watchlist</span></td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td><strong>Date Posted</strong></td>';
                echo '<td>'.date_at_timezone('m/d/Y', 'EST', $fwatchdate[$i]).'</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td><strong>Current Market Study attached</strong></td>';
                echo '<td>';
            }

            echo '<a href="quotereceived.php?QuoteID='.$fquoteid[$i].'">';
            echo $fyear[$i].' '.$fmake[$i].' '.$fmodel[$i].' '.$fstyle[$i];
            echo '</a><br/>';
        }
        echo '</td>';
        echo '</tr>';
        echo '</table>';
    }
?>
        </div><!--end grideightgrey-->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
