
<?php require("globals.php"); ?>
<?php
require_once(WEB_ROOT_PATH . 'common/functions/string.php');
//error_reporting(E_ALL);
//	ini_set("display_errors", "on");
$_SESSION['state'] = 5;
$_SESSION['substate'] = 9;
$_SESSION['titleadd'] = 'Edit Vehicle Specification';

//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";

$userid = $_SESSION['userid'];
if (!isset($userid)) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - UserID not set';
    header('Location: mydashboard.php#admintab');
    exit();
}
        $inuserid = $_POST['ForUserID'];
        $inmneed = $_POST['MarketNeedID'];
        $SearchPlanDetailID = $_POST['SearchPlanDetailID'];
	$total			= 0;
	
	$con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con){
        mysql_select_db(DB_SERVER_DATABASE, $con);
			$vspquery	= "select * from searchplandetails where SearchPlanDetailID = ".$SearchPlanDetailID;
			$vspresult = mysql_query($vspquery, $con);
			
			$vspdata = array();
			
			while($vsprow = mysql_fetch_array($vspresult))
			{
				$total = $total+1;
				$vspdata[$total] = $vsprow;
			}
                        
                        
            $query = "select FirstName, LastName, Email, Note from users where userid=" . $_SESSION['userid'];
            $result = mysql_query($query, $con);
            if ($result && $row = mysql_fetch_array($result)) {
                $ifname = $row[0];
                $ilname = $row[1];
            }
            
          $assessments_query = "SELECT BudgetFrom,BudgetTo FROM  `assessments` WHERE  `MarketNeedID` =".$inmneed;
        
       
        
         $assessments_result = mysql_query($assessments_query, $con);
            if ($assessments_result && $row = mysql_fetch_array($assessments_result)) { 
                $ass_BudgetFrom = $row[0];
                $ass_BudgetTo = $row[1];
            }
		
		mysql_close($con);
	}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php // require("headerend.php"); ?>
<script type="text/javascript">



 $(document).ready(function() {
        $("input:text").click(function() {
            var element = $(this);
            $("#notetext").val(element.val());
            $("#dialog").dialog({
                modal: true,
                title: " ",
                close: function(event, ui) {
                    element.val($("#notetext").val());
                    $("#dialog").dialog("destroy");
                }
            });
        });
    });
	
	function yearchanged(id){
		var year = $("#yearlist"+id).val();
		$.ajax({
			type: "POST",
			url: 'ajaxallmakes.php',
			data: { year:year },
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#makelist"+id).html('');
				$("#makelist"+id).append(data);
				
				makechanged(id);
			}
		});
	}
	
	function makechanged(id){
		var year = $("#yearlist"+id).val();
		var make = $("#makelist"+id).val();
		$.ajax({
			type: "POST",
			url: 'ajaxallmodels.php',
			data: { year:year, make:make},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#modellist"+id).html('');
				$("#modellist"+id).append(data);
				
				modelchanged(id);
			}
		});
	}
	
	function modelchanged(id){
		var year	= $("#yearlist"+id).val();
		var make	= $("#makelist"+id).val();
		var model	= $("#modellist"+id).val();
		
		$.ajax({
			type: "POST",
			url: 'ajaxallstyles.php',
			data: { year:year, make:make, model:model},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#stylelist"+id).html('');
				$("#stylelist"+id).append(data);
				
				stylechanged(id);
			}
		});
	}
	
	function stylechanged(id){
		var style = $("#stylelist"+id).val().split(";");
		$.ajax({
			type: "POST",
			url: 'ajaxcheckvehicletype.php',
			data: { id:style[0]},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#vehicletype"+id).val(data);
				if(data=='Auto'){
					$("#drivetrain"+id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).hide();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).hide();
					$("#trbackupcamera"+id).hide();
					$("#trtpackage"+id).hide();
					
				}else if(data=='Minivan' || data=='MiniVan'){
					$("#drivetrain"+id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).show();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).show();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).hide();

				}else if(data=='SUV'){
					$("#drivetrain"+id).html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
				
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).show();
					$("#trthirdrs"+id).show();
					$("#trcrow"+id).show();
					$("#trprhatch"+id).show();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).show();

				}else if(data=='Pickup'){
					$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					//@TODO make function with 2 parameter (Array of ids and event)
					$("#trfrontstype"+id).show();
					$("#trbedtype"+id).show();
					$("#trrearwindow"+id).show();
					$("#trbedliner"+id).show();
					$("#trentertainmentsystem"+id).hide();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).hide();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).show();
				}
			}
		});
	}
	
	function specific(id, val){
		$("#specific"+id).val(val);
		if(val==1){
			$("#specificsection"+id).hide();
			$("#selectyear"+id).hide();
			$("#selectmake"+id).hide();
			$("#selectmodel"+id).hide();
			$("#selectstyle"+id).hide();
			
			$("#vehiclesection"+id).show();
			$("#textyear"+id).show();
			$("#textmake"+id).show();
			$("#textmodel"+id).show();
			$("#textstyle"+id).show();
		}else{
			$("#specificsection"+id).show();
			$("#selectyear"+id).show();
			$("#selectmake"+id).show();
			$("#selectmodel"+id).show();
			$("#selectstyle"+id).show();
			
			$("#vehiclesection"+id).hide();
			$("#textyear"+id).hide();
			$("#textmake"+id).hide();
			$("#textmodel"+id).hide();
			$("#textstyle"+id).hide();
		}
	}

</script>
<link href="demovehicles/css/style.css" rel="stylesheet" type="text/css">
<?php
					//$vspdata['Year']
					for($current = 1; $current <= $total; $current++){
						$recon = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
						if($recon){
							mysql_select_db(DB_SERVER_DATABASE, $recon);
							
							//Get all vehicle years
							$yquery = "select distinct v.Year from vehicles v order by v.Year desc";
							$yresult = mysql_query($yquery, $recon);
							$index = 0;
							$allyears = array();
							
							while($yrow = mysql_fetch_array($yresult))
							{
								$allyears[$index] = $yrow[0];
								$index++;
							}
							
							//Get all vehicle makes
							$mquery = "select distinct m.MakeID, m.Name from vehicles v, makes m where m.MakeID = v.MakeID and v.Year = '".$vspdata[$current]['Year']."' order by 2";
							$result = mysql_query($mquery, $recon);
							
							$allmakes = array();
							while($result && $mrow = mysql_fetch_array($result))
							{
								$allmakes[] = $mrow;
							}
							
							//Get all vehicle model
							$modquery = "select distinct v.Model from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$vspdata[$current]['Make']." and v.Year = '".$vspdata[$current]['Year']."' order by 1";
							$result = mysql_query($modquery, $recon);
							
							$allmodels = array();
							while($result && $row = mysql_fetch_array($result))
							{
								$allmodels[] = $row[0];
							}
							
							//Get all vehicle Style
							$styquery = "select distinct v.Style, v.VehicleID from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$vspdata[$current]['Make']." and v.Year = '".$vspdata[$current]['Year']."' and v.Model = '".$vspdata[$current]['Model']."' order by 1";
							$result = mysql_query($styquery, $recon);
							
							$allstyles = array();
							$k = 0;
							while($result && $strow = mysql_fetch_array($result))
							{
								if($k==0){
									$vehicleid = $strow['VehicleID'];
								}else{
									$k = 1;
								}
								$allstyles[] = $strow;
							}
							
							//Update type of the vehicle based on Vehicle ID
							$query = "select Type from vehicles where VehicleID = '".$vehicleid."'";
							$result = mysql_query($query, $recon);
							while($result && $row = mysql_fetch_array($result)){
								$vehicletype = $row['Type'];
							}
							
							$specific				= $vspdata[$current]['Specific'];
							$drivetrain				= $vspdata[$current]['DriveTrain'];
							$transmission			= $vspdata[$current]['Transmission'];
							$mileagefrom			= $vspdata[$current]['MileageFrom'];
							$mileageto				= $vspdata[$current]['MileageTo'];
							$mileageceiling			= $vspdata[$current]['MileageCeiling'];
							$extlike				= $vspdata[$current]['ExtLike'];
							$extdislike				= $vspdata[$current]['ExtDislike'];
							$intlike				= $vspdata[$current]['IntLike'];
							$intdislike				= $vspdata[$current]['IntDisike'];
							$budgetfrom				= $vspdata[$current]['BudgetFrom'];
							$budgetto				= $vspdata[$current]['BudgetTo'];
							$borrowmaxpayment		= $vspdata[$current]['BorrowMaxPayment'];
							$borrowdownpayment		= $vspdata[$current]['BorrowDownPayment'];
							$vehicleneed			= $vspdata[$current]['VehicleNeed'];

							//What do you prefer start
							$frontstype				= $vspdata[$current]['FrontSType'];
							$bedtype				= $vspdata[$current]['BedType'];
							$leather				= $vspdata[$current]['Leather'];
							$heatedseat				= $vspdata[$current]['HeatedSeat'];
							$navigation				= $vspdata[$current]['Navigation'];
							$sunroof				= $vspdata[$current]['SunRoof'];
							$alloywheels			= $vspdata[$current]['AlloyWheels'];
							$rearwindow				= $vspdata[$current]['RearWindow'];
							$bedliner				= $vspdata[$current]['BedLiner'];
							$entertainmentsystem	= $vspdata[$current]['EntertainmentSystem'];
							$thirdrs				= $vspdata[$current]['ThirdRD'];
							$crow					= $vspdata[$current]['CRow'];
							$prhatch				= $vspdata[$current]['PRHatch'];
							$backupcamera			= $vspdata[$current]['BackupCamera'];
							$tpackage				= $vspdata[$current]['TPackage'];
                                                        $plan_rating				= $vspdata[$current]['plan_rating'];                                                        
                                                        $SearchPlanDetailID=  $vspdata[$current]['SearchPlanDetailID'];
                                                         $SearchPlanID=  $vspdata[$current]['SearchPlanID'];
							//What do you prefer end
							
                                  ?>    
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;"> Add Vehicle For Demo</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: -5px;">
   <form method="post" action="/salesrepactions.php">
        <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
        <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
        <button class="blueongrey_dash" value="" style="font-size: 11px;" type="submit">
            Go back to Customer Review
        </button>
    </form>
               <div id="dialog" title=" " style="display: none;">
    <textarea name="notetext" id="notetext"  style="resize:vertical;width:100%;height:100%"></textarea>
</div> 
                <table class="table"  style="margin-top: 25px;">
										
                                                                            <tr >
											<td style="width: 40%;"><strong>Seller Name</strong></td>
											<td style="width: 60%;">
                                                                                            <strong><?=$ifname . ' ' . $ilname;?></strong>
											</td>
										</tr>
                                                                            
                                                                            <tr id="selectyear" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td ><strong>Year</strong></td>
											<td ><?php echo $vspdata[$current]['Year']?>
											</td>
										</tr>
										<tr id="textyear" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Year</strong></td>
											<td >
												<input type="text" name="yeartext" id="yeartext" value="<?php echo $vspdata[$current]['TextYear'];?>" />
											</td>
										</tr>
										<tr id="selectmake" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td><strong>Make</strong></td>
											<td >
												<?php echo $vspdata[$current]['Make'];?>
											</td> 
										</tr>
										<tr id="textmake" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Make</strong></td>
											<td >
												<?php echo $vspdata[$current]['TextMake'];?>
											</td>
										</tr>
										<tr id="selectmodel" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td><strong>Model</strong></td>
											<td >
												<?php echo $vspdata[$current]['Model'];?>
											</td>
										</tr>
										<tr id="textmodel" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Model</strong></td>
											<td >
												<?php echo $vspdata[$current]['TextModel'];?>
											</td>
										</tr>
										<tr id="selectstyle" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td><strong>Style</strong></td>
											<td >
												<?php echo $vspdata[$current]['Style'];?>
											</td>
										</tr>
										<tr id="textstyle" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Style</strong></td>
											<td >
												<?php echo $vspdata[$current]['TextStyle'];?>
											</td>
										</tr>
										<tr><td colspan="2">&nbsp;</td></tr>										
										
										
                                                                                
										
									</table>
                
                
                <div class="model_detail_main subm" id="sendnew">
<h2>Send New Quote</h2>
<input type="hidden" name="fname" value="<?php echo $total_result['Firstname']; ?>">
<input type="hidden" name="email_id" value="<?php echo $total_result['Email']; ?>">
<input type="hidden" name="user_id" value="<?php echo $total_result['UserID']; ?>">
<input type="hidden" name="user_name" value="<?php  echo mt_rand(); ?>">
<input type="hidden" name="user_pass" value="<?php echo mt_rand(); ?>">
<!--------------------Gallery Tabing----------------------------->

<div class="send_top_bar">



</div>
</div>

<!--------------------Gallery Tabing----------------------------->
<div class="detail_form_con">
<div class="detail_form_out">
<div class="model_name_con">Year</div>
<div class="model_txt_out">
<input type="text" name="year" class="model_txtfld" required>
</div>
</div>

<div class="detail_form_out">
<div class="model_name_con">Make</div>
<div class="model_txt_out">
<input type="text" name="make" class="model_txtfld" required>
</div>
</div>
</div>


<div class="detail_form_con">
<div class="detail_form_out">
<div class="model_name_con">Model</div>
<div class="model_txt_out">
<input type="text" name="modal" class="model_txtfld" required>
</div>
</div>
<div class="detail_form_out">
<div class="model_name_con">Style</div>
<div class="model_txt_out">
<input type="text" name="style" class="model_txtfld" required>
</div>
</div>
</div>

<div class="detail_form_con">
<div class="detail_form_out">
<div class="model_name_con">Mileage</div>
<div class="model_txt_out">
<input type="text" name="mileage" class="model_txtfld" required>
</div>
</div>
<div class="detail_form_out">
<div class="model_name_con">Exterior Color</div>
<div class="model_txt_out">
<input type="text" name="ex_color" class="model_txtfld" required>
</div>
</div>
</div>


<div class="detail_form_con">
<div class="detail_form_out">
<div class="model_name_con">Interior Color</div>
<div class="model_txt_out">
<input type="text" name="in_color" class="model_txtfld" required></div>
</div>
<div class="detail_form_out">
<div class="model_name_con">Price</div>
<div class="model_txt_out">
<input type="text" name="price" class="model_txtfld" required>
</div>
</div>
</div>

<div class="detail_form_con">
<div class="detail_form_out">
<div class="model_name_con">V.I.N</div>
<div class="model_txt_out"><input name="vin" type="text" class="model_txtfld" ></div>
</div>
<div class="detail_form_out">
<div class="model_name_con">Notes</div>
<div class="model_txt_out"><textarea name="notes" cols="" rows="" class="model_txtarea" required></textarea></div>
</div>
</div>
<div class="details_form_btn">
<input type="submit" value="Send Quote" name="send" class="wholesale_btn">
</div>


</div>
                
                
		
        </div>
          
    </div><!-- end grideightgrey-->
    
       <div class="grideightgrey">
        <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;"></p>
        <div class="grideight" style="width: 600px; margin-top:-5px; margin-bottom: 0px;">
            
                
    </div>
            </div>
    </div>
    
   
        <style>
            
             td, th {
        padding: 3px !important;
    }
    .assessment_insidetd{
        background: none repeat scroll 0% 0% gray; 
        color: white;
    }
    select, textarea, input[type='text'], input[type='password'] {
  border: 1px solid #aaaaaa;
  background: #fff;
  resize: none;
  color: #252525;
  cursor: text;
  padding: 3px;
  font-family: Arial, Helvetica, Sans Serif;
  font-size: 13px;
  width: 78%;
  
  .model_txtfld
{
background-color: #f9f9f9;
border-color: #C3C5C9;
box-shadow: 0 4px 7px #ebeced inset;
font-family: Arial, Helvetica, sans-serif;
font-weight: 300;
height: auto;
padding: 10px 3%;
border:solid 1px #C3C5C9;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
color:#787878;
width:94%;
margin: 0 0 7px;
font-size:13px;
}
}
    </style>
<?php require("teaser.php"); ?>
</div><!--end content-->

          <?php }
                                        }
				?>

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
