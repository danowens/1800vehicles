<?php require("globals.php"); ?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<style>
	.chat-container iframe {
		width: 100%;
		height: 400px;
	}
	#chat-sup-modal .modal-title img {
		width: 290px;
	}
	.dashboardimage {
		width: 100%;
		border: 1px solid #000;
		border-radius: 5px;		
		height: auto;
		margin-bottom: 30px;
		background-color: #ffffff;
		cursor: pointer;
	}
</style>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Support</h1>
        <div class="grideightgrey">
            
                <div class="col-xs-4 col-sm-3 placeholder dashboard-icon support-customer">                   
					<a href="javascript:void(0);" class="contact-by-phone-pop-up">
                        <img class="dashboardimage contact_phone" class="img-responsive"  alt="" src="images/contactusphone.png">
					</a>
                </div>  
                <div class="col-xs-4 col-sm-3 placeholder dashboard-icon support-customer">
					<a href="javascript:void(0);" class="chat-form-pop-up">
                        <img class="dashboardimage chat_support" class="img-responsive"  alt="" src="images/instantchat.png">
                     </a>					
                </div> 
                <div class="col-xs-4 col-sm-3 placeholder dashboard-icon support-customer">
                    <a href="javascript:void(0);" class="contact-form-pop-up">
                        <img class="dashboardimage contact_email" class="img-responsive"  alt="" src="images/throughemail.png">
                     </a>
                </div>				
        </div>
    </div><!-- grid eight container -->
    
<?php require("teaser.php"); ?>
</div><!--end content-->
<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
<script>
	jQuery(document).ready( function() {	
				
	});
</script>
