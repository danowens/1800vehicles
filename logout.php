<?php require("globals.php"); ?>
<?php
    unset($_SESSION['user']);
    unset($username);
    unset($_SESSION['firstname']);
    unset($_SESSION['lastname']);
    unset($_SESSION['userid']);
    unset($_SESSION['marketneedid']);
    $state = $_SESSION['state'];
    $substate = $_SESSION['substate'];

    //session_destroy();

    switch($state)
    {
        case 0:
            switch($substate)
            {
                case 1:
                    header('Location: faq.php');
                    break;
                case 2:
                    header('Location: testimony.php');
                    break;
                case 3:
                    header('Location: finance.php');
                    break;
                case 4:
                    header('Location: franchise.php');
                    break;
                case 5:
                    header('Location: help.php');
                    break;
                default:
                    header('Location: index.php');
                    break;
            }
            break;
        case 1:
            switch($substate)
            {
                case 0:
                    header('Location: learntheprocess.php');
                    break;
                case 1:
                    header('Location: researchvehicles.php');
                    break;
                case 2:
                    header('Location: researchspecific.php');
                    break;
                case 3:
                    header('Location: researchprice.php');
                    break;
                case 4:
                    header('Location: excellent.php');
                    break;
                default:
                    header('Location: index.php');
                    break;
            }
            break;
        default:
            header('Location: index.php');
            break;
    }
?>
