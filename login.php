<?php require("globals.php");?>
<?php
    require_once(WEB_ROOT_PATH."common/functions/usernavfunctions.php"); 
    require_once(WEB_ROOT_PATH."common/functions/globalfunctions.php");
    require_once(WEB_ROOT_PATH."common/functions/DB.php");
    require_once(WEB_ROOT_PATH."common/functions/User.php");
    
    unset($_SESSION['user']);
    $_SESSION['loginfailed'] = false;
	$input = $_POST;
	$input['user'] = filter_var($input['user'], FILTER_UNSAFE_RAW);
	$input['pass'] = filter_var($input['pass'], FILTER_UNSAFE_RAW);

    //echo "Inputs <pre>" . print_r($input, true) . "</pre>\n";
    if(!isset($input["user"]) || !isset($input["pass"]) || (strlen($input["user"]) < 1) || (strlen($input["pass"]) < 1))
    {
        $_SESSION['loginfailed'] = true;
    } else {
        $db   = DB::init();
        $user = User::getUserByLogin($db, $input["user"]);
        //echo "user <pre>" . print_r($user, true) . "</pre>\n";

        if ($user["Password"] == md5($input["pass"])) {
            $_SESSION['user']        = $input["user"];
            $_SESSION['userid']      = $user['UserID'];
            $_SESSION['firstname']   = $user['FirstName'];
            $_SESSION['lastname']    = $user['LastName'];
            $_SESSION['loginfailed'] = false;
        } else {
            $_SESSION['loginfailed'] = true;
        }

        if(!$_SESSION['loginfailed']) {
            // if the user is not a customer, they won't have a market need
            if(!isset($user['profiles'][User::PROFILE_CUSTOMER])) {
                $_SESSION['marketneedid'] = -1;
            } else {
                /* The code below seems needlessly complex and I think all it really does it set the last time the user
                 * logged in for the current market need. It does also email the admins if the user logged in for the first
                 * time in more than 8 weeks, which could be useful.
                 * If we re-enable the code below, we'll need to move it to MySQLi.
				*/ 
                $salesrep = getsalesrep($_SESSION['userid']);
                //$_SESSION['ShowError'] = 'In Customer Login, Salesrep = '.$salesrep.'\n';

                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con){
                    mysql_select_db(DB_SERVER_DATABASE, $con);

                    $query = "select MarketNeedID from marketneeds where Active = 1 and ShowToUser = 1 and UserID = ".$_SESSION['userid'];
                    $result = mysql_query($query);
                    if($result && $row = mysql_fetch_array($result))
                    {
                        //$_SESSION['ShowError'] .= 'MarketNeedID = '.$row[0].'\n';
                        $query = "select max(LastLogin) 'LastLogin' from marketneeds where Active = 1 and ShowToUser = 1 and UserID = ".$_SESSION['userid'];
                        $result = mysql_query($query);
                        if($result && $row = mysql_fetch_array($result))
                        {
                            $lastlogin = $row['LastLogin'];
                            //$_SESSION['ShowError'] .= 'MaxLogin = '.$row[0].'\n';

                            // We start a new Market Need if they have not logged in for more than 8 weeks...
                            if(($lastlogin == '0000-00-00 00:00:00') || (abs(time_diff(date_at_timezone('Y-m-d H:i:s','EST'), $lastlogin)) > abs(60*60*24*7*16))) // 16 weeks
                            {
                                //$_SESSION['ShowError'] .= 'In Old Login'.'\n';
                                // First inactivate all the others...
                                $oquery = "select MarketNeedID from marketneeds where Active = 1 and ShowToUser = 1 and UserID = ".$_SESSION['userid'];
                                $oresult = mysql_query($oquery);
                                while($oresult && $orow = mysql_fetch_array($oresult))
                                {
                                    $uquery = "update marketneeds set Active = 0 where marketneedid = ".$orow[0];
                                    mysql_query($uquery, $con);
                                }

                                // Now add the new one...
                                $query = "insert into marketneeds (UserID,Created,LastLogin,Title) values (".$_SESSION['userid'].",'".date_at_timezone('Y-m-d H:i:s','EST')."','".date_at_timezone('Y-m-d H:i:s','EST')."','Vehicle Purchase')";
                                if(mysql_query($query, $con))
                                {
                                    // If they had a salesrep, then keep that rep by default...
                                    if($salesrep != -1)
                                    {
                                        $lastid = mysql_insert_id($con);
                                        $query = "insert into assignedreps (MarketNeedID, UserRepID, StartDate) values (".$lastid.",".$salesrep.",'".date_at_timezone('Y-m-d H:i:s','EST')."')";
                                        mysql_query($query, $con);
                                    }
                                }

                                $query = "select marketneedid from marketneeds where SeenBySales = 127 and Active = 1 and ShowToUser = 1 and UserID = ".$_SESSION['userid'];
                                $result = mysql_query($query);
                                if($result && $row = mysql_fetch_array($result))
                                {
                                    $uquery = "update marketneeds set SeenBySales = 0 where marketneedid = ".$row[0];
                                    mysql_query($uquery, $con);
                                }
                                //$_SESSION['ShowError'] .= 'SeenBySales Updated'.'\n';

                                $uequery = "select u.firstname,u.lastname,ul.useemail,u.email,ul.login from users u, userlogin ul where ul.userid = u.userid and u.userid = ".$_SESSION['userid'];
                                $ueresult = mysql_query($uequery);
                                if($ueresult && $uerow = mysql_fetch_array($ueresult))
                                {
                                    //$_SESSION['ShowError'] .= 'About to Send Email'.'\n';
                                    $newusername = $uerow[0].' '.$uerow[1];
                                    $upquery = "select un.phonenumber,nt.typename from users u, usernumbers un, numbertypes nt where nt.numbertypeid=un.numbertypeid and un.displayorder = 1 and un.userid = u.userid and u.userid = ".$_SESSION['userid'];
                                    $upresult = mysql_query($upquery);
                                    if($upresult && $uprow = mysql_fetch_array($upresult))
                                    {
                                        $newuserphone = '('.substr($uprow[0],0,3).') '.substr($uprow[0],3,3).'-'.substr($uprow[0],6);
                                        $newuserphone .= ' ['.$uprow[1].']';
                                    }
                                    else $newuserphone = 'Unknown';
                                    if($uerow[2] == 0) $newuserlogin = $uerow[4];
                                    else $newuserlogin = $uerow[3];
                                    $newuseremail = $uerow[3];
                                    $uaquery = "select ua.zip from users u, useraddresses ua where ua.userid = u.userid and ua.isprimary = 1 and u.userid = ".$_SESSION['userid'];
                                    $uaresult = mysql_query($uaquery);
                                    if($uaresult && $uarow = mysql_fetch_array($uaresult)) $newuserzip = $uarow[0];
                                    else $newuserzip = '00000';

                                    $emailbody = 'A user has returned: '.$newusername.'<br/>';
                                    $emailbody .= 'Primary Login: '.$newuserlogin.'<br/>';
                                    $emailbody .= 'Primary Email: '.$newuseremail.'<br/>Primary Phone: '.$newuserphone.'<br/>Zip Code: '.$newuserzip.'<br/>';
                                    $emailbody .= 'They have started a new customer experience since they had not logged in in the last 8 weeks or were an original customer.<br/>';
                                    sendtoadmins($con, $emailbody, '1800vehicles.com User Returned');
                                    //$_SESSION['ShowError'] .= 'Email Sent = '.$emailbody.'\n';
                                }
                            }
                            else
                            {
                                //$_SESSION['ShowError'] .= 'Not in old login'.'\n';
                                $query = "select marketneedid from marketneeds where SeenBySales = 127 and Active = 1 and ShowToUser = 1 and UserID = ".$_SESSION['userid'];
                                $result = mysql_query($query);
                                if($result && $row = mysql_fetch_array($result))
                                {
                                    $uquery = "update marketneeds set SeenBySales = 0 where marketneedid = ".$row[0];
                                    mysql_query($uquery, $con);
                                }
                            }
                        }

                        $query = "select max(MarketNeedID) from marketneeds where Active = 1 and ShowToUser = 1 and UserID = ".$_SESSION['userid'];
                        $result = mysql_query($query);
                        if($result && $row = mysql_fetch_array($result))
                        {
                            $_SESSION['marketneedid'] = $row[0];
                            $marketneedid = $_SESSION['marketneedid'];

                            // Now we set the Last Login for this Market Need to now...
                            $uquery = "update marketneeds set LastLogin = '".date_at_timezone('Y-m-d H:i:s','EST')."' where marketneedid = ".$_SESSION['marketneedid'];
                            mysql_query($uquery, $con);
                        }
                    }
                    else
                    {
                        $query = "insert into marketneeds (UserID,Created,LastLogin,Title) values (".$_SESSION['userid'].",'".date_at_timezone('Y-m-d H:i:s','EST')."','".date_at_timezone('Y-m-d H:i:s','EST')."','New Vehicle Purchase')";
                        if(mysql_query($query, $con))
                        {
                            $lastid = mysql_insert_id($con);
                            $query = "insert into assignedreps (MarketNeedID, UserRepID, StartDate) values (".$lastid.",".$salesrep.",'".date_at_timezone('Y-m-d H:i:s','EST')."')";
                            mysql_query($query, $con);
                        }

                        $query = "select max(MarketNeedID) 'marketneedid' from marketneeds where Active = 1 and ShowToUser = 1 and UserID = ".$_SESSION['userid'];
                        $result = mysql_query($query);
                        if($result && $row = mysql_fetch_array($result))
                        {
                            $_SESSION['marketneedid'] = $row['marketneedid'];
                            $marketneedid = $_SESSION['marketneedid'];
                        }
                    }
                    mysql_close($con);
                }
            }
        }
    }
    if(isset($_SESSION['user']))
    {
        if(isset($_SESSION['laststate']) && isset($_SESSION['lastsubstate']))
        {
            $_SESSION['state'] = $_SESSION['laststate'];
            $_SESSION['substate'] = $_SESSION['lastsubstate']; 
            unset($_SESSION['laststate']);
            unset($_SESSION['lastsubstate']);
            $state = $_SESSION['state'];
            $substate = $_SESSION['substate'];
            
        }
        else
        {
            header('Location: dashboard.php');
            exit();
        }
        
        switch($state)
        {
            case 0:
                switch($substate)
                {
                    case 1:
                        header('Location: faq.php');
                        break;
                    case 2:
                        header('Location: testimony.php');
                        break;
                    case 3:
                        header('Location: finance.php');
                        break;
                    case 4:
                        header('Location: franchise.php');
                        break;
                    case 5:
                        header('Location: help.php');
                        break;
                    
                     case 7:
                        header('Location: register.php');
                        break;
                     case 11:
                        header('Location: assessment.php');
                        break;
                    case 111:
                        header('Location: assessmentsave.php');
                        break;
                    
                    case 25:
                        header('Location: add_searchplan.php');
                        break;
                    case 255:
                        header('Location: searchplansave.php');
                        break;
                    case 24:
                        header('Location: add_pas.php');
                        break;
                    case 124:
                        header('Location: posplansave.php');
                        break;
                     
                    case 12:
                        header('Location: consult.php');
                        break;
                    case 122:
                        header('Location: consultsave.php');
                        break;
                     case 100:
                        header('Location: dashboard.php');
                        break;
                  
                     case 103:
                        header('Location: get_a_search_started.php');
                        break; 
                    
                    case 1056:
                        header('Location: requesttradeinquote.php');
                        break; 
                    default:
                        header('Location: index.php');
                        break;
                }
                break;
            case 1:
                switch($substate)
                {
                    case 0:
                        header('Location: learntheprocess.php');
                        break;
                    case 1:
                        header('Location: researchvehicles.php');
                        break;
                    case 2:
                        header('Location: researchspecific.php');
                        break;
                    case 3:
                        header('Location: researchprice.php');
                        break;
                    case 4:
                        header('Location: excellent.php');
                        break;
                    case 10:
                        header('Location: favorites.php');
                        break;
                    case 11:
                        header('Location: assessment.php');
                        break;
                    case 12:
                        header('Location: consult.php');
                        break;
                    case 13:
                        header('Location: requestquote.php');
                        break;
                    case 21:
                        header('Location: viewagreements.php');
                        break;
                    case 22:
                        header('Location: searchplan.php');
                        break;
                    case 23:
                        header('Location: pas.php');
                        break;
                     case 24:
                        header('Location: add_pas.php');
                        break;
                    case 25:
                        header('Location: add_searchplan.php');
                        break;
                    case 26:
                        header('Location: consider_and_approve_video.php');
                        break;
                     case 27:
                        header('Location: inspect_and_takedelivery_video.php');
                        break;
                    case 99:
                        header('Location: mydashboard.php');
                        break;
                    case 101:
                        header('Location: inspect_and_takedelivery.php');
                        break;
                    case 102:
                        header('Location: consider_and_approve.php');
                        break;
                             
                    default:
                        header('Location: index.php');
                        break;
                }
                break;
            case 2:
                switch($substate)
                {
                    case 0:
                        header('Location: allfirmquotes.php');
                        break;
                    case 1:
                        header('Location: requesttradeinquote.php');
                        break;
                    case 2:
                        header('Location: placeorder.php');
                        break;
                    case 5:
                        header('Location: posttogroup.php');
                        break;
                    case 99:
                        header('Location: mydashboard.php');
                        break;
                    default:
                        header('Location: index.php');
                        break;
                }
                break;
            case 3:
                switch($substate)
                {
                    case 0:
                        header('Location: orderdetails.php');
                        break;
                    case 1:
                        header('Location: watchlist.php');
                        break;
                    case 2:
                        header('Location: svqlist.php');
                        break;
                    case 4:
                        header('Location: searchrequest.php');
                        break;
                    case 99:
                        header('Location: mydashboard.php');
                        break;
                    default:
                        header('Location: index.php');
                        break;
                }
                break;
            case 4:
                switch($substate)
                {
                    case 0:
                        header('Location: vehicle.php');
                        break;
                    case 1:
                        header('Location: vehicleactionplan.php');
                        break;
                    case 2:
                        header('Location: vehicledelivery.php');
                        break;
                    case 3:
                        header('Location: progressrequest.php');
                        break;
                    case 99:
                        header('Location: mydashboard.php');
                        break;
                    default:
                        header('Location: index.php');
                        break;
                }
                break;
            case 5:
                switch($substate)
                {
                    case 0:
                        header('Location: groupwatch.php');
                        break;
                    case 1:
                        header('Location: mywholesale.php');
                        break;
                    case 3:
                        header('Location: allwholesale.php');
                        break;
                    default:
                        header('Location: mydashboard.php');
                        break;
                }
                break;
            default:
                header('Location: mydashboard.php');
                break;
        }
    }
    else
    {
        $state = $_SESSION['state'];
        $substate = $_SESSION['substate'];
        switch($state)
        {
            case 0:
                switch($substate)
                {
                    case 1:
                        header('Location: faq.php');
                        break;
                    case 2:
                        header('Location: testimony.php');
                        break;
                    case 3:
                        header('Location: finance.php');
                        break;
                    case 4:
                        header('Location: franchise.php');
                        break;
                    case 5:
                        header('Location: help.php');
                        break;
                    default:
                        header('Location: my_login.php');
                        break;
                }
                break;
            case 1:
                switch($substate)
                {
                    case 0:
                        header('Location: learntheprocess.php');
                        break;
                    case 1:
                        header('Location: researchvehicles.php');
                        break;
                    case 2:
                        header('Location: researchspecific.php');
                        break;
                    case 3:
                        header('Location: researchprice.php');
                        break;
                    case 4:
                        header('Location: excellent.php');
                        break;
                    default:
                        header('Location: index.php');
                        break;
                }
                break;
            default:
                header('Location: my_login.php');
                break;
        }
    }
?>
