<?php
    require_once('common/functions/imagefunctions.php');

    //Get image name from URL
    if(isset($_REQUEST['image']))
    {
        $image = $_REQUEST['image'];
        if(isset($_REQUEST['mwidth'])) $mwidth = $_REQUEST['mwidth'];
        if(isset($_REQUEST['mheight'])) $mheight = $_REQUEST['mheight'];

        if(!isset($mwidth) || !isset($mheight))
        {
            list($mwidth, $mheight, $image_type) = getimagesize($image);
        }

        ScaledImageFile($image, $mwidth, $mheight, NULL, 'false', 'true');
    }
?>
