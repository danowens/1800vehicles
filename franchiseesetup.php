<?php
    require_once("globals.php");
//error_reporting(E_ALL);
//	ini_set("display_errors", "on");
    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 2;
    $_SESSION['titleadd'] = 'Franchise Setup';
    $_SESSION['robots'] = 'noindex, nofollow';

    require_once("checkaccess.php");
    require_once("zipcodedatafunctions.php");

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // $query = "select FranchiseeID,Name,Description,NDA1,NDA1Time,NDA2,NDA2Time,Type,Level,Active,Standing,AutoAssign from Franchisees order by Name";
        $query = "select FranchiseeID,Name,Description from franchisees order by Name";
        $result = mysql_query($query, $con);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $fid[$index] = $row[0];
            $fname[$index] = $row[1];
            $fdesc[$index] = $row[2];

            $index++;
        }

        if(isset($_REQUEST['Franchisee']))
        {
            $curfranid = $_REQUEST['Franchisee'];
        }
        elseif($index > 0)
        {
            $curfranid = $fid[0];
        }

        for ($i=0; $i<count($fname); $i++)
        {
            if ($fid[$i] == $curfranid) $curfranname = $fname[$i];
        }
        $_SESSION['titleadd'] .= ' - '.$curfranname;

        if(isset($curfranid))
        {
            // Franchisees
            $query = "select FranchiseeID,Name,Description,NDA1,NDA1Time,NDA2,NDA2Time,Type,Level,Active,Standing,AutoAssign from franchisees where FranchiseeID=".$curfranid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $ifid = $row[0];
                $ifname = $row[1];
                $ifdesc = $row[2];
                $ifnda1 = $row[3];
                $ifnda1d = $row[4];
                $ifnda2 = $row[5];
                $ifnda2d = $row[6];
                $iftype = $row[7];
                $iflevel = $row[8];
                $ifactive = $row[9];
                $ifstanding = $row[10];
                $ifautoassign = $row[11];
            }

            // create zip code data
            // TODO make a button at some point
            if (0)
            {
                // DO RARELY Create and populate zipcodedata table
                dropzipcodedatadb($con);
                createzipcodedatadb($con);
                $countzips = fillzipcodedatadb($con);
            }
            else
                $countzips = countzipcodedatadb($con);

            // Existing Zips
            $query = "select f.ZIP, z.City, z.State, z.ZipcodeType, z.Lat, z.Lon, z.EstimatedPopulation, z.TotalWages from franchiseetozip f, zipcodedata z where f.ZIP=z.Zipcode and f.FranchiseeID=".$curfranid;
            $result = mysql_query($query, $con);
            $index = 0;
            $franpop = 0;
            $franwag = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $ifzips[$index] = $row[0];
                $ifzipcity[$index] = $row[1];
                $ifzipstate[$index] = $row[2];
                $ifziptype[$index] = $row[3];
                $ifziplat[$index] = $row[4];
                $ifziplon[$index] = $row[5];
                $ifzippop[$index] = $row[6];
                $franpop += $ifzippop[$index];
                $ifzipwag[$index] = $row[7] / 1000000;
                $franwag += $ifzipwag[$index];
                $index++;
            }

            // add on zipcodes (that aren't in the zipcode database) with stub data
            $query = "select f.ZIP from franchiseetozip f left outer join zipcodedata z on f.ZIP=z.Zipcode where f.FranchiseeID=".$curfranid." and z.Zipcode is null";
            $result = mysql_query($query, $con);
            //$index = 0;  //continuing on
            while($result && $row = mysql_fetch_array($result))
            {
                $ifzips[$index] = $row[0];
                $ifzipcity[$index] = "NODATA";
                $ifzipstate[$index] = "NA";
                $ifziptype[$index] = "FAKE";
                $ifziplat[$index] = -90;
                $ifziplon[$index] = -90;
                $ifzippop[$index] = 0;
                $ifzipwag[$index] = 0;
                $index++;
            }

            // nearby zips

            // use first zip unless starting zip from url is a valid zip for this franchise
            $startzip = $ifzips[0];
            if(isset($_REQUEST['NearZip']))
            {
                for ($i=0; $i < count($ifzips); $i++)
                {
                    if ($_REQUEST['NearZip'] == $ifzips[$i])
                        $startzip = $_REQUEST['NearZip'];
                }
            }

            $range = 5;
            $result = nearbyzipcodes($con, $startzip, $range);
            $index=0;
            while($result && $row = mysql_fetch_array($result))
            {
                $nbzips[$index] = $row[0];
                $nbcity[$index] = $row[1];
                $nbstate[$index] = $row[2];
                $nbtype[$index] = $row[3];
                $nblat[$index] = $row[4];
                $nblon[$index] = $row[5];
                $nbzippop[$index] = $row[6];
                $nbzipwag[$index] = $row[7] / 1000000;
                $nbexists[$index] = 0;
                for ($i=0; $i < count($ifzips); $i++)
                {
                    if ($row[0] == $ifzips[$i]) $nbexists[$index]=1;
                }
                $index++;
            }

            // Existing Customers for the current franchise
            if(count($ifzips) > 0)
            {
                if (count($ifzips) > 2)
                {
                    $ziplist = implode(",", $ifzips);
                    $zipquery = ' and a.ZIP in ('.$ziplist.') ';
                } else {
                    $zipquery = ' ';
                }

                // Get the Customers in these zips...
                $query = "select u.UserID, max(m.MarketNeedID), concat(u.FirstName,' ',u.LastName,' (',u.Email,')') AS FullName, a.ZIP, max(m.LastLogin), count(m.Active), count(m.MarketNeedID) from users u, useraddresses a, marketneeds m where m.UserID=u.UserID and u.UserID=a.UserID and a.IsPrimary=1 ".$zipquery." group by m.UserID order by max(m.LastLogin) desc";
                $result = mysql_query($query, $con);
                $index = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    $icustid[$index] = $row[0];
                    $icmnid[$index] = $row[1];
                    $icname[$index] = $row[2];
                    $iczip[$index]  = $row[3];
                    $iclastlogin[$index] = $row[4];
                    $icactive[$index] = $row[5];
                    $ictotal[$index] = $row[6];

                    // TODO replace with outer join
                    $mquery = "select a.AssignedRepID, a.UserRepID, a.StartDate, a.EndDate, concat(u.FirstName,' ',u.LastName,' (',u.Email,')') AS FullName from assignedreps a, users u where u.userID = a.UserRepID and MarketNeedID=".$icmnid[$index]." order by a.StartDate desc";
                    $mresult = mysql_query($mquery, $con);
                    if($mrow = mysql_fetch_array($mresult))
                    {
                        $icarepid[$index] = $mrow[0];
                        $icurepid[$index] = $mrow[1];
                        $icstart[$index]  = $mrow[2];
                        $icend[$index]    = $mrow[3];
                        $icurepname[$index]    = $mrow[4];
                    } else {
                        $icarepid[$index] = null;
                        $icurepid[$index] = null;
                        $icstart[$index]  = null;
                        $icend[$index]    = null;
                    }
                    $index++;
                }
            }

            // Profile Names
            $iprofilename[1] = "Customer";
            $iprofilename[2] = "Administrator";
            $iprofilename[3] = "Territory&nbsp;Administrator";
            $iprofilename[4] = "Franchisee";
            $iprofilename[5] = "Operations&nbsp;Manager";
            $iprofilename[6] = "General&nbsp;Manager";
            $iprofilename[7] = "Authorized&nbsp;Buyer";
            $iprofilename[8] = "Sales&nbsp;Representative";

            // Members
            $query = "select m.FranchiseeMemberID, m.UserID, concat(u.LastName,', ',u.FirstName,'<br/>(',u.Email,')') AS FullName, m.AbleToControl, group_concat(up.profileid)";
            $query .= " from franchiseemembers m, users u, userprofiles up";
            $query .= " where u.UserId=m.UserID and up.userid=u.UserId and m.FranchiseeID=".$curfranid." group by up.userid order by up.profileid, FullName";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $ifmemberid[$index] = $row[0];
                $ifuserid[$index] = $row[1];
                $iffull[$index] = $row[2];
                $ifcontrol[$index] = $row[3];
                $ifroles[$index] = $row[4];

                // convert to names
                $aroles = explode(',',$ifroles[$index]);
                sort($aroles);

                $count = count($aroles);
                for ($i=0; $i<$count; $i++)
                {
                    $aroles[$i] = $iprofilename[$aroles[$i]];
                }
                if ($count > 0)
                    $ifroles[$index] = implode(', ', $aroles);

                $index++;
            }

            // prospective members
            // (users with some admin like profiles - show which franchisee they are in)
            // TODO: subtract members already in any franchisee or let them be pulled out of the other one and placed here
            $query = "select u.userid, u.LastName, u.FirstName, u.Email, max(userprofiles.profileid)"; //, fm.FranchiseeID, f.Name";
            $query .= " from  users u"; //,FranchiseeMembers fm, Franchisees f";
            $query .= " left outer join userprofiles on userprofiles.userid = u.userid";
            $query .= " where userprofiles.profileid > 1";//fm.UserID = u.UserId and (f.FranchiseeID = fm.FranchiseeID OR f.franchiseeID = 'NULL')";
            $query .= " group by u.userid order by u.LastName";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $iuuserid[$index] = $row[0];
                $iufullname[$index] = $row[1].', '.$row[2];
                $iuemail[$index] = $row[3];
                $iumaxprofile[$index] = $row[4]; //$iprofilename[intval($row[4])];
                $iufranchiseeid[$index] = $row[5];
                $iufranchiseename[$index] = $row[6];
                $index++;
            }

        }
/*
-- Table structure for table `RestrictToZIP`

DROP TABLE IF EXISTS `RestrictToZIP`;
CREATE TABLE IF NOT EXISTS `RestrictToZIP` (
  `FranchiseeMemberID` int(10) unsigned NOT NULL,
  `ZIP` varchar(5) NOT NULL,
  PRIMARY KEY  (`FranchiseeMemberID`,`ZIP`)
) ENGINE=MyISAM;
*/

        mysql_close($con);
    }
?>
<?php require_once(WEB_ROOT_PATH."headerstart.php"); ?>
<script language=JavaScript>
    function franchanged()
    {
        var vfran = document.getElementById("franlist");
        self.location='FranchiseeSetup.php?Franchisee=' + vfran.options[vfran.selectedIndex].value + '#fran';
    }

    function zipchanged()
    {
        var vfran = document.getElementById("franlist");
        var vzip  = document.getElementById("nearzip");
        self.location='FranchiseeSetup.php?Franchisee=' + vfran.options[vfran.selectedIndex].value + '&NearZip=' + vzip.options[vzip.selectedIndex].value + '#nearzip';
    }

    function zipformvalid()
    {
        var vzip = document.getElementById("ZIP");
        if(vzip.value.length < 5)
        {
            alert("Zip code must be 5 digits!"+'\n');
            return false;
        }
        return true;
    }

    function memberformvalid()
    {
        var vmember = document.getElementById("MemberID");
        if(vmember.value.length < 1)
        {
            alert("Member ID is required!"+'\n');
            return false;
        }
        return true;
    }

    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }

    function confirmadd()
    {
        var r=confirm("Are you sure you want to Add a Franchise?")
        if(r==true) return true;
        else return false;
    }

</script>
<?php require_once(WEB_ROOT_PATH."header.php"); ?>
<?php require_once(WEB_ROOT_PATH."foursteps.php"); ?>
<?php require_once(WEB_ROOT_PATH."headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 220px;">Franchisee Setup</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: -5px;">
                <p class="blackeleven" style="margin: 0pt;"><a href="mydashboard.php#admintab">Go back to My Dashboard</a></p>
                <br clear="all" />
<?php
    echo '<a name="fran"><h4 class="subhead">Franchisees</h4></a><br/>';
    echo '<form action="addfranchisee.php" method="post" onsubmit="javascript:return confirmadd();">';
    echo '<input type="hidden" value="pleasedo" name="runok" />';
    echo '<button type="submit" value="" class="med">ADD FRANCHISEE</button>';
    echo '</form><br/>';

    echo '<table><tr><td>';
    echo '<label for="userlist"><strong>Franchisee:</strong> </label>';
    echo '<select name="franlist" id="franlist" width="75" onchange="javascript:franchanged()">';
    $count = count($fid);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$fid[$i].'" ';
        if(isset($curfranid) && ($curfranid == $fid[$i])) echo 'selected="selected"';
        echo '>'.$fname[$i].' ('.$fdesc[$i].')</option>';
    }
    echo '</select></td>';
    if(isset($ifid))
    {
        echo '<td><form action="addfranchisee.php" method="post">';
        echo '<input type="hidden" value="pleasedo" name="runok" />';
        echo '<input type="hidden" value="'.$ifid.'" name="FranchiseeID" />';
        echo '<input type="hidden" value="'.$ifname.'" name="Name" />';
        echo '<input type="hidden" value="'.$ifdesc.'" name="Desc" />';
        echo '<input type="hidden" value="'.$ifnda1.'" name="NDA1" />';
        echo '<input type="hidden" value="'.$ifnda1d.'" name="NDA1D" />';
        echo '<input type="hidden" value="'.$ifnda2.'" name="NDA2" />';
        echo '<input type="hidden" value="'.$ifnda2d.'" name="NDA2D" />';
        echo '<input type="hidden" value="'.$iftype.'" name="Type" />';
        echo '<input type="hidden" value="'.$iflevel.'" name="Level" />';
        echo '<input type="hidden" value="'.$ifactive.'" name="Standing" />';
        echo '<input type="hidden" value="'.$ifstanding.'" name="Active" />';
        echo '<input type="hidden" value="'.$ifautoassign.'" name="AutoAssign" />';
        echo '<button type="submit" value="" class="med">EDIT</button>';
        echo '</form></td><td width="75"/>';
        echo '<td><form action="deletefran.php" method="post">';
        echo '<input type="hidden" value="'.$ifid.'" name="FranchiseeID" />';
        echo '<input type="hidden" value="pleasedo" name="runok" />';
        echo '<button type="submit" disabled value="" class="med">DELETE</button><br/>';
        echo '</form>';
    }
    echo '</td></tr><tr><td colspan="4" align="right">';
    echo 'Note: Delete will clean a Franchise out COMPLETELY...<br/>there will be no traces or history left after the cascading delete!';
    echo '</td></tr></table>';

    echo '<h4 class="subhead">Franchise Summary</h4><br/>';
    echo '<b>Total Zips:</b> '.count($ifzips).'<br/>';
    echo '<b>Total People:</b> '.number_format($franpop).' <br/>';
    echo '<b>Total Wages:</b> $'.number_format($franwag).' Million<br/>';
    echo '<br/>';
    echo '<b>Total Sales (30 days):</b> 0 (0)<br/>';

    if(isset($ifid))
    {
        echo '<div>';

        // Zip Codes
        // Members...
        echo '<h4 class="subhead">Members</h4>';

        // member list
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center"><b>Member</b></td><td align="center"><b>Controller?</b></td><td align="center" width="200"><b>Roles</b></td><td align="center"><b>Actions</b></td></tr>';
        if(isset($ifmemberid)) $count = count($ifmemberid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="left">'.$iffull[$i].'</td>';
            if($ifcontrol[$i] == 1) echo '<td align="left">Yes</td>';
            else echo '<td align="left">No</td>';

            echo '<td width="200">'.$ifroles[$i].'</td>';
            echo '<td align="left"><form action="delmember.php" method="post">';
            echo '<input type="hidden" value="'.$ifid.'" name="FranchiseeID" />';
            echo '<input type="hidden" value="'.$ifuserid[$i].'" name="MemberID" />';
            echo '<input type="hidden" value="pleasedo" name="runok" />';
            echo '<button type="submit" value="" class="med">X</button>';
            echo '&nbsp;|&nbsp;<a href="UserFunctions.php?User='.$ifuserid[$i].'">Edit</a></td>';
            echo '</form>';
            echo '</tr>';
        }
        echo '</table><br/>';

        // add a member - but show if already in a franchise and only show members with > customer roles.
        echo '<form action="addmember.php" method="post" onsubmit="javascript:return memberformvalid();">';
        echo '<strong>Add Members: </strong>';
        echo '<input type="hidden" value="'.$ifid.'" name="FranchiseeID" />';
        //echo '<input type="text" value="" name="MemberID" id="MemberID" />';
        echo '<select name="MemberID" id="MemberID" >';
        $count = count($iuuserid);
        for($i = 0; $i < $count; $i++)
        {
            if ($iumaxprofile[$i] > 1)
            {
                echo '<option value="'.$iuuserid[$i].'" >'.$iufullname[$i].' ('.$iuemail[$i].')</option>';
                //echo '<option value="'.$iuuserid[$i].'" >'.$iufullname[$i].' ('.$iuemail[$i].') as '.$iumaxprofile[$i].' in '.$iufranchiseename[$i].' ('.$iufranchiseeid[$i].')</option>';
            }
        }
        echo '</select>';
        echo '<br/><input name="Control" id="Control" type="checkbox" value="1" ';
        echo '/>Controls Franchise&nbsp;';
        echo '<input type="hidden" value="pleasedo" name="runok" />';
        echo '<button type="submit" value="" class="med">ADD MEMBER</button>';
        echo '</form><br/>';

        // zip codes
        echo '<a name="zip"><h4 class="subhead">Zip Codes</h4></a><br/>';

        // add zip code button
        echo '<form action="addzip.php" method="post" onsubmit="javascript:return zipformvalid();">';
        echo '<input type="hidden" value="'.$ifid.'" name="FranchiseeID" />';
        echo '<input type="text" value="" name="ZIP" id="ZIP" size=5 maxlength=5 onkeypress="return numbersonly(event)" />';
        echo '<input type="hidden" value="pleasedo" name="runok" />';
        echo '<button type="submit" value="" class="med">ADD ZIP</button>';
        echo '<a href="http://www.zipmap.net" target="_blank"> Map of County and Zipcodes</a>';
        echo '</form><br/>';

        // existing zip codes in dense 3 across rows with city and state
        echo '<table cellpadding="5" cellspacing="5">';
        if(isset($ifzips)) $count = count($ifzips);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            if ($i == 0) echo '<tr>';
            if ((($i) % 3) == 0) echo '</tr><tr>';
            echo '<td align="right"><b>'.$ifzips[$i].'</b><br/>'.$ifzipcity[$i].', '.$ifzipstate[$i].'<br/>('.$ifziptype[$i].')<br/>('.number_format($ifzippop[$i]).' | $'.number_format($ifzipwag[$i]).'M)';
            //echo ' ['.number_format($ifziplat[$i],2).','.number_format($ifziplon[$i],2).']';
            echo '</td><td align="left"><form action="delzip.php" method="post">';
            echo '<input type="hidden" value="'.$ifid.'" name="FranchiseeID" />';
            echo '<input type="hidden" value="'.$ifzips[$i].'" name="ZIP" />';
            echo '<input type="hidden" value="pleasedo" name="runok" />';
            echo '<button type="submit" value="" class="med">X</button>';
            echo '</form></td>';
        }
        echo '</tr></table>';

        if ($count > 0)
        {
            // first zip code expanded with nearby zip codes
            echo '<a href="nearzip"><h4 class="subhead">Zip Codes Near '.$startzip.'</h4></a><br/>';

            // nearby zip codes in dense 3 across rows with city and state
            echo '<table cellpadding="5" cellspacing="5">';

            echo count($nbzips).' zip codes found within '.$range.' miles of '; //'.$startzip.'<br/>';
            echo '<select name="zip" id="nearzip" onchange="javascript:zipchanged()">';

            $count = count($ifzips);
            for($i = 0; $i < $count; $i++)
            {
                echo '<option value="'.$ifzips[$i].'"';
                if ($startzip == $ifzips[$i]) echo ' selected="selected" ';
                echo '>'.$ifzips[$i].'</option>';
            }

            echo '</select>';

            $count = count($nbzips);
            for($i=0;$i<$count;$i++)
            {
                if ($i == 0) echo '<tr>';
                if ((($i) % 3) == 0) echo '</tr><tr>';
                echo '<td align="right"><b>'.$nbzips[$i].'</b><br/>'.$nbcity[$i].', '.$nbstate[$i].'<br/>('.$nbtype[$i].')<br/>('.number_format($nbzippop[$i]).' | $'.number_format($nbzipwag[$i]).'M)';;
                //echo ' ['.number_format($nblat[$i],2).','.number_format($nblon[$i],2).']';
                echo '</td><td align="left">';
                echo '<form action="addzip.php" method="post">';
                echo '<input type="hidden" value="'.$ifid.'" name="FranchiseeID" />';
                echo '<input type="hidden" value="'.$startzip.'" name="NearZip" />';
                echo '<input type="hidden" value="'.$nbzips[$i].'" name="ZIP" id="ZIP" />';
                echo '<input type="hidden" value="pleasedo" name="runok" />';
                if ($nbexists[$i]==0) echo '<button type="submit" value="" class="med">+</button>';
                else echo '[&#10003;]';
                echo '</form><br/>';
            }
            echo '</tr></table>';
        }

        // existing customers in this franchises
        echo '<a href="customers"><h4 class="subhead">Customers</h4></a><br/>';
        echo '<table cellpadding="5" cellspacing="5">';
        if(isset($icustid)) $count = count($icustid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '</tr><tr>';
            echo '<td align="left"><b>'.$icname[$i].'</b><br/>';
            $parts = explode(' ',$iclastlogin[$i]);
            $date = $parts[0];
            echo '<b>last login: </b>'.$date.' <span style="color: rgb(133, 193, 27);">('.timesincenow($iclastlogin[$i]).')</span><br/>';
            echo '<b>count: </b>'.$ictotal[$i].' | ';
            if ($icactive[$i] > 0) echo '<span style="color: rgb(133, 193, 27);"><b>active: </b></span>'.$icactive[$i].' | ';
            else echo '<b>active: </b>'.$icactive[$i].'<br/>';
            echo '<b>mnid: </b>'.$icmnid[$i].' | ';
            $parts = explode(' ',$icstart[$i]);
            $date = $parts[0];
            echo '<b>start: </b>'.$date.'<br/>';
            echo '<b>zip: </b>'.$iczip[$i].'<br/>';
            echo '<b>rep: </b>'.$icurepname[$i].' ('.$icurepid[$i].')';
            echo '</td><td align="left">';
            // using _blank for now until the main group is figured out and franchise setup is fast again.
            echo '<a target="_blank" href="salesrepactions.php?Switching=1&ForUserID='.$icustid[$i].'&Franchisee='.$curfranid.'&MarketNeedID='.$icmnid[$i].'#mnid;">View</a>';
            echo '</tr>';
        }
        echo '</tr></table>';
    }
    echo '<br/>';
?>
            </div><!-- grid eight -->
        </div><!-- grid eight grey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require_once(WEB_ROOT_PATH."footerstart.php"); ?>
<?php require_once(WEB_ROOT_PATH."footer.php"); ?>
<?php require_once(WEB_ROOT_PATH."footerend.php"); ?>
