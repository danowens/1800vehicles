<?php require("globals.php"); ?>
<?php
$_SESSION['state'] = 0;
$_SESSION['substate'] = 122;
$_SESSION['titleadd'] = 'Consultation';

if (!isset($_SESSION['user'])) {
    $_SESSION['consult_post'] = $_POST;
    $_SESSION['laststate'] = $_SESSION['state'];
    $_SESSION['lastsubstate'] = $_SESSION['substate'];
    echo "<script>window.location='register.php';</script>";
} else {

// See what was passed in...
    if ($_SESSION['consult_post']) {
        $notetext = $_SESSION['consult_post']['notetext'];
        $repcall = $_SESSION['consult_post']["repcall"];
        if(isset($_SESSION['consult_post']["conatact_type"]))
        {
            $contact_type=  implode(",", $_SESSION['consult_post']["conatact_type"]);
        }else{
            $contact_type="N/A";
        }
        unset($_SESSION['consult_post']);
    } else if ($_POST) {
        $notetext = $_POST['notetext'];
        $repcall = $_POST["repcall"];
        if(isset($_POST["conatact_type"]))
        {
            $contact_type=  implode(",", $_POST["conatact_type"]);
        }else{
            $contact_type="N/A";
        }
    }
    
    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];
    $srep = getsalesrep($userid, $marketneedid);

    $errorinsave = 'false';

//    if (!isset($notetext) && !isset($repcall))
//        $errorinsave = 'Not all fields were supplied!';

    if ($errorinsave == 'false') {
        $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
        if ($con) {
            mysql_select_db(DB_SERVER_DATABASE, $con);

// save note
            if (isset($notetext)) {
// See if they already have one...
                $query = "select * from consultations where MarketNeedID = " . $marketneedid;
                $result = mysql_query($query, $con);
                if (!$result || !$row = mysql_fetch_array($result)) {
// There is not one already saved, so add it...
                    $query = "insert into consultations (MarketNeedID,contact_type, Created, LastUpdated, SeenBySales, Notes) values (" . $marketneedid . ",'".$contact_type."',";
                    $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','" . date_at_timezone('Y-m-d H:i:s', 'EST') . "',0,";
                    if (isset($notetext) && (strlen($notetext) > 0))
                        $query .= "'" . escapestr($notetext) . "'";
                    else
                        $query .= "NULL";
                    $query .= ")";
                    
                    if (!mysql_query($query, $con))
                        $errorinsave = 'Could not add Consultation';

                    if ($errorinsave == 'false') {
// Add a Message Update when this happens...
                        posttodashboard($con, $userid, $userid, 'completed a <a href="/consult.php">Request for Consultation</a>.', $marketneedid);

                        if ($srep != -1) {
// alert salesman to this consult request on dashboard and in email.
                            posttodashboard($con, $userid, $srep, '<a href="' . WEB_SERVER_NAME . 'salesrepactions.php?ForUserID=' . $userid . '&MarketNeedID=' . $marketneedid . '">' . $firstname . ' ' . $lastname . '</a> added a Consultation Request.');

                            $message = 'Your customer has Requested a Consultation...<br/>';
                            $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '<br/>';
                            $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '<br/>';
                            $message .= "&nbsp;&nbsp;Mode: &nbsp;&nbsp;&nbsp;&nbsp" . htmlifystr(escapestr($contact_type)) . "<br/>";
                            $message .= '&nbsp;&nbsp;Request: &nbsp;&nbsp;&nbsp;' . escapestr($notetext) . '<br/>';
                            sendemail(getuseremailnodb($con, $srep), 'Consultation Request Received!', $message, 'true');

// send the customer a confirmation email
                            $message = 'Your consultation request has been received.<br/>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, your sales representative will discuss your request with you soon.';
                        } else {
// TODO: can this go to the general manager's dashboad in this case?
// send the customer a confirmation email
                            $message = 'Your consultation request has been received.<br/>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, a sales representative will be assigned to you soon to discuss your request.';
                        }

                        sendemail(getuseremailnodb($con, $userid), 'Consultation Request Received', $message, 'false');

//admin email alert
                        $message = 'A customer has Requested a Consultation...<br/>';
                        $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '<br/>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '<br/>';
                        $message .= "&nbsp;&nbsp;Mode: &nbsp;&nbsp;&nbsp;&nbsp" . htmlifystr(escapestr($contact_type)) . "<br/>";
                        $message .= '&nbsp;&nbsp;Request: &nbsp;&nbsp;&nbsp;' . escapestr($notetext) . '<br/>';
                        sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                    } else {
// TODO: could send the admins a consult page is broken alert
                    }
                } else {
// There is already one saved, so update it...
                    $query = "update consultations set LastUpdated = '" . date_at_timezone('Y-m-d H:i:s', 'EST') . "'";
                    $query .= ", SeenBySales = 0";
                    if (isset($contact_type))
                        $query .= ", contact_type = '" . escapestr($contact_type) . "'";
                    else
                        $query .= ", contact_type = N/A";
                    if (isset($notetext))
                        $query .= ", Notes = '" . escapestr($notetext) . "'";
                    else
                        $query .= ", Notes = NULL";
                    $query .= " where MarketNeedID = " . $marketneedid;
                    
                  //  echo'<pre>';print_r($query);echo'</pre>';die('here');
                    if (!mysql_query($query, $con))
                        $errorinsave = 'Could not update Assessment';

                    if ($errorinsave == 'false') {
// Add a Message Update when this happens...
                        posttodashboard($con, $userid, $userid, 'updated <a href="/consult.php">Request for Consultation</a> information.', $marketneedid);

                        if ($srep != -1) {
// Add a Message when this happens...
                            posttodashboard($con, $userid, $srep, '<a href="' . WEB_SERVER_NAME . 'salesrepactions.php?ForUserID=' . $userid . '&MarketNeedID=' . $marketneedid . '">' . $firstname . ' ' . $lastname . '</a> updated their Consultation Request.');

                            $emailmsg = "<html><body>Your customer has Updated their Consultation Request...<br/>";
                            $emailmsg .= "&nbsp;&nbsp;Customer: " . getuserfullnamenodb($con, $userid, "false") . "<br/>";
                            $emailmsg .= "&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;" . getuserphonenodb($con, $userid) . "<br/>";
                            $emailmsg .= "&nbsp;&nbsp;Mode: &nbsp;&nbsp;&nbsp;&nbsp" . htmlifystr(escapestr($contact_type)) . "<br/>";
                            $emailmsg .= '&nbsp;&nbsp;Request: (below)<br/><br/>' . htmlifystr(escapestr($notetext)) . '<br/></body></html>';
                           
                            sendemail(getuseremailnodb($con, $srep), "Consultation Request Update Received!", $emailmsg, "true");

                            $emailmsg = 'Your consultation request update has been received.<br/>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, your sales representative will discuss your request with you soon.';
                        } else {
// if no salesman - need to send to franchisee / general manager
                            $emailmsg = 'Your consultation request update has been received.<br/>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, a sales representative will be assigned to you soon to discuss your request.';
                        }

// this is really going to corporate
                        sendemail(getuseremailnodb($con, $userid), 'Consultation Request Update Received', $emailmsg, 'false');

                        $emailmsg = '<html><body>A customer has Updated their Consultation Request...<br/>';
                        $emailmsg .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '<br/>';
                        $emailmsg .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '<br/>';
                        $emailmsg .= "&nbsp;&nbsp;Mode: &nbsp;&nbsp;&nbsp;&nbsp" . htmlifystr(escapestr($contact_type)) . "<br/>";
                        $emailmsg .= '&nbsp;&nbsp;Request: (below)<br/><br/>' . htmlifystr(escapestr($notetext)) . '<br/></body></html>';
                        
                        sendtoadmins($con, $emailmsg, '1800vehicles.com Admin Alert - Consultation Request Update', 'true');
                    } else {
// TODO: could send the admins a consult page is broken alert
                    }
                }
            }

            if (isset($repcall)) {
                if ($errorinsave == 'false') {
                    $query = "update marketneeds set NeedsContact=1";
                    $query .= " where MarketNeedID=" . $marketneedid;
                    if (!mysql_query($query, $con))
                        $errorinsave = 'Could not set the immediate call back.';
                }
            }
            else {
                if ($errorinsave == 'false') {
                    $query = "update marketneeds set NeedsContact=0";
                    $query .= " where MarketNeedID=" . $marketneedid;
                    if (!mysql_query($query, $con))
                        $errorinsave = 'Could not set the immediate call back.';
                }
            }

            mysql_close($con);
        } else
            $errorinsave = 'Could not connect to the database';
    }
    ?>
    <?php require("headerstart.php"); ?>
    <?php require("header.php"); ?>
    <?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
    <div id="content">
        <div class="grideightcontainer">
    <?php
    if ($errorinsave != 'false') {
        echo '<h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Request Issue!</h1>';
    } else {
        echo '<h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Request Complete!</h1>';
    }
    ?>
            <div class="grideightgrey">
                <div class="grideight">
    <?php
    if ($errorinsave != 'false') {
        echo '<p class="blacktwelve">Sorry!  There was an error processing your request.</p>';
        echo '<p class="blacktwelve">Please contact a representative at 1-800-vehicles (834-4253) for help with this issue.</p>';
        if ($errorinsave != 'true')
            echo $errorinsave . '<br/>';
        echo '<p class="blacktwelve">Use your browser <a href="javascript:history.back()">back</a> button or click <a href="/consult.php">here</a>.</p>';
    }
    else {
        echo '<p class="blacktwelve">Thank you for your consultation request. Your 1-800-vehicles.com representative will be in touch soon.</p>';
        echo '<p class="blacktwelve"><a href="/dashboard.php">Back to My Dashboard.</a></p>';
    }
    ?>
                </div><!-- end greyeight-->
            </div><!-- grid eight container -->
        </div><!-- end grideightgrey-->
    <?php require("teaser.php"); ?>
    </div><!--end content-->

                    <?php require("footerstart.php"); ?>
                    <?php require("footer.php"); ?>
                    <?php
                    require("footerend.php");
                }
                ?>
