<?php
    require_once("globals.php");

    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 3;
    $_SESSION['titleadd'] = 'User Functions';
    $_SESSION['robots'] = 'noindex, nofollow';

    require_once("checkaccess.php");

    // Get the list of customers in the system...
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the current User Information...
        $query = "select userid, concat(LastName,', ',FirstName) AS fullname, Email from users order by LastName";
        $result = mysql_query($query, $con);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $iuserid[$index] = $row[0];
            $ifullname[$index] = $row[1];
            $iemail[$index] = $row[2];
            $index++;
        }

        if(isset($_REQUEST['User']))
        {
            $curuserid = $_REQUEST['User'];
        }
        elseif($index > 0)
        {
            $curuserid = $iuserid[0];
        }

        if(isset($curuserid))
        {

            // Get the current User Information...
            $query = "select userid, concat(LastName,', ',FirstName) AS fullname, Email from users where userid=".$curuserid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $icuruserid = $row[0];
                $icurfullname = $row[1];
                $icuremail = $row[2];
            }
            $_SESSION['titleadd'] .= ' - '.$icurfullname;

            // Profiles
            $query = "select profileid from userprofiles where userid=".$curuserid.' order by profileid';
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $iprofile[$index] = $row[0];
                $index++;
            }

            // Market Needs
            $query = "select marketneedid, title, showtouser, needscontact, active, completed, seenbysales from marketneeds where userid=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $imarketneedid[$index] = $row[0];
                $ititle[$index] = $row[1];
                $ishowtouser[$index] = $row[2];
                $ineedscontact[$index] = $row[3];
                $iactive[$index] = $row[4];
                $icompleted[$index] = $row[5];
                $iseenbysales[$index] = $row[6];
                $index++;
            }

            if($index > 0)
            {
                // MarketNeeds Sub Tables...
                $marketidlist = implode(",",$imarketneedid);

                // AssignedReps
                $query = "select AssignedRepID, UserRepID, StartDate, EndDate from assignedreps where MarketNeedID in (".$marketidlist.")";
                $result = mysql_query($query, $con);
                $index = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    $imassid[$index] = $row[0];
                    $imassuser[$index] = $row[1];
                    $imassstart[$index] = $row[2];
                    $imassend[$index] = $row[3];
                    $index++;
                }

                // MarketUpdates
                //$query = "select MarketUpdateID, Created, UpdateText from marketupdates where MarketNeedID in (".$marketidlist.")";
                //$result = mysql_query($query, $con);
                //$index = 0;
                //while($result && $row = mysql_fetch_array($result))
                //{
                //    $imupdateid[$index] = $row[0];
                //    $imupdatedate[$index] = $row[1];
                //    $imupdatetext[$index] = $row[2];
                //    $index++;
                //}

                // Assessments
                $query = "select AssessmentID, Created, LastUpdated, SeenBySales, MileageGoal, BudgetCeiling, NeedType, MostlyDrive, KeepFor, CareGiven, Particular,";
                $query .= "HowSoon, Origin, Economy, Luxury, Sportiness, Comfort ,FuelEfficiency ,Roominess, Safety, Quietness, Dependability, Security, ForProfessionalUse,";
                $query .= "StatusLevel, AdditionalInfo from assessments where MarketNeedID in (".$marketidlist.")";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $imaid = $row[0];
                    $imadate = $row[1];
                    $imaupdated = $row[2];
                    $imaseen = $row[3];
                    $imamiles = $row[4];
                    $imabudget = $row[5];
                    $imaneed = $row[6];
                    $imamostly = $row[7];
                    $imakeep = $row[8];
                    $imacare = $row[9];
                    $imapart = $row[10];
                    $imasoon = $row[11];
                    $imaorigin = $row[12];
                    $imaeconomy = $row[13];
                    $imalux = $row[14];
                    $imasport = $row[15];
                    $imacom = $row[16];
                    $imafuel = $row[17];
                    $imaroom = $row[18];
                    $imasafe = $row[19];
                    $imaquiet = $row[20];
                    $imadepend = $row[21];
                    $imasecure = $row[22];
                    $imaprof = $row[23];
                    $imastatus = $row[24];
                    $imainfo = $row[25];
                    $index++;
                }

                // Consultations
                $query = "select ConsultationID, Created, LastUpdated, SeenBySales, Notes from consultations where MarketNeedID in (".$marketidlist.")";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $imconid = $row[0];
                    $imcondate = $row[1];
                    $imconupdated = $row[2];
                    $imconseen = $row[3];
                    $imconnote = $row[4];
                    $index++;
                }

                // Favorites
                $query = "select FavoriteID, VehicleID, Created, LastUpdated, Information, Visible, HiddenOn from favorites where MarketNeedID in (".$marketidlist.")";
                $result = mysql_query($query, $con);
                $index = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    $imfavid[$index] = $row[0];
                    $imfavvehid[$index] = $row[1];
                    $imfavdate[$index] = $row[2];
                    $imfavupdated[$index] = $row[3];
                    $imfavinfo[$index] = $row[4];
                    $imfavvis[$index] = $row[5];
                    $imfavhide[$index] = $row[6];
                    $index++;
                }

                // TradeIns
                $query = "select TradeInID, VehicleDetailID, PriceWanted, PriceOffered, PriceAccepted, Available from tradeins where MarketNeedID in (".$marketidlist.")";
                $result = mysql_query($query, $con);
                $index = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    $imtrdid[$index] = $row[0];
                    $imtrdvehid[$index] = $row[1];
                    $imtrdwant[$index] = $row[2];
                    $imtrdoffer[$index] = $row[3];
                    $imtrdaccept[$index] = $row[4];
                    $imtrdavail[$index] = $row[5];
                    $index++;
                }
            }

/*
            // QuoteRequests
  `QuoteRequestID` int(10) unsigned NOT NULL auto_increment,
  `MarketNeedID` int(10) unsigned NOT NULL,
  `QuoteType` enum('Standard','Special','Pickup') NOT NULL default 'Standard',
  `Year` year(4) NOT NULL,
  `Make` varchar(50) NOT NULL,
  `Model` varchar(30) NOT NULL,
  `Style` varchar(25) NOT NULL,
  `Created` datetime NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `MileageCeiling` mediumint(9) NOT NULL default '-1',
  `EngineCylinders` tinyint(4) NOT NULL default '-1',
  `Transmission` enum('Flexible','Automatic','Manual') NOT NULL default 'Flexible',
  `CabType` enum('Flexible','Hatchback','Regular','Extended') NOT NULL default 'Flexible',
  `FrontSeatType` enum('Flexible','Bench','Standard','Bucket') NOT NULL default 'Flexible',
  `BedType` enum('Flexible','Mini','Short','Long') NOT NULL default 'Flexible',
  `SeatMaterial` enum('Flexible','Cloth','Leather') NOT NULL default 'Flexible',
  `WheelDriveType` enum('Flexible','Rear','Front','Four') NOT NULL default 'Flexible',
  `WheelDrive` enum('Flexible','2','4') NOT NULL default 'Flexible',
  `WheelCovers` enum('Flexible','Alloy','Chrome','Hubcaps') NOT NULL default 'Flexible',
  `Sunroof` enum('Flexible','None','Glass','Metal','Moon') NOT NULL default 'Flexible',
  `RadioNeeded` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `RadioFM` tinyint(1) NOT NULL default '0',
  `RadioCassette` tinyint(1) NOT NULL default '0',
  `RadioCD` tinyint(1) NOT NULL default '0',
  `RadioSat` tinyint(1) NOT NULL default '0',
  `PowerDoors` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `PowerWindows` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `PowerSeats` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `HeatedSeats` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `AirConditioning` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `RemoteEntry` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `TractionControl` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `SecuritySystem` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `CruiseControl` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `Navigation` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `RearSlidingWindow` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `BedLiner` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `TowPackage` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `LuggageRack` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `Convertible` enum('Flexible','No','Yes') NOT NULL default 'Flexible',
  `Doors` tinyint(4) NOT NULL default '2',
  `ColorCombination` text,
  `SpecialRequests` text,
  `Pickup` tinyint(1) NOT NULL default '0',
  `DeliverToCity` varchar(50) default NULL,
  `DeliverToState` varchar(2) default NULL,
  `Visible` tinyint(1) NOT NULL default '1',
  `VehicleID` int(10) unsigned default NULL,
            $query = "select TradeInID, VehicleDetailID, PriceWanted, PriceOffered, PriceAccepted, Available from quoterequests where MarketNeedID in (".$marketidlist.")";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $imtrdid[$index] = $row[0];
                $imtrdvehid[$index] = $row[1];
                $imtrdwant[$index] = $row[2];
                $imtrdoffer[$index] = $row[3];
                $imtrdaccept[$index] = $row[4];
                $imtrdavail[$index] = $row[5];
                $index++;
            }
CREATE TABLE IF NOT EXISTS `FirmQuotes` (
  `FirmQuoteID` int(10) unsigned NOT NULL auto_increment,
  `QuoteRequestID` int(10) unsigned NOT NULL,
  `SalesRepID` int(10) unsigned default NULL,
  `Created` datetime NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `OrderType` enum('Unknown','Standard','Special','Group') NOT NULL default 'Unknown',
  `MileageLow` mediumint(9) default NULL,
  `MileageHigh` mediumint(9) default NULL,
  `PricePoint` decimal(6,2) default NULL,
  `UpdateStatus` enum('Researching','Completed') NOT NULL default 'Researching',
  `StatusDate` datetime default NULL,
  `ImageFile` varchar(200) default NULL,
  `AdminNote` text default NULL,
CREATE TABLE IF NOT EXISTS `Orders` (
  `OrderID` int(10) unsigned NOT NULL auto_increment,
  `FirmQuoteID` int(10) unsigned NOT NULL,
  `WaitPeriod` int(4) NOT NULL default '0',
  `Expires` datetime default NULL,
  `Accepted` datetime NOT NULL default '0000-00-00 00:00:00',
  `DepositAmount` decimal(6,2) NOT NULL default '250.00',
  `DepositReceived` decimal(6,2) NOT NULL default '0.00',
  `SecondKeyLimit` decimal(5,2) NOT NULL default '75.00',
  `SellerRepairLimit` decimal(6,2) NOT NULL default '400.00',
  `PurchaserRepairLimit` decimal(6,2) NOT NULL default '300.00',
  `NavDiscLimit` decimal(5,2) NOT NULL default '75.00',
CREATE TABLE IF NOT EXISTS `Watches` (
  `WatchID` int(10) unsigned NOT NULL auto_increment,
  `FirmQuoteID` int(10) unsigned NOT NULL,
  `Accepted` datetime NOT NULL default '0000-00-00 00:00:00',
  `Expires` datetime default NULL,
  `DepositAmount` decimal(6,2) NOT NULL default '0.00',
  `DepositReceived` decimal(6,2) NOT NULL default '0.00',
  `SecondKeyLimit` decimal(5,2) NOT NULL default '75.00',
  `SellerRepairLimit` decimal(6,2) NOT NULL default '400.00',
  `PurchaserRepairLimit` decimal(6,2) NOT NULL default '300.00',
  `NavDiscLimit` decimal(5,2) NOT NULL default '75.00',

            // SpecificVehicles
CREATE TABLE IF NOT EXISTS `SpecificVehicles` (
  `SpecificVehicleID` int(10) unsigned NOT NULL auto_increment,
  `MarketNeedID` int(10) unsigned NOT NULL,
  `VehicleDetailID` int(10) unsigned NOT NULL,
  `TagExpirationDate` datetime default NULL,
  `Warranty` text,
            $query = "select TradeInID, VehicleDetailID, PriceWanted, PriceOffered, PriceAccepted, Available from specificvehicles where MarketNeedID in (".$marketidlist.")";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $imtrdid[$index] = $row[0];
                $imtrdvehid[$index] = $row[1];
                $imtrdwant[$index] = $row[2];
                $imtrdoffer[$index] = $row[3];
                $imtrdaccept[$index] = $row[4];
                $imtrdavail[$index] = $row[5];
                $index++;
            }
CREATE TABLE IF NOT EXISTS `SpecificMatches` (
  `SpecificMatchID` int(10) unsigned NOT NULL auto_increment,
  `SpecificVehicleID` int(10) unsigned NOT NULL,
  `MatchingID` int(10) unsigned NOT NULL,
  `Source` enum('Order','Watch','InStock','TradeIn') NOT NULL default 'Order',
CREATE TABLE IF NOT EXISTS `Denials` (
  `DenialID` int(10) unsigned NOT NULL auto_increment,
  `SpecificVehicleID` int(10) unsigned NOT NULL,
  `UserID` int(10) unsigned NOT NULL,
  `Created` datetime NOT NULL,
  `Reason` text NOT NULL,
CREATE TABLE IF NOT EXISTS `Purchases` (
  `PurchaseID` int(10) unsigned NOT NULL auto_increment,
  `SpecificVehicleID` int(10) unsigned NOT NULL,
  `Created` datetime NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `ChosenOn` datetime NOT NULL,
  `BoughtOn` datetime NOT NULL,
  `Satisfaction` text default NULL,
  `SalesReviewed` tinyint(1) NOT NULL default '0',
  `SalesRepID` int(10) unsigned default NULL,
  `Status` enum('Waiting','Chosen','Bought','Not Bought','Completed') NOT NULL default 'Waiting',
  `SpecialNotes` text default NULL,
  `AskPrice` decimal(7,2) NOT NULL default '0.00',
CREATE TABLE IF NOT EXISTS `SpecificActions` (
  `SpecificActionID` int(10) unsigned NOT NULL auto_increment,
  `ActionPlanID` int(10) unsigned NOT NULL,
  `PurchaseID` int(10) unsigned NOT NULL,
  `UpdateStatus` enum('None Requested','Waiting','Received') NOT NULL default 'None Requested',
  `StartDate` datetime NOT NULL,
  `EndDate` datetime default NULL,
  `Notes` text default NULL,
  PRIMARY KEY  (`SpecificActionID`)
CREATE TABLE IF NOT EXISTS `SpecificItems` (
  `SpecificItemID` int(10) unsigned NOT NULL auto_increment,
  `SpecificActionID` int(10) unsigned NOT NULL,
  `ChecklistID` int(10) unsigned NOT NULL,
  `UpdateStatus` enum('None Requested','Waiting','Received') NOT NULL default 'None Requested',
  `StartDate` datetime NOT NULL,
  `EndDate` datetime default NULL,
  `Needed` tinyint(1) NOT NULL default '1',
  `Verified` tinyint(1) NOT NULL default '1',
  `Completed` tinyint(1) NOT NULL default '0',
  `CompletedDate` datetime default NULL,
  `Amount` decimal(5,2) NOT NULL default '0.00',
CREATE TABLE IF NOT EXISTS `SpecificDelivery` (
  `SpecificDeliveryID` int(10) unsigned NOT NULL auto_increment,
  `PurchaseID` int(10) unsigned NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime default NULL,
  `UpdateStatus` enum('In Route','Waiting','Arrived') NOT NULL default 'Waiting',
  `LatestStatusDate` datetime default NULL,
  `StopName` varchar(75) default NULL,
  `StopNumber` int(5) unsigned NOT NULL default '1',
  `Address1` varchar(150) default NULL,
  `Address2` varchar(150) default NULL,
  `City` varchar(35) default NULL,
  `State` char(2) default NULL,
  `ZIP` varchar(5) NOT NULL,

            $query = "select DenialID,SpecificVehicleID,Created,Reason from denials where userid=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $idenid[$index] = $row[0];
                $idenveh[$index] = $row[1];
                $idendate[$index] = $row[2];
                $idenreason[$index] = $row[3];
                $index++;
            }
*/
            // Messages
            $query = "select MessageID, UserFromID, Message, Created, SendOn, MarketNeedID from messages where UserToID=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $imsgid[$index] = $row[0];
                $imsgfrom[$index] = $row[1];
                $imsgmsg[$index] = $row[2];
                $imsgcreated[$index] = $row[3];
                $imsgsendon[$index] = $row[4];
                $imsgmarket[$index] = $row[5];
                $index++;
            }

            // Franchisee Roles
            $query = "select fm.FranchiseeID, fm.AbleToControl, f.Name from franchiseemembers as fm, franchisees as f where fm.FranchiseeID=f.FranchiseeID and fm.userid=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $ifranchise[$index] = $row[0];
                $icontrol[$index] = $row[1];
                $ifranchisename[$index] = $row[2];
                $index++;
            }

            // Broadcasts
            $query = "select BroadcastID,Created,Message,ImageFile,FranchiseeID from broadcasts where userid=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $ibroadcast[$index] = $row[0];
                $ibroadcreated[$index] = $row[1];
                $ibroadmessage[$index] = $row[2];
                $ibroadimage[$index] = $row[3];
                $ibroadfranchise[$index] = $row[4];
                $index++;
            }

            // User Login
            $query = "select useemail, login from userlogin where userid=".$curuserid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $iuseemail = $row[0];
                $ilogin = $row[1];
            }

            // User Addresses
            $query = "select UserAddressID,IsPrimary,ZIP,Address1,Address2,City,State,StartDate,EndDate from useraddresses where userid=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $iaddressid[$index] = $row[0];
                $iaddprimary[$index] = $row[1];
                $iaddzip[$index] = $row[2];
                $iaddaddr1[$index] = $row[3];
                $iaddaddr2[$index] = $row[4];
                $iaddcity[$index] = $row[5];
                $iaddstate[$index] = $row[6];
                $iaddstart[$index] = $row[7];
                $iaddend[$index] = $row[8];
                $index++;
            }

            // Alternate Emails
            $query = "select AlternateEmailID,AddInfo,Email,EmailToPhone,StartDate,EndDate from alternateemails where userid=".$curuserid." order by DisplayOrder";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $iemailid[$index] = $row[0];
                $iemailtype[$index] = $row[1];
                $iemailaddr[$index] = $row[2];
                $iemaile2p[$index] = $row[3];
                $iemailstart[$index] = $row[4];
                $iemailend[$index] = $row[5];
                $index++;
            }

            // Numbers
            $query = "select n.UserNumberID, t.TypeName, n.PhoneNumber, n.Extension, n.AddInfo, n.Texting, n.StartDate, n.EndDate from usernumbers n, numbertypes t where n.NumberTypeID=t.NumberTypeID and userid=".$curuserid." order by n.DisplayOrder";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $inumid[$index] = $row[0];
                $inumtype[$index] = $row[1];
                $inumnumber[$index] = $row[2];
                $inumext[$index] = $row[3];
                $inuminfo[$index] = $row[4];
                $inumtext[$index] = $row[5];
                $inumstart[$index] = $row[6];
                $inumend[$index] = $row[7];
                $index++;
            }

            // Testimonials
            $query = "select TestimonialID,Message,Place from testimonials where userid=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $itestid[$index] = $row[0];
                $itestmsg[$index] = $row[1];
                $itestplace[$index] = $row[2];
                $index++;
            }

            // Referrals
            $query = "select ReferralID,Name,ContactInfo,PersonalNotes,VehicleNotes,SeenBySales,Quoted,Credited from referrals where userid=".$curuserid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $irefid[$index] = $row[0];
                $irefname[$index] = $row[1];
                $irefcontact[$index] = $row[2];
                $irefpersonal[$index] = $row[3];
                $irefvehicle[$index] = $row[4];
                $irefseen[$index] = $row[5];
                $irefquoted[$index] = $row[6];
                $irefcredit[$index] = $row[7];
                $index++;
            }
        }

        mysql_close($con);
    }
?>
<?php require_once(WEB_ROOT_PATH."headerstart.php"); ?>
<script language=JavaScript>
    function itemchanged()
    {
        var vuser = document.getElementById("userlist");
        self.location='userfunctions.php?User=' + vuser.options[vuser.selectedIndex].value;
    }
    function confirmdelete()
    {
        var r=confirm("Are you sure you want to Delete this user?")
        if(r==true) return true;
        else return false;
    }

</script>
<?php require_once(WEB_ROOT_PATH."header.php"); ?>
<?php require_once(WEB_ROOT_PATH."foursteps.php"); ?>
<?php require_once(WEB_ROOT_PATH."headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 220px;">User Functions</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: -5px;">
                <p class="blackeleven" style="margin: 0pt;"><a href="mydashboard.php#admintab">Go back to My Dashboard</a></p>
                <br clear="all" />
<?php
    echo '<label for="userlist">User: </label>';
    echo '<select name="userlist" id="userlist" width="75" onchange="javascript:itemchanged()">';
    $count = count($iuserid);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$iuserid[$i].'" ';
        if(isset($curuserid) && ($curuserid == $iuserid[$i])) echo 'selected="selected"';
        echo '>'.$ifullname[$i].' ('.$iemail[$i].')</option>';
    }
    echo '</select><br/>';
    echo '<form action="deleteuser.php" method="post" onsubmit="javascript:return confirmdelete();">';
    echo '<input type="hidden" value="'.$curuserid.'" name="DelUser" />';
    echo '<input type="hidden" value="pleasedo" name="runok" />';
    echo '<button type="submit" value="" class="med">DELETE</button> Note: This will clean a User out COMPLETELY...there will be no traces or history left after the cascading delete!';
    echo '</form>';
    echo '</div><br/>';

    if(isset($curuserid))
    {
        echo '<div>';
        echo '<b>UserID:</b> '.$curuserid.' <br/>';
        echo '<b>Name:</b> '.$icurfullname.' <br/>';
        if($iuseemail)
            echo '<b>Login:</b> '.$icuremail.'&nbsp;(using email)';
        else
            echo '<b>Login:</b> '.$ilogin;
        echo '<br/></div><br/><br/>';

        // Addresses...
        echo '<h4 class="subhead">Addresses</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Primary?</td><td align="center">Address 1</td><td align="center">Address 2</td><td align="center">City</td><td align="center">State</td><td align="center">ZIP</td><td align="center">Starting</td><td align="center">Ending</td></tr>';
        if(isset($iaddressid)) $count = count($iaddressid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$iaddressid[$i].'</td>';
            echo '<td align="center">'.$iaddprimary[$i].'</td>';
            echo '<td>'.$iaddaddr1[$i].'</td>';
            echo '<td>'.$iaddaddr2[$i].'</td>';
            echo '<td>'.$iaddcity[$i].'</td>';
            echo '<td>'.$iaddstate[$i].'</td>';
            echo '<td>'.$iaddzip[$i].'</td>';
            echo '<td align="center">'.$iaddstart[$i].'</td>';
            echo '<td align="center">'.$iaddend[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Emails...
        echo '<h4 class="subhead">Other Emails</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Type</td><td align="center">Address</td><td align="center">Email to Phone?</td><td align="center">Starting</td><td align="center">Ending</td></tr>';
        if(isset($iemailid)) $count = count($iemailid);
        else $count = 0;
            echo '<tr>';
            echo '<td align="center">Login</td>';
            echo '<td>Primary</td>';
            echo '<td>'.$icuremail.'</td>';
            echo '<td align="center">-</td>';
            echo '<td align="center">-</td>';
            echo '<td align="center">-</td>';
            echo '</tr>';

        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$iemailid[$i].'</td>';
            echo '<td>'.$iemailtype[$i].'</td>';
            echo '<td>'.$iemailaddr[$i].'</td>';
            echo '<td align="center">'.$iemaile2p[$i].'</td>';
            echo '<td align="center">'.$iaddstart[$i].'</td>';
            echo '<td align="center">'.$iaddend[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Phone Numbers...
        echo '<h4 class="subhead">Phone Numbers</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Type</td><td align="center">Number</td><td align="center">Extension</td><td align="center">Info</td><td align="center">Text?</td><td align="center">Starting</td><td align="center">Ending</td></tr>';
        if(isset($inumid)) $count = count($inumid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$inumid[$i].'</td>';
            echo '<td>'.$inumtype[$i].'</td>';
            echo '<td align="center">'.$inumnumber[$i].'</td>';
            echo '<td align="center">'.$inumext[$i].'</td>';
            echo '<td>'.$inuminfo[$i].'</td>';
            echo '<td align="center">'.$inumtext[$i].'</td>';
            echo '<td align="center">'.$inumstart[$i].'</td>';
            echo '<td align="center">'.$inumend[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Profiles...
        $countprof = 8;
        $iprofilename[1] = "Customer";
        $iprofilename[2] = "Administrator";
        $iprofilename[3] = "Territory Administrator";
        $iprofilename[4] = "Franchisee";
        $iprofilename[5] = "Operations Manager";
        $iprofilename[6] = "General Manager";
        $iprofilename[7] = "Researcher";
        $iprofilename[8] = "Sales Representative";

        echo '<a name="profiles"><h4 class="subhead">Profiles</h4></a>';
        echo '<table cellpadding="5" cellspacing="5">';
        if(isset($iprofile)) $count = count($iprofile);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="left">&#10003;'.$iprofilename[$iprofile[$i]].' ('.$iprofile[$i].')</td>';
            echo '</tr>';
        }
        echo '</table><br/>';
        echo '<form action="changeuserprof.php" method="post">';
        echo '<label for="ProfileID">Profile Level: </label>';
        echo '<select name="ProfileID">';
        for($i = 1; $i <= $countprof; $i++)
        {
            echo '<option value="'.$i.'" >';
            echo $iprofilename[$i].' ('.$i.')</option>';
        }
        echo '</select>';
        echo '<input type="hidden" value="'.$curuserid.'" name="User" />';
        echo '<input type="hidden" value="pleasedo" name="runok" />';
        echo '<button type="submit" value="" class="med">ADD</button>';
        echo '</form>';
        echo '<form action="deluserprof.php" method="post">';
        echo '<label for="ProfileID">Profile Level: </label>';
        echo '<select name="ProfileID">';
        if(isset($iprofile)) $count = count($iprofile);
        else $count = 0;
        for($i = 0; $i < $count; $i++)
        {
            echo '<option value="'.$iprofile[$i].'" >';
            echo $iprofilename[$iprofile[$i]].' ('.$iprofile[$i].')</option>';
        }
        echo '</select>';
        echo '<input type="hidden" value="'.$curuserid.'" name="User" />';
        echo '<input type="hidden" value="pleasedo" name="runok" />';
        echo '<button type="submit" value="" class="med">DELETE</button>';
        echo '</form>';
        //echo '<br/>1 = Customer<br/>2 = Administrator<br/>3 = Territory Admin<br/>4 = Franchisee<br/>5 = Operations Manager<br/>6 = General Manager<br/>7 = Researcher<br/>8 = Sales Representative<br/>';
        echo '<br/><br/>';

        // Franchise Member...
        echo '<h4 class="subhead">Member of Franchise</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Name</td><td align="center">Can Control?</td></tr>';
        if(isset($ifranchise)) $count = count($ifranchise);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$ifranchise[$i].'</td>';
            echo '<td align="center">'.$ifranchisename[$i].'</td>';
            echo '<td align="center">'.$icontrol[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Broadcasts...
        echo '<h4 class="subhead">Broadcasts</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Created</td><td align="center">Message</td><td align="center">Image</td><td align="center">Franchise</td></tr>';
        if(isset($ibroadcast)) $count = count($ibroadcast);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$ibroadcast[$i].'</td>';
            echo '<td align="center">'.$ibroadcreated[$i].'</td>';
            echo '<td>'.$ibroadmessage[$i].'</td>';
            echo '<td align="center">'.$ibroadimage[$i].'</td>';
            echo '<td align="center">'.$ibroadfranchise[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Messages...
        echo '<h4 class="subhead">Messages</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">From</td><td align="center">Message</td><td align="center">Created</td><td align="center">Sent On</td><td align="center">MarketNeed</td></tr>';
        if(isset($imsgid)) $count = count($imsgid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$imsgid[$i].'</td>';
            echo '<td align="center">'.$imsgfrom[$i].'</td>';
            echo '<td>'.$imsgmsg[$i].'</td>';
            echo '<td align="center">'.$imsgcreated[$i].'</td>';
            echo '<td align="center">'.$imsgsendon[$i].'</td>';
            echo '<td align="center">'.$imsgmarket[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Testimonials...
        echo '<h4 class="subhead">Testimonials</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Message</td><td align="center">Place</td></tr>';
        if(isset($itestid)) $count = count($itestid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$itestid[$i].'</td>';
            echo '<td>'.$itestmsg[$i].'</td>';
            echo '<td>'.$itestplace[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Referrals...
        echo '<h4 class="subhead">Referrals</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Name</td><td align="center">Contact</td><td align="center">Personal</td><td align="center">Vehicle</td><td align="center">Seen By Sales?</td><td align="center">Quoted?</td><td align="center">Credited?</td></tr>';
        if(isset($irefid)) $count = count($irefid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$irefid[$i].'</td>';
            echo '<td>'.$irefname[$i].'</td>';
            echo '<td'.$irefcontact[$i].'</td>';
            echo '<td'.$irefpersonal[$i].'</td>';
            echo '<td'.$irefvehicle[$i].'</td>';
            echo '<td align="center">'.$irefseen[$i].'</td>';
            echo '<td align="center">'.$irefquoted[$i].'</td>';
            echo '<td align="center">'.$irefcredit[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

        // Market Needs ...
        echo '<h4 class="subhead">Market Needs</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Title</td><td align="center">Show To User?</td><td align="center">Needs Contact?</td><td align="center">Active?</td><td align="center">Completed?</td><td align="center">Seen By Sales?</td></tr>';
        if(isset($imarketneedid)) $count = count($imarketneedid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$imarketneedid[$i].'</td>';
            echo '<td>'.$ititle[$i].'</td>';
            echo '<td align="center">'.$ishowtouser[$i].'</td>';
            echo '<td align="center">'.$ineedscontact[$i].'</td>';
            echo '<td align="center">'.$iactive[$i].'</td>';
            echo '<td align="center">'.$icompleted[$i].'</td>';
            echo '<td align="center">'.$iseenbysales[$i].'</td>';
            echo '</tr>';
        }
        echo '</table><br/><br/>';

/*
            // AssignedReps
            $query = "select AssignedRepID, UserRepID, StartDate, EndDate from assignedreps where MarketNeedID in (".$curuserid.")";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $imassid[$index] = $row[0];
                $imassuser[$index] = $row[1];
                $imassstart[$index] = $row[2];
                $imassend[$index] = $row[3];
                $index++;
            }

            // MarketUpdates
            //$query = "select MarketUpdateID, Created, UpdateText from marketupdates where MarketNeedID in (".$curuserid.")";
            //$result = mysql_query($query, $con);
            //$index = 0;
            //while($result && $row = mysql_fetch_array($result))
            //{
            //    $imupdateid[$index] = $row[0];
            //    $imupdatedate[$index] = $row[1];
            //    $imupdatetext[$index] = $row[2];
            //    $index++;
            //}

            // Assessments
            $query = "select AssessmentID, Created, LastUpdated, SeenBySales, MileageGoal, BudgetCeiling, NeedType, MostlyDrive, KeepFor, CareGiven, Particular,";
            $query .= "HowSoon, Origin, Economy, Luxury, Sportiness, Comfort ,FuelEfficiency ,Roominess, Safety, Quietness, Dependability, Security, ForProfessionalUse,";
            $query .= "StatusLevel, AdditionalInfo from assessments where MarketNeedID in (".$curuserid.")";
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $imaid = $row[0];
                $imadate = $row[1];
                $imaupdated = $row[2];
                $imaseen = $row[3];
                $imamiles = $row[4];
                $imabudget = $row[5];
                $imaneed = $row[6];
                $imamostly = $row[7];
                $imakeep = $row[8];
                $imacare = $row[9];
                $imapart = $row[10];
                $imasoon = $row[11];
                $imaorigin = $row[12];
                $imaeconomy = $row[13];
                $imalux = $row[14];
                $imasport = $row[15];
                $imacom = $row[16];
                $imafuel = $row[17];
                $imaroom = $row[18];
                $imasafe = $row[19];
                $imaquiet = $row[20];
                $imadepend = $row[21];
                $imasecure = $row[22];
                $imaprof = $row[23];
                $imastatus = $row[24];
                $imainfo = $row[25];
                $index++;
            }

            // Consultations
            $query = "select c.ConsultationID, c.Created, c.LastUpdated, c.SeenBySales, c.Notes from consultations c, marketneeds m where c.MarketNeedID = m.MarketNeedID and m.UserID in (".$curuserid.")";
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $imconid = $row[0];
                $imcondate = $row[1];
                $imconupdated = $row[2];
                $imconseen = $row[3];
                $imconnote = $row[4];
                $index++;
            }

            // Favorites
            $query = "select f.FavoriteID, f.VehicleID, f.Created, f.LastUpdated, f.Information, f.Visible, f.HiddenOn from favorites f, marketneeds m where f.MarketNeedID = m.MarketNeedID and m.UserID in (".$curuserid.")";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $imfavid[$index] = $row[0];
                $imfavvehid[$index] = $row[1];
                $imfavdate[$index] = $row[2];
                $imfavupdated[$index] = $row[3];
                $imfavinfo[$index] = $row[4];
                $imfavvis[$index] = $row[5];
                $imfavhide[$index] = $row[6];
                $index++;
            }

            // TradeIns
            $query = "select TradeInID, VehicleDetailID, PriceWanted, PriceOffered, PriceAccepted, Available from tradeins where MarketNeedID in (".$curuserid.")";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $imtrdid[$index] = $row[0];
                $imtrdvehid[$index] = $row[1];
                $imtrdwant[$index] = $row[2];
                $imtrdoffer[$index] = $row[3];
                $imtrdaccept[$index] = $row[4];
                $imtrdavail[$index] = $row[5];
                $index++;
            }
*/

        // Denials...
        echo '<h4 class="subhead">Denials</h4>';
        echo '<table cellpadding="5" cellspacing="5">';
        echo '<tr><td align="center">ID</td><td align="center">Reason</td><td align="center">Date</td><td align="center">Vehicle</td></tr>';
        if(isset($idenid)) $count = count($idenid);
        else $count = 0;
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            echo '<td align="center">'.$idenid[$i].'</td>';
            echo '<td>'.$idenreason[$i].'</td>';
            echo '<td align="center">'.$idendate[$i].'</td>';
            echo '<td align="center">'.$idenveh[$i].'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
?>
            </div><!-- grid eight -->
        </div><!-- grid eight grey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require_once(WEB_ROOT_PATH."footerstart.php"); ?>
<?php require_once(WEB_ROOT_PATH."footer.php"); ?>
<?php require_once(WEB_ROOT_PATH."footerend.php"); ?>
