<?php require("globals.php"); ?>
<?php
$_SESSION['state'] = 1;
$_SESSION['substate'] = 102;
$_SESSION['titleadd'] = 'consider and approve';

$userid = $_SESSION['userid'];
$marketneedid = $_SESSION['marketneedid'];
$con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
if ($con) {
    mysql_select_db(DB_SERVER_DATABASE, $con);

    // Get the current User Information...
    $query = 'select u.firstname, u.lastname, u.created, u.imagefile from users u where u.userid = ' . $userid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $infirst = $row[0];
        $inlast = $row[1];
        $increated = $row[2];
        $inimage = $row[3];
    }

    $chkqueryvehiclespec = "select SearchPlanID from searchplans where MarketNeedID = " . $marketneedid;
    $resultVehicleSpec = mysql_query($chkqueryvehiclespec, $con);
    $countVehicleSpec = mysql_num_rows($resultVehicleSpec);


    $queryMyBestVehicleAssessment = 'SELECT MarketNeedID  FROM `assessments` WHERE `MarketNeedID` =' . $marketneedid;
    $resultMyBestVehicleAssessment = mysql_query($queryMyBestVehicleAssessment, $con);
    $MyBestVehicleAssessment = mysql_num_rows($resultMyBestVehicleAssessment);


    $query_MypriceAvailabilityStudy = "select SearchPlanID from pasplans where MarketNeedID = " . $marketneedid;
    $result_MypriceAvailabilityStudy = mysql_query($query_MypriceAvailabilityStudy, $con);
    $MypriceAvailabilityStudy = mysql_num_rows($result_MypriceAvailabilityStudy);


    $query_MyFavorites = "select FavoriteID from favorites where MarketNeedID =" . $marketneedid;
    $result_MyFavorites = mysql_query($query_MyFavorites, $con);
    $MyFavorites = mysql_num_rows($result_MyFavorites);


    $query_MyVehicleSpecifications = "select SearchPlanID from searchplans where MarketNeedID  = " . $marketneedid;
    $result_MyVehicleSpecifications = mysql_query($query_MyVehicleSpecifications, $con);
    $MyVehicleSpecifications = mysql_num_rows($result_MyVehicleSpecifications);

    $query_MyTradeinQuote = "SELECT MarketNeedID  FROM `tradeinrequests` WHERE `MarketNeedID` =" . $marketneedid;
    $result_MyTradeinQuote = mysql_query($query_MyTradeinQuote, $con);
    $MyTradeinQuote = mysql_num_rows($result_MyTradeinQuote);

    $chkquery = "select SearchPlanID, notes, search_post from searchplans where MarketNeedID = " . $marketneedid;
    $planresult = mysql_query($chkquery, $con);

    $row11 = mysql_fetch_array($planresult);
    $have_search_plan = 0;

    if (isset($row11['SearchPlanID']) && $row11['SearchPlanID'] > 0) {
        if ($row11['search_post'] == 1) {
            $have_search_plan = 1;
        }
    }
}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>


<style>
    table { border: none; }
    td a {
        display: inline-block;
        height:100%;
        width:100%;
    }
    .sub-header {
        padding-bottom: 10px;
        border-bottom: 1px solid #eee;
    }

    .grideightcontainer table a {
        color: #fff;
        font-weight: normal;
    }



    .nav.navbar-nav.navbar-right {
        background-color: transparent;
    }

    #tableother-link a{
        color: #5FB796;
    }

    .dashboardmenu a{
        color: #00000;
    }

    .tdactive {
        background-color: #92d050;
    }
    .header-menu a{
        width: 100%;
        color: #5FB796;
    }
    /*td, th {
      border: solid 1px black;
    }*/


</style>

<script type="text/javascript">

    $(document).ready(function() {
        $("#youtube1").click(function() {
            $.colorbox({href: "consider_and_approve_video.php", scrolling: false, width: "90%"});
        });
         $("#step_2_video").click(function(){
                $.colorbox({href:"step_2_video.php",scrolling:false,width: "90%"});
            });
    });
</script>


<?php
if($have_search_plan==0){
        echo "<script>window.location='page-unavailable.php';</script>";
    }

?>
<!--<div class="gridtwelve"></div>-->
<div id="content" >

    <div class="grideightcontainer"> 
        <div class="col-xs-12 col-sm-12"> 

            <!--<div class="col-xs-6 col-sm-6">
                <img src="images/number.png">
            </div> -->

            <div class="col-xs-12 col-sm-12" style="margin-bottom: 10px;">
<?php
if (isset($_SESSION['user'])) {
    ?>  
                    <!--<div  class="header-menu">              
                        <div style="float: right">
                            <a href="#">My Dashboard <span>|</span></a>
                            <a href="myaccount.php">My Account <span>|</span></a>
                            <a href="logout.php" id="logout">Logout </a>
                        </div> 
                    </div>-->
                <?php } ?>
            </div>
        </div>
        <!--<div class="breaker"></div>
       

       <br>
       <br>-->

        <div class="col-xs-12 col-sm-12 dashboard-title">
            <h3>My Dashboard</h3>
        </div> 

        <!--<div style="width: 100%">              
            <table class="table table-bordered secondmenu-table">
                <thead>
                    <tr>
                        <td align="center" ><a href="dashboard.php">GET HELP WITH WHAT TO BUY</a></td>
                        <td align="center" class="tdactive"><a href="consider_and_approve.php">CONSIDER & APPROVE</a></td>
                        <td align="center"><a href="inspect_and_takedelivery.php">INSPECT & TAKE DELIVERY</a></td>
                    </tr>
                </thead>   
            </table>
        </div>-->


        <div style="width: 100%">              
            <table class="table secondmenu-table">
                <thead>
                    <tr>


                        <td align="center"><a href="dashboard.php">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <span class="fa-stack-lg step-text">1</span>
                                </span>
<?php if ($countVehicleSpec) {
    echo "GET A SEARCH STARTED";
} else {
    echo "GET HELP WITH WHAT TO BUY";
} ?></a></td>



                        <!--<?php if ($_SERVER[REQUEST_URI] != "/dashboard.php") {
    ?>
                            <span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <span class="fa-stack-lg step-text">1</span>
                            </span>-->

                            <!--<span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-lg"></i>
                              <span class="fa-stack-1x step-text">2</span>
                            </span>-->


                            <!--START THE SEARCH PROCESS</a></td>
<?php
} else {
    $_SESSION['ACTIVE_DASHBOARD'] = "dashboard.php";
}
?>  -->      

                        <!--<td align="center"><a href="get_a_search_started.php">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">2</span>
                        </span>
                        GET A SEARCH STARTED</a></td>-->

                        <td align="center" class="tdactive"><a href="consider_and_approve.php">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <span class="fa-stack-lg step-text">2</span>
                                </span>
                                CONSIDER & APPROVE</a></td>


                        <td align="center"><a href="inspect_and_takedelivery.php">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <span class="fa-stack-lg step-text">3</span>
                                </span>
                                INSPECT & TAKE DELIVERY</a></td>


                    </tr>
                </thead>   
            </table>
        </div>


        <!--
         
         <div class="col-xs-12 col-sm-12 dash-step2" style="margin-bottom: 5px;"> 
<?php
if (strlen($inimage) > 0)
    $srctext = $inimage;
else
    $srctext = "userimages/nopicture.jpg";
?>            
             <div class="col-xs-4 col-sm-4">
                 <div style="margin-top: 30px;">
                     My Dashboard
                     </div></div> 
             <div class="col-xs-4 col-sm-4" style="text-align: center;">
                 
               <a <?php if (isset($_SESSION['user'])) { ?> id="youtube1" href="javascript:void(0);"  <?php } else { ?>  href="loginrequired.php"  <?php } ?> >
                     <img src="images/play.png"/>
                 </a>
                 <div class="breaker"></div>
                 About this Step
             </div> 
             <div class="col-xs-4 col-sm-4">
        <?php
        if (isset($_SESSION['user'])) {
            ?>
                    <div style="float: right;"> 
                     <img class="img-responsive" style="border-radius: 14px; margin-top: 5px; padding: 5px; width: 60px;" src="<?= $srctext ?>">
                     <div class="breaker"></div>
                     <span ><?= $infirst . ' ' . $inlast ?></span> 
                    </div>
        <?php } ?>
               
                 </div>
         </div>  -->





        <div class="col-xs-12 col-sm-12 about-this-video dashboard-video" style="margin-bottom: 20px;">
            <a href="#" id="step_2_video"><img src="images/play.png"/> About Step 2</a></br>
        </div>


        <div class="col-xs-12 col-sm-12 user-avatar-container" style="margin-bottom: 20px;">
            <span class="dashboard-title-mobile pull-left">MY DASHBOARD</span>
            <span>
                <?php if (isset($_SESSION['user'])) { ?> 
                    <div class="user-info user-avatar pull-right"> 
                        <img class="user-img img-responsive pull-right" src="<?= $srctext ?>">
                        <span class="pull-right"><?= $infirst . ' ' . $inlast ?></span> 
                    </div>
                <?php } ?>
            </span>
        </div>







        <div style="clear: both;"></div>
        <div class="dashboardmenu">
            <div class="col-sm-12">          
                <div class="row placeholders">                                                
                    <!--<div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                        <a href="#"> <div style=" " class="divcenter">RECENT SPECIFIC VEHICLE PREVIEW</div> </a>
                    </div>-->
                     <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a  id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'dont_have_vehicle_preview.php'; ?>"> <img class="img-responsive"  alt="" src="images/recent_specific_vehicle_preview.png"> </a>
                            </div>
                    
                    <!--<div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                        <a href="#">  <div style=" " class="divcenter">ALL SPECIFIC VEHICLE PREVIEWS</div> </a>
                    </div>-->
                    <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a  id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'dont_have_vehicle_preview.php'; ?>"> <img class="img-responsive"  alt="" src="images/all_specific_vehicle_previews.png"> </a>
                            </div>
                    
                    
                    <!--<div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                        <a href="<?php echo WEB_SERVER_NAME.'mysearchplan.php';?>">  <div style="" class="divcenter">EDIT MY SEARCH PLAN</div> </a>
                    </div> -->
                    <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a  id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'mysearchplan.php'; ?>"> <img class="img-responsive"  alt="" src="images/edit_my_search_plan.png"> </a>
                            </div>
                    
                    <!--<div class="col-xs-4 col-sm-3 placeholder dashboard-icon consider-icon">
                        <a href="#"> <div style=" " class="divcenter">REQUEST A SEARCH UPDATE</div> </a>
                    </div>    -->
                     <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a  id="dashboardimage" class="img-responsive" href="#"> <img class="img-responsive"  alt="" src="images/request_a_search_update.png"> </a>
                            </div>
                    
                    
                    <!--<div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                        <a href="#">   <div style=" " class="divcenter">24*7 CUSTOMER SUPPORT</div> </a>
                    </div> -->
                    <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="support.php">  
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/support.png">
                                </a>
                            </div>
                    
                </div>
            </div>
        </div> 

        <!--<table class="table" id="tableother-link">
            <tr><td colspan="2" style="background-color: #404040;" align="center"><strong style="color: #fff;">VIEW:</strong></td></tr>
            <tr>
                <td class="common_action" align="center">
                    <a href="assessment.php"> My Vehicle Needs Assessment</a>
                </td>
                <td class="common_action" align="center">
                    <a href="#"> Our Best Vehicle Recommendations </a>
                </td>
            </tr>
            <tr>
                <td class="common_action" align="center">
                    <a href="vehiclespec.php"> All Vehicle Specifications & Quotes </a>
                </td>
                <td class="common_action" align="center">
                    <a href="#">  All Specific Vehicle Previews </a>
                </td>
            </tr>
            <tr>
                <td class="common_action" align="center">
                    <a href="mysearchplan.php"> My Search Plan </a>
                </td>
                <td class="common_action" align="center">
                    <a href="requesttradeinquote.php">My Trade In Quote </a>
                </td>
            </tr>            
            <tr>
                <td class="common_action"align="center">
                    <a href="#">My Favorites </a>
                </td>
                <td class="common_action" align="center">
                    <a href="#"> View Our Guarantee</a>
                </td>
            </tr>
            <tr>
                <td class="common_action"align="center">
                     <a href="myaccount.php" id="myaccount">My Account</a>
                </td>
                
            </tr>

           
        </table> -->
        
        
        <div class="row view-dashboard" style="margin-left: 1%;text-align: center; width: 98%;">
                <div class="col-xs-12" style="background-color: #404040;padding: 8px;">
                    <strong style="color: #fff;">VIEW:</strong>
                </div> 
              <?php if($MyBestVehicleAssessment){ ?>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="assessment.php"> My Vehicle Needs Assessment </a>
                </div>
                <?php }?>
             <?php if(1){ ?>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="mybestvehicles.php">Our Best Vehicle Recommendations  </a>
                </div>
                <?php }?>
             <?php if($countVehicleSpec){ ?>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="vehiclespec.php">  All Vehicle Specifications & Quotes</a>
                </div>
                <?php }?>
             <?php if(1){ ?>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href=""> All Specific Vehicle Previews  </a>
                </div>
                <?php }?>
             <?php if($have_search_plan){ ?>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="mysearchplan.php"> My Search Plan </a>
                </div>
                <?php }?>
             <?php if(1){ ?>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="">My Trade In Quote</a>
                </div>
                <?php }?>
             <?php if(1){ ?>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="favorites.php">My Favorites</a>
                </div>
                <?php }?>
            
            
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="viewagreements.php">  View Our Guarantee </a>
                </div>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="myaccount.php" id="myaccount">My Account</a>
                </div> 
      
                
        </div><!--end content-->
    </div>

    <?php require("teaser.php"); ?>
</div><!--end content-->
<?php require("footerstart.php"); ?>

<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>

