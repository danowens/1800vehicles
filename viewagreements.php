<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 21;
    $_SESSION['titleadd'] = 'View Agreements';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Agreements</h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0; margin-left: 0px;"><a href="dashboard.php">Go back to My Dashboard</a></p><br/>
            <div class="grideight" style="margin-top: 0px; color: rgb(117, 117, 117); font-size: 12px;">
                <p style="margin: 0pt;">These are the four agreements used by 1-800-vehicles.com dealers - </p>
                <form action="viewagreement.php" method="post">
                    <input type="hidden" value="Standard Order" name="AgreementName"/>
                    <p style="color: rgb(133, 193, 27); font-size: 13px; margin-bottom: 5px;"><strong>Standard Order Agreement</strong></p>
                    <p style="margin-left: 10px; margin-top: 0pt;">
                        Used when the vehicle you want is in reasonably good wholesale supply and
                        your 1-800-vehicles.com salesperson is confident that it can be found in within your allotted time frame. &nbsp;You
                        agree to work with your salesperson exclusively during the search period (a two week minimum term). &nbsp;The vehicle
                        you want is also posted on our Group Watchlist for all of our dealer franchises to see.
                    </p>
                    <button type="submit" value="" class="med" style="float: right;">VIEW AGREEMENT</button>
                    <br clear="all" /><br />
                </form>
                <form action="viewagreement.php" method="post">
                    <input type="hidden" value="Special Order" name="AgreementName"/>
                    <p style="color: rgb(133, 193, 27); font-size: 13px; margin-bottom: 5px;"><strong>Special Order Agreement</strong></p>
                    <p style="margin-left: 10px; margin-top: 0pt;">
                        Used when the vehicle you want is in not in good wholesale supply but your 1-800-vehicles.com salesperson is confident
                        that it can be found if given enough time.&nbsp; You agree to work with your salesperson exclusively during the search
                        period (a four week minimum term). &nbsp;The vehicle you want is also posted on our Group Watchlist for all of our dealer
                        franchises to see.
                    </p>
                    <button type="submit" value="" class="med" style="float: right;">VIEW AGREEMENT</button>
                    <br clear="all" /><br />
                </form>
                <form action="viewagreement.php" method="post">
                    <input type="hidden" value="Group Watchlist Posting" name="AgreementName"/>
                    <p style="color: rgb(133, 193, 27); font-size: 13px; margin-bottom: 5px;"><strong>Group Watchlist Posting Agreement</strong></p>
                    <p style="margin-left: 10px; margin-top: 0pt;">
                        Used when the vehicle you want is not in good wholesale supply and your 1-800-vehicles.com salesperson is not confident enough
                        to accept it as an order. &nbsp;The vehicle you want is posted and can be seen by all of our dealer franchises. &nbsp;Your
                        salesperson will do his or her best to search the wholesale market on your behalf but there is no exclusive search period.
                    </p>
                    <button type="submit" value="" class="med" style="float: right;">VIEW AGREEMENT</button>
                    <br clear="all" /><br />
                </form>
                <form action="viewagreement.php" method="post">
                    <input type="hidden" value="Specific Vehicle Purchase" name="AgreementName"/>
                    <p style="color: rgb(133, 193, 27); font-size: 13px; margin-bottom: 5px;"><strong>Specific Vehicle Purchase Agreement</strong></p>
                    <p style="margin-left: 10px; margin-top: 0pt;">
                        Used when a specific vehicle has been found that meets your approval. &nbsp;The agreement gives you the final price up front and
                        covers the mechanical and cosmetic condition requirements that must be met upon inspection by you and an independent mechanic.
                    </p>
                    <button type="submit" value="" class="med" style="float: right;">VIEW AGREEMENT</button>
                    <br clear="all" /><br />
                </form>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
