<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 3;
    $_SESSION['substate'] = 2;
    $_SESSION['titleadd'] = 'Specific Vehicle Quotes';

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);
        $query = 'select s.SpecificVehicleID, s.Created, s.LastUpdated, v.Year, m.Name, v.Model, v.Style, s.PriceQuoted, s.SpecificVehicleID from specificvehicles s, vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=s.VehicleDetailID and s.MarketNeedID='.$marketneedid;
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $svqid[$index] = $row[0];
            $svqcreated[$index] = $row[1];
            $svqupdated[$index] = $row[2];
            $svqyear[$index] = $row[3];
            $svqmake[$index] = $row[4];
            $svqmodel[$index] = $row[5];
            $svqstyle[$index] = $row[6];
            $svqprice[$index] = $row[7];
            $svqsid[$index] = $row[8];

            // Get the current Purchase Approval Information (if any)...
            $pquery = 'select Status, ChosenOn, BoughtOn, SalesReviewed from purchases where SpecificVehicleID = '.$svqsid[$index];
            $presult = mysql_query($pquery, $con);
            if($prow = mysql_fetch_array($presult))
            {
                if($prow[0] == 'Chosen') $svqstatus[$index] = 'Chosen for Purchase';
                elseif($prow[0] == 'Bought') $svqstatus[$index] = 'Specific Vehicle Bought';
                else $svqstatus[$index] = 'Specific Vehicle Being Purchased';
            }
            else $svqstatus[$index] = 'Unknown';

            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 300px;">All Specific Vehicle Quotes</h1>
        <div class="grideightgrey">
<?php
    if(isset($svqid)) $count = count($svqid);
    else $count = 0;
    if($count < 1)
    {
        echo '<p class="blackfourteen" style="color:#142c3c; font-size: 13px;">* No Active Specific Vehicle Quotes Found</p>';
    }
    else
    {
        echo '<table width="600" cellpadding="3" cellspacing="3"><tr valign="bottom">';
        //echo '<td width="50" align="center"><h3 class="greensub">SVQ</h3></td>';
        echo '<td width="250"><h3 class="greensub">SPECIFIC VEHICLE QUOTE(S)</h3></td>';
        echo '<td width="200" align="center"><h3 class="greensub">STATUS</h3></td>';
        echo '<td width="100" align="center"><h3 class="greensub">PRICE</h3></td></tr>';
        for($i = 0; $i < $count; $i++)
        {
            echo '<tr valign="baseline">';
            //echo '<td align="center"><p class="greyeleven">'.str_pad($svqid[$i],5,"0",STR_PAD_LEFT).'</p></td>';
            echo '<td>';
            echo '<p class="formbluetext">';
            if($svqstatus[$i] == 'Unknown') echo '<a href="specificquote.php?SVQID='.$svqid[$i].'">';
            else echo '<a href="specificordered.php?SVQID='.$svqid[$i].'">';
            echo $svqyear[$i].' '.$svqmake[$i].' '.$svqmodel[$i].' '.$svqstyle[$i];
            echo '</a>';
            echo '</p><p class="greyeleven">'.$svqupdated[$i].'</p>';
            echo '</td>';
            echo '<td align="center"><p class="greyeleven">'.$svqstatus[$i].'</p></td>';
            if($svqprice[$i]==0) echo '<td align="center"><p class="greyeleven">?</p></td>';
            else echo '<td align="center"><p class="greyeleven">$'.number_format($svqprice[$i]).'</p></td></tr>';
        }
        echo '</table>';
    }
?>
            <br />
        </div><!-- end grideightgrey-->
    </div><!-- end grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
