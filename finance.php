<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 3;
    $_SESSION['titleadd'] = 'Financing Options';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 250px;">Financing Options</h1>
        <form action="consultsave.php" method="post" name="consultform">
            <div class="grideightgrey">
                <div class="grideight" style="width: 600px;">
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        1-800-vehicles.com dealers can offer you great options for financing a vehicle within your budget. Once you have registered on our site, your 1-800-vehicles.com representative will help you determine the best financing source to meet your specific needs.
                    </p>
<?php
    $max_width = 550;
    $max_height = 550;
    $imagename = 'common/layout/finance.jpg';
    echo '<center><img src="loadimage.php?image='.$imagename.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" /></center><br/>';
?>
                </div><!-- end greyeight-->
            </div><!-- end greyeightgrey-->
        </form>
    </div><!-- end grideightgrey-->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("testimonials.php"); ?>
<?php require("footerstart.php"); ?>
<?php require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
