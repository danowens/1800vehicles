<?php require("globals.php"); ?>


<?php
//error_reporting(E_ALL); 
//ini_set("display_errors", "on");
$_SESSION['state'] = 0;
$_SESSION['substate'] = 111;
$_SESSION['titleadd'] = 'Best Vehicle Assessment';

if (!isset($_SESSION['user'])) {
    $_SESSION['assessment_post'] = $_POST;
    $_SESSION['laststate'] = $_SESSION['state'];
    $_SESSION['lastsubstate'] = $_SESSION['substate'];	
	
	unset($_SESSION['pos_post']);
	unset($_SESSION['searchplan_post']);
	unset($_SESSION['consult_post']);
	unset($_SESSION['request_trade_in_quote']);
	$_SESSION['submit_btn'] = "SUBMIT TO COMPLETE YOUR VEHICLE ASSESSMENT";
    echo "<script>window.location='register.php';</script>";
    ?>

<!--    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="/common/scripts/jquery-1.4.2.min.js"></script>
    <link rel="stylesheet" href="assets/css/colorbox.css" />
    <script src="assets/js/jquery.colorbox.js"></script>
    <link href="/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    $(document).ready(function() {
            $.colorbox({href: "loginneed.php", scrolling: false, maxWidth: "80%"});
    });
   </script>-->
<?php
} else {

if($_SESSION['assessment_post']){
    $vehicletype = $_SESSION['assessment_post']['vehicletype'];
    $bodytype = $_SESSION['assessment_post']['bodytype'];
    $vehiclesize = $_SESSION['assessment_post']['vehiclesize'];
    $drivetrain = $_SESSION['assessment_post']['drivetrain'];
    $notes = $_SESSION['assessment_post']['notes'];
    $origin = $_SESSION['assessment_post']['origin'];
    $transmission = $_SESSION['assessment_post']['transmission'];
    $mileagefrom = $_SESSION['assessment_post']['mileagefrom'];
    $mileageto = $_SESSION['assessment_post']['mileageto'];
    $mileageceiling = $_SESSION['assessment_post']['mileageceiling'];
    $extlike = $_SESSION['assessment_post']['extlike'];
    $extdislike = $_SESSION['assessment_post']['extdislike'];
    $intlike = $_SESSION['assessment_post']['intlike'];
    $intdislike = $_SESSION['assessment_post']['intdislike'];
    $budgetfrom = $_SESSION['assessment_post']['budgetfrom'];
    $budgetto = $_SESSION['assessment_post']['budgetto'];
    $borrowmaxpayment = $_SESSION['assessment_post']['borrowmaxpayment'];
    $borrowdownpayment = $_SESSION['assessment_post']['borrowdownpayment'];
    $vehicleneed = $_SESSION['assessment_post']['vehicleneed'];
    $brandlike = $_SESSION['assessment_post']['brandlike'];
    $branddislike = $_SESSION['assessment_post']['branddislike'];
    $model = $_SESSION['assessment_post']['model'];
    $vehicleage = $_SESSION['assessment_post']['vehicleage'];
    $ratefuelefficiency = $_SESSION['assessment_post']['ratefuelefficiency'];
    $ratemaintenancecost = $_SESSION['assessment_post']['ratemaintenancecost'];
    $ratereliability = $_SESSION['assessment_post']['ratereliability'];
    $rateluxury = $_SESSION['assessment_post']['rateluxury'];
    $ratesporty = $_SESSION['assessment_post']['ratesporty'];
    $ratesafety = $_SESSION['assessment_post']['ratesafety'];

//What do you prefer start
    $frontstype = $_SESSION['assessment_post']['frontstype'];
    $bedtype = $_SESSION['assessment_post']['bedtype'];
    $leather = $_SESSION['assessment_post']['leather'];
    $heatedseat = $_SESSION['assessment_post']['heatedseat'];
    $navigation = $_SESSION['assessment_post']['navigation'];
    $sunroof = $_SESSION['assessment_post']['sunroof'];
    $alloywheels = $_SESSION['assessment_post']['alloywheels'];
    $rearwindow = $_SESSION['assessment_post']['rearwindow'];
    $bedliner = $_SESSION['assessment_post']['bedliner'];
    $entertainmentsystem = $_SESSION['assessment_post']['entertainmentsystem'];
    $thirdrs = $_SESSION['assessment_post']['thirdrs'];
    $crow = $_SESSION['assessment_post']['crow'];
    $prhatch = $_SESSION['assessment_post']['prhatch'];
    $backupcamera = $_SESSION['assessment_post']['backupcamera'];
    $tpackage = $_SESSION['assessment_post']['tpackage'];
    $otherprefereces = $_SESSION['assessment_post']['otherprefereces'];
    $otherreallyhave = $_SESSION['assessment_post']['otherreallyhave'];
    $additionalinfo = $_SESSION['assessment_post']['additionalinfo'];
    unset($_SESSION['assessment_post']);
    
}
else if($_POST)
{
 
    if(isset($_POST['edit_mode']) && $_POST['edit_mode']){
        
    }
   
    if(isset($_POST['edit_mode']) && $_POST['edit_mode']){
       $go_to_dashboard=1;
    }
    // See what was passed in...
    $vehicletype = $_POST['vehicletype'];
    $bodytype = $_POST['bodytype'];
    $vehiclesize = $_POST['vehiclesize'];
    $drivetrain = $_POST['drivetrain'];
    $notes = $_POST['notes'];
    $origin = $_POST['origin'];
    $transmission = $_POST['transmission'];
    $mileagefrom = $_POST['mileagefrom'];
    $mileageto = $_POST['mileageto'];
    $mileageceiling = $_POST['mileageceiling'];
    $extlike = $_POST['extlike'];
    $extdislike = $_POST['extdislike'];
    $intlike = $_POST['intlike'];
    $intdislike = $_POST['intdislike'];
    $budgetfrom = $_POST['budgetfrom'];
    $budgetto = $_POST['budgetto'];
    $borrowmaxpayment = $_POST['borrowmaxpayment'];
    $borrowdownpayment = $_POST['borrowdownpayment'];
    $vehicleneed = $_POST['vehicleneed'];
    $brandlike = $_POST['brandlike'];
    $branddislike = $_POST['branddislike'];
    $model = $_POST['model'];
    $vehicleage = $_POST['vehicleage'];
    $ratefuelefficiency = $_POST['ratefuelefficiency'];
    $ratemaintenancecost = $_POST['ratemaintenancecost'];
    $ratereliability = $_POST['ratereliability'];
    $rateluxury = $_POST['rateluxury'];
    $ratesporty = $_POST['ratesporty'];
    $ratesafety = $_POST['ratesafety'];

//What do you prefer start
    $frontstype = $_POST['frontstype'];
    $bedtype = $_POST['bedtype'];
    $leather = $_POST['leather'];
    $heatedseat = $_POST['heatedseat'];
    $navigation = $_POST['navigation'];
    $sunroof = $_POST['sunroof'];
    $alloywheels = $_POST['alloywheels'];
    $rearwindow = $_POST['rearwindow'];
    $bedliner = $_POST['bedliner'];
    $entertainmentsystem = $_POST['entertainmentsystem'];
    $thirdrs = $_POST['thirdrs'];
    $crow = $_POST['crow'];
    $prhatch = $_POST['prhatch'];
    $backupcamera = $_POST['backupcamera'];
    $tpackage = $_POST['tpackage'];
    $otherprefereces = $_POST['otherprefereces'];
    $otherreallyhave = $_POST['otherreallyhave'];
    $additionalinfo = $_POST['additionalinfo'];
}

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];
    $srep = getsalesrep($userid, $marketneedid);

    $errorinsave = 'false';
//@TODO: check for validation of the form data

    if ($vehicletype == "Auto") {
//insert quert data
        $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels';
        $doyouprefervalues = "'" . $leather . "','" . $heatedseat . "','" . $navigation . "','" . $sunroof . "','" . $alloywheels . "'";

//update query data
        $updatedoyouprefer = ", Leather = '" . $leather . "'";
        $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
        $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
        $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
        $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
    } elseif ($vehicletype == "Minivan") {
//insert quert data
        $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, PRHatch, BackupCamera';
        $doyouprefervalues = "'" . $leather . "','" . $heatedseat . "','" . $navigation . "','" . $sunroof . "','" . $alloywheels . "','" . $entertainmentsystem . "','" . $prhatch . "','" . $backupcamera . "'";

//update query data
        $updatedoyouprefer = ", Leather = '" . $leather . "'";
        $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
        $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
        $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
        $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
        $updatedoyouprefer .= ", EntertainmentSystem = '" . $entertainmentsystem . "'";
        $updatedoyouprefer .= ", PRHatch = '" . $prhatch . "'";
        $updatedoyouprefer .= ", BackupCamera = '" . $backupcamera . "'";
    } elseif ($vehicletype == "SUV") {
        $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, ThirdRD, CRow, PRHatch, BackupCamera, TPackage';
        $doyouprefervalues = "'" . $leather . "','" . $heatedseat . "','" . $navigation . "','" . $sunroof . "','" . $alloywheels . "','" . $entertainmentsystem . "','" . $thirdrs . "','" . $crow . "','" . $prhatch . "','" . $backupcamera . "','" . $tpackage . "'";

//update query data
        $updatedoyouprefer = ", Leather = '" . $leather . "'";
        $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
        $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
        $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
        $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
        $updatedoyouprefer .= ", EntertainmentSystem = '" . $entertainmentsystem . "'";
        $updatedoyouprefer .= ", ThirdRD = '" . $thirdrs . "'";
        $updatedoyouprefer .= ", CRow = '" . $crow . "'";
        $updatedoyouprefer .= ", PRHatch = '" . $prhatch . "'";
        $updatedoyouprefer .= ", BackupCamera = '" . $backupcamera . "'";
        $updatedoyouprefer .= ", TPackage = '" . $tpackage . "'";
    } elseif ($vehicletype == "Pickup") {
        $doyouprefer = 'FrontSType, BedType, Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, RearWindow, BedLiner, BackupCamera, TPackage';
        $doyouprefervalues = "'" . $frontstype . "','" . $bedtype . "','" . $leather . "','" . $heatedseat . "','" . $navigation . "','" . $sunroof . "','" . $alloywheels . "','" . $rearwindow . "','" . $bedliner . "','" . $backupcamera . "','" . $tpackage . "'";

//update query data
        $updatedoyouprefer = ", FrontSType = '" . $frontstype . "'";
        $updatedoyouprefer .= ", BedType = '" . $bedtype . "'";
        $updatedoyouprefer .= ", Leather = '" . $leather . "'";
        $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
        $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
        $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
        $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
        $updatedoyouprefer .= ", RearWindow = '" . $rearwindow . "'";
        $updatedoyouprefer .= ", BedLiner = '" . $bedliner . "'";
        $updatedoyouprefer .= ", BackupCamera = '" . $backupcamera . "'";
        $updatedoyouprefer .= ", TPackage = '" . $tpackage . "'";
    }


    if ($errorinsave == 'false') {
        $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
        if ($con) {
            mysql_select_db(DB_SERVER_DATABASE, $con);

// See if they already have one...
            $query = "select * from assessments where MarketNeedID = " . $marketneedid;
            $result = mysql_query($query, $con);
            if (!$result || !$row = mysql_fetch_array($result)) {
// There is not one already saved, so add it...
                $query = "insert into assessments (MarketNeedID, Created, LastUpdated, SeenBySales, VehicleType, BodyType, VehicleSize, DriveTrain, Notes, Origin, Transmission, MileageFrom, MileageTo, MileageCeiling, ExtLike, ExtDislike, IntLike, IntDisike, BudgetFrom, BudgetTo, BorrowMaxPayment, BorrowDownPayment, VehicleNeed, BrandLike, BrandDislike, Model, VehicleAge, RateFuelEfficiency, RateMintenanceCost, RateReliability, RateLuxury, RateSporty, RateSafety," . $doyouprefer . ", OtherPrefereces, OtherReallyHave, AdditionalInfo) values (" . $marketneedid . ",";
                $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','" . date_at_timezone('Y-m-d H:i:s', 'EST') . "',0,";
                $query .= "'" . $vehicletype . "',";
                $query .= "'" . $bodytype . "',";
                $query .= "'" . $vehiclesize . "',";
                $query .= "'" . $drivetrain . "',";
                $query .= "'" . $notes . "',";
                $query .= "'" . $origin . "',";
                $query .= "'" . $transmission . "',";
                $query .= "'" . $mileagefrom . "',";
                $query .= "'" . $mileageto . "',";
                $query .= "'" . $mileageceiling . "',";
                $query .= "'" . $extlike . "',";
                $query .= "'" . $extdislike . "',";
                $query .= "'" . $intlike . "',";
                $query .= "'" . $intdislike . "',";
                $query .= "'" . $budgetfrom . "',";
                $query .= "'" . $budgetto . "',";
                $query .= "'" . $borrowmaxpayment . "',";
                $query .= "'" . $borrowdownpayment . "',";
                $query .= "'" . $vehicleneed . "',";
                $query .= "'" . $brandlike . "',";
                $query .= "'" . $branddislike . "',";
                $query .= "'" . $model . "',";
                $query .= "'" . $vehicleage . "',";
                $query .= "'" . $ratefuelefficiency . "',";
                $query .= "'" . $ratemaintenancecost . "',";
                $query .= "'" . $ratereliability . "',";
                $query .= "'" . $rateluxury . "',";
                $query .= "'" . $ratesporty . "',";
                $query .= "'" . $ratesafety . "',";
                $query .= $doyouprefervalues . ",";
                $query .= "'" . $otherprefereces . "',";
                $query .= "'" . $otherreallyhave . "',";
                $query .= "'" . $additionalinfo . "'";
                $query .= ")";
		
                if (!mysql_query($query, $con))
                    $errorinsave = 'Could not add Assessment';

                if ($errorinsave == 'false') {
// Add a Message Update when this happens...
                    posttodashboard($con, $userid, $userid, 'completed an <a href="' . WEB_SERVER_NAME . 'assessment.php">Assessment</a>.', $marketneedid);

                    if ($srep != -1) {
// Add a Message when this happens...
                        posttodashboard($con, $userid, $srep, '<a href="' . WEB_SERVER_NAME . 'salesrepactions.php?ForUserID=' . $userid . '&MarketNeedID=' . $marketneedid . '">' . $firstname . ' ' . $lastname . '</a> added an Assessment.');

                        $message = 'Your customer has Added an Assessment...</br>';
                        $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                        sendemail(getuseremailnodb($con, $srep), 'Assessment Received!', $message, 'true');

                        $message = 'Your assessment has been received.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, your sales representative will discuss options with you soon.';
                    } else {
                        $message = 'Your assessment has been received.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, a sales representative will be assigned to you soon to discuss your options.';
                    }

                    sendemail(getuseremailnodb($con, $userid), 'Assessment Received', $message, 'false');

                    $message = 'A customer has Added an Assessment...</br>';
                    $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                    sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                }
            } else {
// There is already one saved, so update it...
                $query = "update assessments set LastUpdated = '" . date_at_timezone('Y-m-d H:i:s', 'EST') . "'";
                $query .= ", SeenBySales = 0";
                $query .= ", VehicleType = '" . $vehicletype . "'";
                $query .= ", BodyType = '" . $bodytype . "'";
                $query .= ", VehicleSize = '" . $vehiclesize . "'";
                $query .= ", DriveTrain = '" . $drivetrain . "'";
                $query .= ", Notes = '" . $notes . "'";
                $query .= ", Origin = '" . $origin . "'";
                $query .= ", Transmission = '" . $transmission . "'";
                $query .= ", MileageFrom = '" . $mileagefrom . "'";
                $query .= ", MileageTo = '" . $mileageto . "'";
                $query .= ", MileageCeiling = '" . $mileageceiling . "'";
                $query .= ", ExtLike = '" . $extlike . "'";
                $query .= ", ExtDislike = '" . $extdislike . "'";
                $query .= ", IntLike = '" . $intlike . "'";
                $query .= ", IntDisike = '" . $intdislike . "'";
                $query .= ", BudgetFrom = '" . $budgetfrom . "'";
                $query .= ", BudgetTo = '" . $budgetto . "'";
                $query .= ", BorrowMaxPayment = '" . $borrowmaxpayment . "'";
                $query .= ", BorrowDownPayment = '" . $borrowdownpayment . "'";
                $query .= ", VehicleNeed = '" . $vehicleneed . "'";
                $query .= ", BrandLike = '" . $brandlike . "'";
                $query .= ", BrandDislike = '" . $branddislike . "'";
                $query .= ", Model = '" . $model . "'";
                $query .= ", VehicleAge = '" . $vehicleage . "'";
                $query .= ", RateFuelEfficiency = '" . $ratefuelefficiency . "'";
                $query .= ", RateMintenanceCost = '" . $ratemaintenancecost . "'";
                $query .= ", RateReliability = '" . $ratereliability . "'";
                $query .= ", RateLuxury = '" . $rateluxury . "'";
                $query .= ", RateSporty = '" . $ratesporty . "'";
                $query .= ", RateSafety = '" . $ratesafety . "'";
                $query .= $updatedoyouprefer;
                $query .= ", OtherPrefereces = '" . $otherprefereces . "'";
                $query .= ", OtherReallyHave = '" . $otherreallyhave . "'";
                $query .= ", AdditionalInfo = '" . $additionalinfo . "'";
                $query .= " where MarketNeedID = " . $marketneedid;
                if (!mysql_query($query, $con))
                    $errorinsave = 'Could not update Assessment';

                if ($errorinsave == 'false') {
// Add a Message Update when this happens...
                    posttodashboard($con, $userid, $userid, 'updated <a href="' . WEB_SERVER_NAME . 'assessment.php">Assessment</a> information.', $marketneedid);

                    if ($srep != -1) {
// Add a Message when this happens...
                        posttodashboard($con, $userid, $srep, '<a href="' . WEB_SERVER_NAME . 'salesrepactions.php?ForUserID=' . $userid . '&MarketNeedID=' . $marketneedid . '">' . $firstname . ' ' . $lastname . '</a> updated their Assessment Information.');

                        $message = 'Your customer has Updated their Assessment Information...</br>';
                        $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                        sendemail(getuseremailnodb($con, $srep), 'Assessment Updated!', $message, 'true');
                    }

                    $message = 'A customer has Updated their Assessment...</br>';
                    $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                    sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                }
            }

            mysql_close($con);
        } else
            $errorinsave = 'Could not connect to the database';
    }
    ?>

    <?php  
    require("headerstart.php"); ?>
    <?php require("header.php"); ?>
    <?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
    <div id="content">
        <div class="grideightcontainer">
    <?php
    if(isset($go_to_dashboard) && $go_to_dashboard==1){
         echo"<script>window.location='dashboard.php';</script>";
    }
    
    
    if ($errorinsave != 'false') {
        echo ' <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Assessment Issue!</h1>';
    } else {
        echo ' <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Vehicle Needs Assessment Complete!</h1>';
    }
    ?>
            <div class="grideightgrey">
                <div class="grideight">
    <?php
    if ($errorinsave != 'false') {
        echo '<p class="blacktwelve">Sorry!  There was an error processing your assessment.</p>';
        echo '<p class="blacktwelve">Please contact a representative at 1-800-vehicles (834-4253) for help with this issue.</p>';
        if ($errorinsave != 'true')
            echo $errorinsave . '<br/>';
        echo '<p class="blacktwelve">Use your browser <a href="javascript:history.back()">back</a> button to add missing fields or to start over click <a href="' . WEB_SERVER_NAME . 'assessment.php">here</a>.</p>';
//echo $query.'<br/>';
    }
    else {
        echo '<p class="blacktwelve">A 1-800-vehicles.com representative will be assigned to shortly, and he or she reply to you with Our Best Vehicle Recommendations within 24 hours.</p>';
        echo '<p class="blacktwelve">To change your vehicle needs assessment, you can revisit the <a href="' . WEB_SERVER_NAME . 'assessment.php">Vehicle Needs Assessment</a> page at any time o go back to <a href="' . WEB_SERVER_NAME . 'dashboard.php"> Dashboard.</a></p>';
//if($errorinsave != 'true') echo $errorinsave.'<br/>';
//echo $query.'<br/>';
    }
    ?>
                </div><!-- end greyeight-->
            </div><!-- grid eight container -->
        </div><!-- end grideightgrey-->
                    <?php require("teaser.php"); ?>
    </div><!--end content-->

                    <?php require("footerstart.php"); ?>
                    <?php require("footer.php"); ?>
                    <?php
                    require("footerend.php");
                }
                ?>
