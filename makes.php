<?php
    require_once(WEB_ROOT_PATH."globals.php");

    // AddMake = find/add a make in the database
    // $pmake = Name of the Make to add
    // $porigin = 'Domestic' by default or 'Import' is also allowed
    // Return = return the ID of the existing or new make
    function AddMake($pmake, $porigin = 'Domestic')
    {
        if($porigin != 'Domestic' && $porigin != 'Import') $porigin = 'Domestic';

        $amcon = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($amcon)
        {
            mysql_select_db(DB_SERVER_DATABASE, $amcon);

            // Look to see if we the make exists already...
            $amquery = "select MakeID from makes m where m.Name = '".$pmake."' and m.Origin = '".$porigin."'";
            $amresult = mysql_query($amquery, $amcon);
            if($amrow = mysql_fetch_array($amresult))
            {
                $pid = $amresult[0];
                mysql_close($amcon);
                return $pid;
            }

            // We did not find it, so lets add it...
            $addquery = "insert into makes (Name, Origin) values ('".$pmake."' and m.Origin = '".$porigin."')";
            if(!mysql_query($addquery, $amcon)) $_SESSION['ShowError'] .= '\nCould not add the Make to the database.\n\n';
            else
            {
                $pid = mysql_insert_id($amcon);
                mysql_close($amcon);
                return $pid;
            }

            mysql_close($amcon);
        }

        // If we get here there was an issue, so return a bad ID number...
        return 0;
    }

    // AddMakeNoDB = find/add a make in the database
    // $pcon = The database connection to use
    // $pmake = Name of the Make to add
    // $porigin = 'Domestic' by default or 'Import' is also allowed
    // Return = return the ID of the existing or new make
    function AddMakeNoDB($pcon, $pmake, $porigin = 'Domestic')
    {
        if($porigin != 'Domestic' && $porigin != 'Import') $porigin = 'Domestic';

        // Look to see if we the make exists already...
        $amquery = "select MakeID from makes m where m.Name = '".$pmake."' and m.Origin = '".$porigin."'";
        $amresult = mysql_query($amquery, $pcon);
        if($amrow = mysql_fetch_array($amresult))
        {
            $pid = $amresult[0];
            mysql_close($pcon);
            return $pid;
        }

        // We did not find it, so lets add it...
        $addquery = "insert into makes (Name, Origin) values ('".$pmake."' and m.Origin = '".$porigin."')";
        if(!mysql_query($addquery, $pcon)) $_SESSION['ShowError'] .= '\nCould not add the Make to the database.\n\n';
        else
        {
            $pid = mysql_insert_id($pcon);
            return $pid;
        }

        // If we get here there was an issue, so return a bad ID number...
        return 0;
    }

    // GetMakeName = find the make in the database and return the name
    // $pid = ID of the Make to get
    // $porigin = 0 or 1 as to whether to include the origin in the name returned
    // Return = return the Name of the existing make
    function GetMakeName($pid, $porigin = 0)
    {
        $amcon = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($amcon)
        {
            mysql_select_db(DB_SERVER_DATABASE, $amcon);

            // Look to see if we the make exists already...
            $amquery = "select Name, Origin from makes m where m.MakeID = ".$pid;
            $amresult = mysql_query($amquery, $amcon);
            if($amrow = mysql_fetch_array($amresult))
            {
                $pname = $amresult[0];
                if($porigin != 0) $pname .= " (".$amresult[0].")";
                mysql_close($amcon);
                return $pname;
            }

            mysql_close($amcon);
        }

        // If we get here there was an issue, so return a bad name...
        return "Unknown";
    }

    // GetMakeName = find the make in the database and return the name
    // $pcon = The database connection to use
    // $pid = ID of the Make to get
    // $porigin = 0 or 1 as to whether to include the origin in the name returned
    // Return = return the Name of the existing make
    function GetMakeNameNoDB($pcon, $pid, $porigin = 0)
    {
        // Look to see if we the make exists already...
        $amquery = "select Name, Origin from makes m where m.MakeID = ".$pid;
        $amresult = mysql_query($amquery, $pcon);
        if($amrow = mysql_fetch_array($amresult))
        {
            $pname = $amresult[0];
            if($porigin != 0) $pname .= " (".$amresult[0].")";
            return $pname;
        }

        // If we get here there was an issue, so return a bad name...
        return "Unknown";
    }
?>
