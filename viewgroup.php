<?php require("globals.php"); ?>
<style>
input[type="text"],select {
	width:65px!important;
}
</style>
<?php
    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = "All Virtual Vehicles";
    //$_SESSION['ShowError'] = $_SESSION['LastGroup'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

                if(empty($_POST['Reason']))
                {
                    // purposefully empty
                }
                else if($_POST['Reason'] == 'ChangePrices')
                {
                    if (!empty($_POST['ids_csv']))
                    {

                        $ids = explode(",", $_POST['ids_csv']);
                        $updates = array();

                        foreach($ids as $id)
                        {
                            $id = (int) $id;
                            if(empty($id) || !isset($_POST["price_$id"]))
                            {
                                continue;
                            }
                            $new_price = str_replace(",", "", $_POST["price_$id"]);
                            $new_price = preg_replace("/[^0-9.]/", "", $new_price);
							
                            $new_price = (int) $new_price;
                            if($new_price <= 0)
                            {
                                continue;
                            }
                            $updates[$id] = $new_price;
                        }

                        require_once(WEB_ROOT_PATH . "common/functions/pricefunctions.php");
                        $success = mysql_query("start transaction", $con);
                        if($success)
                        {
                            $error = false;
                            $count = 0;
                            foreach($updates as $id => $new_price)
                            {
							$hybrid = $_POST["Hybrid_$id"];
							$size = $_POST["Size_$id"];
                                $error = setVehiclePrice($con, $id, $new_price, $hybrid, $size);
                                if($error)
                                {
                                    echo "Received error \"$error\"<br />\n";
                                    mysql_query("rollback", $con);
                                    break;
                                }
                                $count++;
                            }

                            if(!$error)
                            {
                                $success = mysql_query("commit", $con);
                            }
                        }
                        else
                        {
                            $error = true;
                        }

                        if(!$error)
                        {
                            $_SESSION['ShowError'] = "Successfully changed prices on $count vehicles in the group.";
                        }
                        else
                        {
                            $_SESSION['ShowError'] = 'Error changing the price on one or more vehicles.';
                        }
                    }
                }
                else if($_POST['Reason'] == 'ChangeVisibility')
                {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select v.VehicleID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicles set Visible = '.$_POST['statuslist'].' where VehicleID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Visibility on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Visibility on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
                }
                else if($_POST['Reason'] == 'RunValidators')
                {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dl.VehicleDataID, dh.VehicleDataID, v.VehicleID';
                $cvuquery .= ', v.BlackBookAvg, v.Year';
                $cvuquery .= ', dl.MileageStart, dl.MileageEnd, dh.MileageStart, dh.MileageEnd';
                $cvuquery .= ', dl.PriceStart, dl.PriceEnd, dh.PriceStart, dh.PriceEnd';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                $records = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    // run validators
                    $records++;

    if (1) {
                    // gather data from query
                    $lowvehdataid   = $cvurow[0];
                    $highvehdataid  = $cvurow[1];
                    $vehid          = $cvurow[2];

                    $bbprice        = $cvurow[3];
                    $caryear        = $cvurow[4];

                    $highmilestart  = $cvurow[5];
                    $highmileend    = $cvurow[6];
                    $lowmilestart   = $cvurow[7];
                    $lowmileend     = $cvurow[8];

                    $highpricestart = $cvurow[9];
                    $highpriceend   = $cvurow[10];
                    $lowpricestart  = $cvurow[11];
                    $lowpriceend    = $cvurow[12];

                    // run reverse bbprice algorithm
                    // handle case of bbprice being blank and calculate it from price ranges
                    if ((strlen($bbprice) == 0) || $bbprice == 0)
                    {
                        // TODO REMOVE: once all vehicles a valid bbprice
                        // Goal: handle case where old data had price ranges entered but bbprice blank
                        // this picks a reasonable value which would have expanded to the existing price ranges using the older 3000/4500 markups.
                        // however since the markups have changed slightly and because some ranges may be wrong,
                        // it is still necessary to run the new pricing algorithm to ensure that the price ranges are correct

                        // Examples
                        // Scenario 1 - average price is one interval lower than low price
                        //  STARTING - low price range is 38,000-40,000 and high price range is 36,000-38,000
                        //    subtract low  price markup: low price  - 4500 = 33,500 - 35,500
                        //    subtract high price markup: high price - 3000 = 33,000 - 35,000
                        //    to satisfy both ranges: price must be between 33,500 - 35,000
                        //    use midpoint of range: midpoint is 34,250 ((33,500 + 35,000) / 2)
                        //    RESULT - this will cause the mileage range to be identical (unless markup has changed) when running price algorithm

                        // Scenario 2 - average price and low price are identical interval
                        //  STARTING - low price range is 38,000-40,000 and high price range is 38,000-40,000
                        //    subtract low  price markup: low price  - 4500 = 33,500 - 35,500
                        //    subtract high price markup: high price - 3000 = 35,000 - 37,000
                        //    to satisfy both ranges: price must be between 35,000 - 35,500
                        //    use midpoint of range: midpoint is 35,250 ((35,000 + 35,500) / 2)
                        //    RESULT - this will cause the mileage range to be identical (unless markup has changed) when running price algorithm

                        // Scenario 3 - average price and low price are not realistic (not overlapping in the final analysis)
                        //  STARTING - low price range is 38,000-40,000 and high price range is 34,000-36,000
                        //    subtract low  price markup: low price  - 4500 = 33,500 - 35,500
                        //    subtract high price markup: high price - 3000 = 31,000 - 33,000
                        //    to satisfy both ranges: price must be between 33,500 - 33,000
                        //    use midpoint of range: midpoint is 33,250 ((33,500 + 33,000) / 2)
                        //    NOTE - this is not really a range, but the midpoint is still a reasonable price
                        //    RESULT - this will cause the mileage range to be different but the range was invalid and the result is close enough
                        //      average: 33,250 + 3000 = 36,250 -> 36-38
                        //      low:     33,250 + 4500 = 37,750 -> 36-38

                        //Algorithm
                        //0. use fixed markup since Dan just changed his mind recently, all existing ranges used these numbers, also means some prices will now be higher.
                            $hmarkup = 3000;
                            $lmarkup = 4500;
                        //1. subtract markup to get original range
                            $tmpLowPriceStart  = $lowpricestart  - $lmarkup;
                            $tmpLowPriceEnd    = $lowpriceend    - $lmarkup;
                            $tmpHighPriceStart = $highpricestart - $hmarkup;
                            $tmpHighPriceEnd   = $highpriceend   - $hmarkup;
                        //2. solve for overlapping range (but it doesn't matter if end is less than start (see scenario #3 above)
                            $pricerangestart = max($tmpLowPriceStart, $tmpHighPriceStart);
                            $pricerangeend   = min($tmpLowPriceEnd,   $tmpHighPriceEnd);
                        //3. take midpoint
                            $bbprice = ($pricerangestart + $pricerangeend) / 2;
                        //4. still need to run the pricing algorithm now since Scenario #3 will change the price ranges (and different markups at higher prices will too)
                    }

                    // run forward price algorithm

                    // calculate markup
                    if ($bbprice < 20000)
                    {
                      $hmarkup = 3000;
                      $lmarkup = 4500;
                    }
                    elseif ($bbprice < 40000)
                    {
                      $hmarkup = 3500;
                      $lmarkup = 5000;
                    }
                    else
                    {
                      $hmarkup = 3500;
                      $lmarkup = 5500;
                    }

                    // calculate price targets
                    $ltarget = $lmarkup + $bbprice;

                    // snap to ranges and intervals
                    if ($ltarget < 5000)
                    {
                      $lowpricestart = 0;
                      $linterval = 5000;
                    }
                    elseif ($ltarget < 10000)
                    {
                        // 6,001 -> 6,000
                        // 6,999 -> 6,000

                      $lowpricestart = floor($ltarget / 1000) * 1000;
                      $linterval = 1000;
                    }
                    elseif ($ltarget < 40000)
                    {
                        // 10,500 -> 10,000
                        // 11,999 -> 10,000
                        // 30,500 -> 30,000
                        // 34,999 -> 34,000
                      $lowpricestart = floor($ltarget / 2000) * 2000;
                      $linterval = 2000;
                    }
                    else
                    {
                        // 50,500 -> 50,000
                        // 54,999 -> 50,000
                      $lowpricestart = floor($ltarget / 5000) * 5000;
                      $linterval = 5000;
                    }

                    $lowpriceend = $linterval + $lowpricestart;

                    // calculate price targets
                    $htarget = $hmarkup + $bbprice;

                    // snap to ranges and intervals
                    if ($htarget < 5000)
                    {
                        // 500 -> 0 .. 5,000
                      $highpricestart = 0;
                      $hinterval = 5000;
                    }
                    elseif ($htarget < 10000)
                    {
                        // 6,001 -> 6,000 .. 7,000
                        // 6,999 -> 6,000 .. 7,000

                      $highpricestart = floor($htarget / 1000) * 1000;
                      $hinterval = 1000;
                    }
                    elseif ($htarget < 40000)
                    {
                        // 10,500 -> 10,000 .. 12,000
                        // 11,999 -> 10,000 .. 12,000
                        // 30,500 -> 30,000 .. 32,000
                        // 34,999 -> 34,000 .. 36,000
                      $highpricestart = floor($htarget / 2000) * 2000;
                      $hinterval = 2000;
                    }
                    else
                    {
                        // 50,500 -> 50,000 .. 55,000
                        // 54,999 -> 50,000 .. 55,000
                      $highpricestart = floor($htarget / 5000) * 5000;
                      $hinterval = 5000;
                    }

                    $highpriceend = $hinterval + $highpricestart;

                    // run forward mileage algorithm

                    if ($caryear > 2000 && $caryear < 2020)
                    {
                        // rendering: highmileend (highest) , midpoint , highmilestart = lowmileend , midpoint , lowmilestart
                        // algorithm:
                        //   - todays date - cars date (3/1/(year)) in months / 4 rounded and multiplied by 5,000 per 4 month period
                        $curyear  = intval(date('Y'));

                        $curmonth = intval(date('n'));
                        $carmonth = 3;

                        $diffquadmonths = round ((abs($curyear - $caryear) * 12 + ($curmonth - $carmonth) ) / 4);
                        $targetmiles = $diffquadmonths * 5000;

                        // high (actually average) mileage is +/- 10,000 miles from target, low mileage is -30 below to -10 below
                        $highmileend   = max(0,$targetmiles + 10000);
                        $highmilestart = max(0,$targetmiles - 10000);
                        $lowmileend    = $highmilestart;
                        $lowmilestart  = max(0,$targetmiles - 30000);
                    }

                    // store bbprice, mileage ranges and price ranges

                    // update vehicle record
                    $cvuuquery = 'update vehicles set';
                    $cvuuquery .= '  BlackBookAvg = '.$bbprice;
                    $cvuuquery .= ' where VehicleID = '.$vehid;
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;

                    // update low mileage vehicle data record
                    $cvuuquery = 'update vehicledata set';
                    $cvuuquery .= ' MileageStart = '.$lowmilestart;
                    $cvuuquery .= ', MileageEnd  = '.$lowmileend;
                    $cvuuquery .= ', PriceStart  = '.$lowpricestart;
                    $cvuuquery .= ', PriceEnd    = '.$lowpriceend;
                    $cvuuquery .= ' where VehicleDataID = '.$lowvehdataid;
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;

                    // update high mileage vehicle data record
                    $cvuuquery = 'update vehicledata set';
                    $cvuuquery .= ' MileageStart = '.$highmilestart;
                    $cvuuquery .= ', MileageEnd  = '.$highmileend;
                    $cvuuquery .= ', PriceStart  = '.$highpricestart;
                    $cvuuquery .= ', PriceEnd    = '.$highpriceend;
                    $cvuuquery .= ' where VehicleDataID = '.$highvehdataid;
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }
    }
                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully ran mileage and price validators on all '.$records.' vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not update the mileage and/or price validators on '.$errorfound.' vehicles in the group. Last DB write attempt was: '.$cvuuquery;
                }
                //header('Location: virtualvehiclegroups.php');
            }
                }
                else if($_POST['Reason'] == 'ChangeLowMiles')
                {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dl.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set MileageStart = ';
                    if($bumplmiles == 'on') $cvuuquery .= 'MileageStart + ';
                    $cvuuquery .= $_POST['llmiles'].', MileageEnd = ';
                    if($bumplmiles == 'on') $cvuuquery .= 'MileageEnd + ';
                    $cvuuquery .= $_POST['lhmiles'].' where VehicleDataID = '.$cvurow[0];
                    //var_dump($cvuuquery);
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Low Mileage Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Low Mileage Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
                }
                else if($_POST['Reason'] == 'ChangeHighMiles')
                {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dh.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set MileageStart = ';
                    if($bumphmiles == 'on') $cvuuquery .= 'MileageStart + ';
                    $cvuuquery .= $_POST['ahlmiles'].', MileageEnd = ';
                    if($bumphmiles == 'on') $cvuuquery .= 'MileageEnd + ';
                    $cvuuquery .= $_POST['ahhmiles'].' where VehicleDataID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Average to High Mileage Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Average to High Mileage Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
                }
                else if($_POST['Reason'] == 'ChangeLowPrice')
                {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dl.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set PriceStart = ';
                    if($bumplprice == 'on') $cvuuquery .= 'PriceStart - ';
                    $cvuuquery .= $_POST['llprice'].', PriceEnd = ';
                    if($bumplprice == 'on') $cvuuquery .= 'PriceEnd - ';
                    $cvuuquery .= $_POST['lhprice'].' where VehicleDataID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Low Mile Price Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Low Mile Price Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
                }
                else if($_POST['Reason'] == 'ChangeHighPrice')
                {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dh.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set PriceStart = ';
                    if($bumphprice == 'on') $cvuuquery .= 'PriceStart - ';
                    $cvuuquery .= $_POST['ahlprice'].', PriceEnd = ';
                    if($bumphprice == 'on') $cvuuquery .= 'PriceEnd - ';
                    $cvuuquery .= $_POST['ahhprice'].' where VehicleDataID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Average to High Mile Price Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Average to High Mile Price Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
        }

        $query = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
        $query .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
        $query .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $groupname = $row[0];

            $vquery = 'select v.VehicleID, m.Name,v.Year,v.Model,v.Style,v.Type,v.Doors,v.Convertible,v.WheelDrive,v.BodyType,v.Visible,v.ImageFile,v.BlackBookAvg,v.Hybrid,v.Size';
			$vquery .= " ,"; 
            $vquery .= 'dl.MileageStart "LowMileStart",dl.MileageEnd "LowMileEnd",dl.PriceStart "LowPriceStart",dl.PriceEnd "LowPriceEnd",dl.BestBuy "LowBestBuy",';
            $vquery .= 'dh.MileageStart "HighMileStart",dh.MileageEnd "HighMileEnd",dh.PriceStart "HighPriceStart",dh.PriceEnd "HighPriceEnd",dh.BestBuy "HighBestBuy",';
            $vquery .= 'v.ImageFile "ImageFile"';
                        $vquery .= ' from vehicles v join makes m using (MakeID)';
                        $vquery .= ' join vehicledata dh on dh.VehicleID = v.VehicleID and dh.lowmiles = 0';
                        $vquery .= ' join vehicledata dl on dl.VehicleID = v.VehicleID and dl.lowmiles = 1';
            if(!is_null($row[2])) $vquery .= ' and v.Visible = '.$row[2];
            if(!is_null($row[3])) $vquery .= ' and v.MakeID = '.$row[3];
            if(!is_null($row[4])) $vquery .= " and v.Year = '".$row[4]."'";
            if(!is_null($row[5])) $vquery .= " and v.Model like '%".$row[5]."%'";
            if(!is_null($row[6])) $vquery .= " and v.Style like '%".$row[6]."%'";
            if(!is_null($row[7]) && (strlen($row[7]) > 0)) $vquery .= " and v.Type = '".$row[7]."'";
            if(!is_null($row[8])) $vquery .= " and v.Doors = '".$row[8]."'";
            if(!is_null($row[9]) && (strlen($row[9]) > 0)) $vquery .= " and v.Convertible = '".$row[9]."'";
            if(!is_null($row[10]) && (strlen($row[10]) > 0)) $vquery .= " and v.WheelDrive = '".$row[10]."'";
            if(!is_null($row[11]) && (strlen($row[11]) > 0)) $vquery .= " and v.BodyType = '".$row[11]."'";
            if(!is_null($row[14])) $vquery .= ' and dl.MileageStart > '.$row[14];
            if(!is_null($row[15])) $vquery .= ' and dl.MileageEnd < '.$row[15];
            if(!is_null($row[16])) $vquery .= ' and dh.MileageStart > '.$row[16];
            if(!is_null($row[17])) $vquery .= ' and dh.MileageEnd < '.$row[17];
            if(!is_null($row[18])) $vquery .= ' and dl.PriceStart > '.$row[18];
            if(!is_null($row[19])) $vquery .= ' and dl.PriceEnd < '.$row[19];
            if(!is_null($row[20])) $vquery .= ' and dh.PriceStart > '.$row[20];
            if(!is_null($row[21])) $vquery .= ' and dh.PriceEnd < '.$row[21];
            if(!is_null($row[22])) $vquery .= ' and (dl.BestBuy = '.$row[22].' or dh.BestBuy = '.$row[22].')';
            $vquery .= ' order by v.Year desc, m.Name asc, v.Model desc, v.Style';
			
            //$vquery .= ' limit 10';
                        //echo "<pre>\n" . htmlspecialchars($vquery) . "\n</pre>\n";
            //var_dump($vquery);
            $vresult = mysql_query($vquery, $con);
                        if($vresult)
                        {
                            $index = 0;
                            while($vrow = mysql_fetch_array($vresult))
                            {
                                    $vehid[$index] = $vrow[0];
                                    $vehmake[$index] = $vrow[1];
                                    $vehyear[$index] = $vrow[2];
                                    $vehmodel[$index] = $vrow[3];
                                    $vehstyle[$index] = $vrow[4];
                                    $vehtype[$index] = $vrow[5];
                                    $vehdoors[$index] = $vrow[6];
                                    $vehconv[$index] = $vrow[7];
                                    $vehwheel[$index] = $vrow[8];
                                    $vehbody[$index] = $vrow[9];
                                    $vehvisible[$index] = $vrow[10];
                                    $vehimage[$index] = $vrow[11];
                                    $vehbb[$index] = $vrow[12];
									$vehybrid[$index] = $vrow[13];
									$vesize[$index] = $vrow[14];
                                    $vehlms[$index] = $vrow[15];
                                    $vehlme[$index] = $vrow[16];
                                    $vehlps[$index] = $vrow[17];
                                    $vehlpe[$index] = $vrow[18];
                                    $vehlbb[$index] = $vrow[19];
                                    $vehhms[$index] = $vrow[20];
                                    $vehhme[$index] = $vrow[21];
                                    $vehhps[$index] = $vrow[22];
                                    $vehhpe[$index] = $vrow[23];
                                    $vehhbb[$index] = $vrow[24];
                                    $vehimg[$index] = $vrow[25];
                                    $index++;
                            }
                        }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>
<script src="./common/scripts/jquery.jeditable.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">

$(function() {

  $(".inplaceclick").editable("saveblackbook.php",
  {
      tooltip   : "Click to edit...",
      indicator : "Saving...",
      submit    : "OK",
      cancel    : "Cancel",
      id        : "vehicleid",
      name      : "bbval",
      onblur    : "submit",
      style     : "inherit",
      select    : "true"
  });

});
</script>
<div id="content">
    <div class="grideightcontainer" style="width: 1000px;">
        <h1 class="subhead" style="width:275px;">
<?php
    echo empty($groupname) ? "Unknown Vehicle Group" : $groupname;
?>
        </h1>
        <div class="grideightgrey" style="width: 1000px;">
            <div class="grideight" style="margin-top: 0px;width: 1000px;">
                <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">&lt;&lt;&lt;Go to MyDashboard</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="virtualvehiclegroups.php">&lt;&lt;&lt;Go Back to Group Setups</a></p>
                <br/>
                <form action="allvehicleedit.php" method="post">
                    <input type="hidden" name="AddEditType" value="AddNewFromGroup" />
                    <button type="submit" value="" class="med">Add New Vehicle</button>
                </form>
                <br/>
                <h4 class="subhead">Vehicle Group Options</h4>
                <table>
                <tr>
                <td width="200">
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="Refresh" />
                    <button type="submit" value="" class="med">Refresh this Group</button>
                </form>
                </td><td width="400">
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="ChangeVisibility" />
                    <table border="0" width="400" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td width="300">
                                <button type="submit" value="" class="med">Set Visibility</button>
                                for the group to:
                                <select name="statuslist" id="statuslist">
                                    <option value="1">Live</option>
                                    <option value="0" selected="selected"><span style="color:red">Not Live</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
                </td><td width="300" style="display:none">
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="RunValidators" />
                    <button type="submit" value="" class="med">Update Mileage and Price Ranges</button>
                </td></tr>
                </form>
                </table>
                <h4 class="subhead">
<?php
    $count = empty($vehid) ? 0 : count($vehid);
    echo $count;
?>
                Vehicles In Group</h4>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
<input type="hidden" name="ids_csv" value="<?php echo implode(",", $vehid) ?>" />
<input type="hidden" name="Reason" value="ChangePrices">
                <table border="0" width="900" cellspacing="15" style="margin-left:-10px;">
                    <thead style="color: green;">
                        <tr valign="baseline" align="left">
                            <th width="75">Status</th>
                            <th width="300">Vehicle</th>
                            <th width="50">Book</th>
                            <th width="150">Price</th>
                            <th width="160">Class</th>
							<th width="160">Hybrid Only</th>
							<th width="160">Size</th>
                            <th width="200">Details</th>
                            <th width="175">Mileage</th>
                            <th width="100">Avail++</th>
                            <th width="100">Actions</th>
                        </tr>
                    </thead>
                    <tbody style="color: blue;">
<?php
    $index = 0;
    while($index < $count)
    {
        echo '<tr valign="baseline">';
        echo '<td>';
        if($vehvisible[$index] == 1) echo 'Live';
        else echo '<span style="color:red">Not Live</span>';
        echo '</td>';
        echo '<td>'.$vehyear[$index].' '.$vehmake[$index].'<br/>'.$vehmodel[$index].'<br/>'.$vehstyle[$index].'</td>';
?>
    <td style="white-space: nowrap">
            $<input type="text" tabindex="<?php echo $index+1 ?>" name="price_<?php echo $vehid[$index] ?>" value="<?php echo number_format($vehbb[$index]) ?>" />
    </td>
<?php
                //if($vehbb[$index] > 0)
                //{
                //    echo '<td><div class="inplaceclick" id="'.$vehid[$index].'">$'.number_format($vehbb[$index]).'</div></td>';
                //}
                //else
                //{
                //    echo '<td><div class="inplaceclick" id="'.$vehid[$index].'">$-</div></td>';
                //}
        echo '<td>';
        $min = $vehhps[$index]/1000;
        $max = $vehhpe[$index]/1000;
        echo 'Avg:&nbsp;$'.$min.'&nbsp;-&nbsp;'.$max.'k';
        echo '<br/>';
        $min = $vehlps[$index]/1000;
        $max = $vehlpe[$index]/1000;
        echo 'Low:&nbsp;$'.$min.'&nbsp;-&nbsp;'.$max.'k';
        echo '</td>';
		
		echo '<td>Type:&nbsp;'.$vehtype[$index].'<br/>Doors:&nbsp;'.$vehdoors[$index].'<br/>';
        if (!strcmp($vehconv[$index],'Yes')) echo '<span style="color:green">Convertible:&nbsp;'.$vehconv[$index].'</span>';
        else echo 'Convertible:&nbsp;'.$vehconv[$index];
		echo '</td>';
		
		echo '<td>'; 
		//echo $vehybrid[$index];
		
		?>
		
		<select class="form-control" id="Vehicle-element-8" name="Hybrid_<?php echo $vehid[$index]; ?>">
			<option value="Yes" <?php if($vehybrid[$index] == 'Yes') { ?> selected <?php } ?>>Yes</option>
			<option value="No" <?php if($vehybrid[$index] == 'No') { ?> selected <?php } ?>>No</option>
			<option value="Either" <?php if($vehybrid[$index] == 'Either') { ?> selected <?php } ?>>Either</option>
		</select>
		
		<?php echo '</td>';
		
		echo '<td>'; ?>
		
		<select class="form-control" id="Vehicle-element-9" name="Size_<?php echo $vehid[$index]; ?>">
			<?php if($vehtype[$index] == 'Auto') { ?>
			<option value="Sub Compact" <?php if($vesize[$index] == 'Sub Compact') { ?> selected <?php } ?>>Sub Compact</option>
			<option value="Compact" <?php if($vesize[$index] == 'Compact') { ?> selected <?php } ?>>Compact</option>
			<option value="Mid Size" <?php if($vesize[$index] == 'Mid Size') { ?> selected <?php } ?>>Mid Size</option>
			<option value="Full Size" <?php if($vesize[$index] == 'Full Size') { ?> selected <?php } ?>>Full Size</option>
			
			<?php } elseif($vehtype[$index] == 'SUV') { 
			?>
				<option value="Small" <?php if($vesize[$index] == 'Small') { ?> selected <?php } ?>>Small</option>
				<option value="Mid Size" <?php if($vesize[$index] == 'Mid Size') { ?> selected <?php } ?>>Mid Size</option>
				<option value="Full Size" <?php if($vesize[$index] == 'Full Size') { ?> selected <?php } ?>>Full Size</option>
			<?php } ?>
		</select>
		
		<?php echo '</td>';
		
        echo '<td>Wheel&nbsp;Drive:&nbsp;'.$vehwheel[$index].'<br/>Body&nbsp;Type:&nbsp;'.$vehbody[$index];
        if (strlen($vehimg[$index]) == 0) echo '<br/>Image&nbsp;File:&nbsp;<span style="color:red">Missing</span>';
        echo '</td>';

		
        echo '<td>';
        $min = $vehhms[$index]/1000;
        $max = $vehhme[$index]/1000;
        echo 'Avg: '.$min.'-'.$max.'k';
        echo '<br/>';
        $min = $vehlms[$index]/1000;
        $max = $vehlme[$index]/1000;
        echo 'Low: '.$min.'-'.$max.'k';
        echo '</td>';
        echo '<td>';
        if($vehhbb[$index] == 1) echo 'Exc Avail';
        else echo 'No';
        echo '<br/>';
        if($vehlbb[$index] == 1) echo 'Exc Avail';
        else echo 'No';
        echo '</td>';
        echo '<td>'.'<a href="allvehicleedit.php?AddEditType=EditFromGroup&vehid='.$vehid[$index].'">Edit</a>'.' | <a target="_blank" href="details.php?vehid='.$vehid[$index].'">View</a>'.'</td>';
        echo '</tr>';
        $index++;
    }
?>
                    </tbody>
                </table>
<input type="submit" value="Update Prices">
</form>
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
