<?php
	if(!isset($_SESSION['ignoreaccesscheck'])) require_once('checkaccess.php');
    unset($_SESSION['ignoreaccesscheck']);

    if(isset($_SESSION['loginfailed']))
    {
        $loginfailed = $_SESSION['loginfailed'];
        unset($_SESSION['loginfailed']);
    }
    else $loginfailed = "false";
    if(isset($_SESSION['user'])) $username = $_SESSION['user'];
    else $username = "";
    if(isset($_SESSION['userid'])) $userid = $_SESSION['userid'];
    else $userid = "";
    if(isset($_SESSION['firstname'])) $firstname = $_SESSION['firstname'];
    else $firstname = "";
    if(isset($_SESSION['lastname'])) $lastname = $_SESSION['lastname'];
    else $lastname = "";
    $titleadd = $_SESSION['titleadd'];
    if(!isset($_SESSION['firstVisit']))
    {
        $_SESSION['firstVisit'] = 'Been Here';
        $firstTime = 1;
    }
    else $firstTime = 0;

    if(isset($_SESSION['keywords']))
    {
        $keywords = $_SESSION['keywords'];
        unset($_SESSION['keywords']);
    }
    else $keywords = '';
    if(isset($_SESSION['robots']))
    {
        $robots = $_SESSION['robots'];
        unset($_SESSION['robots']);
    }
    else $robots = '';
    if(isset($_SESSION['description']))
    {
        $description = $_SESSION['description'];
        unset($_SESSION['description']);
    }
    else $description = '';
?>
<!DOCTYPE html>
<html>
    <head>
		<title>1800vehicles.com<?php if(isset($titleadd) && (strlen($titleadd) > 0)) echo ' - '.$titleadd?></title>
		<link href="<?php echo WEB_SERVER_NAME;?>common/layout/favicon.ico" rel="SHORTCUT ICON" />
		<?php if($_SESSION['substate']==6){?>
			<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<?php }else{?>
			<link href="<?php echo WEB_SERVER_NAME;?>style.css" rel="stylesheet" type="text/css" />
		<?php }?>		
		<link href="<?php echo WEB_SERVER_NAME;?>screen.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="<?php echo WEB_SERVER_NAME;?>droplinetabs.css" rel="stylesheet" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo WEB_SERVER_NAME;?>common/scripts/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="<?php echo WEB_SERVER_NAME;?>common/scripts/easySlider1.7.js"></script>
		<script type="text/javascript" src="<?php echo WEB_SERVER_NAME;?>common/scripts/master.js"></script>
		<script type="text/javascript" src="<?php echo WEB_SERVER_NAME;?>common/scripts/jquery.hoverIntent.minified.js"></script>
		<?php
		if(isset($_SESSION['addheaderscript'])) {
			echo $_SESSION['addheaderscript'];
			unset($_SESSION['addheaderscript']);
		}
		?>

    <meta charset="utf-8">
	<meta name="description" content="<?php echo $description; ?>" />
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<meta name="robots" content="<?php echo $robots; ?>" />
