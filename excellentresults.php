<?php require("globals.php"); ?>


<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = 'Excellent Availability';

    $type = $_REQUEST["typelist"];
    $from = $_REQUEST["fromselect"];
    $style = $_REQUEST["stylelist"];
    $make = $_REQUEST["makeitem"];
    if(isset($_REQUEST["beginat"])) $beginat = $_REQUEST["beginat"];
    else $beginat = 0;
    $atatime = 25;

    if(!isset($type) || !isset($from) || !isset($style)) header('Location: excellent.php');
    switch($style)
    {
        //case "Don't Care":
        case "4 Door Sedan":
            $styleadd = " and v.doors=4 and v.convertible='No'";
            break;
        case "2 Door Coupe":
            $styleadd = " and v.doors=2 and v.convertible='No'";
            break;
        case "2 Door Convertible":
            $styleadd = " and v.doors=2 and v.convertible='Yes'";
            break;
        case "3 Door Hatchback":
            $styleadd = " and v.doors=3 and v.convertible='No' and v.bodytype='Hatchback'";
            break;
        case "5 Door Hatchback":
            $styleadd = " and v.doors=5 and v.convertible='No' and v.bodytype='Hatchback'";
            break;
        case "5 Door Wagon":
            $styleadd = " and v.doors=5 and v.convertible='No' and v.bodytype!='Hatchback'";
            break;
        case "2 Door 2WD":
            $styleadd = " and v.doors=2 and v.wheeldrive='2'";
            break;
        case "2 Door 4WD":
            $styleadd = " and v.doors=2 and v.wheeldrive='4'";
            break;
        case "4 Door 2WD":
            $styleadd = " and v.doors=4 and v.wheeldrive='2'";
            break;
        case "4 Door 4WD":
            $styleadd = " and v.doors=4 and v.wheeldrive='4'";
            break;
        case "Dual Sliding Doors":
            $styleadd = " and v.slidingdoors='Dual'";
            break;
            break;
    }

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select m.name 'Make', count(*) 'Amount' from vehicles v, vehicledata e, makes m where e.lowmiles=0 and m.makeid=v.makeid and v.vehicleid=e.vehicleid and v.visible=1 and e.bestbuy = 1";
        if(isset($type)) $query .= " and v.type='".$type."'";
        if(isset($from) && ($from != 'Any')) $query .= " and m.origin='".$from."'";
        if(isset($styleadd)) $query .= $styleadd;
        if(isset($make)) $query .= " and m.name='".$make."'";
        $query .= " group by m.name order by 1";
        $result = mysql_query($query);
        $index = 1;
        while($result && $row = mysql_fetch_array($result))
        {
            $makes[$index] = $row['Make'];
            $amounts[$index] = $row['Amount'];
            $index++;
        }

        $query = "select count(*) 'Total' from vehicles v, vehicledata e, makes m where e.lowmiles=0 and m.makeid=v.makeid and v.vehicleid=e.vehicleid and v.visible=1 and e.bestbuy=1";
        if(isset($type)) $query .= " and v.type='".$type."'";
        if(isset($from) && ($from != 'Any')) $query .= " and m.origin='".$from."'";
        if(isset($styleadd)) $query .= $styleadd;
        if(isset($make)) $query .= " and m.name='".$make."'";
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $howmany = $row['Total'];
        }

        $query = "select m.name 'Make', v.year 'Year', v.model 'Model', v.style 'Style', e.pricestart 'PS', e.priceend 'PE', e.mileagestart 'MS', e.mileageend 'ME', v.vehicleid 'VID', v.ImageFile 'Image' from vehicles v, vehicledata e, makes m where e.lowmiles=0 and m.makeid=v.makeid and v.vehicleid=e.vehicleid and v.visible=1 and e.bestbuy=1";
        if(isset($type)) $query .= " and v.type='".$type."'";
        if(isset($from) && ($from != 'Any')) $query .= " and m.origin='".$from."'";
        if(isset($styleadd)) $query .= $styleadd;
        if(isset($make)) $query .= " and m.name='".$make."'";
        $query .= " order by 2,1,3,4,5,7";
        $query .= ' limit '.$beginat.','.$atatime;
        $result = mysql_query($query);
        $index = 1;
        while($result && $row = mysql_fetch_array($result))
        {
            $makedets[$index] = $row['Make'];
            $yeardets[$index] = $row['Year'];
            $modeldets[$index] = $row['Model'];
            $styledets[$index] = $row['Style'];
            $psdets[$index] = $row['PS'];
            $pedets[$index] = $row['PE'];
            $msdets[$index] = $row['MS'];
            $medets[$index] = $row['ME'];
            $vehid[$index] = $row['VID'];
            $vimage[$index] = $row['Image'];
            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border-top: none;
}
</style>
<div class="gridtwelve"></div>
<div id="content" class="excellent-top">
<div class="grideightcontainer">
<h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Excellent Availability Results</h1>
<div class="grideightgrey">
<p class="blackeleven" style="margin: 0;"><a href="researchvehicles.php">Go back to Research Vehicles</a></p>
<p class="blackeleven" style="margin: 0;">
<?php
        echo '<a href="excellent.php';
        if(isset($type) || isset($from) || isset($style)) echo '?';
        if(isset($type))
        {
            echo 'typeitem='.$type;
            if(isset($from) || isset($style)) echo '&';
        }
        if(isset($from))
        {
            echo 'fromselect='.$from;
            if(isset($style)) echo '&';
        }
        if(isset($style)) echo 'styleitem='.$style;
        echo '">Start the search over with new criteria</a>';
?>
</p>
<!--a href="#"><img src="common/layout/print.gif" border="0" align="right" hspace="0" alt="Print this page." /></a-->
<br />
<?php //echo $query.'<br/>';?>
<?php //echo $model.'<br/>';?>
<?php //echo $_SERVER['QUERY_STRING'].'<br/>';?>
        <!--form action="excellentresults.php" method="get" name="excellent"-->
        <table class="table borderless" class="searchmake">
<?php
    $count = count($makes);
    if($count < 1)
    {
        echo '<tr><td>No results found.</td></tr>';
    }
    else
    {
        $row = 1;
        $col = 1;
        while((($row*4)-4)<$count)
        {
            echo '<tr>';
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '><a href="excellentresults.php?makeitem='.$makes[$col];
                if(isset($type)) echo '&typelist='.$type;
                if(isset($from)) echo '&fromselect='.$from;
                if(isset($style)) echo '&stylelist='.$style;
                echo '">'.$makes[$col].' ('.$amounts[$col].')</a></td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '><a href="excellentresults.php?makeitem='.$makes[$col];
                if(isset($type)) echo '&typelist='.$type;
                if(isset($from)) echo '&fromselect='.$from;
                if(isset($style)) echo '&stylelist='.$style;
                echo '">'.$makes[$col].' ('.$amounts[$col].')</a></td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '><a href="excellentresults.php?makeitem='.$makes[$col];
                if(isset($type)) echo '&typelist='.$type;
                if(isset($from)) echo '&fromselect='.$from;
                if(isset($style)) echo '&stylelist='.$style;
                echo '">'.$makes[$col].' ('.$amounts[$col].')</a></td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '><a href="excellentresults.php?makeitem='.$makes[$col];
                if(isset($type)) echo '&typelist='.$type;
                if(isset($from)) echo '&fromselect='.$from;
                if(isset($style)) echo '&stylelist='.$style;
                echo '">'.$makes[$col].' ('.$amounts[$col].')</a></td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            echo '</tr>';
            $row++;
        }
    }
?>
        </table>
        <!--/form-->
        <br />
<p class="blackeleven" style="margin: 0;">
<?php
    if(isset($make))
    {
        echo '<a href="excellentresults.php';
        if(isset($type) || isset($from) || isset($style)) echo '?';
        if(isset($type))
        {
            echo 'typelist='.$type;
            if(isset($from) || isset($style)) echo '&';
        }
        if(isset($from))
        {
            echo 'fromselect='.$from;
            if(isset($style)) echo '&';
        }
        if(isset($style)) echo 'stylelist='.$style;
        echo '">Remove '.$make.' as criteria</a>';
    }
?>
</p>
        <p class="blackeleven" style="color:#142c3c;">Prices continually fall, therefore an actual quote may be lower than the price ranges shown.  Price ranges shown do not include tax, title, or license.</p>

        </div><!-- end grideightgrey-->

        <div class="grideight">
<?php
    if($howmany > 0)
    {
        echo '<p class="searchheaderc">';
        echo 'Showing '.($beginat+1).'-';
        if(($beginat+$atatime) < $howmany) echo ($beginat+$atatime);
        else echo $howmany;
        echo ' of '.$howmany;
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &lt; ';
        if($beginat > 0)
        {
            echo '<a href="excellentresults.php?';
            echo 'beginat='.($beginat-$atatime);
            if(isset($make) || isset($type) || isset($from) || isset($style)) echo '&';
            if(isset($make))
            {
                echo 'makeitem='.$make;
                if(isset($type) || isset($from) || isset($style)) echo '&';
            }
            if(isset($type))
            {
                echo 'typelist='.$type;
                if(isset($from) || isset($style)) echo '&';
            }
            if(isset($from))
            {
                echo 'fromselect='.$from;
                if(isset($style)) echo '&';
            }
            if(isset($style)) echo 'stylelist='.$style;
            echo '">Previous</a>';
        }
        if(($beginat > 0) && ($beginat+$atatime < $howmany)) echo ' | ';
        if($beginat+$atatime < $howmany)
        {
            echo '<a href="excellentresults.php?';
            echo 'beginat='.($beginat+$atatime);
            if(isset($make) || isset($type) || isset($from) || isset($style)) echo '&';
            if(isset($make))
            {
                echo 'makeitem='.$make;
                if(isset($type) || isset($from) || isset($style)) echo '&';
            }
            if(isset($type))
            {
                echo 'typelist='.$type;
                if(isset($from) || isset($style)) echo '&';
            }
            if(isset($from))
            {
                echo 'fromselect='.$from;
                if(isset($style)) echo '&';
            }
            if(isset($style)) echo 'stylelist='.$style;
            echo '">Next</a>';
        }
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &gt; ';
        echo '</p>';

        $numToShow = count($makedets);
        for($index = 1; $index <= $numToShow; $index++)
        { ?>
            <table class="table borderless" bgcolor="#ffffff" style="font-size:14px; font-weight:bold; color:#757575;">
            <tr>
          <td style="width: 100%">
              
              <div>
                  <div style="float: left">
                       <a href="details.php?vehid=<?php echo $vehid[$index];?>" target="_blank">
            
                  <?php
             if(!file_exists($vimage[$index])) $imagefile = '';
            else $imagefile = $vimage[$index];
          
            if($imagefile)
            {
                $max_width = 250;
                $max_height = 250;
                echo '<img id="vehimage" class="img-responsive" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            }
            else echo '** Image currently not available **'; ?>
            </a>
                      
                  </div>
                  <div class="clearboth"></div>
                  <!--<div style="float: left;margin-right: 20px;margin-left: 10px;">-->
                  <div style="float: left;">
                      <a href="details.php?vehid=<?php echo $vehid[$index];?>" target="_blank">
            <strong>
                <?php 
            echo $yeardets[$index].' '.$makedets[$index].' '.$modeldets[$index].' '.$styledets[$index]; ?>
            </strong></a><br /><span style="color:#85c11b;">Price:</span>
            <?php
            if($psdets[$index] < 5000) echo 'Below $'.number_format($pedets[$index]);
            else echo '$'.number_format($psdets[$index]).' to $'.number_format($pedets[$index]);
            ?>
            
            <br /><span style="color:#85c11b;">Mileage:</span>
            <?php
            echo number_format($msdets[$index]);
            echo ' - ';
            echo number_format($medets[$index]);
            ?>
                      
                  </div>
                  <div class="clearboth"></div>
                    <?php
    if(isset($_SESSION['user'])){
    ?>
                  <div style="float: left;margin-top: 20px;margin-left: 10px;">
                      <form action="favorites.php" method="post" name="saveform'.$vehid[$index].'">
                          <input type="hidden" value="<?php echo $vehid[$index]?>" name="AddVehicleID" />
            <button type="submit" class="med">SAVE</button>
            </form> 
                  </div>
    <?php } ?>
                  
                  
              </div>
              
              
              
           
            </td>
          
          <?php if($index < $numToShow) echo '<tr><td colspan="3" align="center" ><img style="width:100%" src="common/layout/short-bar.gif" /></td></tr>'; ?>
            </table>
            
            <?php
        }

        echo '<p class="searchheaderblk">';
        echo 'Showing '.($beginat+1).'-';
        if($beginat+$atatime < $howmany) echo ($beginat+$atatime);
        else echo $howmany;
        echo ' of '.$howmany;
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &lt; ';
        if($beginat > 0)
        {
            echo '<a href="excellentresults.php?';
            echo 'beginat='.($beginat-$atatime);
            if(isset($make) || isset($type) || isset($from) || isset($style)) echo '&';
            if(isset($make))
            {
                echo 'makeitem='.$make;
                if(isset($type) || isset($from) || isset($style)) echo '&';
            }
            if(isset($type))
            {
                echo 'typelist='.$type;
                if(isset($from) || isset($style)) echo '&';
            }
            if(isset($from))
            {
                echo 'fromselect='.$from;
                if(isset($style)) echo '&';
            }
            if(isset($style)) echo 'stylelist='.$style;
            echo '">Previous</a>';
        }
        if(($beginat > 0) && ($beginat+$atatime < $howmany)) echo ' | ';
        if($beginat+$atatime < $howmany)
        {
            echo '<a href="excellentresults.php?';
            echo 'beginat='.($beginat+$atatime);
            if(isset($make) || isset($type) || isset($from) || isset($style)) echo '&';
            if(isset($make))
            {
                echo 'makeitem='.$make;
                if(isset($type) || isset($from) || isset($style)) echo '&';
            }
            if(isset($type))
            {
                echo 'typelist='.$type;
                if(isset($from) || isset($style)) echo '&';
            }
            if(isset($from))
            {
                echo 'fromselect='.$from;
                if(isset($style)) echo '&';
            }
            if(isset($style)) echo 'stylelist='.$style;
            echo '">Next</a>';
        }
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &gt; ';
        echo '</p>';
    }
    else
    {
        echo '<p class="searchheaderc">Sorry! No results matched the criteria.</p>';
        echo '<a href="excellent.php';
        if(isset($type) || isset($from) || isset($style)) echo '?';
        if(isset($type))
        {
            echo 'typeitem='.$type;
            if(isset($from) || isset($style)) echo '&';
        }
        if(isset($from))
        {
            echo 'fromselect='.$from;
            if(isset($style)) echo '&';
        }
        if(isset($style)) echo 'styleitem='.$style;
        echo '">Start the search over with new criteria</a>';
    }
?>
        </div><!-- grid eight -->
    </div><!-- grid eight container -->
<?php
    $_SESSION['hideteaser3'] = 'true';
    require("teaser.php");
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php // require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>

