<?php require("globals.php"); ?>
<?php
    require_once(WEB_ROOT_PATH.'common/functions/string.php');
    $_SESSION['state'] = 2;
    $_SESSION['substate'] = 6;
    $_SESSION['titleadd'] = 'Post to the Group Watchlist';

    $loaderror = 'false';
    if(!isset($_POST['FirmQuoteID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000206';
        header('Location: posttogroup.php');
        exit();
    }

    $firmquoteids = implode(",", $_POST['FirmQuoteID']);

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select q.QuoteRequestID, q.Year, q.Make, q.Model, q.Style, f.LastUpdated, q.QuoteType, f.OrderType, f.PricePoint, f.FirmQuoteID from quoterequests q,firmquotes f where f.QuoteRequestID=q.QuoteRequestID and f.FirmQuoteID in (".$firmquoteids.")";
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $fquoteid[$index] = $row[0];
            $fyear[$index] = $row[1];
            $fmake[$index] = $row[2];
            $fmodel[$index] = $row[3];
            $fstyle[$index] = $row[4];
            $fupdated[$index] = $row[5];
            $fquotetype[$index] = $row[6];
            $fordertype[$index] = $row[7];
            $fprice[$index] = $row[8];
            $ffirmid[$index] = $row[9];

            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function validateFormOnSubmit()
    {
        var reason = "";

        var voterm = document.getElementById("orderterm");
        var vsterm = document.getElementById("specificterm");
        if(!(voterm.checked && vsterm.checked))
        {
            alert("Both checkboxes must be selected so we know you have reviewed the terms."+'\n');
            return false;
        }
        return true;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
<div class="grideightcontainer">
<?php
    if($loaderror != 'false')
    {
        echo '<h1 class="subhead">Error on Order Creation</h1>';
        echo '<div class="grideightgrey">';
        echo '<p class="blackfourteen"><strong>There was an issue creating the order:</strong></p>';
        echo '<p class="blackfourteen">'.$loaderror.'</p>';
    }
    else
    {
        $count = count($fquoteid);
        echo '<h1 class="subhead" style="width: 275px;">Post to  Group Watchlist</h1>';
        echo '<div class="grideightgrey">';
        echo '<table border="0" cellpadding="5" cellspacing="0" width="600">';
        echo '<tbody><tr>';
        //echo '<td align="center" width="50"><h3 class="greensub">No.</h3></td>';
        $plural = pluralize_noun($count, "CURRENT MARKET STUD", "IES", "Y");
        echo '<td width="250"><h3 class="greensub">'.$plural.'</h3></td>';
        echo '<td align="center" width="100"><h3 class="greensub">STATUS</h3></td>';
        echo '<td align="center" width="100"><h3 class="greensub">ORDER TYPE</h3></td>';
        echo '<td align="center" width="100"><h3 class="greensub">PRICE</h3></td>';
        echo '<td width="95"><h3 class="greensub">&nbsp;</h3></td>';
        echo '</tr>';
        for($i=0;$i<$count;$i++)
        {
            echo '<tr>';
            //echo '<td align="center">';
            //echo '<label for="select3">';
            //echo '</label><p class="greyeleven">';
            //if(isset($fquoteid[$i])) echo str_pad($fquoteid[$i],5,"0",STR_PAD_LEFT);
            //echo '</p><br />';
            //echo '</td>';
            echo '<td><p class="formbluetext">';
            echo '<a href="quotereceived.php?QuoteID='.$fquoteid[$i].'">';
            echo $fyear[$i].' '.$fmake[$i].' '.$fmodel[$i].' '.$fstyle[$i];
            echo '</a>';
            echo '</p>';
            echo '<p class="greyeleven">';
            echo date_at_timezone('m/d/Y', 'EST', $fupdated[$i]);
            echo '</p></td>';
            echo '<td align="center"><p class="greyeleven">Received</p></td>';
            echo '<td align="center"><p class="greyeleven">';
            //echo $fordertype[$i];
            echo 'Group Watchlist';
            echo '</p></td>';
            echo '<td align="center"><p class="greyeleven">$'.number_format($fprice[$i]).'</p></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';
        echo '</div><!-- end grideightgrey-->';
?>
<h2 class="subhead" style="width: 235px;">Group Watchlist Agreement</h2>
<div class="grideight" style="width: 615px; margin-left: 20px;">
<?php
    $aname = 'Group Watchlist Posting';
    $pname = 'Specific Vehicle Purchase';

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select AgreementID from agreements where AgreementName='".$aname."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $aid = $row[0];

            $query = "select SectionName, SectionText from agreementsections where AgreementID=".$aid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $secname[$index] = $row[0];
                $sectext[$index] = $row[1];
                $index++;
            }
        }

        $query = "select AgreementID from agreements where AgreementName='".$pname."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $pid = $row[0];

            $query = "select SectionName, SectionText from agreementsections where AgreementID=".$pid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $psecname[$index] = $row[0];
                $psectext[$index] = $row[1];
                $index++;
            }
        }

        mysql_close($con);
    }

    $count = count($secname);
    ini_set('display_errors','on');
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($sectext[$index]);
        //echo '<p class="blackfourteen"><strong>'.strtoupper($secname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen"><strong>'.strtoupper($secname[$index]).'</strong> - '.$sectext[$index].'</p>';
    }

    echo '</div><!--end grideight-->';
    echo '<h2 class="subhead" style="width: 335px;">Specific Vehicle Purchase Agreement</h2>';
    echo '<div class="grideight" style="padding:5px;">';

    $count = count($psecname);
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($psectext[$index]);
        //echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($psecname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($psecname[$index]).'</strong> - '.$psectext[$index].'</p>';
    }
?>
<p class="blackfourteen" style="color: rgb(133, 193, 27);"><strong>THANK YOU FOR GIVING US AN OPPORTUNITY TO SERVE YOU! </strong></p>
</div><!-- end grideight-->

<div class="grideightgrey">
    <table cellpadding="3" cellspacing="3">
        <tbody>
            <tr valign="bottom">
                <td width="20">
                    <form action="posttogroupcomplete.php" onsubmit="javascript:return validateFormOnSubmit()" method="post">
                    <input id="orderterm" name="orderterm" value="Yes" type="checkbox" checked="checked" />
                </td>
                <td colspan="3">I agree to the Group Watchlist Agreement terms</td>
            </tr>
            <tr valign="bottom">
                <td width="20">
                    <input id="specificterm" name="specificterm" value="Yes" type="checkbox" checked="checked" />
                </td>
                <td colspan="3">I have read the Specific Vehicle Purchase Agreement and understand its terms</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2">
                    <form action="watchlist.php" method="post">
                        <button value="" class="med"><nobr>PLACE ON GROUP WATCHLIST</nobr></button>
<?php
                    echo '<input type="hidden" value="'.$firmquoteids.'" name="FirmQuoteID[]" />';
?>
                    </form>
                </td>
                <td width="281">
                    <form action="posttogroup.php" method="post"><button value="" class="med"><nobr>CANCEL</nobr></button></form>
                </td>
            </tr>
        </tbody>
    </table>
</div><!-- end grideightgrey-->
<?php
    }
?>
</div><!-- end grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
