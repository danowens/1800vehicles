<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 3;
    $_SESSION['titleadd'] = "All Wholesale Inventory";

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select i.InStockID, i.PricePaid, i.PriceWanted, v.VIN, v.Year, v.Model, m.Name, v.Mileage from instock i,vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=i.VehicleDetailID and i.Available=1";
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $svstockID[$index] = $row[0];
            $svprice[$index] = $row[1];
            $svwanted[$index] = $row[2];
            $svvin[$index] = $row[3];
            $svyear[$index] = $row[4];
            $svmodel[$index] = $row[5];
            $svmake[$index] = $row[6];
            $svmiles[$index] = $row[7];
            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

<style>
    button.blueongrey {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
}
</style>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;"> All Wholesale Inventory</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: 0px;">
                <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
<?php
    $count = count($svstockID);
    if($count > 0)
    {
?>
                <form action="allwholesale.php" method="post">
                    <input type="hidden" value="true" name="PostBack" />
                    <table class="table">
                        <tr valign="baseline">
                            <td width="50"><label for="yearlist"><strong>Year</strong></label></td>
                            <td width="100">
                                <select name="yearlist" id="yearlist" >
                                    <option value="">All</option>
                                </select>
                            </td>
                            <td width="50">&nbsp;</td>
                            <td width="100">&nbsp;</td>
                        </tr>
                        <tr valign="baseline">
                            <td><label for="makelist"><strong>Make</strong></label></td>
                            <td>
                                <select name="makelist" id="makelist" >
                                    <option value="">All</option>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr valign="baseline">
                            <td><label for="modellist"><strong>Model</strong></label></td>
                            <td>
                                <select name="modellist" id="modellist">
                                    <option value="">All</option>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr valign="baseline">
                            <td><label for="milestart"><strong>Mileage</strong></label></td>
                            <td>
                                <select name="milestart" id="milestart">
                                    <option value="">0</option>
                                </select>
                            </td>
                            <td><label for="mileend"><strong>To</strong></label></td>
                            <td>
                                <select name="mileend" id="mileend">
                                    <option value="">0</option>
                                </select>
                            </td>
                        </tr>
                        <tr valign="baseline">
                            <td><label for="pricestart"><strong>Price</strong></label></td>
                            <td>
                                <select name="pricestart" id="pricestart">
                                    <option value="">Any</option>
                                </select>
                            </td>
                            <td><label for="priceend"><strong>To</strong></label></td>
                            <td>
                                <select name="priceend" id="priceend">
                                    <option value="">Any</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <button type="submit" value="" class="med">SEARCH CURRENT INVENTORY</button>
                    <br/>
                    <br/>
                </form>
<?php
    }
?>
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
<?php
    if(isset($_POST['PostBack']))
    {
        echo ' <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;"> Results</h1>';
        echo '<div class="grideightgrey">';
        echo '<div class="grideight" style="margin-top: 0px;">';
        if($count < 1)
        {
            echo '<p class="blackeleven" style="margin: 0; float:right;">No results match the current criteria</p>';
        }
        else
        {
            echo '<table class="table">';
            echo '<tr style="font-size:15px;">';
            echo '<td  align="center" style="color:#85c11b;"><strong>YEAR/MAKE/MODEL</strong></td>';
            echo '<td align="center" style="color:#85c11b;"><strong>MILEAGE</strong></td>';
            echo '<td  align="center" style="color:#85c11b;"><strong>ASKING PRICE</strong></td>';
            echo '<td  align="center">&nbsp;</td>';
            echo '<td  align="center">&nbsp;</td>';
            echo '</tr>';
            for($i=0;$i<$count;$i++)
            {
                echo '<tr valign="baseline">';
                echo '<td><strong>';
                //echo '<a href="#">';
                echo $svyear[$i].' '.$svmake[$i].' '.$svmodel[$i];
                //echo '</a>';
                echo '</strong></td>';
                echo '<td align="center"><strong>';
                echo number_format($svmiles[$i]);
                echo '</strong></td>';
                echo '<td align="center"><strong>$';
                echo number_format($svwanted[$i]);
                if(!is_null($svprice[$i])) echo ' [Paid: $'.number_format($svprice[$i]).']';
                echo '</strong></td>';
                echo '<td align="center">';
                //echo '<form action="editinventory.php" method="post">';
                //echo '<input type="hidden" value="'.$svstockID[$i].'" name="InStockID" />';
                echo '<button type="submit" value="" class="blueongrey"><img alt="Edit" title="Edit Inventory Item" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                //echo '</form>';
                echo '</td>';
                echo '<td align="center">';
                //echo '<form action="delinventory.php" method="post">';
                //echo '<input type="hidden" value="'.$svstockID[$i].'" name="InStockID" />';
                echo '<button type="submit" value="" class="blueongrey"><img alt="Delete" title="Remove Inventory from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                //echo '</form>';
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
        echo '<br/>';
        echo '<br/><br/><p class="blackeleven" style="margin: 0; float:right;"><a href="dashboard.php">Go to MyDashboard</a></p></div><!-- endgrideight --></div> <!-- endgrideightgrey -->';
    }
?>
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
