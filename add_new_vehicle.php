<?php 
ob_start();
require("globals.php"); 
require_once("common/functions/DB.php");
require_once("common/functions/Vehicle.php");

require("headerstart.php"); 
require("header.php"); 
require("foursteps.php"); 
$db = DB::init();
$makes = Vehicle::getMakes($db);

$BlackBookAvg = $_POST['BlackBookAvg'];
$Year = $_POST['Year'];
$MakeID = $_POST['MakeID'];
$Model = $_POST['Model'];
$Style = $_POST['Style'];
$Type = $_POST['Type'];
$Hybrid = $_POST['Hybrid'];
$Size = $_POST['Size'];
$Convertible = $_POST['Convertible'];
$WheelDrive =  $_POST['WheelDrive'];
$Doors =  $_POST['Doors'];
$BodyType = $_POST['BodyType'];
$low_mileage = $_POST['low_mileage'];
$avg_mileage = $_POST['avg_mileage'];
$visibility =  $_POST['visibility'];
if($visibility == 'visible') {
	$visibility = 1;
}
else {
	$visibility = 0;
}
$image_file = $_FILES['image_file'];
$internal_images = $_FILES['internal_image'];

if(isset($_POST['save'])) {
	$values = array(
	
		'Visible' => $visibility,
		'MakeID' => $MakeID,
        'Year'  => 	$Year,
		'Model'	=>	$Model,
        'Style'	=>	$Style,
        'Type'	=>	$Type,
        'Doors'	=>	$Doors,
        'Hybrid' =>	$Hybrid,
        'Convertible' => $Convertible,	
        'WheelDrive' => $WheelDrive,
        'BodyType' => $BodyType,
		'BlackBookAvg' => $BlackBookAvg,
		'Size'	=>	$Size,
		
    );
	
	  $vehicle_id = Vehicle::add($db, $values);
	//  echo $vehicle_id;
	  
			if($image_file){
				$temp_file_name = filter_var($_FILES['image_file']['tmp_name'], FILTER_UNSAFE_RAW);
                $orig_file_name = filter_var($_FILES['image_file']['name'], FILTER_UNSAFE_RAW);
                $info = pathinfo($orig_file_name);
                $ext = strtolower($info['extension']);
                $save_file_name = "vehimages/vehicle$vehicle_id.$ext";

                if (($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe")) {
                    if (move_uploaded_file($temp_file_name, $save_file_name)) {
                        // update the DB if necessary
							$values = array('ImageFile' => $save_file_name);
							$save_img = Vehicle::update($db, $vehicle_id, $values);
							if($save_img)
							{	
								echo "save";
							}
							else {
								"not saved";
							}
                        
                    } else {
                        $error = "Error saving image file";
                    }
                } else {
                    $error = "The image file must be in GIF, JPEG or PNG format.";
                }
				
		}
		
		
		if ($internal_images) {
            $no_images = count($_FILES['internal_image']['name']);
           
            for ($i = 0; $i < $no_images; $i++) {
                if ($_FILES['internal_image']['error'][$i] != UPLOAD_ERR_OK) {
                    echo'<pre>';print_r("here");echo'</pre>';die('here');
                    $error = "Error uploading image file (error code {$_FILES['image_file']['error'][$i]})";
                } else {

                    $temp_file_name = filter_var($_FILES['internal_image']['tmp_name'][$i], FILTER_UNSAFE_RAW);
                   
                    $orig_file_name = filter_var($_FILES['internal_image']['name'][$i], FILTER_UNSAFE_RAW);
                    $info = pathinfo($orig_file_name);
                    $ext = strtolower($info['extension']);
                    $save_file_name = "vehimages/vehicle_".  time()."_".$i."_".$vehicle_id.".".$ext;

                    if (($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe")) {
                        if (move_uploaded_file($temp_file_name, $save_file_name)) {
                            // update the DB if necessary
                           $values=array("vehilce_id"=>$vehicle_id,"image"=>$save_file_name);
                          Vehicle::add_internal_images($db, $values);
                        } else {
                            $error = "Error saving image file";
                        }
                    } else {
                        $error = "The image file must be in GIF, JPEG or PNG format.";
                    }
                }
            }
        }
		
		
header('location: /allvehicles.php');
	
}

elseif(isset($_POST['save_add_another'])) {

$values = array(

		'Visible' => $visibility,
		'MakeID' => $MakeID,
        'Year'  => 	$Year,
		'Model'	=>	$Model,
        'Style'	=>	$Style,
        'Type'	=>	$Type,
        'Doors'	=>	$Doors,
        'Hybrid' =>	$Hybrid,
        'Convertible' => $Convertible,	
        'WheelDrive' => $WheelDrive,
        'BodyType' => $BodyType,
		'BlackBookAvg' => $BlackBookAvg,
		'Size'	=>	$Size,
		
    );
	
		$vehicle_id = Vehicle::add($db, $values);
		
		if($image_file){
				$temp_file_name = filter_var($_FILES['image_file']['tmp_name'], FILTER_UNSAFE_RAW);
                $orig_file_name = filter_var($_FILES['image_file']['name'], FILTER_UNSAFE_RAW);
                $info = pathinfo($orig_file_name);
                $ext = strtolower($info['extension']);
                $save_file_name = "vehimages/vehicle$vehicle_id.$ext";

                if (($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe")) {
                    if (move_uploaded_file($temp_file_name, $save_file_name)) {
                        // update the DB if necessary
							$values = array('ImageFile' => $save_file_name);
							$save_img = Vehicle::update($db, $vehicle_id, $values);
							if($save_img)
							{	
								echo "save";
							}
							else {
								"not saved";
							}
                        
                    } else {
                        $error = "Error saving image file";
                    }
                } else {
                    $error = "The image file must be in GIF, JPEG or PNG format.";
                }
				
		}
		
		
		if ($internal_images) {
            $no_images = count($_FILES['internal_image']['name']);
           
            for ($i = 0; $i < $no_images; $i++) {
                if ($_FILES['internal_image']['error'][$i] != UPLOAD_ERR_OK) {
                    echo'<pre>';print_r("here");echo'</pre>';die('here');
                    $error = "Error uploading image file (error code {$_FILES['image_file']['error'][$i]})";
                } else {

                    $temp_file_name = filter_var($_FILES['internal_image']['tmp_name'][$i], FILTER_UNSAFE_RAW);
                   
                    $orig_file_name = filter_var($_FILES['internal_image']['name'][$i], FILTER_UNSAFE_RAW);
                    $info = pathinfo($orig_file_name);
                    $ext = strtolower($info['extension']);
                    $save_file_name = "vehimages/vehicle_".  time()."_".$i."_".$vehicle_id.".".$ext;

                    if (($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe")) {
                        if (move_uploaded_file($temp_file_name, $save_file_name)) {
                            // update the DB if necessary
                           $values=array("vehilce_id"=>$vehicle_id,"image"=>$save_file_name);
                          Vehicle::add_internal_images($db, $values);
                        } else {
                            $error = "Error saving image file";
                        }
                    } else {
                        $error = "The image file must be in GIF, JPEG or PNG format.";
                    }
                }
            }
        }
		
		$query = "select * from vehicles where VehicleID=$vehicle_id";
		$list=array();
		$res = $db->query($query);
		while($vehicle = $res->fetch_assoc()){
           $list[]=$vehicle;
       }
?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
		<br />
            <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
            <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">New Vehicle</h1>
            <div class="grideightgrey">
       <br />
	  
<form class="form-horizontal" enctype="multipart/form-data" method="post" id="Vehicle" action="<?php $_SERVER['PHP_SELF']; ?>" role="form">
    <fieldset>
        <input type="hidden" id="Vehicle-element-0" value="SaveNew" name="AddEditType">
            <input type="hidden" id="Vehicle-element-1" value="AllVehicles" name="ReturnTo">
                <div class="form-group">
                    <label for="Vehicle-element-2" class="col-md-4 control-label">Black Book Avg:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="Vehicle-element-2" min="2000" value="<?php echo $list[0]['BlackBookAvg']; ?>" name="BlackBookAvg">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Vehicle-element-3" class="col-md-4 control-label">Year:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="Vehicle-element-3" value="<?php echo $list[0]['Year']; ?>" max="2016" min="2000" name="Year">
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="Vehicle-element-4" class="col-md-4 control-label">Make:</label>
                            <div class="col-md-6">
                                <select class="form-control" id="Vehicle-element-4" name="MakeID">
									<?php foreach ($makes as $key => $make) { 
										
									?>
								<option <?php if($MakeID == $key) { echo 'selected="selected"';} ?> value="<?php echo $key ?>"> <?php echo $make ?></option>
										
									<?php } 
									?>	
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Vehicle-element-5" class="col-md-4 control-label">Model:</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="Vehicle-element-5" value="<?php echo $list[0]['Model']; ?>" name="Model">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Vehicle-element-6" class="col-md-4 control-label">Style:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="Vehicle-element-6" value="<?php echo $list[0]['Style']; ?>" name="Style">
                                </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-7" class="col-md-4 control-label">Type:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-7" onchange="get_size1();" name="Type">
                                            <option <?php if($list[0]['Type'] == 'Auto') { ?> selected <?php } ?> value="Auto">Auto</option>
                                            <option <?php if($list[0]['Type'] == 'MiniVan') { ?> selected <?php } ?> value="MiniVan">MiniVan</option>
                                            <option <?php if($list[0]['Type'] == 'SUV') { ?> selected <?php } ?> value="SUV">SUV</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-8" class="col-md-4 control-label">Hybrid Only:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-8" name="Hybrid">
                                            <option <?php if($list[0]['Hybrid'] == 'Yes') { ?> selected <?php } ?> value="Yes">Yes</option>
                                            <option <?php if($list[0]['Hybrid'] == 'No') { ?> selected <?php } ?> value="No">No</option>
                                            <option <?php if($list[0]['Hybrid'] == 'Either') { ?> selected <?php } ?> value="Either">Either</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-9" class="col-md-4 control-label">Size:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-9" name="Size">
                                            <option <?php if($list[0]['Size'] == 'Sub Compact') { ?> selected <?php } ?> value="Sub Compact">Sub Compact</option>
                                            <option <?php if($list[0]['Size'] == 'Compact') { ?> selected <?php } ?> value="Compact">Compact</option>
                                            <option <?php if($list[0]['Size'] == 'Mid Size') { ?> selected <?php } ?> value="Mid Size">Mid Size</option>
                                            <option <?php if($list[0]['Size'] == 'Full Size') { ?> selected <?php } ?> value="Full Size">Full Size</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-10" class="col-md-4 control-label">Convertible:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-10" name="Convertible">
                                            <option <?php if($list[0]['Convertible'] == 'N/A') { ?> selected <?php } ?> value="N/A">N/A</option>
                                            <option <?php if($list[0]['Convertible'] == 'No') { ?> selected <?php } ?> value="No">No</option>
                                            <option <?php if($list[0]['Convertible'] == 'Yes') { ?> selected <?php } ?> value="Yes">Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-11" class="col-md-4 control-label">Wheel Drive:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-11" name="WheelDrive">
                                            <option <?php if($list[0]['WheelDrive'] == 'N/A') { ?> selected <?php } ?> value="N/A">N/A</option>
                                            <option <?php if($list[0]['WheelDrive'] == '2') { ?> selected <?php } ?> value="2">2 Wheel Drive</option>
                                            <option <?php if($list[0]['WheelDrive'] == '4') { ?> selected <?php } ?> value="4">4 Wheel Drive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-12" class="col-md-4 control-label">Doors:</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="Vehicle-element-12" value="<?php echo $list[0]['Doors']; ?>" max="5" min="2" name="Doors">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Vehicle-element-13" class="col-md-4 control-label">Body Type:</label>
                                        <div class="col-md-6">
                                            <select class="form-control" id="Vehicle-element-13" name="BodyType">
                                                <option <?php if($list[0]['BodyType'] == 'N/A') { ?> selected <?php } ?> value="N/A">N/A</option>
                                                <option <?php if($list[0]['BodyType'] == 'Hatchback') { ?> selected <?php } ?> value="Hatchback">Hatchback</option>
                                                <option <?php if($list[0]['BodyType'] == 'Reg Cab') { ?> selected <?php } ?> value="Reg Cab">Regular Cab</option>
                                                <option <?php if($list[0]['BodyType'] == 'Ext Cab') { ?> selected <?php } ?> value="Ext Cab">Extended Cab</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Vehicle-element-14" class="col-md-4 control-label">Best Buy:</label>
                                        <div class="col-md-6">
                                            <label class="checkbox">
                                                <input type="checkbox" value="lowbest" class="form-control" name="low_mileage" id="Vehicle-element-14-0"> Low Mileage 
                                             </label>
                                                <label class="checkbox">
                                                    <input type="checkbox" value="highbest" class="form-control" name="avg_mileage" id="Vehicle-element-14-1"> Average Mileage 
                                                    </label>
                                         </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Vehicle-element-15" class="col-md-4 control-label">Visibility:</label>
                                                <div class="col-md-6">
                                                    <label class="checkbox">
                                                        <input type="checkbox" value="visible" class="form-control" name="visibility" id="Vehicle-element-15-0" <?php if($list[0]['Visible'] == '1') { ?> checked <?php } ?>> Visible 
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Vehicle-element-16" class="col-md-4 control-label">Cover Image:</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="Vehicle-element-16" class="myclass" name="image_file">
                                                        </div>
                                                 </div>
												  <div class="form-group">
                                                    <label for="Vehicle-element-16" class="col-md-4 control-label">Add New Internal Image:</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="Vehicle-element-16" class="myclass" name="internal_image[]" multiple="multiple" accept="image/*" />
                                                    </div>
                                                 </div>
												<input type="submit" class="btn-primary" name="save" value="Save">
												<input type="submit" class="btn-primary" name="save_add_another" value="Save &amp; Add Another">
                                                    
                                                </fieldset>
							</form>
	   </div>
			
			 <div class="grideightgrey">
                <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Add New Make</h1>
                <br />
				
<form class="form-horizontal" method="post" id="make" action="allvehicleedit.php" role="form">
    <fieldset>
        <input type="hidden" id="make-element-0" value="AddMake" name="AddEditType">
            <div class="form-group">
                <label for="make-element-1" class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="make-element-1" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="make-element-2" class="col-md-4 control-label">Origin</label>
                    <div class="col-md-6">
                        <select class="form-control" id="make-element-2" name="origin">
                            <option value="Import">Import</option>
                            <option value="Domestic">Domestic</option>
                        </select>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit" id="make-element-3" class="btn btn-primary" name="" value="Add New Make">
                    </div>
                </fieldset>
</form>
				
            </div>
			
			
			
	</div>
	
	
</div>

<style>
.new_form {
	display:none;
}

.new_form_div {
	display:none;
}
</style>		
<?php		
}

?>

<style>
    select, textarea, input[type="text"], input[type="password"] {
        background: none repeat scroll 0 0 #fff;
        border: 1px solid #aaaaaa;
        color: #252525;
        cursor: text;
        font-family: Arial,Helvetica,Sans Serif;
        font-size: 13px;
        padding: 0 22px;
        resize: none;
        width: 100%;
        height: 30px;
    }
    .form-control {
        background-color: #fff;
        background-image: none;
        border: 1px solid #5fb796;
        border-radius: 0;
        box-shadow: none;
        color: #555;
        display: block;
        font-size: 14px;
        height: 30px;
        line-height: 1.42857;
        width: 100%;
        padding: 0 15px;
    }
    .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
        margin-top: -10px;
        position: absolute;
    }

</style>

<script type="text/javascript">
    function get_size1() {

        var vtype = document.getElementById("Vehicle-element-7");
        var size = document.getElementById("Vehicle-element-9");
        var html_drop = "";
        $("#Vehicle-element-9").parent().parent().show();
        $("#Vehicle-element-9").empty();

        if (vtype.options[vtype.selectedIndex].value == "SUV")
        {


            html_drop = '<option value="Small">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "Auto")
        {


            html_drop = '<option value="Sub Compact">Sub Compact</option><option value="Compact">Compact</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "MiniVan")
        {

            $("#Vehicle-element-9").parent().parent().hide();
        }
        else {
            $("#Vehicle-element-9").parent().parent().hide();
            html_drop = '<option value="Default">Default</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
    }

    function edit_mode_size() {
        var vtype = document.getElementById("Vehicle-element-7");
        var size = document.getElementById("Vehicle-element-9");
        var selected_siz = "<?php echo $vehicle['Size']; ?>";
       // alert(selected_siz);
        $("#Vehicle-element-9").parent().parent().show();


        // alert(selected_siz);
        var html_drop = "";
        $("#Vehicle-element-9").empty();

        if (vtype.options[vtype.selectedIndex].value == "SUV")
        {

            html_drop = '<option value="Small">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "Auto")
        {

            html_drop = '<option value="Sub Compact">Sub Compact</option><option value="Compact">Compact</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "MiniVan")
        {

            $("#Vehicle-element-9").parent().parent().hide();
        }
      //  alert( $('#Vehicle-element-9 option[value=' + selected_siz + ']'));
       // $('#Vehicle-element-9 option[value=' + selected_siz + ']').attr('selected', 'selected');
        $('#Vehicle-element-9').val(selected_siz);
        
       // $('.id_100 option[value=val2]').attr('selected','selected');
    }
</script>


<div class="gridtwelve new_form_div"></div>
<div id="content" class="new_form">
    <div class="grideightcontainer">
		<br />
            <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
            <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">New Vehicle</h1>
            <div class="grideightgrey">
       <br />
	  
<form class="form-horizontal" enctype="multipart/form-data" method="post" id="Vehicle" action="<?php $_SERVER['PHP_SELF']; ?>" role="form">
    <fieldset>
        <input type="hidden" id="Vehicle-element-0" value="SaveNew" name="AddEditType">
            <input type="hidden" id="Vehicle-element-1" value="AllVehicles" name="ReturnTo">
                <div class="form-group">
                    <label for="Vehicle-element-2" class="col-md-4 control-label">Black Book Avg:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="Vehicle-element-2" min="2000" name="BlackBookAvg">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Vehicle-element-3" class="col-md-4 control-label">Year:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="Vehicle-element-3" max="2016" min="2000" name="Year">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Vehicle-element-4" class="col-md-4 control-label">Make:</label>
                            <div class="col-md-6">
                                <select class="form-control" id="Vehicle-element-4" name="MakeID">
									<?php foreach ($makes as $key => $make) { ?>
										<option value="<?php echo $key ?>"> <?php echo $make ?></option>
									<?php } ?>	
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Vehicle-element-5" class="col-md-4 control-label">Model:</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="Vehicle-element-5" name="Model">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Vehicle-element-6" class="col-md-4 control-label">Style:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="Vehicle-element-6" name="Style">
                                </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-7" class="col-md-4 control-label">Type:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-7" onchange="get_size1();" name="Type">
                                            <option value="Auto">Auto</option>
                                            <option value="MiniVan">MiniVan</option>
                                            <option value="SUV">SUV</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-8" class="col-md-4 control-label">Hybrid Only:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-8" name="Hybrid">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                            <option value="Either">Either</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-9" class="col-md-4 control-label">Size:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-9" name="Size">
                                            <option value="Sub Compact">Sub Compact</option>
                                            <option value="Compact">Compact</option>
                                            <option value="Mid Size">Mid Size</option>
                                            <option value="Full Size">Full Size</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-10" class="col-md-4 control-label">Convertible:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-10" name="Convertible">
                                            <option value="N/A">N/A</option>
                                            <option value="No">No</option>
                                            <option value="Yes">Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-11" class="col-md-4 control-label">Wheel Drive:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Vehicle-element-11" name="WheelDrive">
                                            <option value="N/A">N/A</option>
                                            <option value="2">2 Wheel Drive</option>
                                            <option value="4">4 Wheel Drive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Vehicle-element-12" class="col-md-4 control-label">Doors:</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="Vehicle-element-12" max="5" min="2" name="Doors">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Vehicle-element-13" class="col-md-4 control-label">Body Type:</label>
                                        <div class="col-md-6">
                                            <select class="form-control" id="Vehicle-element-13" name="BodyType">
                                                <option value="N/A">N/A</option>
                                                <option value="Hatchback">Hatchback</option>
                                                <option value="Reg Cab">Regular Cab</option>
                                                <option value="Ext Cab">Extended Cab</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Vehicle-element-14" class="col-md-4 control-label">Best Buy:</label>
                                        <div class="col-md-6">
                                            <label class="checkbox">
                                                <input type="checkbox" value="lowbest" class="form-control" name="low_mileage" id="Vehicle-element-14-0"> Low Mileage 
                                                </label>
                                                <label class="checkbox">
                                                    <input type="checkbox" value="highbest" class="form-control" name="avg_mileage" id="Vehicle-element-14-1"> Average Mileage 
                                                    </label>
                                         </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Vehicle-element-15" class="col-md-4 control-label">Visibility:</label>
                                                <div class="col-md-6">
                                                    <label class="checkbox">
                                                        <input type="checkbox" value="visible" class="form-control" name="visibility" id="Vehicle-element-15-0"> Visible 
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Vehicle-element-16" class="col-md-4 control-label">Cover Image:</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="Vehicle-element-16" class="myclass" name="image_file">
                                                        </div>
                                                 </div>
												  <div class="form-group">
                                                    <label for="Vehicle-element-16" class="col-md-4 control-label">Add New Internal Image:</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="Vehicle-element-16" class="myclass" name="internal_image[]" multiple="multiple" accept="image/*" />
                                                    </div>
                                                 </div>
												<input type="submit" class="btn-primary" name="save" value="Save">
												<input type="submit" class="btn-primary" name="save_add_another" value="Save &amp; Add Another">
                                                    
                                                </fieldset>
							</form>
	   </div>
			
			 <div class="grideightgrey">
                <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Add New Make</h1>
                <br />
				
<form class="form-horizontal" method="post" id="make" action="allvehicleedit.php" role="form">
    <fieldset>
        <input type="hidden" id="make-element-0" value="AddMake" name="AddEditType">
            <div class="form-group">
                <label for="make-element-1" class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="make-element-1" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="make-element-2" class="col-md-4 control-label">Origin</label>
                    <div class="col-md-6">
                        <select class="form-control" id="make-element-2" name="origin">
                            <option value="Import">Import</option>
                            <option value="Domestic">Domestic</option>
                        </select>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit" id="make-element-3" class="btn btn-primary" name="" value="Add New Make">
                    </div>
                </fieldset>
</form>
				
            </div>
			
			
			
	</div>
	
	
</div><!--end content-->	

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>