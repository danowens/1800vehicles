<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 12;
    $_SESSION['titleadd'] = 'Specific Vehicle Images';

    if(!isset($_REQUEST['SVQID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x020512';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    $svqid = $_REQUEST['SVQID'];

    $errormessage = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select v.Year, m.Name, v.Model, v.Style, v.VIN, v.Mileage, v.Doors, v.ExteriorColor, v.InteriorColor, v.Transmission, v.WheelDriveType, v.EngineCylinders";
        $query .= ",v.SeatMaterial, v.WheelCovers, v.Sunroof, v.RadioFM, v.RadioCassette, v.RadioCD, v.PowerDoors, v.PowerWindows, v.PowerSeats, v.FrontHeatedSeats";
        $query .= ",v.AirConditioning, v.RemoteEntry, v.TractionControl, v.SecuritySystem, v.CruiseControl, v.Navigation, s.PriceQuoted, v.SpecialNotes, v.Information";
        $query .= ", v.ExtraFeatures";
        $query .= " from specificvehicles s, vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=s.VehicleDetailID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        if(!$result || !$row = mysql_fetch_array($result))
        {
            mysql_close($con);
            $_SESSION['ShowError'] = 'Internal Error - 0x020512';
            header('Location: mydashboard.php#admintab');
            exit();
        }

        $svqyear = $row[0];
        $svqmake = $row[1];
        $svqmodel = $row[2];
        $svqstyle = $row[3];
        $svqvin = $row[4];
        $svqmiles = $row[5];
        $svqdoors = $row[6];
        $svqextcolor = $row[7];
        $svqintcolor = $row[8];
        $svqtrans = $row[9];
        $svqwdt = $row[10];
        $svqcyl = $row[11];
        $svqseat = $row[12];
        $svqwtype = $row[13];
        $svqroof = $row[14];
        $svqradfm = $row[15];
        $svqradcas = $row[16];
        $svqradcd = $row[17];
        $svqpdoor = $row[18];
        $svqpwin = $row[19];
        $svqpseat = $row[20];
        $svqhseat = $row[21];
        $svqair = $row[22];
        $svqremote = $row[23];
        $svqtraction = $row[24];
        $svqsecure = $row[25];
        $svqcruise = $row[26];
        $svqnav = $row[27];
        $svqprice = $row[28];
        $svqspecial = $row[29];
        $svqdesc = $row[30];
        $svqextra = $row[31];

        // Determine whether pickup or delivery...
        $svqpickup = 1;
        $svqcity = '';
        $svqstate = '';

        // Check the orders first...
        $query = "select q.Pickup, q.DeliverToCity, q.DeliverToState from specificvehicles s, orderfor o, firmquotes f, quoterequests q where q.QuoteRequestID=f.QuoteRequestID and f.FirmQuoteID=o.FirmQuoteID and o.OrderID=s.OrderID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        while($result && $row = mysql_fetch_array($result))
        {
            if($row[0] == 0)
            {
                $svqpickup = 0;
                $svqcity = $row[1];
                $svqstate = $row[2];
                break;
            }
        }

        // Check the watches next...
        if($svqpickup == 1)
        {
            $query = "select q.Pickup, q.DeliverToCity, q.DeliverToState from specificvehicles s, watchfor w, firmquotes f, quoterequests q where q.QuoteRequestID=f.QuoteRequestID and f.FirmQuoteID=w.FirmQuoteID and w.WatchID=s.WatchID and s.SpecificVehicleID=".$svqid;
            $result = mysql_query($query);
            while($result && $row = mysql_fetch_array($result))
            {
                if($row[0] == 0)
                {
                    $svqpickup = 0;
                    $svqcity = $row[1];
                    $svqstate = $row[2];
                    break;
                }
            }
        }

        // Look for images of the vehicle in question...
        $query = "select count(*) from specificvehicles s, vehicleimages i where s.VehicleDetailID=i.VehicleDetailID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $svqimages = $row[0];
        }

        if($svqimages > 0)
        {
            $query = "select i.ImageFile from specificvehicles s, vehicleimages i where s.VehicleDetailID=i.VehicleDetailID and s.SpecificVehicleID=".$svqid;
            $result = mysql_query($query);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $svqimage[$index] = $row[0];
                $index++;
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width:300px;">Specific Vehicle Images</h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0; margin-left: -5px;"><a href="invdetails.php?SVQID=<?php echo $svqid; ?>&ForUserID=<?php echo $_REQUEST['ForUserID']; ?>&MarketNeedID=<?php echo $_REQUEST['MarketNeedID']; ?>">Go back to Specific Vehicle Details</a></p>
<?php
    $count = count($svqimage);
    for($i=0; $i < $count; $i++)
    {
        if(isset($svqimage[$i]))
        {
            $max_width = 365;
            $max_height = 365;
            echo '<img id="vehimage" src="loadimage.php?image='.$svqimage[$i].'&mwidth='.$max_width.'&mheight='.$max_height.'" style="margin-top:10px;" />';
        }
        else echo '** Image not available **';
        echo '<br/>';
    }
?>
            <p class="blackeleven" style="margin: 0; margin-left: -5px;"><a href="invdetails.php?SVQID=<?php echo $svqid; ?>&ForUserID=<?php echo $_REQUEST['ForUserID']; ?>&MarketNeedID=<?php echo $_REQUEST['MarketNeedID']; ?>">Go back to Specific Vehicle Details</a></p>
        </div><!-- end grideightgrey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
