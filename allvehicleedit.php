<?php require("globals.php"); ?>
<?php
//error_reporting(E_ALL);
error_reporting(0);
ini_set("display_errors", "on");
$_SESSION['state'] = 6;
$_SESSION['substate'] = 4;
$_SESSION['titleadd'] = 'Virtual Vehicle Add/Edit';

// echo'<pre>';print_r($_REQUEST);echo'</pre>';die('here');
require_once("common/functions/DB.php");
require_once("common/functions/Vehicle.php");
require_once("common/PFBC/Form.php");

use PFBC\Form;
use PFBC\Element;

$db = DB::init();
$mode = isset($_REQUEST['AddEditType']) ? $_REQUEST['AddEditType'] : 'AddNew';
$vehicle_id = isset($_REQUEST['vehid']) ? (int) $_REQUEST['vehid'] : 0;
$add_after_save = isset($_REQUEST['save']) ? ($_REQUEST['save'] == 'save_then_add') : false;

$makes = Vehicle::getMakes($db);

$form = new Form("Vehicle");
$form->configure(array(
    "prevent" => array("bootstrap", "jQuery"),
    "resourcesPath" => "common"
));

$blackbookavg = new Element\Textbox("Black Book Avg:", "BlackBookAvg", array("min" => 2000));
$year = new Element\Textbox("Year:", "Year", array("min" => 2000, "max" => date("Y") + 1));
$model = new Element\Textbox("Model:", "Model");
$make = new Element\Select("Make:", "MakeID", $makes);
$style = new Element\Textbox("Style:", "Style");
$type = new Element\Select("Type:", "Type", Vehicle::$TYPES_NEW, array("onChange" => "get_size1();"));
$hybrid = new Element\Select("Hybrid Only:", "Hybrid", array("Yes" => "Yes", "No" => "No", "Either" => "Either"));
$size = new Element\Select("Size:", "Size", array("Sub Compact" => "Sub Compact", "Compact" => "Compact", "Mid Size" => "Mid Size", "Full Size" => "Full Size"));
$convertible = new Element\Select("Convertible:", "Convertible", Vehicle::$CONVERTIBLE);
$wheel_drive = new Element\Select("Wheel Drive:", "WheelDrive", Vehicle::$WHEEL_DRIVE_OPTIONS);
$doors = new Element\Textbox("Doors:", "Doors", array("min" => 2, "max" => 5));
$body_type = new Element\Select("Body Type:", "BodyType", Vehicle::$BODY_TYPE_OPTIONS);
$best_buy = new Element\Checkbox("Best Buy:", "best_buy", array("lowbest" => "Low Mileage", "highbest" => "Average Mileage"));
$visible = new Element\Checkbox("Visibility:", "visible", array("visible" => "Visible"));
$image_file = new Element\File("Image File:", "image_file", array("class" => "myclass"));
$internal_images = new Element\File("Add New Internal Image:", "internal_image[]", array("class" => "myclass", "multiple accept" => "image/*"));

//echo "<pre>\n" . print_r($_REQUEST, true), "</pre>\n";
if ($mode == 'EditFromGroup') {

    $vehicle = Vehicle::getById($db, $vehicle_id);
} else if ($mode == 'EditExisting') {

    if ($vehicle_id == 0) {
        $stylelist = explode(";", $_REQUEST['stylelist']);
        if (!empty($stylelist)) {
            $vehicle_id = (int) $stylelist[0];
        }
    }
    $form->addElement(new Element\Hidden('AddEditType', 'SaveExisting'));
    $vehicle = Vehicle::getById($db, $vehicle_id);
} else if ($mode == 'SaveNew' || $mode == 'SaveExisting') {

    $FIELDS = array(
        'MakeID',
        'Year',
        'Model',
        'Style',
        'Type',
        'Doors',
        'Hybrid',
        'Convertible',
        'WheelDrive',
        'BodyType',
        //'SlidingDoors',
        //'ImageFile',
        //'SmallImageFile',
        'BlackBookAvg',
    );
	
		//print_r($FIELDS);

    $values = array(
        'Visible' => empty($_REQUEST['visible']) ? 0 : (int) in_array("visible", $_REQUEST['visible']),
        'highbest' => empty($_REQUEST['best_buy']) ? 0 : (int) in_array("highbest", $_REQUEST['best_buy']),
        'lowbest' => empty($_REQUEST['best_buy']) ? 0 : (int) in_array("lowbest", $_REQUEST['best_buy']),
    );

    foreach ($FIELDS as $field) {
        if (!isset($_REQUEST[$field])) {
            if (empty($error)) {
                $error = "Missing the following field(s):<ul>\n";
            }
            $error .= "<li>$field</li>\n";
        } else {
            $values[$field] = $_REQUEST[$field];
        }
    }
    
    if(isset($_REQUEST['Size'])){
         $values['Size']=$_REQUEST['Size'];
    }
   
  
    //echo "<pre>Values\n" . print_r($values, true), print_r($_FILES), "</pre>\n";

    if (!empty($error)) {

        $error .= "</ul>\n";
    } else {
        

        if ($mode == 'SaveNew') {


            $vehicle_id = Vehicle::add($db, $values);

            if ($vehicle_id == 0) {
                $error = "Error creating the vehicle";
            } else {
                $vehicle = Vehicle::getById($db, $vehicle_id);
            }
        } else {

            if (!$vehicle_id) {
                $error = "Missing vehicle id argument";
            } else {
                if (!Vehicle::update($db, $vehicle_id, $values)) {
                    $error = "Could not update vehicle_id $vehicle_id";
                }
            }
        }

        if (!$error && count($_FILES) && isset($_FILES['image_file']) && ($_FILES['image_file']['error'] != UPLOAD_ERR_NO_FILE)) {


            if ($_FILES['image_file']['error'] != UPLOAD_ERR_OK) {
                $error = "Error uploading image file (error code {$_FILES['image_file']['error']})";
            } else {

                $temp_file_name = filter_var($_FILES['image_file']['tmp_name'], FILTER_UNSAFE_RAW);
                $orig_file_name = filter_var($_FILES['image_file']['name'], FILTER_UNSAFE_RAW);
                $info = pathinfo($orig_file_name);
                $ext = strtolower($info['extension']);
                $save_file_name = "vehimages/vehicle$vehicle_id.$ext";

                if (($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe")) {
                    if (move_uploaded_file($temp_file_name, $save_file_name)) {
                        // update the DB if necessary
                        if ($save_file_name != $vehicle['ImageFile']) {
                            $values = array("ImageFile" => $save_file_name);
                            if (!Vehicle::update($db, $vehicle_id, $values)) {
                                $error = "Could not update ImageFile entry for vehicle_id $vehicle_id";
                            }
                        }
                    } else {
                        $error = "Error saving image file";
                    }
                } else {
                    $error = "The image file must be in GIF, JPEG or PNG format.";
                }
            }
        }
        
        if (!$error && count($_FILES) && isset($_FILES['internal_image']) && ($_FILES['internal_image']['error'][0] != UPLOAD_ERR_NO_FILE)) {
            $no_images = count($_FILES['internal_image']['name']);
           
            for ($i = 0; $i < $no_images; $i++) {
                if ($_FILES['internal_image']['error'][$i] != UPLOAD_ERR_OK) {
                    echo'<pre>';print_r("here");echo'</pre>';die('here');
                    $error = "Error uploading image file (error code {$_FILES['image_file']['error'][$i]})";
                } else {

                    $temp_file_name = filter_var($_FILES['internal_image']['tmp_name'][$i], FILTER_UNSAFE_RAW);
                   
                    $orig_file_name = filter_var($_FILES['internal_image']['name'][$i], FILTER_UNSAFE_RAW);
                    $info = pathinfo($orig_file_name);
                    $ext = strtolower($info['extension']);
                    $save_file_name = "vehimages/vehicle_".  time()."_".$i."_".$vehicle_id.".".$ext;

                    if (($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe")) {
                        if (move_uploaded_file($temp_file_name, $save_file_name)) {
                            // update the DB if necessary
                           $values=array("vehilce_id"=>$vehicle_id,"image"=>$save_file_name);
                          Vehicle::add_internal_images($db, $values);
                        } else {
                            $error = "Error saving image file";
                        }
                    } else {
                        $error = "The image file must be in GIF, JPEG or PNG format.";
                    }
                }
            }
        }
    }
    if ($add_after_save) {
		$form->addElement(new Element\Hidden('AddEditType', 'SaveNew'));
    } else {

        $form->addElement(new Element\Hidden('AddEditType', 'SaveExisting'));
        $vehicle = Vehicle::getById($db, $vehicle_id);
    }
} else if ($mode == 'AddMake') {

    if (empty($_REQUEST['name']) || empty($_REQUEST['origin'])) {
        $error = "Missing parameters. Please correctly submit the form.";
    } else {
        $id = Vehicle::addMake($db, $_REQUEST['name'], $_REQUEST['origin']);
        if ($id == 0) {
            $error = "Could not add make \"" . htmlspecialchars($_REQUEST['name']) . "\"";
        } else {
            $message = "Added make \"" . htmlspecialchars($_REQUEST['name']) . "\" as MakeID $id";
        }
    }
} else {

    $form->addElement(new Element\Hidden('AddEditType', 'SaveNew'));
    $form->addElement(new Element\Hidden('ReturnTo', 'AllVehicles'));
}

if (isset($vehicle)) {


    //echo'<pre>';print_r($vehicle);echo'</pre>';die('here');	
    $form->addElement(new Element\Hidden('vehid', $vehicle['VehicleID']));


    $blackbookavg->setAttribute('value', $vehicle['BlackBookAvg']);
    $year->setAttribute('value', $vehicle['Year']);
    $make->setAttribute('value', $vehicle['MakeID']);
    $model->setAttribute('value', $vehicle['Model']);
    $style->setAttribute('value', $vehicle['Style']);
    $hybrid->setAttribute('value', $vehicle['Hybrid']);
    $size->setAttribute('value', $vehicle['Size']);
    $type->setAttribute('value', $vehicle['Type']);
    $convertible->setAttribute('value', $vehicle['Convertible']);
    $wheel_drive->setAttribute('value', $vehicle['WheelDrive']);
    $doors->setAttribute('value', $vehicle['Doors']);
    $body_type->setAttribute('value', $vehicle['BodyType']);

    // The checkboxes require a little more fiddling to set the checked property
    $best_buy_values = array();
    foreach (array('lowbest', 'highbest') as $best) {
        if ($vehicle[$best]) {
            $best_buy_values[] = $best;
        }
    }
    if (!empty($best_buy_values)) {
        $best_buy->setAttribute('value', $best_buy_values);
    }

    if ($vehicle['Visible']) {
        $visible->setAttribute('value', array('visible'));
    }

    if ($vehicle['ImageFile'] != "") {
        $ImageFile = $vehicle['ImageFile'];
    } else {
        $ImageFile = 'vehimages/No-Image-Found.jpg';
    }
}

        
        
$form->addElement($blackbookavg);
$form->addElement($year);
$form->addElement($make);
$form->addElement($model);
$form->addElement($style);
$form->addElement($type);
$form->addElement($hybrid);
$form->addElement($size);
$form->addElement($convertible);
$form->addElement($wheel_drive);
$form->addElement($doors);
$form->addElement($body_type);
$form->addElement($best_buy);
$form->addElement($visible);
if (isset($vehicle['ImageFile'])) {
    $form->addElement(new Element\HTML("Current Image: <img src=\"loadimage.php?image={$ImageFile}\" style='margin: 0px auto; padding: 5px 0px;' class='img-responsive'/>"));
    //$form->addElement(new Element\HTML("<button name=\"delete\" type=\"button\" class=\"\" value=\"save\">Save</button>"));
}
$form->addElement($image_file);



if (isset($vehicle)) {
    $get_internal_images= Vehicle::get_internal_images($db, $vehicle_id);
     
    if(is_array($get_internal_images)){
        foreach($get_internal_images as $val_list){
             $form->addElement(new Element\HTML("Internal Image: <img src=\"loadimage.php?image={$val_list['image']}\" style='margin: 0px auto; padding: 5px 0px;' class='img-responsive'/>"));
             $form->addElement(new Element\HTML("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button name=\"remove_image\" type=\"button\" vehicle_id=".$val_list['image']." class=\"remove\" value=\"delete\">Remove</button>"));
        }
    }
   
    
    $form->addElement($internal_images);
}
$form->addElement(new Element\HTML("<button name=\"save\" type=\"submit\" class=\"btn-primary\" value=\"save\">Save</button>"));
$form->addElement(new Element\HTML(" <button name=\"save\" type=\"submit\" class=\"btn-primary\" value=\"save_then_add\">Save &amp; Add Another</button>"));
//$form->addElement(new Element\HTML("<button name=\"save\" type=\"submit\" class=\"btn-primary\" value=\"save\">Update</button>"));
if (isset($vehicle)) {
    $form->addElement(new Element\HTML("<button name=\"save\" type=\"button\" class=\"btn-primary del11\" value=\"delete\">Delete</button>"));
}
//$save_button = new Element\Button("Save", "button", array("name" => "save", "class" => "btn-primary"));
//$form->addElement($save_button);
//$save_add_button = new Element\Button("Save & Start Adding", "button", array("name" => "save", "class" => "btn-primary"));
//$form->addElement($save_add_button);

$add_make_form = new Form("make");
$add_make_form->addElement(new Element\Hidden('AddEditType', 'AddMake'));
$add_make_form->addElement(new Element\Textbox("Name", "name"));
$add_make_form->addElement(new Element\Select("Origin", "origin", Vehicle::$ORIGINS));
$add_make_form->addElement(new Element\Button("Add New Make"));
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<style>
    select, textarea, input[type="text"], input[type="password"] {
        background: none repeat scroll 0 0 #fff;
        border: 1px solid #aaaaaa;
        color: #252525;
        cursor: text;
        font-family: Arial,Helvetica,Sans Serif;
        font-size: 13px;
        padding: 0 22px;
        resize: none;
        width: 100%;
        height: 30px;
    }
    .form-control {
        background-color: #fff;
        background-image: none;
        border: 1px solid #5fb796;
        border-radius: 0;
        box-shadow: none;
        color: #555;
        display: block;
        font-size: 14px;
        height: 30px;
        line-height: 1.42857;
        width: 100%;
        padding: 0 15px;
    }
    .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
        margin-top: -10px;
        position: absolute;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {
        $(".myclass").removeClass('form-control');
        
        
        $(".remove").click(function(){
            var image_name=$(this).attr("vehicle_id");
             var r = confirm("Are you sure want to delete this image.");
    if (r == true) {
        $.ajax({
                                                type: 'POST',
                                                url: 'remove_image.php',
                                                dataType: 'json',
                                                data: {'image_name':image_name},
                                                success: function(data) {
                                                    
                                                    if (data.error == 0) {
                                                       
                                                         window.location='allvehicles.php';
                                                   
                                                }
                                                }
                                            });
    } else {
       
    }
            
          
        });
        
        $(".del11").click(function(){
            
           var vehicle_id =<?php echo $vehicle_id;?>;
            var r = confirm("Are you sure want to delete this Vehicle.");
    if (r == true) {
           window.location='delete_vehicle.php?vehicle_id='+vehicle_id;
       }else{
       }
           
        })
<?php if (isset($vehicle)) { ?>
            edit_mode_size();
<?php } ?>
    });
    function get_size1() {

        var vtype = document.getElementById("Vehicle-element-7");
        var size = document.getElementById("Vehicle-element-9");
        var html_drop = "";
        $("#Vehicle-element-9").parent().parent().show();
        $("#Vehicle-element-9").empty();

        if (vtype.options[vtype.selectedIndex].value == "SUV")
        {


            html_drop = '<option value="Small">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "Auto")
        {


            html_drop = '<option value="Sub Compact">Sub Compact</option><option value="Compact">Compact</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "MiniVan")
        {

            $("#Vehicle-element-9").parent().parent().hide();
        }
        else {
            $("#Vehicle-element-9").parent().parent().hide();
            html_drop = '<option value="Default">Default</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
    }

    function edit_mode_size() {
        var vtype = document.getElementById("Vehicle-element-7");
        var size = document.getElementById("Vehicle-element-9");
        var selected_siz = "<?php echo $vehicle['Size']; ?>";
       // alert(selected_siz);
        $("#Vehicle-element-9").parent().parent().show();


        // alert(selected_siz);
        var html_drop = "";
        $("#Vehicle-element-9").empty();

        if (vtype.options[vtype.selectedIndex].value == "SUV")
        {

            html_drop = '<option value="Small">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "Auto")
        {

            html_drop = '<option value="Sub Compact">Sub Compact</option><option value="Compact">Compact</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option>';

            $("#Vehicle-element-9").html(html_drop);
        }
        else if (vtype.options[vtype.selectedIndex].value == "MiniVan")
        {

            $("#Vehicle-element-9").parent().parent().hide();
        }
      //  alert( $('#Vehicle-element-9 option[value=' + selected_siz + ']'));
       // $('#Vehicle-element-9 option[value=' + selected_siz + ']').attr('selected', 'selected');
        $('#Vehicle-element-9').val(selected_siz);
        
       // $('.id_100 option[value=val2]').attr('selected','selected');
    }
</script>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <?php
        if (!empty($error)) {
            ?>
            <h1 class="subhead">Error!</h1>
            <p><?= $error ?>
                <?php
            } else {
                if (!empty($message)) {
                    ?>
                <h1 class="subhead">Success!</h1>
                <p><?= $message ?>
                    <?php
                }
                ?>
                <br />
            <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
            <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">New Vehicle</h1>
            <div class="grideightgrey">
                <br />
                <?php $form->render(); ?>
                <br /> 
            </div>

            <div class="grideightgrey">
                <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Add New Make</h1>
                <br />
                <?php $add_make_form->render() ?>
            </div>
            <!--<p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>-->

            <?php
        }
        ?>
    </div>
</div><!--end content-->


<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
