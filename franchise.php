<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 5;
    $_SESSION['titleadd'] = 'Franchising Options';
    $page = $_REQUEST['page'];
    
    what-is-1-800-vehicles-com
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

		<script src="assets/js/jquery.carouFredSel-6.1.0-packed.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() { 
			
			$('.slider-wrapper').hide();
			$(window).load(function() {
			$('.slider-wrapper').show();
				$('#carousel').carouFredSel({
					width: "100%",
					items: 1,
					scroll: 1,
					auto: {
						//duration: 2250,
						//timeoutDuration: 2500
						play:false
					},
					prev: '#prev',
					next: '#next',
					pagination: '#pager',
					responsive:true
				});
	
			});
			
			});
		</script>
        <style type="text/css">
		
  #prev, #next {
				background: transparent url( assets/img/carousel_control.png ) no-repeat 0 0;
				text-indent: -999px;
				display: block;
				overflow: hidden;
				width: 15px;
				height: 21px;
				position: absolute;
				bottom: 5px;
			}
			#prev {
				background-position: 0 0;
				left: 40%;
			}
			#prev:hover {
				left: 39.5%;
			}			
			#next {
				background-position: -18px 0;
				right: 40%;
			}
			#next:hover {
				right: 39.5%;
			}				
			#pager {
				text-align: center;
				margin: 0 auto;
				padding-top: 20px;
			}
			#pager a {
				background: transparent url(assets/img/carousel_control.png) no-repeat -2px -32px;
				text-decoration: none;
				text-indent: -999px;
				display: inline-block;
				overflow: hidden;
				width: 8px;
				height: 8px;
				margin: 0 5px 0 0;
			}
			#pager a.selected {
				background: transparent url(assets/img/carousel_control.png) no-repeat -12px -32px;
				text-decoration: underline;				
			}  
			.testimonial-images {text-align: center;}
			.testimonial-images > img { width: 40%;}
			.slider-wrapper { background-color: #ffffff; padding-top: 30px; position: relative;}
			#content { margin: 40px 0 0;}
			.testimonial-images > p {width:90%; margin:0 auto;}
			.testimonial-images > ul { color: #7bb12d; margin: 0 auto; text-align: left; width: 460px;}
		    .about-us-button {	background-color: #9bc74c;
    				border-style: none;
    				color: #fff!important;
   					 font-family: Helvetica,Arial,sans-serif;
   					 font-size: 18px;
				    margin-bottom: 10px;
 				   margin-top: 20px;
 				   padding: 10px;}
		   .about-us-button:hover{color:#222222!important;}
		   .grideight > ul {
    margin: 0 auto !important;
    width: 50%;
}
.title-about-us {
    font-weight: 500;
}
}
  </style>    

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer franchise-image">
<?php if ($page == '') { ?>

   <h3 class="title-about-us">Franchise Opportunities</h3>

   <!-- <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Franchise Opportunities</h1>-->    <div class="grideightgrey">   
          <div class="slider-wrapper">
              <div id="carousel">
				<div class="testimonial-images">
                 <img src="common/layout/landingpage.jpg" /> 
                <p>
                   The 1-800-vehicles.com system has been developed and proven since 2001. Selling vehicles online is the future!
                </p>
                               
                </div>
				<div class="testimonial-images">
                 <img src="common/layout/landingpage.jpg" />
              
                <p>
                   To learn how you can be part of "America's Online Dealership", please contact a franchise territory representative at 1-800-VEHICLES (834-4253) ext. 3 or go to &quot;<a href="consult.php">Request Contact</a>&quot; to reach us by email.
                </p><br/>
                 <a class="about-us-button" href="franchise.php?page=how-it-works-for-customers">BEGIN THE TOUR</a>
                </div>
				
			</div>
			<a id="prev" href="#"></a>
			<a id="next" href="#"></a>
			<div id="pager"></div>
            
            
            <!-- <form method="get" action="franchise.php"><input type="hidden" value="how-it-works-for-customers" name="page" /><button value="" class="med" style="float:right;"><nobr>BEGIN THE TOUR</nobr></button></form>    --> 
            
		</div>
                  
           
              
     </div> 
              
<?php } else if ($page == 'what-is-1-800-vehicles-com') { ?>
    <h3 class="title-about-us"> About Us </h3>
    <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">What Is 1-800-vehicles.com?</h1>        <div class="grideightgrey">      
            <div class="slider-wrapper">
              <div id="carousel">
				<div class="testimonial-images">
                 <img src="common/layout/whatis1800.jpg" /> 
                <p>
                   1-800-vehicles.com is a revolutionary online dealership that has been operating since 2001. It is a powerful brand, a proven system and a quality group of dealer franchises. 1-800-vehicles.com offers consumers a better way to buy legitimately nice vehicles at excellent prices.
                </p>
                
                            
                
                </div>
				<div class="testimonial-images">
                 <img src="common/layout/whatis1800.jpg" />
                 <p>
                  1-800-vehicles.com customers are able to consult with our experts without obligation, get firm pricing up front, and post a search through our simple online process. Once the perfect vehicle has been located at the wholesale market, it is fully serviced and independently inspected to confirm that the condition is as promised.
                </p> <br/>
                   <a class="about-us-button" href="franchise.php?page=why-is-it-revolutionary">NEXT</a>
                </div>
				
			</div>
			<a id="prev" href="#"></a>
			<a id="next" href="#"></a>
			<div id="pager"></div>
            
           <!-- <form method="get" action="franchise.php"><input type="hidden" value="why-is-it-revolutionary" name="page" /><button value="" class="med" style="float:right;"><nobr>NEXT</nobr></button></form>--> 
            
		</div>
             
              
              
     </div>       
            
            
<?php } else if ($page == 'why-is-it-revolutionary') { ?>
   
   <h3 class="title-about-us"> About Us </h3>
   
   
   
   
    <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Why Is It Revolutionary?</h1>   <div class="grideightgrey">
           <div class="slider-wrapper">
              <div id="carousel">
				<div class="testimonial-images">
                 <img src="common/layout/revolutionary.jpg" /> 
                <p>
                    Because the 1-800-vehicles.com system allows consumers to buy a legitimately nice vehicle at a great price through a low pressure online process. It is mobile friendly and simple. They get honest advice about vehicles that best meet their specific needs, wants and budget, instead of pressure to buy something in dealer stock.
                </p>
                
                            
                
                </div>
				<div class="testimonial-images">
                 <img src="common/layout/revolutionary.jpg" />
                 <p>
                   The "just in time" aspect of the process makes it possible for 1-800-vehicles.com dealers to show consumers only the best of a massive wholesale "virtual inventory", and still save them time, money and hassles along the way. Rather than searching the internet for a vehicle, then trying to determine if the dealer is reputable -- or if the vehicle is any good at all, 1-800-vehicles.com consumers work with one representative who handles everything.
                </p>
               <p>
                The 1-800-vehicles.com process is revolutionary because it offers consumers:
                </p>
                
                <ul><li>Honest and professional consultation without pressure</li><li>A massive number of vehicles from which to choose</li><li>A tremendous savings in time, energy and frustration</li><li>The cost efficiency of ordering a vehicle from the wholesale market </li><li>Confidence  through an inspection by an independent mechanic</li></ul><br/>
                 <a class="about-us-button" href="index.php">BACK TO HOME PAGE</a>
                
                </div>
				
			</div>
			<a id="prev" href="#"></a>
			<a id="next" href="#"></a>
			<div id="pager"></div>
            
    </div>
            
                
      <!-- <form method="get" action="index.php"><input type="hidden" value="" name="" /><button value="" class="med" style="float:right;"><nobr>BACK TO HOME PAGE</nobr></button></form>-->     
      </div> 
       
<?php } else if ($page == 'how-it-works-for-customers') { ?>
    <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">How It Works For Customers</h1>        <div class="grideightgrey">
                <div class="grideight" style="font-size:13px;">
    <div align="center"><img class="img-responsive" src="loadimage.php?image=common/layout/customers.jpg&mwidth=425&mheight=425" border="0" hspace="10" vspace="10" /></div><p style="color:#142c3c;">Customers use the 1-800-vehicles.com website to consider all of the vehicles available within their price range, including vehicles with "Excellent Availability". A "Quick Assessment" is available as a scientific tool to help anyone involved with the decision making process. 1-800-vehicles.com representatives offer free consulting and give "Firm Quotes" on vehicles being considered, and they will determine the availability of each.</p><p style="color:#142c3c;">When the order is placed, 1-800-vehicles.com customers can sit back and relax while their representative searches the national wholesale market for the perfect vehicle. Once found and approved by the customer, the buying team purchases the vehicle and it is taken through a sophisticated process of inspection, detailing and service. All of this can be closely monitored by the customer.</p><p style="color:#142c3c;">An independent inspection assures the customer that the vehicle is in top mechanical condition, and nothing is left to chance. After the vehicle is test driven and approved by the customer, they can take delivery with confidence knowing that it is a great car!</p><br /><form method="get" action="franchise.php"><input type="hidden" value="how-it-works-for-franchises" name="page" /><button value="" class="med" style="float:right;"><nobr>CONTINUE THE TOUR</nobr></button></form>          </div><!-- endgrideight -->
            </div><!-- endgrideightgrey -->
<?php } else if ($page == 'how-it-works-for-franchises') { ?>
   <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">How It Works For Franchises</h1>        <div class="grideightgrey">
                <div class="grideight" style="font-size:13px;">
    <div align="center"><img class="img-responsive" src="loadimage.php?image=common/layout/together.jpg&mwidth=425&mheight=425" border="0" hspace="10" vspace="10" /></div><p style="color:#142c3c;">Established dealers who have been awarded a franchise territory keep their existing dealership name and add the 1-800-vehicles.com brand to become part of "Americas Online Dealership". They capture much more online business, as well as many of the customers who normally buy from large franchise dealers. They also gain the significant benefits of being part of a national franchise system, and a much more efficient use of capital.</p><p style="color:#142c3c;">Individuals who have been awarded a franchise territory for a new dealership get a proven business model that is affordable, futuristic and poised for long term success. They gain all of the same benefits of the established dealer franchises, as well as the opportunity to work closely with these highly reputable dealers with many years of experience.</p><p style="color:#142c3c;">All franchisees work closely together and with the franchisor to make the massive wholesale "virtual inventory" a reality. Customers in each territory are directed to the appropriate dealer franchise based on their home zip code, whether they call 1-800-VEHICLES (834-4253) or register on the website.</p><br /><form method="get" action="franchise.php"><input type="hidden" value="what-we-offer" name="page" /><button value="" class="med" style="float:right;"><nobr>CONTINUE THE TOUR</nobr></button></form>            </div><!-- endgrideight -->
            </div><!-- endgrideightgrey -->
<?php } else if ($page == 'what-we-offer') { ?>
    <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">What We Offer</h1>        <div class="grideightgrey">
                <div class="grideight" style="font-size:13px;">
    <div align="center"><img class="img-responsive" src="loadimage.php?image=common/layout/franchisees.jpg&mwidth=425&mheight=425" border="0" hspace="10" vspace="10" /></div>
    <p style="color:#142c3c;">A POWERFUL BRAND<br /><ul><li style="color:#142c3c;">Ownership of an exclusive territory in a national franchise system</li><li style="color:#142c3c;">The &quot;1-800-vehicles.com&quot; trademark</li><li style="color:#142c3c;">The &quot;Americas Online Dealership&quot; trademark</li><li style="color:#142c3c;">The 1-800-VEHICLES toll free number</li><li style="color:#142c3c;">A way to compete with large franchise dealers at a fraction of the cost</li></ul><p style="color:#142c3c;"><br/>A PROVEN SYSTEM<br /><ul><li style="color:#142c3c;">A revolutionary online automotive business, proven since 2001</li><li style="color:#142c3c;">A state of the art website experience with sophisticated sales management tools</li><li style="color:#142c3c;">An authentic way to offer a massive &quot;virtual inventory&quot; and increase your online presence</li><li style="color:#142c3c;">A way to lower risk and use your capital much more efficiently</li><li style="color:#142c3c;">A way to make guaranteed deals through a series of online commitments</li></ul><p style="color:#142c3c;"><br/>A QUALITY GROUP<br /><ul><li style="color:#142c3c;">Carefully chosen established dealers with years of success in the industry</li><li style="color:#142c3c;">New dealer franchisees with great energy and a fresh perspective</li><li style="color:#142c3c;">Significant purchasing power in advertising, support services and products</li><li style="color:#142c3c;">A trusted group of franchisees working together from important auction cities</li><li style="color:#142c3c;">A wholesale program with lower fees and built in accountability</li></ul><br /><form method="get" action="franchise.php"><input type="hidden" value="can-you-make-the-cut" name="page" /><button value="" class="med" style="float:right;"><nobr>CONTINUE THE TOUR</nobr></button></form>            </div><!-- endgrideight -->
            </div><!-- endgrideightgrey -->
<?php } else if ($page == 'can-you-make-the-cut') { ?>
   <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Can You Make The Cut?</h1>        <div class="grideightgrey">
                <div class="grideight" style="font-size:13px;">
    <div align="center"><img class="img-responsive" src="loadimage.php?image=common/layout/makethecut.jpg&mwidth=425&mheight=425" border="0" hspace="10" vspace="10" /></div><p style="color:#142c3c;">If you are an established automotive dealership, you must be stable, highly reputable and forward thinking to be considered for an exclusive territory.</p><p style="color:#142c3c;">If you are an individual who has a tremendous desire to get into the automotive business, and you meet the strict criteria set forth in our franchise qualifications, you may be considered to open a new 1-800-vehicles.com dealership franchise.</p><br /><form method="get" action="franchise.php"><input type="hidden" value="training-support" name="page" /><button value="" class="med" style="float:right;"><nobr>CONTINUE THE TOUR</nobr></button></form>            </div><!-- endgrideight -->
            </div><!-- endgrideightgrey -->
<?php } else if ($page == 'training-support') { ?>
    <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Training & Support</h1>        <div class="grideightgrey">
                <div class="grideight" style="font-size:13px;">
    <div align="center"><img class="img-responsive" src="loadimage.php?image=common/layout/training.jpg&mwidth=425&mheight=425" border="0" hspace="10" vspace="10" /></div><p style="color:#142c3c;">1-800-vehicles.com provides extensive training and operational support by the founders of the company. Prior to launching in your local market, we offer a 2-day training seminar at our offices in Nashville, Tennessee.</p><p style="color:#142c3c;"><strong>Local Market Support</strong><br />You will be assigned a specific Territory Development Manager who will work closely with you to insure success.</p><p style="color:#142c3c;"><strong>Ongoing Training</strong><br />Keep your staff sharp through training webinars and video tutorials.</p><p style="color:#142c3c;"><strong>Franchise Partner Support</strong><br />Learn from other Franchise Partners experiences through online discussion forums, annual meetings, and working relationships.</p><br /><form method="get" action="franchise.php"><input type="hidden" value="investment" name="page" /><button value="" class="med" style="float:right;"><nobr>CONTINUE THE TOUR</nobr></button></form>            </div><!-- endgrideight -->
            </div><!-- endgrideightgrey -->
<?php } else if ($page == 'investment') { ?>
   <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Investment</h1>        <div class="grideightgrey">
                <div class="grideight" style="font-size:13px;">
    <div align="center"><img class="img-responsive" src="loadimage.php?image=common/layout/investment.jpg&mwidth=425&mheight=425" border="0" hspace="10" vspace="10" /></div><p style="color:#142c3c;"><strong>CAPITAL REQUIREMENTS</strong></p><p style="color:#142c3c;"><strong>New Dealer Franchises:</strong></p><p style="color:#142c3c;"><strong>$85,000 Liquid capital</strong> &nbsp;&nbsp;&nbsp;Amount required for the franchise fee (with a five year term) and enough funding to start the business. This amount will cover working capital, equipment, professional fees and grand opening advertising.</p><p style="color:#142c3c;"><strong>$140,000 Additional capital</strong> &nbsp;&nbsp;&nbsp;Minimum amount of capital required to purchase vehicles ordered by customers - which can be borrowed. If financing is needed, a franchise territory representative will recommend the best sources to finance wholesale vehicle purchases.</p><br/><p style="color:#142c3c;"><strong>Established Dealer Franchises:</strong></p><p style="color:#142c3c;">The capital requirement, franchise fee and terms for established dealers are being offered with special incentives. Please ask a franchise territory representative for details.</p><br/><br /><form method="get" action="franchise.php"><input type="hidden" value="request-contact" name="page" /><button value="" class="med" style="float:right;"><nobr>CONTINUE THE TOUR</nobr></button></form>            </div><!-- endgrideight -->
            </div><!-- endgrideightgrey -->
<?php } else if ($page == 'request-contact') { ?>
    <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Request Contact</h1>        <div class="grideightgrey">
                <div class="grideight" style="font-size:13px;">
    <div align="center"><img class="img-responsive" src="loadimage.php?image=common/layout/opportunity.jpg&mwidth=425&mheight=425" border="0" hspace="10" vspace="10" /></div><p style="color:#142c3c;"> To learn how you can be part of "America's Online Dealership", please contact a franchise territory representative at 1-800-VEHICLES (834-4253) ext. 3 or go to &quot;<a href="consult.php">Request Contact</a>&quot; to reach us by email. </p><br /> <a class="about-us-button" href="index.php">BACK TO HOME PAGE</a>          </div><!-- endgrideight -->
            </div><!-- endgrideightgrey -->
<?php } ?>
    </div><!-- grid eight container -->

   
    <!--START A FRANCHISE NAV-->
    
<?php if ($page == 'what-is-1-800-vehicles-com' || $page == 'why-is-it-revolutionary' ) { ?>
 
 
 <?php } else if ($page == 'request-contact' || $page == ''  || $page == 'investment' || $page == 'training-support' || $page == 'can-you-make-the-cut' || $page == 'what-we-offer' || $page == 'how-it-works-for-franchises' || $page == 'how-it-works-for-customers') { ?>   
    <div class="grideightcontainer">
          <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">START A FRANCHISE</h1>
        <div class="grideightgrey">          
       <!-- <p <?php if ($page == 1) { echo 'class="current"'; } ?>><a href="franchise.php?page=what-is-1-800-vehicles-com">What Is 1-800-vehicles.com?</a></p>
        <p <?php if ($page == 2) { echo 'class="current"'; } ?>><a href="franchise.php?page=why-is-it-revolutionary">Why Is It Revolutionary?</a></p>
        --><p <?php if ($page == "how-it-works-for-customers") { echo 'class="current"'; } ?>><a href="franchise.php?page=how-it-works-for-customers">How It Works For Customers</a></p>
        <p <?php if ($page == 4) { echo 'class="current"'; } ?>><a href="franchise.php?page=how-it-works-for-franchises">How It Works For Franchises</a></p>
        <p <?php if ($page == 5) { echo 'class="current"'; } ?>><a href="franchise.php?page=what-we-offer">What We Offer</a></p>
        <p <?php if ($page == 6) { echo 'class="current"'; } ?>><a href="franchise.php?page=can-you-make-the-cut">Can You Make The Cut?</a></p>
        <p <?php if ($page == 7) { echo 'class="current"'; } ?>><a href="franchise.php?page=training-support">Training &amp; Support</a></p>
        <p <?php if ($page == 8) { echo 'class="current"'; } ?>><a href="franchise.php?page=investment">Investment</a></p>
        <p <?php if ($page == 9) { echo 'class="current"'; } ?>><a href="franchise.php?page=request-contact">Request Contact<br /></a></p>
         </div>
    </div>
    <!-- END FRANCHISE NAV-->
</div><!--end content-->

<?php } ?>

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
