<!DOCTYPE html>
<html lang="en">

<?php include("header.php"); ?>

    <!-- About Section -->
    <section id="login">
        <div class="section-inner">
            <div class="container">
<div class="row"> 
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
        <form method="post" action="login.php">
        <div class="grideightcontainer">
            <h1 class="subhead">Login</h1>
            <?php if($_SESSION['loginfailed']) { ?>
                <fieldset><div id='success_page'><p>Your login details are incorrect. Please check the username and password.</p></div></fieldset>
            <?php } ?>
            <div class="grideightgrey">
                <p>
                    <label for="username" class="registera"><span>Username</span> <span style="font-size: 16px">&#42;</span></label>
                    <input type="text" name="user" id="username" class="form-control" size='100' value="" maxlength="50" required="" data-validation-required-message="Please enter username."/>
                </p>
                <p>
                    <label for="password" class="registera"><span>Password</span> <span style="font-size: 16px">&#42;</span></label>
                    <input type="password" name="pass" class="form-control"  id="password" value="" maxlength="50" required="" data-validation-required-message="Please enter password." />
                </p>

                <p><span class="gridfour">
                    <input type="submit" name="login" id="logint" value="Login" class="btn btn-primary">
                    </span>
                </p>
            </div>
        </div>
        </form>
    </div><!-- grid eight container -->
    <div class="col-md-3">
    </div>
</div>
            </div>
        </div>
    </section>

    <section id="footer-widgets" class="divider-wrapper"></section>

   <?php include("footer.php") ?>

</html>
