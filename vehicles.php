<?php
    require_once(WEB_ROOT_PATH."globals.php");

    // GetVehicleInfo = find the year,make,model and style in the database
    // $pid = ID of the Vehicle to get
    // Return = return the Info of the vehicle
    function GetVehicleInfo($pid, $pcon)
    {
        // Find the vehicle first...
        $gvquery = "select v.Year, m.Make, v.Model, v.Style from makes m, vehicles v where m.MakeID = v.MakeID and v.VehicleID = ".$pid;
        $gvresult = mysql_query($gvquery, $pcon);
        if($gvrow = mysql_fetch_array($gvresult))
        {
            return $gvresult[0]." - ".$gvresult[1]." - ".$gvresult[2]." - ".$gvresult[3];
        }

        // If we get here there was an issue, so return a bad name...
        return "Unknown";
    }
?>
