<?php require("globals.php");
?>
<?php
//error_reporting(E_ALL);
//ini_set("display_errors", "on");


$_SESSION['state'] = 0;
$_SESSION['substate'] = 7;
$_SESSION['titleadd'] = 'Additional Infomation Form';

$addphone1 = "";
$addphone1ext = "";
$addphone1type = "";
$addphone1text = "";
$addphone2 = "";
$addphone2ext = "";
$addphone2type = "";
$addphone2text = "";
$email2 = "";
$email2type = "";
$email3 = "";
$email3type = "";
$referred = "";
$address1 = "";
$address2 = "";
$city = "";
$thestate = "";
$repcall = "";
$franchise = "";

if (isset($_POST['btnsubmit'])) {

   

    $addphone1 = $_POST["addphone1"];
    $addphone1ext = $_POST["addphone1ext"];
    $addphone1type = $_POST["addphone1type"];
    $addphone2 = $_POST["addphone2"];
    $addphone2ext = $_POST["addphone2ext"];
    $addphone2type = $_POST["addphone2type"];
    $email2 = $_POST["email2"];
    $email2type = $_POST["email2type"];
    $email3 = $_POST["email3"];
    $email3type = $_POST["email3type"];
    $referred = $_POST["referred"];
    $address1 = $_POST["address1"];
    $address2 = $_POST["address2"];
    $city = $_POST["city"];
    $thestate = $_POST["state"];
    $repname = $_POST["repname"];
    $repcall = $_POST["repcall"];
    $franchise = $_POST["franchise"];
    
    $emailexists = 'false';
    $loginexists = 'false';
    $saveerror = 'false';
    $fileerror = 'false';
    $errorinmail = 'false';
    $lastid = base64_decode($_GET['eid']);
    $ordernum = 2;

    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);


// Bring the temp file over to where we can work with it...
        if (isset($_FILES['photoname']['name']) && (strlen($_FILES['photoname']['name']) > 0)) {
            $ext = strtolower(end(explode(".", $_FILES['photoname']['name'])));
            $path = WEB_ROOT_PATH;
            $ifilename = 'userimages/User' . $lastid . 'temp.' . $ext;
            $target = $path . $ifilename;
            if (!is_uploaded_file($_FILES['photoname']['tmp_name']))
                $fileerror = 'Transfer Error - usually due to files over ' . ini_get('upload_max_filesize') . ' in size';
            else {
                if (($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE")) {
                    if ($_FILES["photoname"]["size"] > (20 * 1024 * 1024))
                        $fileerror = 'File is larger than 20M...' . number_format($_FILES["photoname"]["size"] / 1024 / 1024) . 'M';
                    else {
                        if ($_FILES["photoname"]["error"] > 0)
                            $fileerror = 'File Has an Error';
                        else {
                            if (file_exists($target))
                                unlink($target);
                            if (!move_uploaded_file($_FILES['photoname']['tmp_name'], $target))
                                $fileerror = 'Image Invalid';
                        }
                    }
                } else
                    $fileerror = 'File is not a GIF, JPG or PNG format';
            }
        } else
            $ifilename = '';

        if ($fileerror == 'false') {
            if ($ifilename && (strlen($ifilename) > 0)) {
                $ext = strtolower(end(explode(".", $ifilename)));
                $path = WEB_ROOT_PATH;
                $ofilename = 'userimages/User' . $lastid . '.' . $ext;
                $target = $path . $ofilename;
                if (file_exists($target))
                    unlink($target);
                if (!ScaledImageFile($ifilename, 200, 300, $ofilename, 'false', 'true'))
                    $fileerror = 'Could not create thumbnail for your image!';
                else {
                    unlink($ifilename);
// Update the user in the database...
                    $query = "update users set imagefile = '" . escapestr($ofilename) . "' where userid = " . $lastid;
                    if (!mysql_query($query, $con))
                        $saveerror = 'Could not update user image!';
                }
            }
        }

        // Add Other Phone 1 if available...
        if (($saveerror != 'true') && (strlen($addphone1) > 0)) {
            $query = "insert into usernumbers (userid, numbertypeid, phonenumber, extension, displayorder, texting, addinfo, startdate) values (" . $lastid . ",6,";
            $query .= "'" . $addphone1 . "',";
            if (strlen($addphone1ext) > 0)
                $query .= "'" . escapestr($addphone1ext) . "',";
            else
                $query .= "NULL,";
            $query .= $ordernum . ",";
            $ordernum++;
            if ($addphone1text == 'Yes')
                $query .= "1,";
            else
                $query .= "0,";
            if (strlen($addphone1type) > 0)
                $query .= "'" . escapestr($addphone1type) . "',";
            else
                $query .= "NULL,";
            $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
            if (!mysql_query($query, $con))
                $saveerror = 'true';
        }

        // Add Other Phone 2 if available...
        if (($saveerror != 'true') && (strlen($addphone2) > 0)) {
            $query = "insert into usernumbers (userid, numbertypeid, phonenumber, extension, displayorder, texting, addinfo, startdate) values (" . $lastid . ",6,";
            $query .= "'" . $addphone2 . "',";
            if (strlen($addphone2ext) > 0)
                $query .= "'" . escapestr($addphone2ext) . "',";
            else
                $query .= "NULL,";
            $query .= $ordernum . ",";
            $ordernum++;
            if ($addphone2text == 'Yes')
                $query .= "1,";
            else
                $query .= "0,";
            if (strlen($addphone2type) > 0)
                $query .= "'" . escapestr($addphone2type) . "',";
            else
                $query .= "NULL,";
            $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
            if (!mysql_query($query, $con))
                $saveerror = 'true';
        }


        $ordernum = 1;
        // Add Alternate Email if available...
        if (($saveerror != 'true') && (strlen($email2) > 0)) {
            $query = "insert into alternateemails (userid, email, addinfo, emailtophone, displayorder, startdate) values (" . $lastid . ",";
            $query .= "'" . escapestr($email2) . "',";
            if (strlen($email2type) > 0)
                $query .= "'" . escapestr($email2type) . "',";
            else
                $query .= "NULL,";
            if ($e2p2 == 'Yes')
                $query .= "1,";
            else
                $query .= "0,";
            $query .= $ordernum . ",";
            $ordernum++;
            $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
            if (!mysql_query($query, $con))
                $saveerror = 'true';
        }

        // Add Alternate Email if available...
        if (($saveerror != 'true') && (strlen($email3) > 0)) {
            $query = "insert into alternateemails (userid, email, addinfo, emailtophone, displayorder, startdate) values (" . $lastid . ",";
            $query .= "'" . escapestr($email3) . "',";
            if (strlen($email3type) > 0)
                $query .= "'" . escapestr($email3type) . "',";
            else
                $query .= "NULL,";
            if ($e2p3 == 'Yes')
                $query .= "1,";
            else
                $query .= "0,";
            $query .= $ordernum . ",";
            $ordernum++;
            $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
            if (!mysql_query($query, $con))
                $saveerror = 'true';
        }



        // Add Address if available...
        if (($saveerror != 'true')) {
            $address1 = ($address1) ? $address1 : '';
            $address2 = ($address2) ? $address2 : '';
            $city = ($city) ? $city : '';
            $thestate = ($thestate) ? $thestate : '';
            $query = "UPDATE `useraddresses` SET"
                    . " `Address1` = '" . $address1 . "',"
                    . " `Address2` = '" . $address2 . "', "
                    . "`City` = '" . $city . "', "
                    . "`State` = '" . $thestate . "'"
                    . " WHERE `Userid` = '" . $lastid . "'";
            if (!mysql_query($query, $con))
                $saveerror = 'true';
        }

        // Add Address if available...
        if (($saveerror != 'true')) {

            $referred = ($referred) ? $referred : 'NULL';
            $repname = ($repname) ? $repname : 'NULL';
            $franchise = ($franchise == 'on') ? 1 : 0;
            $query = "UPDATE  `users` SET "
                    . " `ReferredBy` =   '" . $referred . "',"
                    . " `franchiseinterest` =   '" . $franchise . "',"
                    . "`Repname` =   '" . $repname . "'"
                    . " WHERE `Userid` = '" . $lastid . "'";
            
            
            if (!mysql_query($query, $con))
                $saveerror = 'true';
        }
        
        if (($saveerror != 'true')) {
            $repcall = ($repcall == 'on') ? 1 : 0;
            $query ="UPDATE `marketneeds` SET "
                    . "`NeedsContact` =  '" . $repcall . "'"
                    . " WHERE `Userid` = '" . $lastid . "'";
            if (!mysql_query($query, $con))
                $saveerror = 'true';
        }
        mysql_close($con);
        if (($saveerror != 'true')) {
              echo "<script>window.location='success_additional_info.php'</script>";  
        }
       
    }

}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/additional-methods.js"></script>
<style>
    .borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
        border: none;
    }
    select, textarea, input[type="text"], input[type="password"] {
        background: none repeat scroll 0 0 #fff;
        border: 1px solid #aaaaaa;
        color: #252525;
        cursor: text;
        font-family: Arial,Helvetica,Sans Serif;
        font-size: 13px;
        padding: 3px;
        resize: none;
        width: 100%;
        padding: 0 6px;
    }

    .gridfour_reg {
        float: left;
        margin: 10px 0px;
        position: relative;
        width: 100%;
    }

    .btn-primary_reg {
        background-color: #85c11b;
        border-color: #85c11b;
        border-radius: 0;
        color: #fff;
        font-weight: bold;
        padding: 10px 15px;
        text-transform: uppercase;
        transition: all 0.5s ease-in-out 0s;
        width: 30%;

    }

    .help-block, help-block2 {
        color: red;
        display: block;
        font-size: 12px;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    .form-control {
        border-radius: 0;
        box-shadow: none;
        display: block;
        height: 30px;
        line-height: 1.42857;
    }
</style>
<script type="text/javascript">
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);

// control keys
        if ((key == null) || (key == 0) || (key == 8) ||
                (key == 9) || (key == 13) || (key == 27))
            return true;

// numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        else
            return false;
    }

 $(document).ready(function() {
     $("#btnlogin").click(function(){
           window.location='dashboard.php';
    });
 });

</script>

<?php
$lastid = base64_decode($_GET['eid']);

if($lastid==""){
     echo "<script>window.location='index.php'</script>";  
} else { ?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer" style="margin-top: 5%;">

        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">           
            Additional Information        
        </h1>
        <form action="" autocomplete="off"  method="post" id=additional_info" name="userform" enctype="multipart/form-data">
            <table class="table borderless"> 
                <tr><td colspan="2" class="assessment_insidetd_green" ><strong class="assessment_inside">* None of the following fields are required</strong></td></tr>

                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2" class="assessment_insidetd_green" style="width: 50%;"><strong class="assessment_inside">Alternate Phone Numbers:</strong></td></tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 28%">Additional Number(s)</td>
                                <td><input name="addphone1" id="addphone1" class="form-control"  type="text" value="" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);"   /></td>
                                <td>&nbsp;Ext</td>
                                <td><input name="addphone1ext" type="text" class="form-control"  value="" size="5" maxlength="10"   /></td>
                                <td>&nbsp;Notes</td>
                                <td><input name="addphone1type" type="text" class="form-control"  value="" size="8" maxlength="50"   placeholder="i.e my work phone"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input name="addphone2" id="addphone2" class="form-control"  type="text" value="" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" /></td>
                                <td>&nbsp;Ext</td>
                                <td><input name="addphone2ext" type="text" class="form-control"  value="" size="5" maxlength="10"    /></td>
                                <td>&nbsp;Notes</td>
                                <td><input name="addphone2type" type="text" class="form-control"  value="" size="8" maxlength="50"  placeholder="i.e my wife phone" /></td>
                            </tr>
                        </table>


                    </td>

                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2" class="assessment_insidetd_green" style="width: 50%;"><strong class="assessment_inside">Alternate E-mails:</strong></td></tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 28%">E-mails</td>
                                <td><input name="email2" id="email2" class="form-control"  type="text" value="" size="30" maxlength="200" /></td>

                                <td>Notes</td>
                                <td> <input name="email2type" type="text" class="form-control"  value="" size="8" maxlength="10"  placeholder="i.e my secondary email" /> </td>
                            </tr>
                            <tr>
                                <td>E-mails</td>
                                <td>
                                    <input name="email3" id="email3" class="form-control"  type="text" value="" size="30" maxlength="200" />
                                </td>

                                <td>Notes</td>
                                <td>
                                    <input name="email3type" type="text" class="form-control"  value="" size="8" maxlength="10"  placeholder="i.e my wife's email"/>
                                </td>
                            </tr>
                        </table>


                    </td>

                </tr>

                <tr><td colspan="2">&nbsp;</td></tr>                
                <tr>
                    <td style="width: 13%">
                        Upload your photo
                    </td> 
                    <td>
                        <input type="file" name="photoname" id="photoname" value="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        How did you find us?
                    </td> 
                    <td>
                        <input type="text" name="referred" value="" size="33" maxlength="50" class="form-control"  />
                    </td>
                </tr>
                <tr>
                    <td>
                        1-800-Vehicles.com representative's name (if you already have one)
                    </td> 
                    <td>
                        <input type="text" name="repname" value="" size="30" maxlength="200" class="form-control"  />
                    </td>
                </tr>


                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2" class="assessment_insidetd_green" style="width: 50%;"><strong class="assessment_inside">Address:</strong></td></tr>       
                <tr>
                    <td>
                        Address 1
                    </td> 
                    <td>
                        <input type="text" name="address1" value="" size="30" maxlength="150" class="form-control" />
                        <div style="clear: both;margin-bottom: 5px;"></div>
                        <input type="text" name="address2" value="" size="30" maxlength="150" class="form-control" />                        
                    </td>
                </tr>
                <tr>
                    <td>
                        City
                    </td> 
                    <td>
                        <input type="text" name="city" value="" size="30" maxlength="35" class="form-control"  />
                    </td>
                </tr>
                <tr>
                    <td>
                        State
                    </td> 
                    <td>
                        <input type="text" name="state" value=""  class="form-control"  />
                    </td>
                </tr>


                <tr>
                    <td colspan="2">

                        <input type="checkbox" name="repcall" />
                        <span style="color:#444444; ">Please have a 1-800-vehicles.com representative contact me for free vehicle consultation.</span>

                    </td>
                </tr>

                <tr>
                    <td colspan="2">

                        <input type="checkbox" name="franchise" <?php if ($autoset == 'true') echo 'checked="checked"'; ?> />
                        <span style="color:#444444; ">Please have a representative contact me regarding the 1-800-vehicles.com franchise opportunity.</span>

                    </td>
                </tr>

                <tr>
                    <td colspan="2">

                        <p style="color:#85c11b; font-size:13px; margin-left:8px;">* Privacy Statement:  Customer information will not be shared or sold!</p>
                        <p>

                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <span class="gridfour_reg">
                            <input type="submit" name="btnsubmit" id="btnsubmit"  value="Submit Information" class="btn btn-primary_reg btncontinue">                            
                            &nbsp;&nbsp;&nbsp; <input type="button" name="btnlogin" id="btnlogin"  value="SKIP" class="btn btn-primary_reg">
                        </span>
                    </td>                                                
                </tr>
            </table>
       
    </div><!-- end grideightgrey-->

<?php

$con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
    mysql_select_db(DB_SERVER_DATABASE, $con);
     $lastid = base64_decode($_GET['eid']);
            $query = "SELECT * FROM  `users` WHERE  `UserID` = '" . $lastid . "'";
            $result = mysql_query($query, $con);
            $row = mysql_fetch_assoc($result);
            $querymarket = "select MarketNeedID from marketneeds where  UserID = ".$lastid;
            $resultmarket = mysql_query($querymarket, $con);
            $rowmarket = mysql_fetch_assoc($resultmarket);
         
            $_SESSION['user'] =$row['Email'];
            $_SESSION['userid'] = $row['UserID']; 
            $_SESSION['marketneedid'] = $rowmarket['MarketNeedID'];
            $_SESSION['firstname'] =  $row['Firstname'];
            $_SESSION['lastname'] = $row['Lastname'];
     mysql_close($con);
    }
?>
    
    
    
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php } ?>
<?php require("footerstart.php"); ?>

<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>


