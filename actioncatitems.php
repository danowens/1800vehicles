<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 7;
    $_SESSION['titleadd'] = "Action Plans";

    $userid = $_SESSION['userid'];
    $uadmin = getuserprofile($userid, 'Administrator');
    $uterr = getuserprofile($userid, 'Teritory Admin');
    $ufran = getuserprofile($userid, 'Franchisee');
    $uops = getuserprofile($userid, 'Operations Manager');

    if(isset($_REQUEST['FranchiseeID']) && ($_REQUEST['FranchiseeID'] != -1))
    {
        $franid = $_REQUEST['FranchiseeID'];

        // Verify this user has access to this page...
        if(isinfranchisee($franid, $userid) == 'false')
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x010507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
        if(!(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true')))
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x010507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
    }
    else
    {
        $franid = -1;
        if(!(($uadmin == 'true') || ($uterr == 'true')))
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x010507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
    }

    if(!isset($_REQUEST['ActionPlanID']) || !isset($_REQUEST['ActionCategoryID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x010507';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    $actionplanid = $_REQUEST['ActionPlanID'];
    $actioncatid = $_REQUEST['ActionCategoryID'];

    $erroronsave = 'false';
    if(isset($_REQUEST['PostBack']))
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            // See what we have and divide them into new and old lines...
            $count = $_REQUEST['NumLines'];
            $oindex = 0;
            $nindex = 0;
            for($current = 1; $current <= $count; $current++)
            {
                $curid = $_REQUEST['id'.$current];
                if($curid == -1)
                {
                    $nitemorder[$nindex] = $_REQUEST['disp'.$current];
                    if(isset($_REQUEST['cust'.$current])) $nitemcust[$nindex] = 1;
                    else $nitemcust[$nindex] = 0;
                    $nitemname[$nindex] = $_REQUEST['name'.$current];
                    $nindex++;
                }
                else
                {
                    $oitemid[$oindex] = $curid;
                    $oitemorder[$oindex] = $_REQUEST['disp'.$current];
                    if(isset($_REQUEST['cust'.$current])) $oitemcust[$oindex] = 1;
                    else $oitemcust[$oindex] = 0;
                    $oitemname[$oindex] = $_REQUEST['name'.$current];
                    $oindex++;
                }
            }

            // Remove any that are no longer used...
            $idlist = implode(",", $oitemid);
            if(strlen($idlist) > 0)
            {
                $query = "delete from actionitems where ActionCategoryID=".$actioncatid." and ActionItemID not in (".$idlist.")";
                mysql_query($query, $con);
            }

            // Update the ones that are still around...
            $numold = count($oitemorder);
            for($i=0; $i < $numold; $i++)
            {
                $query = "update actionitems set Name='".escapestr($oitemname[$i])."', DisplayOrder=".$oitemorder[$i].", CustSee = ".$oitemcust[$i]." where ActionItemID=".$oitemid[$i];
                mysql_query($query, $con);
            }

            // Add the ones that are new...
            $numnew = count($nitemorder);
            for($i=0; $i < $numnew; $i++)
            {
                $query = "insert into actionitems (Name, ActionCategoryID, DisplayOrder, CustSee) values ('".escapestr($nitemname[$i])."',".$actioncatid.",".$nitemorder[$i].",".$nitemcust[$i].")";
                mysql_query($query, $con);
            }

            mysql_close($con);
        }
    }

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select Name from actionplans where ActionPlanID=".$actionplanid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $planname = $row[0];

            $query = "select Name from actioncategories where ActionCategoryID=".$actioncatid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $catname = $row[0];

                $query = "select Name, CustSee, DisplayOrder, ActionItemID from actionitems where ActionCategoryID=".$actioncatid." order by DisplayOrder asc";
                $result = mysql_query($query, $con);
                $index = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    $ainame[$index] = $row[0];
                    $aicustsee[$index] = $row[1];
                    $aidisporder[$index] = $row[2];
                    $aiitemid[$index] = $row[3];
                    $index++;
                }
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function trimAll(sString)
    {
        while(sString.substring(0,1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while(sString.substring(sString.length-1, sString.length) == ' ')
        {
            sString = sString.substring(0,sString.length-1);
        }
        return sString;
    }

    function validateFormOnSubmit()
    {
        var errorstr = "";
        var focusset = 0;

        var hiterror = 0;
        var hitcount = 0;
        var numberbox = document.getElementById("NumLines");
        for(var i = 0; i < parseInt(numberbox.value,10); i++)
        {
            var lasterror = 0;

            var lastdisp = "disp"+(i+1);
            var vlast = document.getElementById(lastdisp);
            var tString = trimAll(vlast.value);
            if(tString.length < 1)
            {
                lasterror = i+1;
                if(focusset == 0)
                {
                    vlast.focus();
                    focusset = 1;
                }
                vlast.style.background = '#ffcccc';
            }
            else vlast.style.background = 'White';

            var lastname = "name"+(i+1);
            vlast = document.getElementById(lastname);
            var tString = trimAll(vlast.value);
            if(tString.length < 1)
            {
                lasterror = i+1;
                if(focusset == 0)
                {
                    vlast.focus();
                    focusset = 1;
                }
                vlast.style.background = '#ffcccc';
            }
            else vlast.style.background = 'White';

            if(lasterror < 1) hitcount++;
            else hiterror = lasterror;
        }

        if(hiterror == 0)
        {
            for(var i = 0; ((i < parseInt(numberbox.value,10))&&(hiterror == 0)); i++)
            {
                var lastdisp = "disp"+(i+1);
                var vlast = document.getElementById(lastdisp);
                for(var j = i+1; ((j < parseInt(numberbox.value,10)) && (hiterror == 0)); j++)
                {
                    var curdisp = "disp"+(j+1);
                    var vcur = document.getElementById(curdisp);
                    if(vcur.value == vlast.value)
                    {
                        errorstr += "The Display Order Numbers must be unique and "+vcur.value+" was included twice."+'\n';
                        hiterror = 1;
                        if(focusset == 0)
                        {
                            vcur.focus();
                            focusset = 1;
                        }
                        vcur.style.background = '#ffcccc';
                    }
                    else vcur.style.background = 'White';
                }
            }
        }
        else
        {
            errorstr += "All included lines must be filled in. Please remove any you are not using.  Line "+(hiterror)+" is one of the incomplete lines."+'\n';
        }

        if((errorstr == "") && (hitcount < 1))
        {
            errorstr += "There are no lines to save."+'\n';
        }

        if(errorstr != "")
        {
            alert("Errors that must be corrected:"+'\n'+errorstr+'\n');
            return false;
        }
        return true;
    }

    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }

    function addline()
    {
        var numberbox = document.getElementById("NumLines");
        var nextone = parseInt(numberbox.value,10)+1;
        var x=document.getElementById('categories').insertRow(nextone);
        x.style.color="000000";
        var c1=x.insertCell(0);
        c1.align="center";
        var c2=x.insertCell(1);
        c2.align="center";
        var c3=x.insertCell(2);
        var c4=x.insertCell(3);
        c1.innerHTML='<input size="3" maxlength="3" type="text" name="disp'+nextone+'" id="disp'+nextone+'" value="" onkeypress="javascript:return numbersonly(event);" />';
        c2.innerHTML='<input type="checkbox" name="cust'+nextone+'" id="cust'+nextone+'" value="Yes" checked="checked"/>';
        c3.innerHTML='<input size="25" maxlength="50" type="text" name="name'+nextone+'" id="name'+nextone+'" value="" />';
        c4.innerHTML='<input type="hidden" name="id'+nextone+'" id="id'+nextone+'" value="-1" />';
        numberbox.value = nextone;
    }

    function remline()
    {
        var numberbox = document.getElementById("NumLines");
        var nextone = parseInt(numberbox.value,10)-1;
        if(nextone > -1)
        {
            document.getElementById('categories').deleteRow(nextone+1);
            numberbox.value = nextone;
        }
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>
<div id="content">
    <div class="grideightcontainer">
        <p style="font-size: 14px; margin-top: 0pt;"><a href="mydashboard.php#admintab">Go to Dashboard</a> (will NOT save changes made below)</p>
        <h1 class="subhead">Action Plan Categories</h1>
        <div class="grideightgrey" style="color: rgb(20, 44, 60);">
<?php
    if($erroronsave != 'false')
    {
        echo $erroronsave.'<br/>';
        echo $query;
    }
?>
            <table style="margin-left: 5px;" align="left" border="0" cellpadding="5" width="600">
                <tr>
                    <td width="100"><p style="font-size: 16px; margin-top: 0pt;"><strong>Plan Name</strong></p></td>
                    <td width="500">
                        <p style="font-size: 16px; margin-top: 0pt;"><a href="actionplan.php?FranchiseeID=<?php echo $franid; ?>&ActionPlanID=<?php echo $actionplanid; ?>"><?php echo $planname; ?></a><span style="font-size: 14px;"> (Click to return to the Plan without saving changes)</span></p>
                    </td>
                </tr>
                <tr>
                    <td width="100"><p style="font-size: 16px; margin-top: 0pt;"><strong>Category Name</strong></p></td>
                    <td width="500">
                        <p style="font-size: 16px; margin-top: 0pt;"><a href="actioncategories.php?FranchiseeID=<?php echo $franid; ?>&ActionPlanID=<?php echo $actionplanid; ?>"><?php echo $catname; ?></a><span style="font-size: 14px;"> (Click to return to the Category without saving changes)</span></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"><p style="font-size: 14px; margin-top: 0pt;">To change the order of the items in the action plan, adjust the numbers in the # column and Save Changes.  The list will re-order upon saving.</p></td>
                </tr>
                <tr>
                    <td colspan="2"><p style="font-size: 14px; margin-top: 0pt;">To delete a line in the middle, change the # to a higher number than any other line, Save Changes to move it to the bottom, then Remove Line to get rid of it and Save Changes to make the delete permanent.</p></td>
                </tr>
            </table>
        </div><!--grideightgrey-->
<?php
    $count = count($ainame);
    echo '<div class="grideightgrey" style="color: rgb(20, 44, 60);">';
    echo '<form action="actioncatitems.php" onsubmit="javascript:return validateFormOnSubmit();" method="post">';
    echo '<input type="hidden" value="'.$count.'" id="NumLines" name="NumLines" />';
    echo '<input type="hidden" value="'.$actionplanid.'" name="ActionPlanID" />';
    echo '<input type="hidden" value="'.$actioncatid.'" name="ActionCategoryID" />';
    echo '<input type="hidden" value="'.$franid.'" name="FranchiseeID" />';
    echo '<input type="hidden" value="true" name="PostBack" />';
    echo '<table width="520" border="0" cellpadding="3" id="categories">';
    echo '<tr style="color:#85c11b; font-size:15px;">';
    echo '<td width="50" align="center"><strong>#</strong></td>';
    echo '<td width="220" align="center"><strong>Customer Can See?</strong></td>';
    echo '<td width="200" align="center"><strong>Name</strong></td>';
    echo '<td width="50" align="center">&nbsp;</td>';
    echo '</tr>';
    if($count > 0)
    {
        for($i=0; $i < $count; $i++)
        {
            echo '<tr style="color:#000000; font-size:15px;">';
            echo '<td align="center"><input size="3" maxlength="3" type="text" name="disp'.($i+1).'" id="disp'.($i+1).'" value="'.$aidisporder[$i].'" onkeypress="javascript:return numbersonly(event);" /></td>';
            echo '<td align="center"><input type="checkbox" name="cust'.($i+1).'" id="cust'.($i+1).'" value="Yes" ';
            if($aicustsee[$i] == 1) echo 'checked="checked"';
            echo ' /></td>';
            echo '<td align="center"><input size="25" maxlength="50" type="text" name="name'.($i+1).'" id="name'.($i+1).'" value="'.$ainame[$i].'" /></td>';
            echo '<td align="center"><input type="hidden" name="id'.($i+1).'" id="id'.($i+1).'" value="'.$aiitemid[$i].'" /></td>';
            echo '</tr>';
        }
    }
    echo '<tr><td colspan="2" align="right"><button type="button" value="addline" onclick="javascript:addline();" class="small"><nobr>ADD LINE</nobr></button></td>';
    echo '<td colspan="2" align="left"><button type="button" value="remline" onclick="javascript:remline();" class="small"><nobr>REMOVE LINE</nobr></button></td></tr>';
    echo '</table>';
    echo '<button type="submit" value="" class="med"><nobr>SAVE CHANGES</nobr></button>';
    echo '</form>';
    echo '<br clear="all" />';
    echo '</div><!--grideightgrey-->';
?>
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
