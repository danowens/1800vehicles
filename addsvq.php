<?php require("globals.php"); ?>
<?php
    require_once(WEB_ROOT_PATH.'common/functions/string.php');
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 11;
    $_SESSION['titleadd'] = 'Add SVQ';

    if(!isset($_REQUEST['ForUserID']) || !isset($_REQUEST['ForNeedID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000511';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    $inneedid = $_REQUEST['ForNeedID'];
    $inuserid = $_REQUEST['ForUserID'];
    $inusername = getuserfullname($inuserid, 'false');

    if(isset($_REQUEST['FranchiseeID'])) $infranid = $_REQUEST['FranchiseeID'];
    else $infranid = -1;
    if(isset($_REQUEST['ForOrderID'])) $inorderid = $_REQUEST['ForOrderID'];
    else $inorderid = -1;
    if(isset($_REQUEST['ForGroupID'])) $ingroupid = $_REQUEST['ForGroupID'];
    else $ingroupid = -1;

    $userid = $_SESSION['userid'];

    $errormessage = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        if($infranid < 0)
        {
            $query = "select FranchiseeID from franchiseemembers where UserId=".$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $infranid = $row[0];
            }
        }

        // Are we in a callback and should set the database?
        if(isset($_POST["PostBack"]))
        {
            if(isset($_POST['ID']) && isset($_POST['AllID']) && isset($_POST['Avail']) && isset($_POST['Veh']))
            {
                //echo print_r($_POST['ID']).'<br/>';
                //echo print_r($_POST['AllID']).'<br/>';
                //echo print_r($_POST['Avail']).'<br/>';
                //echo print_r($_POST['Veh']).'<br/>';
                $added = 0;
                $count = count($_POST['ID']);
                for($a = 0; $a < $count; $a++)
                {
                    $addid = $_POST['ID'][$a];
                    $c2 = count($_POST['AllID']);
                    for($i = 0; $i < $c2; $i++)
                    {
                        $id = $_POST['AllID'][$i];
                        if($id != $addid) continue;

                        $avail = $_POST['Avail'][$i];
                        $vehid = $_POST['Veh'][$i];

                        $query = "select * from specificvehicles where MarketNeedID=".$inneedid." and VehicleDetailID=".$vehid;
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            $query = "insert into specificvehicles (MarketNeedID, VehicleDetailID, Created, LastUpdated, PriceQuoted, AddDepositAmount,AddSecondKeyLimit,AddNavDiscLimit,AddSellerRepairLimit,AddPurchaserRepairLimit,AddTireCopay,AddDelivery, OrderID, WatchID) values (".$inneedid.",".$vehid.",'".date_at_timezone('Y-m-d H:i:s', 'EST')."','".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                            $query .= $_POST['price'].",";
                            $query .= $_POST['AddDep'].",";
                            $query .= $_POST['AddSec'].",";
                            $query .= $_POST['AddNav'].",";
                            $query .= $_POST['AddSel'].",";
                            $query .= $_POST['AddPur'].",";
                            $query .= $_POST['AddTire'].",";
                            $query .= $_POST['AddDelivery'].",";
                            if($inorderid > 0) $query .= $inorderid.",NULL";
                            elseif($ingroupid > 0) $query .= "NULL,".$ingroupid;
                            else $query .= "NULL,NULL";
                            $query .= ")";
                            if(!mysql_query($query, $con)) $errormessage = 'Could not assign the vehicle.';
                            else
                            {
                                $added++;
                                $lastid = mysql_insert_id($con);
                                $query = "insert into specificmatches (SpecificVehicleID, MatchingID, Source) values (".$lastid.",".$id.",";
                                if($avail == 1) $query .= "'TradeIn'";
                                elseif($avail == 2) $query .= "'Other'";
                                else $query .= "'InStock'";
                                $query .= ")";
                                if(!mysql_query($query, $con)) $errormessage = 'Could not assign the vehicle.';
                                else
                                {
                                    // Add Messages when this happens...
                                    posttodashboard($con, $userid, $inuserid, 'added a <a href="specificquote.php?SVQID='.$lastid.'">Specific Vehicle Quote</a> for you to review.', $inneedid);

                                    $message = 'A specific vehicle has been added for you to review.</br>Go to: <a href="http://www.1800vehicles.com">1800vehicles.com</a> to review.<br/>Please contact your sales representative soon to discuss this specific vehicle.';
                                    sendemail(getuseremailnodb($con, $inuserid), 'Specific Vehicle To Review', $message, 'false');

                                    posttodashboard($con, $userid, $userid, 'added <a href="invdetails.php?SVQID='.$lastid.'&ForUserID='.$inuserid.'&MarketNeedID='.$inneedid.'">Specific Vehicle Quote</a> for <a href="salesrepactions.php?ForUserID='.$inuserid.'&MarketNeedID='.$inneedid.'">'.$inusername.'</a>.');

                                    $message = 'Your customer has received the Specific Vehicle to review...</br>';
                                    $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $inuserid, 'false').'</br>';
                                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $inuserid).'</br>';
                                    sendemail(getuseremailnodb($con, $userid), 'Specific Vehicle Received', $message, 'true');

                                    $message = 'A customer has been given a Specific Vehicle to review...</br>';
                                    $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $inuserid, 'false').'</br>';
                                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $inuserid).'</br>';
                                    sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                                }
                            }
                        }
                    }
                }
                if($added > 0)
                {
                    mysql_close($con);
                    header('Location: salesrepactions.php?ForUserID='.$inuserid.'&MarketNeedID='.$inneedid);
                    exit();
                }
            }
        }

        // Otherwise, fill the list of vehicles to chose from...
        if($inorderid > 0)
        {
            // Get the Orders and Posts...
            $query = "select f.OrderType, o.accepted, o.WaitPeriod, o.Expires, q.Year, q.Make, q.Model, q.Style, o.DepositReceived from orders o,orderfor of,firmquotes f,quoterequests q where o.orderid=of.orderid and f.firmquoteid=of.firmquoteid and f.quoterequestid=q.quoterequestid and o.orderid=".$inorderid;
            $result = mysql_query($query);
            $index = 0;
            if($result && $row = mysql_fetch_array($result))
            {
                $oordertype[$index] = $row[0];
                $oaccepted[$index] = $row[1];
                $owait[$index] = $row[2];
                $oexpires[$index] = $row[3];
                $oyear[$index] = $row[4];
                $omake[$index] = $row[5];
                $omodel[$index] = $row[6];
                $ostyle[$index] = $row[7];
                $odeprec[$index] = $row[8];
                $index++;
            }
        }

        if($ingroupid > 0)
        {
            $query = "select f.OrderType, w.accepted, w.Expires, q.Year, q.Make, q.Model, q.Style from watches w,watchfor wf,firmquotes f,quoterequests q where w.watchid=wf.watchid and f.firmquoteid=wf.firmquoteid and f.quoterequestid=q.quoterequestid and w.watchid=".$ingroupid;
            $result = mysql_query($query);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $wordertype[$index] = $row[0];
                $waccepted[$index] = $row[1];
                $wexpires[$index] = $row[2];
                $wyear[$index] = $row[3];
                $wmake[$index] = $row[4];
                $wmodel[$index] = $row[5];
                $wstyle[$index] = $row[6];
                $index++;
            }
        }

        $query = "select 2 'Type', s.SVQOnlyID 'ID', 0 'PricePaid', s.PriceWanted, v.VIN, v.Year, v.Model, m.Name, v.Mileage, v.VehicleDetailID from svqonlys s, vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=s.VehicleDetailID and s.Available=1 union all select 1 'TradeIn', t.TradeInID 'ID', 0 'PricePaid', t.PriceWanted, v.VIN, v.Year, v.Model, m.Name, v.Mileage, v.VehicleDetailID from tradeins t,vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=t.VehicleDetailID and t.Available=1 union all select 0, i.InStockID, i.PricePaid, i.PriceWanted, v.VIN, v.Year, v.Model, m.Name, v.Mileage, v.VehicleDetailID from instock i,vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=i.VehicleDetailID and i.Available=1";
        $result = mysql_query($query);
        $index = 0;
        if(!$result) echo mysql_error();
        else while($result && $row = mysql_fetch_array($result))
        {
            // Get the current Purchase Approval Information (if any)...
            $pquery = 'select * from specificvehicles where VehicleDetailID = '.$row[9];
            $presult = mysql_query($pquery, $con);
            if(!$prow = mysql_fetch_array($presult))
            {
                $svtradein[$index] = $row[0];
                $svID[$index] = $row[1];
                $svprice[$index] = $row[2];
                $svwanted[$index] = $row[3];
                $svvin[$index] = $row[4];
                $svyear[$index] = $row[5];
                $svmodel[$index] = $row[6];
                $svmake[$index] = $row[7];
                $svmiles[$index] = $row[8];
                $svvehid[$index] = $row[9];

                $index++;
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function validateFormOnSubmit()
    {
        var errorstr = "";

        var vprice = document.getElementById("price");
        if(vprice.value.length < 1)
        {
            vprice.style.background = '#ffcccc';
            errorstr += "Asking Price must be entered."+'\n';
            vprice.focus();
        }
        else vprice.style.background = 'white';

        vprice = document.getElementById("AddDep");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddSec");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddNav");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddSel");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddPur");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddTire");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddDelivery");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        if(errorstr != "")
        {
            alert("Errors that must be corrected:"+'\n'+errorstr+'\n');
            return false;
        }
        return true;
    }

    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 250px;">SVQ Assignment</h1>
        <div class="grideightgrey">
        <p class="blackeleven" style="margin: 0; margin-left: -5px;"><a href="salesrepactions.php?ForUserID=<?php echo $_REQUEST['ForUserID']; ?>&MarketNeedID=<?php echo $_REQUEST['ForNeedID']; ?>">Go back to Customer Review</a></p>
<?php
    if($inorderid > 0)
    {
        echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>For Order:</strong></p>';
        echo '<div class="grideight">';
        echo '<table border="0" width="500">';
        echo '<tr>';
        echo '<td width="174" style="color:#142c3c; font-weight:bold; font-size:13px;">Order Type</td>';
        echo '<td width="316" style="color:#757575; font-size:12px;">'.$oordertype[0].' Order</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Date Ordered</td>';
        echo '<td style="color:#757575; font-size:12px;">'.date_at_timezone('m/d/Y', 'EST', $oaccepted[0]).'</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Exclusive Search Period</td>';
        $days = $owait[0] - round((strtotime(date_at_timezone('m/d/Y H:i:s T', 'EST')) - strtotime(date_at_timezone('m/d/Y H:i:s T', 'EST', $oaccepted[0]))) / (60 * 60 * 24));
        if($days < 0) echo '<td style="color:#757575; font-size:12px;">Expired</td>';
        else echo '<td style="color:#757575; font-size:12px;">'.$days.' day minimum</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Expiration Date</td>';
        echo '<td style="color:#757575; font-size:12px;">';
        if(isset($oexpires[0])) echo date_at_timezone('m/d/Y', 'EST', $oexpires[0]);
        else echo 'Not Set';
        echo '</td>';
        echo '</tr>';
        echo '<tr>';
        $count = count($oaccepted);
        $plural = pluralize_noun($count, "Current Market Stud", "ies", "y");
        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">'.$plural.'Attached</td>';
        for($i=0;$i<$count;$i++)
        {
            echo '<td style="color:#757575; font-size:12px;">'.$oyear[$i].' '.$omake[$i].' '.$omodel[$i].' '.$ostyle[$i].'</td>';
        }
        echo '</tr>';
        echo '</table>';
        echo '<br clear="all" />';
        echo '<table width="163" border="0" style="font-weight:bold; font-size: 12px; float:left;">';
        echo '<tr style="color:#142c3c;"><td width="114" valign="bottom">Deposit Amount</td><td width="39" valign="bottom">$'.number_format(0.00).'</td></tr>';
        echo '<tr style="color:#85c11b;"><td height="20" valign="baseline">Deposit Received</td>';
        echo '<td valign="baseline">';
        if($odeprec[0] > 0) echo '$'.number_format($odeprec[0]);
        else echo 'No';
        echo '</td>';
        echo '</tr>';
        echo '</table>';
        echo '<br clear="all" /><br />';
        echo '</div>';
    }
    elseif($ingroupid > 0)
    {
        echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>For Group Search:</strong></p>';
        echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;">';
        echo '<br/><strong>Watch List:</strong><br/></p>';
        echo '<div class="grideight">';

        echo '<table border="0" width="500">';
        echo '<tr>';
        echo '<td width="174" style="color:#142c3c; font-weight:bold; font-size:13px;">Order Type</td>';
        echo '<td width="316" style="color:#757575; font-size:12px;">'.$wordertype[0].' Order</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Date Ordered</td>';
        echo '<td style="color:#757575; font-size:12px;">'.date_at_timezone('m/d/Y', 'EST', $waccepted[0]).'</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Expiration Date</td>';
        echo '<td style="color:#757575; font-size:12px;">';
        if(isset($wexpires[0])) echo date_at_timezone('m/d/Y', 'EST', $wexpires[0]);
        else echo 'Not Set';
        echo '</td>';
        echo '</tr>';
        echo '<tr>';
        $count = count($waccepted);
        $plural = pluralize_noun($count, "Current Market Stud", "ies", "y");
        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">'.$plural.'Attached</td>';
        for($i=0;$i<$count;$i++)
        {
            echo '<td style="color:#757575; font-size:12px;">'.$wyear[$i].' '.$wmake[$i].' '.$wmodel[$i].' '.$wstyle[$i].'</td>';
        }
        echo '</tr>';
        echo '</table>';
        echo '<br clear="all" /><br />';
        echo '</div>';
    }
    echo '<br/>';
    if($errormessage == 'false')
    {
        echo '<form action="addinventory.php" method="post">';
        echo '<input type="hidden" value="'.$inneedid.'" name="MarketNeedID" />';
        echo '<input type="hidden" value="'.$inuserid.'" name="ForUserID" />';
        echo '<input type="hidden" value="'.$infranid.'" name="FranchiseeID" />';
        echo '<input type="hidden" value="'.$inorderid.'" name="ForOrderID" />';
        echo '<input type="hidden" value="'.$ingroupid.'" name="ForGroupID" />';
        echo '<input type="hidden" value="true" name="SVQONLY" />';
        echo '<button type="submit" value="" class="med">ADD VEHICLE</button>';
        echo '</form>';
        echo '<form action="addsvq.php" method="post" onsubmit="javascript:return validateFormOnSubmit();" name="svqform">';

        $count = count($svID);
        if($count > 0)
        {
            echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>Or use one of the existing vehicles in the system:</strong></p>';
            echo '<table border="0" width="600">';
            echo '<tr>';
            echo '<td width="10" style="color:#142c3c; font-weight:bold; font-size:13px;"><strong>&nbsp;</strong></td>';
            echo '<td width="200" style="color:#142c3c; font-weight:bold; font-size:13px;"><strong>Vehicle</strong></td>';
            echo '<td width="200" style="color:#142c3c; font-weight:bold; font-size:13px;"><strong>Year/Make/Model</strong></td>';
            echo '<td width="90" style="color:#142c3c; font-weight:bold; font-size:13px;"><strong>Availability</strong></td>';
            echo '<td width="50" style="color:#142c3c; font-weight:bold; font-size:13px;"><strong>Miles</strong></td>';
            echo '<td width="50" style="color:#142c3c; font-weight:bold; font-size:13px;"><strong>Price Wanted</strong></td>';
            echo '</tr>';
            for($i=0; $i < $count; $i++)
            {
                echo '<tr>';
                echo '<td style="color:#757575; font-size:12px;"><input type="checkbox" name="ID[]" value="'.$svID[$i].'" /><input type="hidden" name="AllID[]" value="'.$svID[$i].'" /><input type="hidden" name="Veh[]" value="'.$svvehid[$i].'" /><input type="hidden" name="Avail[]" value="'.$svtradein[$i].'" /></td>';
                echo '<td style="color:#757575; font-size:12px;">'.$svvin[$i].'</td>';
                echo '<td style="color:#757575; font-size:12px;">'.$svyear[$i].' '.$svmake[$i].' '.$svmodel[$i].'</td>';
                echo '<td style="color:#757575; font-size:12px;">';
                if($svtradein[$i] == 1) echo 'Trade';
                elseif($svtradein[$i] == 2) echo 'SVQ Only';
                else echo 'In-Stock';
                //echo number_format($svtradein[$i],0);
                echo '</td>';
                echo '<td style="color:#757575; font-size:12px;">'.number_format($svmiles[$i]).'</td>';
                echo '<td style="color:#757575; font-size:12px;">$'.number_format($svwanted[$i]).'</td>';
                echo '</tr>';
            }
            echo '</table><br/>';
        }
        echo '<br clear="all" />';
    }
    else echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>'.$errormessage.'</strong></p>';

    echo '<input type="hidden" value="true" name="PostBack" />';
    echo '<input type="hidden" value="'.$inuserid.'" name="ForUserID" />';
    echo '<input type="hidden" value="'.$inneedid.'" name="ForNeedID" />';
    if($infranid > 0) echo '<input type="hidden" value="'.$infranid.'" name="FranchiseeID" />';
    if($inorderid > 0) echo '<input type="hidden" value="'.$inorderid.'" name="ForOrderID" />';
    if($ingroupid > 0) echo '<input type="hidden" value="'.$ingroupid.'" name="ForGroupID" />';
?>
                <p style="font-size: 18px; font-weight: bold; margin-top: 0pt;">Additional Details:</p>
                <table style="margin-left: 5px;" align="left" border="0" cellpadding="5" width="530">
                    <tbody>
                        <tr>
                            <td width="250"><strong>Price to Quote</strong></td>
                            <td width="280">
                                <input id="price" name="price" value="<?php if(isset($_POST['price'])) echo number_format($_POST['price'],0,'.',''); ?>" size="12" maxlength="9" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100"><strong>Additional Deposit</strong></td>
                            <td width="230">
                                <input id="AddDep" name="AddDep" value="<?php if(isset($_POST['AddDep'])) echo number_format($_POST['AddDep'],0,'.',''); ?>" size="12" maxlength="7" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100"><strong>Additional Second Key Limit</strong></td>
                            <td width="230">
                                <input id="AddSec" name="AddSec" value="<?php if(isset($_POST['AddSec'])) echo number_format($_POST['AddSec'],0,'.',''); ?>" size="12" maxlength="7" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100"><strong>Additional Nav Disc Limit</strong></td>
                            <td width="230">
                                <input id="AddNav" name="AddNav" value="<?php if(isset($_POST['AddNav'])) echo number_format($_POST['AddNav'],0,'.',''); ?>" size="12" maxlength="7" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100"><strong>Additional Seller Repair Limit</strong></td>
                            <td width="230">
                                <input id="AddSel" name="AddSel" value="<?php if(isset($_POST['AddSel'])) echo number_format($_POST['AddSel'],0,'.',''); ?>" size="12" maxlength="7" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100"><strong>Additional Purchaser Repair Limit</strong></td>
                            <td width="230">
                                <input id="AddPur" name="AddPur" value="<?php if(isset($_POST['AddPur'])) echo number_format($_POST['AddPur'],0,'.',''); ?>" size="12" maxlength="7" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100"><strong>Additional Tire Copay</strong></td>
                            <td width="230">
                                <input id="AddTire" name="AddTire" value="<?php if(isset($_POST['AddTire'])) echo number_format($_POST['AddTire'],0,'.',''); ?>" size="12" maxlength="7" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100"><strong>Delivery Cost (if any)</strong></td>
                            <td width="230">
                                <input id="AddDelivery" name="AddDelivery" value="<?php if(isset($_POST['AddDelivery'])) echo number_format($_POST['AddDelivery'],0,'.',''); ?>" size="12" maxlength="7" type="text" onkeypress="javascript:return numbersonly(event);" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p><span class="gridfour">
                <button type="submit" value="Add" name="Add" class="med">ADD SVQ</button>
                <!--button type="submit" value="AddVeh" name="AddVeh" class="med">ADD SVQ USING A NEW VEHICLE</button-->
                </span></p>
            </div>
        </div><!-- grid eight container -->
    </form>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
