<?php require("globals.php"); ?>
<?php
    require_once('common/functions/globalfunctions.php');
    require_once('common/functions/emailfunctions.php');
    require_once('common/functions/imagefunctions.php');

    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 7;
    $_SESSION['titleadd'] = 'Register';

    if(!isset($_POST['firstname']) || !isset($_POST['lastname']) || !isset($_POST['zip']) || !isset($_POST['email']) || !isset($_POST['answer1']))
    {
        $_SESSION['ShowError'] = 'Missing Required Information: '.$_POST[firstname].'-'.$_POST[lastname].'-'.$_POST[zip].'-'.$_POST[email].'-'.$_POST[answer1].'\n'.' '.'\n\n';
        header('Location: my_register.php');
        exit();
    }

    if(!isset($_POST['homephone']) && !isset($_POST['cellphone']) && !isset($_POST['workphone']))
    {
        $_SESSION['ShowError'] = 'Missing Phone Number: '.'\n\n';
        header('Location: my_register.php');
        exit();
    }

    if(isset($_POST['homephone']))
    {
        $hlen = strlen(trim($_POST['homephone']));
        if(($hlen > 0) && ($hlen < 10))
        {
            $_SESSION['ShowError'] = 'Invalid Home Number: '.$_POST['homephone'].'\n\n';
            header('Location: my_register.php');
            exit();
        }
    }

    if(isset($_POST['cellphone']))
    {
        $clen = strlen(trim($_POST['cellphone']));
        if(($clen > 0) && ($clen < 10))
        {
            $_SESSION['ShowError'] = 'Invalid Cell Number: '.$_POST['cellphone'].'\n\n';
            header('Location: my_register.php');
            exit();
        }
    }

    if(isset($_POST['workphone']))
    {
        $wlen = strlen(trim($_POST['workphone']));
        if(($wlen > 0) && ($wlen < 10))
        {
            $_SESSION['ShowError'] = 'Invalid Work Number: '.$_POST['workphone'].'\n\n';
            header('Location: my_register.php');
            exit();
        }
    }

    if(!is_numeric(trim($_POST['zip'])))
    {
        $_SESSION['ShowError'] = 'Invalid ZIP: '.$_POST['zip'].'\n\n';
        header('Location: my_register.php');
        exit();
    }

    $zlen = strlen(trim($_POST['zip']));
    if($zlen != 5)
    {
        $_SESSION['ShowError'] = 'Invalid ZIP: '.$_POST['zip'].'\n\n';
        header('Location: my_register.php');
        exit();
    }


    // See what was passed in...
    $fname = $_POST['firstname'];
    $lname = $_POST['lastname'];
    $zip = $_POST["zip"];
    $email = $_POST["email"];
    $e2p = $_POST["e2p"];
    $homephone = $_POST["homephone"];
    $cellphone = $_POST["cellphone"];
    $celltext = $_POST["celltext"];
    $workphone = $_POST["workphone"];
    $workext = $_POST["workext"];
    $addphone1 = $_POST["addphone1"];
    $addphone1ext = $_POST["addphone1ext"];
    $addphone1type = $_POST["addphone1type"];
    $addphone1text = $_POST["addphone1text"];
    $addphone2 = $_POST["addphone2"];
    $addphone2ext = $_POST["addphone2ext"];
    $addphone2type = $_POST["addphone2type"];
    $addphone2text = $_POST["addphone2text"];
    $faxnum = $_POST["faxnum"];
    $loginname = $_POST["loginname"];
    $useemail = $_POST["useemail"];
    //$password = $_POST["password"];
    $rp = $_POST["rp"];
    $question1 = $_POST["question1"];
    $answer1 = $_POST["answer1"];
    $address1 = $_POST["address1"];
    $address2 = $_POST["address2"];
    $city = $_POST["city"];
    $thestate = $_POST["state"];
    $email2 = $_POST["email2"];
    $email2type = $_POST["email2type"];
    $e2p2 = $_POST["e2p2"];
    $email3 = $_POST["email3"];
    $email3type = $_POST["email3type"];
    $e2p3 = $_POST["e2p3"];
    $referred = $_POST["referred"];
    $repname = $_POST["repname"];
    $company = $_POST["company"];
    $photoname = $_POST["photoname"];
    $repcall = $_POST["repcall"];
    $franchise = $_POST["franchise"];

    $emailexists = 'false';
    $loginexists = 'false';
    $saveerror = 'false';
    $fileerror = 'false';
    $errorinmail = 'false';

    //echo '<pre>';

    //print_r($_POST);

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // See if this email address was already used or not...
        $query = "select * from users where email = '".escapestr($email)."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result)) $emailexists = 'true';
        else
        {
            // See if the login was used already...
            $query = "select * from userlogin where login = '".escapestr($loginname)."'";
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result)) $loginexists = 'true';
            else
            {
                mysql_query("begin", $con);

                // Add the user to the database...
                $query = "insert into users (firstname, lastname, email, emailtophone, franchiseinterest, created, lastupdated, company, referredby) values ('";
                $query .= ucwords(strtolower(escapestr($fname)))."','";
                $query .= ucwords(strtolower(escapestr($lname)))."','";
                $query .= escapestr($email)."',";
                if($e2p == 'Yes') $query .= "1,";
                else $query .= "0,";
                if($franchise == 'on') $query .= "1,'";
                else $query .= "0,'";
                $query .= date_at_timezone('Y-m-d H:i:s', 'EST')."','".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                if(strlen($company)>0) $query .= "'".escapestr($company)."',";
                else $query .= "NULL,";
                if(strlen($referred)>0) $query .= "'".escapestr($referred)."'";
                else $query .= "NULL";
                $query .= ")";

                if(!mysql_query($query, $con)) $saveerror = 'true';
                else
                {
                    $lastid = mysql_insert_id($con);
                    $ordernum = 1;

                    // Bring the temp file over to where we can work with it...
                    if(isset($_FILES['photoname']['name']) && (strlen($_FILES['photoname']['name']) > 0))
                    {
                        $ext = strtolower(end(explode(".", $_FILES['photoname']['name'])));
                        $path = WEB_ROOT_PATH;
                        $ifilename = 'userimages/User'.$lastid.'temp.'.$ext;
                        $target = $path.$ifilename;
                        if(!is_uploaded_file($_FILES['photoname']['tmp_name'])) $fileerror = 'Transfer Error - usually due to files over '.ini_get('upload_max_filesize').' in size';
                        else
                        {
                            if(($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE"))
                            {
                                if($_FILES["photoname"]["size"] > (20*1024*1024)) $fileerror = 'File is larger than 20M...'.number_format($_FILES["photoname"]["size"]/1024/1024).'M';
                                else
                                {
                                    if($_FILES["photoname"]["error"] > 0) $fileerror = 'File Has an Error';
                                    else
                                    {
                                        if(file_exists($target)) unlink($target);
                                        if(!move_uploaded_file($_FILES['photoname']['tmp_name'], $target)) $fileerror = 'Image Invalid';
                                    }
                                }
                            }
                            else $fileerror = 'File is not a GIF, JPG or PNG format';
                        }
                    }
                    else $ifilename = '';

                    if($fileerror == 'false')
                    {
                        if($ifilename && (strlen($ifilename)>0))
                        {
                            $ext = strtolower(end(explode(".", $ifilename)));
                            $path = WEB_ROOT_PATH;
                            $ofilename = 'userimages/User'.$lastid.'.'.$ext;
                            $target = $path.$ofilename;
                            if(file_exists($target)) unlink($target);
                            if(!ScaledImageFile($ifilename, 200, 300, $ofilename, 'false', 'true')) $fileerror = 'Could not create thumbnail for your image!';
                            else
                            {
                                unlink($ifilename);

                                // Update the user in the database...
                                $query = "update users set imagefile = '".escapestr($ofilename)."' where userid = ".$lastid;
                                if(!mysql_query($query, $con)) $saveerror = 'Could not update user image!';
                            }
                        }
                    }

                    // Add Home Phone if available...
                    if(strlen($homephone)>0)
                    {
                        $query = "insert into usernumbers (userid, numbertypeid, phonenumber, displayorder, texting, startdate) values (".$lastid.",1,";
                        $query .= "'".$homephone."',";
                        $query .= $ordernum.",0,";
                        $ordernum++;
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Cell Phone if available...
                    if(($saveerror != 'true') && (strlen($cellphone)>0))
                    {
                        $query = "insert into usernumbers (userid, numbertypeid, phonenumber, displayorder, texting, startdate) values (".$lastid.",2,";
                        $query .= "'".$cellphone."',";
                        $query .= $ordernum.",";
                        $ordernum++;
                        if($celltext == 'Yes') $query .= "1,";
                        else $query .= "0,";
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Work Phone if available...
                    if(($saveerror != 'true') && (strlen($workphone)>0))
                    {
                        $query = "insert into usernumbers (userid, numbertypeid, phonenumber, extension, displayorder, texting, startdate) values (".$lastid.",3,";
                        $query .= "'".$workphone."',";
                        if(strlen($workext)>0) $query .= "'".$workext."',";
                        else $query .= "NULL,";
                        $query .= $ordernum.",0,";
                        $ordernum++;
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Fax if available...
                    if(($saveerror != 'true') && (strlen($faxnum)>0))
                    {
                        $query = "insert into usernumbers (userid, numbertypeid, phonenumber, displayorder, texting, startdate) values (".$lastid.",4,";
                        $query .= "'".$faxnum."',";
                        $query .= $ordernum.",0,";
                        $ordernum++;
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Other Phone 1 if available...
                    if(($saveerror != 'true') && (strlen($addphone1)>0))
                    {
                        $query = "insert into usernumbers (userid, numbertypeid, phonenumber, extension, displayorder, texting, addinfo, startdate) values (".$lastid.",6,";
                        $query .= "'".$addphone1."',";
                        if(strlen($addphone1ext)>0) $query .= "'".escapestr($addphone1ext)."',";
                        else $query .= "NULL,";
                        $query .= $ordernum.",";
                        $ordernum++;
                        if($addphone1text == 'Yes') $query .= "1,";
                        else $query .= "0,";
                        if(strlen($addphone1type)>0) $query .= "'".escapestr($addphone1type)."',";
                        else $query .= "NULL,";
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Other Phone 2 if available...
                    if(($saveerror != 'true') && (strlen($addphone2)>0))
                    {
                        $query = "insert into usernumbers (userid, numbertypeid, phonenumber, extension, displayorder, texting, addinfo, startdate) values (".$lastid.",6,";
                        $query .= "'".$addphone2."',";
                        if(strlen($addphone2ext)>0) $query .= "'".escapestr($addphone2ext)."',";
                        else $query .= "NULL,";
                        $query .= $ordernum.",";
                        $ordernum++;
                        if($addphone2text == 'Yes') $query .= "1,";
                        else $query .= "0,";
                        if(strlen($addphone2type)>0) $query .= "'".escapestr($addphone2type)."',";
                        else $query .= "NULL,";
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Address if available...
                    if(($saveerror != 'true') && (strlen($zip)>0))
                    {
                        $query = "insert into useraddresses (userid, isprimary, zip, address1, address2, city, state, startdate) values (".$lastid.",1,";
                        $query .= "'".$zip."',";
                        if(strlen($address1)>0) $query .= "'".escapestr($address1)."',";
                        else $query .= "NULL,";
                        if(strlen($address2)>0) $query .= "'".escapestr($address2)."',";
                        else $query .= "NULL,";
                        if(strlen($city)>0) $query .= "'".ucwords(strtolower(escapestr($city)))."',";
                        else $query .= "NULL,";
                        if(strlen($thestate)>0) $query .= "'".strtoupper($thestate)."',";
                        else $query .= "NULL,";
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    $ordernum = 1;
                    // Add Alternate Email if available...
                    if(($saveerror != 'true') && (strlen($email2)>0))
                    {
                        $query = "insert into alternateemails (userid, email, addinfo, emailtophone, displayorder, startdate) values (".$lastid.",";
                        $query .= "'".escapestr($email2)."',";
                        if(strlen($email2type)>0) $query .= "'".escapestr($email2type)."',";
                        else $query .= "NULL,";
                        if($e2p2 == 'Yes') $query .= "1,";
                        else $query .= "0,";
                        $query .= $ordernum.",";
                        $ordernum++;
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Alternate Email if available...
                    if(($saveerror != 'true') && (strlen($email3)>0))
                    {
                        $query = "insert into alternateemails (userid, email, addinfo, emailtophone, displayorder, startdate) values (".$lastid.",";
                        $query .= "'".escapestr($email3)."',";
                        if(strlen($email3type)>0) $query .= "'".escapestr($email3type)."',";
                        else $query .= "NULL,";
                        if($e2p3 == 'Yes') $query .= "1,";
                        else $query .= "0,";
                        $query .= $ordernum.",";
                        $ordernum++;
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add Login Info if available...
                    if($saveerror != 'true')
                    {
                        $query = "insert into userlogin (userid, useemail, login, password, pwquestion1, pwanswer1) values (".$lastid.",";
                        if($useemail == 'on') $query .= "1,NULL,";
                        else
                        {
                            if(strlen($loginname)>0) $query .= "0,'".escapestr($loginname)."',";
                            else $query .= "1,NULL,";
                        }
                        $query .= "'".md5($rp)."',";
                        $query .= "'".$question1."',";
                        if(strlen($answer1) > 0) $query .= "'".escapestr($answer1)."')";
                        else $query .= "'none')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // Add a Market Need to work from...
                    if($saveerror != 'true')
                    {
                        $query = "insert into marketneeds (userid, title, needscontact, lastlogin, created) values (".$lastid.",'Original Search',";
                        if($repcall == 'on') $query .= "1,";
                        else $query .= "0,";
                        $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."','".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                        else $marketneedid = mysql_insert_id($con);
                    }
                    
                    
                    if($saveerror != 'true')
                    {
                        $query_assignedreps = "INSERT INTO  `assignedreps` (`MarketNeedID` ,`StartDate` ,`UserRepID`) VALUES ('".$marketneedid."',  '" . date_at_timezone('Y-m-d H:i:s', 'EST')."',  '5676');";
                        if(!mysql_query($query_assignedreps, $con)) $saveerror = 'true';                        
                    }
                    
                    // Add a Profile for the user...
                    if($saveerror != 'true')
                    {
                        $query = "insert into userprofiles (userid, profileid) values (".$lastid.",1)";
                        if(!mysql_query($query, $con)) $saveerror = 'true';
                    }

                    // TODO: Do the Assign Rep function and email the assigned rep plus add a Message to the user...
                } // user was created
                if($saveerror != 'true')
                {
                    mysql_query("commit", $con);

                    if($useemail == 'on') $login = $email;
                    else $login = $loginname;
                    if(isset($cellphone) && (strlen($cellphone) > 0)) $phone = $cellphone.' [Cell]';
                    elseif(isset($homephone) && (strlen($homephone) > 0)) $phone = $homephone.' [Home]';
                    elseif(isset($workphone) && (strlen($workphone) > 0)) $phone = $workphone.' [Work]';
                    else $phone = 'unknown';
                    if($phone != 'unknown') $phone = '('.substr($phone,0,3).') '.substr($phone,3,3).'-'.substr($phone,6);
                    $newusername = $fname.' '.$lname;
                    if(!sendwelcome($con, $email, $login, $phone, $newusername, $zip)) $errorinmail = 'true';

                    //$message = 'Thank you for registering with <a href="'.WEB_SERVER_NAME.'index.php">1-800-vehicles.com</a>!<br/><br/>';
                    //$message .= 'For your records, the login you entered was: ';
                    //if($useemail == 'on') $message .= $email;
                    //else $message .= $loginname;
                    //$message .= '<br/<br/><a href="'.WEB_SERVER_NAME.'index.php">1-800-vehicles.com</a> may contact you to see if you would like free consulting.<br/><br/>';
                    //$message .= 'To be sure you get our emails, ';
                    //$message .= 'you should add <a href="mailto://vehicles@1800vehicles.com">vehicles@1800vehicles.com</a> and ';
                    //$message .= '<a href="mailto://sales@1800vehicles.com">sales@1800vehicles.com</a> to your email program\'s whitelist.<br/>';
                    //$message .= 'This will prevent our messages to you from being blocked by spam filters.<br/><br/>';
                    //$message .= 'To change any of your personal information, log in and click on \'My Account\'.<br/><br/>';
                    //$message .= 'If you forget your password follow the \'Forgot Password\' link in the Login Area to set a new one.';
                    //if(!sendemail($email, 'Thank you for registering!', $message, 'false')) $errorinmail = 'true';
                }
                else mysql_query("rollback", $con);
            } // login does not exist
        } // email does not exist
        mysql_close($con);
    } // done with database
?>

<!DOCTYPE html>
<html lang="en">

<?php include("header.php"); ?>
<section id="about-us">
    <div class="section-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                <?php
                    if($emailexists != 'false')
                    {
                        echo '<h1 class="subhead" >Email Exists!</h1>';
                    }
                    elseif($loginexists != 'false')
                    {
                        echo '<h1 class="subhead" >Login Exists!</h1>';
                    }
                    elseif($saveerror != 'false')
                    {
                        echo '<h1 class="subhead">Registration Issue!</h1>';
                    }
                    else
                    {
                        echo '<h1 class="subhead">Thank you for registering!</h1>';
                    }
                ?>
                        <div class="grideightgrey">
                            <div class="grideight">
                <?php
                    if($emailexists != 'false')
                    {
                        echo '<p class="blacktwelve">Sorry!  That email has already been used in the system.</p>';
                        echo '<p class="blacktwelve">You may login at the top of the page, or if you have forgotten your password, please visit the <a href="forgotpass1.php">forgotten password</a> area.</p>';
                    }
                    elseif($loginexists != 'false')
                    {
                        echo '<p class="blacktwelve">Sorry!  That login has already been used in the system.</p>';
                        echo '<p class="blacktwelve">You may login at the top of the page, or if you have forgotten your password, please visit the <a href="forgotpass1.php">forgotten password</a> area.</p>';
                    }
                    elseif($saveerror != 'false')
                    {
                        echo '<p class="blacktwelve">Sorry!  There was an error processing your registration.</p>';
                        echo '<p class="blacktwelve">Please contact a representative at 1-800-vehicles (834-4253) for help with this issue.</p>';
                        if($saveerror != 'true') echo $saveerror.'<br/>';
                        echo $query.'<br/>';
                    }
                    else
                    {
                        if($useemail == 'on') $_SESSION['user'] = $email;
                        else $_SESSION['user'] = $loginname;
                        $_SESSION['userid'] = $lastid;
                        $_SESSION['marketneedid'] = $marketneedid;
                        $_SESSION['firstname'] = $fname;
                        $_SESSION['lastname'] = $lname;
                        echo '<p class="blacktwelve">Your information has been saved.</p>';
                        if($errorinmail != 'true') echo '<p class="blacktwelve">Your login has been emailed to you.</p>';
                        else echo '<p class="blacktwelve">Your login could not be emailed to you...please verify your email address is correct in the My Account area.</p>';
                        echo '<p class="blacktwelve">To change your information, log in and go to "My Account".</p>';
                        echo '<p class="blacktwelve" style="color:#85C11B;"><strong>You must log in to get quotes or consult with our staff.</strong></p>';
                        if($fileerror != 'false')
                        {
                            echo '<br/><br/><p class="blacktwelve">Note: Your photo could not be uploaded due to the following error: '.$fileerror.'<br/>';
                            echo 'You may try again from the My Account area once logged in.</p>';
                        }
                        echo '<p class="blacktwelve" style="color:#757575;"><strong>You will be logged in automatically by clicking <a href="mydashboard.php">here</a>.</strong></p>';
                    }
                ?>
            </div>  <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
</div>
</section>
<section id="footer-widgets" class="divider-wrapper"></section>

   <?php include("footer.php") ?>

</html>
