<?php require("globals.php"); ?>
<?php
//error_reporting(E_ALL);
//ini_set("display_errors", "on");

//redirect to homepage if user is already logged in
if( $_SESSION['user'] ) {
	header('Location: dashboard.php');
	exit();
}

$_SESSION['state']    = 0;
$_SESSION['substate'] = 7;
$_SESSION['titleadd'] = 'Registration Form';
//echo'<pre>';print_r($_SESSION);echo'</pre>';


//Set value for submit button
if( isset( $_SESSION['submit_btn'] ) ) {
	$submit_value = $_SESSION['submit_btn'];
} else {
	$submit_value = "Create Account";
}


require_once('common/functions/globalfunctions.php');
require_once('common/functions/emailfunctions.php');
require_once('common/functions/imagefunctions.php');

$emailexists = 'false';
$loginexists = 'false';
$saveerror   = 'false';
$fileerror   = 'false';
$errorinmail = 'false';

if (isset($_POST['btnsubmit'])) {
    
    if (!isset($_POST['firstname']) || !isset($_POST['lastname']) || !isset($_POST['zip']) || !isset($_POST['email'])) {
        $_SESSION['ShowError'] = 'Missing Required Information: ' . $_POST[firstname] . '-' . $_POST[lastname] . '-' . $_POST[zip] . '-' . $_POST[email];
    }
    if (!isset($_POST['cellphone'])) {
        $_SESSION['ShowError'] = 'Missing Phone Number';
    }
    if (!is_numeric(trim($_POST['zip']))) {
        $_SESSION['ShowError'] = 'Invalid ZIP';
    }
    $fname     = $_POST['firstname'];
    $lname     = $_POST['lastname'];
    $zip       = $_POST["zip"];
    $email     = $_POST["email"];
    $cellphone = $_POST["cellphone"];
    $useemail  = $_POST["useemail"];
    $rp        = $_POST["rp"];
    $loginname = $_POST["loginname"];
    $phonetyle = $_POST["phonetyle"];
    $repcall   = "";
    
    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);
        
        // See if this email address was already used or not...
        $query  = "select * from users where email = '" . escapestr($email) . "'";
        $result = mysql_query($query, $con);
        if ($result && $row = mysql_fetch_array($result))
            $emailexists = 'true';
        else {
            // See if the login was used already...
            $query  = "select * from userlogin where login = '" . escapestr($loginname) . "'";
            $result = mysql_query($query);
            if ($result && $row = mysql_fetch_array($result))
                $loginexists = 'true';
            else {
                //mysql_query("begin", $con);
                // Add the user to the database...
                $query = "insert into users (firstname, lastname, email,created, lastupdated) values ('";
                $query .= ucwords(strtolower(escapestr($fname))) . "','";
                $query .= ucwords(strtolower(escapestr($lname))) . "','";
                $query .= escapestr($email) . "','";
                $query .= date_at_timezone('Y-m-d H:i:s', 'EST') . "','" . date_at_timezone('Y-m-d H:i:s', 'EST') . "'";
                $query .= ")";
                if (!mysql_query($query, $con)) {
                    echo $saveerror = 'true';
                } else {
                    $lastid = mysql_insert_id();
                    
                    $ordernum = 1;
                    // Add Cell Phone if available...
                    if (($saveerror != 'true') && (strlen($cellphone) > 0)) {
                        $query = "insert into usernumbers (userid, numbertypeid, phonenumber, displayorder, texting, phonetyle ,startdate) values (" . $lastid . ",2,";
                        $query .= "'" . $cellphone . "',";
                        $query .= $ordernum . ",";
                        $ordernum++;
                        if ($celltext == 'Yes')
                            $query .= "1,";
                        else
                            $query .= "0,";
                        $query .= "'" . $phonetyle . "',";
                        $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
                        if (!mysql_query($query, $con))
                            $saveerror = 'true';
                    }
                    
                    // Add Address if available...
                    if (($saveerror != 'true') && (strlen($zip) > 0)) {
                        $query = "insert into useraddresses (userid, isprimary, zip, address1, address2, city, state, startdate) values (" . $lastid . ",1,";
                        $query .= "'" . $zip . "',";
                        if (strlen($address1) > 0)
                            $query .= "'" . escapestr($address1) . "',";
                        else
                            $query .= "NULL,";
                        if (strlen($address2) > 0)
                            $query .= "'" . escapestr($address2) . "',";
                        else
                            $query .= "NULL,";
                        if (strlen($city) > 0)
                            $query .= "'" . ucwords(strtolower(escapestr($city))) . "',";
                        else
                            $query .= "NULL,";
                        if (strlen($thestate) > 0)
                            $query .= "'" . strtoupper($thestate) . "',";
                        else
                            $query .= "NULL,";
                        $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
                        if (!mysql_query($query, $con))
                            $saveerror = 'true';
                    }
                    
                    $ordernum = 1;
                    
                    // Add Login Info if available...
                    if ($saveerror != 'true') {
                        $query = "insert into userlogin (userid, useemail, login, password) values (" . $lastid . ",";
                        if ($useemail == 'on')
                            $query .= "1,NULL,";
                        else {
                            if (strlen($loginname) > 0)
                                $query .= "0,'" . escapestr($loginname) . "',";
                            else
                                $query .= "1,NULL,";
                        }
                        $query .= "'" . md5($rp) . "')";
                        if (!mysql_query($query, $con))
                            $saveerror = 'true';
                    }
                    
                    // Add a Market Need to work from...
                    if ($saveerror != 'true') {
                        $query = "insert into marketneeds (userid, title, needscontact, lastlogin, created) values (" . $lastid . ",'Original Search',";
                        if ($repcall == 'on')
                            $query .= "1,";
                        else
                            $query .= "0,";
                        $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
                        if (!mysql_query($query, $con))
                            $saveerror = 'true';
                        else
                            $marketneedid = mysql_insert_id($con);
                    }
                    
                    
                    if ($saveerror != 'true') {
                        $query_assignedreps = "INSERT INTO  `assignedreps` (`MarketNeedID` ,`StartDate` ,`UserRepID`) VALUES ('" . $marketneedid . "',  '" . date_at_timezone('Y-m-d H:i:s', 'EST') . "',  '5676');";
                        if (!mysql_query($query_assignedreps, $con))
                            $saveerror = 'true';
                    }
                    
                    // Add a Profile for the user...
                    if ($saveerror != 'true') {
                        $query = "insert into userprofiles (userid, profileid) values (" . $lastid . ",1)";
                        if (!mysql_query($query, $con))
                            $saveerror = 'true';
                    }
                    
                    // TODO: Do the Assign Rep function and email the assigned rep plus add a Message to the user...
                } // user was created
                
                
                if ($saveerror != 'true') {
                    
                    // echo'<pre>';print_r($_SESSION);echo'</pre>';die('here');
                    mysql_query("commit", $con);
                    
                    if ($useemail == 'on')
                        $login = $email;
                    else
                        $login = $loginname;
                    if (isset($cellphone) && (strlen($cellphone) > 0))
                        $phone = $cellphone . ' [Cell]';
                    elseif (isset($homephone) && (strlen($homephone) > 0))
                        $phone = $homephone . ' [Home]';
                    elseif (isset($workphone) && (strlen($workphone) > 0))
                        $phone = $workphone . ' [Work]';
                    else
                        $phone = 'unknown';
                    if ($phone != 'unknown')
                        $phone = '(' . substr($phone, 0, 3) . ') ' . substr($phone, 3, 3) . '-' . substr($phone, 6);
                    $newusername = $fname . ' ' . $lname;
                    if (!sendwelcome($con, $email, $login, $phone, $newusername, $zip)) {
                        $errorinmail = 'true';
                    }                    
                    
                    if ( $_SESSION['assessment_post'] ) {
                        $fname                    = $_POST['firstname'];
                        $_SESSION['user']         = $email;
                        $_SESSION['userid']       = $lastid;
                        $_SESSION['marketneedid'] = $marketneedid;
                        $_SESSION['firstname']    = $fname;
                        $_SESSION['lastname']     = $lname;
                        echo "<script>window.location='assessmentsave.php';</script>";
                    
					} elseif ( $_SESSION['pos_post'] ) {
                        $fname                    = $_POST['firstname'];
                        $_SESSION['user']         = $email;
                        $_SESSION['userid']       = $lastid;
                        $_SESSION['marketneedid'] = $marketneedid;
                        $_SESSION['firstname']    = $fname;
                        $_SESSION['lastname']     = $lname;
                        echo "<script>window.location='posplansave.php';</script>";
						
                    } elseif ( $_SESSION['searchplan_post'] ) {
                        $fname                    = $_POST['firstname'];
                        $_SESSION['user']         = $email;
                        $_SESSION['userid']       = $lastid;
                        $_SESSION['marketneedid'] = $marketneedid;
                        $_SESSION['firstname']    = $fname;
                        $_SESSION['lastname']     = $lname;
                        echo "<script>window.location='vehiclespecsave.php';</script>";
						
                    } elseif ( $_SESSION['consult_post'] ) {
                        $fname                    = $_POST['firstname'];
                        $_SESSION['user']         = $email;
                        $_SESSION['userid']       = $lastid;
                        $_SESSION['marketneedid'] = $marketneedid;
                        $_SESSION['firstname']    = $fname;
                        $_SESSION['lastname']     = $lname;
						$_SESSION['welcome_message'] = "Welcome " . $fname . " " . $lname . "! You are now registered to our website. You can now continue to request for consultation.";
                        echo "<script>window.location='consult.php';</script>";
						
                    } elseif ( $_SESSION['request_trade_in_quote'] ) {
                        $fname                    = $_POST['firstname'];
                        $_SESSION['user']         = $email;
                        $_SESSION['userid']       = $lastid;
                        $_SESSION['marketneedid'] = $marketneedid;
                        $_SESSION['firstname']    = $fname;
                        $_SESSION['lastname']     = $lname;
						$_SESSION['welcome_message'] = "Welcome " . $fname . " " . $lname . "! You are now registered to our website. You can now continue to request for trade-in quote.";
                        echo "<script>window.location='requesttradeinquote.php';</script>";
                    
					} elseif ( $_SESSION['save_fav11'] ) {
                        $fname                    = $_POST['firstname'];
                        $_SESSION['user']         = $email;
                        $_SESSION['userid']       = $lastid;
                        $_SESSION['marketneedid'] = $marketneedid;
                        $_SESSION['firstname']    = $fname;
                        $_SESSION['lastname']     = $lname;
                        echo "<script>window.location='favorites.php';</script>";
						
                    } else {
                        $_SESSION['user'] = $email;
                        echo "<script>window.location='success.php?eid=" . base64_encode($lastid) . "';</script>";
                    }
                    
                    //$message = 'Thank you for registering with <a href="'.WEB_SERVER_NAME.'index.php">1-800-vehicles.com</a>!<br/><br/>';
                    //$message .= 'For your records, the login you entered was: ';
                    //if($useemail == 'on') $message .= $email;
                    //else $message .= $loginname;
                    //$message .= '<br/<br/><a href="'.WEB_SERVER_NAME.'index.php">1-800-vehicles.com</a> may contact you to see if you would like free consulting.<br/><br/>';
                    //$message .= 'To be sure you get our emails, ';
                    //$message .= 'you should add <a href="mailto://vehicles@1800vehicles.com">vehicles@1800vehicles.com</a> and ';
                    //$message .= '<a href="mailto://sales@1800vehicles.com">sales@1800vehicles.com</a> to your email program\'s whitelist.<br/>';
                    //$message .= 'This will prevent our messages to you from being blocked by spam filters.<br/><br/>';
                    //$message .= 'To change any of your personal information, log in and click on \'My Account\'.<br/><br/>';
                    //$message .= 'If you forget your password follow the \'Forgot Password\' link in the Login Area to set a new one.';
                    //if(!sendemail($email, 'Thank you for registering!', $message, 'false')) $errorinmail = 'true';
                } else {
                    mysql_query("rollback", $con);
                }
            } // login does not exist
        } // email does not exist
        mysql_close($con);
    } // done with database
}
?>
<?php
require("headerstart.php");
?>
<?php
require("header.php");
?>
<?php
require("foursteps.php");
?>
<?php //require("headerend.php"); 
?>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/additional-methods.js"></script>
<style>
    .borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
        border: none;
    }
    select, textarea, input[type="text"], input[type="password"] {
        background: none repeat scroll 0 0 #fff;
        border: 1px solid #aaaaaa;
        color: #252525;
        cursor: text;
        font-family: Arial,Helvetica,Sans Serif;
        font-size: 13px;
        padding: 3px;
        resize: none;
        width: 100%;
        padding: 0 6px;
    }

    .gridfour_reg {
        float: left;
        margin: 10px 0px;
        position: relative;
        width: 100%;
    }

    .btn-primary_reg {
        background-color: #85c11b;
        border-color: #85c11b;
        border-radius: 0;
        color: #fff;
        font-weight: bold;
        padding: 10px 15px;
        text-transform: uppercase;
        transition: all 0.5s ease-in-out 0s;
        

    }

    .help-block, help-block2 {
        color: red;
        display: block;
        font-size: 12px;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
.select-style {
    border: 1px solid #aaaaaa;
    border-radius: 3px;
    height: 20%;
    margin: 0;
    overflow: hidden;
    padding: 10px;
    width: 100%;
}


    
    
</style>
<link rel="stylesheet" href="assets/css/colorbox.css" />
<script src="assets/js/jquery.colorbox.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".btncontinue").click(function() {            
            var emailval = $('#hiddenstep').val();
            var form = $("#myform");
            form.validate({
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function(element, errorClass, validClass) {
                    $(element).closest('.form-group').addClass("has-error");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).closest('.form-group').removeClass("has-error");
                },
                rules: {
                    firstname: {
                        required: true,
                    },
                    lastname: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    zip: {
                        required: true,
                        minlength: 5,
                    },
                    loginname: {
                        required: true,
                    },
                    cellphone: {
                        required: true,
                    },
                    rp: {
                        required: true,
                        minlength: 5,
                    },
                    rp2: {
                        required: true,
                        minlength: 5,
                        equalTo: '#rp',
                    },
                },
                messages: {
                    firstname: {
                        required: "First name required",
                    },
                    lastname: {
                        required: "Last name required",
                    },
                    email: {
                        required: "Email required",
                    },
                    zip: {
                        required: "Zip required",
                    },
                    cellphone: {
                        required: "Phone required",
                    },
                    loginname: {
                        required: "Username required",
                    },
                    rp: {
                        required: "Password required",
                    },
                    rp2: {
                        required: "Verify Password required",
                        equalTo: "Password don't match",
                    }
                }
            });
            if (form.valid() === true && emailval == 0) {
                if ($('#step1').is(":visible")) {

                    current_fs = $('#step1');
                    next_fs = $('#step2');

                }
                next_fs.show();
                current_fs.hide();
            }
        });

        $('#btnback').click(function() {
            if ($('#step2').is(":visible")) {
                current_fs = $('#step2');
                next_fs = $('#step1');
            }
            next_fs.show();
            current_fs.hide();
        });

        $("#email").keyup(function() {
//gets the value of the field
            var email = $("#email").val();

//here would be a good place to check if it is a valid email before posting to your db

//displays a loader while it is checking the database
            $("#emailError").html('<img alt="" src="images/ajax-loader.gif" />');

//here is where you send the desired data to the PHP file using ajax
            $.post("email_validate.php", {email: email},
            function(result) {
                if (result == 0) {
//the email is available
                    $("#emailError").html("Available").css("color", "green");
                    $('#hiddenstep').val('0');
                }
                else if (result == 2) {
                    $("#emailError").html("Email not Valid").css("color", "red");
                    $('#hiddenstep').val('1')
                } else {
//the email is not available
                    $("#emailError").html("Email already Exist").css("color", "red");
                    $('#hiddenstep').val('1')
                }
            });
        });


    });

</script>


<div class="gridtwelve"></div>
<div id="content" style="margin-top: 0;">
    <div class="grideightcontainer" style="margin-top: 0%;">
            
        
         <?php
//        echo "<pre>";
//        print_r($_SESSION);
//        echo "</pre>";
?>   
        
        
        
       
        
       
        <span style="text-align: center;width: 100%;margin-left: 0;color: red;">
            <?php
if ($emailexists != 'false') {
    echo '<h4>Email Exists!</h4>';
} elseif ($loginexists != 'false') {
    echo '<h4>Username Exists!</h4>';
} elseif ($saveerror != 'false') {
    echo '<h4>Registration Issue!</h4>';
}
?>
        </span>



        <form action="" autocomplete="off"  method="post" id="myform" name="userform" enctype="multipart/form-data">

            <input type="hidden" name="hiddenstep"  id="hiddenstep" />
            <table class="table borderless" id="step1" > 
                <tr>
                    <td>
                         <?php
//if request to create an account heading will be Create an Account
if (isset($_GET['request'])) {
    if ($_GET['request'] == "create_account") {
?>
        <h1 class="subhead" style="  text-align: center;width: 100%;margin-left: 0;">           
            Create an Account      
        </h1>
    <?php
    }
} elseif (isset($_SESSION['pos_post']) || isset($_SESSION['assessment_post']) || isset($_SESSION['searchplan_post']) || isset($_SESSION['consult_post']) || isset($_SESSION['request_trade_in_quote'])) {
?>
                        <div style="text-align: center;font-size: 20px;">Please create an account or  <a href="#" id="signin" style="color: #91D615;">LOGIN</a> if you already have one </div>
                        
                        <div style="text-align: center;">This does not obligate you !!</div>
              <?php
} else {
?>
                        <h1 class="subhead" style="  text-align: center;width: 100%;margin-left: 0;">           
                            Create an Account      
                        </h1>  
            <?php
}
?>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label for="firstname" ><span>F</span>irst <span>N</span>ame <span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <input type="text" name="firstname" id="firstname" class="form-control"  value="<?= $_POST['firstname'] ?>" maxlength="50" required="" data-validation-required-message="Please enter your first name."/>
                    </td>                                                
                </tr>
                <tr>
                    <td>
                        <label for="firstname" ><span>L</span>ast <span>N</span>ame <span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <input type="text" name="lastname" class="form-control"  id="lastname" value="<?= $_POST['lastname'] ?>" maxlength="50" required="" data-validation-required-message="Please enter your last name." />
                    </td>                                                
                </tr>
                <tr>
                    <td>
                        <label for="zip" >Zip <span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <input name="zip" id="zip" type="text" class="form-control"  value="<?= $_POST['zip'] ?>" maxlength="5" required="" data-validation-required-message="Please enter zip." onkeypress="javascript:return numbersonly(event);" />
                    </td>                                                
                </tr>

                <tr>
                    <td>
                        <label for="email" >Email <span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <input type="text" name="email" id="email" class="form-control" required=""  data-validation-required-message="Please enter your email."  value="" maxlength="200" onblur="javascript:emailchanged()" />                       
                        <span id="emailError" class="help-block2"></span>
                    </td>                                                
                </tr>


                <tr>
                    <td>
                        <label for="cellphone" >Phone<span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>  
                    <td>
                        <div style="width: 100%;">
                            <span style="float: left;width: 70%;">
                                 <input style="" name="cellphone" id="cellphone" type="text" class="form-control"  value="<?= $_POST['cellphone'] ?>" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" />
                            </span>
                            <span style="float: left;width: 30%;">
                                <select class="select-style" name="phonetyle" id="phonetyle">
                                 <option value="mobile">Mobile</option>
                                 <option value="home">Home</option>
                                 <option value="work">Work</option>
                            </select>
                            </span>
                        </div>
                       
                            
                    </td>                                                
                </tr>


                <tr>
                    <td align="center">

                        <span class="gridfour_reg">
                            <input type="button" name="btncontinue" id="btncontinue"  value="Continue" class="btn btn-primary_reg btncontinue">
                        </span>
                    </td>                                                
                </tr>

            </table>

            <table class="table borderless" id="step2" style="display: none;">                                
                 <tr>
                    <td>
               
                        <h1 class="subhead" style="  text-align: center;width: 100%;margin-left: 0;">           
                            ALMOST DONE!    
                        </h1>  
        
                    </td>
                </tr>
                
                <tr><td></td></tr>
                <tr>
                    <td>
                        <div style="background-color: gray; color: white; padding: 7px;">
                            <strong class="assessment_inside">Create a Login &amp; Password:</strong>
                            </div>
                    </td></tr>
                <tr>
                    <td>
                        <p style="margin-top: 5px; color: rgb(68, 68, 68); font-weight: bold; font-size: 14px"><span style="font-size: 16px;color: red">&#42;</span> We suggest using your email as your login name</p>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <div>
                            <span style="float: left;"> <label for="loginname"><span>L</span>ogin</label> </span> <span style="float: right;"><input type="checkbox" name="useemail" id="useemail" checked="checked" onclick="javascript:useemailswitched();"/> Use email as login name<br/></span>
                        </div>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>
                        <input type="text" name="loginname" id="loginname" value="<?= $_POST['loginname'] ?>" size="30" class="form-control"  maxlength="20" disabled="disabled" style="background:LightGrey none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial;" />
                    </td>                                                
                </tr>
                <tr>
                    <td>


                        <div>
                            <span style="float: left;"> <label for="rp"> <span>P</span>assword <span style="font-size: 16px;color: red">&#42;</span></label> </span> <span style="float: right;font-size: 14px; color: rgb(153, 153, 153);">(Password must be a minimum of six letters and at least one number. No symbols please.)</span>
                        </div>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>                         
                        <input type="password" id="rp" name="rp"  value="<?= $_POST['rp'] ?>" class="form-control"  size="30"  maxlength="15" />                                               
                    </td>                                                
                </tr>
                <tr>
                    <td>
                        <label for="rp2" > Verify <span>P</span>assword <span style="font-size: 16px;color: red">&#42;</span></label>
                    </td>                                                                             
                </tr>
                <tr>
                    <td>

                        <input type="password" id="rp2" name="rp2"  class="form-control"  value="<?= $_POST['rp2'] ?>" size="30" maxlength="15" />
                    </td>                                                
                </tr> 


                <tr>
                    <td align="center">
                        <span style="font-size: 14px; color: rgb(153, 153, 153);">There is no cost to joining our site, and no obligation whatsoever</span>                       
                    </td>                                                                             
                </tr>

                <tr>
                    <td align="center">

                        <span class="gridfour_reg">
                            <input type="button" name="btnback" id="btnback" value="BACK" class="btn btn-primary_reg"> &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="submit" name="btnsubmit"  value="<?php echo $submit_value; ?>" class="btn btn-primary_reg">
                        </span>
                    </td>                                                
                </tr>

            </table>


        </form>
    </div><!-- end grideightgrey-->

    <style>
        .assessment_insidetd{
            background: none repeat scroll 0% 0% gray; 
            color: white;
        }
        .assessment_insidsales{
            background: none repeat scroll 0% 0% #85c11b; 
            color: white;
        }


    </style>
<?php
require("teaser.php");
?>
</div><!--end content-->

<?php
require("footerstart.php");
?>
   
    <?php
require("footer.php");
?>
<?php
require("footerend.php");
?>

<script type="text/javascript">
    function clearform()
    {
        emailchanged();
    }



    function validateOneSet(fld1, fld2, fld3)
    {
        if (fld1.value.length > 0)
        {
            return true;
        }
        if (fld2.value.length > 0)
        {
            return true;
        }
        if (fld3.value.length > 0)
        {
            return true;
        }
        return false;
    }

    function trimAll(sString)
    {
        while (sString.substring(0, 1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while (sString.substring(sString.length - 1, sString.length) == ' ')
        {
            sString = sString.substring(0, sString.length - 1);
        }
        return sString;
    }

    function validateEmpty(fld, title)
    {
        var error = "";
        var tString = trimAll(fld.value);

        if (tString.length == 0)
        {
            fld.style.background = '#ffcccc';
            error = title + " is required." + '\n';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    function validateZip(fld)
    {
        var error = "";
        if (fld.value.length == 0)
            return error;
        if (fld.value.length != 5)
        {
            fld.style.background = '#ffcccc';
            error = "Zip Code must be five digits." + '\n';
        }
        return error;
    }

    function validateEmail(fld)
    {
        var error = "";
        var tfld = trimAll(fld.value);
        tfld = tfld.replace(/^\s+|\s+$/, '');  // value of field with whitespace trimmed off
        var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
        var illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/;

        if (fld.value.length == 0)
            return error;
        if (!emailFilter.test(tfld))
        {
            fld.style.background = '#ffcccc';
            error = "Email address is not valid." + '\n';
        }
        else if (fld.value.match(illegalChars))
        {
            fld.style.background = '#ffcccc';
            error = "Email address contains illegal characters." + '\n';
        }
        return error;
    }

    function validatePhone(fld)
    {
        var error = "";
        var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');

        if (fld.value.length == 0)
            return error;
        if (isNaN(parseInt(stripped, 10)))
        {
            error = "The phone number contains illegal characters." + '\n';
            fld.style.background = '#ffcccc';
        }
        else if (!(stripped.length == 10))
        {
            error = "The phone number is the wrong length." + '\n';
            fld.style.background = '#ffcccc';
        }
        return error;
    }

    function validatePassword(fld)
    {
        var error = "";
        var illegalChars = new RegExp("[\W_]");
        var letterChars = new RegExp("[A-Z|a-z]");
        var numChars = new RegExp("[0-9]");

        if (fld.value.length < 1)
        {
            error = "The password must be filled in. " + '\n';
            fld.style.background = '#ffcccc';
        }
        else if ((fld.value.length < 6) || (fld.value.length > 15))
        {
            error = "The password must be 6-15 characters or numbers. " + '\n';
            fld.style.background = '#ffcccc';
        }
        else if (illegalChars.test(fld.value))
        {
            error = "The password should contains only characters and numbers." + '\n';
            fld.style.background = '#ffcccc';
        }
        else if (!numChars.test(fld.value))
        {
            error = "The password must contain numbers." + '\n';
            fld.style.background = '#ffcccc';
        }
        else if (!letterChars.test(fld.value))
        {
            error = "The password must contain letters." + '\n';
            fld.style.background = '#ffcccc';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    function validatePassword2(fld, fld2)
    {
        var error = "";
        if (fld.value.length < 1)
        {
            error = "The verification password must be filled in. " + '\n';
            fld.style.background = '#ffcccc';
        }
        else if (fld.value != fld2.value)
        {
            error = "The verification password must match the first password field. " + '\n';
            fld.style.background = '#ffcccc';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    function useemailswitched()
    {
        document.forms["userform"].elements["loginname"].disabled = !document.forms["userform"].elements["loginname"].disabled;
        if (!document.forms["userform"].elements["loginname"].disabled)
            document.forms["userform"].elements["loginname"].style.background = 'White';
        else
            document.forms["userform"].elements["loginname"].style.background = 'LightGrey';
        emailchanged();
    }

    function emailchanged()
    {
        if (document.forms["userform"].elements["loginname"].disabled)
        {
            document.forms["userform"].elements["loginname"].disabled = false;
            document.forms["userform"].elements["loginname"].value = document.userform.email.value;
            document.forms["userform"].elements["loginname"].disabled = true;
        }
    }

// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);

// control keys
        if ((key == null) || (key == 0) || (key == 8) ||
                (key == 9) || (key == 13) || (key == 27))
            return true;

// numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        else
            return false;
    }
</script>
