<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = "All Virtual Vehicles";
    $_SESSION['onloadfunction'] = 'initform()';

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $yquery = "select distinct v.Year from vehicles v order by v.Year desc";
        $yresult = mysql_query($yquery, $con);
        $index = 0;
        while($yrow = mysql_fetch_array($yresult))
        {
            $allyears[$index] = $yrow[0];
            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    $(document).ready(function() {
        yearchanged();
    });
</script>

<style>

.new_med {
    background-color: #9BC74C;
    border-style: none;
    color: #FFFFFF !important;
    font-family: Helvetica,Arial,Sans-serif;
    font-size: 18px;
    margin-bottom: 10px;
    margin-top: 20px;
    padding: 15px;
}

.new_med:hover {
    background-color: #C2EE73;
    color: #444444 !important;
}

</style>

<script language="javascript" type="text/javascript">
<!--
    var yearhttpObject = null;
    var makehttpObject = null;
    var modelhttpObject = null;

    // Get the HTTP Object
    function getHTTPObject(){
        var xmlHttp = null;
        try
        {
            // Firefox, Opera, Safari
            xmlHttp = new XMLHttpRequest();
        }
        catch (e)
        {
            // Internet Explorer
            try
            {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e)
            {
                try
                {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e)
                {
                    alert('Your browser must be Firefox, Safari, Opera or IE 5 or higher');
                }
            }
        }
        return xmlHttp;
    }

//    function initform()
//    {
//        yearchanged();
//    }

    function yearchanged()
    {
        var vyearlist = document.getElementById("yearlist");

        yearhttpObject = getHTTPObject();
        if (yearhttpObject != null)
        {
            yearhttpObject.open("GET", "ajaxgetmakes.php?Year=" + vyearlist.options[vyearlist.selectedIndex].value, true);
            yearhttpObject.send(null);
            yearhttpObject.onreadystatechange = makesReturned;
            document.getElementById('makelist').style.cursor = "wait";
        }
    }

    function makesReturned()
    {
        if(yearhttpObject.readyState == 4)
        {
            var vmakelist = document.getElementById("makelist");

            // Clear the list out...
            while (vmakelist.options.length)  vmakelist.remove(0);

            var vlastmake = document.getElementById("LastMakeUsed");
            var found = 0;

            // Now add the list from the returned results...
            names = yearhttpObject.responseText.split(';');
            var index = 0;
            var current = 0;
            while (index < (names.length-1))
            {
                newOpt = document.createElement("option");
                newOpt.value = names[index];
                newOpt.text = names[index+1];
                if(vlastmake.value == newOpt.value) found = current;
                vmakelist.add(newOpt);
                index += 2;
                current++;
            }
            vmakelist.options[found].selected = true;
            vmakelist.style.cursor = "default";
            makechanged();
        }
    }

    function makechanged()
    {
        var vyearlist = document.getElementById("yearlist");
        var vmakelist = document.getElementById("makelist");

        makehttpObject = getHTTPObject();
        if (makehttpObject != null)
        {
            makehttpObject.open("GET", "ajaxgetmodels.php?Year=" + vyearlist.options[vyearlist.selectedIndex].value + "&Make=" + vmakelist.options[vmakelist.selectedIndex].value, true);
            makehttpObject.send(null);
            makehttpObject.onreadystatechange = modelsReturned;
            document.getElementById('modellist').style.cursor = "wait";
        }
    }

    function modelsReturned()
    {
        if(makehttpObject.readyState == 4)
        {
            var vmodellist = document.getElementById("modellist");

            // Clear the list out...
            while (vmodellist.options.length)  vmodellist.remove(0);

            var vlastmodel = document.getElementById("LastModelUsed");
            var found = 0;

            // Now add the list from the returned results...
            names = makehttpObject.responseText.split(';');
            var index = 0;
            var current = 0;
            while (index < (names.length-1))
            {
                newOpt = document.createElement("option");
                newOpt.value = names[index];
                newOpt.text = names[index];
                if(vlastmodel.value == newOpt.value) found = current;
                vmodellist.add(newOpt);
                index++;
                current++;
            }
            vmodellist.options[found].selected = true;
            vmodellist.style.cursor = "default";
            modelchanged();
        }
    }

    function modelchanged()
    {
        var vyearlist = document.getElementById("yearlist");
        var vmakelist = document.getElementById("makelist");
        var vmodellist = document.getElementById("modellist");

        modelhttpObject = getHTTPObject();
        if (modelhttpObject != null)
        {
            modelhttpObject.open("GET", "ajaxgetstyles.php?Year=" + vyearlist.options[vyearlist.selectedIndex].value + "&Make=" + vmakelist.options[vmakelist.selectedIndex].value + "&Model=" + vmodellist.options[vmodellist.selectedIndex].value, true);
            modelhttpObject.send(null);
            modelhttpObject.onreadystatechange = stylesReturned;
            document.getElementById('stylelist').style.cursor = "wait";
        }
    }

    function stylesReturned()
    {
        if(modelhttpObject.readyState == 4)
        {
            var vstylelist = document.getElementById("stylelist");

            // Clear the list out...
            while (vstylelist.options.length)  vstylelist.remove(0);

            var vlaststyle = document.getElementById("LastStyleUsed");
            var found = 0;

            // Now add the list from the returned results...
            names = modelhttpObject.responseText.split(';');
            var index = 0;
            var current = 0;
            while (index < (names.length-1))
            {
                newOpt = document.createElement("option");
                newOpt.value = names[index+1] + ';' + names[index];
                newOpt.text = names[index];
                if(vlaststyle.value == newOpt.value) found = current;
                vstylelist.add(newOpt);
                index += 2;
                current++;
            }
            vstylelist.options[found].selected = true;
            vstylelist.style.cursor = "default";
            stylechanged();
        }
    }

    function stylechanged()
    {
        //var vyearlist = document.getElementById("yearlist");
        //var vmakelist = document.getElementById("makelist");
        //var vmodellist = document.getElementById("modellist");
        var vstylelist = document.getElementById("stylelist");
        var vlaststyle = document.getElementById("LastStyleUsed");
        vlaststyle.value = vstylelist.options[vstylelist.selectedIndex].value;

        // This is here in case we want to display details of the selected item on this page...
    }

//-->
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">All Virtual Vehicles</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: 0px;">
                <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
                <br/>
                <form action="allvehicleedit.php" method="post">
                    <input type="hidden" name="AddEditType" value="AddNew" />
                    <button type="submit" value="" class="med">Add New Vehicle</button>
					<a href="/add_new_vehicle.php" class="new_med">Add Another New Vehicle</a>
                </form>
				
                <!--form action="allvehicleedit.php" method="post">
                    <input type="hidden" name="AddEditType" value="EditExisting" />
<?php
    if(isset($_SESSION['LastMakeUsed'])) echo '<input type="hidden" id="LastMakeUsed" name="LastMakeUsed" value="'.$_SESSION['LastMakeUsed'].'" />';
    else echo '<input type="hidden" id="LastMakeUsed" name="LastMakeUsed" value="" />';
    if(isset($_SESSION['LastModelUsed'])) echo '<input type="hidden" id="LastModelUsed" name="LastModelUsed" value="'.$_SESSION['LastModelUsed'].'" />';
    else echo '<input type="hidden" id="LastModelUsed" name="LastModelUsed" value="" />';
    if(isset($_SESSION['LastStyleUsed'])) echo '<input type="hidden" id="LastStyleUsed" name="LastStyleUsed" value="'.$_SESSION['LastStyleUsed'].'" />';
    else echo '<input type="hidden" id="LastStyleUsed" name="LastStyleUsed" value="" />';
?>
                    <table class="table">
                        <tr valign="baseline">
                            <td>Year</td>
                            <td ><select  name="yearlist" id="yearlist" onchange="javascript:yearchanged();">
<?php
    $count = count($allyears);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$allyears[$i].'"';
        if(isset($_SESSION['LastYearUsed']) && ($_SESSION['LastYearUsed'] == $allyears[$i])) echo 'selected="selected"';
        echo '>'.$allyears[$i].'</option>';
    }
?>
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Make</td>
                            <td><select  name="makelist" id="makelist" onchange="javascript:makechanged();">
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Model</td>
                            <td><select  name="modellist" id="modellist" onchange="javascript:modelchanged();">
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Style</td>
                            <td><select  name="stylelist" id="stylelist" onchange="javascript:stylechanged();">
                            </select></td>
                        </tr>
                    </table>
                    <button type="submit" value="" class="med">Edit Selected Vehicle</button>
                </form-->
				
<!--- new edit form starts-->				

<form action="vehicle_edit.php" method="post">
                    <input type="hidden" name="AddEditType" value="EditExisting" />
<?php
    if(isset($_SESSION['LastMakeUsed'])) echo '<input type="hidden" id="LastMakeUsed" name="LastMakeUsed" value="'.$_SESSION['LastMakeUsed'].'" />';
    else echo '<input type="hidden" id="LastMakeUsed" name="LastMakeUsed" value="" />';
    if(isset($_SESSION['LastModelUsed'])) echo '<input type="hidden" id="LastModelUsed" name="LastModelUsed" value="'.$_SESSION['LastModelUsed'].'" />';
    else echo '<input type="hidden" id="LastModelUsed" name="LastModelUsed" value="" />';
    if(isset($_SESSION['LastStyleUsed'])) echo '<input type="hidden" id="LastStyleUsed" name="LastStyleUsed" value="'.$_SESSION['LastStyleUsed'].'" />';
    else echo '<input type="hidden" id="LastStyleUsed" name="LastStyleUsed" value="" />';
?>
                    <table class="table">
                        <tr valign="baseline">
                            <td>Year</td>
                            <td ><select  name="yearlist" id="yearlist" onchange="javascript:yearchanged();">
<?php
    $count = count($allyears);
    for($i = 0; $i < $count; $i++)
    {
        echo '<option value="'.$allyears[$i].'"';
        if(isset($_SESSION['LastYearUsed']) && ($_SESSION['LastYearUsed'] == $allyears[$i])) echo 'selected="selected"';
        echo '>'.$allyears[$i].'</option>';
    }
?>
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Make</td>
                            <td><select  name="makelist" id="makelist" onchange="javascript:makechanged();">
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Model</td>
                            <td><select  name="modellist" id="modellist" onchange="javascript:modelchanged();">
                            </select></td>
                        </tr>
                        <tr valign="baseline">
                            <td>Style</td>
                            <td><select  name="stylelist" id="stylelist" onchange="javascript:stylechanged();">
                            </select></td>
                        </tr>
                    </table>
                    <button type="submit" value="" class="med">Edit Selected Vehicle</button>
                </form>
				
<!--- new edit form starts-->				
				
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
