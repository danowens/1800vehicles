<?php include "functions.php";?>
<?php session_start(); ?>
<?php
	if( count($_SESSION['user_meta']) ) {
		$location = 'http://' . $_SERVER['SERVER_NAME'] . '/demovehicles/dashboard.php';
		$controller->redirect( $location );		
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>1800vehicles.com</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="js/jquery-1.9.1.min.js"> </script>
</head>

<body>
	<div class="outer_main">
		<div class="auto_con">
			<div class="specific_outer">
				<div class="logo_main"><img src="images/img_logo.png" alt=""></div>
				<div class="model_main">

				<!--start login page-->
					<div class="login-main">
						<div class="upme-inner">
							<div class="upme-main">
							<?php 
								if( isset( $_POST['sub'] ) ) {
									date_default_timezone_set("Asia/Kolkata"); 
									$arg = $_POST;
									$return_msg = $controller->login($arg);								
									if( !empty($return_msg['msg'] ) ) : ?>
									<span style="float: left; text-align: center; padding: 19px; width: 82%; color:<?php echo $return_msg['color'];?>;"><?php echo $return_msg['msg'];?></span> 
									<?php endif; ?>
									
									<?php
									if( !empty($return_msg['err_count']) && $return_msg['err_count']===4 ) {
										$_SESSION['resume_date'] = date("Y/m/d h:i:sa");
									}
									
									if(!empty($_SESSION['resume_date']) && $_SESSION['resume_date']!=0) {
										$endTime = strtotime("+1 minutes", strtotime($_SESSION['resume_date']));
									} else {
										$endTime =0;
									}
								}
								
								if( $endTime > strtotime(date("Y/m/d h:i:sa") ) ) : ?>
								<div class="upme-field">   
									<span>You have reached the wrong login attempts limit.Plz Come Back after 15 mins!</span> 
								</div>
								<?php else : ?>
								<h1>Sales agent login  </h1>
								<form name="login" method="post" id="login_form">
									<div class="upme-field"><label for="user_login" class="upme-field-type">
										<span>Username</span></label>
										<div class="upme-field-value">
										<input type="text" name="name" class="upme-input user-name" placeholder="Please enter your username" required>
										</div>										
										<div class="upme-clear"></div>
									</div>
									
									<div class="upme-clear"></div>
									
									<div class="upme-field">
										<label for="login_user_pass" class="upme-field-type">
										<span>Password</span></label>
										<div class="upme-field-value">
										<input type="password" name="password" id="login_user_pass" class="upme-input user-pass" placeholder="Please enter your password" required>
										</div>
										<div class="upme-clear"></div>
									</div>									
									
									<div class="upme-field">										
										<label class="upme-field-type">&nbsp;</label>
										<div class="upme-field-value">
											<div class="upme-login-btn02"><input type="submit" value="SUBMIT" class="upme-login-btn" name="sub"></div>
											<br>
										</div>
										<div class="upme-clear"></div>
									</div>
									<div class="upme-clear"></div>
									<p class="message"></p>
								</form>       
								<?php endif; ?>
							</div>
						</div>
					</div>
				<!--end login page-->
				</div>
			</div>
		</div>
	</div>	
	<script src="js/script.js"> </script>
	<script>
	jQuery(document).ready( function($){
		/* $('.upme-login-btn').click( function(){
			
			var $message = $('#login_form .message');
			$message.html('');
			
			$("#login_form input").each(function(){
				
				if( $(this).val() == '') {
					$(this).addClass("error");
					
				} else {
					$(this).removeClass("error");
				}
			});
			
			var i = 0;
			$("#login_form .error").each( function() {
				i++;
			});
			
			if( i > 0 ) {
				
				$message.html('Please fill out highlighted fields!');
				$message.css({
					'text-align' : 'center',
					'padding-bottom' : '15px',
					'color' : 'red'					
				});
				
				return false;
			}
		}); */
	});
</script>
</body>
</html>
