<?php include "functions.php"; ?>

<?php
if( isset( $_POST['load_page'] ) ) {
	
	if( $_POST['load_page']=='Newer') {
		$older = "";
		$newer = "selected";
		
	} else {
		$newer = "";
		$older = "selected";
	}
}

if( $_POST['page'] ) {
	
	$page = $_POST['page'];
	$cur_page = $page;
	$page -= 1;
	$per_page = 10;
	$previous_btn = true;
	$next_btn = true;
	$first_btn = true;
	$last_btn = true;
	$start = $page * $per_page;
	$query_pag_data = "SELECT * from users ORDER BY UserID DESC LIMIT $start, $per_page ";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());

	$msg = "";
	echo ' <ul class="pagination3">';
	$num_1_tab = mysql_num_rows($result_pag_data);
	
	if( $num_1_tab > 0 ) {
		$counter = 0;
		while ( $total_result = mysql_fetch_array( $result_pag_data ) ) {
		
		$counter += 1;
		
		if( ( $counter % 2 ) > 0 ) {
			$stripe_class = 'odd';
		} else {
			$stripe_class = 'even';
		}
		?>
		<!-- light box  -->
		<div class="dash-btm">
			<div class="dash-name <?php echo $stripe_class; ?>  dash-bg"><?php echo $total_result['Firstname']; ?></div>
			<div class="dash-email <?php echo $stripe_class; ?>  dash-bg"><?php echo $total_result['Email']; ?></div>
			<div class="dash-car <?php echo $stripe_class; ?>  dash-bg">Nissan</div>
			<div class="dash-model <?php echo $stripe_class; ?>  dash-bg">2015</div>
			<div class="dash-status <?php echo $stripe_class; ?>  dash-bg"><input type="submit" name="sub" onclick="get(<?php echo $total_result['UserID']; ?>)"  class="dash-status-btn" value="Reply"></div>
		</div>
		
		<div class="clear"></div>

		<div class="model_main model01" id="modal_<?php echo $total_result['UserID'];?>" style="display:none;">
		<div class="model_detail_main subm" id="sendnew">
		<h2>Send New Quote</h2>
		<form action="" method="post" enctype="multipart/form-data" name="new" onsubmit="return checkForm(this);">
			<input type="hidden" name="fname" value="<?php echo $total_result['Firstname']; ?>">
			<input type="hidden" name="email_id" value="<?php echo $total_result['Email']; ?>">
			<input type="hidden" name="user_id" value="<?php echo $total_result['UserID']; ?>">
			<input type="hidden" name="user_name" value="<?php  echo mt_rand(); ?>">
			<input type="hidden" name="user_pass" value="<?php echo mt_rand(); ?>">
			<!--------------------Gallery Tabing----------------------------->

			<div class="send_top_bar">
				<div id="send_brows_bar">
				<span onclick="return tab_gallery(<?php echo $total_result['UserID'];?>);"><input type="button" value="Upload Gallery"/></span>
				<span onclick="return tab_caption(<?php echo $total_result['UserID'];?>);"><input type="button" value="Upload With Caption"/></span>
				</div>

				<div class="model_pic" id="sho_gallery_<?php echo $total_result['UserID'];?>">
				<input id="gallery_main<?php echo $total_result['UserID'];?>" type="file" name="imgs[]" multiple accept="image/*" required>
				</div>
				<div class="model_pic" id="gal_caption_<?php echo $total_result['UserID'];?>" style="display:none;">
				</div>
			</div>

			<!--------------------Gallery Tabing----------------------------->
			<div class="detail_form_con">		
				<div class="detail_form_out">
					<div class="model_name_con">Year</div>
					<div class="model_txt_out">
					<input type="text" name="year" class="model_txtfld" required>
					</div>
				</div>

				<div class="detail_form_out">
					<div class="model_name_con">Make</div>
					<div class="model_txt_out">
					<input type="text" name="make" class="model_txtfld" required>
					</div>
				</div>			
			</div>


			<div class="detail_form_con">		
				<div class="detail_form_out">
					<div class="model_name_con">Model</div>
					<div class="model_txt_out">
					<input type="text" name="modal" class="model_txtfld" required>
					</div>
				</div>
				
				<div class="detail_form_out">
					<div class="model_name_con">Style</div>
					<div class="model_txt_out">
					<input type="text" name="style" class="model_txtfld" required>
					</div>
				</div>			
			</div>

			<div class="detail_form_con">
				<div class="detail_form_out">
					<div class="model_name_con">Mileage</div>
					<div class="model_txt_out">
					<input type="text" name="mileage" class="model_txtfld" required>
					</div>
				</div>
				
				<div class="detail_form_out">
					<div class="model_name_con">Exterior Color</div>
					<div class="model_txt_out">
					<input type="text" name="ex_color" class="model_txtfld" required>
					</div>
				</div>
			</div>


			<div class="detail_form_con">
				<div class="detail_form_out">
					<div class="model_name_con">Interior Color</div>
					<div class="model_txt_out">
					<input type="text" name="in_color" class="model_txtfld" required></div>
				</div>
				
				<div class="detail_form_out">
					<div class="model_name_con">Price</div>
					<div class="model_txt_out">					
					<select class="model_select" name="price_from">	
						<?php 
						$selected= 'selected="selected"';
						for( $i = 1000; $i <= 50000; $i += 100 ) : ?>
						<option value="$<?php echo $i?>" <?php if( $i == 1000 ) { echo $selected; } ?>>$<?php echo $i?></option>
						<?php endfor; ?>
					</select>
					<span class="gray"> to </span>
					<select class="model_select" name="price_to">						
						<?php for( $i = 1000; $i <= 50000; $i += 100 ) : ?>
						<option value="$<?php echo $i?>" <?php if( $i == 1000 ) { echo $selected; } ?> >$<?php echo $i?></option>
						<?php endfor; ?>						
					</select>
					<input type="hidden" value="" name="price">
					</div>
				</div>
			</div>

			<div class="detail_form_con">
				<div class="detail_form_out">
					<div class="model_name_con">V.I.N</div>
					<div class="model_txt_out">
						<input name="vin" type="text" class="model_txtfld" maxlength="17">
						<p class="vin-note">* Must be 17 characters</p>
					</div>
					
				</div>
				
				<div class="detail_form_out">
					<div class="model_name_con">Notes</div>
					<div class="model_txt_out"><textarea name="notes" cols="" rows="" class="model_txtarea" required></textarea></div>
				</div>
			</div>
			
			<div class="details_form_btn">
				<input type="submit" value="Send Quote" name="send" class="wholesale_btn">
			</div>
		</form>

		</div>
		<?php

		//$today = date("H:i:s");  
		//echo "select * from feedback where user_id='".$total_result['UserID']."'";
		$query_getfeed= mysql_query("SELECT t1.* FROM feedback t1 JOIN (SELECT reply_id, MAX(timestamp) timestamp FROM feedback where user_id='".$total_result['UserID']."' GROUP BY reply_id)  t2 ON t1.reply_id = t2.reply_id AND t1.timestamp = t2.timestamp; ");
		$count= mysql_num_rows($query_getfeed);

		if($count > 0)
		{
		?>
		<!--<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>-->
		<script>
		function additional( rep_id ) {			
			
			$("#addd_"+rep_id).slideToggle("slow", function() {
				$(".subm").slideUp("slow");
				$(".re-sub").slideUp("slow");			
			}).addClass('opened');
			
		}
		</script>
		<script>
		function resubmit( quote_id ) {
			$("#re"+quote_id).slideToggle("slow");
			//$(".re-sub").slideToggle("slow");
			$('.quote_id_field').val(quote_id);
			$(".subm").slideUp("slow");
			$(".additional_main").slideUp("slow");
			//$(".subm").hide();
		}
		</script>

		<div class="feedback" >
		<table cellspacing="0" border="0" cellpadding="0">
		<tr class="intrest-main">
			<th>No.</th>
			<th>Intrest</th> 
			<th>Feedback</th>
			<th>Make</th>  
			<th>Resend</th>
			<th>Completed Quote</th> 
		</tr>
		<?php 
		$i = 1;
		while( $fields=mysql_fetch_array( $query_getfeed ) ) { ?>

		<tr class="feed-back">
		<td><?php echo $i; ?></td>
		<td><?php  if($fields['intrest']==""){ echo "yes"; } else { echo $fields['intrest']; } ?></td> 
		<td><?php echo $fields['feedback']; ?></td>
		<td><?php echo $fields['make']; ?></td>
		<td><input type="button" value="Resend" class="dash-status-btn" onclick="resubmit(<?php echo $fields['reply_id']; ?>)"></td>
		<td><?php if($fields['acceptfull_quote']!=''){ echo "Approved Please get in touch with user."; } else { ?>
		<input type="button" name="aa" value="Show" class="dash-status-btn" onclick="additional(<?php echo $fields['reply_id']; ?>)"><?php } ?></td>
		</tr>
		<?php $i++;?> 

		<!--re submit form--> 
		<tr>
		<td colspan="6">
		<div class="model_detail_main re-sub" id="re<?php echo $fields['reply_id']; ?>"  style="display:none;">
		<form method="post" action="" enctype="multipart/form-data" onsubmit="return checkForm(this);">
		<h2>Resend Quote Below</h2>

		<input type="hidden" class="quote_id_field" name="quote_id_field">
		<input type="hidden" name="fname" value="<?php echo $total_result['Firstname']; ?>">
		<input type="hidden" name="email_id" value="<?php echo $total_result['Email']; ?>">
		<input type="hidden" name="user_id" value="<?php echo $total_result['UserID']; ?>">
		<input type="hidden" name="user_name" value="<?php  echo mt_rand(); ?>">
		<input type="hidden" name="user_pass" value="<?php echo mt_rand(); ?>">


		<div class="test resend_gallery_tab">			
			<a class="uploads upload1" title="Upload Gallery">Upload Gallery</a>				
			<input class="resend_up_btn" id="gallery_main<?php echo $fields['reply_id']; ?>" type="file" name="resend_imgss[]" multiple accept="image/*"></li>
			
			<a class="uploads" title="Upload Gallery With Caption" onclick="multi_resend(<?php echo $fields['reply_id']; ?>)" class="resend_upl_btn">Upload Gallery with caption</a>
			<div id="yoyo<?php echo $fields['reply_id']; ?>" style="display:none;"></div>
			
		</div>
		
		<?php
		$autofield_resend = mysql_query("select * from resend_quote where user_id='".$total_result['UserID']."' and quote_id='".$fields['reply_id']."' order by id desc");
		$autofield_first = mysql_query("select * from reply where user_id='".$total_result['UserID']."' and id='".$fields['reply_id']."' order by id desc ");
		$autofield_imgs = mysql_query("select * from reply_vehicle where reply_id='".$fields['reply_id']."'");
		$count_resend = mysql_num_rows($autofield_resend);
		
		if($count_resend > 0) {
			$autofill=(mysql_fetch_array($autofield_resend));
		
		} else {
			$autofill=(mysql_fetch_array($autofield_first));
		}
		?>
		<div class="filled_img_outer">
		<?php while($autofill_img = mysql_fetch_array($autofield_imgs)) {
		?>

		<div class="filled_img"><img width="100px" src="vehicles/<?php echo $autofill_img['images']; ?>"></div>
		<?php } ?>
		</div>		

		<div class="detail_form_con">
			<div class="detail_form_out">
				<div class="model_name_con">Year</div>
				<div class="model_txt_out">
					<input type="text" name="year" class="model_txtfld" value="<?php echo $autofill['Year']; ?>">
				</div>
			</div>
			
			<div class="detail_form_out">
				<div class="model_name_con">Make</div>
				<div class="model_txt_out">
					<input type="text" name="make" class="model_txtfld" value="<?php echo $autofill['Make']; ?>">
				</div>
			</div>
		</div>

		<div class="detail_form_con">
			<div class="detail_form_out">
				<div class="model_name_con">Model</div>
				<div class="model_txt_out">
					<input type="text" name="modal" class="model_txtfld" value="<?php echo $autofill['Model']; ?>">
				</div>
			</div>
			
			<div class="detail_form_out">
				<div class="model_name_con">Style</div>
				<div class="model_txt_out">
					<input type="text" name="style" class="model_txtfld" value="<?php echo $autofill['Style']; ?>">
				</div>
			</div>
		</div>

		<div class="detail_form_con">
			<div class="detail_form_out">
				<div class="model_name_con">Mileage</div>
				<div class="model_txt_out">
					<input type="text" name="mileage" class="model_txtfld" value="<?php echo $autofill['Mileage']; ?>">
				</div>
			</div>
			
			<div class="detail_form_out">
				<div class="model_name_con">Exterior Color</div>
				<div class="model_txt_out">
					<input type="text" name="ex_color" class="model_txtfld" value="<?php echo $autofill['ex_color']; ?>">
				</div>
			</div>
		</div>


		<div class="detail_form_con">
			<div class="detail_form_out">
				<div class="model_name_con">Interior Color</div>
				<div class="model_txt_out">
				<input type="text" name="in_color" class="model_txtfld" value="<?php echo $autofill['in_color']; ?>"></div>
			</div>
			
			<div class="detail_form_out">
				<div class="model_name_con">Price</div>
				<div class="model_txt_out">
					<input type="text" name="price" class="model_txtfld" value="<?php echo $autofill['Price']; ?>">
				</div>
			</div>
		</div>

		<div class="detail_form_con">
			<div class="detail_form_out">
				<div class="model_name_con">V.I.N</div>
				<div class="model_txt_out"><input name="vin" type="text" class="model_txtfld" value="<?php echo $autofill['vin']; ?>"></div>
			</div>
			
			<div class="detail_form_out">
				<div class="model_name_con">Notes</div>
				<div class="model_txt_out"><textarea name="notes" cols="" rows="" class="model_txtarea" required><?php echo $autofill['notes']; ?></textarea></div>
			</div>		
		</div>
		
		<div class="details_form_btn">
			<input type="submit" value="Resend Quote" name="resend" class="wholesale_btn">
		</div>
		
		</form>
		</div>
		</td>
		</tr>


		<!-----------Additional Detail Start------------------->
		<tr>
		<td colspan="6">
		<form method="post" action="" enctype="multipart/form-data" onsubmit="return checkaddForm(this);">
		<div class="additional_main" id="addd_<?php echo $fields['reply_id']; ?>" style="display:none;">

		<input type="hidden" name="mail_id" value="<?php echo $total_result['Email']; ?>">
		<input type="hidden" name="userr_id" value="<?php echo $total_result['UserID']; ?>">
		<input type="hidden"  value="<?php echo $fields['reply_id']; ?>"  name="additional_rplyid">
		<div  class="model_detail_main">
		
		<!--- gallery ---->
		<div class="resend_gallery_tab">		
			<span class="uploads"><a class="upload1" title="Upload Gallery">Upload Gallery</a></span>
			<span class="uploads"><a title="Upload Gallery With Caption" onclick="multi_last(<?php echo $fields['reply_id']; ?>)">Upload Gallery With Caption</a></span>
			<p class="upload1" style="margin-top: 25px;"><input id="lastt<?php echo $fields['reply_id']; ?>" type="file"  accept="image/*" multiple="" name="resend_imgs[]"></p>
			<div class="lc" id="lastcaption_<?php echo $fields['reply_id']; ?>"></div>		
		</div>
		<!-----gallery------->

		<div class="filled_img_outer">
		<?php
		
		$reply_id = $fields['reply_id'];
		
		$autofield_resend_last=mysql_query("select * from complete_quote where quote_id='".$fields['reply_id']."' order by id desc");

		$autofield_center_last=mysql_query("select * from resend_quote where user_id='".$total_result['UserID']."' and quote_id='".$fields['reply_id']."' order by id desc ");

		$autofield_first_last=mysql_query("select * from reply where user_id='".$total_result['UserID']."' and id='".$fields['reply_id']."' order by id desc ");

		$count_resend_last=mysql_num_rows($autofield_resend_last);
		$count_center_last=mysql_num_rows($autofield_center_last);
		
		if($count_resend_last > 0) {
			$autofill_last=(mysql_fetch_array($autofield_resend_last));
		
		} else if($count_center_last > 0) {
			$autofill_last=(mysql_fetch_array($autofield_center_last));
		
		} else {
			$autofill_last=(mysql_fetch_array($autofield_first_last));
		}


		$autofield_imgages_first=mysql_query("select * from reply_vehicle where reply_id='".$fields['reply_id']."'");

		$autofield_imgages_last=mysql_query("select * from complete_quote_images where quote_id='".$fields['reply_id']."'");

		$count_fir=mysql_num_rows($autofield_imgages_last);
		
		if( $count_fir > 0) {
			while($show_last_imgss=mysql_fetch_array($autofield_imgages_last)) {
				echo '<div class="filled_img"><img width="100px" src="vehicles/'.$show_last_imgss['images'].'"></div>';
			}
		} else {

			while($show_last_imgs=mysql_fetch_array($autofield_imgages_first)) {
				echo '<div class="filled_img"><img width="100px" src="vehicles/'.$show_last_imgs['images'].'"></div>';
			}
		}
		?>
		</div>

		<div class="detail_form_con">
		<div class="detail_form_out">
		<div class="model_name_con">Year</div>
		<div class="model_txt_out">
		<input type="text" name="year" class="model_txtfld" value="<?php echo $autofill_last['Year']; ?>" required>
		</div>
		</div>
		<div class="detail_form_out">
		<div class="model_name_con">Make</div>
		<div class="model_txt_out">
		<input type="text" name="make" class="model_txtfld" value="<?php echo $autofill_last['Make']; ?>" required>
		</div>
		</div>
		</div>

		<div class="detail_form_con">
		<div class="detail_form_out">
		<div class="model_name_con">Model</div>
		<div class="model_txt_out">
		<input type="text" name="modal" class="model_txtfld" value="<?php echo $autofill_last['Model']; ?>" required>
		</div>
		</div>
		<div class="detail_form_out">
		<div class="model_name_con">Style</div>
		<div class="model_txt_out">
		<input type="text" name="style" class="model_txtfld" value="<?php echo $autofill_last['Style']; ?>" required>
		</div>
		</div>
		</div>


		<div class="detail_form_con">
		<div class="detail_form_out">
		<div class="model_name_con">Mileage</div>
		<div class="model_txt_out">
		<input type="text" name="mileage" class="model_txtfld" value="<?php echo $autofill_last['Mileage']; ?>" required>
		</div>
		</div>
		<div class="detail_form_out">
		<div class="model_name_con">Exterior Color</div>
		<div class="model_txt_out">
		<input type="text" name="ex_color" class="model_txtfld" value="<?php echo $autofill_last['ex_color']; ?>" required>
		</div>
		</div>
		</div>


		<div class="detail_form_con">
		<div class="detail_form_out">
		<div class="model_name_con">Interior Color</div>
		<div class="model_txt_out">
		<input type="text" name="in_color" class="model_txtfld" value="<?php echo $autofill_last['in_color']; ?>" required></div>
		</div>
		<div class="detail_form_out">
		<div class="model_name_con">Price</div>
		<div class="model_txt_out">
		<input type="text" name="price" class="model_txtfld" value="<?php echo $autofill_last['Price']; ?>" required>
		</div>
		</div>
		</div>


		<div class="detail_form_con">
		<div class="detail_form_out">
		<div class="model_name_con">V.I.N</div>
		<div class="model_txt_out"><input name="vin" type="text" class="model_txtfld" value="<?php echo $autofill_last['vin']; ?>" required></div>
		</div>
		<div class="detail_form_out">
		<div class="model_name_con">Notes</div>
		<div class="model_txt_out"><textarea name="notes" cols="" rows="" class="model_txtarea" required><?php echo $autofill_last['notes']; ?></textarea></div>
		</div>
		</div>
		</div>
		<div class="detail_form_con">
			
			<div class="additional_innr text-center">
				<h3>ADDITIONAL DETAILS:</h3>
				<!---------Equipment  checkboxes --------->
				<?php				
				
				
				/* INTERIOR EQUIPMENTS */
				$equip_list = mysql_query("select * from equipment where categories='INTERIOR'"); 
				?>
				<h5>INTERIOR items:</h5>
				<?php //echo $reply_id; ?>
				<div class='interior_equip'>				
				<?php while( $show_list = mysql_fetch_array( $equip_list ) ) : ?>
					
					<?php					
						$query = "select equipment from additional_detail where reply_id=" . $reply_id . " AND equipment='" . $show_list['name'] . "'";
						$item = mysql_query($query);
						
						$add_check = "";
						if( $item ) {
							$num_rows = mysql_num_rows( $item );
							if( $num_rows > 0 ) {
								$add_check = "checked=checked";
							}
						}
					?>					
					<div class="add_check">
						<input name="equipment[]" type="checkbox" value="<?php echo $show_list['name']; ?>" class="check" <?php echo $add_check; ?>>
						<span><?php echo $show_list['name']; ?></span>
					</div>
				<?php endwhile; ?>
				</div>
				
				<?php
				/* LUXURY EQUIPMENTS */
				$equip_list = mysql_query("select * from equipment where categories='LUXURY'");
				echo "<h5>LUXURY items:</h5><div class='interior_equip'>"; ?>
				
				<?php while( $show_list = mysql_fetch_array( $equip_list ) ) : ?>					
					<?php					
						$query = "select equipment from additional_detail where reply_id=" . $reply_id . " AND equipment='" . $show_list['name'] . "'";
						$item = mysql_query($query);
						
						$add_check = "";
						if( $item ) {
							$num_rows = mysql_num_rows( $item );
							if( $num_rows > 0 ) {
								$add_check = "checked=checked";
							}
						}
					?>					
					<div class="add_check">
						<input name="equipment[]" type="checkbox" value="<?php echo $show_list['name']; ?>" class="check" <?php echo $add_check; ?>>
						<span><?php echo $show_list['name']; ?></span>
					</div>
				<?php endwhile; ?>
				</div>

				<?php
				/* SOUND EQUIPMENTS */
				$equip_list=mysql_query("select * from equipment where categories='SOUND'");
				echo "<h5>SOUND SYSTEM items:</h5><div class='interior_equip'>"; ?>
				
				<?php while( $show_list = mysql_fetch_array( $equip_list ) ) : ?>					
					<?php					
						$query = "select equipment from additional_detail where reply_id=" . $reply_id . " AND equipment='" . $show_list['name'] . "'";
						$item = mysql_query($query);
						
						$add_check = "";
						if( $item ) {
							$num_rows = mysql_num_rows( $item );
							if( $num_rows > 0 ) {
								$add_check = "checked=checked";
							}
						}
					?>					
					<div class="add_check">
						<input name="equipment[]" type="checkbox" value="<?php echo $show_list['name']; ?>" class="check" <?php echo $add_check; ?>>
						<span><?php echo $show_list['name']; ?></span>
					</div>
				<?php endwhile; ?>
				</div>

				<?php

				/* BASIC EQUIPMENTS */
				$equip_list=mysql_query("select * from equipment where categories='BASIC'");
				echo "<h5>BASIC items:</h5><div class='interior_equip'>
				";
				
				?>
				
				<?php while( $show_list = mysql_fetch_array( $equip_list ) ) : ?>					
					<?php					
						$query = "select equipment from additional_detail where reply_id=" . $reply_id . " AND equipment='" . $show_list['name'] . "'";
						$item = mysql_query($query);
						
						$add_check = "";
						if( $item ) {
							$num_rows = mysql_num_rows( $item );
							if( $num_rows > 0 ) {
								$add_check = "checked=checked";
							}
						}
					?>					
					<div class="add_check">
						<input name="equipment[]" type="checkbox" value="<?php echo $show_list['name']; ?>" class="check" <?php echo $add_check; ?>>
						<span><?php echo $show_list['name']; ?></span>
					</div>
				<?php endwhile; ?>
				</div>

				<?php

				/* OTHER EQUIPMENTS */
				$equip_list=mysql_query("select * from equipment where categories='OTHER'");
				echo "<h5>OTHER items:</h5><div class='interior_equip'>";
				
				?>
				
				<?php while( $show_list = mysql_fetch_array( $equip_list ) ) : ?>					
					<?php					
						$query = "select equipment from additional_detail where reply_id=" . $reply_id . " AND equipment='" . $show_list['name'] . "'";
						$item = mysql_query($query);
						
						$add_check = "";
						if( $item ) {
							$num_rows = mysql_num_rows( $item );
							if( $num_rows > 0 ) {
								$add_check = "checked=checked";
							}
						}
					?>					
					<div class="add_check">
						<input name="equipment[]" type="checkbox" value="<?php echo $show_list['name']; ?>" class="check" <?php echo $add_check; ?>>
						<span><?php echo $show_list['name']; ?></span>
					</div>
				<?php endwhile; ?>
				</div>
			</div>
		</div>

		<div class="detail_form_con">
			<div class="messages_con"><a href="#">Messages</a></div>
			<div class="messages_inn" style="">
				<?php
				/*$massage=mysql_query("select DISTINCT(feedback.feedback),resend_quote.notes from feedback LEFT JOIN resend_quote on feedback.reply_id=resend_quote.quote_id where feedback.reply_id='1' AND resend_quote.quote_id='1'");*/
				$mass_id = $fields['reply_id'];
				$first_reply = mysql_query("select * from reply where id='$mass_id'");
				$massage1 = mysql_query("select * from feedback where reply_id='$mass_id' order by timestamp");
				$massage2 = mysql_query("select * from resend_quote where quote_id='$mass_id' order by timestamp");
				
				//select new agreement
				$agreement = mysql_query("select agreement_notes from term_conditions where reply_id='$mass_id'");
				
				$show_first = mysql_fetch_array($first_reply);
				echo "<p><span>Dan (salesperson):</span>".$show_first['notes']." </p>";


				while( $show_massage=mysql_fetch_array($massage1) and $show_massage2 = mysql_fetch_array( $massage2 ) ) {
					echo "<p><span>" . $total_result['Firstname'] . " (client ):</span>" . $show_massage['feedback'] . " </p>";
					echo "<p><span>Dan (salesperson):</span>" . $show_massage2['notes'] . " </p>";
				}
				?>
			</div>
			<div class="specific_out">
			<h4><input type="checkbox" name="general" value="yes">Specific Vehicle Purchase Order</h4>
			<h5>IN GENERAL</h5>
			<p>The vehicle delivered will be in excellent mechanical and cosmetic condition with no frame damage, flood damage or salvaged title. The vehicle will have all standard equipment, such as floor mats, owner's manual, CD cartridge, wiper blades, spare tire, jack, etc., which will be in new or like new condition. Seller will provide a factory key and remote (if applicable), and will reimburse Purchaser up to $75 towards a second key and remote (if applicable). Seller will also reimburse Purchaser up to $75 towards any navigation disc that may be needed. The vehicle will have smooth riding tires with greater than 40% tread life remaining or Seller and Purchaser will split the cost of new tires. The vehicle will be professionally detailed, including door ding removal and touch up painting of small scratches (to the best of professional ability).</p>
			</div>
			<div class="specific_out">
			<h5>SERVICE AND REPAIRS</h5>
			<p>Purchaser shall choose a mechanic to inspect the vehicle once it has been delivered. Seller shall pay up to $400 towards any service or repair needs recommended by this mechanic. Purchaser agrees to contribute up to $300 in addition to Seller's $400 (if necessary), but any amount above a total bill of $700 will be the responsibility of the Seller. If Seller chooses not to pay an amount over $700, Purchaser is not obligated to purchase that vehicle (deposit refunded). If any inspection fee is charged by this mechanic, it will be paid for by the Purchaser. Seller reserves the right to request a second opinion (by another mechanic or specialist of Purchaser's choosing) and may choose the lesser of the two estimates as the true mechanical need. This inspection would be paid for by the Seller. In addition, Seller agrees that if the first mechanic believes that the vehicle has an issue (such as undercarriage rust or poor service history) that would significantly increase the cost of future repairs (and the second mechanic or specialist holds the same belief), Purchaser is not obligated to purchase that vehicle (deposit refunded).</p>
			</div>
			<div class="specific_out">
			<h5>COSMETIC</h5>
			<p>If Purchaser believes that the vehicle delivered has cosmetic wear and tear in excess of normal use (for vehicles of similar year, make, model and mileage), Seller will take corrective measures satisfactory to Purchaser or release Purchaser from any obligation for the purchase of that vehicle. Under such circumstances, Purchaser will allow Seller a reasonable opportunity to make such corrections.</p>
			</div>
			<div class="specific_out">
			<h5>ADDITIONAL CO-PAYS</h5>
			<p>Some vehicles required additional deposit amounts, or higher co-pays for second keys, navigation discs, repair limits, etc. If any additional amounts or limits are applicable to this vehicle, it must be noted on the Specific Vehicle Preview listing above. </p>
			</div>
			<div class="specific_out">
			<h5>FINAL DETAILS </h5>
			<p>Purchaser understands that this vehicle is being purchased by Seller specifically for him or her and that Seller has no desire or intention to inventory this vehicle for the purpose of resale. Purchaser understands that this vehicle may or may not have service records. In addition, Purchaser understands that if he or she does not complete the purchase of the vehicle within three business days after delivery (for reasons other than those provided in this agreement), he or she will forfeit the deposit and could be billed for shipping expenses and transaction fees incurred by Seller. If the deposit has not been received by Seller at the time of such an event, Purchaser may also be billed for the deposit amount by Seller. Seller shall be allowed up to two weeks (after the vehicle has been purchased) to make it ready for inspection by Purchaser. </p>
			</div>
			<div class="specific_out">
			<h5>AGREEMENT NOTES  </h5>
			<?php $show_agreement_notes = mysql_fetch_array( $agreement ); ?>		
			<?php 
				$result_agreement = "";
				if( $show_agreement_notes ) {
					$result_agreement = $show_agreement_notes['agreement_notes'];				
				}
			?>		
			<p><textarea name="agreement_notes" rows="5" cols="50"><?php echo $result_agreement; ?></textarea></p>
			</div>
			<div class="wholesale_btn2"><input type="submit" name="additional_info" value="Send Complete Quote" id="add_id"></div>
			</div>
		</div>
		</form>
		</td>
		</tr>

		<?php } ?>
		</table></div>
		<?php } ?>
		</div>	
		<?php 
		}
		echo '</ul>';
		
	} else { ?>
	<div class="acc_feed_tab_out" style="border-bottom:1px solid #cdcdcd; background: none;">
	<div class="acc_feed_tab_top">
	<div class="acc_feed_open_out">
	<div class="acc_feed_open_lft no_task_fnd you-have">You  haven't  posted  any  Tasks  Yet</div>
	<a href="<?php echo site_url;?>choose_category.php" target="_blank" class="acc_feed_open_dolr post_new_task browse-btn">Post  Task</a>
	</div></div></div>
	<?php } ?>

	<?php
		//$msg = "<div class='data'><ul>" . $msg . "</ul></div>"; // Content for Data


		/* --------------------------------------------- */
		$query_pag_num     = "SELECT COUNT(*) AS count FROM users $total_task_user";
		$result_pag_num    = mysql_query($query_pag_num);
		$row               = mysql_fetch_array($result_pag_num);
		$count             = $row['count'];
		$no_of_paginations = ceil($count / $per_page);

		/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
		if ($cur_page >= 7) {
			$start_loop = $cur_page - 3;
			if ($no_of_paginations > $cur_page + 3)
				$end_loop = $cur_page + 3;
			else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
				$start_loop = $no_of_paginations - 6;
				$end_loop   = $no_of_paginations;
			} else {
				$end_loop = $no_of_paginations;
			}
		} else {
			$start_loop = 1;
			if ($no_of_paginations > 7)
				$end_loop = 7;
			else
				$end_loop = $no_of_paginations;
		}
		/* ----------------------------------------------------------------------------------------------------------- */
		$msg .= '<div class="showing_page" style="text-align: center;margin-top: 14px;">Showing page '. $cur_page .' of ' . $no_of_paginations . '</div>';
		
		$msg .= "<div class='pagination'><ul>";

		// FOR ENABLING THE FIRST BUTTON
		if ($first_btn && $cur_page > 1) {
			//  $msg .= "<li p='1' class='active'>First</li>";
		} else if ($first_btn) {
			// $msg .= "<li p='1' class='inactive'>First</li>";
		}

		// FOR ENABLING THE PREVIOUS BUTTON
		if ($count > 10) {
			if ($cur_page > 1) {
				if ($previous_btn && $cur_page > 1) {
					
					$pre = $cur_page - 1;
					$msg .= "<li p='$pre' class='active " . $newer . "' id='Newer'><a>&laquo; Newer</a></li>";
					
				} else if ($previous_btn) {
					$msg .= "<li class='inactive " . $newer . "' id='Newer'><a>&laquo; Newer</a></li>";
				}
			}
		}

		for ($i = $start_loop; $i <= $end_loop; $i++) {
		}

		// TO ENABLE THE NEXT BUTTON
		if ($count > 10) {
			if ($next_btn && $cur_page < $no_of_paginations) {
				$nex = $cur_page + 1;
				$msg .= "<li p='$nex' class='active " . $older . "'  id='Older'><a>Older &raquo;</a></li>";
			} else if ($next_btn) {
				$msg .= "<li class='inactive " . $older . "' id='Older'><a>Older &raquo;</a></li>";
			}
		}
		// TO ENABLE THE END BUTTON
		if ($last_btn && $cur_page < $no_of_paginations) {
			
		} else if ($last_btn) {
			
		}
		
		$msg .= "</div></ul>";
		echo $msg;
	}
?>
<div class="webdiv" style="display: none;">
	<pre>
	<?php
		//print_r( $autofield_resend );		
	?>	
	</pre>
</div>
<script>	
	$(document).on('click', "a.remove_project_file", function() {
		$(this).parents('.ww').remove();		       
	});	

	function checkForm( form ) {
		// validation fails if the input is blank
		if(form.vin.value == "") {
			alert("V.I.N should not be empty");
			form.vin.focus();
			return false;
		}

		if( form.vin.value.length != 17 ) {
			alert("V.I.N No. Should be of  17 characters");
			form.vin.focus();
			return false;
		}

		// regular expression to match only alphanumeric characters and spaces
		var re = /^[\w ]+$/;

		// validation fails if the input doesn't match our regular expression
		if( !re.test( form.vin.value ) ) {
			alert("Please Enter Just Alphanumeric Value");
			form.vin.focus();
			return false;
		}
		
		return true;
	}

	function checkaddForm( form ) {
		if ( $("input.check:checked").length > 0 ) {
			return true;		
		} else { 
			alert("Atleast one equipment should be selected");
			return false; 
		}
	}

	function get(id) {
		$('#modal_'+id).slideToggle("slow");
		$('.subm').show();
		$(".re-sub").hide();
		$(".additional_main").hide();
	}

	////////////////////////// Answer Tab
	function tab_gallery( id ) {
		$('.ww').remove();	
		$('#gal_caption_'+id).hide();
		$('#sho_gallery_'+id).show();
		$('#sho_gallery_'+id).show();
		document.getElementById("gallery_main"+id).required = true;  
	}

	function tab_caption( id ) {
		$('#gal_caption_'+id).append('<div class="ww"><input id="gallery_caption_1" class="gallery_caption" type="file" name="image[]" required/><input type="text" class="caption_fld" name="image_caption[]" placeholder="First Image Caption"/><a  class="remove_project_file" border="2"><img src="del.png" /></a></div>');
		$('#sho_gallery_'+id).hide();
		$('#gallery_main'+id).attr({ value: '' });	
		$('#gallery_main'+id).removeAttr('required');
		$('#gal_caption_'+id).show();
	}
	
	jQuery(document).ready( function() {
		//Concatenate prices
		function get_price_string() {		
			var price_from = $('select[name=price_from]').val();
			var price_to = $('select[name=price_to]').val();
			var price_string = price_from + ' to ' + price_to;
			
			$('input[name=price]').val(price_string);		
		}	
		
		$('select[name=price_from]').change( function() {
			get_price_string();
		});
		
		$('select[name=price_to]').change( function() {
			get_price_string();
		});		
		
		get_price_string();
		
		//First upload
		$('a.upload1').click( function() {		
			$('p.upload1').show();
			$('.lc').html('');
		});		
		
		//Disabled submit button on send
		$('input[name="additional_info"]').click( function() {
			//$(this).attr('disabled', 'disabled');
			//$(this).val('Processing... Please wait...');
		});
		
	});
	
</script>