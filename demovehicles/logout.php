<?php include "functions.php"; ?>
<?php
session_start();
session_destroy();

if( isset( $_GET['ref'] ) ) {	
	$controller->redirect('/demovehicles/user.php');
	
} else {
	$controller->redirect('/demovehicles/');
}
?>