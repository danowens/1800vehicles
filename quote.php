<?php require("globals.php"); ?>
<?php
    if(isset($_REQUEST['QuoteType'])) {
        $quotetype = $_REQUEST['QuoteType'];
        unset($_REQUEST['QuoteType']);
    }
    else $quotetype = 'Standard';

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];

    $errorinsave = 'false';

    // Allows for changes to the drop downs without re-reading the current record...
    if(isset($_REQUEST['QuoteID']))
    {
        $editquoteID = $_REQUEST['QuoteID'];
        $pagerefresh = 'true';
        unset($_REQUEST['QuoteID']);
    }

    // See if we are editing rather than starting over...
    if(isset($_REQUEST['EditQuoteID']))
    {
        $editquoteID = $_REQUEST['EditQuoteID'];
        unset($_REQUEST['EditQuoteID']);
    }

    if(isset($editquoteID) && ($editquoteID != -1) && !isset($pagerefresh))
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $query = "select QuoteType, Year, Make, Model, Style, MileageCeiling, EngineCylinders, Transmission,
                CabType, FrontSeatType, BedType, SeatMaterial, WheelDriveType, WheelCovers, RadioNeeded, RadioFM, RadioCassette, RadioCD, PowerDoors, PowerWindows,
                PowerSeats, HeatedSeats, AirConditioning, RemoteEntry, TractionControl, SecuritySystem, CruiseControl, Navigation, RearSlidingWindow, BedLiner, TowPackage,
                ColorCombination, SpecialRequests, Pickup, DeliverToCity, DeliverToState, LuggageRack, SunRoof, VehicleID from quoterequests where QuoteRequestID = ".$editquoteID;
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                $iquoteid = $editquoteID;
                $quotetype = $row['QuoteType'];
                $iyear = $row['Year'];
                $imake = $row['Make'];
                $imodel = $row['Model'];
                $istyle = $row['Style'];
                $imileage = $row['MileageCeiling'];
                $iseattype = $row['SeatMaterial'];
                $iengine = $row['EngineCylinders'];
                $idrive = $row['WheelDriveType'];
                $itrans = $row['Transmission'];
                $iwheels = $row['WheelCovers'];
                $icab = $row['CabType'];
                $ifront = $row['FrontSeatType'];
                $ibedtype = $row['BedType'];
                $iradflex = $row['RadioNeeded'];
                $iradfm = $row['RadioFM'];
                $iradcas = $row['RadioCassette'];
                $iradcd = $row['RadioCD'];
                $ipdoor = $row['PowerDoors'];
                $ipwin = $row['PowerWindows'];
                $ipseat = $row['PowerSeats'];
                $iheat = $row['HeatedSeats'];
                $iair = $row['AirConditioning'];
                $iremote = $row['RemoteEntry'];
                $itraction = $row['TractionControl'];
                $isecure = $row['SecuritySystem'];
                $icruise = $row['CruiseControl'];
                $inavsys = $row['Navigation'];
                $irearwin = $row['RearSlidingWindow'];
                $ibed = $row['BedLiner'];
                $itow = $row['TowPackage'];
                $icolors = $row['ColorCombination'];
                $inotes = $row['SpecialRequests'];
                $idelorpick = $row['Pickup'];
                $idelcity = $row['DeliverToCity'];
                $idelstate = $row['DeliverToState'];
                $iluggage = $row['LuggageRack'];
                $isunroof = $row['SunRoof'];
                $ivehid = $row['VehicleID'];

                if($quotetype == 'Standard')
                {
                    if(!is_null($ivehid)) $result = mysql_query("select ImageFile from vehicles where VehicleID = ".$ivehid);
                    else $result = mysql_query("select v.ImageFile from vehicles v, makes m where m.makeid=v.makeid and m.name='".$imake."' and v.year=".$iyear." and v.model='".$imodel."' and v.style='".$istyle."'");
                    if($result && $row = mysql_fetch_array($result))
                    {
                        $imagefile = $row[0];
                    }
                }
            }

            mysql_close($con);
        }
        else $errorinsave = 'Could not connect to the database';
    }
    elseif(isset($pagerefresh))
    {
        // See what was passed in...
        //Unused $iquotetype = $_REQUEST['QuoteType'];
        $iquoteid = isset($_REQUEST['QuoteID']) ? $_REQUEST['QuoteID'] : null;
        $iyear = $_REQUEST['Year'];
        $imake = $_REQUEST['Make'];
        $imodel = $_REQUEST['Model'];
        $istyle = $_REQUEST['Style'];
        $imileage = $_REQUEST['selectmileage'];
        $iseattype = $_REQUEST['selectseat'];
        $iengine = $_REQUEST['selectengine'];
        $idrive = $_REQUEST['selectdrive'];
        $itrans = $_REQUEST['selecttrans'];
        $iwheels = $_REQUEST['selectwheels'];
        $icab = isset($_REQUEST['selectcab']) ? $_REQUEST['selectcab'] : null;
        $ifront = isset($_REQUEST['selectfrontseat']) ? $_REQUEST['selectfrontseat'] : null;
        $ibedtype = isset($_REQUEST['selectbedtype']) ? $_REQUEST['selectbedtype'] : null;
        $isunroof = $_REQUEST['selectsunroof'];
        $iradflex = isset($_REQUEST['RadFlex']) ? $_REQUEST['RadFlex'] : null;
        if(isset($_REQUEST['RadFM'])) $iradfm = 1;
        else $iradfm = 0;
        if(isset($_REQUEST['RadCas'])) $iradcas = 1;
        else $iradcas = 0;
        if(isset($_REQUEST['RadCD'])) $iradcd = 1;
        else $iradcd = 0;
        $ipdoor = $_REQUEST['PowerDoorLocks'];
        $ipwin = $_REQUEST['PowerWindows'];
        $ipseat = $_REQUEST['PowerSeats'];
        $iheat = $_REQUEST['HeatedSeats'];
        $iair = $_REQUEST['AirConditioning'];
        $iremote = $_REQUEST['RemoteEntry'];
        $itraction = $_REQUEST['TractionControl'];
        $isecure = $_REQUEST['SecuritySystem'];
        $icruise = $_REQUEST['CruiseControl'];
        $inavsys = $_REQUEST['Navigation'];
        $irearwin = isset($_REQUEST['RearSlidingWindow']) ? $_REQUEST['RearSlidingWindow'] : null;
        $ibed = isset($_REQUEST['BedLiner']) ? $_REQUEST['BedLiner'] : null;
        $itow = isset($_REQUEST['TowPackage']) ? $_REQUEST['TowPackage'] : null;
        $iluggage = $_REQUEST['LuggageRack'];
        $icolors = $_REQUEST['colorcombo'];
        $inotes = $_REQUEST['notes'];
        $idelorpick = $_REQUEST['DeliveryPickup'];
        if($idelorpick == 'Deliver') $idelorpick = 0;
        else $idelorpick = 1;
        $idelcity = $_REQUEST['DeliveryCity'];
        $idelstate = $_REQUEST['DeliveryState'];
    }

    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 14;
    switch($quotetype)
    {
        case 'Standard':
            $_SESSION['titleadd'] = 'Current Market Study';
            break;
        case 'Special':
            $_SESSION['titleadd'] = 'Special Current Market Study';
            break;
        case 'Pickup':
            $_SESSION['titleadd'] = 'Pick-up Current Market Study';
            break;
        default:
            $quotetype = 'Standard';
            $_SESSION['titleadd'] = 'Current Market Study';
            break;
    }

    // Fill the mileage options...
    $index = 1;
    for($i = 1; $i <= 40; $i++)
    {
        $pmiles[$index] = 2500 + (($i - 1) * 2500);
        $index++;
    }
    if(!isset($imileage))
    {
        $imileage = $pmiles[24];
    }

    // Need these to fill the boxes in the case of a Standard Quote...
    if($quotetype == 'Standard')
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            // First fill the list of Makes...
            mysql_select_db(DB_SERVER_DATABASE, $con);
            $result = mysql_query("select distinct m.makeid 'makeid', m.name 'make' from makes m, vehicles v where m.makeid = v.makeid and v.visible=1 order by 2");
            $data = '';
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['make']);
                if(strlen($data) > 0)
                {
                    $makeids[$index] = $row['makeid'];
                    $makes[$index] = $data;
                    $index++;
                }
            }

            // See if Make was selected already and this is a refresh or edit...
            if(isset($imake))
            {
                $make = $imake;
                // Get the ID...
                $query = 'select makeid from makes where name="'.$make.'"';
                $result = mysql_query($query);
                if($result && $row = mysql_fetch_array($result))
                {
                    $makeid = $row['makeid'];
                }
                else if($index > 0)
                {
                    $make = $makes[1];
                    $makeid = $makeids[1];
                }
            }
            else
            {
                if(isset($_REQUEST['makeitem']) && strlen($_REQUEST['makeitem']) > 0)
                {
                    $make = $_REQUEST['makeitem'];
                    // Get the ID...
                    $query = 'select makeid from makes where name="'.$make.'"';
                    $result = mysql_query($query);
                    if($result && $row = mysql_fetch_array($result))
                    {
                        $makeid = $row['makeid'];
                    }
                    else if($index > 0)
                    {
                        $make = $makes[1];
                        $makeid = $makeids[1];
                    }
                }
                else if($index > 0)
                {
                    $make = $makes[1];
                    $makeid = $makeids[1];
                }
            }

            // See if we are in a valid state...
            if(isset($makeid))
            {
                if(isset($iyear) && isset($imodel) && isset($istyle))
                {
                    $year = $iyear;
                    $model = $imodel;
                    $style = $istyle;
                    $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."' and style='".$style."'";
                    $result = mysql_query($query);
                    if(!$result || !$row = mysql_fetch_array($result))
                    {
                        unset($style);
                        $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            unset($model);
                            $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                            $result = mysql_query($query);
                            if(!$result || !$row = mysql_fetch_array($result))
                            {
                                unset($year);
                            }
                        }
                    }
                }
                else
                {
                    if(isset($_REQUEST['yearitem']) && isset($_REQUEST['modelitem']) && isset($_REQUEST['styleitem']))
                    {
                        $year = $_REQUEST['yearitem'];
                        $model = $_REQUEST['modelitem'];
                        $style = $_REQUEST['styleitem'];
                        $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."' and style='".$style."'";
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            unset($style);
                            $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                            $result = mysql_query($query);
                            if(!$result || !$row = mysql_fetch_array($result))
                            {
                                unset($model);
                                $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                                $result = mysql_query($query);
                                if(!$result || !$row = mysql_fetch_array($result))
                                {
                                    unset($year);
                                }
                            }
                        }
                    }
                    else
                    {
                        if(isset($_REQUEST['styleitem'])) unset($_REQUEST['styleitem']);
                        if(isset($_REQUEST['yearitem']) && isset($_REQUEST['modelitem']))
                        {
                            $year = $_REQUEST['yearitem'];
                            $model = $_REQUEST['modelitem'];
                            $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                            $result = mysql_query($query);
                            if(!$result || !$row = mysql_fetch_array($result))
                            {
                                unset($model);
                                $query = "select vehicleid from vehicles where visible=1 and veh_makeid=".$makeid." and year=".$year;
                                $result = mysql_query($query);
                                if(!$result || !$row = mysql_fetch_array($result))
                                {
                                    unset($year);
                                }
                            }
                        }
                        else
                        {
                            if(isset($_REQUEST['modelitem'])) unset($_REQUEST['modelitem']);
                            if(isset($_REQUEST['yearitem']))
                            {
                                $year = $_REQUEST['yearitem'];
                                $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                                $result = mysql_query($query);
                                if(!$result || !$row = mysql_fetch_array($result))
                                {
                                    unset($year);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if(isset($year)) unset($year);
                if(isset($model)) unset($model);
                if(isset($style)) unset($style);
            }

            // Set up our year search string...
            $yearquery = "select distinct year from vehicles where visible=1";
            $modelquery = "select distinct model from vehicles where visible=1";
            $stylequery = "select distinct style from vehicles where visible=1";
            if(isset($makeid))
            {
                $yearquery .= " and makeid=".$makeid;
                $modelquery .= " and makeid=".$makeid;
                $stylequery .= " and makeid=".$makeid;
            }
            $yearquery .= " order by 1 desc";

            // Get the Years...
            $result = mysql_query($yearquery);
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['year']);
                if(strlen($data) > 0)
                {
                    $years[$index] = $data;
                    $index++;
                }
            }
            if(!isset($year) && $index > 0)
            {
                $year = $years[1];
            }

            // Set up our model search string...
            if(isset($year))
            {
                $modelquery .= " and year=".$year;
                $stylequery .= " and year=".$year;
            }
            $modelquery .= " order by 1";

            // Get the Models...
            $result = mysql_query($modelquery);
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['model']);
                if(strlen($data) > 0)
                {
                    $models[$index] = $data;
                    $index++;
                }
            }
            if(!isset($model) && $index > 0)
            {
                $model = $models[1];
            }

            // Set up our style search string...
            if(isset($model))
            {
                $stylequery .= " and model='".$model."'";
            }
            $stylequery .= " order by 1";

            // Get the Styles...
            $result = mysql_query($stylequery);
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['style']);
                if(strlen($data) > 0)
                {
                    $styles[$index] = $data;
                    $index++;
                }
            }
            if(!isset($style) && $index > 0)
            {
                $style = $styles[1];
            }
            mysql_close($con);
        }
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function validateFormOnSubmit(theForm)
    {
        var vmake = document.getElementById("makelist");
        var vyear = document.getElementById("yearlist");
        var vmodel = document.getElementById("modellist");
        var vstyle = document.getElementById("stylelist");
        var vdel = document.getElementById("DeliveryAddress");

        var qtype = document.getElementById("QuoteType");
        if(qtype.value == "Standard")
        {
            theForm.Make.value = vmake.options[vmake.selectedIndex].value;
            theForm.Year.value = vyear.options[vyear.selectedIndex].value;
            theForm.Model.value = vmodel.options[vmodel.selectedIndex].value;
            theForm.Style.value = vstyle.options[vstyle.selectedIndex].value;
        }
        else
        {
            theForm.Make.value = vmake.value;
            theForm.Year.value = vyear.value;
            theForm.Model.value = vmodel.value;
            theForm.Style.value = vstyle.value;
        }

        var reason = "";
        var focusset = 0;
        var text = validateEmpty(theForm.Year, "Year");
        if(text != "")
        {
            reason += text;
            vyear.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vyear.focus();
                focusset = 1;
            }
        }
        else if((parseInt(theForm.Year.value,10) < 1901) || (parseInt(theForm.Year.value,10) > 2155))
        {
            reason += 'Year is not valid (must be between 1901-2155).\n';
            vyear.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vyear.focus();
                focusset = 1;
            }
        }
        else vyear.style.background = 'White';

        text = validateEmpty(theForm.Make, "Make");
        if(text.length > 0)
        {
            reason += text;
            vmake.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vmake.focus();
                focusset = 1;
            }
        }
        else vmake.style.background = 'White';

        text = validateEmpty(theForm.Model, "Model");
        if(text.length > 0)
        {
            reason += text;
            vmodel.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vmodel.focus();
                focusset = 1;
            }
        }
        else vmodel.style.background = 'White';

        text = validateEmpty(theForm.Style, "Style");
        if(text.length > 0)
        {
            reason += text;
            vstyle.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vstyle.focus();
                focusset = 1;
            }
        }
        else vstyle.style.background = 'White';

        text = validateEmpty(theForm.colorcombo, "Color Combination")
        if(text.length > 0)
        {
            reason += text;
            theForm.colorcombo.style.background = '#ffcccc';
            if(focusset == 0)
            {
                theForm.colorcombo.focus();
                focusset = 1;
            }
        }

        if(vdel.checked)
        {
            text = validateEmpty(theForm.DeliveryCity, "Delivery City");
            if(text.length > 0)
            {
                reason += text;
                theForm.DeliveryCity.style.background = '#ffcccc';
                if(focusset == 0)
                {
                    theForm.DeliveryCity.focus();
                    focusset = 1;
                }
            }
            else theForm.DeliveryCity.style.background = 'White';
            text = validateEmpty(theForm.DeliveryState, "Delivery State");
            if(text.length > 0)
            {
                reason += text;
                theForm.DeliveryState.style.background = '#ffcccc';
                if(focusset == 0)
                {
                    theForm.DeliveryState.focus();
                    focusset = 1;
                }
            }
            else theForm.DeliveryState.style.background = 'White';
        }

        if(reason != "")
        {
            alert("Some fields need correction:"+'\n' + reason + '\n');
            return false;
        }
        return true;
    }

    function trimAll(sString)
    {
        while(sString.substring(0,1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while(sString.substring(sString.length-1, sString.length) == ' ')
        {
            sString = sString.substring(0,sString.length-1);
        }
        return sString;
    }

    function validateEmpty(fld, title)
    {
        var error = "";
        var tString = trimAll(fld.value);

        if (tString.length == 0)
        {
            fld.style.background = '#ffcccc';
            error = title + " is required."+'\n';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }

    function itemchanged()
    {
        var vform = document.getElementById("quoteform");
        var vmake = document.getElementById("makelist");
        var vyear = document.getElementById("yearlist");
        var vmodel = document.getElementById("modellist");
        var vstyle = document.getElementById("stylelist");

        var qtype = document.getElementById("QuoteType");
        if(qtype.value == "Standard")
        {
            vform.Make.value = vmake.options[vmake.selectedIndex].value;
            vform.Year.value = vyear.options[vyear.selectedIndex].value;
            vform.Model.value = vmodel.options[vmodel.selectedIndex].value;
            vform.Style.value = vstyle.options[vstyle.selectedIndex].value;
        }
        else
        {
            vform.Make.value = vmake.value;
            vform.Year.value = vyear.value;
            vform.Model.value = vmodel.value;
            vform.Style.value = vstyle.value;
        }

        /*
        var vquote = document.getElementById("QuoteID");
        var vtype = document.getElementById("QuoteType");
        var vmake = document.getElementById("makelist");
        var vyear = document.getElementById("yearlist");
        var vmodel = document.getElementById("modellist");
        var vstyle = document.getElementById("stylelist");
        self.location='quote.php?' +
            'makeitem=' + vmake.options[vmake.selectedIndex].value +
            '&yearitem=' + vyear.options[vyear.selectedIndex].value +
            '&modelitem=' + vmodel.options[vmodel.selectedIndex].value +
            '&styleitem=' + vstyle.options[vstyle.selectedIndex].value +
            '&QuoteID=' + vquote.value +
            '&QuoteType=' + vtype.value + '#refresh';
        */
        vform.action = 'quote.php';
        vform.submit();
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>


<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
<?php
    echo '<h1 class="subhead" style=" text-align: left;margin-left: 0;width: ';
    switch($quotetype)
    {
        case 'Standard':
            if(isset($editquoteID) && ($editquoteID != -1)) echo '100';
            else echo '100';
            break;
        case 'Special':
            if(isset($editquoteID) && ($editquoteID != -1)) echo '100';
            else echo '100';
            break;
        case 'Pickup':
            if(isset($editquoteID) && ($editquoteID != -1)) echo '100';
            else echo '100';
            break;
    }
    echo '%;" >';
    if(isset($editquoteID) && ($editquoteID != -1)) echo 'Edit ';
    switch($quotetype)
    {
        case 'Standard':
            echo 'Current Market Study';
            break;
        case 'Special':
            echo 'Special Current Market Study';
            break;
        case 'Pickup':
            echo 'Pick-up Truck Current Market Study';
            break;
    }
    echo '</h1>';
?>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0px;"><a href="allfirmquotes.php">Go to existing Current Market Studies</a></p>
            <table width="600" border="0" cellspacing="0" cellpadding="10"><tr><td>
<?php
    if($quotetype == 'Special')
    {
        echo '<p>Special requested vehicles may or may not be available through this ordering process. A representative will contact ';
        echo 'you after researching the availability and pricing of your special request.</p>';
    }
    elseif($quotetype == 'Pickup')
    {
        echo '<p>Please request a quote for a pick-up that is 1 - 6 years old, allowing some flexibility in the specific options and ';
        echo 'colors you desire. Since pick-ups come in many combinations of the options available (and nice ones can be ';
        echo 'difficult to find), flexibility plays an important role.</p>';
    }
    echo '</td></tr></table></div><!-- end grideightgrey-->';
    echo '<a name="refresh"></a><h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">';
    if($quotetype == 'Standard') echo 'Please Select a Vehicle';
    else echo 'Please Specify a Vehicle';
    echo '</h2>';
?>
        <div class="grideightgrey">
            <table width="600" border="0" cellspacing="0" cellpadding="10">
            <tr>
<?php
    if($quotetype == 'Standard')
    {
        echo '<td><label for="yearlist">Year</label><br /><select name="yearlist" id="yearlist" onchange="javascript:itemchanged()">';
        $current = date('Y');
        $count = count($years);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$years[$i].'"';
            if(isset($year) && $years[$i]==$year) echo 'selected="selected"';
            else if(!isset($year) && $years[$i]==($current-1)) echo 'selected="selected"';
            echo '>'.$years[$i].'</option>';
        }
        echo '</select></td>';
        echo '<td><label for="makelist">Make</label><br /><select name="makelist" id="makelist" onchange="javascript:itemchanged()">';
        $count = count($makes);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$makes[$i].'"';
            if(isset($make) && $makes[$i]==$make) echo 'selected="selected"';
            echo '>'.$makes[$i].'</option>';
        }
        echo '</select></td>';
        echo '<td><label for="modellist">Model</label><br /><select name="modellist" id="modellist" onchange="javascript:itemchanged()">';
        $count = count($models);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$models[$i].'"';
            if(isset($model) && $models[$i]==$model) echo 'selected="selected"';
            echo '>'.$models[$i].'</option>';
        }
        echo '</select>';
        echo '</td><td width="300"><label for="stylelist">Style</label><br /><select name="stylelist" id="stylelist" onchange="javascript:itemchanged()">';
        $count = count($styles);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$styles[$i].'"';
            if(isset($style) && $styles[$i]==$style) echo 'selected="selected"';
            echo '>'.$styles[$i].'</option>';
        }
        echo '</select></td>';

        if(isset($editquoteID) && ($editquoteID != -1))
        {
            echo '</tr><tr><td colspan="4">';
            echo '<center>Image is representative of the selections made when last saved, not the actual vehicle.';
            $max_width = 400;
            $max_height = 400;
            if(isset($imagefile) && !is_null($imagefile)) echo '<img id="vehimage" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            else echo '** Image currently not available **';
            echo '</center></td>';
        }

        echo '</tr><tr><td colspan="4">';
        echo "Can't find the vehicle you want? &nbsp;";
        echo 'You can enter any vehicle as a "<a href="quote.php?QuoteType=Special">Special Quote</a>".</td>';
    }
    else
    {
        echo '<td>';
        echo '<label for="yearlist">Year</label>';
        echo '<br />';
        echo '<input name="yearlist" id="yearlist" type="text" size="5" maxlength="4" value="';
        if(isset($iyear)) echo $iyear;
        echo '" onkeypress="javascript:return numbersonly(event);" />';
        echo '</td>';
        echo '<td>';
        echo '<label for="makelist">Make</label>';
        echo '<br />';
        echo '<input name="makelist" id="makelist" type="text" size="20" maxlength="50" value="';
        if(isset($imake)) echo $imake;
        echo '" />';
        echo '</td>';
        echo '<td>';
        echo '<label for="modellist">Model</label>';
        echo '<br />';
        echo '<input name="modellist" id="modellist" type="text" size="5" maxlength="30" value="';
        if(isset($imodel)) echo $imodel;
        echo '" />';
        echo '</td>';
        echo '<td width="300">';
        echo '<label for="stylelist">Style</label>';
        echo '<br />';
        echo '<input name="stylelist" id="stylelist" type="text" size="25" maxlength="25" value="';
        if(isset($istyle)) echo $istyle;
        echo '" />';
        echo '</td></tr><tr><td colspan="4">';
    }
?>
            </tr>
            </table>
        </div><!-- end grideightgrey-->
        <form action="quoterequested.php" onsubmit="javascript:return validateFormOnSubmit(this)" method="post" id="quoteform" name="quoteform">
            <input type="hidden" name="QuoteID" id="QuoteID" value="<?php if(isset($iquoteid)) echo $iquoteid; else echo '-1' ?>">
            <input type="hidden" name="QuoteType" id="QuoteType" value="<?php echo $quotetype; ?>">
            <input name="Year" id="Year" type="hidden" size="5" maxlength="4" value="" />
            <input name="Make" id="Make" type="hidden" size="20" maxlength="50" value="" />
            <input name="Model" id="Model" type="hidden" size="5" maxlength="30" value="" />
            <input name="Style" id="Style" type="hidden" size="25" maxlength="25" value="" />
          <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;"> Vehicle Preferences</h1>
            <div class="grideightgrey">
                <table width="300" border="0" cellspacing="0" cellpadding="10" align="left">
                <tr>
                    <td>
                        <label for="selectmileage"><strong>Mileage Ceiling</strong></label>
                        <br />
                        <select id="selectmileage" name="selectmileage" >
<?php
    $count = count($pmiles);
    for($i = 1; $i <= $count; $i++)
    {
        echo '<option value="'.$pmiles[$i].'"';
        if(isset($imileage) && ($pmiles[$i]==$imileage)) echo 'selected="selected"';
        elseif(!isset($imileage) && ($pmiles[$i]==60000)) echo 'selected="selected"';
        echo '>'.number_format($pmiles[$i]).'</option>';
    }
?>
                        </select>
                    </td>
                    <td>
                        <label for="selectseat"><strong>Seat Material</strong></label>
                        <br />
                        <select name="selectseat" id="selectseat">
                            <option value="Flexible" <?php if(isset($iseattype) && ($iseattype == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Cloth" <?php if(!isset($iseattype) || ($iseattype == 'Cloth')) echo 'selected="selected"'; ?>>Cloth</option>
                            <option value="Leather" <?php if(isset($iseattype) && ($iseattype == 'Leather')) echo 'selected="selected"'; ?>>Leather</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="selectengine"><strong>Engine</strong></label>
                        <br />
                        <select name="selectengine" id="selectengine">
                            <option value="0" <?php if(isset($iengine) && ($iengine == 0)) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="4" <?php if(!isset($iengine) || ($iengine == 4)) echo 'selected="selected"'; ?>>4 cylinder</option>
                            <option value="5" <?php if(isset($iengine) && ($iengine == 5)) echo 'selected="selected"'; ?>>5 cylinder</option>
                            <option value="6" <?php if(isset($iengine) && ($iengine == 6)) echo 'selected="selected"'; ?>>6 cylinder</option>
                            <option value="8" <?php if(isset($iengine) && ($iengine == 8)) echo 'selected="selected"'; ?>>8 cylinder</option>
                            <option value="10" <?php if(isset($iengine) && ($iengine == 10)) echo 'selected="selected"'; ?>>10 cylinder</option>
                            <option value="12" <?php if(isset($iengine) && ($iengine == 12)) echo 'selected="selected"'; ?>>12 cylinder</option>
                        </select>
                    </td>
                    <td>
                        <label for="selectdrive"><strong>Wheel Drive</strong></label>
                        <br />
                        <select name="selectdrive" id="selectdrive">
                            <option value="Flexible" <?php if(isset($idrive) && ($idrive == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Rear" <?php if(!isset($idrive) || ($idrive == 'Rear')) echo 'selected="selected"'; ?>>Rear</option>
                            <option value="Front" <?php if(isset($idrive) && ($idrive == 'Front')) echo 'selected="selected"'; ?>>Front</option>
                            <option value="Four" <?php if(isset($idrive) && ($idrive == 'Four')) echo 'selected="selected"'; ?>>Four</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="selecttrans"><strong>Transmission</strong></label>
                        <br />
                        <select name="selecttrans" id="selecttrans">
                            <option value="Flexible" <?php if(isset($itrans) && ($itrans == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Automatic" <?php if(!isset($itrans) || ($itrans == 'Automatic')) echo 'selected="selected"'; ?>>Automatic</option>
                            <option value="Manual" <?php if(isset($itrans) && ($itrans == 'Manual')) echo 'selected="selected"'; ?>>Manual</option>
                        </select>
                    </td>
                    <td>
                        <label for="selectwheels"><strong>Wheel Type</strong></label>
                        <br />
                        <select name="selectwheels" id="selectwheels">
                            <option value="Flexible" <?php if(isset($iwheels) && ($iwheels == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Alloy" <?php if(!isset($iwheels) || ($iwheels == 'Alloy')) echo 'selected="selected"'; ?>>Alloy</option>
                            <option value="Chrome" <?php if(isset($iwheels) && ($iwheels == 'Chrome')) echo 'selected="selected"'; ?>>Chrome</option>
                            <option value="Hubcaps" <?php if(isset($iwheels) && ($iwheels == 'Hubcaps')) echo 'selected="selected"'; ?>>Hubcaps</option>
                        </select>
                    </td>
                </tr>
                <tr>
<?php
    if($quotetype == 'Pickup')
    {
?>
                    <td valign="top">
                        <label for="selectcab"><strong>Cab Type</strong></label>
                        <br />
                        <select name="selectcab" id="selectcab">
                            <option value="Flexible" <?php if(isset($icab) && ($icab == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Regular" <?php if(!isset($icab) || ($icab == 'Regular')) echo 'selected="selected"'; ?>>Regular</option>
                            <option value="Extended" <?php if(isset($icab) && ($icab == 'Extended')) echo 'selected="selected"'; ?>>Extended</option>
                            <option value="FourDoor" <?php if(isset($icab) && ($icab == 'FourDoor')) echo 'selected="selected"'; ?>>Four Door</option>
                        </select>
                        <br /><br />
                        <label for="selectfrontseat"><strong>Front Seat Type</strong></label>
                        <br />
                        <select name="selectfrontseat" id="selectfrontseat">
                            <option value="Flexible" <?php if(isset($ifront) && ($ifront == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Bench" <?php if(!isset($ifront) || ($ifront == 'Bench')) echo 'selected="selected"'; ?>>Bench with Armrest</option>
                            <option value="Standard" <?php if(isset($ifront) && ($ifront == 'Standard')) echo 'selected="selected"'; ?>>Standard Bench Seat</option>
                            <option value="Bucket" <?php if(isset($ifront) && ($ifront == 'Bucket')) echo 'selected="selected"'; ?>>Bucket Seats</option>
                        </select>
                        <br /><br />
                        <label for="selectbedtype"><strong>Bed Type</strong></label>
                        <br />
                        <select name="selectbedtype" id="selectbedtype">
                            <option value="Flexible" <?php if(isset($ibedtype) && ($ibedtype == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Mini" <?php if(isset($ibedtype) && ($ibedtype == 'Mini')) echo 'selected="selected"'; ?>>Mini bed (about 4 ft)</option>
                            <option value="Short" <?php if(!isset($ibedtype) || ($ibedtype == 'Short')) echo 'selected="selected"'; ?>>Short bed (about 6 ft)</option>
                            <option value="Long" <?php if(isset($ibedtype) && ($ibedtype == 'Long')) echo 'selected="selected"'; ?>>Long bed (about 8 ft)</option>
                            <option value="ExtraLong" <?php if(isset($ibedtype) && ($ibedtype == 'ExtraLong')) echo 'selected="selected"'; ?>>Extra Long bed (about 10 ft)</option>
                        </select>
                    </td>
<?php
    }
    else
    {
?>
                    <td valign="top">
                        <label for="selectsunroof"><strong>Sunroof</strong></label>
                        <br />
                        <select name="selectsunroof" id="selectsunroof">
                            <option value="Flexible" <?php if(isset($isunroof) && ($isunroof == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="None" <?php if(!isset($isunroof) || ($isunroof == 'None')) echo 'selected="selected"'; ?>>No Thanks</option>
                            <option value="Glass" <?php if(isset($isunroof) && ($isunroof == 'Glass')) echo 'selected="selected"'; ?>>Yes</option>
                        </select>
                    </td>
<?php
    }
?>
                    <td valign="top">
                        <input name="RadFM" type="checkbox" value="FM" <?php if(!isset($iradfm) || ($iradfm == 1)) echo 'checked="checked"'; ?> />AM/FM<BR />
                        <input name="RadCas" type="checkbox" value="Cassette" <?php if(isset($iradcas) && ($iradcas == 1)) echo 'checked="checked"'; ?> />Cassette<BR />
                        <input name="RadCD" type="checkbox" value="CD" <?php if(isset($iradcd) && ($iradcd == 1)) echo 'checked="checked"'; ?> />CD<BR />
                        <input name="RadFlex" type="checkbox" value="Flexible" <?php if(isset($iradflex) && ($iradflex == 'Flexible')) echo 'checked="checked"'; ?> />Flexible<BR />
                    </td>
                </tr>
                </table>
                <table width="275" border="0" cellspacing="0" cellpadding="3" align="right">
                <tr>
                    <td>&nbsp;</td>
                    <td align="center" width="45"><p class="greyeleven; color:black">Yes</p></td>
                    <td align="center" width="45"><p class="greyeleven; color:black">No</p></td>
                    <td align="center" width="45"><p class="greyeleven; color:black">Flexible</p></td>
                </tr>
                <tr>
                    <td><label for="PowerDoorLocks">Power Door Locks</label></td>
                    <td align="center"><input name="PowerDoorLocks" type="radio" value="Yes" <?php if(!isset($ipdoor) || ($ipdoor == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerDoorLocks" type="radio" value="No" <?php if(isset($ipdoor) && ($ipdoor == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerDoorLocks" type="radio" value="Flexible" <?php if(isset($ipdoor) && ($ipdoor == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="PowerWindows">Power Windows</label></td>
                    <td align="center"><input name="PowerWindows" type="radio" value="Yes" <?php if(!isset($ipwin) || ($ipwin == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerWindows" type="radio" value="No" <?php if(isset($ipwin) && ($ipwin == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerWindows" type="radio" value="Flexible" <?php if(isset($ipwin) && ($ipwin == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="PowerSeats">Power Seats</label></td>
                    <td align="center"><input name="PowerSeats" type="radio" value="Yes" <?php if(isset($ipseat) && ($ipseat == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerSeats" type="radio" value="No" <?php if(!isset($ipseat) || ($ipseat == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerSeats" type="radio" value="Flexible" <?php if(isset($ipseat) && ($ipseat == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="HeatedSeats">Heated Seats</label></td>
                    <td align="center"><input name="HeatedSeats" type="radio" value="Yes" <?php if(isset($iheat) && ($iheat == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="HeatedSeats" type="radio" value="No" <?php if(!isset($iheat) || ($iheat == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="HeatedSeats" type="radio" value="Flexible" <?php if(isset($iheat) && ($iheat == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="AirConditioning">Air Conditioning</label></td>
                    <td align="center"><input name="AirConditioning" type="radio" value="Yes" <?php if(!isset($iair) || ($iair == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="AirConditioning" type="radio" value="No" <?php if(isset($iair) && ($iair == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="AirConditioning" type="radio" value="Flexible" <?php if(isset($iair) && ($iair == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="RemoteEntry">Remote Entry</label></td>
                    <td align="center"><input name="RemoteEntry" type="radio" value="Yes" <?php if(isset($iremote) && ($iremote == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RemoteEntry" type="radio" value="No" <?php if(!isset($iremote) || ($iremote == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RemoteEntry" type="radio" value="Flexible" <?php if(isset($iremote) && ($iremote == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="TractionControl">Traction Control</label></td>
                    <td align="center"><input name="TractionControl" type="radio" value="Yes" <?php if(isset($itraction) && ($itraction == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TractionControl" type="radio" value="No" <?php if(!isset($itraction) || ($itraction == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TractionControl" type="radio" value="Flexible" <?php if(isset($itraction) && ($itraction == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="SecuritySystem">Security System</label></td>
                    <td align="center"><input name="SecuritySystem" type="radio" value="Yes" <?php if(isset($isecure) && ($isecure == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="SecuritySystem" type="radio" value="No" <?php if(!isset($isecure) || ($isecure == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="SecuritySystem" type="radio" value="Flexible" <?php if(isset($isecure) && ($isecure == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="CruiseControl">Cruise Control</label></td>
                    <td align="center"><input name="CruiseControl" type="radio" value="Yes" <?php if(!isset($icruise) || ($icruise == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="CruiseControl" type="radio" value="No" <?php if(isset($icruise) && ($icruise == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="CruiseControl" type="radio" value="Flexible" <?php if(isset($icruise) && ($icruise == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="Navigation">Navigation</label></td>
                    <td align="center"><input name="Navigation" type="radio" value="Yes" <?php if(isset($inavsys) && ($inavsys == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="Navigation" type="radio" value="No" <?php if(!isset($inavsys) || ($inavsys == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="Navigation" type="radio" value="Flexible" <?php if(isset($inavsys) && ($inavsys == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
<?php
    if($quotetype == 'Pickup')
    {
?>
                <tr>
                    <td><label for="RearSlidingWindow">Rear Sliding Window</label></td>
                    <td align="center"><input name="RearSlidingWindow" type="radio" value="Yes" <?php if(isset($irearwin) && ($irearwin == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RearSlidingWindow" type="radio" value="No" <?php if(!isset($irearwin) || ($irearwin == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RearSlidingWindow" type="radio" value="Flexible" <?php if(isset($irearwin) && ($irearwin == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="BedLiner">Bed Liner</label></td>
                    <td align="center"><input name="BedLiner" type="radio" value="Yes" <?php if(!isset($ibed) || ($ibed == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="BedLiner" type="radio" value="No" <?php if(isset($ibed) && ($ibed == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="BedLiner" type="radio" value="Flexible" <?php if(isset($ibed) && ($ibed == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="TowPackage">Tow Package</label></td>
                    <td align="center"><input name="TowPackage" type="radio" value="Yes" <?php if(isset($itow) && ($itow == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TowPackage" type="radio" value="No" <?php if(!isset($itow) || ($itow == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TowPackage" type="radio" value="Flexible" <?php if(isset($itow) && ($itow == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
<?php
    }
    else
    {
?>
                <tr>
                    <td><label for="LuggageRack">Luggage Rack</label></td>
                    <td align="center"><input name="LuggageRack" type="radio" value="Yes" <?php if(isset($iluggage) && ($iluggage == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="LuggageRack" type="radio" value="No" <?php if(!isset($iluggage) || ($iluggage == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="LuggageRack" type="radio" value="Flexible" <?php if(isset($iluggage) && ($iluggage == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
<?php
    }
?>
                </table>
                <br clear="all" />
                <br />
                <table width="600" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="300">
                        <label for="colorcombo">* Preferred color combinations:</label>
                    </td>
                    <td width="300">
                        <label for="notes">Special notes:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea name="colorcombo" id="colorcombo" cols="37" rows="4"><?php if(isset($icolors)) echo $icolors; ?></textarea>
                    </td>
                    <td>
                        <textarea name="notes" id="notes" cols="37" rows="4"><?php if(isset($inotes)) echo $inotes; ?></textarea>
                    </td>
                </tr>
                </table>
                <br />
                <h4 class="subhead" style="font-size:18px;">Delivery Options</h4>
                <br />
                <table width="325" border="0" cellspacing="0" cellpadding="5">
                <tr valign="middle">
                    <td>
                        <input name="DeliveryPickup" id="DeliveryPickup" type="radio" value="PickUp" <?php if(!isset($idelorpick) || ($idelorpick == 1)) echo 'checked="checked"'; ?> />
                    </td>
                    <td colspan="2">
                        <label for="DeliveryPickup">I will pick up at my 1-800-vehicles dealer location</label>
                    </td>
                </tr>
                <tr valign="middle">
                    <td>
                        <input name="DeliveryPickup" id="DeliveryAddress" type="radio" value="Deliver" <?php if(isset($idelorpick) && ($idelorpick == 0)) echo 'checked="checked"'; ?> />
                    </td>
                    <td colspan="2">
                        <label for="DeliveryAddress">Please deliver my vehicle to:</label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <label for="DeliveryCity"><p class="greyeleven" style="margin-top:0; margin-left:5px;">City</p></label>
                        <input name="DeliveryCity" id="DeliveryCity" type="text" value="<?php if(isset($idelcity)) echo $idelcity; ?>" size="15" maxlength="50" />
                    </td>
                    <td>
                        <label for="DeliveryState"><p class="greyeleven" style="margin-top:0; margin-left:5px;">State</p></label>
                        <input name="DeliveryState" id="DeliveryState" type="text" value="<?php if(isset($idelstate)) echo $idelstate; ?>" size="15" maxlength="2" />
                    </td>
                </tr>
                </table>
                <br />
<?php
    if(($quotetype == 'Standard') || ($quotetype == 'Special') || ($quotetype == 'Pickup'))
    {
        if(isset($editquoteID) && ($editquoteID != -1)) echo '<button type="submit" class="med"><nobr>SAVE CHANGES</nobr></button>';
        else echo '<button type="submit" class="med"><nobr>REQUEST A PRICE & AVAILABILITY STUDY</nobr></button>';
    }
?>
            </div><!-- end grideightgrey-->
        </div><!-- grid eight container -->
    </form>
<?php
    $_SESSION['hideteaser1'] = 'true';
    $_SESSION['hideteaser2'] = 'true';
    $_SESSION['hideteaser4'] = 'true';
    require("teaser.php");
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
