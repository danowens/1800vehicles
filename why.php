<div id="footerinfo">
    <div id="slider_footer">
        <h2 class="subheadfooter" width="300">Why 1-800-vehicles.com?</h2>
        <span class="footshow">
            Because our revolutionary process allows you to choose a vehicle from the best of our nation's wholesale inventory, and save time, money and hassles in the process.
        </span>
        <ul>
            <li>
                <span class="gridfour">
                    <img src="common/layout/footercars.jpg" />
                </span>
                <h2>CHOOSE FROM THE BEST</h2>
                <span class="footdesc">
                    Only one fourth of used vehicles on the market are in excellent condition. Our cost efficient process allows us to pay what is necessary to acquire such vehicles
                    and guarantee the condition. An independent inspection is made to confirm that your vehicle has met the terms of our agreement.
                </span>
            </li>
            <li>
                <span class="gridfour">
                    <img src="common/layout/footercars2.jpg" />
                </span>
                <h2>SAVE TIME</h2>
                <span class="footdesc">
                    You can do it all online and by phone. Avoid wasting time visiting dealerships to look at vehicles or calling about vehicles listed out of state, especially since
                    three fourths of them are not in excellent condition anyway.
                </span>
            </li>
            <li>
                <span class="gridfour">
                    <img src="common/layout/footercars3.jpg" />
                </span>
                <h2>SAVE MONEY</h2>
                <span class="footdesc">
                    Our "virtual inventory" process saves you money in three important ways:<br />
                    1) We don't have an expensive property to hold these vehicles in inventory<br />
                    2) We avoid high interest and advertising expenses<br />
                    3) We take advantage of today's lower wholesale market prices
                </span>
            </li>
            <li>
                <span class="gridfour">
                    <img src="common/layout/footercars4.jpg" />
                </span>
                <h2>SAVE HASSLES</h2>
                <span class="footdesc">
                    1-800-vehicles.com representatives provide free consulting that is professional and impartial, and based on a massive "virtual inventory" of vehicles. They will
                    help you determine what vehicle best meets your needs, not pressure you to buy something that has been sitting in inventory for months.<br />
                    <span class="footer_callout" style="margin-top: 2px;">
                        We have put the fun back in the car buying experience!
                    </span>
                </span>
            </li>
        </ul>
    </div><!-- end slider_footer-->
</div><!-- end footerinfo-->
