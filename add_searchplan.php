<?php require("globals.php"); ?>
<?php
$_SESSION['state'] = 0;
$_SESSION['substate'] = 25;
$_SESSION['titleadd'] = 'Add Vehicle Specification';

   unset($_SESSION['assessment_post']);
   unset($_SESSION['pos_post']);
   unset($_SESSION['searchplan_post']);
   unset($_SESSION['consult_post']);
   

$userid = $_SESSION['userid'];
$marketneedid = $_SESSION['marketneedid'];
$srep = getsalesrep($userid, $marketneedid);
$total = 0;
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->



<script type="text/javascript">
    var count = <?php echo $total; ?>;
    $(document).ready(function() {
        $('body').on('click', '#form_searchplan input:text', function() {
            var element = $(this);
            $("#notetext").val(element.val());
            $("#dialog").dialog({
                modal: true,
                title: " ",
                close: function(event, ui) {
                    element.val($("#notetext").val());
                    $("#dialog").dialog("destroy");
                }
            });
        });
        if (count == 0) {
            addvehicle();
        }
        
        $(".grideightgrey .showrequirfield").bind('click', function(e) {
           
                        e.preventDefault();
			var id= $(this).attr('myhidevalue');
                    
                        $('#notshowfields'+id).show();
	}); 
        
        $("body").on("click", "#pos_page", function(event){
            location.href = "<?php echo WEB_SERVER_NAME . 'add_pas.php'; ?>";
        });
        
    });

    function validateFormOnSubmit() {
        var reason = "";
        if (reason != "")
        {
            vbudget.focus();
            alert("Some fields need correction:" + '\n' + reason + '\n');
            return false;
        }
        return true;
    }
    
    
    

    function addvehicle() {
        id = window.count;
        window.count = window.count + 1;
        console.log($("#number").val());
        $("#number").val(window.count);
        console.log("latest - " + $("#number").val());
        if (window.count == 1) {
            $.ajax({
                type: "POST",
                url: 'ajaxaddtoplan.php',
                data: {number: window.count},
                cache: false,
                async: false,
                dataType: 'html',
                success: function(data) {
                  $("#vehiclesplan").append(data);
                   $(".grideightgrey .showrequirfield").unbind( "click" );
                      $(".grideightgrey .showrequirfield").bind('click', function(e) { 
                            e.preventDefault();
			var id= $(this).attr('myhidevalue');
                           
                        $('#notshowfields'+id).show();
                    }); 

                }
            });
        } else {
            yearlist = $("#yearlist" + id).val();
            makelist = $("#makelist" + id).val();
            modellist = $("#modellist" + id).val();
            stylelist = $("#stylelist" + id).val();
            otherstyle = $("#otherstyle" + id).val();
            otheryear = $("#otheryear" + id).val();
            vehicleneed = $("#vehicleneed" + id).val();
            mileagefrom = $("#mileagefrom" + id).val();
            mileageto = $("#yearlist" + id).val();
            mileageceiling = $("#mileageceiling" + id).val();
            transmission = $("#transmission" + id).val();
            drivetrain = $("#drivetrain" + id).val();
            extlike = $("#extlike" + id).val();
            extdislike = $("#extdislike" + id).val();
            intlike = $("#intlike" + id).val();
            intdislike = $("#intdislike" + id).val();
            budgetfrom = $("#budgetfrom" + id).val();
            budgetto = $("#budgetto" + id).val();
            borrowmaxpayment = $("#borrowmaxpayment" + id).val();
            borrowdownpayment = $("#borrowdownpayment" + id).val();
//Do you Prefer section start
            frontstype = $("#frontstype" + id).val();
            bedtype = $("#bedtype" + id).val();
            leather = $("#leather" + id).val();
            heatedseat = $("#heatedseat" + id).val();
            navigation = $("#navigation" + id).val();
            sunroof = $("#sunroof" + id).val();
            alloywheels = $("#alloywheels" + id).val();
            rearwindow = $("#rearwindow" + id).val();
            bedliner = $("#bedliner" + id).val();
            entertainmentsystem = $("#entertainmentsystem" + id).val();
            thirdrs = $("#thirdrs" + id).val();
            crow = $("#crow" + id).val();
            prhatch = $("#prhatch" + id).val();
            backupcamera = $("#backupcamera" + id).val();
            tpackage = $("#tpackage" + id).val();
//Do you Prefer section end
            musthave = $("#musthave" + id).val();
            reallyhave = $("#reallyhave" + id).val();
            flexible = $("#flexible" + id).val();
            notwant = $("#notwant" + id).val();
            clientnote = $("#clientnote" + id).val();


            $.ajax({
                type: "POST",
                url: 'ajaxaddtoplan.php',
                data: {number: window.count, yearlist: yearlist, makelist: makelist, modellist: modellist, stylelist: stylelist, otherstyle: otherstyle, otheryear: otheryear, vehicleneed: vehicleneed, mileagefrom: mileagefrom, mileageto: mileageto, mileageceiling: mileageceiling, transmission: transmission, drivetrain: drivetrain, extlike: extlike, extdislike: extdislike, intlike: intlike, intdislike: intdislike, budgetfrom: budgetfrom, budgetto: budgetto, borrowmaxpayment: borrowmaxpayment, borrowdownpayment: borrowdownpayment, frontstype: frontstype, bedtype: bedtype, leather: leather, heatedseat: heatedseat, navigation: navigation, sunroof: sunroof, alloywheels: alloywheels, rearwindow: rearwindow, bedliner: bedliner, entertainmentsystem: entertainmentsystem, thirdrs: thirdrs, crow: crow, prhatch: prhatch, backupcamera: backupcamera, tpackage: tpackage, musthave: musthave, reallyhave: reallyhave, flexible: flexible, notwant: notwant, clientnote: clientnote},
                cache: false,
                async: false,
                dataType: 'html',
                success: function(data) {
                  $("#vehiclesplan").append(data);
                   $(".grideightgrey .showrequirfield").unbind( "click" );
                      $(".grideightgrey .showrequirfield").bind('click', function(e) { 
                         
                            e.preventDefault();
			var id= $(this).attr('myhidevalue');
                         
                        $('#notshowfields'+id).show();
                    }); 
                }
            });
        }
    }

    function yearchanged(id) {
        var year = $("#yearlist" + id).val();
        $.ajax({
            type: "POST",
            url: 'ajaxallmakes.php',
            data: {year: year},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#makelist" + id).html('');
                $("#makelist" + id).append(data);

                makechanged(id);
            }
        });
    }

    function makechanged(id) {
        var year = $("#yearlist" + id).val();
        var make = $("#makelist" + id).val();
        $.ajax({
            type: "POST",
            url: 'ajaxallmodels.php',
            data: {year: year, make: make},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#modellist" + id).html('');
                $("#modellist" + id).append(data);

                modelchanged(id);
            }
        });
    }

    function modelchanged(id) {
        var year = $("#yearlist" + id).val();
        var make = $("#makelist" + id).val();
        var model = $("#modellist" + id).val();

        $.ajax({
            type: "POST",
            url: 'ajaxallstyles.php',
            data: {year: year, make: make, model: model},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#stylelist" + id).html('');
                $("#stylelist" + id).append(data);

                stylechanged(id);
            }
        });
    }

    function stylechanged(id) {
        var style = $("#stylelist" + id).val().split(";");
        $.ajax({
            type: "POST",
            url: 'ajaxcheckvehicletype.php',
            data: {id: style[0]},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#vehicletype" + id).val(data);
                if (data == 'Auto') {
                    $("#drivetrain" + id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');

//Do you prefer section
                    $("#trfrontstype" + id).hide();
                    $("#trbedtype" + id).hide();
                    $("#trrearwindow" + id).hide();
                    $("#trbedliner" + id).hide();
                    $("#trentertainmentsystem" + id).hide();
                    $("#trthirdrs" + id).hide();
                    $("#trcrow" + id).hide();
                    $("#trprhatch" + id).hide();
                    $("#trbackupcamera" + id).hide();
                    $("#trtpackage" + id).hide();

                } else if (data == 'Minivan' || data == 'MiniVan') {
                    $("#drivetrain" + id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');

//Do you prefer section
                    $("#trfrontstype" + id).hide();
                    $("#trbedtype" + id).hide();
                    $("#trrearwindow" + id).hide();
                    $("#trbedliner" + id).hide();
                    $("#trentertainmentsystem" + id).show();
                    $("#trthirdrs" + id).hide();
                    $("#trcrow" + id).hide();
                    $("#trprhatch" + id).show();
                    $("#trbackupcamera" + id).show();
                    $("#trtpackage" + id).hide();

                } else if (data == 'SUV') {
                    $("#drivetrain" + id).html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');

//Do you prefer section
                    $("#trfrontstype" + id).hide();
                    $("#trbedtype" + id).hide();
                    $("#trrearwindow" + id).hide();
                    $("#trbedliner" + id).hide();
                    $("#trentertainmentsystem" + id).show();
                    $("#trthirdrs" + id).show();
                    $("#trcrow" + id).show();
                    $("#trprhatch" + id).show();
                    $("#trbackupcamera" + id).show();
                    $("#trtpackage" + id).show();

                } else if (data == 'Pickup') {
                    $("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');

//Do you prefer section
//@TODO make function with 2 parameter (Array of ids and event)
                    $("#trfrontstype" + id).show();
                    $("#trbedtype" + id).show();
                    $("#trrearwindow" + id).show();
                    $("#trbedliner" + id).show();
                    $("#trentertainmentsystem" + id).hide();
                    $("#trthirdrs" + id).hide();
                    $("#trcrow" + id).hide();
                    $("#trprhatch" + id).hide();
                    $("#trbackupcamera" + id).show();
                    $("#trtpackage" + id).show();
                }
            }
        });
    }

    function specific(id, val) {
        $("#specific" + id).val(val);
        if (val == 1) {
            $("#specificsection" + id).hide();
            $("#selectyear" + id).hide();
            $("#selectmake" + id).hide();
            $("#selectmodel" + id).hide();
            $("#selectstyle" + id).hide();

            $("#vehiclesection" + id).show();
            $("#textyear" + id).show();
            $("#textmake" + id).show();
            $("#textmodel" + id).show(); 
            $("#textstyle" + id).show();
        } else {
            $("#specificsection" + id).show();
            $("#selectyear" + id).show();
            $("#selectmake" + id).show();
            $("#selectmodel" + id).show();
            $("#selectstyle" + id).show();

            $("#vehiclesection" + id).hide();
            $("#textyear" + id).hide();
            $("#textmake" + id).hide();
            $("#textmodel" + id).hide();
            $("#textstyle" + id).hide();
        }
    }
</script>
<script type="text/javascript">

$(document).ready(function(){
 $("#youtube1").click(function(){
                $.colorbox({href:"get_a_search_started_video.php",scrolling:false,width: "90%"});
            });
});
</script>
<style>
    .grideightcontainer table a {
    color: #fff;
    font-weight: normal;
}
   .tdactive {
        background-color: #92d050;
    }
</style>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
<?php 
   if (!isset($_SESSION['user'])) {
   ?>
        
            <div class="col-xs-12 col-sm-12 dashboard-title">
                    <h3>My Dashboard</h3>
            </div> 

         <div style="width: 100%">              
            <table class="table dashboard_table secondmenu-table">
                <thead>
                    <tr>
                       <td align="center" class="tdactive"><a href="get_a_search_started.php">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">1</span>
                        </span>
                       START THE SEARCH PROCESS</a></td>                                              
                        <td align="center"><a href="consider_and_approve.php">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">2</span>
                        </span>
                        CONSIDER & APPROVE</a></td>
                        <td align="center"><a href="inspect_and_takedelivery.php">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">3</span>
                        </span>
                        INSPECT & TAKE DELIVERY</a></td>
                    </tr>
                </thead>   
            </table>
        </div>  

<div class="about-this-video"><a href="#" id="youtube1"><img src="assets/img/video189 copy.png"/> About this Step</a></div>

            <div class="col-xs-12 col-sm-12 dash-step2" style="margin-bottom: 5px;"> 
                  
            <div class="col-xs-4 col-sm-4">
                
            </div> 
            <!--<div class="col-xs-4 col-sm-4" style="text-align: center;"><a href="#" id="youtube1"><img src="images/play.png"/></a>
                <div class="breaker"></div>
                About this Step
            </div> -->
            <div class="col-xs-12 col-sm-12">
                
            </div> 
            <div class="col-xs-4 col-sm-4">

            </div>
        </div> 
        
        <?php 
   }
   ?>
<div class="dashboardmenu">
         
            <div class="row placeholders">     
                
                 <div class="col-xs-6 col-md-4 placeholder">
                    <a href="consult.php"> 
                        <img id="dashboardimage" class="img-responsive"  alt="" src="images/request.png">
                    </a>
                </div> 
                
                 <!--<div class="col-xs-6 col-md-4 placeholder">
                    <a title="Newsweek"  href="researchvehicles.php">
                        <img id="dashboardimage" class="img-responsive"  alt="" src="images/research.png">
                     </a>
                </div>-->
               
                <div class="col-xs-6 col-md-4 placeholder">
                    <a href="#">  
                      <img id="dashboardimage" class="img-responsive"  alt="" src="images/support.png">
                    </a>
                </div> 
            </div>
        </div>

        
        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">
            <!--Vehicle Specification--> 
<!--                Add  Vehicle Search Plan-->
                Describe the Vehicle you want: Vehicle Specification
                 <span style="float: right; margin-right: 10px; font-size: 16px;"><?php if (isset($_SESSION['user'])) { ?> <a href="searchplan.php" style="color:black"><img src="images/back.png"></a> <?php } ?></span>
        </h1>
        <form action="searchplansave.php" onsubmit="javascript:return validateFormOnSubmit()" method="post" name="assessform" id="form_searchplan">
            <div id="vehiclesplan">
            </div>
             <div class="grideightgrey">
                <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;"></p>
                <div class="grideight" style="width: 95%; margin-top:-5px; margin-bottom: 0px;">
                    <p><button type="button" class="med" onclick="addvehicle();">Add Another Vehicle Spec</button></p>
                    <p><button type="button"  class="med submit_form">Submit Vehicle Spec</button></p>
                    <input type="hidden" name="reseller_status" value="0" id="reseller_status" />
                    <input type="hidden" name="number" id="number" value="<?php echo $total; ?>" />
                    <div id="dialog" title=" " style="display: none;">
                        <textarea name="notetext" id="notetext"  style="resize:vertical;width:100%;height:100%"></textarea>
                    </div>
                </div>
            </div>
        </form>
        <link rel="stylesheet" href="pupup/popupwindow.css">
        <div id="pop-up-3" class="pop-up-display-content">
            <span style="font-size: 15px;font-weight: bold;">Please show me vehicles to consider that closely meet my specifications</span>
</div>
    </div><!-- end grideightgrey-->

    <style>
        .assessment_insidetd{
            background: none repeat scroll 0% 0% gray; 
            color: white;
        }
        .assessment_insidsales{
            background: none repeat scroll 0% 0% #85c11b; 
            color: white;
        }
    </style>
<?php require("teaser.php"); ?>
</div><!--end content-->


<script src="pupup/popupwindow.js"></script>

<script>
$(document).ready(function()
{
    
    //$(".submit_form").click(function()
    $(document).on("click", '.submit_form', function ()
    {
          $('#pop-up-3').popUpWindow({
            action: "open",
            buttons: [{
                text: "Yes",
                cssClass: "btn-yes",
                click: function () {
                     $('#reseller_status').val(1);
                        $("#form_searchplan").submit();
                     //this.close();
                }
            }, {
                text: "No",
                cssClass: "btn-no",
                click: function () {
                   $('#reseller_status').val(0);                  
                   $("#form_searchplan").submit();
                }
            }]
        });
       
 
    });

});
</script>

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
    <?php require("footerend.php"); ?>
