<?php require("globals.php"); ?>
<?php
    require_once(WEB_ROOT_PATH.'common/functions/string.php');
    $_SESSION['state'] = 3;
    $_SESSION['substate'] = 0;
    $_SESSION['titleadd'] = 'Order Details';

    $marketneedid = $_SESSION['marketneedid'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select q.QuoteRequestID, q.Year, q.Make, q.Model, q.Style, f.LastUpdated, q.QuoteType, f.OrderType, f.PricePoint, f.FirmQuoteID, f.LastUpdated, o.Accepted,f.ImageFile,o.WaitPeriod,f.AddDepositAmount,o.DepositReceived from quoterequests q,firmquotes f,orderfor of,orders o where o.OrderID=of.OrderID and of.FirmQuoteID=f.FirmQuoteID and f.QuoteRequestID=q.QuoteRequestID and q.MarketNeedID=".$marketneedid;
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $fquoteid[$index] = $row[0];
            $fyear[$index] = $row[1];
            $fmake[$index] = $row[2];
            $fmodel[$index] = $row[3];
            $fstyle[$index] = $row[4];
            $fupdated[$index] = $row[5];
            $fquotetype[$index] = $row[6];
            $fordertype[$index] = $row[7];
            $fprice[$index] = $row[8];
            $ffirmid[$index] = $row[9];
            $flastup[$index] = $row[10];
            $forderdate[$index] = $row[11];
            $fimage[$index] = $row[12];
            $fwaitperiod[$index] = $row[13];
            $fdepneeded[$index] = $row[14] + 250.00;
            $fdeprec[$index] = $row[15];
            $fdepreq[$index] = 1;
            $mquery = "select depositrequired from marketneeds where marketneedid=".$marketneedid;
            $mresult = mysql_query($mquery);
            if($mrow = mysql_fetch_array($mresult)) if($mrow[0] != 1) $fdepreq[$index] = 0;

            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width:300px;">View My Order Details</h1>
        <div class="grideightgrey">
<?php
    if($index < 1)
    {
        echo '<span style="color: #85c11b;">You do not have an Order placed at this time.</span>';
    }
    else
    {
        $i = 0;
        $imagefile = '';
        if(isset($fimage[$i]))
        {
            if(!file_exists($fimage[$i])) $imagefile = '';
            else $imagefile = $fimage[$i];
        }

        if(!isset($imagefile) || (strlen($imagefile) < 1))
        {
            if($fquotetype[$i] == 'Standard')
            {
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    // First fill the list of Makes...
                    mysql_select_db(DB_SERVER_DATABASE, $con);

                    $result = mysql_query("select v.ImageFile from vehicles v, makes m where m.makeid=v.makeid and m.name='".$fmake[$i]."' and v.year=".$fyear[$i]." and v.model='".$fmodel[$i]."' and v.style='".$fstyle[$i]."'");
                    if($result && $row = mysql_fetch_array($result))
                    {
                        if(strlen($row[0]) > 0) $imagefile = $row[0];
                    }
                    else $imagefile = '';
                    mysql_close($con);
                }
                else $imagefile = '';
            }
        }

        if(isset($imagefile) && (strlen($imagefile) > 0))
        {
            $max_width = 500;
            $max_height = 500;
            echo '<center>';
            echo '<img id="vehimage" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" style="margin-top:10px;" />';
            echo '</center>';
            echo '<br />';
            echo '<br />';
        }
        //else echo '** No Image Available **';

        echo '<table border="0" width="550" cellspacing="5" style="margin-left:35px; font-size:14px">';
        echo '<tr>';
        echo '<td><strong>Order Type</strong></td>';
        echo '<td><span style="color: #85c11b;">'.$fordertype[$i].'</span></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td><strong>Date Ordered</strong></td>';
        echo '<td>'.date_at_timezone('m/d/Y', 'EST', $forderdate[$i]).'</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td><strong>Exclusive Search Period</strong></td>';
        echo '<td>'.$fwaitperiod[$i].' Days</td>';
        echo '</tr>';

        $sec = time_diff(date_at_timezone('Y-m-d H:i:s','EST'), date_at_timezone('Y-m-d H:i:s', 'EST', $forderdate[$i]));
        $days = $fwaitperiod[$i] - round($sec/(60*60*24));
        echo '<tr>';
        echo '<td><strong>Search Period Status</strong></td>';
        if($days > 0)
            echo '<td>'.$days.' Days Remaining</td>';
        else
            echo '<td>Day by Day (Order may be cancelled upon request)</td>';
        echo '</tr>';

        $count = count($forderdate);
        echo '<tr>';
        $plural = pluralize_noun($count, "Current Market Stud", "ies", "y");
        echo "<td><strong>$plural attached</strong></td>";
        echo '<td>';
        for($i=0;$i<$count;$i++)
        {
            echo '<a href="quotereceived.php?QuoteID='.$fquoteid[$i].'">';
            echo $fyear[$i].' '.$fmake[$i].' '.$fmodel[$i].' '.$fstyle[$i];
            echo '</a><br/>';
        }
        echo '</td>';
        echo '</tr>';
        if($fdepreq[0] == 1)
        {
            echo '<tr>';
            echo '<td><strong>Deposit Amount</strong></td>';
            echo '<td>$'.number_format($fdepneeded[0],2).'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td><strong>Deposit Received</strong></td>';
            echo '<td><span style="color: #85c11b;"><strong>$'.number_format($fdeprec[0],2).'</strong></span></td>';
            echo '</tr>';
        }
        echo '</table>';
    }
?>
        </div><!--end grideightgrey-->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
