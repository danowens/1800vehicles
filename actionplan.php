<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 7;
    $_SESSION['titleadd'] = "Action Plans";

    $userid = $_SESSION['userid'];
    $uadmin = getuserprofile($userid, 'Administrator');
    $uterr = getuserprofile($userid, 'Teritory Admin');
    $ufran = getuserprofile($userid, 'Franchisee');
    $uops = getuserprofile($userid, 'Operations Manager');

    if(isset($_REQUEST['FranchiseeID']) && ($_REQUEST['FranchiseeID'] != -1))
    {
        $franid = $_REQUEST['FranchiseeID'];

        // Verify this user has access to this page...
        if(isinfranchisee($franid, $userid) == 'false')
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x020507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
        if(!(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true')))
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x020507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
    }
    else
    {
        $franid = -1;
        if(!(($uadmin == 'true') || ($uterr == 'true')))
        {
            $_SESSION['ShowError'] = 'Internal Error - 0x020507';
            header('Location: mydashboard.php#admintab');
            exit();
        }
    }

    $erroronsave = 'false';
    if(isset($_REQUEST['PostBack']))
    {
        // Time to add it to the database...
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            if(isset($_REQUEST['ActionPlanID']) && ($_REQUEST['ActionPlanID'] > 0))
            {
                $query = "update actionplans set Name='".escapestr($_REQUEST['PlanName'])."', Visible=1, ";
                if($franid == -1) $query .= "CorporateAdded=1, FranchiseeID=NULL";
                else $query .= "CorporateAdded=0, FranchiseeID=".$franid;
                $query .= " where ActionPlanID=".$_REQUEST['ActionPlanID'];
                if(!mysql_query($query, $con)) $erroronsave = 'Could Not Update Action Plan';
            }
            else
            {
                $query = "insert into actionplans (Name, Visible, CorporateAdded, FranchiseeID) values ('".escapestr($_REQUEST['PlanName'])."',1,";
                if($franid == -1) $query .= "1, NULL";
                else $query .= "0, ".$franid;
                $query .= ")";
                if(!mysql_query($query, $con)) $erroronsave = 'Could Not Add Action Plan';
                else $newplanid = mysql_insert_id($con);
            }

            mysql_close($con);
        }
    }

    if(isset($_REQUEST['DeleteIt']) && isset($_REQUEST['ActionPlanID']))
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $query = "delete from actionplans where ActionPlanID=".$_REQUEST['ActionPlanID'];
            if(!mysql_query($query, $con)) $erroronsave = 'Could Not Delete the Action Plan';

            mysql_close($con);
        }
        header('Location: '.$_REQUEST['ReturnTo']);
        exit();
    }

    if(isset($newplanid)) $actionplanid = $newplanid;
    elseif(isset($_REQUEST['ActionPlanID'])) $actionplanid = $_REQUEST['ActionPlanID'];
    else $actionplanid = -1;

    if($actionplanid > 0)
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $query = "select Name from actionplans where ActionPlanID=".$actionplanid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $planname = $row[0];

                $query = "select Name, CustSee, DisplayOrder, ActionCategoryID from actioncategories where ActionPlanID=".$actionplanid." order by DisplayOrder asc";
                $result = mysql_query($query, $con);
                $index = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    $acname[$index] = $row[0];
                    $accustsee[$index] = $row[1];
                    $acdisporder[$index] = $row[2];

                    // $row[3] is ActionCategoryID to get the count with...
                    $cquery = "select count(*) from actionitems where ActionCategoryID=".$row[3];
                    $cresult = mysql_query($cquery, $con);
                    if($crow = mysql_fetch_array($cresult)) $acitemc[$index] = $crow[0];
                    else $acitemc[$index] = 0;

                    $index++;
                }
            }

            mysql_close($con);
        }
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function trimAll(sString)
    {
        while(sString.substring(0,1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while(sString.substring(sString.length-1, sString.length) == ' ')
        {
            sString = sString.substring(0,sString.length-1);
        }
        return sString;
    }

    function validateFormOnSubmit()
    {
        var errorstr = "";

        var ve = document.getElementById("PlanName");
        var tString = trimAll(ve.value);
        if(tString.length < 1)
        {
            ve.style.background = '#ffcccc';
            errorstr += "Plan Name must be entered."+'\n';
            ve.focus();
        }
        else ve.style.background = 'white';

        if(errorstr != "")
        {
            alert("Errors that must be corrected:"+'\n'+errorstr+'\n');
            return false;
        }
        return true;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>
<div id="content">
    <div class="grideightcontainer">
        <p style="font-size: 14px; margin-top: 0pt;"><a href="mydashboard.php#admintab">Go to Dashboard</a></p>
        <h1 class="subhead">Action Plan</h1>
        <div class="grideightgrey" style="color: rgb(20, 44, 60);">
<?php
    if($erroronsave != 'false')
    {
        echo $erroronsave.'<br/>';
        echo $query;
    }
?>
            <form action="actionplan.php" onsubmit="javascript:return validateFormOnSubmit();" method="post">
                <input type="hidden" value="<?php echo $franid; ?>" name="FranchiseeID" />
                <input type="hidden" value="<?php echo $actionplanid; ?>" name="ActionPlanID" />
                <input type="hidden" value="true" name="PostBack" />
                <p style="font-size: 18px; font-weight: bold; margin-top: 0pt;">Action Plan</p>
                <table style="margin-left: 5px;" align="left" border="0" cellpadding="5" width="430">
                    <tbody>
                        <tr>
                            <td width="200"><strong>Plan Name</strong></td>
                            <td width="230">
                                <input id="PlanName" name="PlanName" size="50" maxlength="50" type="text" value="<?php if(isset($planname)) echo $planname; ?>" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br clear="all" />
                <br />
                <span style="float: right;">
<?php
    if($actionplanid == -1)
    {
        echo '<button type="submit" value="" class="med"><nobr>SAVE NEW ACTION PLAN</nobr></button>';
    }
    else
    {
        echo '<button type="submit" value="" class="med"><nobr>SAVE ACTION PLAN NAME</nobr></button>';
    }
?>
                </span>
                <br clear="all" />
                <br />
            </form>
        </div><!--grideightgrey-->
<?php
    if($actionplanid > 0)
    {
        echo '<h4 class="subhead">Categories:</h4>';
        $count = count($acname);
        if($count > 0)
        {
            echo '<div class="grideightgrey" style="color: rgb(20, 44, 60);">';
            echo '<table width="500" border="0" cellpadding="3">';
            echo '<tr style="color:#85c11b; font-size:15px;">';
            echo '<td width="50" align="center"><strong>#</strong></td>';
            echo '<td width="200" align="center"><strong>Customer Can See?</strong></td>';
            echo '<td width="200" align="center"><strong>Name</strong></td>';
            echo '<td width="50" align="center"><strong>Items</strong></td>';
            echo '</tr>';
            for($i=0; $i < $count; $i++)
            {
                echo '<tr style="color:#000000; font-size:15px;">';
                echo '<td align="center">'.$acdisporder[$i].'</td>';
                if($accustsee[$i] == 1) echo '<td align="center">Yes</td>';
                else echo '<td align="center">No</td>';
                echo '<td align="center">'.$acname[$i].'</td>';
                echo '<td align="center">'.$acitemc[$i].'</td>';
                echo '</tr>';
            }
            echo '</table>';
            echo '<br clear="all" />';
            echo '</div><!--grideightgrey-->';
        }
        else
        {
            echo '<div class="grideightgrey" style="color: rgb(20, 44, 60);">';
            echo '<p style="font-size: 14px; margin-top: 0pt;">There are no categories to Display</p>';
            echo '</div><!--grideightgrey-->';
        }
        echo '<form action="actioncategories.php" method="post">';
        echo '<input type="hidden" value="'.$franid.'" name="FranchiseeID" />';
        echo '<input type="hidden" value="'.$actionplanid.'" name="ActionPlanID" />';
        echo '<button type="submit" value="" class="med"><nobr>EDIT CATEGORIES</nobr></button>';
        echo '</form>';
    }
?>
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
