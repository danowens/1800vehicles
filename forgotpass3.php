<?php require("globals.php"); ?>
<?php
require_once("common/functions/User.php");
require_once("common/functions/DB.php");
require_once("common/functions/emailfunctions.php");
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 5;
    $_SESSION['titleadd'] = 'Lost Password';

    $loginname = $_REQUEST['loginname'];
    $answer    = $_REQUEST['answer'];
    $db        = DB::init();
    $user      = User::getUserByLogin($db, $loginname);

    $email_sent = false;
    if (!empty($user) && ($user['PWAnswer1'] == $answer)) {
        $body = "
Hello {$user['FirstName']},
Copy and paste (or click) the following link into your browser to complete the password recovery process:
http://1800vehicles.com/changepass.php?uid={$user['UserID']}&genlink={$user['Password']}
";
        $subject = "1-800vehicles.com Password Reset";
        $email_sent = sendemail($user['Email'], $subject, $body);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <form action="forgotpass3.php" onsubmit="return validateFormOnSubmit(this)" method="post">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 260px;">Forgot your password?</h1>
        <div class="grideightgrey">
            <div class="grideight" style="width:580px;">
<?php if ($email_sent) { ?>
<p class="blackfourteen" style="margin-top:0; font-size: 13px; color:#142c3c;"><strong>A link has been sent to your email address.</strong></p><p class="blackfourteen" style="padding-bottom: 0px; font-size: 13px; color:#142c3c;"><strong>Please follow the link to reset your password.</strong></p>			</div><!-- endgrideight -->
<?php } else { ?>
<p class="blackfourteen" style="margin-top:0; font-size: 13px; color:#142c3c;"><strong>The answer you supplied did not match what was stored for your login.</strong></p><p class="blackfourteen" style="font-size: 13px; color:#142c3c;"><strong>Please contact your sales representative for additional help or <a href="forgotpass2.php?loginname=<?php echo htmlspecialchars($user['UserID']); ?>">try again</a>.</strong></p>
<?php } ?>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
