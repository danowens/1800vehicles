<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 3;
    $_SESSION['titleadd'] = 'Research by Price';

    $types[1] = 'Auto';
    $types[2] = 'SUV';
    $types[3] = 'MiniVan';

    // See if Make was selected already and this is a refresh...
    $type = $_REQUEST['typeitem'];
    if(!isset($type) || strlen($type) <= 0)
    {
        $type = $types[1];
    }

    switch($type)
    {
        case 'Auto':
            $styles[1] = '4 Door Sedan';
            $styles[2] = '2 Door Coupe';
            $styles[3] = '2 Door Convertible';
            $styles[4] = '3 Door Hatchback';
            $styles[5] = '5 Door Hatchback';
            $styles[6] = '5 Door Wagon';
			
			$sizes[1] = 'Sub Compact';
			$sizes[2] = 'Compact';
			$sizes[3] = 'Mid Size';
			$sizes[4] = 'Full Size';
            break;
        case 'SUV':
            $styles[1] = '2 Door 2WD';
            $styles[2] = '2 Door 4WD';
            $styles[3] = '4 Door 2WD';
            $styles[4] = '4 Door 4WD';
			
			$sizes[1] = 'Small';
			$sizes[2] = 'Mid Size';
			$sizes[3] = 'Full Size';
            break;
        case 'MiniVan':
            break;
    }


    // See if Style was selected already and this is a refresh...
    $style = $_REQUEST['styleitem'];
    if((!isset($style) || strlen($style) <= 0) && $index > 0)
    {
        $style = $styles[1];
    }

    // Check what was in fromselect...
    $from = $_REQUEST['fromselect'];
    if(!isset($from))
    {
        $from = 'Any';
    }

    // Fill the beginning prices...
    $pstart[1] = '0';
    $pend[1] = '0';
    $index = 2;
    for($i = 1; $i <= 5; $i++)
    {
        $pstart[$index] = 5000 + (($i - 1) * 1000);
        $pend[$index] = 5000 + (($i - 1) * 1000);
        $index++;
    }
    for($i = 1; $i <= 15; $i++)
    {
        $pstart[$index] = 10000 + (($i - 1) * 2000);
        $pend[$index] = 10000 + (($i - 1) * 2000);
        $index++;
    }
    for($i = 1; $i <= 13; $i++)
    {
        $pstart[$index] = 40000 + (($i - 1) * 5000);
        $pend[$index] = 40000 + (($i - 1) * 5000);
        $index++;
    }
    $pstart[$index] = 'Any';
    $pend[$index] = 'Any';

    // See if Prices are selected already and this is a refresh...
    $startprice = $_REQUEST['startprice'];
    if(!isset($startprice))
    {
        $startprice = $pstart[$index];
    }
    $maxprice = $_REQUEST['maxprice'];
    if(!isset($maxprice))
    {
        $maxprice = $pend[$index];
    }
?>
<?php require("headerstart.php"); ?>
<script language=JavaScript>
    function itemchanged()
    {
        for (var i=0; i < document.priceform.fromselect.length; i++)
        {
            if (document.priceform.fromselect[i].checked)
            {
                var vfrom = document.priceform.fromselect[i].value;
            }
        }
        var vtype = document.getElementById("typelist");
        var vstyle = document.getElementById("stylelist");
        var vstart = document.getElementById("pricestart");
        var vmax = document.getElementById("priceend");
       if(vstyle==null ){
        self.location='researchprice.php?' +
            'typeitem=' + vtype.options[vtype.selectedIndex].value +
            '&fromselect=' + vfrom +
            '&styleitem='+
            '&startprice=' + vstart.options[vstart.selectedIndex].value +
            '&maxprice=' + vmax.options[vmax.selectedIndex].value;
       }else{
           self.location='researchprice.php?' +
            'typeitem=' + vtype.options[vtype.selectedIndex].value +
            '&fromselect=' + vfrom +
            '&styleitem=' + vstyle.options[vstyle.selectedIndex].value +
            '&startprice=' + vstart.options[vstart.selectedIndex].value +
            '&maxprice=' + vmax.options[vmax.selectedIndex].value;
       }
    }
</script>

<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
       <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Research by Price Ranges</h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0;"><a href="researchvehicleoptions.php">Go back to Research Vehicles</a></p>
            <div class="grideight">
                <form action="priceresults.php" method="get" name="priceform">
                    
                      <table class="table">
						<tr>
							<td style="width:30%"><strong>Vehicle Type</strong></td>
							<td style="width:40%">
 <select name="typelist" id="typelist" onchange="javascript:itemchanged()">
						<?php
							$count = count($types);
							for($i = 1; $i <= $count; $i++)
							{
								echo '<option value="'.$types[$i].'"';
								if(isset($type) && $types[$i]==$type) echo 'selected="selected"';
								echo '>'.$types[$i].'</option>';
							}
						?>
                    </select>
							</td>
						</tr>
                                                <tr>
							<td style="width:30%"><strong>Hybrid</strong></td>
							<td style="width:40%">
                                            <select name="hybridlist" id="hybridlist">
						<option value="Yes">Yes</option>
						<option value="No" selected="selected">No</option>
						<option value="Either">Flexible</option>
                                                  </select>
							</td>
						</tr>
                                         
                                                
                                                
                                                 <tr>
							<td style="width:30%"><strong>From</strong></td>
							<td style="width:40%">
                                                            <!--<input name="fromselect" type="radio" value="Any" <?php if($from=='Any') echo 'checked '; ?>/> From &nbsp;&nbsp;-->
                           <input name="fromselect" type="radio" value="Import" <?php if($from=='Import' || $from=='Any') echo 'checked '; ?>/> Import &nbsp;&nbsp;
                           <input name="fromselect" type="radio" value="Domestic" <?php if($from=='Domestic') echo 'checked '; ?>/> Domestic &nbsp;&nbsp;
                           
							</td>
						</tr>
                                                
                                                
                                                
                                                   <?php if($type!='MiniVan'){?>
                                                 <tr>
							<td style="width:30%"><strong>Size</strong></td>
							<td style="width:40%">
		
                              <select name="sizelist" id="sizelist" onchange="javascript:itemchanged()">
						<?php
							$count = count($sizes);
							for($i = 1; $i <= $count; $i++)
							{
								echo '<option value="'.$sizes[$i].'"';
								if(isset($style) && $sizes[$i]==$style) echo 'selected="selected"';
								echo '>'.$sizes[$i].'</option>';
							}
						?>
                    </select>
							</td>
						</tr>
                                                
                                                  <tr>
							<td style="width:30%"><strong>Class</strong></td>
							<td style="width:40%">
		
                            <select name="stylelist" id="stylelist" onchange="javascript:itemchanged()">
						<?php
							$count = count($styles);
							for($i = 1; $i <= $count; $i++)
							{
								echo '<option value="'.$styles[$i].'"';
								if(isset($style) && $styles[$i]==$style) echo 'selected="selected"';
								echo '>'.$styles[$i].'</option>';
							}
						?>
                    </select>
							</td>
						</tr>
                                                <?php }?>
                                                 <tr>
							<td style="width:30%"><strong>Price Range</strong></td>
							<td style="width:40%">
		
                                                            <select name="pricestart" id="pricestart" style=" width: 35%;">
						<?php
							$count = count($pstart);
							for($i = 1; $i <= $count; $i++)
							{
								echo '<option value="'.$pstart[$i].'"';
								if(isset($startprice) && $pstart[$i]==$startprice) echo 'selected="selected"';
								if($pstart[$i] == 'Any') 
								{
									// default range to 'Any to Any'
									if (!isset($startprice)) echo 'selected="selected"';
									echo '>'.$pstart[$i].'</option>';
								}
								else echo '>$'.number_format($pstart[$i]).'</option>';
							}
						?>
                    </select>
                    to
                    <select name="priceend" id="priceend"  style=" width: 35%;">
						<?php
							$count = count($pend);
							for($i = 1; $i <= $count; $i++)
							{
								echo '<option value="'.$pend[$i].'"';
								if(isset($maxprice) && $pend[$i]==$maxprice) echo 'selected="selected"';
								if($pend[$i] == 'Any') echo '>'.$pend[$i].'</option>';
								else echo '>$'.number_format($pend[$i]).'</option>';
							}
						?>
                    </select>
							</td>
						</tr>
                                                <tr>
                                                    <td colspan="2">
                                                       <button type="submit" value="" class="med">SEARCH</button>
                                                    </td>
                                                </tr>
                        </table>                    
                </form>
            </div> <!-- grideight -->
        </div> <!-- grideightgrey  -->
    </div><!-- grid eight container -->
<?php
    $_SESSION['hideteaser5'] = 'true';
    require("teaser.php");
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>