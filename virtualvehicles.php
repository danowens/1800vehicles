<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = "All Virtual Vehicles";
    $_SESSION['onloadfunction'] = 'initform()';

?>
<?php require("headerstart.php"); ?>
<script language="javascript" type="text/javascript">
<!--
    var yearhttpObject = null;
    var makehttpObject = null;
    var modelhttpObject = null;

    // Get the HTTP Object
    function getHTTPObject(){
        var xmlHttp = null;
        try
        {
            // Firefox, Opera, Safari
            xmlHttp = new XMLHttpRequest();
        }
        catch (e)
        {
            // Internet Explorer
            try
            {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e)
            {
                try
                {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e)
                {
                    alert('Your browser must be Firefox, Safari, Opera or IE 5 or higher');
                }
            }
        }
        return xmlHttp;
    }

    function initform()
    {
        yearchanged();
    }

    function yearchanged()
    {
        var vyearlist = document.getElementById("yearlist");

        yearhttpObject = getHTTPObject();
        if (yearhttpObject != null)
        {
            yearhttpObject.open("GET", "ajaxgetmakes.php?Year=" + vyearlist.options[vyearlist.selectedIndex].value, true);
            yearhttpObject.send(null);
            yearhttpObject.onreadystatechange = makesReturned;
            document.getElementById('makelist').style.cursor = "wait";
        }
    }

    function makesReturned()
    {
        if(yearhttpObject.readyState == 4)
        {
            var vmakelist = document.getElementById("makelist");

            // Clear the list out...
            while (vmakelist.options.length)  vmakelist.remove(0);

            var vlastmake = document.getElementById("LastMakeUsed");
            var found = 0;

            // Now add the list from the returned results...
            names = yearhttpObject.responseText.split(';');
            var index = 0;
            var current = 0;
            while (index < (names.length-1))
            {
                newOpt = document.createElement("option");
                newOpt.value = names[index];
                newOpt.text = names[index+1];
                if(vlastmake.value == newOpt.value) found = current;
                vmakelist.add(newOpt);
                index += 2;
                current++;
            }
            vmakelist.options[found].selected = true;
            vmakelist.style.cursor = "default";
            makechanged();
        }
    }

    function makechanged()
    {
        var vyearlist = document.getElementById("yearlist");
        var vmakelist = document.getElementById("makelist");

        makehttpObject = getHTTPObject();
        if (makehttpObject != null)
        {
            makehttpObject.open("GET", "ajaxgetmodels.php?Year=" + vyearlist.options[vyearlist.selectedIndex].value + "&Make=" + vmakelist.options[vmakelist.selectedIndex].value, true);
            makehttpObject.send(null);
            makehttpObject.onreadystatechange = modelsReturned;
            document.getElementById('modellist').style.cursor = "wait";
        }
    }

    function modelsReturned()
    {
        if(makehttpObject.readyState == 4)
        {
            var vmodellist = document.getElementById("modellist");

            // Clear the list out...
            while (vmodellist.options.length)  vmodellist.remove(0);

            var vlastmodel = document.getElementById("LastModelUsed");
            var found = 0;

            // Now add the list from the returned results...
            names = makehttpObject.responseText.split(';');
            var index = 0;
            var current = 0;
            while (index < (names.length-1))
            {
                newOpt = document.createElement("option");
                newOpt.value = names[index];
                newOpt.text = names[index];
                if(vlastmodel.value == newOpt.value) found = current;
                vmodellist.add(newOpt);
                index++;
                current++;
            }
            vmodellist.options[found].selected = true;
            vmodellist.style.cursor = "default";
            modelchanged();
        }
    }

    function modelchanged()
    {
        var vyearlist = document.getElementById("yearlist");
        var vmakelist = document.getElementById("makelist");
        var vmodellist = document.getElementById("modellist");

        modelhttpObject = getHTTPObject();
        if (modelhttpObject != null)
        {
            modelhttpObject.open("GET", "ajaxgetstyles.php?Year=" + vyearlist.options[vyearlist.selectedIndex].value + "&Make=" + vmakelist.options[vmakelist.selectedIndex].value + "&Model=" + vmodellist.options[vmodellist.selectedIndex].value, true);
            modelhttpObject.send(null);
            modelhttpObject.onreadystatechange = stylesReturned;
            document.getElementById('stylelist').style.cursor = "wait";
        }
    }

    function stylesReturned()
    {
        if(modelhttpObject.readyState == 4)
        {
            var vstylelist = document.getElementById("stylelist");

            // Clear the list out...
            while (vstylelist.options.length)  vstylelist.remove(0);

            var vlaststyle = document.getElementById("LastStyleUsed");
            var found = 0;

            // Now add the list from the returned results...
            names = modelhttpObject.responseText.split(';');
            var index = 0;
            var current = 0;
            while (index < (names.length-1))
            {
                newOpt = document.createElement("option");
                newOpt.value = names[index+1] + ';' + names[index];
                newOpt.text = names[index];
                if(vlaststyle.value == newOpt.value) found = current;
                vstylelist.add(newOpt);
                index += 2;
                current++;
            }
            vstylelist.options[found].selected = true;
            vstylelist.style.cursor = "default";
            stylechanged();
        }
    }

    function stylechanged()
    {
        //var vyearlist = document.getElementById("yearlist");
        //var vmakelist = document.getElementById("makelist");
        //var vmodellist = document.getElementById("modellist");
        var vstylelist = document.getElementById("stylelist");
        var vlaststyle = document.getElementById("LastStyleUsed");
        vlaststyle.value = vstylelist.options[vstylelist.selectedIndex].value;

        // This is here in case we want to display details of the selected item on this page...
    }

    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) || 
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }

    var milenpricehttpObject = null;

    function bumpallmilenprice()
    {
        milenpricehttpObject = getHTTPObject();
        if (milenpricehttpObject != null)
        {
            milenpricehttpObject.open("GET", "ajaxbumpallmilenprice.php", true);
            milenpricehttpObject.send(null);
            milenpricehttpObject.onreadystatechange = bumpallmilenpriceReturned;
            document.getElementById('bumpallmilenpriceres').style.cursor = "wait";
        }
    }

    function bumpallmilenpriceReturned()
    {
        if(milenpricehttpObject.readyState == 4)
        {
            var vbumpres = document.getElementById("bumpallmilenpriceres");

            // Now add the returned result...
            vbumpres.value = milenpricehttpObject.responseText;
            vbumpres.style.cursor = "default";
        }
    }

    var copycarshttpObject = null;

    function copycars()
    {
        //alert("In Copy Cars");
        var vfrom = document.getElementById("yearfrom");
        //alert(vfrom.value);
        var vto = document.getElementById("yearto");
        //alert(vto.value);

        copycarshttpObject = getHTTPObject();
        if (copycarshttpObject != null)
        {
            //alert("Calling Open");
            copycarshttpObject.open("GET", "ajaxcopycars.php?From=" + vfrom.value + "&To=" + vto.value, true);
            copycarshttpObject.send(null);
            copycarshttpObject.onreadystatechange = copycarsReturned;
            document.getElementById('copycarsres').style.cursor = "wait";
        }
    }

    function copycarsReturned()
    {
        //alert("In Copy Cars Returned");
        if(copycarshttpObject.readyState == 4)
        {
            var vbumpres = document.getElementById("copycarsres");

            // Now add the returned result...
            vbumpres.value = copycarshttpObject.responseText;
            //alert(copycarshttpObject.responseText);
            vbumpres.style.cursor = "default";
        }
    }

    var milesuphttpObject = null;

    function bumpallmilesup()
    {
        var vbump = document.getElementById("bumpmilesupamt");

        milesuphttpObject = getHTTPObject();
        if (milesuphttpObject != null)
        {
            milesuphttpObject.open("GET", "ajaxbumpallmiles.php?Amount=" + vbump.value, true);
            milesuphttpObject.send(null);
            milesuphttpObject.onreadystatechange = bumpallmilesupReturned;
            document.getElementById('bumpallmilesupres').style.cursor = "wait";
        }
    }

    function bumpallmilesupReturned()
    {
        if(milesuphttpObject.readyState == 4)
        {
            var vbumpres = document.getElementById("bumpallmilesupres");

            // Now add the returned result...
            vbumpres.value = milesuphttpObject.responseText;
            vbumpres.style.cursor = "default";
        }
    }

    var milesdownhttpObject = null;

    function bumpallmilesdown()
    {
        var vbump = document.getElementById("bumpmilesdownamt");

        milesdownhttpObject = getHTTPObject();
        if (milesdownhttpObject != null)
        {
            milesdownhttpObject.open("GET", "ajaxbumpallmiles.php?Amount=-" + vbump.value, true);
            milesdownhttpObject.send(null);
            milesdownhttpObject.onreadystatechange = bumpallmilesdownReturned;
            document.getElementById('bumpallmilesdownres').style.cursor = "wait";
        }
    }

    function bumpallmilesdownReturned()
    {
        if(milesdownhttpObject.readyState == 4)
        {
            var vbumpres = document.getElementById("bumpallmilesdownres");

            // Now add the returned result...
            vbumpres.value = milesdownhttpObject.responseText;
            vbumpres.style.cursor = "default";
        }
    }

    var pricesuphttpObject = null;

    function bumpallpricesup()
    {
        var vbump = document.getElementById("bumppricesupamt");

        pricesuphttpObject = getHTTPObject();
        if (pricesuphttpObject != null)
        {
            pricesuphttpObject.open("GET", "ajaxbumpallprices.php?Amount=" + vbump.value, true);
            pricesuphttpObject.send(null);
            pricesuphttpObject.onreadystatechange = bumpallpricesupReturned;
            document.getElementById('bumpallpricesupres').style.cursor = "wait";
        }
    }

    function bumpallpricesupReturned()
    {
        if(pricesuphttpObject.readyState == 4)
        {
            var vbumpres = document.getElementById("bumpallpricesupres");

            // Now add the returned result...
            vbumpres.value = pricesuphttpObject.responseText;
            vbumpres.style.cursor = "default";
        }
    }

    var pricesdownhttpObject = null;

    function bumpallpricesdown()
    {
        var vbump = document.getElementById("bumppricesdownamt");

        pricesdownhttpObject = getHTTPObject();
        if (pricesdownhttpObject != null)
        {
            pricesdownhttpObject.open("GET", "ajaxbumpallprices.php?Amount=-" + vbump.value, true);
            pricesdownhttpObject.send(null);
            pricesdownhttpObject.onreadystatechange = bumpallpricesdownReturned;
            document.getElementById('bumpallpricesdownres').style.cursor = "wait";
        }
    }

    function bumpallpricesdownReturned()
    {
        if(pricesdownhttpObject.readyState == 4)
        {
            var vbumpres = document.getElementById("bumpallpricesdownres");

            // Now add the returned result...
            vbumpres.value = pricesdownhttpObject.responseText;
            vbumpres.style.cursor = "default";
        }
    }
//-->
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width:275px;">All Virtual Vehicles</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: 0px;">
                <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
                <br/>
                <!--form action="allvehicleedit.php" method="post">
                    <input type="hidden" name="AddEditType" value="AddNew" />
                    <button type="submit" value="" class="med">Add New Vehicle</button>
                </form>
                <br/-->
                <h4 class="subhead">Adjust Mile and Price</h4>
                <table border="0" width="400" cellspacing="10" style="margin-left:-10px;">
                    <tr valign="baseline">
                        <td width="350">Adjust ALL Vehicles Mileage and Price by formulas based on year and blackbook amount.</td>
                        <td width="50"><button type="button" value="bumpmilenprice" onclick="javascript:bumpallmilenprice();" class="med">Run</button></td>
                    </tr>
                    <tr valign="baseline">
                        <td colspan="2"><textarea id="bumpallmilenpriceres" readonly="readonly" rows="2" cols="72"></textarea></td>
                    </tr>
                </table>
                <br/>
                <h4 class="subhead">Create Vehicle Copies</h4>
                <table border="0" width="400" cellspacing="10" style="margin-left:-10px;">
                    <tr valign="baseline">
                        <td width="350">From Year: <input style="width: 50px;" type="text" id="yearfrom" name="yearfrom" maxlength="6" onkeypress="javascript:return numbersonly(event);"/>
                            To Year: <input style="width: 50px;" type="text" id="yearto" name="yearto" maxlength="6" onkeypress="javascript:return numbersonly(event);"/></td>
                        <td width="50"><button type="button" value="copycars" onclick="javascript:copycars();" class="med">Make Copy</button></td>
                    </tr>
                    <tr valign="baseline">
                        <td colspan="2"><textarea id="copycarsres" readonly="readonly" rows="2" cols="72"></textarea></td>
                    </tr>
                </table>
                <br/>
                <h4 class="subhead">Adjust Miles</h4>
                <table border="0" width="400" cellspacing="10" style="margin-left:-10px;">
                    <tr valign="baseline">
                        <td width="350">Bump up ALL Vehicles by: <input style="width: 200px;" type="text" id="bumpmilesupamt" name="bumpmilesupamt" maxlength="6" onkeypress="javascript:return numbersonly(event);"/></td>
                        <td width="50"><button type="button" value="bumpallup" onclick="javascript:bumpallmilesup();" class="med">Run</button></td>
                    </tr>
                    <tr valign="baseline">
                        <td colspan="2"><textarea id="bumpallmilesupres" readonly="readonly" rows="2" cols="72"></textarea></td>
                    </tr>
                </table>
                <br/>
                <table border="0" width="400" cellspacing="10" style="margin-left:-10px;">
                    <tr valign="baseline">
                        <td width="350">Bump down ALL Vehicles by: <input style="width: 200px;" type="text" id="bumpmilesdownamt" name="bumpmilesdownamt" maxlength="6" onkeypress="javascript:return numbersonly(event);"/></td>
                        <td width="50"><button type="button" value="bumpalldown" onclick="javascript:bumpallmilesdown();" class="med">Run</button></td>
                    </tr>
                    <tr valign="baseline">
                        <td colspan="2"><textarea id="bumpallmilesdownres" readonly="readonly" rows="2" cols="72"></textarea></td>
                    </tr>
                </table>
                <br/>
                <h4 class="subhead">Adjust Prices</h4>
                <table border="0" width="400" cellspacing="10" style="margin-left:-10px;">
                    <tr valign="baseline">
                        <td width="350">Bump up ALL Vehicles by: <input style="width: 200px;" type="text" id="bumppricesupamt" name="bumppricesupamt" maxlength="6" onkeypress="javascript:return numbersonly(event);"/></td>
                        <td width="50"><button type="button" value="bumpallup" onclick="javascript:bumpallpricesup();" class="med">Run</button></td>
                    </tr>
                    <tr valign="baseline">
                        <td colspan="2"><textarea id="bumpallpricesupres" readonly="readonly" rows="2" cols="72"></textarea></td>
                    </tr>
                </table>
                <br/>
                <table border="0" width="400" cellspacing="10" style="margin-left:-10px;">
                    <tr valign="baseline">
                        <td width="350">Bump down ALL Vehicles by: <input style="width: 200px;" type="text" id="bumppricesdownamt" name="bumppricesdownamt" maxlength="6" onkeypress="javascript:return numbersonly(event);"/></td>
                        <td width="50"><button type="button" value="bumpalldown" onclick="javascript:bumpallpricesdown();" class="med">Run</button></td>
                    </tr>
                    <tr valign="baseline">
                        <td colspan="2"><textarea id="bumpallpricesdownres" readonly="readonly" rows="2" cols="72"></textarea></td>
                    </tr>
                </table>
                <br/>
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
