<?php require("globals.php"); ?>
<?php
    require_once('common/functions/globalfunctions.php');
    require_once('common/functions/imagefunctions.php');

    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 8;
    $_SESSION['titleadd'] = 'My Account';

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];

    // See what was passed in...
    $fname = $_POST['firstname'];
    $lname = $_POST['lastname'];
    $zip = $_POST["zip"];
    $email = $_POST["email"];
    $e2p = $_POST["e2p"];
    $homephone = $_POST["homephone"];
    $cellphone = $_POST["cellphone"];
    $celltext = $_POST["celltext"];
    $workphone = $_POST["workphone"];
    $workext = $_POST["workext"];
    $addphone1 = $_POST["addphone1"];
    $addphone1ext = $_POST["addphone1ext"];
    $addphone1type = $_POST["addphone1type"];
    $addphone1text = $_POST["addphone1text"];
    $addphone2 = $_POST["addphone2"];
    $addphone2ext = $_POST["addphone2ext"];
    $addphone2type = $_POST["addphone2type"];
    $addphone2text = $_POST["addphone2text"];
    $faxnum = $_POST["faxnum"];
    $loginname = $_POST["loginname"];
    $useemail = $_POST["useemail"];
    //$password = $_POST["password"];
    $rp = $_POST["rp"];
    $question1 = $_POST["question1"];
    $answer1 = $_POST["answer1"];
    $address1 = $_POST["address1"];
    $address2 = $_POST["address2"];
    $city = $_POST["city"];
    $thestate = $_POST["state"];
    $email2 = $_POST["email2"];
    $email2type = $_POST["email2type"];
    $e2p2 = $_POST["e2p2"];
    $email3 = $_POST["email3"];
    $email3type = $_POST["email3type"];
    $e2p3 = $_POST["e2p3"];
    $referred = $_POST["referred"];
    $repname = $_POST["repname"];
    $company = $_POST["company"];
    $photoname = $_POST["photoname"];
    $repcall = $_POST["repcall"];
    $franchise = $_POST["franchise"];

    $emailexists = 'false';
    $loginexists = 'false';
    $saveerror = 'false';
    $fileerror = 'false';

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);
        mysql_query("begin", $con);

        // Update the user in the database...
        $query = "update users Set Firstname = '".ucwords(strtolower(escapestr($fname)))."',";
        $query .= "Lastname = '".ucwords(strtolower(escapestr($lname)))."',";
        $query .= "Email = '".escapestr($email)."',";
        if($e2p == 'Yes') $query .= "EmailToPhone = 1,";
        else $query .= "EmailToPhone = 0,";
        if($franchise == 'on') $query .= "FranchiseInterest = 1,";
        else $query .= "FranchiseInterest = 0,";
        $query .= "LastUpdated = '".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
        if(strlen($company)>0) $query .= "Company = '".escapestr($company)."'";
        else $query .= "Company = NULL";
        $query .= " where UserID = ".$userid;
        if(!mysql_query($query, $con)) $saveerror = 'Could not update user information!';
        else
        {
            $lastid = $userid;

            // Bring the temp file over to where we can work with it...
            if(isset($_FILES['photoname']['name']) && (strlen($_FILES['photoname']['name']) > 0))
            {
                $ext = strtolower(end(explode(".", $_FILES['photoname']['name'])));
                $path = WEB_ROOT_PATH;
                $ifilename = 'userimages/User'.$lastid.'temp.'.$ext;
                $target = $path.$ifilename;
                if(!is_uploaded_file($_FILES['photoname']['tmp_name'])) $fileerror = 'Transfer Error - usually due to files over '.ini_get('upload_max_filesize').' in size';
                else
                {
                    if(($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE"))
                    {
                        if($_FILES["photoname"]["size"] > (20*1024*1024)) $fileerror = 'File is larger than 20M...'.number_format($_FILES["photoname"]["size"]/1024/1024).'M';
                        else
                        {
                            if($_FILES["photoname"]["error"] > 0) $fileerror = 'File Has an Error';
                            else
                            {
                                $remoldfiles = $path.'userimages/User'.$lastid.'*.*';
                                foreach(glob($remoldfiles) as $fn) unlink($fn);
                                if(!move_uploaded_file($_FILES["photoname"]["tmp_name"], $target)) $fileerror = 'Image Invalid';
                            }
                        }
                    }
                    else $fileerror = 'File is not a GIF, JPG or PNG format';
                }
            }
            else $ifilename = '';

            if($fileerror == 'false')
            {
                if($ifilename && (strlen($ifilename)>0))
                {
                    $ext = strtolower(end(explode(".", $ifilename)));
                    $path = WEB_ROOT_PATH;
                    $ofilename = 'userimages/User'.$lastid.'.'.$ext;
                    $target = $path.$ofilename;

                    if(!ScaledImageFile($ifilename, 200, 300, $ofilename, 'false', 'true')) $fileerror = 'Could not create thumbnail for your image!';
                    else
                    {
                        unlink($ifilename);

                        // Update the user in the database...
                        $iquery = "update users Set ImageFile = '".escapestr($ofilename)."' where UserID = ".$lastid;
                        if(!mysql_query($iquery, $con)) $saveerror = 'Could not update user image!';
                    }
                }
            }

            if($saveerror == 'false' && isset($marketneedid) && !is_null($marketneedid))
            {
                $query = "update marketneeds set NeedsContact = ";
                if($repcall == 'on') $query .= "1";
                else $query .= "0";
                $query .= " where marketneedid = ".$marketneedid;
                if(!mysql_query($query, $con)) $saveerror = 'Could not update "Needs Contact" information!';
            }

            // Easier to delete the current numbers and start adding them over...
            if($saveerror == 'false')
            {
                $query = "delete from usernumbers where UserID = ".$lastid;
                if(!mysql_query($query, $con)) $saveerror = 'Could not remove old numbers.';
            }

            $ordernum = 1;

            // Add Home Phone if available...
            if(($saveerror == 'false') && (strlen($homephone)>0))
            {
                $query = "insert into usernumbers (UserID, NumberTypeID, PhoneNumber, DisplayOrder, Texting, StartDate) values (".$lastid.",1,";
                $query .= "'".$homephone."',";
                $query .= $ordernum.",0,";
                $ordernum++;
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Home Phone';
            }

            // Add Cell Phone if available...
            if(($saveerror == 'false') && (strlen($cellphone)>0))
            {
                $query = "insert into usernumbers (UserID, NumberTypeID, PhoneNumber, DisplayOrder, Texting, StartDate) values (".$lastid.",2,";
                $query .= "'".$cellphone."',";
                $query .= $ordernum.",";
                $ordernum++;
                if($celltext == 'Yes') $query .= "1,";
                else $query .= "0,";
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Cell Phone';
            }

            // Add Work Phone if available...
            if(($saveerror == 'false') && (strlen($workphone)>0))
            {
                $query = "insert into usernumbers (UserID, NumberTypeID, PhoneNumber, Extension, DisplayOrder, Texting, StartDate) values (".$lastid.",3,";
                $query .= "'".$workphone."',";
                if(strlen($workext)>0) $query .= "'".$workext."',";
                else $query .= "NULL,";
                $query .= $ordernum.",0,";
                $ordernum++;
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Work Phone';
            }

            // Add Fax if available...
            if(($saveerror == 'false') && (strlen($faxnum)>0))
            {
                $query = "insert into usernumbers (UserID, NumberTypeID, PhoneNumber, DisplayOrder, Texting, StartDate) values (".$lastid.",4,";
                $query .= "'".$faxnum."',";
                $query .= $ordernum.",0,";
                $ordernum++;
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Fax';
            }

            // Add Other Phone 1 if available...
            if(($saveerror == 'false') && (strlen($addphone1)>0))
            {
                $query = "insert into usernumbers (UserID, NumberTypeID, PhoneNumber, Extension, DisplayOrder, Texting, AddInfo, StartDate) values (".$lastid.",6,";
                $query .= "'".$addphone1."',";
                if(strlen($addphone1ext)>0) $query .= "'".escapestr($addphone1ext)."',";
                else $query .= "NULL,";
                $query .= $ordernum.",";
                $ordernum++;
                if($addphone1text == 'Yes') $query .= "1,";
                else $query .= "0,";
                if(strlen($addphone1type)>0) $query .= "'".escapestr($addphone1type)."',";
                else $query .= "NULL,";
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Phone 1';
            }

            // Add Other Phone 2 if available...
            if(($saveerror == 'false') && (strlen($addphone2)>0))
            {
                $query = "insert into usernumbers (UserID, NumberTypeID, PhoneNumber, Extension, DisplayOrder, Texting, AddInfo, StartDate) values (".$lastid.",6,";
                $query .= "'".$addphone2."',";
                if(strlen($addphone2ext)>0) $query .= "'".escapestr($addphone2ext)."',";
                else $query .= "NULL,";
                $query .= $ordernum.",";
                $ordernum++;
                if($addphone2text == 'Yes') $query .= "1,";
                else $query .= "0,";
                if(strlen($addphone2type)>0) $query .= "'".escapestr($addphone2type)."',";
                else $query .= "NULL,";
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Phone 2';
            }

            // Add Address if available...
            if(($saveerror == 'false') && (strlen($zip)>0))
            {
                $query = "select UserAddressID from useraddresses where UserID = ".$lastid." and IsPrimary = 1";
                $result = mysql_query($query, $con);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    $query = "insert into useraddresses (UserID, IsPrimary, ZIP, Address1, Address2, City, State, StartDate) values (".$lastid.",1,";
                    $query .= "'".$zip."',";
                    if(strlen($address1)>0) $query .= "'".escapestr($address1)."',";
                    else $query .= "NULL,";
                    if(strlen($address2)>0) $query .= "'".escapestr($address2)."',";
                    else $query .= "NULL,";
                    if(strlen($city)>0) $query .= "'".ucwords(strtolower(escapestr($city)))."',";
                    else $query .= "NULL,";
                    if(strlen($thestate)>0) $query .= "'".strtoupper($thestate)."',";
                    else $query .= "NULL,";
                    $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                    if(!mysql_query($query, $con)) $saveerror = 'Could not add Address';
                }
                else
                {
                    $uaddrID = $row[0];
                    $query = "update useraddresses set ZIP = '".$zip."',";
                    if(strlen($address1)>0) $query .= "Address1 = '".escapestr($address1)."',";
                    else $query .= "Address1 = NULL,";
                    if(strlen($address2)>0) $query .= "Address2 = '".escapestr($address2)."',";
                    else $query .= "Address2 = NULL,";
                    if(strlen($city)>0) $query .= "City = '".ucwords(strtolower(escapestr($city)))."',";
                    else $query .= "City = NULL,";
                    if(strlen($thestate)>0) $query .= "State = '".strtoupper($thestate)."'";
                    else $query .= "State = NULL";
                    $query .= " where UserAddressID = ".$uaddrID;
                    if(!mysql_query($query, $con)) $saveerror = 'Could not update Address';
                }
            }

            // Easier to delete the current numbers and start adding them over...
            if($saveerror == 'false')
            {
                $query = "delete from alternateemails where UserID = ".$lastid;
                if(!mysql_query($query, $con)) $saveerror = 'Could not remove old emails.';
            }

            $ordernum = 1;
            // Add Alternate Email if available...
            if(($saveerror == 'false') && (strlen($email2)>0))
            {
                $query = "insert into alternateemails (UserID, Email, AddInfo, EmailToPhone, DisplayOrder, StartDate) values (".$lastid.",";
                $query .= "'".escapestr($email2)."',";
                if(strlen($email2type)>0) $query .= "'".escapestr($email2type)."',";
                else $query .= "NULL,";
                if($e2p2 == 'Yes') $query .= "1,";
                else $query .= "0,";
                $query .= $ordernum.",";
                $ordernum++;
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Email 2';
            }

            // Add Alternate Email if available...
            if(($saveerror == 'false') && (strlen($email3)>0))
            {
                $query = "insert into alternateemails (UserID, Email, AddInfo, EmailToPhone, DisplayOrder, StartDate) values (".$lastid.",";
                $query .= "'".escapestr($email3)."',";
                if(strlen($email3type)>0) $query .= "'".escapestr($email3type)."',";
                else $query .= "NULL,";
                if($e2p3 == 'Yes') $query .= "1,";
                else $query .= "0,";
                $query .= $ordernum.",";
                $ordernum++;
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
                if(!mysql_query($query, $con)) $saveerror = 'Could not update Email 3';
            }

            // Add Login Info if available...
            if($saveerror == 'false')
            {
                $query = "select UserID from userlogin where UserID = ".$lastid;
                $result = mysql_query($query, $con);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    $query = "insert into userlogin (UserID, UseEmail, Login, Password, PWQuestion1, PWAnswer1) values (".$lastid.",";
                    if(useemail == 'on') $query .= "1,";
                    else
                    {
                        if(strlen($loginname)>0) $query .= "0,'".escapestr($loginname)."',";
                        else $query .= "1,NULL,";
                    }
                    $query .= "'".md5($rp)."',";
                    $query .= "'".$question1."',";
                    if(strlen($answer1) > 0) $query .= "'".escapestr($answer1)."')";
                    else $query .= "'none')";
                    if(!mysql_query($query, $con)) $saveerror = 'Could not update Login';
                }
                else
                {
                    $query = "update userlogin set ";
                    ", , , , ) values (".$lastid.",";
                    if(useemail == 'on') $query .= "UseEmail = 1,";
                    else
                    {
                        if(strlen($loginname)>0) $query .= "UseEmail = 0, Login = '".escapestr($loginname)."',";
                        else $query .= "UseEmail = 1,Login = NULL,";
                    }
                    if(strlen($rp)>0) $query .= "Password = '".md5($rp)."',";
                    $query .= "PWQuestion1 = '".$question1."',";
                    if(strlen($answer1) > 0) $query .= "PWAnswer1 = '".escapestr($answer1)."'";
                    else $query .= "PWAnswer1 = 'none'";
                    $query .= " where UserID = ".$lastid;
                    if(!mysql_query($query, $con)) $saveerror = 'Could not update Login';
                }
            }
        } // user was created
        if($saveerror != 'true') mysql_query("commit", $con);
        else mysql_query("rollback", $con);

        mysql_close($con);
    } // done with database
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
<?php
    if($emailexists != 'false')
    {
        echo '<h1 class="subhead" style="width:300px;">Email Exists!</h1>';
    }
    elseif($loginexists != 'false')
    {
        echo '<h1 class="subhead" style="width:300px;">Login Exists!</h1>';
    }
    elseif($saveerror != 'false')
    {
        echo '<h1 class="subhead" style="width:300px;">Registration Issue!</h1>';
    }
    else
    {
        echo '<h1 class="subhead" style="width:300px;">Update Successful!</h1>';
    }
?>
        <div class="grideightgrey">
            <div class="grideight" style="width:580px;">
<?php
    if($emailexists != 'false')
    {
        echo '<p class="blacktwelve">Sorry!  That email has already been used in the system.</p>';
        echo '<p class="blacktwelve">You may login at the top of the page, or if you have forgotten your password, please visit the <a href="forgotpass1.php">forgotten password</a> area.</p>';
    }
    elseif($loginexists != 'false')
    {
        echo '<p class="blacktwelve">Sorry!  That login has already been used in the system.</p>';
        echo '<p class="blacktwelve">You may login at the top of the page, or if you have forgotten your password, please visit the <a href="forgotpass1.php">forgotten password</a> area.</p>';
    }
    elseif($saveerror != 'false')
    {
        echo '<p class="blacktwelve">Sorry!  There was an error processing your registration.</p>';
        echo '<p class="blacktwelve">Please contact a representative at 1-800-vehicles (834-4253) for help with this issue.</p>';
        if($saveerror != 'true') echo $saveerror.'<br/>';
        echo $query.'<br/>';
        //echo var_dump($_SESSION).'<br/>';
    }
    else
    {
        echo '<!--p class="blacktwelve">Your information has been saved and your login and password have been emailed to you.</p-->';
        echo '<p class="blacktwelve">Your information has been saved.</p>';
        if($fileerror != 'false')
        {
            echo '<br/><br/><p class="blacktwelve">Note: Your photo could not be uploaded due to the following error: '.$fileerror.'<br/>';
            echo 'You may try again from the My Account area with a different file.</p>';
            //echo $query.'<br/>';
            //echo $ifilename.' --> '.$ofilename.'<br/>';
            //echo $target.'<br/>';
        }
        //echo $ifilename.' --> '.$ofilename.'<br/>';
        //echo $width.' '.$height.' '.$image_type;
        //echo $iquery.'<br/>';
        //echo $target.'<br/>';
    }
?>
            </div>  <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
