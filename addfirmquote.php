<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 10;
    $_SESSION['titleadd'] = 'Add Firm Quote';

    if(!isset($_POST['ForUserID']) || !isset($_POST['MarketNeedID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000510';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    $userid = $_SESSION['userid'];

    // Determine who this is for and which market need we are working on...
    $inuserid = $_POST['ForUserID'];
    $inusername = getuserfullname($inuserid, 'false');
    $inmneed = $_POST['MarketNeedID'];

    // If this is not set, then we are going to create a new quote request for the customer...
    if(isset($_POST['ForQuoteRequestID'])) $quoterequestid = $_POST['ForQuoteRequestID'];
    else $quoterequestid = -1;

    // If this is not set, then we will be creating a new firm quote for the customer...
    if(isset($_POST['FirmQuoteID'])) $firmquoteid = $_POST['FirmQuoteID'];
    else $firmquoteid = -1;

    // What state are we being called? If it is a postback it may not mean we have to save yet if just selections are changing...
    if(isset($_POST["InPostBack"]))
    {
        $postback = 'true';

        // This determines whether the user only selected new choices in the drop downs for the vehicle or not...
        // We only save the data when we are not simply refreshing...
        if(isset($_POST["RefreshOnly"])) $refreshonly = 'true';
        else $refreshonly = 'false';
    }
    else $postback = 'false';

    // Use these to tell the salesrep about any problems encountered...
    $loaderror = 'false';
    $saveerror = 'false';
    $fileerror = 'false';

    if($postback == 'true')
    {
        // We are re-entering this page, so we need to grab everything that was sent in to reflect the salesrep changes...
        $iquotetype = $_POST['QuoteType'];
        $iyear = $_POST['Year'];
        $imake = $_POST['Make'];
        $imodel = $_POST['Model'];
        $istyle = $_POST['Style'];
        $imileage = $_POST['selectmileage'];
        $iseattype = $_POST['selectseat'];
        $iengine = $_POST['selectengine'];
        $idrive = $_POST['selectdrive'];
        $itrans = $_POST['selecttrans'];
        $iwheels = $_POST['selectwheels'];
        $icab = $_POST['selectcab'];
        $ifront = $_POST['selectfrontseat'];
        $ibedtype = $_POST['selectbedtype'];
        $isunroof = $_POST['selectsunroof'];
        $iradflex = $_POST['RadFlex'];
        if(isset($_POST['RadFM'])) $iradfm = 1;
        else $iradfm = 0;
        if(isset($_POST['RadCas'])) $iradcas = 1;
        else $iradcas = 0;
        if(isset($_POST['RadCD'])) $iradcd = 1;
        else $iradcd = 0;
        $ipdoor = $_POST['PowerDoorLocks'];
        $ipwin = $_POST['PowerWindows'];
        $ipseat = $_POST['PowerSeats'];
        $iheat = $_POST['HeatedSeats'];
        $iair = $_POST['AirConditioning'];
        $iremote = $_POST['RemoteEntry'];
        $itraction = $_POST['TractionControl'];
        $isecure = $_POST['SecuritySystem'];
        $icruise = $_POST['CruiseControl'];
        $inavsys = $_POST['Navigation'];
        $irearwin = $_POST['RearSlidingWindow'];
        $ibed = $_POST['BedLiner'];
        $itow = $_POST['TowPackage'];
        $iluggage = $_POST['LuggageRack'];
        $icolors = $_POST['colorcombo'];
        $inotes = $_POST['notes'];
        if($_POST['DeliveryPickup'] == 'Deliver') $idelorpick = 0;
        else $idelorpick = 1;
        $idelcity = $_POST['DeliveryCity'];
        $idelstate = $_POST['DeliveryState'];

        if(isset($_POST['researching'])) $fstatus = 1;
        else $fstatus = 0;
        $fordertype = $_POST['ordertype'];
        if(isset($_POST['WaitPeriod']) && (strlen($_POST['WaitPeriod']) > 0)) $fwaitperiod = $_POST['WaitPeriod'];
        else $fwaitperiod = 0;
        if(isset($_POST['Mileage']) && (strlen($_POST['Mileage']) > 0)) $fmiles = $_POST['Mileage'];
        else $fmiles = 'NULL';
        if(isset($_POST['PricePoint']) && (strlen($_POST['PricePoint']) > 0)) $fprice = $_POST['PricePoint'];
        else $fprice = 'NULL';
        $fadddep = $_POST['AddDep'];
        $faddsec = $_POST['AddSec'];
        $faddnav = $_POST['AddNav'];
        $faddsel = $_POST['AddSel'];
        $faddpur = $_POST['AddPur'];
        $faddtire = $_POST['AddTire'];
        $fadddel = $_POST['AddDelivery'];
        $fimage = $_POST['imagename'];
        $fnote = $_POST['AdminNote'];
        if(isset($_POST['visible'])) $fvisible = 1;
        else $fvisible = 0;

        // We are not just refreshing and need to insert/update as appropriate...
        if($refreshonly == 'false')
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                if($quoterequestid != -1)
                {
                    $query = "update quoterequests set ";
                    $query .= "LastUpdated = '".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                    $query .= "QuoteType = '".$iquotetype."',";
                    $query .= "Year = ".escapestr($iyear).",";
                    $query .= "Make = '".escapestr($imake)."',";
                    $query .= "Model = '".escapestr($imodel)."',";
                    $query .= "Style = '".escapestr($istyle)."',";
                    $query .= "MileageCeiling = ".$imileage.",";
                    $query .= "EngineCylinders = ".$iengine.",";
                    $query .= "Transmission = '".$itrans."',";
                    if(isset($icab)) $query .= "CabType = '".$icab."',";
                    else $query .= "CabType = 'Flexible',";
                    if(isset($ifront)) $query .= "FrontSeatType = '".$ifront."',";
                    else $query .= "FrontSeatType = 'Flexible',";
                    if(isset($ibedtype)) $query .= "BedType = '".$ibedtype."',";
                    else $query .= "BedType = 'Flexible',";
                    if(isset($isunroof)) $query .= "SunRoof = '".$isunroof."',";
                    else $query .= "SunRoof = 'Flexible',";
                    $query .= "SeatMaterial = '".$iseattype."',";
                    $query .= "WheelDriveType = '".$idrive."',";
                    if($idrive == 'Flexible') $query .= "WheelDrive = 'Flexible',";
                    elseif($idrive == 'Four') $query .= "WheelDrive = '4',";
                    else $query .= "WheelDrive = '2',";
                    $query .= "WheelCovers = '".$iwheels."',";
                    if(isset($iradflex)) $query .= "RadioNeeded = 'Flexible',";
                    elseif(($iradfm == 1) || ($iradcas == 1) || ($iradcd == 1)) $query .= "RadioNeeded = 'Yes',";
                    else $query .= "RadioNeeded = 'No',";
                    if($iradfm==1) $query .= "RadioFM = 1,";
                    else $query .= "RadioFM = 0,";
                    if($iradcas==1) $query .= "RadioCassette = 1,";
                    else $query .= "RadioCassette = 0,";
                    if($iradcd==1) $query .= "RadioCD = 1,";
                    else $query .= "RadioCD = 0,";
                    $query .= "RadioSat = 0,";
                    $query .= "PowerDoors = '".$ipdoor."',";
                    $query .= "PowerWindows = '".$ipwin."',";
                    $query .= "PowerSeats = '".$ipseat."',";
                    $query .= "HeatedSeats = '".$iheat."',";
                    $query .= "AirConditioning = '".$iair."',";
                    $query .= "RemoteEntry = '".$iremote."',";
                    $query .= "TractionControl = '".$itraction."',";
                    $query .= "SecuritySystem = '".$isecure."',";
                    $query .= "CruiseControl = '".$icruise."',";
                    $query .= "Navigation = '".$inavsys."',";
                    if(isset($irearwin)) $query .= "RearSlidingWindow = '".$irearwin."',";
                    else $query .= "RearSlidingWindow = 'Flexible',";
                    if(isset($ibed)) $query .= "BedLiner = '".$ibed."',";
                    else $query .= "BedLiner = 'Flexible',";
                    if(isset($itow)) $query .= "TowPackage = '".$itow."',";
                    else $query .= "TowPackage = 'Flexible',";
                    if(isset($iluggage)) $query .= "LuggageRack = '".$iluggage."',";
                    else $query .= "LuggageRack = 'Flexible',";
                    $query .= "Convertible = 'Flexible',";
                    if(isset($icolors) && (strlen($icolors) > 0)) $query .= "ColorCombination = '".escapestr($icolors)."',";
                    else $query .= "ColorCombination = NULL,";
                    if(isset($inotes) && (strlen($inotes) > 0)) $query .= "SpecialRequests = '".escapestr($inotes)."',";
                    else $query .= "SpecialRequests = NULL,";
                    if($idelorpick == 0)
                    {
                        $query .= "Pickup = 0,";
                        if(isset($idelcity)) $query .= "DeliverToCity = '".$idelcity."',";
                        else $query .= "DeliverToCity = NULL,";
                        if(isset($idelstate)) $query .= "DeliverToState = '".$idelstate."'";
                        else $query .= "DeliverToState = NULL";
                    }
                    else $query .= "Pickup = 1,DeliverToCity = NULL,DeliverToState = NULL";
                    $query .= ",Visible=1 where QuoteRequestID = ".$quoterequestid;
                    if(!mysql_query($query, $con)) $saveerror = 'Could not update Request';
                }
                else
                {
                    // Not editing, so insert a new one...
                    $query = "insert into quoterequests (MarketNeedID, Created, LastUpdated, QuoteType, Year, Make, Model, Style, MileageCeiling, EngineCylinders, Transmission,
                        CabType, FrontSeatType, BedType, SunRoof, SeatMaterial, WheelDriveType, WheelDrive, WheelCovers, RadioNeeded, RadioFM, RadioCassette, RadioCD, RadioSat,
                        PowerDoors, PowerWindows, PowerSeats, HeatedSeats, AirConditioning, RemoteEntry, TractionControl, SecuritySystem, CruiseControl, Navigation, RearSlidingWindow,
                        BedLiner, TowPackage, LuggageRack, Convertible, ColorCombination, SpecialRequests, Pickup, DeliverToCity, DeliverToState) values (".$inmneed.",";
                    $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                    $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                    $query .= "'".$iquotetype."',";
                    $query .= escapestr($iyear).",";
                    $query .= "'".escapestr($imake)."',";
                    $query .= "'".escapestr($imodel)."',";
                    $query .= "'".escapestr($istyle)."',";
                    $query .= $imileage.",";
                    $query .= $iengine.",";
                    $query .= "'".$itrans."',";
                    if(isset($icab)) $query .= "'".$icab."',";
                    else $query .= "'Flexible',";
                    if(isset($ifront)) $query .= "'".$ifront."',";
                    else $query .= "'Flexible',";
                    if(isset($ibedtype)) $query .= "'".$ibedtype."',";
                    else $query .= "'Flexible',";
                    if(isset($isunroof)) $query .= "'".$isunroof."',";
                    else $query .= "'Flexible',";
                    $query .= "'".$iseattype."',";
                    $query .= "'".$idrive."',";
                    if($idrive == 'Flexible') $query .= "'Flexible',";
                    elseif($idrive == 'Four') $query .= "'4',";
                    else $query .= "'2',";
                    $query .= "'".$iwheels."',";
                    if(isset($iradflex)) $query .= "'Flexible',";
                    elseif(($iradfm==1) || ($iradcas==1) || ($iradcd==1)) $query .= "'Yes',";
                    else $query .= "'No',";
                    if($iradfm==1) $query .= "1,";
                    else $query .= "0,";
                    if($iradcas==1) $query .= "1,";
                    else $query .= "0,";
                    if($iradcd==1) $query .= "1,";
                    else $query .= "0,";
                    $query .= "0,";
                    $query .= "'".$ipdoor."',";
                    $query .= "'".$ipwin."',";
                    $query .= "'".$ipseat."',";
                    $query .= "'".$iheat."',";
                    $query .= "'".$iair."',";
                    $query .= "'".$iremote."',";
                    $query .= "'".$itraction."',";
                    $query .= "'".$isecure."',";
                    $query .= "'".$icruise."',";
                    $query .= "'".$inavsys."',";
                    if(isset($irearwin)) $query .= "'".$irearwin."',";
                    else $query .= "'Flexible',";
                    if(isset($ibed)) $query .= "'".$ibed."',";
                    else $query .= "'Flexible',";
                    if(isset($itow)) $query .= "'".$itow."',";
                    else $query .= "'Flexible',";
                    if(isset($iluggage)) $query .= "'".$iluggage."',";
                    else $query .= "'Flexible',";
                    $query .= "'Flexible',";
                    if(isset($icolors) && (strlen($icolors) > 0)) $query .= "'".escapestr($icolors)."',";
                    else $query .= "NULL,";
                    if(isset($inotes) && (strlen($inotes) > 0)) $query .= "'".escapestr($inotes)."',";
                    else $query .= "NULL,";
                    if($idelorpick == 0)
                    {
                        $query .= "0,";
                        if(isset($idelcity)) $query .= "'".$idelcity."',";
                        else $query .= "NULL,";
                        if(isset($idelstate)) $query .= "'".$idelstate."'";
                        else $query .= "NULL";
                    }
                    else $query .= "1,NULL,NULL";
                    $query .= ")";
                    if(!mysql_query($query, $con)) $saveerror = 'Could not add Request';
                    $quoterequestid = mysql_insert_id($con);
                }

                if($firmquoteid != -1)
                {
                    $query = "update firmquotes set ";
                    $query .= "SalesRepID=".$userid.",";
                    $query .= "LastUpdated='".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                    if(isset($_POST['researching'])) $query .= "UpdateStatus='Researching',";
                    else $query .= "UpdateStatus='Completed',";
                    $query .= "OrderType='".$_POST['ordertype']."',";
                    $query .= "WaitPeriod=".$fwaitperiod.",";
                    $query .= "Mileage=".$fmiles.",";
                    $query .= "PricePoint=".$fprice.",";
                    $query .= "AddDepositAmount=".$_POST['AddDep'].",";
                    $query .= "AddSecondKeyLimit=".$_POST['AddSec'].",";
                    $query .= "AddNavDiscLimit=".$_POST['AddNav'].",";
                    $query .= "AddSellerRepairLimit=".$_POST['AddSel'].",";
                    $query .= "AddPurchaserRepairLimit=".$_POST['AddPur'].",";
                    $query .= "AddTireCopay=".$_POST['AddTire'].",";
                    $query .= "AddDelivery=".$_POST['AddDelivery'].",";
                    if(isset($_POST['AdminNote']) && (strlen($_POST['AdminNote']) > 0)) $query .= "AdminNote='".escapestr($_POST['AdminNote'])."',";
                    else $query .= "AdminNote=NULL,";
                    if(isset($_POST['visible'])) $query .= "Visible=1";
                    else $query .= "Visible=0";
                    $query .= " where FirmQuoteID=".$firmquoteid;
                    if(!mysql_query($query, $con)) $saveerror = 'Could not Update the Firm Quote';

                    // Add a Message Update when this happens...
                    if(!isset($_POST['researching']))
                    {
                        posttodashboard($con, $userid, $userid, 'updated the Firm Quote for '.$inusername.'.');

                        $message = 'A firm quote has been updated for you to review.</br>Go to: <a href="http://www.1800vehicles.com">1800vehicles.com</a> to review the quote.<br/>Please contact your sales representative soon to discuss the changes to the firm quote.';
                        sendemail(getuseremailnodb($con, $inuserid), 'Firm Quote To Review', $message, 'false');

                        posttodashboard($con, $userid, $inuserid, $firstname.' '.$lastname.' updated a <a href="'.WEB_SERVER_NAME.'quotereceived.php?QuoteID='.$quoterequestid.'">Firm Quote</a> for you to review.');

                        $message = 'Your customer has received the update to the Firm Quote to review...</br>';
                        $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $inuserid, 'false').'</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $inuserid).'</br>';
                        sendemail(getuseremailnodb($con, $userid), 'Firm Quote Received', $message, 'true');

                        $message = 'A customer has been given an updated Firm Quote to review...</br>';
                        $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $inuserid, 'false').'</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $inuserid).'</br>';
                        sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                    }
                }
                else
                {
                    // Not editing, so insert a new one...
                    $query = "insert into firmquotes (QuoteRequestID,SalesRepID,Created,LastUpdated,UpdateStatus,OrderType,WaitPeriod,Mileage,PricePoint,AddDepositAmount,AddSecondKeyLimit,AddNavDiscLimit,AddSellerRepairLimit,AddPurchaserRepairLimit,AddTireCopay,AddDelivery,AdminNote,Visible) values (";
                    $query .= $quoterequestid.",".$userid.",";
                    $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."','".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                    if(isset($_POST['researching'])) $query .= "'Researching',";
                    else $query .= "'Completed',";
                    $query .= "'".$_POST['ordertype']."',";
                    $query .= $fwaitperiod.",";
                    $query .= $fmiles.",";
                    $query .= $fprice.",";
                    $query .= $_POST['AddDep'].",";
                    $query .= $_POST['AddSec'].",";
                    $query .= $_POST['AddNav'].",";
                    $query .= $_POST['AddSel'].",";
                    $query .= $_POST['AddPur'].",";
                    $query .= $_POST['AddTire'].",";
                    $query .= $_POST['AddDelivery'].",";
                    if(isset($_POST['AdminNote']) && (strlen($_POST['AdminNote']) > 0)) $query .= "'".escapestr($_POST['AdminNote'])."',";
                    else $query .= "NULL,";
                    if(isset($_POST['visible'])) $query .= "1)";
                    else $query .= "0)";
                    if(!mysql_query($query, $con)) $saveerror = 'Could not Add the Firm Quote';
                    else $firmquoteid = mysql_insert_id($con);

                    // Add a Message Update when this happens...
                    if(!isset($_POST['researching']))
                    {
                        posttodashboard($con, $userid, $userid, 'added the Firm Quote for '.$inusername.'.');

                        $message = 'A firm quote has been added for you to review.</br>Go to: <a href="http://www.1800vehicles.com">1800vehicles.com</a> to review the quote.<br/>Please contact your sales representative soon to discuss the firm quote.';
                        sendemail(getuseremailnodb($con, $inuserid), 'Firm Quote To Review', $message, 'false');

                        posttodashboard($con, $userid, $inuserid, $firstname.' '.$lastname.' provided a <a href="'.WEB_SERVER_NAME.'quotereceived.php?QuoteID='.$quoterequestid.'">Firm Quote</a> for you to review.');

                        $message = 'Your customer has received the Firm Quote to review...</br>';
                        $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $inuserid, 'false').'</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $inuserid).'</br>';
                        sendemail(getuseremailnodb($con, $userid), 'Firm Quote Received', $message, 'true');

                        $message = 'A customer has been given a Firm Quote to review...</br>';
                        $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $inuserid, 'false').'</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $inuserid).'</br>';
                        sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                    }
                }

                if($firmquoteid != -1)
                {
                    // Bring the temp file over to where we can work with it...
                    if(isset($_FILES['imagename']['name']) && (strlen($_FILES['imagename']['name']) > 0))
                    {
                        $ext = strtolower(end(explode(".", $_FILES['imagename']['name'])));
                        $path = WEB_ROOT_PATH;
                        $ifilename = 'fqimages/Quote'.$firmquoteid.'temp.'.$ext;
                        $target = $path.$ifilename;
                        if(!is_uploaded_file($_FILES['imagename']['tmp_name'])) $fileerror = 'Transfer Error - usually due to files over '.ini_get('upload_max_filesize').' in size';
                        else
                        {
                            if(($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE"))
                            {
                                if($_FILES["imagename"]["size"] > (20*1024*1024)) $fileerror = 'File is larger than 20M...'.number_format($_FILES["imagename"]["size"]/1024/1024).'M';
                                else
                                {
                                    if($_FILES["imagename"]["error"] > 0) $fileerror = 'File Has an Error';
                                    else
                                    {
                                        if(file_exists($target)) unlink($target);
                                        if(!move_uploaded_file($_FILES['imagename']['tmp_name'], $target)) $fileerror = 'Image Invalid';
                                    }
                                }
                            }
                            else $fileerror = 'File is not a GIF, JPG or PNG format';
                        }
                    }
                    else $ifilename = '';

                    if($fileerror == 'false')
                    {
                        if($ifilename && (strlen($ifilename)>0))
                        {
                            $ext = strtolower(end(explode(".", $ifilename)));
                            $path = WEB_ROOT_PATH;
                            $ofilename = 'fqimages/Quote'.$firmquoteid.'.'.$ext;
                            $target = $path.$ofilename;
                            if(file_exists($target)) unlink($target);
                            if(!ScaledImageFile($ifilename, 500, 500, $ofilename, 'false', 'true')) $fileerror = 'Could not create thumbnail for your image!';
                            else
                            {
                                unlink($ifilename);

                                $query = "update firmquotes Set ImageFile = '".escapestr($ofilename)."' where FirmQuoteID = ".$firmquoteid;
                                if(!mysql_query($query, $con)) $saveerror = 'Could not update image!';
                            }
                        }
                        else
                        {
                            if(isset($_POST['PriorImage']) && isset($_POST['RemImg']))
                            {
                                $query = "update firmquotes Set ImageFile = NULL where FirmQuoteID = ".$firmquoteid;
                                if(!mysql_query($query, $con)) $saveerror = 'Could not remove image!';
                            }
                        }
                    }
                }

                mysql_close($con);
            }
            if(($saveerror != 'false') || ($fileerror != 'false') || ($loaderror != 'false'))
            {
                $_SESSION['ShowError'] = 'Errors: \n';
                if($loaderror != 'false') $_SESSION['ShowError'] .= 'Load = '.$loaderror.'\n';
                if($fileerror != 'false') $_SESSION['ShowError'] .= 'File = '.$fileerror.'\n';
                if($saveerror != 'false') $_SESSION['ShowError'] .= 'Save = '.$saveerror.'\n';
                $_SESSION['ShowError'] .= 'Last Query = '.$query.'\n';
            }

            header('Location: salesrepactions.php?ForUserID='.$inuserid.'&MarketNeedID='.$inmneed);
            exit();
        }
    }
    else
    {
        // This is the first call to the page, so we may need to load in the data for this quote...
        if($quoterequestid != -1)
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                // Get the current Quote Request Information (if any)...
                $query = "select QuoteType, Year, Make, Model, Style, MileageCeiling, EngineCylinders, Transmission,
                    CabType, FrontSeatType, BedType, SeatMaterial, WheelDriveType, WheelCovers, RadioNeeded, RadioFM, RadioCassette, RadioCD, RadioSat, PowerDoors, PowerWindows,
                    PowerSeats, HeatedSeats, AirConditioning, RemoteEntry, TractionControl, SecuritySystem, CruiseControl, Navigation, RearSlidingWindow, BedLiner, TowPackage,
                    ColorCombination, SpecialRequests, Pickup, DeliverToCity, DeliverToState, LuggageRack, SunRoof, VehicleID from quoterequests where QuoteRequestID=".$quoterequestid;
                $result = mysql_query($query);
                if($result && $row = mysql_fetch_array($result))
                {
                    $iquotetype = $row[0];
                    $iyear = $row['Year'];
                    $imake = $row['Make'];
                    $imodel = $row['Model'];
                    $istyle = $row['Style'];
                    $imileage = $row['MileageCeiling'];
                    $iseattype = $row['SeatMaterial'];
                    $iengine = $row['EngineCylinders'];
                    $idrive = $row['WheelDriveType'];
                    $itrans = $row['Transmission'];
                    $iwheels = $row['WheelCovers'];
                    $icab = $row['CabType'];
                    $ifront = $row['FrontSeatType'];
                    $ibedtype = $row['BedType'];
                    $iradflex = $row['RadioNeeded'];
                    $iradfm = $row['RadioFM'];
                    $iradcas = $row['RadioCassette'];
                    $iradcd = $row['RadioCD'];
                    $iradsat = $row['RadioSat'];
                    $ipdoor = $row['PowerDoors'];
                    $ipwin = $row['PowerWindows'];
                    $ipseat = $row['PowerSeats'];
                    $iheat = $row['HeatedSeats'];
                    $iair = $row['AirConditioning'];
                    $iremote = $row['RemoteEntry'];
                    $itraction = $row['TractionControl'];
                    $isecure = $row['SecuritySystem'];
                    $icruise = $row['CruiseControl'];
                    $inavsys = $row['Navigation'];
                    $irearwin = $row['RearSlidingWindow'];
                    $ibed = $row['BedLiner'];
                    $itow = $row['TowPackage'];
                    $icolors = $row['ColorCombination'];
                    $inotes = $row['SpecialRequests'];
                    $idelorpick = $row['Pickup'];
                    $idelcity = $row['DeliverToCity'];
                    $idelstate = $row['DeliverToState'];
                    $iluggage = $row['LuggageRack'];
                    $isunroof = $row['SunRoof'];
                    $ivehid = $row['VehicleID'];

                    if($iquotetype == 'Standard')
                    {
                        if(!is_null($ivehid)) $result = mysql_query("select ImageFile from vehicles where VehicleID = ".$ivehid);
                        else $result = mysql_query("select v.ImageFile, v.VehicleID from vehicles v, makes m where m.makeid=v.makeid and m.name='".$imake."' and v.year=".$iyear." and v.model='".$imodel."' and v.style='".$istyle."'");
                        if($result && $row = mysql_fetch_array($result))
                        {
                            $imagefile = $row[0];
                            $ivehid = $row[1];
                        }

                        $highquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=0 and vehicleid = ".$ivehid;
                        $result = mysql_query($highquery);
                        if($result && $row = mysql_fetch_array($result))
                        {
                            $highbb = $row['bestbuy'];
                            $highmilestart = $row['mileagestart'];
                            $highmileend = $row['mileageend'];
                            $highpricestart = $row['pricestart'];
                            $highpriceend = $row['priceend'];
                        }

                        $lowquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=1 and vehicleid = ".$ivehid;
                        $result = mysql_query($lowquery);
                        if($result && $row = mysql_fetch_array($result))
                        {
                            $lowbb = $row['bestbuy'];
                            $lowmilestart = $row['mileagestart'];
                            $lowmileend = $row['mileageend'];
                            $lowpricestart = $row['pricestart'];
                            $lowpriceend = $row['priceend'];
                        }
                    }
                }
                else $loaderror = 'Could not find the specified quote request.';

                if($loaderror == 'false')
                {
                    // See if we need to load the firm quote as well...
                    if($firmquoteid != -1)
                    {
                        $query = "Select UpdateStatus,OrderType,Mileage,PricePoint,ImageFile,AdminNote,Visible,AddDepositAmount,AddSecondKeyLimit, AddNavDiscLimit, AddSellerRepairLimit, AddPurchaserRepairLimit, AddTireCopay, AddDelivery,WaitPeriod from firmquotes where FirmQuoteID=".$firmquoteid;
                        $result = mysql_query($query);
                        if($result && $row = mysql_fetch_array($result))
                        {
                            if($row[0] == 'Researching') $fstatus = 1;
                            else $fstatus = 0;
                            $fordertype = $row[1];
                            $fmiles = $row[2];
                            $fprice = $row[3];
                            $fimage = $row[4];
                            $fnote = $row[5];
                            $fvisible = $row[6];
                            $fadddep = $row[7];
                            $faddsec = $row[8];
                            $faddnav = $row[9];
                            $faddsel = $row[10];
                            $faddpur = $row[11];
                            $faddtire = $row[12];
                            $fadddel = $row[13];
                            $fwaitperiod = $row[14];
                        }
                        else $loaderror = 'Could not find the specified firm quote.';
                    }
                    else  // Set defaults...
                    {
                        $firmquoteid = -1;
                        $fstatus = 1;
                        $fvisible = 1;
                        $fordertype = 'Unknown';
                        $fwaitperiod = 14;
                    }
                }

                mysql_close($con);
            }
        }
        else
        {
            $iquotetype = 'Standard';
            $fstatus = 1;
            $fvisible = 1;
            $fordertype = 'Unknown';
            $fwaitperiod = 14;
        }
    }

    // Fill the mileage options...
    $index = 1;
    for($i = 1; $i <= 40; $i++)
    {
        $pmiles[$index] = 2500 + (($i - 1) * 2500);
        $index++;
    }
    if(!isset($imileage))
    {
        $imileage = $pmiles[24];
    }

    // Need these to fill the boxes in the case of a Standard Quote...
    if(!isset($iquotetype) || ($iquotetype == 'Standard'))
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            // First fill the list of Makes...
            mysql_select_db(DB_SERVER_DATABASE, $con);
            $result = mysql_query("select distinct m.makeid 'makeid', m.name 'make' from makes m, vehicles v where m.makeid = v.makeid and v.visible=1 order by 2");
            $data = '';
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['make']);
                if(strlen($data) > 0)
                {
                    $makeids[$index] = $row['makeid'];
                    $makes[$index] = $data;
                    $index++;
                }
            }

            // See if Make was selected already and this is a refresh or edit...
            if(isset($imake))
            {
                $make = $imake;
                // Get the ID...
                $query = 'select makeid from makes where name="'.$make.'"';
                $result = mysql_query($query);
                if($result && $row = mysql_fetch_array($result))
                {
                    $makeid = $row['makeid'];
                }
                else if($index > 0)
                {
                    $make = $makes[1];
                    $makeid = $makeids[1];
                }
            }
            else
            {
                if(isset($_POST['makeitem']) && strlen($_POST['makeitem']) > 0)
                {
                    $make = $_POST['makeitem'];
                    // Get the ID...
                    $query = 'select makeid from makes where name="'.$make.'"';
                    $result = mysql_query($query);
                    if($result && $row = mysql_fetch_array($result))
                    {
                        $makeid = $row['makeid'];
                    }
                    else if($index > 0)
                    {
                        $make = $makes[1];
                        $makeid = $makeids[1];
                    }
                }
                else if($index > 0)
                {
                    $make = $makes[1];
                    $makeid = $makeids[1];
                }
            }

            // See if we are in a valid state...
            if(isset($makeid))
            {
                if(isset($iyear) && isset($imodel) && isset($istyle))
                {
                    $year = $iyear;
                    $model = $imodel;
                    $style = $istyle;
                    $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."' and style='".$style."'";
                    $result = mysql_query($query);
                    if(!$result || !$row = mysql_fetch_array($result))
                    {
                        unset($style);
                        $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            unset($model);
                            $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                            $result = mysql_query($query);
                            if(!$result || !$row = mysql_fetch_array($result))
                            {
                                unset($year);
                            }
                        }
                    }
                }
                else
                {
                    if(isset($_POST['yearitem']) && isset($_POST['modelitem']) && isset($_POST['styleitem']))
                    {
                        $year = $_POST['yearitem'];
                        $model = $_POST['modelitem'];
                        $style = $_POST['styleitem'];
                        $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."' and style='".$style."'";
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            unset($style);
                            $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                            $result = mysql_query($query);
                            if(!$result || !$row = mysql_fetch_array($result))
                            {
                                unset($model);
                                $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                                $result = mysql_query($query);
                                if(!$result || !$row = mysql_fetch_array($result))
                                {
                                    unset($year);
                                }
                            }
                        }
                    }
                    else
                    {
                        if(isset($_POST['styleitem'])) unset($_POST['styleitem']);
                        if(isset($_POST['yearitem']) && isset($_POST['modelitem']))
                        {
                            $year = $_POST['yearitem'];
                            $model = $_POST['modelitem'];
                            $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                            $result = mysql_query($query);
                            if(!$result || !$row = mysql_fetch_array($result))
                            {
                                unset($model);
                                $query = "select vehicleid from vehicles where visible=1 and veh_makeid=".$makeid." and year=".$year;
                                $result = mysql_query($query);
                                if(!$result || !$row = mysql_fetch_array($result))
                                {
                                    unset($year);
                                }
                            }
                        }
                        else
                        {
                            if(isset($_POST['modelitem'])) unset($_POST['modelitem']);
                            if(isset($_POST['yearitem']))
                            {
                                $year = $_POST['yearitem'];
                                $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                                $result = mysql_query($query);
                                if(!$result || !$row = mysql_fetch_array($result))
                                {
                                    unset($year);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if(isset($year)) unset($year);
                if(isset($model)) unset($model);
                if(isset($style)) unset($style);
            }

            // Set up our year search string...
            $yearquery = "select distinct year from vehicles where visible=1";
            $modelquery = "select distinct model from vehicles where visible=1";
            $stylequery = "select distinct style from vehicles where visible=1";
            if(isset($makeid))
            {
                $yearquery .= " and makeid=".$makeid;
                $modelquery .= " and makeid=".$makeid;
                $stylequery .= " and makeid=".$makeid;
            }
            $yearquery .= " order by 1 desc";

            // Get the Years...
            $result = mysql_query($yearquery);
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['year']);
                if(strlen($data) > 0)
                {
                    $years[$index] = $data;
                    $index++;
                }
            }
            if(!isset($year) && $index > 0)
            {
                $year = $years[1];
            }

            // Set up our model search string...
            if(isset($year))
            {
                $modelquery .= " and year=".$year;
                $stylequery .= " and year=".$year;
            }
            $modelquery .= " order by 1";

            // Get the Models...
            $result = mysql_query($modelquery);
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['model']);
                if(strlen($data) > 0)
                {
                    $models[$index] = $data;
                    $index++;
                }
            }
            if(!isset($model) && $index > 0)
            {
                $model = $models[1];
            }

            // Set up our style search string...
            if(isset($model))
            {
                $stylequery .= " and model='".$model."'";
            }
            $stylequery .= " order by 1";

            // Get the Styles...
            $result = mysql_query($stylequery);
            $index = 1;
            while($result && $row = mysql_fetch_array($result))
            {
                $data = trim($row['style']);
                if(strlen($data) > 0)
                {
                    $styles[$index] = $data;
                    $index++;
                }
            }
            if(!isset($style) && $index > 0)
            {
                $style = $styles[1];
            }
            mysql_close($con);
        }
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }

    function trimAll(sString)
    {
        while(sString.substring(0,1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while(sString.substring(sString.length-1, sString.length) == ' ')
        {
            sString = sString.substring(0,sString.length-1);
        }
        return sString;
    }

    function movevalues()
    {
        var vmake = document.getElementById("Make");
        var vyear = document.getElementById("Year");
        var vmodel = document.getElementById("Model");
        var vstyle = document.getElementById("Style");

        var qtype = document.getElementById("QuoteType");
        if(qtype.value == "Standard")
        {
            var vlmake = document.getElementById("makelist");
            var vlyear = document.getElementById("yearlist");
            var vlmodel = document.getElementById("modellist");
            var vlstyle = document.getElementById("stylelist");

            vmake.value = vlmake.options[vlmake.selectedIndex].value;
            vyear.value = vlyear.options[vlyear.selectedIndex].value;
            vmodel.value = vlmodel.options[vlmodel.selectedIndex].value;
            vstyle.value = vlstyle.options[vlstyle.selectedIndex].value;
        }
        else
        {
            var vbmake = document.getElementById("makebox");
            var vbyear = document.getElementById("yearbox");
            var vbmodel = document.getElementById("modelbox");
            var vbstyle = document.getElementById("stylebox");

            vmake.value = vbmake.value;
            vyear.value = vbyear.value;
            vmodel.value = vbmodel.value;
            vstyle.value = vbstyle.value;
        }
    }

    function validateFormOnSubmit()
    {
        movevalues();

        var reason = "";
        var focusset = 0;

        var vmake = document.getElementById("Make");
        var vyear = document.getElementById("Year");
        var vmodel = document.getElementById("Model");
        var vstyle = document.getElementById("Style");

        var vqtype = document.getElementById("QuoteType");
        if(vqtype.options[vqtype.selectedIndex].value == 'Standard')
        {
            var vcmake = document.getElementById("makelist");
            var vcyear = document.getElementById("yearlist");
            var vcmodel = document.getElementById("modellist");
            var vcstyle = document.getElementById("stylelist");
        }
        else
        {
            var vcmake = document.getElementById("makebox");
            var vcyear = document.getElementById("yearbox");
            var vcmodel = document.getElementById("modelbox");
            var vcstyle = document.getElementById("stylebox");
        }

        var text = validateEmpty(vyear, "Year");
        if(text.length > 0)
        {
            reason += text;
            vcyear.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vcyear.focus();
                focusset = 1;
            }
        }
        else if((parseInt(vyear.value,10) < 1901) || (parseInt(vyear.value,10) > 2155))
        {
            reason += 'Year is not valid (must be between 1901-2155).\n';
            vcyear.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vcyear.focus();
                focusset = 1;
            }
        }
        else vcyear.style.background = 'White';

        text = validateEmpty(vmake, "Make");
        if(text.length > 0)
        {
            reason += text;
            vcmake.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vcmake.focus();
                focusset = 1;
            }
        }
        else vcmake.style.background = 'White';

        text = validateEmpty(vmodel, "Model");
        if(text.length > 0)
        {
            reason += text;
            vcmodel.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vcmodel.focus();
                focusset = 1;
            }
        }
        else vcmodel.style.background = 'White';

        text = validateEmpty(vstyle, "Style");
        if(text.length > 0)
        {
            reason += text;
            vcstyle.style.background = '#ffcccc';
            if(focusset == 0)
            {
                vcstyle.focus();
                focusset = 1;
            }
        }
        else vcstyle.style.background = 'White';

        var vcolor = document.getElementById("colorcombo");
        text = validateEmpty(vcolor, "Color Combination")
        if(text.length > 0)
        {
            reason += text;
            if(focusset == 0)
            {
                vcolor.focus();
                focusset = 1;
            }
        }

        var vdel = document.getElementById("DeliveryAddress");
        if(vdel.checked)
        {
            var vcity = document.getElementById("DeliveryCity");
            text = validateEmpty(vcity, "Delivery City")
            if(text.length > 0)
            {
                reason += text;
                if(focusset == 0)
                {
                    vcity.focus();
                    focusset = 1;
                }
            }
            var vstate = document.getElementById("DeliveryState");
            text = validateEmpty(vstate, "Delivery State")
            if(text.length > 0)
            {
                reason += text;
                if(focusset == 0)
                {
                    vstate.focus();
                    focusset = 1;
                }
            }
        }

        var vorder1 = document.getElementById("ordertype1");
        var vorder2 = document.getElementById("ordertype2");
        var vorder3 = document.getElementById("ordertype3");
        var vorder4 = document.getElementById("ordertype4");
        var vorder5 = document.getElementById("ordertype5");
        if(!vorder1.checked && !vorder2.checked && !vorder3.checked && !vorder4.checked && !vorder5.checked)
        {
            reason += "Order Type must be selected."+'\n';
        }

        var vres = document.getElementById("researching");
        if(!vres.checked && vorder1.checked)
        {
            reason += "Order Type can not be 'Unknown' when marked 'Completed'."+'\n';
        }

        // If we are no longer researching, these are required...
        if(!vres.checked)
        {
            // If Group Watchlist Type...we do not care about the Search Period...
            if(!vorder4.checked)
            {
                var vwaitperiod = document.getElementById("WaitPeriod");
                var tString = trimAll(vwaitperiod.value);
                if(tString.length < 1)
                {
                    reason += "Wait Period Days must be entered."+'\n';
                    vwaitperiod.style.background = '#ffcccc';
                    if(focusset == 0)
                    {
                        vwaitperiod.focus();
                        focusset = 1;
                    }
                }
                else vwaitperiod.style.background = 'White';
            }

            var vlowmiles = document.getElementById("Mileage");
            var tString = trimAll(vlowmiles.value);
            if(tString.length < 1)
            {
                reason += "Estimated Mileage amount must be entered."+'\n';
                vlowmiles.style.background = '#ffcccc';
                if(focusset == 0)
                {
                    vlowmiles.focus();
                    focusset = 1;
                }
            }
            else vlowmiles.style.background = 'White';

            var vprice = document.getElementById("PricePoint");
            if(vprice.value.length < 1)
            {
                reason += "Price amount must be entered."+'\n';
                vprice.style.background = '#ffcccc';
                if(focusset == 0)
                {
                    vprice.focus();
                    focusset = 1;
                }
            }
            else vprice.style.background = 'White';
        }

        vprice = document.getElementById("AddDep");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
            vprice.style.background = 'White';
        }
        else
        {
            var amount = parseFloat(vprice.value);
            if((amount < 0.00) || (amount > 250.00))
            {
                reason += "Additional Deposit amount must be between $0.00 and $250.00."+'\n';
                vprice.style.background = '#ffcccc';
                if(focusset == 0)
                {
                    vprice.focus();
                    focusset = 1;
                }
            }
        }

        vprice = document.getElementById("AddSec");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddNav");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddSel");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddPur");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddTire");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        vprice = document.getElementById("AddDelivery");
        if(vprice.value.length < 1)
        {
            vprice.value = '0';
        }

        if(reason != "")
        {
            alert("Some fields need correction:"+'\n' + reason + '\n');
            return false;
        }
        return true;
    }

    function validateEmpty(fld, title)
    {
        var error = "";
        var tString = trimAll(fld.value);

        if (tString.length == 0)
        {
            fld.style.background = '#ffcccc';
            error = title + " is required."+'\n';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    function itemchanged()
    {
        movevalues();

        var vform = document.getElementById('firmquote');
        var vBox = document.createElement('input');
        vBox.type = 'hidden';
        vBox.name = 'RefreshOnly';
        vform.appendChild(vBox);
        vform.submit();
    }

    function typechanged()
    {
        var vqtype = document.getElementById("QuoteType");
        var vltype = document.getElementById("LastType");

        var vmake = document.getElementById("Make");
        var vyear = document.getElementById("Year");
        var vmodel = document.getElementById("Model");
        var vstyle = document.getElementById("Style");

        if(vltype.value == 'Standard')
        {
            var vlmake = document.getElementById("makelist");
            var vlyear = document.getElementById("yearlist");
            var vlmodel = document.getElementById("modellist");
            var vlstyle = document.getElementById("stylelist");
            vmake.value = vlmake.options[vlmake.selectedIndex].value;
            vyear.value = vlyear.options[vlyear.selectedIndex].value;
            vmodel.value = vlmodel.options[vlmodel.selectedIndex].value;
            vstyle.value = vlstyle.options[vlstyle.selectedIndex].value;
        }
        else
        {
            var vbmake = document.getElementById("makebox");
            var vbyear = document.getElementById("yearbox");
            var vbmodel = document.getElementById("modelbox");
            var vbstyle = document.getElementById("stylebox");
            vmake.value = vbmake.value;
            vyear.value = vbyear.value;
            vmodel.value = vbmodel.value;
            vstyle.value = vbstyle.value;
        }

        vltype.value = vqtype.options[vqtype.selectedIndex].value;

        var vsel = document.getElementById('selectboxes');
        var vedit = document.getElementById('editboxes');

        if(vltype.value == 'Standard')
        {
            vsel.style.display = '';
            vedit.style.display = 'none';
        }
        else
        {
            var vbmake = document.getElementById("makebox");
            var vbyear = document.getElementById("yearbox");
            var vbmodel = document.getElementById("modelbox");
            var vbstyle = document.getElementById("stylebox");
            vbmake.value = vmake.value;
            vbyear.value = vyear.value;
            vbmodel.value = vmodel.value;
            vbstyle.value = vstyle.value;

            vsel.style.display = 'none';
            vedit.style.display = '';
        }

        // Reset the form based upon the type...
        var vform = document.getElementById('firmquote');
        var vBox = document.createElement('input');
        vBox.type = 'hidden';
        vBox.name = 'RefreshOnly';
        vform.appendChild(vBox);
        vform.submit();
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
<?php
    if($loaderror != 'false')
    {
        echo '<h1 class="subhead" style="width: 250px;">Firm Quote Error</h1>';
        echo '<div class="grideightgrey">';
        echo '<p class="blackeleven" style="margin: 0;"><a href="mydashboard.php#admintab">Go back to My Dashboard</a></p><br/>';
        echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>There was an error on the page:</strong></p>';
        echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;">';
        echo $loaderror;
        echo '</p>';
    }
    elseif($saveerror != 'false')
    {
        echo '<h1 class="subhead" style="width: 250px;">Firm Quote Error</h1>';
        echo '<div class="grideightgrey">';
        echo '<p class="blackeleven" style="margin: 0;"><a href="mydashboard.php#admintab">Go back to My Dashboard</a></p><br/>';
        echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>There was an error on the page:</strong></p>';
        echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;">';
        echo $saveerror;
        echo '</p>';
    }
    else
    {
        echo '<h1 class="subhead" style="width: 250px;">Firm Quote</h1>';
        echo '<div class="grideightgrey">';
        echo '<p class="blackeleven" style="margin: 0;"><a href="mydashboard.php#admintab">Go back to My Dashboard</a></p><br/>';

        echo '<form action="addfirmquote.php" method="post" name="firmquote" id="firmquote" enctype="multipart/form-data" onsubmit="javascript:return validateFormOnSubmit()" >';
        echo '<input type="hidden" value="'.$inuserid.'" name="ForUserID" />';
        echo '<input type="hidden" value="'.$inmneed.'" name="MarketNeedID" />';
        echo '<input type="hidden" value="'.$quoterequestid.'" name="ForQuoteRequestID" />';
        echo '<input type="hidden" value="'.$firmquoteid.'" name="FirmQuoteID" />';
        echo '<input type="hidden" value="true" name="InPostBack" />';
        echo '<input name="Year" id="Year" type="hidden" size="5" maxlength="4" value="" />';
        echo '<input name="Make" id="Make" type="hidden" size="20" maxlength="50" value="" />';
        echo '<input name="Model" id="Model" type="hidden" size="5" maxlength="30" value="" />';
        echo '<input name="Style" id="Style" type="hidden" size="25" maxlength="25" value="" />';

        echo '<a name="refresh"></a><label for="QuoteType">Quote Type: </label><select name="QuoteType" id="QuoteType" onchange="javascript:typechanged()" >';
        echo '<option value="Standard"';
        if(isset($iquotetype) && $iquotetype=='Standard') echo 'selected="selected"';
        echo '>Standard</option>';
        echo '<option value="Special"';
        if(isset($iquotetype) && $iquotetype=='Special') echo 'selected="selected"';
        echo '>Special</option>';
        echo '<option value="Pickup"';
        if(isset($iquotetype) && $iquotetype=='Pickup') echo 'selected="selected"';
        echo '>Pickup</option>';
        echo '</select>';
        echo '<input type="hidden" value="'.$iquotetype.'" id="LastType" name="LastType" />';
        echo '</div><!-- end grideightgrey-->';

        echo '<h2 class="subhead" style="margin-top:0;">';
        echo 'Vehicle Choice';
        echo '</h2>';

        echo '<div class="grideightgrey" id="selectboxes" ';
        if(isset($iquotetype) && ($iquotetype!='Standard')) echo 'style="display:none"';
        echo '>';
        echo '<table width="600" border="0" cellspacing="0" cellpadding="10">';
        echo '<tr>';
        echo '<td><label for="yearlist">Year</label><br /><select name="yearlist" id="yearlist" onchange="javascript:itemchanged()">';
        $current = date('Y');
        $count = count($years);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$years[$i].'"';
            if(isset($year) && $years[$i]==$year) echo 'selected="selected"';
            else if(!isset($year) && $years[$i]==($current-1)) echo 'selected="selected"';
            echo '>'.$years[$i].'</option>';
        }
        echo '</select></td>';
        echo '<td><label for="makelist">Make</label><br /><select name="makelist" id="makelist" onchange="javascript:itemchanged()">';
        $count = count($makes);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$makes[$i].'"';
            if(isset($make) && $makes[$i]==$make) echo 'selected="selected"';
            echo '>'.$makes[$i].'</option>';
        }
        echo '</select></td>';
        echo '<td><label for="modellist">Model</label><br /><select name="modellist" id="modellist" onchange="javascript:itemchanged()">';
        $count = count($models);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$models[$i].'"';
            if(isset($model) && $models[$i]==$model) echo 'selected="selected"';
            echo '>'.$models[$i].'</option>';
        }
        echo '</select>';
        echo '</td><td width="300"><label for="stylelist">Style</label><br /><select name="stylelist" id="stylelist" onchange="javascript:itemchanged()">';
        $count = count($styles);
        for($i = 1; $i <= $count; $i++)
        {
            echo '<option value="'.$styles[$i].'"';
            if(isset($style) && $styles[$i]==$style) echo 'selected="selected"';
            echo '>'.$styles[$i].'</option>';
        }
        echo '</select></td>';
        if(isset($quoterequestid) && ($quoterequestid != -1))
        {
            echo '</tr><tr><td colspan="4">';
            echo '<center>Image is representative of the selections made when last saved (may not match current selections).<br/>';
            $max_width = 400;
            $max_height = 400;
            if(isset($imagefile) && !is_null($imagefile)) echo '<img id="vehimage" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            else echo '** Image currently not available **';
            echo '</center></td></tr><tr><td colspan="4">';
?>
            <table width="400" bordercolor="#142c3c" border="1" cellpadding="0" cellspacing="0"  bgcolor="#ebebeb" align="center">
                <tr>
                    <td width="200" align="center" bgcolor="#142c3c" style="color:#FFF; font-size:13px;"><b>AVERAGE MILEAGE</b></td>
                    <td width="200" align="center" bgcolor="#142c3c" style="color:#FFF; font-size:13px;"><b>LOW MILEAGE</b></td>
                </tr>
                <tr>
                    <td width="400" colspan="2" align="center" bgcolor="#142c3c">
                        <table width="400" border="0" cellpadding="0" cellspacing="0">
                            <!-- mileage -->
                            <tr valign="middle" style="font-size:13px; color:#142c3c;">
                                <td width="80" height="20" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($highmileend); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($highmilestart+(($highmileend-$highmilestart)/2)); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($highmilestart); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($lowmilestart+(($highmilestart-$lowmilestart)/2)); ?>
                                </td>
                                <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($lowmilestart); ?>
                                </td>
                            </tr><!-- end mileage -->
                            <!-- top line of hash marks -->
                            <tr>
                                <td height="3" width="40" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="38" bgcolor="#ebebeb" align="center"></td>
                            </tr><!-- end top line of hash marks -->
                            <!-- middle black bar -->
                            <tr>
                                <td bgcolor="#ebebeb" height="2"></td>
                                <td colspan="13" height="2"></td>
                                <td bgcolor="#ebebeb" height="2"></td>
                            </tr><!-- end middle black bar -->
                            <!-- bottom line of hash marks -->
                            <tr>
                                <td height="3" width="40" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="39" bgcolor="#ebebeb" align="center"></td>
                                <td width="2" bgcolor="#142c3c" align="center"></td>
                                <td width="38" bgcolor="#ebebeb" align="center"></td>
                            </tr><!-- end bottom line of hash marks -->
                            <!-- excellent availability section -->
                            <tr valign="bottom">
                                <td height="20" bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                <td colspan="6" align="center" bgcolor="#ebebeb">
<?php if($highbb == 1) echo '<span class="excellent">Excellent Availability!</span>'; ?>
                                </td>
                                <td></td>
                                <td colspan="6" align="center" bgcolor="#ebebeb">
<?php if($lowbb == 1) echo '<span class="excellent">Excellent Availability!</span>'; ?>
                                </td>
                                <td height="20" bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                            </tr><!-- end excellent availability section -->
                            <!-- estimates -->
                            <tr style="font-size:13px; color:#142c3c;">
                                <td bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                <td class="bodybold" bgcolor="#ebebeb" align="center" colspan="6">
<?php
    if(($lowpricestart == $highpricestart) && ($lowpriceend == $highpriceend))
    {
        echo '$'.number_format($highpricestart).' to $'.number_format($highpricestart + (($highpriceend-$highpricestart)/2));
    }
    else if($highpricestart < 5000) echo 'Below $'.number_format($highpriceend);
    else echo '$'.number_format($highpricestart).' to $'.number_format($highpriceend);
?>
                                </td>
                                <td></td>
                                <td class="bodybold" bgcolor="#ebebeb" align="center" colspan="6">
<?php
    if(($lowpricestart == $highpricestart) && ($lowpriceend == $highpriceend))
    {
        echo '$'.number_format($lowpricestart + (($lowpriceend-$lowpricestart)/2)).' to $'.number_format($lowpriceend);
    }
    else if($lowpricestart < 5000) echo 'Below $'.number_format($lowpriceend);
    else echo '$'.number_format($lowpricestart).' to $'.number_format($lowpriceend);
?>
                                </td>
                                <td bgcolor="#ebebeb" height="20"><img src="common/layout/trans.gif" width="1" height="1"></td>
                            </tr>
                            <!-- end estimates -->
                        </table>
                    </td>
                </tr>
            </table>
<?php
            echo '</td>';
        }
        echo '</tr></table></div><!-- end grideightgrey-->';

        echo '<div class="grideightgrey" id="editboxes" ';
        if(!isset($iquotetype) || ($iquotetype=='Standard')) echo 'style="display:none"';
        echo '>';
        echo '<table width="600" border="0" cellspacing="0" cellpadding="10">';
        echo '<tr>';
        echo '<td>';
        echo '<label for="yearbox">Year</label>';
        echo '<br />';
        echo '<input name="yearbox" id="yearbox" type="text" size="5" maxlength="4" value="';
        if(isset($iyear)) echo $iyear;
        echo '" onkeypress="javascript:return numbersonly(event);" />';
        echo '</td>';
        echo '<td>';
        echo '<label for="makebox">Make</label>';
        echo '<br />';
        echo '<input name="makebox" id="makebox" type="text" size="20" maxlength="50" value="';
        if(isset($imake)) echo $imake;
        echo '" />';
        echo '</td>';
        echo '<td>';
        echo '<label for="modelbox">Model</label>';
        echo '<br />';
        echo '<input name="modelbox" id="modelbox" type="text" size="5" maxlength="30" value="';
        if(isset($imodel)) echo $imodel;
        echo '" />';
        echo '</td>';
        echo '<td width="300">';
        echo '<label for="stylebox">Style</label>';
        echo '<br />';
        echo '<input name="stylebox" id="stylebox" type="text" size="25" maxlength="25" value="';
        if(isset($istyle)) echo $istyle;
        echo '" />';
        echo '</td>';
        echo '</tr></table></div><!-- end grideightgrey-->';
?>
            <h2 class="subhead"> Vehicle Preferences</h2>
            <div class="grideightgrey">
                <table width="300" border="0" cellspacing="0" cellpadding="10" align="left">
                <tr>
                    <td>
                        <label for="selectmileage"><strong>Mileage Ceiling</strong></label>
                        <br />
                        <select id="selectmileage" name="selectmileage" >
<?php
    $count = count($pmiles);
    for($i = 1; $i <= $count; $i++)
    {
        echo '<option value="'.$pmiles[$i].'"';
        if(isset($imileage) && ($pmiles[$i]==$imileage)) echo 'selected="selected"';
        elseif(!isset($imileage) && ($pmiles[$i]==60000)) echo 'selected="selected"';
        echo '>'.number_format($pmiles[$i]).'</option>';
    }
?>
                        </select>
                    </td>
                    <td>
                        <label for="selectseat"><strong>Seat Material</strong></label>
                        <br />
                        <select name="selectseat" id="selectseat">
                            <option value="Flexible" <?php if(isset($iseattype) && ($iseattype == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Cloth" <?php if(!isset($iseattype) || ($iseattype == 'Cloth')) echo 'selected="selected"'; ?>>Cloth</option>
                            <option value="Leather" <?php if(isset($iseattype) && ($iseattype == 'Leather')) echo 'selected="selected"'; ?>>Leather</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="selectengine"><strong>Engine</strong></label>
                        <br />
                        <select name="selectengine" id="selectengine">
                            <option value="0" <?php if(isset($iengine) && ($iengine == 0)) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="4" <?php if(!isset($iengine) || ($iengine == 4)) echo 'selected="selected"'; ?>>4 cylinder</option>
                            <option value="5" <?php if(isset($iengine) && ($iengine == 5)) echo 'selected="selected"'; ?>>5 cylinder</option>
                            <option value="6" <?php if(isset($iengine) && ($iengine == 6)) echo 'selected="selected"'; ?>>6 cylinder</option>
                            <option value="8" <?php if(isset($iengine) && ($iengine == 8)) echo 'selected="selected"'; ?>>8 cylinder</option>
                            <option value="10" <?php if(isset($iengine) && ($iengine == 10)) echo 'selected="selected"'; ?>>10 cylinder</option>
                            <option value="12" <?php if(isset($iengine) && ($iengine == 12)) echo 'selected="selected"'; ?>>12 cylinder</option>
                        </select>
                    </td>
                    <td>
                        <label for="selectdrive"><strong>Wheel Drive</strong></label>
                        <br />
                        <select name="selectdrive" id="selectdrive">
                            <option value="Flexible" <?php if(isset($idrive) && ($idrive == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Rear" <?php if(!isset($idrive) || ($idrive == 'Rear')) echo 'selected="selected"'; ?>>Rear</option>
                            <option value="Front" <?php if(isset($idrive) && ($idrive == 'Front')) echo 'selected="selected"'; ?>>Front</option>
                            <option value="Four" <?php if(isset($idrive) && ($idrive == 'Four')) echo 'selected="selected"'; ?>>Four</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="selecttrans"><strong>Transmission</strong></label>
                        <br />
                        <select name="selecttrans" id="selecttrans">
                            <option value="Flexible" <?php if(isset($itrans) && ($itrans == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Automatic" <?php if(!isset($itrans) || ($itrans == 'Automatic')) echo 'selected="selected"'; ?>>Automatic</option>
                            <option value="Manual" <?php if(isset($itrans) && ($itrans == 'Manual')) echo 'selected="selected"'; ?>>Manual</option>
                        </select>
                    </td>
                    <td>
                        <label for="selectwheels"><strong>Wheel Type</strong></label>
                        <br />
                        <select name="selectwheels" id="selectwheels">
                            <option value="Flexible" <?php if(isset($iwheels) && ($iwheels == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Alloy" <?php if(!isset($iwheels) || ($iwheels == 'Alloy')) echo 'selected="selected"'; ?>>Alloy</option>
                            <option value="Chrome" <?php if(isset($iwheels) && ($iwheels == 'Chrome')) echo 'selected="selected"'; ?>>Chrome</option>
                            <option value="Hubcaps" <?php if(isset($iwheels) && ($iwheels == 'Hubcaps')) echo 'selected="selected"'; ?>>Hubcaps</option>
                        </select>
                    </td>
                </tr>
                <tr>
<?php
    if($iquotetype == 'Pickup')
    {
?>
                    <td valign="top">
                        <label for="selectcab"><strong>Cab Type</strong></label>
                        <br />
                        <select name="selectcab" id="selectcab">
                            <option value="Flexible" <?php if(isset($icab) && ($icab == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Regular" <?php if(!isset($icab) || ($icab == 'Regular')) echo 'selected="selected"'; ?>>Regular</option>
                            <option value="Extended" <?php if(isset($icab) && ($icab == 'Extended')) echo 'selected="selected"'; ?>>Extended</option>
                            <option value="FourDoor" <?php if(isset($icab) && ($icab == 'FourDoor')) echo 'selected="selected"'; ?>>Four Door</option>
                        </select>
                        <br /><br />
                        <label for="selectfrontseat"><strong>Front Seat Type</strong></label>
                        <br />
                        <select name="selectfrontseat" id="selectfrontseat">
                            <option value="Flexible" <?php if(isset($ifront) && ($ifront == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Bench" <?php if(!isset($ifront) || ($ifront == 'Bench')) echo 'selected="selected"'; ?>>Bench with Armrest</option>
                            <option value="Standard" <?php if(isset($ifront) && ($ifront == 'Standard')) echo 'selected="selected"'; ?>>Standard Bench Seat</option>
                            <option value="Bucket" <?php if(isset($ifront) && ($ifront == 'Bucket')) echo 'selected="selected"'; ?>>Bucket Seats</option>
                        </select>
                        <br /><br />
                        <label for="selectbedtype"><strong>Bed Type</strong></label>
                        <br />
                        <select name="selectbedtype" id="selectbedtype">
                            <option value="Flexible" <?php if(isset($ibedtype) && ($ibedtype == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="Mini" <?php if(isset($ibedtype) && ($ibedtype == 'Mini')) echo 'selected="selected"'; ?>>Mini bed (about 4 ft)</option>
                            <option value="Short" <?php if(!isset($ibedtype) || ($ibedtype == 'Short')) echo 'selected="selected"'; ?>>Short bed (about 6 ft)</option>
                            <option value="Long" <?php if(isset($ibedtype) && ($ibedtype == 'Long')) echo 'selected="selected"'; ?>>Long bed (about 8 ft)</option>
                            <option value="ExtraLong" <?php if(isset($ibedtype) && ($ibedtype == 'ExtraLong')) echo 'selected="selected"'; ?>>Extra Long bed (about 10 ft)</option>
                        </select>
                    </td>
<?php
    }
    else
    {
?>
                    <td valign="top">
                        <label for="selectsunroof"><strong>Sunroof</strong></label>
                        <br />
                        <select name="selectsunroof" id="selectsunroof">
                            <option value="Flexible" <?php if(isset($isunroof) && ($isunroof == 'Flexible')) echo 'selected="selected"'; ?>>Flexible</option>
                            <option value="None" <?php if(!isset($isunroof) || ($isunroof == 'None')) echo 'selected="selected"'; ?>>No Thanks</option>
                            <option value="Glass" <?php if(isset($isunroof) && ($isunroof == 'Glass')) echo 'selected="selected"'; ?>>Yes</option>
                        </select>
                    </td>
<?php
    }
?>
                    <td valign="top">
                        <input name="RadFM" type="checkbox" value="FM" <?php if(!isset($iradfm) || ($iradfm == 1)) echo 'checked="checked"'; ?> />AM/FM<BR />
                        <input name="RadCas" type="checkbox" value="Cassette" <?php if(isset($iradcas) && ($iradcas == 1)) echo 'checked="checked"'; ?> />Cassette<BR />
                        <input name="RadCD" type="checkbox" value="CD" <?php if(isset($iradcd) && ($iradcd == 1)) echo 'checked="checked"'; ?> />CD<BR />
                        <input name="RadFlex" type="checkbox" value="Flexible" <?php if(isset($iradflex) && ($iradflex == 'Flexible')) echo 'checked="checked"'; ?> />Flexible<BR />
                    </td>
                </tr>
                </table>
                <table width="275" border="0" cellspacing="0" cellpadding="3" align="right">
                <tr>
                    <td>&nbsp;</td>
                    <td align="center" width="45"><p class="greyeleven" style="color:black">Yes</p></td>
                    <td align="center" width="45"><p class="greyeleven" style="color:black">No</p></td>
                    <td align="center" width="45"><p class="greyeleven" style="color:black">Flexible</p></td>
                </tr>
                <tr>
                    <td><label for="PowerDoorLocks">Power Door Locks</label></td>
                    <td align="center"><input name="PowerDoorLocks" type="radio" value="Yes" <?php if(!isset($ipdoor) || ($ipdoor == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerDoorLocks" type="radio" value="No" <?php if(isset($ipdoor) && ($ipdoor == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerDoorLocks" type="radio" value="Flexible" <?php if(isset($ipdoor) && ($ipdoor == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="PowerWindows">Power Windows</label></td>
                    <td align="center"><input name="PowerWindows" type="radio" value="Yes" <?php if(!isset($ipwin) || ($ipwin == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerWindows" type="radio" value="No" <?php if(isset($ipwin) && ($ipwin == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerWindows" type="radio" value="Flexible" <?php if(isset($ipwin) && ($ipwin == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="PowerSeats">Power Seats</label></td>
                    <td align="center"><input name="PowerSeats" type="radio" value="Yes" <?php if(isset($ipseat) && ($ipseat == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerSeats" type="radio" value="No" <?php if(!isset($ipseat) || ($ipseat == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="PowerSeats" type="radio" value="Flexible" <?php if(isset($ipseat) && ($ipseat == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="HeatedSeats">Heated Seats</label></td>
                    <td align="center"><input name="HeatedSeats" type="radio" value="Yes" <?php if(isset($iheat) && ($iheat == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="HeatedSeats" type="radio" value="No" <?php if(!isset($iheat) || ($iheat == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="HeatedSeats" type="radio" value="Flexible" <?php if(isset($iheat) && ($iheat == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="AirConditioning">Air Conditioning</label></td>
                    <td align="center"><input name="AirConditioning" type="radio" value="Yes" <?php if(!isset($iair) || ($iair == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="AirConditioning" type="radio" value="No" <?php if(isset($iair) && ($iair == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="AirConditioning" type="radio" value="Flexible" <?php if(isset($iair) && ($iair == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="RemoteEntry">Remote Entry</label></td>
                    <td align="center"><input name="RemoteEntry" type="radio" value="Yes" <?php if(isset($iremote) && ($iremote == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RemoteEntry" type="radio" value="No" <?php if(!isset($iremote) || ($iremote == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RemoteEntry" type="radio" value="Flexible" <?php if(isset($iremote) && ($iremote == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="TractionControl">Traction Control</label></td>
                    <td align="center"><input name="TractionControl" type="radio" value="Yes" <?php if(isset($itraction) && ($itraction == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TractionControl" type="radio" value="No" <?php if(!isset($itraction) || ($itraction == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TractionControl" type="radio" value="Flexible" <?php if(isset($itraction) && ($itraction == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="SecuritySystem">Security System</label></td>
                    <td align="center"><input name="SecuritySystem" type="radio" value="Yes" <?php if(isset($isecure) && ($isecure == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="SecuritySystem" type="radio" value="No" <?php if(!isset($isecure) || ($isecure == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="SecuritySystem" type="radio" value="Flexible" <?php if(isset($isecure) && ($isecure == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="CruiseControl">Cruise Control</label></td>
                    <td align="center"><input name="CruiseControl" type="radio" value="Yes" <?php if(!isset($icruise) || ($icruise == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="CruiseControl" type="radio" value="No" <?php if(isset($icruise) && ($icruise == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="CruiseControl" type="radio" value="Flexible" <?php if(isset($icruise) && ($icruise == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="Navigation">Navigation</label></td>
                    <td align="center"><input name="Navigation" type="radio" value="Yes" <?php if(isset($inavsys) && ($inavsys == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="Navigation" type="radio" value="No" <?php if(!isset($inavsys) || ($inavsys == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="Navigation" type="radio" value="Flexible" <?php if(isset($inavsys) && ($inavsys == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
<?php
    if($iquotetype == 'Pickup')
    {
?>
                <tr>
                    <td><label for="RearSlidingWindow">Rear Sliding Window</label></td>
                    <td align="center"><input name="RearSlidingWindow" type="radio" value="Yes" <?php if(isset($irearwin) && ($irearwin == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RearSlidingWindow" type="radio" value="No" <?php if(!isset($irearwin) || ($irearwin == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="RearSlidingWindow" type="radio" value="Flexible" <?php if(isset($irearwin) && ($irearwin == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="BedLiner">Bed Liner</label></td>
                    <td align="center"><input name="BedLiner" type="radio" value="Yes" <?php if(!isset($ibed) || ($ibed == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="BedLiner" type="radio" value="No" <?php if(isset($ibed) && ($ibed == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="BedLiner" type="radio" value="Flexible" <?php if(isset($ibed) && ($ibed == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
                <tr>
                    <td><label for="TowPackage">Tow Package</label></td>
                    <td align="center"><input name="TowPackage" type="radio" value="Yes" <?php if(isset($itow) && ($itow == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TowPackage" type="radio" value="No" <?php if(!isset($itow) || ($itow == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="TowPackage" type="radio" value="Flexible" <?php if(isset($itow) && ($itow == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
<?php
    }
    else
    {
?>
                <tr>
                    <td><label for="LuggageRack">Luggage Rack</label></td>
                    <td align="center"><input name="LuggageRack" type="radio" value="Yes" <?php if(isset($iluggage) && ($iluggage == 'Yes')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="LuggageRack" type="radio" value="No" <?php if(!isset($iluggage) || ($iluggage == 'No')) echo 'checked="checked"'; ?> /></td>
                    <td align="center"><input name="LuggageRack" type="radio" value="Flexible" <?php if(isset($iluggage) && ($iluggage == 'Flexible')) echo 'checked="checked"'; ?> /></td>
                </tr>
<?php
    }
?>
                </table>
                <br clear="all" />
                <br />
                <table width="600" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="300">
                        <label for="colorcombo">* Preferred color combinations:</label>
                    </td>
                    <td width="300">
                        <label for="notes">Special notes:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea name="colorcombo" id="colorcombo" cols="37" rows="4"><?php if(isset($icolors)) echo $icolors; ?></textarea>
                    </td>
                    <td>
                        <textarea name="notes" id="notes" cols="37" rows="4"><?php if(isset($inotes)) echo $inotes; ?></textarea>
                    </td>
                </tr>
                </table>
                <br />
                <h4 class="subhead" style="font-size:18px;">Delivery Options</h4>
                <br />
                <table width="325" border="0" cellspacing="0" cellpadding="5">
                <tr valign="middle">
                    <td>
                        <input name="DeliveryPickup" id="DeliveryPickup" type="radio" value="PickUp" <?php if(!isset($idelorpick) || ($idelorpick == 1)) echo 'checked="checked"'; ?> />
                    </td>
                    <td colspan="2">
                        <label for="DeliveryPickup">I will pick up at my 1-800-vehicles dealer location</label>
                    </td>
                </tr>
                <tr valign="middle">
                    <td>
                        <input name="DeliveryPickup" id="DeliveryAddress" type="radio" value="Deliver" <?php if(isset($idelorpick) && ($idelorpick == 0)) echo 'checked="checked"'; ?> />
                    </td>
                    <td colspan="2">
                        <label for="DeliveryAddress">Please deliver my vehicle to:</label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <label for="DeliveryCity"><p class="greyeleven" style="margin-top:0; margin-left:5px;">City</p></label>
                        <input name="DeliveryCity" id="DeliveryCity" type="text" value="<?php if(isset($idelcity)) echo $idelcity; ?>" size="15" maxlength="50" />
                    </td>
                    <td>
                        <label for="DeliveryState"><p class="greyeleven" style="margin-top:0; margin-left:5px;">State</p></label>
                        <input name="DeliveryState" id="DeliveryState" type="text" value="<?php if(isset($idelstate)) echo $idelstate; ?>" size="15" maxlength="2" />
                    </td>
                </tr>
                </table>
                <br />
            </div><!-- end grideightgrey-->
            <h2 class="subhead">Firm Quote Details</h2>
            <div class="grideightgrey">
<?php
        echo '<input type="checkbox" id="researching" name="researching" ';
        if($fstatus == 1) echo 'checked="checked"';
        echo ' /> Still Researching? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Once removed, the customer will not be able to edit and will see it as Received)<br/>';
        echo '<br/><input type="checkbox" id="visible" name="visible" ';
        if($fvisible == 1) echo 'checked="checked"';
        echo ' /> Visible to Customer?<br/>';
        echo '<br/>Order Type:<br/>';
        echo '<input type="radio" id="ordertype1" name="ordertype" value="Unknown" ';
        if($fordertype == 'Unknown') echo 'checked="checked"';
        echo ' /> Unknown<br/>';
        echo '<input type="radio" id="ordertype2" name="ordertype" value="Standard" ';
        if($fordertype == 'Standard') echo 'checked="checked"';
        echo ' /> Standard<br/>';
        echo '<input type="radio" id="ordertype3" name="ordertype" value="Special" ';
        if($fordertype == 'Special') echo 'checked="checked"';
        echo ' /> Special<br/>';
        echo '<input type="radio" id="ordertype4" name="ordertype" value="Group" ';
        if($fordertype == 'Group') echo 'checked="checked"';
        echo ' /> Group Watchlist<br/>';
        echo '<input type="radio" id="ordertype5" name="ordertype" value="No Order or Posting Available" ';
        if($fordertype == 'No Order or Posting Available') echo 'checked="checked"';
        echo ' /> Cannot be Ordered or Posted<br/>';
        echo '<br/>Exclusive Wait Period Days:<br/>';
        echo '<input name="WaitPeriod" id="WaitPeriod" type="text" value="';
        if(isset($fwaitperiod)) echo number_format($fwaitperiod,0,'.','');
        echo '" size="10" maxlength="3" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Estimated Mileage:<br/>';
        echo '<input name="Mileage" id="Mileage" type="text" value="';
        if(isset($fmiles)) echo number_format($fmiles,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Price:<br/>';
        echo '$<input name="PricePoint" id="PricePoint" type="text" value="';
        if(isset($fprice)) echo number_format($fprice,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Additional Deposit:<br/>';
        echo '$<input name="AddDep" id="AddDep" type="text" value="';
        if(isset($fadddep)) echo number_format($fadddep,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /> Note: No more than $250 can be added here.<br/>';
        echo '<br/>Additional Second Key Limit:<br/>';
        echo '<input name="AddSec" id="AddSec" type="text" value="';
        if(isset($faddsec)) echo number_format($faddsec,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Additional Nav Disc Limit:<br/>';
        echo '$<input name="AddNav" id="AddNav" type="text" value="';
        if(isset($faddnav)) echo number_format($faddnav,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Additional Seller Repair Limit:<br/>';
        echo '$<input name="AddSel" id="AddSel" type="text" value="';
        if(isset($faddsel)) echo number_format($faddsel,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Additional Purchaser Repair Limit:<br/>';
        echo '$<input name="AddPur" id="AddPur" type="text" value="';
        if(isset($faddpur)) echo number_format($faddpur,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Additional Tire Copay:<br/>';
        echo '$<input name="AddTire" id="AddTire" type="text" value="';
        if(isset($faddtire)) echo number_format($faddtire,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Delivery Cost (if any):<br/>';
        echo '$<input name="AddDelivery" id="AddDelivery" type="text" value="';
        if(isset($fadddel)) echo number_format($fadddel,0,'.','');
        echo '" size="10" maxlength="7" onkeypress="javascript:return numbersonly(event);" /><br/>';
        echo '<br/>Note:<br/>';
        echo '<textarea id="AdminNote" name="AdminNote" rows="3" cols="70">';
        if(isset($fnote)) echo $fnote;
        echo '</textarea><br/><br/>';
        echo '<input type="file" name="imagename" id="imagename" value="" /><br/>';

        //echo 'Images: 1)'.$fimage.' 2)'.$imagefile.'<br/>';

        $imagefile = '';
        if(isset($fimage))
        {
            if(!file_exists($fimage)) $imagefile = '';
            else
            {
                $imagefile = $fimage;
                echo '<input type="checkbox" id="RemImg" name="RemImg" /> Remove Image?<br/>';
                echo 'Current Image:<br/>';
                echo '<input type="hidden" value="'.$imagefile.'" name="PriorImage" />';
            }
        }

        if(!isset($imagefile) || (strlen($imagefile) < 1))
        {
            if($iquotetype == 'Standard')
            {
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    // First fill the list of Makes...
                    mysql_select_db(DB_SERVER_DATABASE, $con);

                    $result = mysql_query("select v.ImageFile from vehicles v, makes m where m.makeid=v.makeid and m.name='".$imake."' and v.year=".$iyear." and v.model='".$imodel."' and v.style='".$istyle."'");
                    if($result && $row = mysql_fetch_array($result))
                    {
                        if(strlen($row[0]) > 0)
                        {
                            $imagefile = $row[0];
                            echo 'Standard Image:<br/>';
                        }
                    }
                    else $imagefile = '';
                    mysql_close($con);
                }
                else $imagefile = '';
            }
        }

        if(isset($imagefile) && (strlen($imagefile) > 0))
        {
            $max_width = 200;
            $max_height = 200;
            echo '<center><img id="vehimage" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" /></center>';
            echo '<center>Actual year, make, model, and style</center>';
        }
        else echo 'Current Image: none set<br/>';

        echo '<br/><br/><button type="submit" value="" class="med">SAVE FIRM QUOTE</button>';
        echo '</p>';
        echo '</form>';
    }
?>
        </div>
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
