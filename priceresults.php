<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 3;
    $_SESSION['titleadd'] = 'Research by Price';

    if(isset($_REQUEST["sortby"])) $sortby = $_REQUEST["sortby"];
    $type = $_REQUEST["typelist"];
    $from = $_REQUEST["fromselect"];
    $style = $_REQUEST["stylelist"];
    $startprice = $_REQUEST["pricestart"];
    $maxprice = $_REQUEST["priceend"];
    if(isset($_REQUEST["makeitem"])) $make = $_REQUEST["makeitem"];
    if(isset($_REQUEST["beginat"])) $beginat = $_REQUEST["beginat"];
    else $beginat = 0;
    $atatime = 25;

    if(!isset($type) || !isset($from) || !isset($style) || !isset($startprice) || !isset($maxprice)) header('Location: researchprice.php');

    switch($style)
    {
        //case "Don't Care":
        case "4 Door Sedan":
            $styleadd = " and v.doors=4 and v.convertible='No'";
            break;
        case "2 Door Coupe":
            $styleadd = " and v.doors=2 and v.convertible='No'";
            break;
        case "2 Door Convertible":
            $styleadd = " and v.doors=2 and v.convertible='Yes'";
            break;
        case "3 Door Hatchback":
            $styleadd = " and v.doors=3 and v.convertible='No' and v.bodytype='Hatchback'";
            break;
        case "5 Door Hatchback":
            $styleadd = " and v.doors=5 and v.convertible='No' and v.bodytype='Hatchback'";
            break;
        case "5 Door Wagon":
            $styleadd = " and v.doors=5 and v.convertible='No' and v.bodytype!='Hatchback'";
            break;
        case "2 Door 2WD":
            $styleadd = " and v.doors=2 and v.wheeldrive='2'";
            break;
        case "2 Door 4WD":
            $styleadd = " and v.doors=2 and v.wheeldrive='4'";
            break;
        case "4 Door 2WD":
            $styleadd = " and v.doors=4 and v.wheeldrive='2'";
            break;
        case "4 Door 4WD":
            $styleadd = " and v.doors=4 and v.wheeldrive='4'";
            break;
        case "Dual Sliding Doors":
            $styleadd = " and v.slidingdoors='Dual'";
            break;
        default:
            $styleadd = "";
            break;
    }

    if(($startprice != 'Any') && ($maxprice != 'Any') && ($startprice > $maxprice)) $maxprice = 'Any';
    if($startprice != 'Any') $pstartadd = " and e.pricestart >= ".$startprice;
    if($maxprice != 'Any') $pendadd = " and e.priceend <= ".$maxprice;

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select m.name 'Make', count(*) 'Amount' from vehicles v,vehicledata e, makes m where e.lowmiles=0 and m.makeid=v.makeid and v.vehicleid=e.vehicleid and v.visible=1";
        if(isset($type)) $query .= " and v.type='".$type."'";
        if(isset($from) && ($from != 'Any')) $query .= " and m.origin='".$from."'";
        if(isset($styleadd)) $query .= $styleadd;
        if(isset($pstartadd)) $query .= $pstartadd;
        if(isset($pendadd)) $query .= $pendadd;
        if(isset($make)) $query .= " and m.name='".$make."'";
        $query .= " group by m.name order by 1";
        $result = mysql_query($query);
        $index = 1;
        while($result && $row = mysql_fetch_array($result))
        {
            $makes[$index] = $row['Make'];
            $amounts[$index] = $row['Amount'];
            $index++;
        }

        $query = "select count(*) 'Total' from vehicles v, vehicledata e, makes m where e.lowmiles=0 and m.makeid=v.makeid and v.vehicleid=e.vehicleid and v.visible=1";
        if(isset($type)) $query .= " and v.type='".$type."'";
        if(isset($from) && ($from != 'Any')) $query .= " and m.origin='".$from."'";
        if(isset($styleadd)) $query .= $styleadd;
        if(isset($pstartadd)) $query .= $pstartadd;
        if(isset($pendadd)) $query .= $pendadd;
        if(isset($make)) $query .= " and m.name='".$make."'";
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $howmany = $row['Total'];
        }

        //$query = "select m.name 'Make', v.year 'Year', v.model 'Model', v.style 'Style', e.pricestart 'PS', e.priceend 'PE', e.mileagestart 'MS', e.mileageend 'ME', e.bestbuy 'BB', v.vehicleid 'VID' from vehicles v, vehicledata e, makes m where e.lowmiles=0 and m.makeid=v.makeid and v.vehicleid=e.vehicleid and v.visible=1";
        $query = "select m.name 'Make', v.year 'Year', v.model 'Model', v.style 'Style', min(e.pricestart) 'PS', max(e.priceend) 'PE', min(e.mileagestart) 'MS', max(e.mileageend) 'ME', max(e.bestbuy) 'BB', v.vehicleid 'VID' from vehicles v, vehicledata e, makes m where m.makeid=v.makeid and v.vehicleid=e.vehicleid and v.visible=1";
        if(isset($type)) $query .= " and v.type='".$type."'";
        if(isset($from) && ($from != 'Any')) $query .= " and m.origin='".$from."'";
        if(isset($styleadd)) $query .= $styleadd;
        if(isset($pstartadd)) $query .= $pstartadd;
        if(isset($pendadd)) $query .= $pendadd;
        if(isset($make)) $query .= " and m.name='".$make."'";
        $query .= " group by v.vehicleid, m.name, v.year, v.model, v.style";

        if(isset($sortby))
            $sortingby = $sortby;
        else
            $sortingby = 'default';

        // 1 make, 2 year, 3, model, 4 style, 5 price,
        switch($sortingby)
        {
            case 'makefirst':
                $query .= " order by 1,3,2 desc,4,5";
                break;
            case 'pricefirstup':
                $query .= " order by 5,6,2,1,3,4";
                break;
            case 'pricefirstdown':
                $query .= " order by 5 desc,6,2,1,3,4";
                break;
            case 'yearfirstup':
                $query .= " order by 2,1,3,4,5";
                break;
            case 'yearfirstdown':
            case 'default':
            default:
                $query .= " order by 2 desc,1,3,4,5";
                break;
        }

        $query .= ' limit '.$beginat.','.$atatime;
        $result = mysql_query($query);
        $index = 1;
        while($result && $row = mysql_fetch_array($result))
        {
            $makedets[$index] = $row['Make'];
            $yeardets[$index] = $row['Year'];
            $modeldets[$index] = $row['Model'];
            $styledets[$index] = $row['Style'];
            $psdets[$index] = $row['PS'];
            $pedets[$index] = $row['PE'];
            $msdets[$index] = $row['MS'];
            $medets[$index] = $row['ME'];
            $bbdets[$index] = $row['BB'];
            $vehid[$index] = $row['VID'];
            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript" language="JavaScript">
    function itemchanged()
    {
        var vsort = document.getElementById("sortbox");
        var vmax = document.getElementById("maxpricehidden");
        var vstart = document.getElementById("startpricehidden");
        var vstyle = document.getElementById("stylehidden");
        var vfrom = document.getElementById("fromhidden");
        var vtype = document.getElementById("typehidden");
        var vmake = document.getElementById("makehidden");
        var newsite = 'priceresults.php?sortby=' + vsort.options[vsort.selectedIndex].value;
        if(vmax.value != '') newsite += '&priceend=' + vmax.value;
        if(vstart.value != '') newsite += '&pricestart=' + vstart.value;
        if(vstyle.value != '') newsite += '&stylelist=' + vstyle.value;
        if(vfrom.value != '') newsite += '&fromselect=' + vfrom.value;
        if(vtype.value != '') newsite += '&typelist=' + vtype.value;
        if(vmake.value != '') newsite += '&makeitem=' + vmake.value;
        newsite += '#results';
        self.location = newsite;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer"><a name="results"></a>
        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Research by Price Ranges Results</h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0;"><a href="researchvehicles.php">Go back to Research Vehicles</a></p>
            <p class="blackeleven" style="margin: 0;">
<?php
        echo '<a href="researchprice.php?typeitem='.$type.'&fromselect='.$from.'&styleitem='.$style.'&startprice='.$startprice.'&maxprice='.$maxprice;
        echo '">Start the search over with new criteria</a>';
?>
            </p>
            <br />
            <h3 class="greensub">
<?php
    if(($startprice == 'Any') && ($maxprice == 'Any')) echo 'Results with price range wide open.';
    elseif($startprice == 'Any') echo 'Results with a maximum price of $'.number_format($maxprice);
    elseif($maxprice == 'Any') echo 'Results with a price exceeding $'.number_format($startprice);
    else echo 'Results within the price range: $'.number_format($startprice).'-$'.number_format($maxprice);
?>
            </h3>
            <br />
<?php //echo $query.'<br/>';?>
        <form action="priceresults.php" method="get" name="priceresult">
            <table class="table borderless">
<?php
    $count = count($makes);
    if($count < 1)
    {
        echo '<tr><td>No results found.</td></tr>';
    }
    else
    {
        $row = 1;
        $col = 1;
        while((($row*4)-4)<$count)
        {
            echo '<tr class="priceresult-table">';
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '>';
                if($make != $makes[$col])
                {
                    echo '<a href="priceresults.php?makeitem='.$makes[$col].'&typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice;
                    if(isset($sortby)) echo '&sortby='.$sortby;
                    echo '#results">';
                }
                echo $makes[$col].' ('.$amounts[$col].')';
                if($make != $makes[$col]) echo '</a>';
                echo '</td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '>';
                if($make != $makes[$col])
                {
                    echo '<a href="priceresults.php?makeitem='.$makes[$col].'&typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice;
                    if(isset($sortby)) echo '&sortby='.$sortby;
                    echo '#results">';
                }
                echo $makes[$col].' ('.$amounts[$col].')';
                if($make != $makes[$col]) echo '</a>';
                echo '</td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '>';
                if($make != $makes[$col])
                {
                    echo '<a href="priceresults.php?makeitem='.$makes[$col].'&typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice;
                    if(isset($sortby)) echo '&sortby='.$sortby;
                    echo '#results">';
                }
                echo $makes[$col].' ('.$amounts[$col].')';
                if($make != $makes[$col]) echo '</a>';
                echo '</td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            if($col <= $count)
            {
                echo '<td';
                if($row == 1) echo ' width="152"';
                echo '>';
                if($make != $makes[$col])
                {
                    echo '<a href="priceresults.php?makeitem='.$makes[$col].'&typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice;
                    if(isset($sortby)) echo '&sortby='.$sortby;
                    echo '#results">';
                }
                echo $makes[$col].' ('.$amounts[$col].')';
                if($make != $makes[$col]) echo '</a>';
                echo '</td>';
            }
            else if($row ==1) echo '<td width="152"></td>';
            else echo '<td></td>';
            $col++;
            echo '</tr>';
            $row++;
        }
    }
?>
        </table>
        </form>
        <br />
<p class="blackeleven" style="margin: 0;">
<?php
    if(isset($make))
    {
        echo '<a href="priceresults.php?typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice;
        if(isset($sortby)) echo '&sortby='.$sortby;
        echo '#results">Remove '.$make.' as criteria</a>';
    }
?>
</p>
<p class="blackeleven" style="color:#142c3c;">Prices continually fall, therefore an actual quote may be lower than the price ranges shown. Availability of the vehicles shown is determined after market research by your 1-800-vehicles.com representative. The results shown are accurate examples of each vehicle.
</p>
</div>

    <div class="grideight price-result-2ndtable">
<?php
    if($howmany > 0)
    {
        echo '<table class="table borderless" bgcolor="#ffffff"><tr><td class="searchheader">';
        echo 'Showing '.($beginat+1).'-';
        if(($beginat+$atatime) < $howmany) echo ($beginat+$atatime);
        else echo $howmany;
        echo ' of '.$howmany;
        echo "<br>";
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &lt; ';
        if($beginat > 0)
        {
            echo '<a href="priceresults.php?typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice.'&beginat='.($beginat-$atatime);
            if(isset($make)) echo '&makeitem='.$make;
            if(isset($sortby)) echo '&sortby='.$sortby;
            echo '#results">Previous</a>';
        }
        if(($beginat > 0) && ($beginat+$atatime < $howmany)) echo ' | ';
        if($beginat+$atatime < $howmany)
        {
            echo '<a href="priceresults.php?typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice.'&beginat='.($beginat+$atatime);
            if(isset($make)) echo '&makeitem='.$make;
            if(isset($sortby)) echo '&sortby='.$sortby;
            echo '#results">Next</a>';
        }
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &gt; ';
        echo '</td>';
        echo '<td class="searchheader">Sort by:</td><td colspan="2" class="searchheader"><form action="priceresults.php" method="get" name="sortform">';
        echo '<select name="sortbox" id="sortbox"  onchange="javascript:itemchanged()">';
        echo '<option value="pricefirstdown"';
        if(isset($sortby) && $sortby=='pricefirstdown') echo 'selected="selected"';
        echo '>Price (higher first)</option>';
        echo '<option value="pricefirstup"';
        if(isset($sortby) && $sortby=='pricefirstup') echo 'selected="selected"';
        echo '>Price (lower first)</option>';
        echo '<option value="yearfirstdown"';
        if((isset($sortby) && $sortby=='yearfirstdown') || !isset($sortby)) echo 'selected="selected"';
        echo '>Year (newer first)</option>';
        echo '<option value="yearfirstup"';
        if(isset($sortby) && $sortby=='yearfirstup') echo 'selected="selected"';
        echo '>Year (older first)</option>';
        echo '<option value="makefirst"';
        if(isset($sortby) && $sortby=='makefirst') echo 'selected="selected"';
        echo '>Make/Model/Year</option>';
        echo '</select></form></td></tr>';
        echo '<tr><td class="searchheaderb">';
        if(isset($sortby) && $sortby=='makefirst') echo 'Make Model Year Style';
        elseif(isset($sortby) && $sortby=='yearfirst') echo 'Year Make Model Style';
        elseif(isset($sortby) && $sortby=='pricefirst') echo 'Price Range';
        else echo 'Year Make Model Style';
        echo '</td><td class="searchheaderb">Price Range</td><td class="searchheaderb">Mileage</td>';
        echo '<td class="searchheaderb">&nbsp;</td></tr>';
        $numToShow = count($makedets);
        for($index = 1; $index <= $numToShow; $index++)
        {
            echo '<td class="searchitems"><a href="details.php?vehid='.$vehid[$index].'" target="_blank">';
            if(isset($sortby) && $sortby=='makefirst') echo $makedets[$index].' '.$modeldets[$index].' '.$yeardets[$index].' '.$styledets[$index].'</a> ';
            elseif(isset($sortby) && $sortby=='yearfirst') echo $yeardets[$index].' '.$makedets[$index].' '.$modeldets[$index].' '.$styledets[$index].'</a> ';
            else echo $yeardets[$index].' '.$makedets[$index].' '.$modeldets[$index].' '.$styledets[$index].'</a> ';
            if($bbdets[$index] == 1) echo '</td>';
            else echo '</td>';
            echo '<td class="searchitems" style="color:#142c3c;">';
            if($psdets[$index] > 0)
            {
                echo '$'.number_format($psdets[$index]/1000);
                echo 'K - $'.number_format($pedets[$index]/1000).'K';
            }
            else echo 'Below $5K';
            echo '</td>';
            echo '<td class="searchitems"  style="color:#142c3c;">';
            if($msdets[$index] > 0) echo number_format($msdets[$index]/1000);
            else echo '0';
            echo ' - ';
            if($medets[$index] > 0) echo number_format($medets[$index]/1000);
            else echo '0';
            echo 'K</td>';
            echo '<td class="searchitems search-items-button">';
            echo '<form action="favorites.php" method="post" name="saveform'.$vehid[$index].'">';
            echo '<input type="hidden" value="'.$vehid[$index].'" name="AddVehicleID" />';
            echo '<button type="submit" class="med">SAVE</button>';
            echo '</form>';
            echo '</td>';
            echo '</tr>';
        }
        echo '<tr><td class="searchheaderc">';
        echo 'Showing '.($beginat+1).'-';
        if($beginat+$atatime < $howmany) echo ($beginat+$atatime);
        else echo $howmany;
        echo ' of '.$howmany;
        echo "<br>";
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &lt; ';
        if($beginat > 0)
        {
            echo '<a href="priceresults.php?typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice.'&beginat='.($beginat-$atatime);
            if(isset($make)) echo '&makeitem='.$make;
            if(isset($sortby)) echo '&sortby='.$sortby;
            echo '#results">Previous</a>';
        }
        if(($beginat > 0) && ($beginat+$atatime < $howmany)) echo ' | ';
        if($beginat+$atatime < $howmany)
        {
            echo '<a href="priceresults.php?typelist='.$type.'&fromselect='.$from.'&stylelist='.$style.'&pricestart='.$startprice.'&priceend='.$maxprice.'&beginat='.($beginat+$atatime);
            if(isset($make)) echo '&makeitem='.$make;
            if(isset($sortby)) echo '&sortby='.$sortby;
            echo '#results">Next</a>';
        }
        if(($beginat > 0) || ($beginat+$atatime < $howmany)) echo ' &gt; ';
        echo '</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>';
    }
    else
    {
        echo '<p class="searchheaderc">Sorry! No results matched the criteria.</p>';
        echo '<a href="researchprice.php?typeitem='.$type.'&fromselect='.$from.'&styleitem='.$style.'&startprice='.$startprice.'&maxprice='.$maxprice;
        echo '">Start the search over with new criteria</a>';
    }
?>
        </div>
    </div><!-- grid eight container -->
<?php
    $_SESSION['hideteaser5'] = 'true';
    require("teaser.php");
?>
</div><!--end content-->
<form name="hiddenvalues">
<?php
echo '<input type="hidden" id="maxpricehidden" value="'.$maxprice.'"/>';
echo '<input type="hidden" id="startpricehidden" value="'.$startprice.'"/>';
echo '<input type="hidden" id="stylehidden" value="'.$style.'"/>';
echo '<input type="hidden" id="fromhidden" value="'.$from.'"/>';
echo '<input type="hidden" id="typehidden" value="'.$type.'"/>';
echo '<input type="hidden" id="makehidden" value="'.$make.'"/>';
?>
</form>
<?php require("footerstart.php"); ?>
<?php // require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
