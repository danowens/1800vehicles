<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 22;
    $_SESSION['titleadd'] = 'View Agreements';

    if(!isset($_POST['AgreementName']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x010122';
        header('Location viewagreements.php');
        exit();
    }

    $aname = $_POST['AgreementName'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select AgreementID from agreements where AgreementName='".$aname."'";
        $result = mysql_query($query, $con);
        if(!$result)
        {
            // Bad name passed in...
            mysql_close($con);
            $_SESSION['ShowError'] = 'Internal Error - 0x010122';
            header('Location viewagreements.php');
            exit();
        }
        if(!$row = mysql_fetch_array($result))
        {
            // Bad name passed in...
            mysql_close($con);
            $_SESSION['ShowError'] = 'Internal Error - 0x010122';
            header('Location viewagreements.php');
            exit();
        }

        $aid = $row[0];

        $query = "select SectionName, SectionText from agreementsections where AgreementID=".$aid;
        $result = mysql_query($query, $con);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $secname[$index] = $row[0];
            $sectext[$index] = $row[1];
            $index++;
        }

        mysql_close($con);

        if($index < 1)
        {
            // No sections for some reason...
            $_SESSION['ShowError'] = 'Internal Error - 0x010122';
            header('Location viewagreements.php');
            exit();
        }
    }

    $headingtext = $aname.' Agreement';
    switch($aname)
    {
        case 'Standard Order':
            $headersize = 300;
            break;
        case 'Special Order':
            $headersize = 300;
            break;
        case 'Group Watchlist Posting':
            $headersize = 400;
            break;
        case 'Specific Vehicle Purchase':
            $headersize = 420;
            break;
    }
?>

<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
       <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;"><?php echo $headingtext; ?></h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0; margin-left: 0px;"><a href="viewagreements.php">Go back to View Agreements</a></p>
            <div class="grideight" margin-left: 20px;">
<?php
    $count = count($secname);
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($sectext[$index]);
        //echo '<p class="blackfourteen"><strong>'.strtoupper($secname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen"><strong>'.strtoupper($secname[$index]).'</strong> - '.$sectext[$index].'</p>';
    }
?>
            </div><!-- end grideight-->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
    <br clear="all" />
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
