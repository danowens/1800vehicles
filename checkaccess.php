<?php
    require_once(WEB_ROOT_PATH.'common/functions/usernavfunctions.php');

    if(!isset($_SESSION['user'])) // Not Logged in? Can not get to certain pages...
    {
        if((($_SESSION['state'] == 1) && ($_SESSION['substate'] > 4)) || ($_SESSION['state'] > 1))
        {
            header('Location: '.WEB_SERVER_NAME.'loginrequired.php');
            exit();
        }
    }

    $state = $_SESSION['state'];
    $substate = $_SESSION['substate'];

	//$userid = null;
	if(isset($_SESSION['userid'])) {
		$userid = $_SESSION['userid'];
		$ucust = getuserprofile($userid, 'Customer');
	} else {
		$ucust = 'false';
		$userid = '';
	}

    if($ucust != 'true')
    {
        if((($state == 1) && ($substate > 4)) || (($state > 1) && ($state < 5)))
        {
            header('Location: '.WEB_SERVER_NAME.'notacustomer.php');
            exit();
            //echo $userid.' '.$ucust.' '.$state.' '.$substate;
        }
    }


    if(($state < 5) && ($substate != 99))
    {
        $current = currentstep();
        if($current < $state)
        {
//            if(($current <= 1) && ($state == 2))
//            {
//                header('Location: '.WEB_SERVER_NAME.'notready2.php');
//                exit();
//            }
            if(($current <= 1) && ($state == 3))
            {
                header('Location: '.WEB_SERVER_NAME.'notready3.php');
                exit();
            }
            elseif(($current <= 1) && ($state == 4))
            {
                header('Location: '.WEB_SERVER_NAME.'notready4.php');
                exit();
            }
            elseif(($current == 2) && ($state == 3))
            {
                header('Location: '.WEB_SERVER_NAME.'notready3.php');
                exit();
            }
            elseif(($current == 2) && ($state == 4))
            {
                header('Location: '.WEB_SERVER_NAME.'notready4.php');
                exit();
            }
            elseif(($current == 3) && ($state == 4))
            {
                header('Location: '.WEB_SERVER_NAME.'notready4.php');
                exit();
            }
        }
    }

    $ismore = ismorethancust($userid);
    if(($ismore != 'true') && ($state > 4))
    {
        $_SESSION['state'] = 0;
        header('Location: '.WEB_SERVER_NAME.'error404.php');
        exit();
    }

    if($state > 5)
    {
        $isadmin = getuserprofile($userid, 'Administrator');
        $isterr = getuserprofile($userid, 'Teritory Admin');
        if(!(($isadmin == 'true') || ($isterr == 'true')))
        {
            $_SESSION['state'] = 5;
            header('Location: '.WEB_SERVER_NAME.'error404.php');
            exit();
        }
    }
?>
