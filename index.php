<?php 
	session_start();
	unset($_SESSION['assessment_post']);
	unset($_SESSION['pos_post']);
	unset($_SESSION['searchplan_post']);
	unset($_SESSION['consult_post']);
	unset($_SESSION['request_trade_in_quote']);
?>
<!DOCTYPE html>
<html lang="en"> 

    <?php include("header.php"); ?>
    <script type="text/javascript">
    $(document).ready(function() {
        $('html, body').hide();

        if (window.location.hash) {
            setTimeout(function() {
                $('html, body').scrollTop(0).show();
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top
                    }, 800)
            }, 0);
        }
        else {
            $('html, body').show();
        }
    });
</script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#homepage_video").click(function() {
                $.colorbox({href: "homepage_video.php", scrolling: false, width: "90%"});
            });
            $("#watch_footer").click(function() {
                $.colorbox({href: "homepage_video.php", scrolling: false, width: "90%"});
            });


            $('.navbar123').hide();
            $(document).scroll(function() {
              var y = $(this).scrollTop();
              if (y > 100) {
                $('.navbar123').fadeIn();
              } else {
                $('.navbar123').fadeOut();
              }
            });

            $("#scroll-how-it-works").click(function() {
               $('html,body').animate({
                    scrollTop: $("#2nd-bg-img").offset().top},
                    'slow');
            });  


            /* fix vertical when not overflow
            call fullscreenFix() if .fullscreen content changes */
            function fullscreenFix(){
                var h = $('body').height();
                // set .fullscreen height
                $(".content-b").each(function(i){
                    if($(this).innerHeight() > h){ $(this).closest(".fullscreen").addClass("overflow");
                    }
                });
            }
            $(window).resize(fullscreenFix);
            fullscreenFix();

            /* resize background images */
            function backgroundResize(){
                var windowH = $(window).height();
                $(".background").each(function(i){
                    var path = $(this);
                    // variables
                    var contW = path.width();
                    var contH = path.height();
                    var imgW = path.attr("data-img-width");
                    var imgH = path.attr("data-img-height");
                    var ratio = imgW / imgH;
                    // overflowing difference
                    var diff = parseFloat(path.attr("data-diff"));
                    diff = diff ? diff : 0;
                    // remaining height to have fullscreen image only on parallax
                    var remainingH = 0;
                    if(path.hasClass("parallax")){
                        var maxH = contH > windowH ? contH : windowH;
                        remainingH = windowH - contH;
                    }
                    // set img values depending on cont
                    imgH = contH + remainingH + diff;
                    imgW = imgH * ratio;
                    // fix when too large
                    if(contW > imgW){
                        imgW = contW;
                        imgH = imgW / ratio;
                    }
                    //
                    path.data("resized-imgW", imgW);
                    path.data("resized-imgH", imgH);
                    path.css("background-size", imgW + "px " + imgH + "px");
                });
            }
            $(window).resize(backgroundResize);
            $(window).focus(backgroundResize);
            backgroundResize();


        });    

    </script> 

    <!--<div class="top-push"></div>-->


    <!--<div class="responsive-crop" style="background-image:url('assets/img/bg/img1.jpg');"></div>
    <div class="responsive-crop" style="background-image:url('assets/img/bg/img2.jpg');"></div>
    <div class="responsive-crop" style="background-image:url('assets/img/bg/img3.jpg');"></div>
    <div class="responsive-crop" style="background-image:url('assets/img/bg/img4.jpg');"></div>  
    <div class="responsive-crop" style="background-image:url('assets/img/bg/img5.jpg');"></div>
    <div class="responsive-crop" style="background-image:url('assets/img/bg/img6.jpg');"></div>
    <div class="responsive-crop" style="background-image:url('assets/img/bg/img7.jpg');"></div>-->


    <div class="fullscreen background tinted" data-img-width="1920" data-img-height="1280">
        <div class="img1-box1">
            <span>Welcome to the future of car buying!</span>
        </div>
        <div class="img1-box2">
            <span>Where we do the work... <br/><br/>but you have the control!</span>
        </div>        
        <h1 class="img1-inner-txt-1">Welcome to <br/>the future of <br/>car buying!</h1> 
        <h1 class="img1-inner-txt-2"><br/>Where we <br/>do the work...</h1>  
        <h1 class="img1-inner-txt-3">but you have<br/> the control!</h1>
        <div id="home_ytvideo_watch"><a href="#" id="homepage_video"><i class="fa fa-youtube-play"></i> <span class="video-text">Watch The Video</span></a></div>
        <!--<span class="arrow-scroll-down"><i class="fa fa-angle-down"></i></span>-->
    </div>

<!--<div class="watchthevideolink">
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center wow fadeInDown">
            <h3><a href="#"><i class="fa fa-youtube-play"></i> Watch The Video</a></h3>
        </div> 
    </div>
</div>
</div>-->
<!--<a id="how_work"></a>-->
    <div id="2nd-bg-img" class="fullscreen background bg-img2" style="background-image:url('assets/img/bg/img2a.jpg');" data-img-width="1920" data-img-height="1275" >
         <h1 class="img2-inner-txt">Simply post details of<br/> the vehicle you want<br/> at no cost or obligation</h1>  
    </div>
    <div class="fullscreen background" style="background-image:url('assets/img/bg/img3a.jpg');" data-img-width="1920" data-img-height="1247" >
         <h1 class="img3-inner-txt">Our wholesale buying team<br/> will show you amazing deals<br/> that only dealers can see</h1>  
    </div>
    <div class="fullscreen background" style="background-image:url('assets/img/bg/img4a.jpg');" data-img-width="1920" data-img-height="1278">
         <h1 class="img4-inner-txt">Approve the perfect vehicle for purchase</h1>  
    </div>
    <div class="fullscreen background" style="background-image:url('assets/img/bg/img5a.jpg');" data-img-width="1920" data-img-height="1280">
         <h1 class="img5-inner-txt">Then relax while we inspect,<br/> service and deliver it to you!</h1>
    </div>
    <div class="fullscreen background" style="background-image:url('assets/img/bg/img6a.jpg');" data-img-width="1920" data-img-height="1280">
         <h1 class="img6-inner-txt">Our vehicles are<br/> guaranteed to be in<br/> the condition promised</h1>  
    </div>
     <div class="fullscreen background" style="background-image:url('assets/img/bg/img7a.jpg');" data-img-width="1920" data-img-height="1280">
         <h1 class="img7-inner-txt">And must pass an inspection<br/> by you and a mechanic<br/> of your choosing!</h1>            
    </div>                

    <!-- Header -->
    <!--<div id="bgdiv" class="fullheight">
        <div class="container">
            <div class="intro-text">
                              <div class="intro-lead-in wow bounceInDown" data-wow-delay="0.25s">Shopping for a car</span></div>
                <div class="intro-heading wow bounceInUp" data-wow-delay="1s">will never get easier than this</div>

                <a href="#homepage_youtube_video" class="page-scroll btn btn-xl btn-negative  wow bounceInDown1" data-wow-delay="2s">Learn More <i class="fa fa-angle-down fa-2x"></i></a>
            </div>
        </div>
    </div>-->
    
    <!--<div id="topimage">
        <h1 class="img1-inner-txt-1">Welcome to <br/>the future of <br/>car buying!</h1> 
        <h1 class="img1-inner-txt-2"><br/>Where we <br/>do the work...<br/>but you have<br/> the control!</h1>  
        <div id="home_ytvideo_watch"><a href="#" id="homepage_video"><i class="fa fa-youtube-play"></i> Watch The Video</a></div>
     <section id="services">
        <div class="section-inner">     
            <div class="container">

                <div class="row">
                    <div class="col-lg-12 text-center wow fadeInDown">
                        <h2 class="section-heading"><strong>Video </strong></h2>

                    </div>
                </div>
                <div class="row wow fadeInUp">
                    <div class="col-md-4 service-item">
                        <div class="row" style="margin:0 auto; text-align: center">

                            <iframe width="100%" height="560" src="https://www.youtube.com/embed/i-SKfw7XnWY" frameborder="0" allowfullscreen></iframe>


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>       
    </div>   

    <div id="2nd-bg-img" class="CoverImage FlexEmbed FlexEmbed--16by9" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1),rgba(0, 0, 0, 0.1)),url(assets/img/bg/img2.jpg)">
         <h1 class="img2-inner-txt">Simply post details of<br/> the vehicle you want<br/> at no cost or obligation</h1>          
    </div> 
    <div class="CoverImage FlexEmbed FlexEmbed--16by9" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1),rgba(0, 0, 0, 0.1)),url(assets/img/bg/img3.jpg)">
         <h1 class="img3-inner-txt">Our wholesale buying team<br/> will show you amazing deals<br/> that only dealers can see</h1>           
    </div>     
    <div class="CoverImage FlexEmbed FlexEmbed--16by9" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1),rgba(0, 0, 0, 0.1)),url(assets/img/bg/img4.jpg)">
         <h1 class="img4-inner-txt">Approve the perfect vehicle for purchase</h1>          
    </div> 
    <div class="CoverImage FlexEmbed FlexEmbed--16by9" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1),rgba(0, 0, 0, 0.1)),url(assets/img/bg/img5.jpg)">
         <h1 class="img5-inner-txt">Then relax while we inspect,<br/> service and deliver it to you!</h1>         
    </div> 
    <div class="CoverImage FlexEmbed FlexEmbed--16by9" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1),rgba(0, 0, 0, 0.1)),url(assets/img/bg/img6.jpg)">
         <h1 class="img6-inner-txt">Our vehicles are<br/> guaranteed to be in<br/> the condition promised</h1>          
    </div> 
    <div class="CoverImage FlexEmbed FlexEmbed--16by9" style="background-image:url(assets/img/bg/img7.jpg)">
         <h1 class="img7-inner-txt">And must pass an inspection<br/> by you and a mechanic<br/> of your choosing!</h1>        
    </div> -->
       
    <!--<div id="homepage_youtube_video"><img src="/images/play_button.png"/><a href="#" id="homepage_video">Watch How It Works</a></div>-->

    <!-- Services Section -->
    <!--<section id="services">
        <div class="section-inner">     
            <div class="container">

                <div class="row">
                    <div class="col-lg-12 text-center wow fadeInDown">
                        <h2 class="section-heading"><strong>Video </strong></h2>

                    </div>
                </div>
                <div class="row wow fadeInUp">
                    <div class="col-md-4 service-item">
                        <div class="row" style="margin:0 auto; text-align: center">

                            <iframe width="100%" height="560" src="https://www.youtube.com/embed/i-SKfw7XnWY" frameborder="0" allowfullscreen></iframe>


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>-->



    <!-- About Section 
    <section id="about-us">
        <div class="section-inner">
            <div class="container">

                <div class="homecontent">
                    
               
                <div class="posts" id="post1">
                    <div class="post_image"> <img class="img-responsive" src="/images/1.jpg"/> </div>
                    <div class="post_desc"><span>Simply post details of the vehicle you want at no cost or obligation</span></div>
                </div>
                <div class="posts" id="post2">
                    <div class="post_image"> <img class="img-responsive" src="/images/2.jpg"/> </div>
                    <div class="post_desc"><span>Our wholesale buying team will show you amazing deals that only dealers can see</span></div>
                </div>

                <div class="posts" id="post3">
                    <div class="post_image"> <img class="img-responsive" src="/images/3.jpg"/> </div>
                    <div class="post_desc"><span>Approve the perfect vehicle for purchase</span></div>
                </div>

                <div class="posts" id="post4">
                    <div class="post_image"> <img class="img-responsive" src="/images/4.jpg"/> </div>
                    <div class="post_desc"><span>Then relax while we inspect, service and deliver it to you!</span></div>
                </div>

                <div class="posts" id="post5">
                    <div class="post_image"> <img class="img-responsive" src="/images/5.jpg"/> </div>
                    <div class="post_desc"><span>Our vehicles are guaranteed to be in the condition promised</span></div>
                </div>

                <div class="posts" id="post6">
                    <div class="post_image"> <img class="img-responsive" src="/images/6.jpg"/> </div>
                    <div class="post_desc"><span>And must pass an inspection by you and a mechanic of your choosing!</span></div>
                </div>

                 <div style="clear:both"></div>
              





                </div>
            </div>
        </div>
    </section>-->


<div id="abouthowitworks">
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center wow fadeInDown">
            <h2 class="section-heading"><a href="#"><strong>MORE ABOUT HOW IT WORKS</strong> </a></h2>
        </div> 
    </div>
</div>
</div>

    <div id= "watch_footer" class="watchthevideolink" style="display: block;">
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center wow fadeInDown">
            <h3><a href="#"><i class="fa fa-youtube-play"></i> Watch The Video</a></h3>
        </div> 
    </div>
</div>
</div>



    
<div id="about-us">
<div class="container">
<div class="navbar123">
                <!--<div class="row" id="how_it_works">
                    <div class="col-lg-12 text-center wow fadeInDown">
                        <h2 class="section-heading"><a href="#"><strong>MORE ABOUT HOW IT WORKS</strong> </a></h2>
                    </div>
                </div>-->
                <div class="row" id="get_search_started">
                    <div class="col-lg-12 text-center wow fadeInDown">
                        <h2 class="section-heading"><a href="add_vehicle_spec.php"><strong>START THE SEARCH PROCESS</strong> </a></h2>
                    </div>
                </div>
                <div class="row" id="what_to_buy">
                    <div class="col-lg-12 text-center wow fadeInDown">
                        <h2 class="section-heading"><a href="get_price_and_expert_advice.php"><strong>GET HELP WITH WHAT TO BUY</strong></a> </h2>
                    </div>
                </div>
</div>
</div>
</div>


<section id="footer-widgets" class="divider-wrapper"></section>

<?php include("footer.php") ?>
<?php include("footerend.php") ?>
<!-- livezilla.net code (PLEASE PLACE IN BODY TAG) -->
<div id="livezilla_tracking" style="display:none"></div>
<script type="text/javascript">
var script = document.createElement("script");
script.async=true;
script.type="text/javascript";
var src = "http://234temp-name.s207745.gridserver.com/chat-support/server.php?a=b0d4f&rqst=track&output=jcrpt&ovlc=IzczYmUyOA__&ovlt=V2UgYXJlIG9ubGluZSAtIENoYXQgTm93IQ__&ovlto=U2VuZCBhIG1lc3NhZ2UgdG8gb3VyIHN1cHBvcnQgdGVhbQ__&ovlw=MzIw&ovlh=NTAw&eca=MQ__&ecsp=MQ__&nse="+Math.random();
setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)",1);
</script>
<noscript><img src="http://234temp-name.s207745.gridserver.com/chat-support/server.php?a=b0d4f&amp;rqst=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript>
<!-- http://www.livezilla.net -->
<style>
	#lz_chat_apa, #lz_group_selection_box, #lz_ec_sub_header_text { 
		display: none; 
	}
	#lz_ec_header_text {
		right: 29px !important;
		text-align: center;
		top: 15px;
	}
	#lz_overlay_chat, #lz_eye_catcher {
		right: 0 !important;
		left: auto !important;
	}    

	@media (max-width: 1024px) {
		#lz_overlay_chat {
			bottom: 82px !important;
		}
		#lz_eye_catcher {
			bottom: 106px !important;
		}
	}
</style>
<script src="/assets/js/SmoothScroll.js"></script>
<script>
	jQuery(document).ready( function($) {
		
	});
</script>
</html>
