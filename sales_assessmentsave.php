<?php require("globals.php"); ?>
<?php
//error_reporting(E_ALL); 
//ini_set("display_errors", "on");
$_SESSION['state'] = 5;
$_SESSION['substate'] = 9;
$_SESSION['titleadd'] = 'Save Assessment by Sales Rec';

// See what was passed in...
$vehicletype = $_POST['vehicletype'];
$bodytype = $_POST['bodytype'];
$vehiclesize = $_POST['vehiclesize'];
$drivetrain = $_POST['drivetrain'];
$notes = $_POST['notes'];
$origin = $_POST['origin'];
$transmission = $_POST['transmission'];
$mileagefrom = $_POST['mileagefrom'];
$mileageto = $_POST['mileageto'];
$mileageceiling = $_POST['mileageceiling'];
$extlike = $_POST['extlike'];
$extdislike = $_POST['extdislike'];
$intlike = $_POST['intlike'];
$intdislike = $_POST['intdislike'];
$budgetfrom = $_POST['budgetfrom'];
$budgetto = $_POST['budgetto'];
$borrowmaxpayment = $_POST['borrowmaxpayment'];
$borrowdownpayment = $_POST['borrowdownpayment'];
$vehicleneed = $_POST['vehicleneed'];
$brandlike = $_POST['brandlike'];
$branddislike = $_POST['branddislike'];
$model = $_POST['model'];
$vehicleage = $_POST['vehicleage'];
$ratefuelefficiency = $_POST['ratefuelefficiency'];
$ratemaintenancecost = $_POST['ratemaintenancecost'];
$ratereliability = $_POST['ratereliability'];
$rateluxury = $_POST['rateluxury'];
$ratesporty = $_POST['ratesporty'];
$ratesafety = $_POST['ratesafety'];

//What do you prefer start
$frontstype = $_POST['frontstype'];
$bedtype = $_POST['bedtype'];
$leather = $_POST['leather'];
$heatedseat = $_POST['heatedseat'];
$navigation = $_POST['navigation'];
$sunroof = $_POST['sunroof'];
$alloywheels = $_POST['alloywheels'];
$rearwindow = $_POST['rearwindow'];
$bedliner = $_POST['bedliner'];
$entertainmentsystem = $_POST['entertainmentsystem'];
$thirdrs = $_POST['thirdrs'];
$crow = $_POST['crow'];
$prhatch = $_POST['prhatch'];
$backupcamera = $_POST['backupcamera'];
$tpackage = $_POST['tpackage'];
//What do you prefer end

$otherprefereces = $_POST['otherprefereces'];
$otherreallyhave = $_POST['otherreallyhave'];
$additionalinfo = $_POST['additionalinfo'];

$marketneedid = $_POST['MarketNeedID'];
$useridpost = $_POST['ForUserID'];

$errorinsave = 'false';
//@TODO: check for validation of the form data

if ($vehicletype == "Auto") {
//update query data
    $updatedoyouprefer = ", Leather = '" . $leather . "'";
    $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
    $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
    $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
    $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
} elseif ($vehicletype == "Minivan") {

//update query data
    $updatedoyouprefer = ", Leather = '" . $leather . "'";
    $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
    $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
    $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
    $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
    $updatedoyouprefer .= ", EntertainmentSystem = '" . $entertainmentsystem . "'";
    $updatedoyouprefer .= ", PRHatch = '" . $prhatch . "'";
    $updatedoyouprefer .= ", BackupCamera = '" . $backupcamera . "'";
} elseif ($vehicletype == "SUV") {
 
//update query data
    $updatedoyouprefer = ", Leather = '" . $leather . "'";
    $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
    $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
    $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
    $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
    $updatedoyouprefer .= ", EntertainmentSystem = '" . $entertainmentsystem . "'";
    $updatedoyouprefer .= ", ThirdRD = '" . $thirdrs . "'";
    $updatedoyouprefer .= ", CRow = '" . $crow . "'";
    $updatedoyouprefer .= ", PRHatch = '" . $prhatch . "'";
    $updatedoyouprefer .= ", BackupCamera = '" . $backupcamera . "'";
    $updatedoyouprefer .= ", TPackage = '" . $tpackage . "'";
} elseif ($vehicletype == "Pickup") {
  
//update query data
    $updatedoyouprefer = ", FrontSType = '" . $frontstype . "'";
    $updatedoyouprefer .= ", BedType = '" . $bedtype . "'";
    $updatedoyouprefer .= ", Leather = '" . $leather . "'";
    $updatedoyouprefer .= ", HeatedSeat = '" . $heatedseat . "'";
    $updatedoyouprefer .= ", Navigation = '" . $navigation . "'";
    $updatedoyouprefer .= ", SunRoof = '" . $sunroof . "'";
    $updatedoyouprefer .= ", AlloyWheels = '" . $alloywheels . "'";
    $updatedoyouprefer .= ", RearWindow = '" . $rearwindow . "'";
    $updatedoyouprefer .= ", BedLiner = '" . $bedliner . "'";
    $updatedoyouprefer .= ", BackupCamera = '" . $backupcamera . "'";
    $updatedoyouprefer .= ", TPackage = '" . $tpackage . "'";
}


if ($errorinsave == 'false') {
    $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    if ($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

// See if they already have one...
        $query = "select * from assessments where MarketNeedID = " . $marketneedid;
        $result = mysql_query($query, $con);
        if ($result) {

// There is already one saved, so update it...
            $query = "update assessments set LastUpdated = '" . date_at_timezone('Y-m-d H:i:s', 'EST') . "'";
            $query .= ", SeenBySales = 1";
            $query .= ", VehicleType = '" . $vehicletype . "'";
            $query .= ", BodyType = '" . $bodytype . "'";
            $query .= ", VehicleSize = '" . $vehiclesize . "'";
            $query .= ", DriveTrain = '" . $drivetrain . "'";
            $query .= ", Notes = '" . $notes . "'";
            $query .= ", Origin = '" . $origin . "'";
            $query .= ", Transmission = '" . $transmission . "'";
            $query .= ", MileageFrom = '" . $mileagefrom . "'";
            $query .= ", MileageTo = '" . $mileageto . "'";
            $query .= ", MileageCeiling = '" . $mileageceiling . "'";
            $query .= ", ExtLike = '" . $extlike . "'";
            $query .= ", ExtDislike = '" . $extdislike . "'";
            $query .= ", IntLike = '" . $intlike . "'";
            $query .= ", IntDisike = '" . $intdislike . "'";
            $query .= ", BudgetFrom = '" . $budgetfrom . "'";
            $query .= ", BudgetTo = '" . $budgetto . "'";
            $query .= ", BorrowMaxPayment = '" . $borrowmaxpayment . "'";
            $query .= ", BorrowDownPayment = '" . $borrowdownpayment . "'";
            $query .= ", VehicleNeed = '" . $vehicleneed . "'";
            $query .= ", BrandLike = '" . $brandlike . "'";
            $query .= ", BrandDislike = '" . $branddislike . "'";
            $query .= ", Model = '" . $model . "'";
            $query .= ", VehicleAge = '" . $vehicleage . "'";
            $query .= ", RateFuelEfficiency = '" . $ratefuelefficiency . "'";
            $query .= ", RateMintenanceCost = '" . $ratemaintenancecost . "'";
            $query .= ", RateReliability = '" . $ratereliability . "'";
            $query .= ", RateLuxury = '" . $rateluxury . "'";
            $query .= ", RateSporty = '" . $ratesporty . "'";
            $query .= ", RateSafety = '" . $ratesafety . "'";
            $query .= $updatedoyouprefer;
            $query .= ", OtherPrefereces = '" . $otherprefereces . "'";
            $query .= ", OtherReallyHave = '" . $otherreallyhave . "'";
            $query .= ", AdditionalInfo = '" . $additionalinfo . "'";
            $query .= " where MarketNeedID = " . $marketneedid;
            //echo $query; exit;
            if (!mysql_query($query, $con))
                $errorinsave = 'Could not update Assessment';

        }

        mysql_close($con);
    } else
        $errorinsave = 'Could not connect to the database';
}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
<?php
if ($errorinsave != 'false') {
    echo '<h1 class="subhead" style="width:300px;">Assessment Issue!</h1>';
} else {
    echo '<h1 class="subhead" style="width:300px;">Assessment Update Complete by Sales Rec!</h1>';
}
?>
        <div class="grideightgrey">
            <div class="grideight" style="width:580px;">
<?php
if ($errorinsave != 'false') {
    echo '<p class="blacktwelve">Sorry!  There was an error processing your assessment.</p>';
    echo '<p class="blacktwelve">Please contact a representative at 1-800-vehicles (834-4253) for help with this issue.</p>';
    if ($errorinsave != 'true')
        echo $errorinsave . '<br/>';
    echo '<p class="blacktwelve">Use your browser <a href="javascript:history.back()">back</a> button to add missing fields or to start over click <a href="' . WEB_SERVER_NAME . 'assessment.php">here</a>.</p>';
//echo $query.'<br/>';
}
else { ?>
    <p class="blacktwelve">Thank you for taking the assessment.</p>
    <form method="post" action="/salesrepactions.php">
        <input type="hidden" name="ForUserID" value="<?=$useridpost?>">
        <input type="hidden" name="MarketNeedID" value="<?=$marketneedid?>">
        <button class="blueongrey_dash" value="" style="font-size: 11px;" type="submit">
            Go back to Customer Review
        </button>
    </form>

<?php }
?>
            </div><!-- end greyeight-->
        </div><!-- grid eight container -->
    </div><!-- end grideightgrey-->
                <?php require("teaser.php"); ?>
</div><!--end content-->

                <?php require("footerstart.php"); ?>              
                <?php require("footer.php"); ?>
                <?php require("footerend.php"); ?>
