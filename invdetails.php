<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 12;
    $_SESSION['titleadd'] = 'Specific Vehicle Details';

    if(!isset($_REQUEST['SVQID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x010512';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    $svqid = $_REQUEST['SVQID'];

    $errormessage = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select v.Year, m.Name, v.Model, v.Style, v.VIN, v.Mileage, v.Doors, v.ExteriorColor, v.InteriorColor, v.Transmission, v.WheelDriveType, v.EngineCylinders";
        $query .= ",v.SeatMaterial, v.WheelCovers, v.Sunroof, v.RadioFM, v.RadioCassette, v.RadioCD, v.PowerDoors, v.PowerWindows, v.PowerSeats, v.FrontHeatedSeats";
        $query .= ",v.AirConditioning, v.RemoteEntry, v.TractionControl, v.SecuritySystem, v.CruiseControl, v.Navigation, s.PriceQuoted, v.SpecialNotes, v.Information";
        $query .= ", v.ExtraFeatures, v.AudioFile, v.OnlineLink, v.VideoFile, v.VehicleDetailID, v.LuggageRack, v.WoodGrain, v.TowPackage, v.RunningBoards, s.AddDepositAmount";
        $query .= ", s.AddSecondKeyLimit, s.AddNavDiscLimit, s.AddSellerRepairLimit, s.AddPurchaserRepairLimit, s.AddTireCopay, s.AddDelivery,v.VehicleDetailID";
        $query .= " from specificvehicles s, vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=s.VehicleDetailID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        if(!$result || !$row = mysql_fetch_array($result))
        {
            mysql_close($con);
            $_SESSION['ShowError'] = 'Internal Error - 0x010512';
            header('Location: mydashboard.php#admintab');
            exit();
        }

        $svqyear = $row[0];
        $svqmake = $row[1];
        $svqmodel = $row[2];
        $svqstyle = $row[3];
        $svqvin = $row[4];
        $svqmiles = $row[5];
        $svqdoors = $row[6];
        $svqextcolor = $row[7];
        $svqintcolor = $row[8];
        $svqtrans = $row[9];
        $svqwdt = $row[10];
        $svqcyl = $row[11];
        $svqseat = $row[12];
        $svqwtype = $row[13];
        $svqroof = $row[14];
        $svqradfm = $row[15];
        $svqradcas = $row[16];
        $svqradcd = $row[17];
        $svqpdoor = $row[18];
        $svqpwin = $row[19];
        $svqpseat = $row[20];
        $svqhseat = $row[21];
        $svqair = $row[22];
        $svqremote = $row[23];
        $svqtraction = $row[24];
        $svqsecure = $row[25];
        $svqcruise = $row[26];
        $svqnav = $row[27];
        $svqprice = $row[28];
        $svqspecial = $row[29];
        $svqdesc = $row[30];
        $svqextra = $row[31];
        $svqaudio = $row[32];
        $svqonline = $row[33];
        $svqvideo = $row[34];
        $svqvehdetid = $row[35];
        $svqluggage = $row[36];
        $svqwood = $row[37];
        $svqtowing = $row[38];
        $svqrunning = $row[39];
        $svqadddep = $row[40];
        $svqaddsec = $row[41];
        $svqaddnav = $row[42];
        $svqaddsel = $row[43];
        $svqaddpur = $row[44];
        $svqaddtir = $row[45];
        $svqadddel = $row[46];
        $svqvehdetid = $row[47];

        // Determine whether pickup or delivery...
        $svqpickup = 1;
        $svqcity = '';
        $svqstate = '';

        // Check the orders first...
        $query = "select q.Pickup, q.DeliverToCity, q.DeliverToState from specificvehicles s, orderfor o, firmquotes f, quoterequests q where q.QuoteRequestID=f.QuoteRequestID and f.FirmQuoteID=o.FirmQuoteID and o.OrderID=s.OrderID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        while($result && $row = mysql_fetch_array($result))
        {
            if($row[0] == 0)
            {
                $svqpickup = 0;
                $svqcity = $row[1];
                $svqstate = $row[2];
                break;
            }
        }

        // Check the watches next...
        if($svqpickup == 1)
        {
            $query = "select q.Pickup, q.DeliverToCity, q.DeliverToState from specificvehicles s, watchfor w, firmquotes f, quoterequests q where q.QuoteRequestID=f.QuoteRequestID and f.FirmQuoteID=w.FirmQuoteID and w.WatchID=s.WatchID and s.SpecificVehicleID=".$svqid;
            $result = mysql_query($query);
            while($result && $row = mysql_fetch_array($result))
            {
                if($row[0] == 0)
                {
                    $svqpickup = 0;
                    $svqcity = $row[1];
                    $svqstate = $row[2];
                    break;
                }
            }
        }

        $svqwholesale = 0;
        $svqtradein = 0;
        $svqonly = 0;

        $query = "select StockNum, FranchiseeID from instock where VehicleDetailID=".$svqvehdetid;
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $svqstock = $row[0];
            $svqfranchise = $row[1];
            $svqwholesale = 1;
        }
        else
        {
            $query = "select TradeInID from tradeins where VehicleDetailID=".$svqvehdetid;
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                $svqtradeinid = $row[0];
                $svqtradein = 1;
            }
            else
            {
                $query = "select * from svqonlys where VehicleDetailID=".$svqvehdetid;
                $result = mysql_query($query);
                if($result && $row = mysql_fetch_array($result))
                {
                    $svqonly = 1;
                }
            }
        }

        // Look for images of the vehicle in question...
        $query = "select count(*) from specificvehicles s, vehicleimages i where s.VehicleDetailID=i.VehicleDetailID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $svqimages = $row[0];
        }
        else $svqimages = 0;

        if($svqimages > 0)
        {
            $query = "select i.ImageFile from specificvehicles s, vehicleimages i where s.VehicleDetailID=i.VehicleDetailID and i.DisplayOrder=1 and s.SpecificVehicleID=".$svqid;
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                $svqimage = $row[0];
            }
        }

        $query = "select count(*) from purchases p, specificvehicles s where s.SpecificVehicleID=p.SpecificVehicleID and p.BoughtOn is not null and MarketNeedID=".$_REQUEST['MarketNeedID'];
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $bought = $row[0];
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead">Inventory Detail</h1>
        <div class="grideightgrey" style="color:#142c3c;">
            <p style="font-size:18px; font-weight:bold;">Vehicle Specifications</p>
            <table border="0" width="350" cellpadding="5" align="left" style="margin-left:5px;">
                <tr>
                    <td><strong>Year</strong></td>
                    <td><p class="greytwelve"><?php echo $svqyear; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Make</strong></td>
                    <td><p class="greytwelve"><?php echo $svqmake; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Model</strong></td>
                    <td><p class="greytwelve"><?php echo $svqmodel; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Style</strong></td>
                    <td><p class="greytwelve"><?php echo $svqstyle; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Mileage</strong></td>
                    <td><p class="greytwelve"><?php echo number_format($svqmiles); ?></p></td>
                </tr>
                <tr>
                    <td><strong>VIN</strong></td>
                    <td><p class="greytwelve"><?php echo $svqvin; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Exterior Color</strong></td>
                    <td><p class="greytwelve"><?php echo $svqextcolor; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Interior Color</strong></td>
                    <td><p class="greytwelve"><?php echo $svqintcolor; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Transmission</strong></td>
                    <td><p class="greytwelve"><?php echo $svqtrans; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Wheel Drive</strong></td>
                    <td><p class="greytwelve"><?php if($svqwdt == 'All') echo 'AWD'; elseif($svqwdt == 'Four') echo 'FWD'; else echo '2WD'; ?></p></td>
                </tr>
                <tr>
                    <td><strong>Asking Price</strong></td>
                    <td style="font-weight:bold; color:#85c11b;">$<?php echo number_format($svqprice); ?></td>
                </tr>
                <tr>
                    <td><strong>Additional Deposit</strong></td>
                    <td><p class="greytwelve">$<?php echo number_format($svqadddep); ?></p></td>
                </tr>
                <tr>
                    <td><strong>Additional Second Key Limit</strong></td>
                    <td><p class="greytwelve">$<?php echo number_format($svqaddsec); ?></p></td>
                </tr>
                <tr>
                    <td><strong>Additional Nav Disc Limit</strong></td>
                    <td><p class="greytwelve">$<?php echo number_format($svqaddnav); ?></p></td>
                </tr>
                <tr>
                    <td><strong>Additional Seller Repair Limit</strong></td>
                    <td><p class="greytwelve">$<?php echo number_format($svqaddsel); ?></p></td>
                </tr>
                <tr>
                    <td><strong>Additional Purchaser Repair Limit</strong></td>
                    <td><p class="greytwelve">$<?php echo number_format($svqaddpur); ?></p></td>
                </tr>
                <tr>
                    <td><strong>Additional Tire Copay</strong></td>
                    <td><p class="greytwelve">$<?php echo number_format($svqaddtir); ?></p></td>
                </tr>
                <tr>
                    <td><strong>Additional Delivery</strong></td>
                    <td><p class="greytwelve">$<?php echo number_format($svqadddel); ?></p></td>
                </tr>
            </table>
<?php
    if($svqimages > 0)
    {
?>
            <table width="280" align="right" border="0" style="font-weight:bold; font-size: 17px; line-height:24px;">
                <tr>
                    <td><a href="invimages.php?SVQID=<?php echo $svqid; ?>&ForUserID=<?php echo $_REQUEST['ForUserID']; ?>&MarketNeedID=<?php echo $_REQUEST['MarketNeedID']; ?>">Click here for this vehicle's pictures</a></td>
                </tr>
            </table>
<?php
    }
?>
            <br clear="all" />
            <br />
            <br />
            <p style="font-size:18px; font-weight:bold;">Vehicle Details</p>
            <table border="0" width="280" align="left" style="margin-left:10px;">
                <tr>
                    <td><strong>Stock No.</strong></td>
                    <td><p class="greytwelve"><?php if(isset($svqstock)) echo $svqstock; else echo 'Not Set'; ?></p></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>Engine</strong></td>
                    <td><strong>Seat Material</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="greytwelve"><?php echo $svqcyl; ?> cylinders</p></td>
                    <td><p class="greytwelve"><?php echo $svqseat; ?></p></td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr>
                    <td><strong>Sunroof</strong></td>
                    <td><strong>Wheel Type</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="greytwelve"><?php echo $svqroof; ?></p></td>
                    <td><p class="greytwelve"><?php echo $svqwtype; ?></p></td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><strong>Stereo</strong></td>
                </tr>
                <tr valign="top">
                    <td><p class="greytwelve">&nbsp;</p></td>
                    <td><p class="greytwelve">
<?php
    if($svqradfm)
    {
        echo 'AM/FM';
        if($svqradcas) echo ', CAS';
        if($svqradcd) echo ', CD';
    }
    elseif($svqradcas)
    {
        echo 'CAS';
        if($svqradcd) echo ', CD';
    }
    elseif($svqradcd) echo 'CD';
    else echo 'None';
?>
                    </p></td>
                </tr>
            </table>
            <table border="0" width="280" align="right" valign="top" cellspacing="5">
                <tr>
                    <td width="125">Power Door Locks</td>
                    <td width="136"><span style="color: #06C;"><?php if($svqpdoor == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Power Windows</td>
                    <td><span style="color: #06C;"><?php if($svqpwin == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Power Seats</td>
                    <td><span style="color: #06C;"><?php if($svqpseat == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Heated Seats</td>
                    <td><span style="color: #06C;"><?php if($svqhseat == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td width="125">Air Conditioning</td>
                    <td width="136"><span style="color: #06C;"><?php if($svqair == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Remote Entry</td>
                    <td><span style="color: #06C;"><?php if($svqremote == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Traction Conrol</td>
                    <td><span style="color: #06C;"><?php if($svqtraction == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Security System</td>
                    <td><span style="color: #06C;"><?php if($svqsecure == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td width="125">Cruise Control</td>
                    <td width="136"><span style="color: #06C;"><?php if($svqcruise == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Navigation</td>
                    <td><span style="color: #06C;"><?php if($svqnav == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Luggage Rack</td>
                    <td><span style="color: #06C;"><?php if($svqluggage == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Wood Grain</td>
                    <td><span style="color: #06C;"><?php if($svqwood == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Tow Package</td>
                    <td><span style="color: #06C;"><?php if($svqtowing == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
                <tr>
                    <td>Running Boards</td>
                    <td><span style="color: #06C;"><?php if($svqrunning == 1) echo 'Yes'; else echo 'No'; ?></span></td>
                </tr>
            </table>
            <br clear="all" />
            <br />
            <br />
            <table border="0" width="280" align="left" style="margin-left:10px;">
                <tr>
                    <td><strong>Description of the Vehicle</strong></td>
                </tr>
                <tr>
                    <td><?php if(isset($svqdesc)) echo $svqdesc; ?></td>
                </tr>
            </table>
            <table border="0" width="280" align="right">
                <tr>
                    <td>
                        <strong>Special notes</strong>
                    </td>
                </tr>
                <tr>
                    <td><?php if(isset($svqspecial)) echo $svqspecial; ?></td>
                </tr>
            </table>
            <table border="0" width="280" align="right">
                <tr>
                    <td>
                        <strong>Extra Features</strong>
                    </td>
                </tr>
                <tr>
                    <td><?php if(isset($svqextra)) echo $svqextra; ?></td>
                </tr>
            </table>
            <br clear="all" /><br />
<?php
    if($bought <= 0)
    {
?>
            <form action="invbought.php" method="post">
                <input type="hidden" value="<?php echo $svqid; ?>" name="SVQID" />
                <input type="hidden" value="<?php echo $_REQUEST['ForUserID']; ?>" name="ForUserID" />
                <input type="hidden" value="<?php echo $_REQUEST['MarketNeedID']; ?>" name="MarketNeedID" />
                <button value="" class="med" style="float: right;"><nobr>FLAG AS BOUGHT</nobr></button>
            </form>
<?php
        if($svqwholesale == 1)
        {
            echo '<form action="addinventory.php" method="post">';
            echo '<input type="hidden" value="'.$svqid.'" name="SVQID" />';
            echo '<input type="hidden" value="'.$svqvehdetid.'" name="EditVehID" />';
            echo '<input type="hidden" value="'.$_REQUEST['ForUserID'].'" name="ForUserID" />';
            echo '<input type="hidden" value="'.$_REQUEST['MarketNeedID'].'" name="MarketNeedID" />';
            echo '<input type="hidden" value="'.$svqfranchise.'" name="FranchiseeID" />';
            echo '<input type="hidden" value="true" name="RetToInv" />';
            echo '<input type="hidden" value="true" name="WholeSale" />';
            echo '<button value="" class="med" style="float: right;"><nobr>EDIT VEHICLE DETAILS</nobr></button>';
            echo '</form>';
        }
        elseif($svqtradein == 1)
        {
            echo '<form action="addinventory.php" method="post">';
            echo '<input type="hidden" value="'.$svqid.'" name="SVQID" />';
            echo '<input type="hidden" value="'.$svqvehdetid.'" name="EditVehID" />';
            echo '<input type="hidden" value="'.$_REQUEST['ForUserID'].'" name="ForUserID" />';
            echo '<input type="hidden" value="'.$_REQUEST['MarketNeedID'].'" name="MarketNeedID" />';
            echo '<input type="hidden" value="'.$svqtradeinid.'" name="TradeInID" />';
            echo '<input type="hidden" value="true" name="RetToInv" />';
            echo '<input type="hidden" value="true" name="TradeIn" />';
            echo '<button value="" class="med" style="float: right;"><nobr>EDIT VEHICLE DETAILS</nobr></button>';
            echo '</form>';
        }
        elseif($svqonly == 1)
        {
            echo '<form action="addinventory.php" method="post">';
            echo '<input type="hidden" value="'.$svqid.'" name="SVQID" />';
            echo '<input type="hidden" value="'.$svqvehdetid.'" name="EditVehID" />';
            echo '<input type="hidden" value="'.$_REQUEST['ForUserID'].'" name="ForUserID" />';
            echo '<input type="hidden" value="'.$_REQUEST['MarketNeedID'].'" name="MarketNeedID" />';
            echo '<input type="hidden" value="true" name="RetToInv" />';
            echo '<input type="hidden" value="true" name="SVQONLY" />';
            echo '<button value="" class="med" style="float: right;"><nobr>EDIT VEHICLE DETAILS</nobr></button>';
            echo '</form>';
        }
    }
?>
            <form action="salesrepactions.php" method="post">
                <input type="hidden" value="<?php echo $_REQUEST['ForUserID']; ?>" name="ForUserID" />
                <input type="hidden" value="<?php echo $_REQUEST['MarketNeedID']; ?>" name="MarketNeedID" />
                <button value="" class="med" style="float: right;"><nobr>GO BACK</nobr></button>
            </form>
            <br clear="all" /><br />
        </div><!--grideightgrey-->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
