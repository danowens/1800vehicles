<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 1;
    $_SESSION['titleadd'] = 'Frequently Asked Questions';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php // require("headerend.php"); ?>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
       <h1 id="h1" class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Frequently Asked Questions</h1>
        <div class="grideightgrey">
            <p class="blacktwelve">If you have a question that is not answered here, please contact us at <a href="support.php">24/7 Customer  Support</a>. 
           </p>
            <ul class="blackfourteen">
                <li><a href="#1">What is 1-800-vehicles.com?</a></li>
                <li><a href="#2">Why should I use this service to buy my next car?</a></li>
                <li><a href="#3">Who is a typical 1-800-vehicles.com customer?</a></li>
                <li><a href="#4">How do I know that the car you deliver will be a good one?</a></li>
                <li><a href="#5">How long will it take you to locate my car?</a></li>
                <li><a href="#6">I know what I want. How should I use your website?</a></li>
                <li><a href="#7">I am not sure what I want. How should I use your website?</a></li>
                <li><a href="#8">Where are the price ranges for pick-up trucks?</a></li>
                <li><a href="#9">Do you take trade-ins?</a></li>
                <li><a href="#10">What does "Excellent Availability!" mean?</a></li>
            </ul>
        </div><!-- end greyeightgrey-->
   </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
    <div class="grideightcontainer"> 
       <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Answers</h1>
        <div class="grideightgrey">

           
            <p id="1" class="blacktwentytwo" style="margin-top: 15px;">What is 1-800-vehicles.com?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">1-800-vehicles.com is an online ordering system and dealer franchise group offering
                consumers legitimately nice vehicles from a massive "virtual inventory" throughout the
                United States. 1-800-vehicles.com dealers sell 1 to 6 year old vehicles through this
                unique and proven "one stop shopping" process, by order only. By avoiding the high cost
                of inventory and advertising, the system allows retail customers to choose from the highest
                rated vehicles in wholesale inventory, and still save time, money, and hassles.
                1-800-vehicles.com dealers handle both import and domestic cars, SUVs, minivans and 
                pick-up trucks ranging from the small economy units to the most prestigious luxury models.
            </p>
            <p class="med"><a href="#h1">TOP</a></p>

            
            <p id="2" class="blacktwentytwo">Why should I use this service to buy my next car?</p>
          <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">NO HAGGLE- We give you our best price up front in a "Current Market Study."
                Once you have placed an order, each vehicle we show you in our "Specific Vehicle Quote" will be consistent with the
                original quote you were given. NO HASSLE- With our online ordering system, you can do everything online or by phone.
                We consult with you to determine what vehicle best meets your needs, then buy it on the wholesale market when you are ready.
                GREAT PRICES ON SERVICED VEHICLES- Our efficient process allows us to buy the best vehicles on the wholesale market, service
                them, and still save you money! MASSIVE SELECTION- Our buyers are available to inspect, take pictures, and even record video
                of wholesale vehicles in the best markets in the nation. We have tens of thousands of vehicles available each week in our
                "virtual inventory". NO RISK- We guarantee you a vehicle in top condition. If the vehicle chosen does not pass the independent
                inspection, you do not have to buy it. GOOD CLEAN FUN- We have put the fun back in the car buying experience!
              </p>
            <p class="med"><a href="#h1">TOP</a></p>

            
            <p id="3" class="blacktwentytwo">Who is a typical 1-800-vehicles.com customer?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">Someone who is tired of the traditional car buying process and the difficulty
                of determining which vehicles are actually in great shape. They want a great deal on a quality vehicle and are willing to be somewhat
                patient, flexible and involved.</p>
            <p class="med"><a href="#h1">TOP</a></p>

            
            <p id="4" class="blacktwentytwo">How do I know that the car you deliver will be a good one?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">The efficiency of our direct process allows us to buy a legitimately nice vehicle
                at the wholesale market, service and detail it, and still save you money! Furthermore, our Specific Vehicle Purchase Agreement requires that your
                vehicle pass an independent inspection before you are asked complete the transaction. This "second opinion" helps to assure that nothing
                has been missed and that your vehicle is in the condition promised (after registering, check out our Specific Vehicle Purchase Agreement for more details).
             </p>
            <p class="med"><a href="#h1">TOP</a></p>

           
            <p id="5" class="blacktwentytwo">How long will it take you to locate my car?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">Most of the vehicles ordered are located within two to three weeks. The best way to
                minimize the search period is to choose a vehicle that shows "Excellent Availability" on our site, which often can be found within one week.
            </p>
            <p class="med"><a href="#h1">TOP</a></p>

           
            <p id="6" class="blacktwentytwo">I know what I want. How should I use your website?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">Go to "Research Specific Vehicles" to find estimates of price and general information on all 1
                to 6 year old autos, SUVs and minivans. From there you should register and request a "Current Market Study" on the precise vehicle you want. Then
                you will have our exact price and details about the availability.
            </p>
            <p class="med"><a href="#h1">TOP</a></p>

            
            <p id="7" class="blacktwentytwo">I am not sure what I want. How should I use your website?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">Go to "Research by Price Ranges" or "Research Excellent Availability" to shop the entire market
                for 1 to 6 year old autos, SUVs and minivans within your budget. After entering your specific criteria, these search tools will produce
                a comprehensive list of every vehicle available to you from the wholesale market, period. Then you can save as favorites the vehicles
                you will consider. If you need free and impartial consultation from us, simply register and go to "Request Consultation" from the home page, or call
                us at 1-800-VEHICLES (834-4253).
            </p>
            <p class="med"><a href="#h1">TOP</a></p>

            
            <p id="8" class="blacktwentytwo">Where are the price ranges for pick-up trucks?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">Pick-up trucks are so varied in the options available, it is difficult for us to give
                accurate price ranges until we know more about your specific needs and wants. Simply email us or call us to discuss your situation or go
                ahead and request a "Current Market Study" on the truck you want using the pick-up quote form.
            </p>
            <p class="med"><a href="#h1">TOP</a></p>

            
            <p id="9" class="blacktwentytwo">Do you take trade-ins?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">Yes, and we won't insult you! We just need to have the vehicle inspected to determine
                a fair trade-in value.
            </p>
            <p class="med"><a href="#h1">TOP</a></p>

            
            <p id="10" class="blacktwentytwo">What does "Excellent Availability!" mean?</p>
            <p class="blacktwelve" style="padding: 5px 20px 0px 20px;">Our research has shown that these vehicles are currently in plentiful supply at the
                national wholesale market. Vehicles with "Excellent Availability!" are often found within one week.
            </p>
            <p class="med"><a href="#h1">TOP</a></p>
        </div>
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php // require("why.php"); ?>
<?php require("footer.php"); ?>
<script>
$(document).ready(function(){
	 $('a[href^="#"]').on('click', function(event) {
        var target = $($(this).attr('href'));
		var nav_height = $('.navbar').height();
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top - nav_height
            }, 1000);
        }
    });
});
</script>

<?php require("footerend.php"); ?>
