<?php
    require("globals.php");
    require_once("checkaccess.php");
?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 12;
    $_SESSION['titleadd'] = 'Specific Vehicle Bought';

    if(!isset($_POST['SVQID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000512';
        header('Location: salesrepactions.php?ForUserID='.$_POST['ForUserID'].'&MarketNeedID='.$_POST['MarketNeedID']);
        exit();
    }

    $svqid = $_POST['SVQID'];
    $inneedid = $_POST['MarketNeedID'];
    $inuserid = $_POST['ForUserID'];
    $inusername = getuserfullname($inuserid, 'false');
    $userid = $_SESSION['userid'];

    $errormessage = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $cquery = "select * from purchases where SpecificVehicleID = ".$svqid;
        $cresult = mysql_query($cquery, $con);
        if($crow = mysql_fetch_array($cresult))
        {
            $c2query = "select * from purchases p, specificvehicles s where p.SpecificVehicleID = s.SpecificVehicleID and p.Status='Bought' and s.MarketNeedID = ".$inneedid;
            $c2result = mysql_query($c2query, $con);
            if($c2row = mysql_fetch_array($c2result)) $_SESSION['ShowError'] = 'A Vehicle has already been marked as Bought for this Customer Experience!';
            else
            {
                $query = "update purchases set Status='Bought',LastUpdated='".date_at_timezone('Y-m-d H:i:s', 'EST')."',BoughtOn='".date_at_timezone('Y-m-d H:i:s', 'EST')."',SalesReviewed=1,SalesRepID=".$userid." where SpecificVehicleID=".$svqid;
                mysql_query($query);

                $pquery = "select m.name 'Make', v.year 'Year', v.model 'Model', v.style 'Style' from specificvehicles s, vehicledetails v, makes m where m.MakeID=v.MakeID and s.VehicleDetailID=v.VehicleDetailID and s.SpecificVehicleID = ".$svqid;
                $presult = mysql_query($pquery, $con);
                if($prow = mysql_fetch_array($presult))
                {
                    $svyear[$index] = $prow[0];
                    $svmodel[$index] = $prow[1];
                    $svmake[$index] = $prow[2];
                    $svstyle[$index] = $prow[3];
                }

                posttodashboard($con, $userid, $inuserid, 'bought your '.$svyear.' '.$svmake.' '.$svmodel.' '.$svstyle.' and it is on its way to the dealership.', $inneedid);
                posttodashboard($con, $userid, $userid, 'bought the '.$svyear.' '.$svmake.' '.$svmodel.' '.$svstyle.' for <a href="'.WEB_SERVER_NAME.'salesrepactions.php?ForUserID='.$inuserid.'&MarketNeedID='.$inneedid.'">'.$inusername.'</a>.');
            }
        }
        else $_SESSION['ShowError'] = 'Customer has not selected the vehicle for purchase yet!';

        mysql_close($con);
    }

    header('Location: salesrepactions.php?ForUserID='.$_POST['ForUserID'].'&MarketNeedID='.$_POST['MarketNeedID']);
    exit();
?>
