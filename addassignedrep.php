<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 6;
    $_SESSION['titleadd'] = 'Sales Rep Assignment';

    if(!isset($_POST['AddMarketID']) || !isset($_POST['ForUserID']) || !isset($_POST['FranchiseeID']) || !isset($_POST['ForUserName'])
         || !isset($_POST['ForUserNeed']) || !isset($_POST['ForUserRef']) || !isset($_POST['ForUserRep']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000506';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    $userid = $_SESSION['userid'];

    $inneedid = $_POST['AddMarketID'];
    $inuserid = $_POST['ForUserID'];
    $infranid = $_POST['FranchiseeID'];
    $inusername = $_POST['ForUserName'];
    $inuserneed = $_POST['ForUserNeed'];
    $inuserref = $_POST['ForUserRef'];
    $inuserrep = $_POST['ForUserRep'];
    if(isset($_POST["SalesRep"])) $srep = getuserfirstname($_POST["SalesRep"]);
    else $srep = 'a Sales Rep';

    $errormessage = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // If anything was passed in, we are in a callback and should set the variables...
        if(isset($_POST["SalesRep"]))
        {
            $query = "insert into assignedreps (MarketNeedID, UserRepID, StartDate) values (".$inneedid.",".$_POST["SalesRep"].",'".date_at_timezone('Y-m-d H:i:s', 'EST')."')";
            if(!mysql_query($query, $con)) $_SESSION['ShowError'] = 'Could not assign the salesrep to the customer.';
            else
            {
                // Add a Message Update when this happens...
                if(!posttodashboard($con, $userid, $inuserid, 'assigned you to '.$srep.' who will be your representative and professional buyer.', $inneedid))
                  $_SESSION['ShowError'] .= 'Could not add Update Message to user dashboard.';

                $message = 'Dear '.$inusername.',<br/><br/>My name is '.$srep.' '.getuserlastnamenodb($con, $_POST["SalesRep"]).', and I have been assigned to be your sales representative ';
                $message .= 'and professional buyer from 1-800-vehicles.com. Please do not hesitate to call me any time at 1-800-VEHICLES (834-4253) or on my cell phone number ';
                $message .= 'listed on our website dashboard.  If you do not already know what vehicle you want, I will be happy to help you consider all the options available to';
                $message .= ' you. Also, please take advantage of the many great tools available to you on our website, including the "Firm Quote" request. I will reply promptly to';
                $message .= ' anything you do online.<br/><br/>I am looking forward to helping you find your dream car!<br/><br/>Sincerely,<br/><br/>'.$srep.'<br/>';
                if(!sendemail(getuseremailnodb($con, $inuserid), 'Your representative from 1-800-vehicles.com', $message, 'false'))
                    $_SESSION['ShowError'] .= 'Could not send the email to the user at '.$inuserid.'.\n';

                // Add Messages when this happens...
                if(!posttodashboard($con, $userid, $userid, $inusername.' was assigned to '.$srep.'.'))
                  $_SESSION['ShowError'] .= 'Could not add the Update Message to your dashboard';

                if(!posttodashboard($con, $userid, $_POST["SalesRep"], $firstname.' '.$lastname.' assigned <a href="'.WEB_SERVER_NAME.'salesrepactions.php?ForUserID='.$inuserid.'&MarketNeedID='.$inneedid.'">'.$inusername.'</a> to you.'))
                  $_SESSION['ShowError'] .= 'Could not add the Update Message to salesrep dashboard';

                mysql_close($con);

                if(!isset($_SESSION["ShowError"]) || ($_SESSION["ShowError"] == '')) $_SESSION["ShowError"] = 'Sales Rep Assignment Successful';

                header('Location: mydashboard.php#admintab');
                exit();
            }
        }

        // Otherwise, fill the list of salesreps to chose from...
        $query = "select Zip from useraddresses where IsPrimary=1 and UserID=".$inuserid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $zipcode = $row[0];
        }

        // Get the Franchise Name
        $query = "select Name from franchisees where FranchiseeID=".$infranid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $franname = $row[0];
        }

        // Get the Market Need Name
        $query = "select Title from marketneeds where MarketNeedID=".$inneedid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $mname = $row[0];
        }

        // get the current rep for this marketneed
        $currepid = getsalesrepnodb($con, $inuserid, $inneedid);
        $currepname = getuserfullnamenodb($con, $currepid, 'false');

        mysql_close($con);
    }

    // TODO: avoid need to close and reopen the mysql connection
    if(isset($zipcode)) $repids = getavailablereps($infranid, $zipcode);

    if(isset($repids) && (strlen($repids) > 0))
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $query = "select UserID, concat(FirstName,' ',LastName,' (',Email,')') AS FullName from users where UserID in (".$repids.") order by 2";
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $salesrepid[$index] = $row[0];
                $salesrepname[$index] = $row[1];
                $index++;
            }
        }
        mysql_close($con);
    }
    else $errormessage = 'There are no sales representatives available for zip code '.$zipcode.'.';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <p><a href="mydashboard.php#admintab">Back to Dashboard</a></p>
    <form action="addassignedrep.php" method="post" name="repform">
        <div class="grideightcontainer">
            <h1 class="subhead" style="width: 250px;">Assign a Sales Rep</h1>
            <div class="grideightgrey">
<?php
    echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;">';
    echo '<b>Customer:</b> '.$inusername.'<br/>';
    echo '<b>Zipcode:</b> '.$zipcode.' (in '.$franname.')<br/>';
    echo '<b>Experience:</b> '.$mname.' ('.$inneedid.')<br/>';
    echo '<b>Referred By:</b> '.$inuserref.'<br/>';
    echo '<b>Current Rep:</b> '.$currepname.' ('.$currepid.')<br/>';
    echo '<br/>';
    if($errormessage == 'false')
    {
        echo '<b>New Sales Rep: </b>';
        echo '<select name="SalesRep" id="SalesRep">';
        $count = count($salesrepid);
        for($i = 0; $i < $count; $i++)
        {
            if ($salesrepid[$i] != $currepid)
                echo '<option value="'.$salesrepid[$i].'" >'.$salesrepname[$i].' ('.$salesrepid[$i].')</option>';
            else
                echo '<option disabled value="'.$salesrepid[$i].'" >[Current Rep] '.$salesrepname[$i].' ('.$salesrepid[$i].')</option>';
        }
        echo '</select>';
    }
    else echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>'.$errormessage.'</strong></p>';
    echo '<input type="hidden" value="'.$infranid.'" name="FranchiseeID" />';
    echo '<input type="hidden" value="'.$inneedid.'" name="AddMarketID" />';
    echo '<input type="hidden" value="'.$inuserid.'" name="ForUserID" />';
    echo '<input type="hidden" value="'.$inusername.'" name="ForUserName" />';
    echo '<input type="hidden" value="'.$inuserneed.'" name="ForUserNeed" />';
    echo '<input type="hidden" value="'.$inuserref.'" name="ForUserRef" />';
    echo '<input type="hidden" value="'.$currep.'" name="ForUserRep" />';
?>
                <p><span class="gridfour"><button type="submit" value="" class="med">SET ASSIGNMENT</button></span></p>
            </div>
        </div><!-- grid eight container -->
    </form>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
