<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 0;
    $_SESSION['titleadd'] = 'Learn The Process';

    if(isset($_REQUEST['Page'])) $curpage = $_REQUEST['Page'];
    else $curpage = 0;
    if($curpage > 4) $curpage = 1;
    if($curpage < 0) $curpage = 0;
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
<?php
    switch($curpage)
    {
        case 0:
            echo '<h1 class="subhead" style="width:250px;">Learn The Process</h1>';
            break;
        case 1:
            echo '<h1 class="subhead" style="width:340px;">Why 1-800-vehicles.com?</h1>';
            break;
        case 2:
            echo '<h1 class="subhead" style="width:340px;">What Is 1-800-vehicles.com?</h1>';
            break;
        case 3:
            echo '<h1 class="subhead" style="width:285px;">Why Is It Revolutionary?</h1>';
            break;
        case 4:
            echo '<h1 class="subhead" style="width:350px;">How It Works For Customers</h1>';
            break;
    }
?>
        <div class="grideightgrey">
            <div class="grideight" style="width:580px; font-size:13px;">
<?php
    echo '<div align="center">';
    $max_width = 425;
    $max_height = 425;
    switch($curpage)
    {
        case 0:
            $imagename = 'common/layout/learning.jpg';
            echo '<img src="loadimage.php?image='.$imagename.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            break;
        case 1:
            $imagename = 'common/layout/together.jpg';
            echo '<img src="loadimage.php?image='.$imagename.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            break;
        case 2:
            $imagename = 'common/layout/whatis1800.jpg';
            echo '<img src="loadimage.php?image='.$imagename.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            break;
        case 3:
            $imagename = 'common/layout/revolutionary.jpg';
            echo '<img src="loadimage.php?image='.$imagename.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            break;
        case 4:
            $imagename = 'common/layout/customers.jpg';
            echo '<img src="loadimage.php?image='.$imagename.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
            break;
    }
    echo '</div>';
    switch($curpage)
    {
        case 0:
            echo '<p>The 1-800-vehicles.com process gives you a better way to buy legitimately nice vehicles at excellent prices. The information in this section will explain how we make it happen!</p>';
            echo '<p>Call 1-800-VEHICLES (834-4253) or click "Request Consultation" to get things going right away. Otherwise click on "Research Vehicles" to shop on our site.</p>';
            break;
        case 1:
            echo '<p><strong>CHOOSE FROM THE BEST</strong></p>';
            echo '<p>Only one fourth of used vehicles on the market are in excellent condition. Our cost efficient process allows us to pay what is necessary to acquire such vehicles and guarantee the condition. An independent inspection is made to confirm that your vehicle has met the terms of our agreement.</p><br/>';
            echo '<p><strong>SAVE TIME</strong></p>';
            echo '<p>You can do it all online and by phone. Avoid wasting time visiting dealerships to look at vehicles or calling about vehicles listed out of state, especially since three fourths of them are not in excellent condition anyway.</p><br/>';
            echo '<p><strong>SAVE MONEY</strong></p>';
            echo '<p>Our "virtual inventory" process saves you money in three important ways:<br /><ul><li>We don`t have an expensive property to hold these vehicles in inventory</li><li>We avoid high interest and advertising expenses</li><li>We take advantage of today`s lower wholesale market prices</li></ul></p><br/>';
            echo '<p><strong>SAVE HASSLES</strong></p>';
            echo '<p>1-800-vehicles.com representatives provide free consulting that is professional and impartial, and based on a massive "virtual inventory" of vehicles. They will help you determine what vehicle best meets your needs, not pressure you to buy something that has been sitting in inventory for months.<br />We have put the fun back in the car buying experience!</p>';
            break;
        case 2:
            echo '<p>1-800-vehicles.com is a revolutionary automotive franchise system that was developed in 2001. It is a powerful brand, a proven system and a quality group of dealer franchises offering consumers a better way to buy legitimately nice vehicles at excellent prices.</p>';
            echo '<p>As part of "America�s online dealership", 1-800-vehicles.com dealer franchises capture business in and around their market with an exclusive territory and a massive "virtual inventory" of vehicles from which to choose.</p>';
            echo '<p>1-800-vehicles.com customers are able to consult with a professional buyer without obligation, get firm pricing up front, then place an order through this highly efficient and professional process. Once the perfect vehicle has been located at the wholesale market, it is fully serviced and independently inspected to confirm that the condition is as promised.</p>';
            break;
        case 3:
            echo '<p>Because the 1-800-vehicles.com system allows you to buy a legitimately nice vehicle at a great price through a low pressure online process. You do it all from your home, office or even on the go with a smart phone! You get honest advice about vehicles that best meet your specific needs, wants and budget, instead of pressure to buy something in dealer stock.</p>';
            echo '<p>The "just in time" aspect of the process makes it possible for 1-800-vehicles.com dealers to show you only the best of a massive wholesale "virtual inventory", and still save you time, money and hassles along the way. Rather than searching the internet for a vehicle, then trying to determine if the dealer is reputable -- or if the vehicle is any good at all, 1-800-vehicles.com customers work with one representative who handles everything.</p>';
            echo '<p>The 1-800-vehicles.com process is revolutionary because it offers customers:</p>';
            echo '<ul>';
            echo '<li>Honest and professional consultation without pressure</li>';
            echo '<li>A massive number of vehicles from which to choose</li>';
            echo '<li>A tremendous savings in time, energy and frustration</li>';
            echo '<li>The cost efficiency of ordering a vehicle from the wholesale market </li>';
            echo '<li>Confidence  through an inspection by an independent mechanic</li>';
            echo '</ul>';
            break;
        case 4:
            echo '<p>Customers use the 1-800-vehicles.com website to consider all of the vehicles available within their price range, including vehicles with "Excellent Availability". A "Quick Assessment" is available as a scientific tool to help anyone involved with the decision making process. 1-800-vehicles.com representatives offer free consulting and give "Current Market Studies" on vehicles being considered, and they will determine the availability of each.</p>';
            echo '<p>When the order is placed, 1-800-vehicles.com customers can sit back and relax while their representative searches the national wholesale market for the perfect vehicle. Once found and approved by the customer, the buying team purchases the vehicle and it is taken through a sophisticated process of inspection, detailing and service. All of this can be closely monitored by the customer.</p>';
            echo '<p>An independent inspection assures the customer that the vehicle is in top condition, and nothing is left to chance. 1-800-vehicles.com customers can accept their vehicle with confidence knowing that they have made a great deal!</p>';
            break;
    }
    echo '<br />';
    echo '<div align="center">';
    echo '<form method="post" action="consult.php">';
    echo '<button value="" class="med" style="float:left;"><nobr>REQUEST CONSULTATION</nobr></button>';
    echo '</form>';
    echo '<form method="post" action="researchvehicles.php">';
    echo '<button value="" class="med" style="float:left;"><nobr>RESEARCH VEHICLES</nobr></button>';
    echo '</form>';
    if($curpage < 3)
    {
        echo '<form method="post" action="learntheprocess.php">';
        echo '<input type="hidden" value="'.($curpage+1).'" name="Page" />';
        echo '<button value="" class="med" style="float:right;"><nobr>CONTINUE</nobr></button>';
        echo '</form>';
    }
    echo '</div>';
?>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->

    <!--START A FRANCHISE NAV-->
    <div id="franchise" style="margin:0px; float:right; margin-top:70px;">
        <p style="font-size:17px; color:#85c11b;">Learn The Process</p>
        <p <?php if($curpage == 1) echo 'class="current"'; ?>><a href="learntheprocess.php?Page=1">Why 1-800-vehicles.com?</a></p>
        <p <?php if($curpage == 2) echo 'class="current"'; ?>><a href="learntheprocess.php?Page=2">What Is 1-800-vehicles.com?</a></p>
        <p <?php if($curpage == 3) echo 'class="current"'; ?>><a href="learntheprocess.php?Page=3">Why Is It Revolutionary?</a></p>
        <p <?php if($curpage == 4) echo 'class="current"'; ?>><a href="learntheprocess.php?Page=4">How It Works For Customers</a></p>
        <br/>
        <p style="font-size:17px; color:#85c11b;">More Information</p>
        <p><a href="common/tutorial.swf" target="_blank">Quick Tutorial: Getting Started</a></p>
        <p><a href="faq.php">FAQ</a></p>
        <p><a href="testimony.php" target="_blank">Testimonials</br>
        </a></p>
    </div><!-- END FRANCHISE NAV-->
</div><!--end content-->

<?php require("testimonials.php"); ?>
<?php require("footerstart.php"); ?>
<?php require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
