<?php require("globals.php"); ?>  
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 15;
    $_SESSION['titleadd'] = 'Why 1-800-vehicles.com';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

<style>
    .grideight > ul {
    list-style: outside none none;
}
.footdesc {
    color: black;
    font-size: 14px;
    margin-top: 10px;
}
ul, ol {
    margin: 0;
    padding: 0;
}

h3 {
    font-weight: bold;
}
</style>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Why 1-800-vehicles.com?</h1>
        <div class="grideightgrey">
            <div class="grideight">
                <span style="font-size: 14px;font-style: italic;">
                  Because our revolutionary process allows you to choose a vehicle from the best of our nation's wholesale inventory, and save time, money and hassles in the process.
              </span>
          <ul>
            <li><h3>CHOOSE FROM THE BEST</h3>
              <div align="center">
                    <img class="img-responsive" src="common/layout/footercars.jpg" />
              </div>
                
                <div class="footdesc">
                    Only one fourth of used vehicles on the market are in excellent condition. Our cost efficient process allows us to pay what is necessary to acquire such vehicles
                    and guarantee the condition. An independent inspection is made to confirm that your vehicle has met the terms of our agreement.
                </div>
                 <img  src="common/layout/short-bar.gif" style="padding: 10px;width: 100%" border="0" />
            </li>
            <li> <h3>SAVE TIME</h3>
              <div align="center">
                    <img class="img-responsive" src="common/layout/footercars2.jpg" />
                 </div>
               
                <div class="footdesc">
                    You can do it all online and by phone. Avoid wasting time visiting dealerships to look at vehicles or calling about vehicles listed out of state, especially since
                    three fourths of them are not in excellent condition anyway.
                </div>
                 <img  src="common/layout/short-bar.gif" style="padding: 10px;width: 100%" border="0" />
            </li>
            <li>
                  <h3>SAVE MONEY</h3>
                <div align="center">
                    <img class="img-responsive" src="common/layout/footercars3.jpg" />
                </div>
              
                <div class="footdesc">
                    Our "virtual inventory" process saves you money in three important ways:<br />
                    1) We don't have an expensive property to hold these vehicles in inventory<br />
                    2) We avoid high interest and advertising expenses<br />
                    3) We take advantage of today's lower wholesale market prices
                </div>
                   <img  src="common/layout/short-bar.gif" style="padding: 10px;width: 100%" border="0" />
            </li>
            <li>
                <h3>SAVE HASSLES</h3>
               <div align="center">
                    <img class="img-responsive" src="common/layout/footercars4.jpg" />
                </div>
               
                <div class="footdesc">
                    1-800-vehicles.com representatives provide free consulting that is professional and impartial, and based on a massive "virtual inventory" of vehicles. They will
                    help you determine what vehicle best meets your needs, not pressure you to buy something that has been sitting in inventory for months.<br />
                    <span class="footer_callout" style="margin-top: 2px;">
                        We have put the fun back in the car buying experience!
                    </span>                    
                </div>
            </li>
        </ul>
                
                
            </div> <!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
