<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = 'Excellent Availability';

    $types[1] = 'Auto';
    $types[2] = 'SUV';
    $types[3] = 'MiniVan';

    // See if Make was selected already and this is a refresh...
    $type = $_REQUEST['typeitem'];
    if(!isset($type) || strlen($type) <= 0)
    {
        $type = $types[1];
    }

    switch($type)
    {
        case 'Auto':
            $styles[1] = '4 Door Sedan';
            $styles[2] = '2 Door Coupe';
            $styles[3] = '2 Door Convertible';
            $styles[4] = '3 Door Hatchback';
            $styles[5] = '5 Door Hatchback';
            $styles[6] = '5 Door Wagon';
            break;
        case 'SUV':
            $styles[1] = '2 Door 2WD';
            $styles[2] = '2 Door 4WD';
            $styles[3] = '4 Door 2WD';
            $styles[4] = '4 Door 4WD';
            break;
        case 'MiniVan':
            $styles[1] = 'Dual Sliding Doors';
            break;
    }

    // See if Style was selected already and this is a refresh...
    $style = $_REQUEST['styleitem'];
    if((!isset($style) || strlen($style) <= 0) && $index > 0)
    {
        $style = $styles[1];
    }

    // Check what was in fromselect...
    $from = $_REQUEST['fromselect'];
    if(!isset($from))
    {
        $from = 'Any';
    }
?>
<?php require("headerstart.php"); ?>
<script language=JavaScript>
    function itemchanged()
    {
        for (var i=0; i < document.excellent.fromselect.length; i++)
        {
            if (document.excellent.fromselect[i].checked)
            {
                var vfrom = document.excellent.fromselect[i].value;
            }
        }
        var vtype = document.getElementById("typelist");
        var vstyle = document.getElementById("stylelist");
        self.location='excellent.php?' +
            'typeitem=' + vtype.options[vtype.selectedIndex].value +
            '&fromselect=' + vfrom +
            '&styleitem=' + vstyle.options[vstyle.selectedIndex].value;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>

<div id="content" class="excellent-top">
    <div class="grideightcontainer">
       <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;"> Excellent Availability</h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0;"><a href="researchvehicleoptions.php">Go back to Research Vehicles</a></p>
            <div class="grideight" style="margin-top: 0px;">
                <form action="excellentresults.php" method="get" name="excellent">
                    <p class="greythirteen">
                        Our research has shown that these vehicles are currently in plentiful supply on the national <br />
                        wholesale market.  The research results shown are accurate examples of each vehicle.
                    </p>
                    
                     <table class="table">
						<tr>
							<td style="width:30%"><strong>Vehicle Type</strong></td>
							<td style="width:40%">
    <select name="typelist" id="typelist" onchange="javascript:itemchanged()">
<?php
    $count = count($types);
    for($i = 1; $i <= $count; $i++)
    {
        echo '<option value="'.$types[$i].'"';
        if(isset($type) && $types[$i]==$type) echo 'selected="selected"';
        echo '>'.$types[$i].'</option>';
    }
?>
                    </select>
							</td>
						</tr>
                                                <tr>
							<td style="width:30%"><strong>From</strong></td>
							<td style="width:40%">
                                           <input name="fromselect" type="radio" value="Any" <?php if($from=='Any') echo 'checked '; ?>/> Any &nbsp;&nbsp;
                                            <input name="fromselect" type="radio" value="Import" <?php if($from=='Import') echo 'checked '; ?>/> Import &nbsp;&nbsp;
                                            <input name="fromselect" type="radio" value="Domestic" <?php if($from=='Domestic') echo 'checked '; ?>/> Domestic &nbsp;&nbsp;
							</td>
						</tr>
                                         
                                                <tr>
							<td style="width:30%"><strong>Class</strong></td>
							<td style="width:40%">
                                                <select name="stylelist" id="stylelist" onchange="javascript:itemchanged()">
<?php
    $count = count($styles);
    for($i = 1; $i <= $count; $i++)
    {
        echo '<option value="'.$styles[$i].'"';
        if(isset($style) && $styles[$i]==$style) echo 'selected="selected"';
        echo '>'.$styles[$i].'</option>';
    }
?>
                    </select>
							</td>
						</tr>                                               
                                                <tr>
                                                    <td colspan="2">
                                                      <button type="submit" value="" class="med">SEARCH</button>
                                                    </td>
                                                </tr>
                        </table>                
                </form>
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php
    $_SESSION['hideteaser3'] = 'true';
    require("teaser.php");
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
