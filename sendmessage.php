<?php
    require("globals.php");
    require_once("checkaccess.php");
?>
<script type="text/javascript">
<?php
    if(isset($_POST['MessageToID'])) $usertoID = $_POST['MessageToID'];
    else
    {
        echo 'window.opener = "x";';
        echo 'window.close();';
    }

    if(isset($_POST['PostBack']))
    {
        // We have our message to send now...
        //posttodashboardnodb($userid, $usertoID, 'said: '.$_POST['themessage']);
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            posttodashboard($con, $userid, $usertoID, 'said: '.$_POST['themessage']);
            //$query = "insert into messages (created, message, userfromid, usertoid) values ('".date_at_timezone('Y-m-d H:i:s', 'EST')."','".$_POST['themessage']."',".$userid.",".$usertoID.")";
            //mysql_query($query, $con);

            $uquery = "select firstname, lastname from users where userid=".$usertoID;
            $uresult = mysql_query($uquery);
            if($urow = mysql_fetch_array($uresult))
                posttodashboard($con, $userid, $userid, 'told '.$urow[0].' '.$urow[1].': '.$_POST['themessage']);

            mysql_close($con);
        }
    }
?>
    function trimAll(sString)
    {
        while(sString.substring(0,1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while(sString.substring(sString.length-1, sString.length) == ' ')
        {
            sString = sString.substring(0,sString.length-1);
        }
        return sString;
    }

    function validateFormOnSubmit()
    {
        var vmess = document.getElementById("themessage");
        var tString = trimAll(vmess.value);
        if(tString.length < 1)
        {
            vmess.style.background = '#ffcccc';
            vmess.focus();
            alert("Error:"+'\n'+"Message must be entered."+'\n'+'\n');
            return false;
        }
        return true;
    }

    function clearclicked()
    {
        var vmess = document.getElementById("themessage");
        vmess.value = '';
    }

    function cancelclicked()
    {
        window.opener = "x";
        window.close();
        self.close();
    }

    function checksize()
    {
        if((pageWidth() < 450) || (pageHeight() < 550))
        {
            var w = 450;
            var h = 550;
            if(parseInt(navigator.appVersion) > 3)
            {
                if(navigator.appName == "Netscape")
                {
                    top.outerWidth=w;
                    top.outerHeight=h;
                }
                else top.resizeTo(w,h);
            }
            else window.resizeTo(w,h);
        }
        // set focus to the textarea
        document.theform.themessage.focus();
    }

    function pageWidth()
    {
        return
            window.innerWidth != null ?
                window.innerWidth :
                document.documentElement && document.documentElement.clientWidth ?
                    document.documentElement.clientWidth :
                    document.body != null ?
                        document.body.clientWidth :
                        null;
    }

    function pageHeight()
    {
        return window.innerHeight != null ?
            window.innerHeight :
            document.documentElement && document.documentElement.clientHeight ?
                document.documentElement.clientHeight :
                document.body != null ?
                    document.body.clientHeight :
                    null;
    }

    function posLeft()
    {
        return typeof window.pageXOffset != 'undefined' ?
            window.pageXOffset :
            document.documentElement && document.documentElement.scrollLeft ?
                document.documentElement.scrollLeft :
                document.body.scrollLeft ?
                    document.body.scrollLeft :
                    0;
    }

    function posTop()
    {
        return typeof window.pageYOffset != 'undefined' ?
            window.pageYOffset :
            document.documentElement && document.documentElement.scrollTop ?
                document.documentElement.scrollTop :
                document.body.scrollTop ?
                    document.body.scrollTop :
                    0;
    }

    function posRight()
    {
        return posLeft() + pageWidth();
    }

    function posBottom()
    {
        return posTop() + pageHeight();
    }

<?php
    if(isset($_POST['PostBack']))
    {
        echo 'window.opener = "x";';
        echo 'window.close();';
        echo 'self.close();';
    }
?>

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
    <head>
        <title>1-800-Vehicles.com - Send Message</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="robots" content="" />
        <link href="style.css" rel="stylesheet" type="text/css" />
        <link href="screen.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="droplinetabs.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="common/scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="common/scripts/easySlider1.7.js"></script>
        <script type="text/javascript" src="common/scripts/master.js"></script>
        <script type="text/javascript" src="common/scripts/jquery.hoverIntent.minified.js"></script>
    </head>
<body onload="javascript:checksize()">
    <div id="wrapper">
        <div id="container" style="width:400px;">
            <div id="content" style="width:400px;">
                <h1 class="subhead">Dashboard Post</h1>
                <div class="grideightgrey" style="width:380px;">
                    <form name="theform" action="sendmessage.php" onsubmit="return validateFormOnSubmit();" method="post">
                        <input type="hidden" value="<?php echo $usertoID; ?>" name="MessageToID" />
                        <input type="hidden" value="true" name="PostBack" />
                        <p style="font-size:13px; color:#142c3c; margin-left:20px;"><strong>Message</strong></p>
                        <textarea id="themessage" name="themessage" cols="48" rows="8" style="margin-left:20px;"><?php echo $query; ?></textarea>
                        <br clear="all" /><br />
                        <div style="margin-left:70px;">
                            <button type="submit" value="send" class="med"><nobr>SEND</nobr></button>
                            <button type="button" value="clear" class="med" style="margin-left:10px;" onclick="javascript:clearclicked();" ><nobr>CLEAR</nobr></button>
                            <button type="button" value="cancel" class="med" style="margin-left:10px;" onclick="javascript:cancelclicked();" ><nobr>CANCEL</nobr></button>
                        </div>
                    </form>
                    <br />
                </div> <!-- endgrideightgrey -->
            </div><!--end content-->
        </div><!--end container-->
    </div><!--end wrapper-->
</body>
</html>
