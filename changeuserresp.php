<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 5;
    $_SESSION['titleadd'] = 'Change User Responsibility';

    if(!isset($_POST['ChangeUserID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000505';
        header('Location: /mydashboard.php');
        exit();
    }

    $inuserid = $_POST['ChangeUserID'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // If anything was passed in, we are in a callback and should set the variables...
        if(isset($_POST["genman"]) || isset($_POST["opsman"]) || isset($_POST["buyer"]) || isset($_POST["salesrep"]) || isset($_POST["cust"]))
        {
            // We have info, so lets update the database...
            if(isset($_POST["genman"]))
            {
                $query = "select * from userprofiles where UserID=".$inuserid." and ProfileID = 6";
                $result = mysql_query($query, $con);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    $query = "insert into userprofiles (UserID, ProfileID) values (".$inuserid.",6)";
                    mysql_query($query, $con);
                }
            }
            else
            {
                $query = "delete from userprofiles where UserID = ".$inuserid." and ProfileID = 6";
                mysql_query($query, $con);
            }

            if(isset($_POST["opsman"]))
            {
                $query = "select * from userprofiles where UserID=".$inuserid." and ProfileID = 5";
                $result = mysql_query($query, $con);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    $query = "insert into userprofiles (UserID, ProfileID) values (".$inuserid.",5)";
                    mysql_query($query, $con);
                }
            }
            else
            {
                $query = "delete from userprofiles where UserID = ".$inuserid." and ProfileID = 5";
                mysql_query($query, $con);
            }

            if(isset($_POST["buyer"]))
            {
                $query = "select * from userprofiles where UserID=".$inuserid." and ProfileID = 7";
                $result = mysql_query($query, $con);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    $query = "insert into userprofiles (UserID, ProfileID) values (".$inuserid.",7)";
                    mysql_query($query, $con);
                }
            }
            else
            {
                $query = "delete from userprofiles where UserID = ".$inuserid." and ProfileID = 7";
                mysql_query($query, $con);
            }

            if(isset($_POST["salesrep"]))
            {
                $query = "select * from userprofiles where UserID=".$inuserid." and ProfileID = 8";
                $result = mysql_query($query, $con);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    $query = "insert into userprofiles (UserID, ProfileID) values (".$inuserid.",8)";
                    mysql_query($query, $con);
                }
            }
            else
            {
                $query = "delete from userprofiles where UserID = ".$inuserid." and ProfileID = 8";
                mysql_query($query, $con);
            }

            if(isset($_POST["cust"]))
            {
                $query = "select * from userprofiles where UserID=".$inuserid." and ProfileID = 1";
                $result = mysql_query($query, $con);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    $query = "insert into userprofiles (UserID, ProfileID) values (".$inuserid.",1)";
                    mysql_query($query, $con);
                }
            }
            else
            {
                $query = "delete from userprofiles where UserID = ".$inuserid." and ProfileID = 1";
                mysql_query($query, $con);
            }

            mysql_close($con);
            header('Location: mydashboard.php');
            exit();
        }

        mysql_close($con);
    }

    // Now get the current state...
    if(getuserprofile($inuserid, 'Customer') == 'true') $cust = 1;
    else $cust = 0;
    if(getuserprofile($inuserid, 'General Manager') == 'true') $genman = 1;
    else $genman = 0;
    if(getuserprofile($inuserid, 'Operations Manager') == 'true') $opsman = 1;
    else $opsman = 0;
    if(getuserprofile($inuserid, 'Researcher') == 'true') $buyer = 1;
    else $buyer = 0;
    if(getuserprofile($inuserid, 'Sales Representative') == 'true') $salesrep = 1;
    else $salesrep = 0;
    $fullname = getuserfullname($inuserid, 'false');
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function validateFormOnSubmit(theForm)
    {
        var reason = "";
        if
        (
            (theForm.elements["genman"] && !theForm.elements["genman"].checked)
            && !theForm.elements["opsman"].checked
            && !theForm.elements["buyer"].checked
            && !theForm.elements["salesrep"].checked
        )
        {
            reason += "At least one responsibility must be checked."+'\n';
        }
        if(reason != "")
        {
            alert("Some fields need correction:"+'\n' + reason + '\n');
            return false;
        }

        return true;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <p><a href="mydashboard.php#admintab">Back to Dashboard</a></p>
    <form action="changeuserresp.php" onsubmit="return validateFormOnSubmit(this)" method="post" name="userform">
        <input type="hidden" value="<?php echo $inuserid; ?>" name="ChangeUserID" />
        <div class="grideightcontainer">
            <h1 class="subhead" style="width: 250px;">Responsibilities</h1>
            <div class="grideightgrey">
                <p><strong>User:</strong> <?php echo $fullname; ?></p>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
<?php
    if(!isset($_POST['NoGMOption']))
    {
        echo '<input name="genman" id="genman" type="checkbox" value="6" ';
        if(isset($genman) && ($genman == 1)) echo 'checked="checked" ';
        echo '/>General Manager<BR />';
    }
    elseif(isset($genman) && ($genman == 1)) echo '<input name="genman" value="'.$genman.'" type="hidden" />';
?>
                            <input name="opsman" id="opsman" type="checkbox" value="5" <?php if(isset($opsman) && ($opsman == 1)) echo 'checked="checked"' ?> />Operations Manager<br/>
                            <input name="buyer" id="buyer" type="checkbox" value="7" <?php if(isset($buyer) && ($buyer == 1)) echo 'checked="checked"' ?> />Researcher<br/>
                            <input name="salesrep" id="salesrep" type="checkbox" value="8" <?php if(isset($salesrep) && ($salesrep == 1)) echo 'checked="checked"' ?> />Sales Representative<br/><br/>
                            <input name="cust" id="cust" type="checkbox" value="8" <?php if(isset($cust) && ($cust == 1)) echo 'checked="checked"' ?> />Customer<br/>
                        </td>
                    </tr>
                </table>
                <p><span class="gridfour"><button type="submit" value="" class="med">SET RESPONSIBILITIES</button></span></p>
            </div>
        </div><!-- grid eight container -->
    </form>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
