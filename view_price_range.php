<?php require("globals.php"); 
require_once("common/functions/Vehicle.php");?>
<?php
   
 
    if(isset($_REQUEST["makelist"]) && isset($_REQUEST["yearlist"]) && isset($_REQUEST["modellist"]))
    {
        
       
        $inmake = $_REQUEST["makelist"]; 
        $inyear = $_REQUEST["yearlist"];
        $inmodel = $_REQUEST["modellist"];
        $instyle = $_REQUEST["style"];
        $in= explode(";", $_REQUEST["style"]);
       $instyle=$in[0];
       
       
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
          
            // Get the Make ID since we need it...
            mysql_select_db(DB_SERVER_DATABASE, $con);
            $query = 'select makeid, origin, Name from makes where MakeID='.$inmake;
            $result = mysql_query($query);
           
            if($result && $row = mysql_fetch_array($result))
            {
               
                $inmakeid = $row['makeid'];
                $inorigin = $row['origin'];
				$inname = $row['Name'];

                // Verify that the item requested does still exist in the database...
                $query = 'select distinct(v.vehicleid), v.doors, v.bodytype, v.convertible, v.wheeldrive, v.slidingdoors, v.type, v.imagefile, v.style from vehicles v, vehicledata d where v.VehicleID=d.VehicleID and v.makeid='.$inmakeid.' and v.year='.$inyear.' and v.model="'.$inmodel.'" and v.vehicleid="'.$instyle.'" order by d.PriceStart asc, d.PriceEnd asc, v.style asc';
                $result = mysql_query($query);
                $index = 0;
				$instyle=array();
                while($result && $row = mysql_fetch_array($result))
                {
                  
                    // It does, so now we need to get the details for it...
                    $invehid[$index] = $row['vehicleid'];
                    $innum_doors[$index] = $row['doors'];
                    $inbody_type[$index] = $row['bodytype'];
                    $inconvertible[$index] = $row['convertible'];
                    $inwheel_drive[$index] = $row['wheeldrive'];
                    $insliding_doors[$index] = $row['slidingdoors'];
                    $intype[$index] = $row['type'];
                    $invimage[$index] = $row[7];//$row['imagefile'];
                    //echo $invimage[$index].'<br/>';
                    $instyle[$index] = $row['style'];
                  
                    switch($intype[$index])
                    {
                        case 'Auto':
                            if($inbody_type[$index] == 'Hatchback')
                            {
                                if($innum_doors[$index] > 3) $invehtype[$index] = '5 Door Hatchback';
                                else $invehtype[$index] = '3 Door Hatchback';
                            }
                            elseif($inconvertible[$index] == 'Yes') $invehtype[$index] = $innum_doors[$index].' Door Convertible';
                            else
                            {
                                if($innum_doors[$index] == 4) $invehtype[$index] = '4 Door Sedan';
                                elseif($innum_doors[$index] == 2) $invehtype[$index] = '2 Door Coupe';
                                else $invehtype[$index] = '5 Door Wagon';
                            }
                            break;
                        case 'SUV':
                            $invehtype[$index] = $innum_doors[$index].' Door '.$inwheel_drive[$index].'WD';
                            break;
                        case 'MiniVan':
                            if($insliding_doors[$index] == 'Dual') $invehtype[$index] = 'Dual Sliding Doors';
                            else $invehtype[$index] = 'Mini Van';
                            break;
                    }

                    // First we retrieve the details for the high mileage side...
                    $highquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=0 and vehicleid = ".$invehid[$index];
                    $hresult = mysql_query($highquery);
                    if($hrow = mysql_fetch_array($hresult))
                    {
                        $inhighbb[$index] = $hrow['bestbuy'];
                        $inhighmilestart[$index] = $hrow['mileagestart'];
                        $inhighmileend[$index] = $hrow['mileageend'];
                        $inhighpricestart[$index] = $hrow['pricestart'];
                        $inhighpriceend[$index] = $hrow['priceend'];
                    }

                    // Next we retrieve the details for the low mileage side...
                    $lowquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=1 and vehicleid = ".$invehid[$index];
                    $lresult = mysql_query($lowquery);
                    if($lrow = mysql_fetch_array($lresult))
                    {
                        $inlowbb[$index] = $lrow['bestbuy'];
                        $inlowmilestart[$index] = $lrow['mileagestart'];
                        $inlowmileend[$index] = $lrow['mileageend'];
                        $inlowpricestart[$index] = $lrow['pricestart'];
                        $inlowpriceend[$index] = $lrow['priceend'];
                    }
                    $index++;
                }
            }

            mysql_close($con);
        }
    }

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        // First fill the list of Makes...
        mysql_select_db(DB_SERVER_DATABASE, $con);
        $result = mysql_query("select distinct m.makeid 'makeid', m.name 'make' from makes m, vehicles v where m.makeid = v.makeid and v.visible=1 order by 2");
        $data = '';
        $index = 1;
        while($result && $row = mysql_fetch_array($result))
        {
            $data = trim($row['make']);
            if(strlen($data) > 0)
            {
                $makeids[$index] = $row['makeid'];
                $makes[$index] = $data;
                $index++;
            }
        }

        // See if Make was selected already and this is a refresh...
        if(isset($_REQUEST['makelist']) && strlen($_REQUEST['makelist']) > 0)
        {
            $make = $_REQUEST['makelist'];
            // Get the ID...
            $query = 'select makeid from makes where name="'.$make.'"';
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                $makeid = $row['makeid'];
            }
            else if($index > 0)
            {
                $make = $makes[1];
                $makeid = $makeids[1];
            }
        }
        else if($index > 0)
        {
            $make = $makes[1];
            $makeid = $makeids[1];
        }

        // See if we are in a valid state...
        if(isset($makeid))
        {
            if(isset($_REQUEST['yearlist']) && isset($_REQUEST['modellist']))
            //if(isset($_REQUEST['yearlist']) && isset($_REQUEST['modellist']) && isset($_REQUEST['stylelist']))
            {
                $year = $_REQUEST['yearlist'];
                $model = $_REQUEST['modellist'];
                //$style = $_REQUEST['stylelist'];
                $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                 //$query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."' and style='".$style."'";
                $result = mysql_query($query);
                if(!$result || !$row = mysql_fetch_array($result))
                {
                    //unset($style);
                    $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                    $result = mysql_query($query);
                    if(!$result || !$row = mysql_fetch_array($result))
                    {
                        unset($model);
                        $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            unset($year);
                        }
                    }
                }
            }
            else
            {
                //if(isset($_REQUEST['stylelist'])) unset($_REQUEST['stylelist']);
                if(isset($_REQUEST['yearlist']) && isset($_REQUEST['modellist']))
                {
                    $year = $_REQUEST['yearlist'];
                    $model = $_REQUEST['modellist'];
                    $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year." and model='".$model."'";
                    $result = mysql_query($query);
                    if(!$result || !$row = mysql_fetch_array($result))
                    {
                        unset($model);
                        $query = "select vehicleid from vehicles where visible=1 and veh_makeid=".$makeid." and year=".$year;
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            unset($year);
                        }
                    }
                }
                else
                {
                    if(isset($_REQUEST['modellist'])) unset($_REQUEST['modellist']);
                    if(isset($_REQUEST['yearlist']))
                    {
                        $year = $_REQUEST['yearlist'];
                        $query = "select vehicleid from vehicles where visible=1 and makeid=".$makeid." and year=".$year;
                        $result = mysql_query($query);
                        if(!$result || !$row = mysql_fetch_array($result))
                        {
                            unset($year);
                        }
                    }
                }
            }
        }
        else
        {
            if(isset($year)) unset($year);
            if(isset($model)) unset($model);
            //if(isset($style)) unset($style);
        }

        // Set up our year search string...
        $yearquery = "select distinct year from vehicles where visible=1";
        $modelquery = "select distinct model from vehicles where visible=1";
        //$stylequery = "select distinct style from vehicles where visible=1";
        if(isset($makeid))
        {
            $yearquery .= " and makeid=".$makeid;
            $modelquery .= " and makeid=".$makeid;
            //$stylequery .= " and makeid=".$makeid;
        }
        $yearquery .= " order by 1 desc";

        // Get the Years...
        $result = mysql_query($yearquery);
        $index = 1;
        while($result && $row = mysql_fetch_array($result))
        {
            $data = trim($row['year']);
            if(strlen($data) > 0)
            {
                $years[$index] = $data;
                $index++;
            }
        }
        if(!isset($year) && $index > 0)
        {
            $year = $years[1];
        }

        // Set up our model search string...
        if(isset($year))
        {
            $modelquery .= " and year=".$year;
            //$stylequery .= " and year=".$year;
        }
        $modelquery .= " order by 1";

        // Get the Models...
        $result = mysql_query($modelquery);
        $index = 1;
        while($result && $row = mysql_fetch_array($result))
        {
            $data = trim($row['model']);
            if(strlen($data) > 0)
            {
                $models[$index] = $data;
                $index++;
            }
        }
        if(!isset($model) && $index > 0)
        {
            $model = $models[1];
        }

        // Set up our style search string...
        //if(isset($model))
        //{
        //    $stylequery .= " and model='".$model."'";
        //}
        //$stylequery .= " order by 1";

        // Get the Styles...
        //$result = mysql_query($stylequery);
        //$index = 1;
        //while($result && $row = mysql_fetch_array($result))
        //{
        //    $data = trim($row['style']);
        //    if(strlen($data) > 0)
        //    {
        //        $styles[$index] = $data;
        //        $index++;
        //    }
        //}
        //if(!isset($style) && $index > 0)
        //{
        //    $style = $styles[1];
        //}
        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>

</script>
<style>
.caption {
	float: left;
	font-size: 30px;
	position: absolute;
}
.slides
{
	top:32px !important;
}
</style>
<script type="text/javascript" src="demovehicles/js/jssor.core.js"></script>
<script type="text/javascript" src="demovehicles/js/jssor.utils.js"></script>
<script type="text/javascript" src="demovehicles/js/jssor.slider.js"></script>
<script>
//    $(document).ready(function() {
//        $('#test_close').click(function(){
//           
//            jQuery('#cboxClose').click();
//        
//        });
//    });
</script>
     
 
    
    
<script language=JavaScript>
    function itemchanged()
    {
        var vmake = document.getElementById("makelist");
        var vyear = document.getElementById("yearlist");
        var vmodel = document.getElementById("modellist");
        //var vstyle = document.getElementById("stylelist");
        var loc = 'researchspecific.php?' +
            'makelist=' + vmake.options[vmake.selectedIndex].value +
            '&yearlist=' + vyear.options[vyear.selectedIndex].value +
            '&modellist=' + vmodel.options[vmodel.selectedIndex].value +
            //'&stylelist=' + vstyle.options[vstyle.selectedIndex].value +
            '#refresh';
        self.location=loc;
    }
</script>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
         
        <div class="grideightgrey">
            
                <p class="greythirteen">
                    
                   <!-- EXAMPLE PICTURES-->
                   <!-- The pictures shown are accurate examples of the vehicle you have chosen. &nbsp; Get a current availability estimate and more specific pricing by requesting a Price & Availability Quote.
                   -->
                </p>
                  
<?php

 
        $count = count($invehid);
		for($i=0;$i<$count;$i++)
        {
            echo '<p style="font-size: 18px; margin-bottom: 0px;">';
            echo $inyear.' '.$inname.' '.$inmodel.' '.$instyle[$i];
			//echo $inyear.' '.$inmodel.' '.$instyle[$i];
            echo '<p style="font-size: 14px; margin-top:0px;">';
            echo $inorigin.' '.$invehtype[$i];
            echo '</p>';
?>
                <br />
                <center>
<?php
    //echo '<a href="details.php?vehid='.$invehid[$i].'" target="_blank">';
    if(!file_exists($invimage[$i])) $imagefile = '';
    else $imagefile = $invimage[$i];

    if($imagefile)
    {
        $internal_images=array();
         $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
      
            mysql_select_db(DB_SERVER_DATABASE, $con);
      $query1 = "select * from vehicleinternalimages where vehilce_id = ".$invehid[$i];
     
            $result1 = mysql_query($query1);         
            $internal_images[]=$imagefile;
            if($result1)
            {
                while($row1 = mysql_fetch_array($result1)){
                    $internal_images[]=$row1['image'];
                }
              
            }
           
        $max_width = 400;
        $max_height = 400;
       // echo '<img id="vehimage" class="img-responsive" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="1" hspace="10" vspace="10" />';
        
        
        if(mysql_num_rows($result1)){
            
            //echo'<pre>';print_r($internal_images);echo'</pre>';?>
                    <script>
                         jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $Loop: false,                                   //[Optional] Enable loop(circular) of carousel or not, default value is true
                    $SpacingX: 3,                                  //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 3,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 6,                              //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 204,                          //[Optional] The offset position to park thumbnail,

                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 6                                       //[Optional] Steps to go for each navigation request, default value is 1
                    }
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container_<?php echo $i;?>", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$SetScaleWidth(Math.min(parentWidth, 720));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
            //responsive code end
        });
                        </script>
                    <div id="slider1_container_<?php echo $i;?>" style="position: relative; width: 720px;
                         height: 480px; overflow: hidden;" class="slider1_container"> 
              
              <!-- Loading Screen -->
              <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;"> </div>
                <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;"> </div>
              </div>
              
              <!-- Slides Container -->
              <div u="slides" style="cursor: move; position: absolute; left: 0px; width: 720px; height: 480px;
            overflow: hidden;">
                <?php
			
			
			foreach($internal_images as $vals)
			{
			?>
                <div> <img u="image" src="loadimage.php?image=<?php echo $vals; ?>" /> <img u="thumb" src="loadimage.php?image=<?php echo $vals; ?>" />
                  <?php 
                $vowels = array(".jpg",".png",".gif");
               $onlyconsonants = str_replace($vowels, "", $vals);
				?>
                  <p class="caption">
                    <?php if(!empty($vals)) { echo ""; }else{ } ?>
                  </p>
                </div>
                <?php } ?>
              </div>
              
              <!-- Thumbnail Navigator Skin Begin -->
              <div u="thumbnavigator" class="jssort07" style="position: absolute; width: 720px; height: 100px; left: 0px; bottom: 0px; overflow: hidden; ">
                <div style=" background-color: #000; filter:alpha(opacity=30); opacity:.3; width: 100%; height:100%;"></div>
                <!-- Thumbnail Item Skin Begin -->
                <style>
                /* jssor slider thumbnail navigator skin 07 css */
                /*
                .jssort07 .p            (normal)
                .jssort07 .p:hover      (normal mouseover)
                .jssort07 .pav          (active)
                .jssort07 .pav:hover    (active mouseover)
                .jssort07 .pdn          (mousedown)
                */
                .jssort07 .i {
                    position: absolute;
                    top: 0px;
                    left: 0px;
                    width: 99px;
                    height: 66px;
                    filter: alpha(opacity=80);
                    opacity: .8;
                }

                .jssort07 .p:hover .i, .jssort07 .pav .i {
                    filter: alpha(opacity=100);
                    opacity: 1;
                }

                .jssort07 .o {
                    position: absolute;
                    top: 0px;
                    left: 0px;
                    width: 97px;
                    height: 64px;
                    border: 1px solid #000;
                    transition: border-color .6s;
                    -moz-transition: border-color .6s;
                    -webkit-transition: border-color .6s;
                    -o-transition: border-color .6s;
                }

                * html .jssort07 .o {
                    /* ie quirks mode adjust */
                    width /**/: 99px;
                    height /**/: 66px;
                }

                .jssort07 .pav .o, .jssort07 .p:hover .o {
                    border-color: #fff;
                }

                .jssort07 .pav:hover .o {
                    border-color: #0099FF;
                }

                .jssort07 .p:hover .o {
                    transition: none;
                    -moz-transition: none;
                    -webkit-transition: none;
                    -o-transition: none;
                }
            </style>
                <div u="slides" style="cursor: move;">
                  <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 99px; HEIGHT: 66px; TOP: 0; LEFT: 0;">
                    <thumbnailtemplate class="i" style="position:absolute;"></thumbnailtemplate>
                    <div class="o"> </div>
                  </div>
                </div>
                <!-- Thumbnail Item Skin End --> 
                <!-- Arrow Navigator Skin Begin -->
                <style>
                    /* jssor slider arrow navigator skin 11 css */
                    /*
                .jssora11l              (normal)
                .jssora11r              (normal)
                .jssora11l:hover        (normal mouseover)
                .jssora11r:hover        (normal mouseover)
                .jssora11ldn            (mousedown)
                .jssora11rdn            (mousedown)
                */
                    .jssora11l, .jssora11r, .jssora11ldn, .jssora11rdn {
                        position: absolute;
                        cursor: pointer;
                        display: block;
                        background: url(..images/a11.png) no-repeat;
                        overflow: hidden;
                    }

                    .jssora11l {
                        background-position: -11px -41px;
                    }

                    .jssora11r {
                        background-position: -71px -41px;
                    }

                    .jssora11l:hover {
                        background-position: -131px -41px;
                    }

                    .jssora11r:hover {
                        background-position: -191px -41px;
                    }

                    .jssora11ldn {
                        background-position: -251px -41px;
                    }

                    .jssora11rdn {
                        background-position: -311px -41px;
                    }
					.navbar {display:none;}
					
            </style>
                <!-- Arrow Left --> 
                <span u="arrowleft" class="jssora11l" style="width: 37px; height: 37px; top: 123px; left: 8px;"> </span> 
                <!-- Arrow Right --> 
                <span u="arrowright" class="jssora11r" style="width: 37px; height: 37px; top: 123px; right: 8px"> </span> 
                <!-- Arrow Navigator Skin End --> 
              </div>
              <!-- ThumbnailNavigator Skin End --> 
              
              <!-- Trigger --> 
            </div>
            
       <?php }else{
           echo '<img id="vehimage" class="img-responsive" src="loadimage.php?image='.$imagefile.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="1" hspace="10" vspace="10" />';
       }
    }
    else echo '** Image currently not available **';
    //echo '<center>File: '.$imagefile.'</center>';
    //echo '</a>';
?>
                   <br /> 
                         EXAMPLE PICTURES
                </center>
                
               
                <br />
                <table class="table borderless graph_image_table graph_image" bordercolor="#142c3c" border="1" style="width:100%">
                    <tr>
                        <td width="50%" align="center" bgcolor="#142c3c" style="color:#FFF; font-size:13px;">
                            <b>AVERAGE MILEAGE</b>
                        </td>
                        <td width="50%" align="center" bgcolor="#142c3c" style="color:#FFF; font-size:13px;">
                            <b>LOW MILEAGE</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" bgcolor="#142c3c">
                            <table  border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                <!-- mileage -->
                                <tr valign="middle" style="font-size:13px; color:#142c3c;">
                                    <td width="80" height="20" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($inhighmileend[$i]); ?>
                                    </td>
                                    <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($inhighmilestart[$i]+(($inhighmileend[$i]-$inhighmilestart[$i])/2)); ?>
                                    </td>
                                    <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($inhighmilestart[$i]); ?>
                                    </td>
                                    <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($inlowmilestart[$i]+(($inhighmilestart[$i]-$inlowmilestart[$i])/2)); ?>
                                    </td>
                                    <td width="80" colspan="3" align="center" bgcolor="#ebebeb" class="bodybold">
<?php echo number_format($inlowmilestart[$i]); ?>
                                    </td>
                                </tr><!-- end mileage -->
                                <!-- top line of hash marks -->
                                <tr>
                                    <td height="3" width="40" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="38" bgcolor="#ebebeb" align="center"></td>
                                </tr><!-- end top line of hash marks -->
                                <!-- middle black bar -->
                                <tr>
                                    <td bgcolor="#ebebeb" height="2"></td>
                                    <td colspan="13" height="2"></td>
                                    <td bgcolor="#ebebeb" height="2"></td>
                                </tr><!-- end middle black bar -->
                                <!-- bottom line of hash marks -->
                                <tr>
                                    <td height="3" width="40" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="39" bgcolor="#ebebeb" align="center"></td>
                                    <td width="2" bgcolor="#142c3c" align="center"></td>
                                    <td width="38" bgcolor="#ebebeb" align="center"></td>
                                </tr><!-- end bottom line of hash marks -->
                                <!-- excellent availability section -->
                                <tr valign="bottom">
                                    <td height="20" bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                    <td colspan="6" align="center" bgcolor="#ebebeb">
<?php if($inhighbb[$i] == 1) echo '<span class="excellent">Excellent Availability!</span>'; ?>
                                    </td>
                                    <td></td>
                                    <td colspan="6" align="center" bgcolor="#ebebeb">
<?php if($inlowbb[$i] == 1) echo '<span class="excellent">Excellent Availability!</span>'; ?>
                                    </td>
                                    <td height="20" bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                </tr><!-- end excellent availability section -->
                                <!-- estimates -->
                                <tr style="font-size:13px; color:#142c3c;">
                                    <td bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                    <td class="bodybold" bgcolor="#ebebeb" align="center" colspan="6">
<?php
    if(($inlowpricestart[$i] == $inhighpricestart[$i]) && ($inlowpriceend[$i] == $inhighpriceend[$i]))
    {
        echo '$'.number_format($inhighpricestart[$i]).' to $'.number_format($inhighpricestart[$i] + (($inhighpriceend[$i]-$inhighpricestart[$i])/2));
    }
    else if($inhighpricestart[$i] < 5000) echo 'Below $'.number_format($inhighpriceend[$i]);
    else echo '$'.number_format($inhighpricestart[$i]).' to $'.number_format($inhighpriceend[$i]);
?>
                                    </td>
                                    <td></td>
                                    <td class="bodybold" bgcolor="#ebebeb" align="center" colspan="6">
<?php
    if(($inlowpricestart[$i] == $inhighpricestart[$i]) && ($inlowpriceend[$i] == $inhighpriceend[$i]))
    {
        echo '$'.number_format($inlowpricestart[$i] + (($inlowpriceend[$i]-$inlowpricestart[$i])/2)).' to $'.number_format($inlowpriceend[$i]);
    }
    else if($inlowpricestart[$i] < 5000) echo 'Below $'.number_format($inlowpriceend[$i]);
    else echo '$'.number_format($inlowpricestart[$i]).' to $'.number_format($inlowpriceend[$i]);
?>
                                    </td>
                                    <td height="20" bgcolor="#ebebeb"><img src="common/layout/trans.gif" width="1" height="1"></td>
                                </tr><!-- end estimates -->
                            </table>
                        </td>
                    </tr>
                </table>
                <center>
                <!--<p style="font-size:11px; color:#142c3c; margin-top:2px;" align="center">plus tax, title and license</p>
                <p style="margin-top:20px; color:#142c3c;">Prices continually fall, therefore an actual quote may be lower than the price ranges shown here.</p>
                <br />-->
                <table  border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td >
                            
                            <div style="width:100%">
                                
                           
                           <div style="float: left;" class="btn_cont">
                             <form action=<?php echo '"quote.php?makeitem='.$inmake.'&yearitem='.$inyear.'&modelitem='.$inmodel.'&styleitem='.$instyle[$i].'&QuoteType=Standard"'; ?> method="post">
                               <button type="submit" class="med">REQUEST A PRICE & AVAILABILITY QUOTE</button>
                            </form>
                           </div>
                           </div> 
                        </td>
                       
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr><td colspan="2">
<?php
            if($i < $count-1) echo '<img src="common/layout/short-bar.gif" style="padding: 10px;" border="0" />';
            else echo '&nbsp;';
?>
                    </td></tr>
                    <!--tr><td>&nbsp;</td><td>&nbsp;</td></tr-->
                </table>
                <!--<a id="selected" href="javascript:void(0);">close</a>-->
                <button id="selected" type="button" style="font-size:14px;" class="med" value='<?php echo "1"?>'>BACK TO VEHICLE SPECIFICATION</button>
                </center>
<?php
        } // for loop
  
?>
        </div><!--end grideightgrey -->
    </div><!-- grid eight container -->
    
    <script>
        $( document ).ready(function() {

    $("#selected").bind('click',function() {
        
     jQuery('#cboxClose').click();


    });

});
    </script>
<?php
    $_SESSION['hideteaser1'] = 'true';
    require("teaser.php");
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>

<?php require("footer.php");?>
<?php require("footerend.php"); ?>
