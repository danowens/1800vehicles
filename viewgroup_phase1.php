<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 4;
    $_SESSION['titleadd'] = "All Virtual Vehicles";
    //$_SESSION['ShowError'] = $_SESSION['LastGroup'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        if($_POST['Reason'] == 'ChangeVisibility')
        {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select v.VehicleID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicles set Visible = '.$_POST['statuslist'].' where VehicleID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Visibility on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Visibility on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
        }

        if($_POST['Reason'] == 'ChangeLowMiles')
        {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dl.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set MileageStart = ';
                    if($bumplmiles == 'on') $cvuuquery .= 'MileageStart + ';
                    $cvuuquery .= $_POST['llmiles'].', MileageEnd = ';
                    if($bumplmiles == 'on') $cvuuquery .= 'MileageEnd + ';
                    $cvuuquery .= $_POST['lhmiles'].' where VehicleDataID = '.$cvurow[0];
                    //var_dump($cvuuquery);
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Low Mileage Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Low Mileage Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
        }

        if($_POST['Reason'] == 'ChangeHighMiles')
        {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dh.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set MileageStart = ';
                    if($bumphmiles == 'on') $cvuuquery .= 'MileageStart + ';
                    $cvuuquery .= $_POST['ahlmiles'].', MileageEnd = ';
                    if($bumphmiles == 'on') $cvuuquery .= 'MileageEnd + ';
                    $cvuuquery .= $_POST['ahhmiles'].' where VehicleDataID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Average to High Mileage Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Average to High Mileage Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
        }

        if($_POST['Reason'] == 'ChangeLowPrice')
        {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dl.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set PriceStart = ';
                    if($bumplprice == 'on') $cvuuquery .= 'PriceStart - ';
                    $cvuuquery .= $_POST['llprice'].', PriceEnd = ';
                    if($bumplprice == 'on') $cvuuquery .= 'PriceEnd - ';
                    $cvuuquery .= $_POST['lhprice'].' where VehicleDataID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Low Mile Price Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Low Mile Price Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
        }

        if($_POST['Reason'] == 'ChangeHighPrice')
        {
            $cvquery = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
            $cvquery .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
            $cvquery .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
            $cvresult = mysql_query($cvquery, $con);
            if($cvrow = mysql_fetch_array($cvresult))
            {
                $cvuquery = 'select dh.VehicleDataID';
                $cvuquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
                $cvuquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
                if(!is_null($cvrow[2])) $cvuquery .= ' and v.Visible = '.$cvrow[2];
                if(!is_null($cvrow[3])) $cvuquery .= ' and v.MakeID = '.$cvrow[3];
                if(!is_null($cvrow[4])) $cvuquery .= " and v.Year = '".$cvrow[4]."'";
                if(!is_null($cvrow[5])) $cvuquery .= " and v.Model like '%".$cvrow[5]."%'";
                if(!is_null($cvrow[6])) $cvuquery .= " and v.Style like '%".$cvrow[6]."%'";
                if(!is_null($cvrow[7]) && (strlen($cvrow[7]) > 0)) $cvuquery .= " and v.Type = '".$cvrow[7]."'";
                if(!is_null($cvrow[8])) $cvuquery .= " and v.Doors = '".$cvrow[8]."'";
                if(!is_null($cvrow[9]) && (strlen($cvrow[9]) > 0)) $cvuquery .= " and v.Convertible = '".$cvrow[9]."'";
                if(!is_null($cvrow[10]) && (strlen($cvrow[10]) > 0)) $cvuquery .= " and v.WheelDrive = '".$cvrow[10]."'";
                if(!is_null($cvrow[11]) && (strlen($cvrow[11]) > 0)) $cvuquery .= " and v.BodyType = '".$cvrow[11]."'";
                if(!is_null($cvrow[14])) $cvuquery .= ' and dl.MileageStart > '.$cvrow[14];
                if(!is_null($cvrow[15])) $cvuquery .= ' and dl.MileageEnd < '.$cvrow[15];
                if(!is_null($cvrow[16])) $cvuquery .= ' and dh.MileageStart > '.$cvrow[16];
                if(!is_null($cvrow[17])) $cvuquery .= ' and dh.MileageEnd < '.$cvrow[17];
                if(!is_null($cvrow[18])) $cvuquery .= ' and dl.PriceStart > '.$cvrow[18];
                if(!is_null($cvrow[19])) $cvuquery .= ' and dl.PriceEnd < '.$cvrow[19];
                if(!is_null($cvrow[20])) $cvuquery .= ' and dh.PriceStart > '.$cvrow[20];
                if(!is_null($cvrow[21])) $cvuquery .= ' and dh.PriceEnd < '.$cvrow[21];
                if(!is_null($cvrow[22])) $cvuquery .= ' and (dl.BestBuy = '.$cvrow[22].' or dh.BestBuy = '.$cvrow[22].')';
                $cvuresult = mysql_query($cvuquery, $con);
                $errorfound = 0;
                while($cvurow = mysql_fetch_array($cvuresult))
                {
                    $cvuuquery = 'update vehicledata set PriceStart = ';
                    if($bumphprice == 'on') $cvuuquery .= 'PriceStart - ';
                    $cvuuquery .= $_POST['ahlprice'].', PriceEnd = ';
                    if($bumphprice == 'on') $cvuuquery .= 'PriceEnd - ';
                    $cvuuquery .= $_POST['ahhprice'].' where VehicleDataID = '.$cvurow[0];
                    if(!mysql_query($cvuuquery, $con)) $errorfound++;
                }

                if($errorfound < 1)
                {
                    $_SESSION['ShowError'] = 'Successfully changed the Average to High Mile Price Range on all vehicles in the group.';
                }
                else
                {
                    $_SESSION['ShowError'] = 'Could not change the Average to High Mile Price Range on '.$errorfound.' vehicles in the group.';
                }
                //header('Location: virtualvehiclegroups.php');
            }
        }

        $query = "select GroupName, VehicleGroupID, Visible, MakeID, Year, Model, Style, Type, Doors, Convertible, WheelDrive, BodyType, SlidingDoors,";
        $query .= " BlackBookAvg, LowMileageStart, LowMileageEnd, HighMileageStart, HighMileageEnd, LowPriceStart, LowPriceEnd,";
        $query .= " HighPriceStart, HighPriceEnd, BestBuy, Rating, AddInfo from vehiclegroups where VehicleGroupID = ".$_SESSION['LastGroup'];
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $vquery = 'select v.VehicleID, m.Name,v.Year,v.Model,v.Style,v.Type,v.Doors,v.Convertible,v.WheelDrive,v.BodyType,v.Visible,v.ImageFile,v.BlackBookAvg,';
            $vquery .= 'dl.MileageStart "LowMileStart",dl.MileageEnd "LowMileEnd",dl.PriceStart "LowPriceStart",dl.PriceEnd "LowPriceEnd",dl.BestBuy "LowBestBuy",';
            $vquery .= 'dh.MileageStart "HighMileStart",dh.MileageEnd "HighMileEnd",dh.PriceStart "HighPriceStart",dh.PriceEnd "HighPriceEnd",dh.BestBuy "HighBestBuy"';
            $vquery .= ' from vehicles v,makes m,vehicledata dh,vehicledata dl';
            $vquery .= ' where m.MakeID=v.MakeID and dl.lowmiles = 1 and dl.VehicleID = v.VehicleID and dh.lowmiles = 0 and dh.VehicleID = v.VehicleID';
            if(!is_null($row[2])) $vquery .= ' and v.Visible = '.$row[2];
            if(!is_null($row[3])) $vquery .= ' and v.MakeID = '.$row[3];
            if(!is_null($row[4])) $vquery .= " and v.Year = '".$row[4]."'";
            if(!is_null($row[5])) $vquery .= " and v.Model like '%".$row[5]."%'";
            if(!is_null($row[6])) $vquery .= " and v.Style like '%".$row[6]."%'";
            if(!is_null($row[7]) && (strlen($row[7]) > 0)) $vquery .= " and v.Type = '".$row[7]."'";
            if(!is_null($row[8])) $vquery .= " and v.Doors = '".$row[8]."'";
            if(!is_null($row[9]) && (strlen($row[9]) > 0)) $vquery .= " and v.Convertible = '".$row[9]."'";
            if(!is_null($row[10]) && (strlen($row[10]) > 0)) $vquery .= " and v.WheelDrive = '".$row[10]."'";
            if(!is_null($row[11]) && (strlen($row[11]) > 0)) $vquery .= " and v.BodyType = '".$row[11]."'";
            if(!is_null($row[14])) $vquery .= ' and dl.MileageStart > '.$row[14];
            if(!is_null($row[15])) $vquery .= ' and dl.MileageEnd < '.$row[15];
            if(!is_null($row[16])) $vquery .= ' and dh.MileageStart > '.$row[16];
            if(!is_null($row[17])) $vquery .= ' and dh.MileageEnd < '.$row[17];
            if(!is_null($row[18])) $vquery .= ' and dl.PriceStart > '.$row[18];
            if(!is_null($row[19])) $vquery .= ' and dl.PriceEnd < '.$row[19];
            if(!is_null($row[20])) $vquery .= ' and dh.PriceStart > '.$row[20];
            if(!is_null($row[21])) $vquery .= ' and dh.PriceEnd < '.$row[21];
            if(!is_null($row[22])) $vquery .= ' and (dl.BestBuy = '.$row[22].' or dh.BestBuy = '.$row[22].')';
            //var_dump($vquery);
            $vresult = mysql_query($vquery, $con);
            $index = 0;
            while($vrow = mysql_fetch_array($vresult))
            {
                $vehid[$index] = $vrow[0];
                $vehmake[$index] = $vrow[1];
                $vehyear[$index] = $vrow[2];
                $vehmodel[$index] = $vrow[3];
                $vehstyle[$index] = $vrow[4];
                $vehtype[$index] = $vrow[5];
                $vehdoors[$index] = $vrow[6];
                $vehconv[$index] = $vrow[7];
                $vehwheel[$index] = $vrow[8];
                $vehbody[$index] = $vrow[9];
                $vehvisible[$index] = $vrow[10];
                $vehimage[$index] = $vrow[11];
                $vehbb[$index] = $vrow[12];
                $vehlms[$index] = $vrow[13];
                $vehlme[$index] = $vrow[14];
                $vehlps[$index] = $vrow[15];
                $vehlpe[$index] = $vrow[16];
                $vehlbb[$index] = $vrow[17];
                $vehhms[$index] = $vrow[18];
                $vehhme[$index] = $vrow[19];
                $vehhps[$index] = $vrow[20];
                $vehhpe[$index] = $vrow[21];
                $vehhbb[$index] = $vrow[22];
                $index++;
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script language="javascript" type="text/javascript">
<!--
    function numbersonly(e)
    {
        var keynum;
        var keychar;
        var numcheck;

        if(window.event) // IE
        {
            keynum = e.keyCode;
        }
        else if(e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which;
        }
        // Tab, Backspace, Delete, Arrows and Home/End are ok...
        if((keynum == 9)||(keynum == 8)||(keynum == 46)||(keynum == 35)||(keynum == 36)||(keynum == 37)||(keynum == 39))
        {
            return true;
        }
        keychar = String.fromCharCode(keynum);
        numcheck = new RegExp("[0-9]");
        return numcheck.test(keychar);
    }

//-->
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>
<div id="content">
    <div class="grideightcontainer" style="width: 1000px;">
        <h1 class="subhead" style="width:275px;">Virtual Vehicle Groups</h1>
        <div class="grideightgrey" style="width: 1000px;">
            <div class="grideight" style="margin-top: 0px;width: 1000px;">
                <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">&lt;&lt;&lt;Go to MyDashboard</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="virtualvehiclegroups.php">&lt;&lt;&lt;Go Back to Group Setups</a></p>
                <br/>
                <form action="allvehicleedit.php" method="post">
                    <input type="hidden" name="AddEditType" value="AddNew" />
                    <button type="submit" value="" class="med">Add New Vehicle</button>
                </form>
                <br/>
<?php
    $count = count($vehid);
    echo 'Number of vehicles in group: '.$count;
?>
                <br/>
                <h4 class="subhead">Vehicle Group Options</h4>
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="ChangeVisibility" />
                    <table border="0" width="600" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td width="150">Visibility</td>
                            <td width="450"><select style="width: 400px;" name="statuslist" id="statuslist">
                                <option value="1">Live</option>
                                <option value="0">Not Live</option>
                            </select></td>
                        </tr>
                    </table>
                    <button type="submit" value="" class="med">Set Visibility for the Group</button>
                </form>
                <br/>
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="ChangeLowMiles" />
                    <table border="0" width="600" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td width="150">Low Miles</td>
                            <td width="450"><select style="width: 100px;" name="llmiles" id="llmiles">
<?php
    $i = 0;
    while($i <= 100000)
    {
        echo '<option value="'.$i.'">'.$i.'</option>';
        $i += 1000;
    }
?>
                            </select> &nbsp;to&nbsp; <select style="width: 100px;" name="lhmiles" id="lhmiles">
<?php
    $i = 0;
    while($i <= 100000)
    {
        echo '<option value="'.$i.'">'.$i.'</option>';
        $i += 1000;
    }
?>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="bumplmiles" id="bumplmiles" />Bump Up rather than Set</td>
                        </tr>
                    </table>
                    <button type="submit" value="" class="med">Set Low Mileage Range for the Group</button>
                </form>
                <br/>
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="ChangeHighMiles" />
                    <table border="0" width="600" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td width="150">Average to High Miles</td>
                            <td width="450"><select style="width: 100px;" name="ahlmiles" id="ahlmiles">
<?php
    $i = 0;
    while($i <= 100000)
    {
        echo '<option value="'.$i.'">'.$i.'</option>';
        $i += 1000;
    }
?>
                            </select> &nbsp;to&nbsp; <select style="width: 100px;" name="ahhmiles" id="ahhmiles">
<?php
    $i = 0;
    while($i <= 100000)
    {
        echo '<option value="'.$i.'">'.$i.'</option>';
        $i += 1000;
    }
?>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="bumphmiles" id="bumphmiles" />Bump Up rather than Set</td>
                        </tr>
                    </table>
                    <button type="submit" value="" class="med">Set Average to High Mileage Range for the Group</button>
                </form>
                <br/>
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="ChangeLowPrice" />
                    <table border="0" width="650" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td width="150">Low Mile Price</td>
                            <td width="500">
                                $<input type="text" style="width: 100px;" name="llprice" id="llprice" maxlength="7" onkeypress="javascript:return numbersonly(event);"/>
                                &nbsp;to&nbsp;
                                $<input type="text" style="width: 100px;" name="lhprice" id="lhprice" maxlength="7" onkeypress="javascript:return numbersonly(event);"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="bumplprice" id="bumplprice" />Bump Down rather than Set
                            </td>
                        </tr>
                    </table>
                    <button type="submit" value="" class="med">Set Low Mile Price Range for the Group</button>
                </form>
                <br/>
                <form action="viewgroup.php" method="post">
                    <input type="hidden" name="Reason" value="ChangeHighPrice" />
                    <table border="0" width="650" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td width="150">Average to High Mile Price</td>
                            <td width="500">
                                $<input type="text" style="width: 100px;" name="ahlprice" id="ahlprice" maxlength="7" onkeypress="javascript:return numbersonly(event);"/>
                                &nbsp;to&nbsp;
                                $<input type="text" style="width: 100px;" name="ahhprice" id="ahhprice" maxlength="7" onkeypress="javascript:return numbersonly(event);"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="bumphprice" id="bumphprice" />Bump Down rather than Set
                            </td>
                        </tr>
                    </table>
                    <button type="submit" value="" class="med">Set Average to High Mile Price Range for the Group</button>
                </form>
                <br/>
                <h4 class="subhead">Vehicles In Group</h4>
                <table border="0" width="900" cellspacing="10" style="margin-left:-10px;">
                    <thead style="color: green;">
                        <tr valign="baseline">
                            <th align="left" width="75">Status</th>
                            <th align="left" width="50">Type</th>
                            <th align="left" width="275">Vehicle</th>
                            <th align="left" width="150">Details</th>
                            <th align="left" width="150">Mileage</th>
                            <th align="left" width="150">Price</th>
                            <th align="left" width="100">Exc. Avail.</th>
                        </tr>
                    </thead>
                    <tbody style="color: blue;">
<?php
    $index = 0;
    while($index < $count)
    {
        echo '<tr valign="baseline">';
        echo '<td>';
        if($vehvisible[$index] == 1) echo 'Live';
        else echo 'Not Live';
        echo '</td>';
        echo '<td>'.$vehtype[$index].'</td>';
        echo '<td>'.$vehyear[$index].' '.$vehmake[$index].' '.$vehmodel[$index].' '.$vehstyle[$index].'</td>';
        echo '<td>Doors: '.$vehdoors[$index].'<br/>Convertible: '.$vehconv[$index].'<br/>Wheel Drive: '.$vehwheel[$index].'<br/>Body Type: '.$vehbody[$index].'<br/>Black Book: '.$vehbb[$index].'</td>';
        echo '<td>';
        $min = $vehlms[$index]/1000;
        $max = $vehlme[$index]/1000;
        echo 'Low: '.$min.' - '.$max.'k<br/>';
        $min = $vehhms[$index]/1000;
        $max = $vehhme[$index]/1000;
        echo 'Avg: '.$min.' - '.$max.'k';
        echo '</td>';
        echo '<td>';
        $min = $vehlps[$index]/1000;
        $max = $vehlpe[$index]/1000;
        echo 'Low: $'.$min.' - '.$max.'k';
        echo '<br/>';
        $min = $vehhps[$index]/1000;
        $max = $vehhpe[$index]/1000;
        echo 'Avg: $'.$min.' - '.$max.'k';
        echo '</td>';
        echo '<td>';
        if($vehlbb[$index] == 1) echo 'Exc Avail';
        else echo 'No';
        echo '<br/>';
        if($vehhbb[$index] == 1) echo 'Exc Avail';
        else echo 'No';
        echo '</td>';
        echo '</tr>';
        $index++;
    }
?>
                    </tbody>
                </table>
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
