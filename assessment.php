<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 11;
    $_SESSION['titleadd'] = 'Best Vehicle Assessment';
    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];    
    unset($_SESSION['assessment_post']);
    unset($_SESSION['pos_post']);
    unset($_SESSION['searchplan_post']);
    unset($_SESSION['consult_post']);    unset($_SESSION['request_trade_in_quote']);	$_SESSION['submit_btn'] = "SUBMIT COMPLETE YOUR ASSESSMENT";	
    $errorinload = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if( $con ) {
        mysql_select_db(DB_SERVER_DATABASE, $con);
        // Get the current User Assessment Information (if any)...
        $query = 'select VehicleType, BodyType, VehicleSize, DriveTrain, Notes, Origin, Transmission, MileageFrom, MileageTo, MileageCeiling, ExtLike, ExtDislike, IntLike, IntDisike, BudgetFrom, BudgetTo, BorrowMaxPayment, BorrowDownPayment, VehicleNeed, BrandLike, BrandDislike, Model, VehicleAge, RateFuelEfficiency, RateMintenanceCost, RateReliability, RateLuxury, RateSporty, RateSafety, FrontSType, BedType, Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, RearWindow, BedLiner, EntertainmentSystem, ThirdRD, CRow, PRHatch, BackupCamera, TPackage, OtherPrefereces, OtherReallyHave, AdditionalInfo from assessments where MarketNeedID = '.$marketneedid;        $result = mysql_query($query, $con);
        if( $result && $row = mysql_fetch_array( $result ) ) {
            echo"<script>window.location='edit_assessment.php';</script>";
			$vehicletype			= $row['VehicleType'];
			$bodytype				= $row['BodyType'];
			$vehiclesize			= $row['VehicleSize'];
			$drivetrain				= $row['DriveTrain'];
			$notes					= $row['Notes'];
			$origin					= $row['Origin'];
			$transmission			= $row['Transmission'];
			$mileagefrom			= $row['MileageFrom'];
			$mileageto				= $row['MileageTo'];
			$mileageceiling			= $row['MileageCeiling'];
			$extlike				= $row['ExtLike'];
			$extdislike				= $row['ExtDislike'];
			$intlike				= $row['IntLike'];
			$intdislike				= $row['IntDisike'];
			$budgetfrom				= $row['BudgetFrom'];
			$budgetto				= $row['BudgetTo'];
			$borrowmaxpayment		= $row['BorrowMaxPayment'];
			$borrowdownpayment		= $row['BorrowDownPayment'];
			$vehicleneed			= $row['VehicleNeed'];
			$brandlike				= $row['BrandLike'];
			$branddislike			= $row['BrandDislike'];
			$model					= $row['Model'];
			$vehicleage				= $row['VehicleAge'];
			$ratefuelefficiency		= $row['RateFuelEfficiency'];
			$ratemaintenancecost	= $row['RateMintenanceCost'];
			$ratereliability		= $row['RateReliability'];
			$rateluxury				= $row['RateLuxury'];
			$ratesporty				= $row['RateSporty'];
			$ratesafety				= $row['RateSafety'];
			
			//What do you prefer start
			$frontstype				= $row['FrontSType'];
			$bedtype				= $row['BedType'];
			$leather				= $row['Leather'];
			$heatedseat				= $row['HeatedSeat'];
			$navigation				= $row['Navigation'];
			$sunroof				= $row['SunRoof'];
			$alloywheels			= $row['AlloyWheels'];
			$rearwindow				= $row['RearWindow'];
			$bedliner				= $row['BedLiner'];
			$entertainmentsystem	= $row['EntertainmentSystem'];
			$thirdrs				= $row['ThirdRD'];
			$crow					= $row['CRow'];
			$prhatch				= $row['PRHatch'];
			$backupcamera			= $row['BackupCamera'];
			$tpackage				= $row['TPackage'];
			//What do you prefer end
			$otherprefereces		= $row['OtherPrefereces'];
			$otherreallyhave		= $row['OtherReallyHave'];
			$additionalinfo			= $row['AdditionalInfo'];
        } else {
			$errorinload = 'Could not retrieve Assessment Information';
			//set default value
			$vehicletype			= 'Auto';
			$bodytype				= 'Sedan';
			$vehiclesize			= 'Small';
			$drivetrain				= 'Front Wheel Drive';
			$notes					= '';
			$origin					= 'Domestic';
			$transmission			= 'Automatic';
			$mileagefrom			= '40000';
			$mileageto				= '50000';
			$mileageceiling			= '60000';
			$extlike				= '';
			$extdislike				= '';
			$intlike				= '';
			$intdislike				= '';
			$budgetfrom				= '30000';
			$budgetto				= '40000';
			$borrowmaxpayment		= 'Not Applicable';
			$borrowdownpayment		= 'Not Applicable';
			$vehicleneed			= 'Now';
			$brandlike				= '';
			$branddislike			= '';
			$model					= '';
			$vehicleage				= '1-2 years old';
			$ratefuelefficiency		= 'Low';
			$ratemaintenancecost	= 'Low';
			$ratereliability		= 'Low';
			$rateluxury				= 'Low';
			$ratesporty				= 'Low';
			$ratesafety				= 'Low';
			
			//What do you prefer start
			$frontstype				= 'Standard Bench';
			$bedtype				= 'Short Bed (about 6 ft.)';
			$leather				= 'Yes';
			$heatedseat				= 'Yes';
			$navigation				= 'Yes';
			$sunroof				= 'Yes';
			$alloywheels			= 'Yes';
			$rearwindow				= 'Yes';
			$bedliner				= 'Yes';
			$entertainmentsystem	= 'Yes';
			$thirdrs				= 'Yes';
			$crow					= 'Yes';
			$prhatch				= 'Yes';
			$backupcamera			= 'Yes';
			$tpackage				= 'Yes';
			//What do you prefer end
			$otherprefereces		= '';
			$otherreallyhave		= '';
			$additionalinfo			= '';
		}		
        mysql_close($con);
    } else {		$errorinload = 'Could not retrieve Assessment Information';	}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php //require("headerend.php"); ?>
<script type="text/javascript">
    $(document).ready(function () {
		doyouprefer('<?php echo $vehicletype;?>');
		$("#form_assessment input:text").click( function() {
			var element = $(this);
			$("#notetext").val(element.val());
			$("#dialog").dialog({
				create: function(e, ui) {
					var widget = $(this).dialog("widget");
					$(".ui-dialog-titlebar-close", widget).css("width","45px").attr("title","Save");					$(".ui-dialog-titlebar-close span", widget).removeClass("ui-icon-closethick").addClass("ui-icon-myCloseButton");					$(".ui-dialog-titlebar-close span.ui-button-icon-primary", widget).removeClass("ui-icon"); 					$(".ui-dialog-titlebar-close span.ui-button-text", widget).html("Save");
				},                modal: true,
                title: " ",
                dialogClass: 'no-close',
                close: function(event, ui) {
                    element.val($("#notetext").val());
                    $("#dialog").dialog("destroy");
                }
            });
		});				$("#showrequirfield").click(function (){
			$('#notshowfields').toggle();
		});
	});
	function vehiclechanged(val){
		if( val=='Auto' ){
			$("#bodytype").html('<option value="Sedan" selected="selected">Sedan</option><option value="Coupe">Coupe</option><option value="Hatchback">Hatchback</option><option value="Convertible">Convertible</option>');			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
			//Do you prefer section
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").hide();
			$("#trtpackage").hide();			
		} else if( val=='Minivan' ) {
			$("#bodytype").html('<option value="Standard" selected="selected">Standard</option>');			$("#vehiclesize").html('<option value="Standard" selected="selected">Standard</option>');			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');		
			//Do you prefer section
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").hide();
		} else if( val=='SUV' ) {
			$("#bodytype").html('<option value="2 door" selected="selected">2 door</option><option value="4 door">4 door</option>');			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');		
			//Do you prefer section
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").show();
			$("#trcrow").show();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").show();			
		} else if(val=='Pickup') {
			$("#bodytype").html('<option value="4 door" selected="selected">4 door</option><option value="Ext Cab">Ext Cab</option><option value="Single Cab">Single Cab</option>');			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
			//Do you prefer section
			$("#trfrontstype").show();
			$("#trbedtype").show();
			$("#trrearwindow").show();
			$("#trbedliner").show();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").show();
			$("#trtpackage").show();
		}
	}
	
    function bodychanged(val){
		var vehicle_type = $('#vehicletype').val();
		console.log("Vehicle type: "+ vehicle_type);
		if(val=='Sedan' || val=='Coupe' || val=='Hatchback'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
			
		}else if(val=='Convertible'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='Standard'){
			$("#vehiclesize").html('<option value="Standard" selected="selected">Standard</option>');
			
			$("#drivetrain").html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='2 door'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='4 door' && vehicle_type =='SUV' ){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Mid Size">Mid Size</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		
		}else if(val=='4 door' && vehicle_type =='Pickup' ){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
			
		}else if(val=='Ext Cab' || val=='Single Cab'){
			$("#vehiclesize").html('<option value="Small" selected="selected">Small</option><option value="Full Size">Full Size</option><option value="Flexible">Flexible</option>');
			
			$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
		}
	}
	
	function doyouprefer(val){
		if(val=='Auto'){
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").hide();
			$("#trtpackage").hide();
			
		}else if(val=='Minivan'){
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").hide();

		}else if(val=='SUV'){
			$("#trfrontstype").hide();
			$("#trbedtype").hide();
			$("#trrearwindow").hide();
			$("#trbedliner").hide();
			$("#trentertainmentsystem").show();
			$("#trthirdrs").show();
			$("#trcrow").show();
			$("#trprhatch").show();
			$("#trbackupcamera").show();
			$("#trtpackage").show();

		}else if(val=='Pickup'){
			$("#trfrontstype").show();
			$("#trbedtype").show();
			$("#trrearwindow").show();
			$("#trbedliner").show();
			$("#trentertainmentsystem").hide();
			$("#trthirdrs").hide();
			$("#trcrow").hide();
			$("#trprhatch").hide();
			$("#trbackupcamera").show();
			$("#trtpackage").show();
		}
	}
	
	function validateFormOnSubmit(){
        var reason = "";
        if(reason != "")
        {
            vbudget.focus();
            alert("Some fields need correction:"+'\n' + reason + '\n');
            return false;
        }
        return true;
    }
</script>
<style>
    .ui-icon-myCloseButton {padding: 0!important;text-indent:0!important;}
    .grideightcontainer table a {
    color: #ffffff;
    font-weight: normal;
}
   .tdactive {
        background-color: #92d050;
    }
	#content {margin:0 auto;}
	.assessment-vehicle {padding-left:0px;float: left;}
	.assess-title-video {padding-left: 15%;}
	.assess-title-all { margin-top: 2%; float:right;}
	.assessment-vehicle > h3 { color: #216ece; font-size: 18px; margin: 5px; padding-left: 10px; letter-spacing:0; text-transform:none;}
	.assess-title-video img { width: 20%;}
	.assess-title-video > span {padding-left: 5px; color: #216ece;}
	.assess-link a{color:#46a25a;}
	h1.subhead{margin:0;}
</style>



<script type="text/javascript">
$(document).ready(function(){
 $("#youtube1").click(function(){
                $.colorbox({href:"dashboard_video.php",scrolling:false,width: "90%"});
            });
            
            $("#step_2_video").click(function(){
                $.colorbox({href:"step_2_video.php",scrolling:false,width: "90%"});
            });
});
</script>
<div class="gridtwelve"></div>
<div id="content">
	<div class="grideightcontainer">            
        <?php 
        if (!isset($_SESSION['user'])) { ?>
           <div class="col-xs-12 col-sm-12 dashboard-title">
                    <h3>My Dashboard</h3>
            </div> 
        <div style="width: 100%">              
            <table class="table table-bordered secondmenu-table">
                <thead>
                    <tr>
                        <td align="center" class="tdactive"><a href="dashboard.php"><span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">1</span>
                        </span>GET HELP WITH WHAT TO BUY</a></td>                                              
                        <td align="center"><a href="page-unavailable.php"><span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">2</span>
                        </span>CONSIDER & APPROVE</a></td>
                        <td align="center"><a href="page-unavailable2.php"><span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">3</span>
                        </span>INSPECT & TAKE DELIVERY</a></td>
                    </tr>
                </thead>   
            </table>
        </div>  
            <div class="col-xs-12 col-sm-12 dash-step2 assess-dash" style="margin-bottom: 5px;"> 
                  
            <!--<div class="col-xs-4 col-sm-4">
                
            </div> -->
            
            <div class="col-xs-6 col-sm-6 assess-title-all">
                <div class="assess-title-video" style="float:left;">
                     <span class="helpful-tools"> Other Helpful Links: </span> <br>
                     <a href="#" id="step_2_video"><img src="images/play.png"/> About Step 2</a>
                    </div>
                   
                    <div class="assess-link" style="float:right;">
                   <!-- <a title="Newsweek"  href="researchvehicles.php">
                       Research Prices & Availability>
                    </a><br>-->
                    <a   href="consult.php">
                     Request Consultation>
                    </a><br>
                     <a   href="#">
                    24/7 Customer support>
                    </a><br>
                    </div>
            </div>
                
        </div>  
            
       <?php } ?>
            
          <!--  <div class="dashboardmenu">
         
            <div class="row placeholders">     
                
                 <div class="col-xs-6 col-md-4 placeholder">
                    <a href="consult.php"> 
                        <img id="dashboardimage" class="img-responsive"  alt="" src="images/request.png">
                    </a>
                </div> 
                
                 <div class="col-xs-6 col-md-4 placeholder">
                    <a title="Newsweek"  href="researchvehicles.php">
                        <img id="dashboardimage" class="img-responsive"  alt="" src="images/research.png">
                     </a>
                </div>
               
                <div class="col-xs-6 col-md-4 placeholder">
                    <a href="#">  
                      <img id="dashboardimage" class="img-responsive"  alt="" src="images/support.png">
                    </a>
                </div> 
            </div>
        </div>-->
         <div class="col-xs-6 col-sm-6 assessment-vehicle">
             <h1 class="subhead" style="  text-align: left;width:65%;margin-left: 0;">            
            DEFINE YOUR VEHICLE NEEDS
            </h1>
             <h3 class="helpful-tools">Vehicle Needs Assessment</h3>
                <div class="breaker"></div>
               
            </div> 
        
		<form action="assessmentsave.php" onsubmit="javascript:return validateFormOnSubmit()" method="post" name="assessform" id="form_assessment">
            <div class="grideightgrey">
               <?php /* <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;">* All fields required except Additional Information</p> */?>
                <div class="grideight" style="width: 97%; margin-top:-5px;">
                 <!--  <strong style="font-size:13px;text-transform: uppercase;">Please choose your preferences</strong>-->
                    <table class="table">
						<tr>
							<td style="width:30%"><strong>Vehicle Type</strong></td>
							<td style="width:40%">
								<select name="vehicletype" id="vehicletype" onchange="javascript:vehiclechanged(this.value)">
									<option value="Auto"<?php if($vehicletype=='Auto'){?> selected="selected" <?php }?>>Auto</option>
									<option value="Minivan"<?php if($vehicletype=='Minivan'){?> selected="selected" <?php }?>>Minivan</option>
									<option value="SUV"<?php if($vehicletype=='SUV'){?> selected="selected" <?php }?>>SUV</option>
									<option value="Pickup"<?php if($vehicletype=='Pickup'){?> selected="selected" <?php }?>>Pickup</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Body Type</strong></td>
							<td align="left" >
								<select name="bodytype" id="bodytype" onchange="javascript:bodychanged(this.value)">
									<?php if($vehicletype=='Auto'){?>
										<option value="Sedan"<?php if($bodytype=='Sedan'){?> selected="selected" <?php }?>>Sedan</option>
										<option value="Coupe"<?php if($bodytype=='Coupe'){?> selected="selected" <?php }?>>Coupe</option>
										<option value="Hatchback"<?php if($bodytype=='Hatchback'){?> selected="selected" <?php }?>>Hatchback</option>
										<option value="Convertible"<?php if($bodytype=='Convertible'){?> selected="selected" <?php }?>>Convertible</option>
									<?php }elseif($vehicletype=='Minivan'){?>
										<option value="Standard"<?php if($bodytype=='Standard'){?> selected="selected" <?php }?>>Standard</option>
									<?php }elseif($vehicletype=='SUV'){?>
										<option value="2 door"<?php if($bodytype=='2 door'){?> selected="selected" <?php }?>>2 door</option>
										<option value="4 door"<?php if($bodytype=='4 door'){?> selected="selected" <?php }?>>4 door</option>
									<?php }elseif($vehicletype=='Pickup'){?>
										<option value="4 door"<?php if($bodytype=='4 door'){?> selected="selected" <?php }?>>4 door</option>
										<option value="Ext Cab"<?php if($bodytype=='Ext Cab'){?> selected="selected" <?php }?>>Ext Cab</option>
										<option value="Single Cab"<?php if($bodytype=='Single Cab'){?> selected="selected" <?php }?>>Single Cab</option>
									<?php }?>
								</select>
							</td> 
						</tr>
						<tr>
							<td><strong>Vehicle Size</strong></td>
							<td align="left" >
								<select name="vehiclesize" id="vehiclesize">
									<?php if($vehicletype=='Auto' && ($bodytype == 'Sedan' || $bodytype == 'Coupe' || $bodytype == 'Hatchback')){?>
										<option value="Small"<?php if($vehiclesize=='Small'){?> selected="selected" <?php }?>>Small</option>
										<option value="Mid Size"<?php if($vehiclesize=='Mid Size'){?> selected="selected" <?php }?>>Mid Size</option>
										<option value="Full Size"<?php if($vehiclesize=='Full Size'){?> selected="selected" <?php }?>>Full Size</option>
										<option value="Flexible"<?php if($vehiclesize=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
									<?php }elseif($vehicletype=='Auto' && $bodytype == 'Convertible'){?>
										<option value="Small"<?php if($vehiclesize=='Small'){?> selected="selected" <?php }?>>Small</option>
										<option value="Full Size"<?php if($vehiclesize=='Full Size'){?> selected="selected" <?php }?>>Full Size</option>
										<option value="Flexible"<?php if($vehiclesize=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>

									<?php }elseif($vehicletype=='Minivan'){?>
										<option value="Standard"<?php if($vehiclesize=='Standard'){?> selected="selected" <?php }?>>Standard</option>
									<?php }elseif($vehicletype=='SUV' && ($bodytype == '2 door' || $bodytype == '4 door')){?>
										<option value="Small"<?php if($vehiclesize=='Small'){?> selected="selected" <?php }?>>Small</option>
										<option value="Mid Size"<?php if($vehiclesize=='Mid Size'){?> selected="selected" <?php }?>>Mid Size</option>
										<option value="Full Size"<?php if($vehiclesize=='Full Size'){?> selected="selected" <?php }?>>Full Size</option>
										<option value="Flexible"<?php if($vehiclesize=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
									<?php }elseif($vehicletype=='Pickup'){?>
										<option value="Small"<?php if($vehiclesize=='Small'){?> selected="selected" <?php }?>>Small</option>
										<option value="Full Size"<?php if($vehiclesize=='Full Size'){?> selected="selected" <?php }?>>Full Size</option>
										<option value="Flexible"<?php if($vehiclesize=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
									<?php }?>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Drive Train</strong></td>
							<td align="left" >
								<select name="drivetrain" id="drivetrain">
								<?php if($vehicletype=='Auto'){?>
									<option value="Front Wheel Drive"<?php if($drivetrain=='Front Wheel Drive'){?> selected="selected" <?php }?>>Front Wheel Drive</option>
									<option value="Rear Wheel Drive"<?php if($drivetrain=='Rear Wheel Drive'){?> selected="selected" <?php }?>>Rear Wheel Drive</option>
									<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
								<?php }elseif($vehicletype=='Minivan'){?>
									<option value="Front Wheel Drive"<?php if($drivetrain=='Front Wheel Drive'){?> selected="selected" <?php }?>>Front Wheel Drive</option>
									<option value="4wd"<?php if($drivetrain=='4wd'){?> selected="selected" <?php }?>>4wd</option>
									<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
								<?php }elseif($vehicletype=='SUV' || $vehicletype == 'Pickup'){?>
									<option value="2wd"<?php if($drivetrain=='2wd'){?> selected="selected" <?php }?>>2wd</option>
									<option value="4wd"<?php if($drivetrain=='4wd'){?> selected="selected" <?php }?>>4wd</option>
									<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
								<?php }?>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Notes</strong></td>
							<td align="left" >
								<input type="text" name="notes" id="notes" value="<?php echo $notes;?>" placeholder="i.e. open to mini van or suv">
								<div id="dialog" title=" " style="display: none;">
									<textarea name="notetext" id="notetext"  style="resize:vertical;width:100%;height:100%"></textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td><strong>Origin</strong></td>
							<td align="left" >
								<select name="origin" id="origin">
									<option value="Domestic"<?php if($origin=='Domestic'){?> selected="selected" <?php }?>>Domestic</option>
									<option value="Import"<?php if($origin=='Import'){?> selected="selected" <?php }?>>Import</option>
									<option value="Any"<?php if($origin=='Any'){?> selected="selected" <?php }?>>Any</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Transmission</strong></td>
							<td align="left" >
								<select name="transmission" id="transmission">
									<option value="Automatic"<?php if($transmission=='Automatic'){?> selected="selected" <?php }?>>Automatic</option>
									<option value="Manual"<?php if($transmission=='Manual'){?> selected="selected" <?php }?>>Manual</option>
									<option value="Flexible"<?php if($transmission=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Mileage you are hoping for</strong></td>
							<td align="left" width="10%">
								<select name="mileagefrom" id="mileagefrom" class="width103">
									<option value="Flexible"<?php if($mileagefrom=='Small'){?> selected="selected" <?php }?>>Flexible</option>
									<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
										<option value="<?php echo $i;?>" <?php if($i==$mileagefrom){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
									<?php }?>
								</select>  TO  
                                                                <select name="mileageto" id="mileageto" class="width103">
									<option value="Flexible"<?php if($mileageto=='Small'){?> selected="selected" <?php }?>>Flexible</option>
									<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
										<option value="<?php echo $i;?>" <?php if($i== $mileageto){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
									<?php }?>
								</select>
							</td>
							
						</tr>
						<tr>
							<td><strong>Mileage Ceiling</strong></td>
							<td align="left" >
								<select name="mileageceiling" id="mileageceiling">
									<option value="Flexible"<?php if($mileageceiling=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
									<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
										<option value="<?php echo $i;?>" <?php if($i==$mileageceiling){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
									<?php }?>
								</select>
							</td>
						</tr>
						<tr><td colspan="2">&nbsp;</td></tr>
						<tr><td><strong>
                                                                Exterior colors you like
                                                        </strong></td>
                                                <td align="left" >
								<input type="text" name="extlike" id="extlike" value="<?php echo $extlike?>" placeholder="Text box">
						</td>
                                                
                                                </tr>

						
						<tr><td ><strong>Interior colors you like</strong></td>
                                                    <td align="left" >
								<input type="text" name="intlike" id="intlike" value="<?php echo $intlike?>" placeholder="Text box">
							</td>
                                                </tr>
						
						
						<tr>
							<td><strong>Budget Range (without tax) </strong></td>
							<td align="left">
                                                            <div style="float: left;">$<select name="budgetfrom" id="budgetfrom" class="width92">
									<option value="Not Sure" <?php if($budgetfrom == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
									<?php for($i=5000; $i <= 100000; $i=$i+500){?>
										<option value="<?php echo $i;?>" <?php if($i==$budgetfrom){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
									<?php }?>
                                                                        
                                                                </select></div>
                                                          <div style="float: left;margin: 4px;">TO</div>
                                                          <div style="float: left;">  $<select name="budgetto" id="budgetto" class="width92">
									<option value="Not Sure" <?php if($budgetto == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
									<?php for($i=5000; $i <= 100000; $i=$i+500){?>
										<option value="<?php echo $i;?>" <?php if($i==$budgetto){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
									<?php }?>
								</select></div>
							</td>
							
						</tr>
                                                
                                                <tr>
							<td><strong>How soon do you need a vehicle?</strong></td>
							<td align="left" >
								<select name="vehicleneed" id="vehicleneed">
									<option value="Now" <?php if($vehicleneed == 'Now') {?> selected="selected" <?php }?>>Now!</option>
									<option value="1-2 weeks" <?php if($vehicleneed == '1-2 weeks') {?> selected="selected" <?php }?>>1-2 weeks</option>
									<option value="3 weeks is ok" <?php if($vehicleneed == '3 weeks is ok') {?> selected="selected" <?php }?>>3 weeks is ok</option>
									<option value="A month or more is ok" <?php if($vehicleneed == 'A month or more is ok') {?> selected="selected" <?php }?>>A month or more is ok</option>
								</select>
							</td>
						</tr>
                                                <tr>
							<td><strong>Age of vehicle you are hoping for</strong></td>
							<td align="left" >
								<select name="vehicleage" id="vehicleage">
									<option value="1-2 years old" <?php if($vehicleage == '1-2 years old') {?> selected="selected" <?php }?>>1-2 years old</option>
									<option value="2-3 years old" <?php if($vehicleage == '2-3 years old') {?> selected="selected" <?php }?>>2-3 years old</option>
									<option value="3-4 years old" <?php if($vehicleage == '3-4 years old') {?> selected="selected" <?php }?>>3-4 years old</option>
									<option value="5-6 years old" <?php if($vehicleage == '5-6 years old') {?> selected="selected" <?php }?>>5-6 years old</option>
									<option value="older than 6 years" <?php if($vehicleage == 'older than 6 years') {?> selected="selected" <?php }?>>older than 6 years</option>
								</select>
							</td>
						</tr>
                                                
                                                
                                                
                                              
						
						
						 <tr><td colspan="2" class="assessment_insidetd_green"><strong class="assessment_inside">Rate your need for:</strong></td></tr>
						<tr>
							<td><strong>Fuel efficiency</strong></td>
							<td align="left" >
								<select name="ratefuelefficiency" id="fuelefficiency">
									<option value="Low" <?php if($ratefuelefficiency == 'Low') {?> selected="selected" <?php }?>>Low</option>
									<option value="Medium" <?php if($ratefuelefficiency == 'Medium') {?> selected="selected" <?php }?>>Medium</option>
									<option value="High" <?php if($ratefuelefficiency == 'High') {?> selected="selected" <?php }?>>High</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Low maintenance costs</strong></td>
							<td align="left" >
								<select name="ratemaintenancecost" id="maintenancecost">
									<option value="Low" <?php if($ratemaintenancecost == 'Low') {?> selected="selected" <?php }?>>Low</option>
									<option value="Medium" <?php if($ratemaintenancecost == 'Medium') {?> selected="selected" <?php }?>>Medium</option>
									<option value="High" <?php if($ratemaintenancecost == 'High') {?> selected="selected" <?php }?>>High</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Reliability</strong></td>
							<td align="left" >
								<select name="ratereliability" id="reliability">
									<option value="Low" <?php if($ratereliability == 'Low') {?> selected="selected" <?php }?>>Low</option>
									<option value="Medium" <?php if($ratereliability == 'Medium') {?> selected="selected" <?php }?>>Medium</option>
									<option value="High" <?php if($ratereliability == 'High') {?> selected="selected" <?php }?>>High</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Luxury "stuff"</strong></td>
							<td align="left" >
								<select name="rateluxury" id="luxury">
									<option value="Low" <?php if($rateluxury == 'Low') {?> selected="selected" <?php }?>>Low</option>
									<option value="Medium" <?php if($rateluxury == 'Medium') {?> selected="selected" <?php }?>>Medium</option>
									<option value="High" <?php if($rateluxury == 'High') {?> selected="selected" <?php }?>>High</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Sporty "stuff"</strong></td>
							<td align="left" >
								<select name="ratesporty" id="sporty">
									<option value="Low" <?php if($ratesporty == 'Low') {?> selected="selected" <?php }?>>Low</option>
									<option value="Medium" <?php if($ratesporty == 'Medium') {?> selected="selected" <?php }?>>Medium</option>
									<option value="High" <?php if($ratesporty == 'High') {?> selected="selected" <?php }?>>High</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Safety</strong></td>
							<td align="left" >
								<select name="ratesafety" id="safety">
									<option value="Low" <?php if($ratesafety == 'Low') {?> selected="selected" <?php }?>>Low</option>
									<option value="Medium" <?php if($ratesafety == 'Medium') {?> selected="selected" <?php }?>>Medium</option>
									<option value="High" <?php if($ratesafety == 'High') {?> selected="selected" <?php }?>>High</option>
								</select>
							</td>
						</tr>
						
						 <tr><td colspan="2" class="assessment_insidetd_green"><strong class="assessment_inside">Do you prefer:</strong></td></tr>
						<tr id="trfrontstype" style="display:none;">
							<td><strong>Front Seat Type</strong></td>
							<td align="left" >
								<select name="frontstype" id="frontstype">
									<option value="Standard Bench" <?php if($frontstype == 'Standard Bench') {?> selected="selected" <?php }?>>Standard Bench</option>
									<option value="Bench Warmrest" <?php if($frontstype == 'Bench Warmrest') {?> selected="selected" <?php }?>>Bench Warmrest</option>
									<option value="Bucket Seats" <?php if($frontstype == 'Bucket Seats') {?> selected="selected" <?php }?>>Bucket Seats</option>
									<option value="Flexible" <?php if($frontstype == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trbedtype" style="display:none;">
							<td><strong>Bed Type</strong></td>
							<td align="left" >
								<select name="bedtype" id="bedtype">
									<option value="Short Bed (about 6 ft.)" <?php if($bedtype == 'Short Bed (about 6 ft.)') {?> selected="selected" <?php }?>>Short Bed (about 6 ft.)</option>
									<option value="Long Bed (about 8 ft.)" <?php if($bedtype == 'Long Bed (about 8 ft.)') {?> selected="selected" <?php }?>>Long Bed (about 8 ft.)</option>
									<option value="Extra Long Bed (about 10 ft.)" <?php if($bedtype == 'Extra Long Bed (about 10 ft.)') {?> selected="selected" <?php }?>>Extra Long Bed (about 10 ft.)</option>
									<option value="Mini Bed (about 4 ft.)" <?php if($bedtype == 'Mini Bed (about 4 ft.)') {?> selected="selected" <?php }?>>Mini Bed (about 4 ft.)</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Leather</strong></td>
							<td align="left" >
								<select name="leather" id="leather">
									<option value="Yes" <?php if($leather == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($leather == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($leather == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($leather == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Heated Seats</strong></td>
							<td align="left" >
								<select name="heatedseat" id="heatedseat">
									<option value="Yes" <?php if($heatedseat == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($heatedseat == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($heatedseat == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($heatedseat == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Navigation</strong></td>
							<td align="left" >
								<select name="navigation" id="navigation">
									<option value="Yes" <?php if($navigation == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($navigation == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($navigation == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($navigation == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Sunroof</strong></td>
							<td align="left" >
								<select name="sunroof" id="sunroof">
									<option value="Yes" <?php if($sunroof == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($sunroof == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($sunroof == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($sunroof == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Alloy Wheels</strong></td>
							<td align="left" >
								<select name="alloywheels" id="alloywheels">
									<option value="Yes" <?php if($alloywheels == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($alloywheels == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($alloywheels == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($alloywheels == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trrearwindow" style="display:none;">
							<td><strong>Rear Sliding Window</strong></td>
							<td align="left" >
								<select name="rearwindow" id="rearwindow">
									<option value="Yes" <?php if($rearwindow == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($rearwindow == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($rearwindow == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($rearwindow == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trbedliner" style="display:none;">
							<td><strong>Bed Liner</strong></td>
							<td align="left" >
								<select name="bedliner" id="bedliner">
									<option value="Yes" <?php if($bedliner == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($bedliner == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($bedliner == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($bedliner == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trentertainmentsystem" style="display:none;">
							<td><strong>Entertainment System</strong></td>
							<td align="left" >
								<select name="entertainmentsystem" id="entertainmentsystem">
									<option value="Yes" <?php if($entertainmentsystem == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($entertainmentsystem == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($entertainmentsystem == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($entertainmentsystem == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trthirdrs" style="display:none;">
							<td><strong>Third Row Seating</strong></td>
							<td align="left" >
								<select name="thirdrs" id="thirdrs">
									<option value="Yes" <?php if($thirdrs == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($thirdrs == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($thirdrs == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($thirdrs == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trcrow" style="display:none;">
							<td><strong>Captain chairs center row</strong></td>
							<td align="left" >
								<select name="crow" id="crow">
									<option value="Yes" <?php if($crow == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($crow == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($crow == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($crow == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trprhatch" style="display:none;">
							<td><strong>Power Rear Hatch</strong></td>
							<td align="left" >
								<select name="prhatch" id="prhatch">
									<option value="Yes" <?php if($prhatch == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($prhatch == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($prhatch == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($prhatch == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trbackupcamera" style="display:none;">
							<td><strong>Backup Camera</strong></td>
							<td align="left" >
								<select name="backupcamera" id="backupcamera">
									<option value="Yes" <?php if($backupcamera == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($backupcamera == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($backupcamera == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($backupcamera == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						<tr id="trtpackage" style="display:none;">
							<td><strong>Tow package</strong></td>
							<td align="left" >
								<select name="tpackage" id="tpackage">
									<option value="Yes" <?php if($tpackage == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
									<option value="No" <?php if($tpackage == 'No') {?> selected="selected" <?php }?>>No</option>
									<option value="Would really like to have" <?php if($tpackage == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
									<option value="Flexible" <?php if($tpackage == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
								</select>
							</td>
						</tr>
						
                                   <tr><td colspan="2" align="center">
                                           <span style="width: 100%">
                                               <button type="submit" class="med" title="After studying your specific needs, we will send you our Best Vehicle Recommendations"><nobr>SUBMIT ASSESSMENT</nobr></button>
                                           &nbsp;<strong>OR</strong>&nbsp; 
                                           <button type="button" class="blueonwhitehideshow" id="showrequirfield" title="After studying your specific needs, we will send you our Best Vehicle Recommendations"><nobr>Give us a little more detail</nobr></button>
                                           </span>
                                       </td></tr>             
                    </table>  
                                 <table class="table" id="notshowfields" style="display: none;">
						
                                             <tr><td colspan="2" class="assessment_insidetd_green" ><strong class="assessment_inside">NONE OF THESE FIELDS ARE REQUIRED</strong></td></tr>
						<tr><td style="width:30%"><strong>Exterior colors you don't like </strong></td>
                                                <td align="left" style="width:40%" >
								
                                                                <input type="text" name="extdislike" id="extdislike" value="<?php echo $extdislike?>" placeholder="Text box">
						</td>
                                                
                                                </tr>

						
						<tr><td ><strong>Interior colors you don't like</strong></td>
                                                    <td align="left" >	
                                                                <input type="text" name="intdislike" id="intdislike" value="<?php echo $intdislike?>" placeholder="Text box">
							</td>
                                                </tr>
                                                
                                                <tr><td colspan="2"><strong style="text-transform: uppercase;">If Borrowing:</strong></td></tr>
						<tr>
							<td><strong>Maximum payment is</strong></td>
							<td align="left" >$
								<select name="borrowmaxpayment" id="borrowmaxpayment" >
									<option value="Not Applicable" <?php if($borrowmaxpayment == 'Not Applicable') {?> selected="selected" <?php }?>>Not Applicable</option>
									<option value="Not Borrowing" <?php if($borrowmaxpayment == 'Not Borrowing') {?> selected="selected" <?php }?>>Not Borrowing</option>
									<option value="Not Sure"<?php if($borrowmaxpayment == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
									<?php for($i=100; $i <= 2000; $i=$i+10){?>
										<option value="<?php echo $i;?>" <?php if($i==$borrowmaxpayment){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
									<?php }?>
								</select>
							</td>
						</tr>
						<tr>
							<td><strong>Down payment is</strong></td>
							<td align="left" >$
								<select name="borrowdownpayment" id="borrowdownpayment" >
									<option value="Not Applicable" <?php if($borrowdownpayment == 'Not Applicable') {?> selected="selected" <?php }?>>Not Applicable</option>
									<option value="Not Borrowing" <?php if($borrowdownpayment == 'Not Borrowing') {?> selected="selected" <?php }?>>Not Borrowing</option>
									<option value="Not Sure" <?php if($borrowdownpayment == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
									<?php for($i=100; $i <= 25000; $i=$i+100){?>
										<option value="<?php echo $i;?>" <?php if($i==$borrowdownpayment){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
									<?php }?>
								</select>
							</td>
						</tr>
						
						<tr>
							<td><strong>Brands you like</strong></td>
							<td align="left" >
								<input type="text" name="brandlike" id="brandlike" value="<?php echo $brandlike;?>" placeholder="Text box">
							</td>
						</tr>
						<tr>
							<td><strong>Brands you dislike</strong></td>
							<td align="left" >
								<input type="text" name="branddislike" id="branddislike" value="<?php echo $branddislike;?>" placeholder="Text box">
							</td>
						</tr>
						<tr>
							<td><strong>Makes & Models you would consider</strong></td>
							<td align="left" >
								<input type="text" name="model" id="model" value="<?php echo $model;?>" placeholder="Text box">
							</td>
						</tr>
                                                
                                                
                                                
                                                  <tr><td colspan="2">&nbsp;</td></tr>
						<tr>
							<td><strong>Other things you must have</strong></td>
							<td align="left" >
								<input type="text" name="otherprefereces" id="otherprefereces" value="<?php echo $otherprefereces;?>" placeholder="Text box">
							</td>
						</tr>
						
						<tr>
							<td><strong>Other things you would really like to have</strong></td>
							<td align="left" >
								<input type="text" name="otherreallyhave" id="otherreallyhave" value="<?php echo $otherreallyhave;?>" placeholder="Text box">
							</td>
						</tr>
						
						<tr>
							<td><strong>Additional Information</strong></td>
							<td align="left" >
								<input type="text" name="additionalinfo" id="additionalinfo" value="<?php echo $additionalinfo;?>" placeholder="Text box">
							</td>
						</tr>
                                                <tr><td colspan="2" align="center"><button type="submit" class="med"><nobr>SUBMIT ASSESSMENT</nobr></button> </td></tr>
					</table>
					
                
                </div><!-- end greyeight-->
            </div><!-- grid eight container -->
        </form>
    </div><!-- end grideightgrey-->
<?php require("teaser.php"); ?>
</div><!--end content-->
<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>