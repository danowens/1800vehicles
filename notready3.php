<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = -1;
    $_SESSION['titleadd'] = 'Not in Step 3 Yet';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 260px;">Getting Too Far Ahead</h1>
        <div class="grideightgrey">
            <div class="grideight" style="width:580px;">
                <p class="blackfourteen" style="margin-top:0;"><strong>You have not completed the actions required to reach step 3 yet.</strong></p>
                <p class="blackfourteen">
                    <strong>
                        To reach Step 3 in the process you will need to perform one of the following actions:<br/>
                        &nbsp;&nbsp;A) Place an <a href="placeorder.php">Order</a><br/>
                        &nbsp;&nbsp;B) Place an <a href="posttogroup.php">Item to the Group Watchlist</a><br/>
                    </strong>
                </p>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
