<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 8;
    $_SESSION['titleadd'] = 'My Account';
    $_SESSION['onloadfunction'] = 'clearform()';

    $userid = $_REQUEST['uid'];
    $genlink = $_REQUEST['genlink'];

    $errorinload = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Check for their correct link code...
        $query = 'select UserID from passwordrecovery u where u.UserID = '.$userid.' and PasswordKey = "'.$genlink.'"';
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            // It is valid...Get the current User Information...
            $_SESSION['userid'] = $userid;
            $_SESSION['loginfailed'] = false;

            // Remove the old key now...
            $query = "delete from passwordrecovery where UserID = ".$userid;
            mysql_query($query, $con);

            $query = 'select u.FirstName, u.LastName, u.Email, u.EmailToPhone, u.Company, u.ImageFile, u.ReferredBy from users u where u.UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $infirst = $row[0];
                $inlast = $row[1];
                $inemail = $row[2];
                $inetp = $row[3];
                $incompany = $row[4];
                $inimage = $row[5];
                $inrefer = $row[6];
            }
            $_SESSION['firstname'] = $infirst;
            $_SESSION['lastname'] = $inlast;

            $query = 'select l.UseEmail, l.Login, l.PWQuestion1, l.PWAnswer1 from userlogin l where l.UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $inuseemail = $row[0];
                $inlogin = $row[1];
                $inpwq1 = $row[2];
                $inpwa1 = $row[3];
            }
            if($inuseemail == 1) $_SESSION['user'] = $inemail;
            else $_SESSION['user'] = $inlogin;

            $query = 'select max(marketneedid) from marketneeds where UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $inneed = $row[0];
            }
            $_SESSION['marketneedid'] = $inneed;

            $query = 'select a.ZIP, a.Address1, a.Address2, a.City, a.State from useraddresses a where a.IsPrimary = 1 and a.UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $inzip = $row[0];
                $inaddr1 = $row[1];
                $inaddr2 = $row[2];
                $incity = $row[3];
                $instate = $row[4];
            }

            $query = 'select hn.PhoneNumber from usernumbers hn where hn.NumberTypeID = 1 and hn.UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $inhome = $row[0];
            }

            $query = 'select cn.PhoneNumber, cn.Texting from usernumbers cn where cn.NumberTypeID = 2 and cn.UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $inmobl = $row[0];
                $inmobltxt = $row[1];
            }

            $query = 'select wn.PhoneNumber, wn.Extension from usernumbers wn where wn.NumberTypeID = 3 and wn.UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $inwork = $row[0];
                $inworkext = $row[1];
            }

            $query = 'select fn.PhoneNumber from usernumbers fn where fn.NumberTypeID = 4 and fn.UserID = '.$userid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result))
            {
                $infax = $row[0];
            }

            // Get the optional User Information...
            $query = 'select AddInfo, Email, EmailToPhone from alternateemails where UserID = '.$userid.' order by DisplayOrder';
            $result = mysql_query($query, $con);
            $count = 0;
            while($result && ($row = mysql_fetch_array($result)) && ($count < 2))
            {
                $inaltemailinfo[$count] = $row[0];
                $inaltemail[$count] = $row[1];
                $inaltemailetp[$count] = $row[2];
            }

            $count = 0;
            $query = 'select PhoneNumber, Extension, AddInfo, Texting from usernumbers where NumberTypeID = 6 and UserID = '.$userid.' order by DisplayOrder';
            $result = mysql_query($query, $con);
            $count = 0;
            while($result && ($row = mysql_fetch_array($result)) && ($count < 2))
            {
                $inaltnum[$count] = $row[0];
                $inaltnumext[$count] = $row[1];
                $inaltnuminfo[$count] = $row[2];
                $inaltnumtxt[$count] = $row[3];
            }
        }
        else
        {
            $errorinload = 'Password Recovery Link may only be used once.  Please try generating a new one.';
            unset($_SESSION['userid']);
            unset($_SESSION['loginfailed']);
            unset($_SESSION['firstname']);
            unset($_SESSION['lastname']);
            unset($_SESSION['user']);
            $_SESSION['state'] = 0;
            $_SESSION['substate'] = 0;
        }

        mysql_close($con);
    }
    else $errorinload = 'Could not retrieve User Information...please try again later.';
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function clearform()
    {
        emailchanged();
    }

    function validateFormOnSubmit(theForm)
    {
        var focusset = 0;
        var reason = "";
        var temp = validateEmpty(theForm.elements["firstname"], "FirstName");
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["firstname"].focus();
                focusset = 1;
            }
        }
        temp = validateEmpty(theForm.elements["lastname"], "LastName");
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["lastname"].focus();
                focusset = 1;
            }
        }
        temp = validateEmpty(theForm.elements["zip"], "Zip Code");
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["zip"].focus();
                focusset = 1;
            }
        }
        temp = validateZip(theForm.elements["zip"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["zip"].focus();
                focusset = 1;
            }
        }
        temp = validateEmpty(theForm.elements["email"], "Email");
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["email"].focus();
                focusset = 1;
            }
        }
        temp = validateEmail(theForm.elements["email"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["email"].focus();
                focusset = 1;
            }
        }
        temp = validateEmail(theForm.elements["email2"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["email2"].focus();
                focusset = 1;
            }
        }
        else theForm.elements["email2"].style.background = "White";
        temp = validateEmail(theForm.elements["email3"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["email3"].focus();
                focusset = 1;
            }
        }
        else theForm.elements["email3"].style.background = "White";
        temp = validatePhone(theForm.elements["homephone"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["homephone"].focus();
                focusset = 1;
            }
        }
        temp = validatePhone(theForm.elements["cellphone"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["cellphone"].focus();
                focusset = 1;
            }
        }
        temp = validatePhone(theForm.elements["workphone"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["workphone"].focus();
                focusset = 1;
            }
        }
        temp = validatePhone(theForm.elements["addphone1"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["addphone1"].focus();
                focusset = 1;
            }
        }
        else theForm.elements["addphone1"].style.background = "White";
        temp = validatePhone(theForm.elements["addphone2"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["addphone2"].focus();
                focusset = 1;
            }
        }
        else theForm.elements["addphone2"].style.background = "White";
        temp = validatePhone(theForm.elements["faxnum"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["faxnum"].focus();
                focusset = 1;
            }
        }
        else theForm.elements["faxnum"].style.background = "White";
        if(!validateOneSet(theForm.elements["homephone"], theForm.elements["cellphone"], theForm.elements["workphone"]))
        {
            reason += "At least one phone number must be filled in."+'\n';
            theForm.elements["cellphone"].style.background = '#ffcccc';
            if(focusset == 0)
            {
                theForm.elements["cellphone"].focus();
                focusset = 1;
            }
        }
        if(theForm.elements["useemail"].checked == false)
        {
            temp = validateEmpty(theForm.elements["loginname"], "Login");
            if(temp != "")
            {
                reason += temp;
                if(focusset == 0)
                {
                    theForm.elements["loginname"].focus();
                    focusset = 1;
                }
            }
        }
        else theForm.elements["loginname"].style.background = 'LightGrey';
        temp = validatePassword(theForm.elements["rp"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["rp"].focus();
                focusset = 1;
            }
        }
        temp = validatePassword2(theForm.elements["rp2"],theForm.elements["rp"]);
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["rp2"].focus();
                focusset = 1;
            }
        }
        temp = validateEmpty(theForm.elements["answer1"], "Password Answer");
        if(temp != "")
        {
            reason += temp;
            if(focusset == 0)
            {
                theForm.elements["answer1"].focus();
                focusset = 1;
            }
        }

        return true;
    }

    function validateOneSet(fld1, fld2, fld3)
    {
        if(fld1.value.length > 0)
        {
            return true;
        }
        if(fld2.value.length > 0)
        {
            return true;
        }
        if(fld3.value.length > 0)
        {
            return true;
        }
        return false;
    }

    function trimAll(sString)
    {
        while(sString.substring(0,1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while(sString.substring(sString.length-1, sString.length) == ' ')
        {
            sString = sString.substring(0,sString.length-1);
        }
        return sString;
    }

    function validateEmpty(fld, title)
    {
        var error = "";
        var tString = trimAll(fld.value);

        if (tString.length == 0)
        {
            fld.style.background = '#ffcccc';
            error = title + " is required."+'\n';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    function validateZip(fld)
    {
        var error = "";
        if (fld.value.length == 0) return error;
        if(fld.value.length != 5)
        {
            fld.style.background = '#ffcccc';
            error = "Zip Code must be five digits."+'\n';
        }
        return error;
    }

    function validateEmail(fld)
    {
        var error = "";
        var tfld = trimAll(fld.value);
        tfld = tfld.replace(/^\s+|\s+$/, '');  // value of field with whitespace trimmed off
        var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
        var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;

        if (fld.value.length == 0) return error;
        if(!emailFilter.test(tfld))
        {
            fld.style.background = '#ffcccc';
            error = "Email address is not valid."+'\n';
        }
        else if(fld.value.match(illegalChars))
        {
            fld.style.background = '#ffcccc';
            error = "Email address contains illegal characters."+'\n';
        }
        return error;
    }

    function validatePhone(fld)
    {
        var error = "";
        var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');

        if (fld.value.length == 0) return error;
        if(isNaN(parseInt(stripped,10)))
        {
            error = "The phone number contains illegal characters."+'\n';
            fld.style.background = '#ffcccc';
        }
        else if(!(stripped.length == 10))
        {
            error = "The phone number is the wrong length."+'\n';
            fld.style.background = '#ffcccc';
        }
        return error;
    }

    function validatePassword(fld)
    {
        var error = "";
        var illegalChars = new RegExp("[\W_]");
        var letterChars = new RegExp("[A-Z|a-z]");
        var numChars = new RegExp("[0-9]");

        if (fld.value.length < 1)
        {
            error = "The password must be filled in. "+'\n';
            fld.style.background = '#ffcccc';
        }
        else if ((fld.value.length < 6) || (fld.value.length > 15))
        {
            error = "The password must be 6-15 characters or numbers. "+'\n';
            fld.style.background = '#ffcccc';
        }
        else if (illegalChars.test(fld.value))
        {
            error = "The password should contains only characters and numbers."+'\n';
            fld.style.background = '#ffcccc';
        }
        else if (!numChars.test(fld.value))
        {
            error = "The password must contain numbers."+'\n';
            fld.style.background = '#ffcccc';
        }
        else if (!letterChars.test(fld.value))
        {
            error = "The password must contain letters."+'\n';
            fld.style.background = '#ffcccc';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    function validatePassword2(fld, fld2)
    {
        var error = "";
        if (fld.value.length < 1)
        {
            error = "The verification password must be filled in. "+'\n';
            fld.style.background = '#ffcccc';
        }
        else if(fld.value != fld2.value)
        {
            error = "The verification password must match the first password field. "+'\n';
            fld.style.background = '#ffcccc';
        }
        else
        {
            fld.style.background = 'White';
        }
        return error;
    }

    // copyright 1999 Idocs, Inc. http://www.idocs.com
    // Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
           key = window.event.keyCode;
        else if (e)
           key = e.which;
        else
           return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
           return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
           return true;

        else
           return false;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
<?php
    if($errorinload != 'false')
    {
        echo '<div class="grideightcontainer">';
        echo '<div class="grideightgrey">';
        echo $errorinload;
        echo '</div>';
        echo '</div>';
    }
    else{
?>
    <form action="accountstatus.php" autocomplete="off" onsubmit="return validateFormOnSubmit(this)" method="post" name="userform" enctype="multipart/form-data">
        <div class="grideightcontainer">
            <h1 class="subhead" style="width: 250px;">Required Information</h1>
            <div class="grideightgrey">
                <p class="blacktwelve" style="margin-top:-3px; color: rgb(68, 68, 68); font-weight: bold; font-size: 12px;"><span style="font-size: 16px">&#42;</span> Required fields </p>
                <p>
                    <label for="firstname" class="registera"><span>F</span>irst <span>N</span>ame <span style="font-size: 16px">&#42;</span></label>
                    <input type="text" name="firstname" value="<?php echo $infirst;?>" size="30" maxlength="50" />
                </p>
                <p>
                    <label for="lastname" class="registera"></pan>L</span>ast <span>N</span>ame <span style="font-size: 16px">&#42;</span></label>
                    <input type="text" name="lastname" value="<?php echo $inlast;?>" size="30" maxlength="50" />
                </p>
                <p>
                    <label for="zip" class="registera">Zip <span style="font-size: 16px">&#42;</span></label>
                    <input name="zip" type="text" value="<?php echo $inzip;?>" size="6" maxlength="5" onkeypress="javascript:return numbersonly(event);" />
                </p>
                <p>
                    <label for="email" class="registera">Email <span style="font-size: 16px">&#42;</span></label>
                    <input type="text" name="email" value="<?php echo $inemail;?>" size="30" maxlength="200" onchange="javascript:emailchanged()" />
                </p>
                <p>
                    <label for="e2p" class="registera">Email to Phone? <span style="font-size: 16px">&#42;</span></label>
                    <input type="radio" name="e2p" value="Yes" <?php if($inetp == 1) echo 'checked="checked"';?> />
                    <label for="e2p" class="register">Yes</label>
                    <input type="radio" name="e2p" value="No" <?php if($inetp == 0) echo 'checked="checked"';?> />
                    <label for="e2p" class="register">No</label>
                </p>
                <h4 class="subhead" style="width: 300px;">Phone Numbers:</h4>
                <p style="margin-top: 5px; color: rgb(68, 68, 68); font-weight: bold; font-size: 12px"><span style="font-size: 16px">&#42;</span> Only one is required - please include the area code</p>
                <p>
                    <label for="homephone" class="registera">Home Phone</label>
                    <input name="homephone" type="text" value="<?php echo $inhome;?>" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" />
                </p>
                <p>
                    <label for="cellphone" class="registera">Mobile Phone</label>
                    <input name="cellphone" type="text" value="<?php echo $inmobl;?>" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" />

                    <label for="celltext" class="register">Text Messaging?</label>
                    <input type="radio" name="celltext" value="Yes" <?php if($inmobltxt == 1) echo 'checked="checked"';?> />
                    <label for="celltext" class="register">Yes</label>
                    <input type="radio" name="celltext" value="No" <?php if($inmobltxt == 0) echo 'checked="checked"';?>/>
                    <label for="celltext" class="register">No</label>
                </p>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="320">
                            <label for="workphone" class="registera">Work Phone</label>
                            <input name="workphone" type="text" value="<?php echo $inwork;?>" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" />
                        </td>
                        <td>
                            <label for="workext" class="registera" style="width:20px;">ext</label>
                            <input name="workext" type="text" value="<?php echo $inworkext;?>" size="5" maxlength="10" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <h4 class="subhead" style="width: 300px;">Create Login &amp; Password</h4>
                <p style="margin-top: 5px; color: rgb(68, 68, 68); font-weight: bold; font-size: 12px"><span style="font-size: 16px">&#42;</span> We suggest using your email as your login name</p>
                <p>
                    <span style="float: right; width: 168px; margin-left: -5px; font-size: 11px; color: rgb(153, 153, 153);">
                        <label for="useemail" class="registera"></label>
                        <input type="checkbox" name="useemail" <?php if($inuseemail == 1) echo 'checked="checked"';?> onclick="javascript:useemailswitched();"/>Use email as login name<br/>
                    </span>
                    <label for="loginname" class="registera"><span>L</span>ogin</label>
                    <input type="text" name="loginname" value="<?php echo $inlogin;?>" size="30" maxlength="20" disabled="disabled" style="background:LightGrey none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial;" />
                </p>
                <p>
                    <span style="float: right; width: 155px; font-size: 11px; color: rgb(153, 153, 153); padding-right: 9px;">
                        Password must be a minimum of six letters and at least one number. No symbols please.
                    </span>
                    <label for="rp" class="registera"> Change <span>P</span>assword <span style="font-size: 16px">&#42;</span></label>
                    <input type="password" name="rp" id="rp" value="" size="30" maxlength="15" style="width:211px;padding: 3px;" />
                </p>
                <p>
                    <label for="rp2" class="registera"> Verify Changed <span>P</span>assword <span style="font-size: 16px">&#42;</span></label>
                    <input type="password" name="rp2" id="rp2" value="" size="30" maxlength="15" style="width:211px;padding: 3px;" />
                </p>
                <h4 class="subhead" style="width: 300px;">In case of forgotten password:</h4>
                <p class="searchheaderblk" style="color: rgb(68, 68, 68);"> Ask me a question: </p>
                <p>
                    <label for="question1" class="registera">Question <span style="font-size: 16px">&#42;</span></label>
                    <select name="question1" id="question1" style="width: 220px;">
                        <option value="mothersmaiden" <?php if($inpwq1 == 'mothersmaiden') echo 'selected="selected"';?> >What is your mother's maiden name?</option>;
                        <option value="firstelem" <?php if($inpwq1 == 'firstelem') echo 'selected="selected"';?> >What is the first elementary school you attended?</option>;
                        <option value="grandma" <?php if($inpwq1 == 'grandma') echo 'selected="selected"';?> >What is your favorite grandmother's name?</option>;
                        <option value="favpetsname" <?php if($inpwq1 == 'favpetsname') echo 'selected="selected"';?> >What is your favorite pet's name?</option>;
                        <option value="highschool" <?php if($inpwq1 == 'highschool') echo 'selected="selected"';?> >What was the primary high school you attended?</option>;
                        <option value="sports" <?php if($inpwq1 == 'sports') echo 'selected="selected"';?> >What is your favorite sport team's name?</option>;
                        <option value="fathermid" <?php if($inpwq1 == 'fathermid') echo 'selected="selected"';?> >What is your father's middle name?</option>;
                        <option value="mothermid" <?php if($inpwq1 == 'mothermid') echo 'selected="selected"';?> >What is your mother's middle name?</option>;
                        <option value="spousemid" <?php if($inpwq1 == 'spousemid') echo 'selected="selected"';?> >What is your spouse's middle name?</option>;
                        <option value="childmiddle" <?php if($inpwq1 == 'childmiddle') echo 'selected="selected"';?> >What is your first child's middle name?</option>;
                        <option value="birthplace" <?php if($inpwq1 == 'birthplace') echo 'selected="selected"';?> >What city were you born in?</option>;
                        <option value="favteacher" <?php if($inpwq1 == 'favteacher') echo 'selected="selected"';?> >Who was your favorite teacher?</option>;
                        <option value="firstjob" <?php if($inpwq1 == 'firstjob') echo 'selected="selected"';?> >What was your first job growing up?</option>;
                    </select>
                </p>
                <p>
                    <label for="answer1" class="registera">Answer <span style="font-size: 16px">&#42;</span></label>
                    <input type="text" name="answer1" value="<?php if($inpwa1 != 'none') echo $inpwa1;?>" size="30" maxlength="50" />
                </p>
                <p class="searchheaderblk" style="color: rgb(68, 68, 68);">Once registered you will be able to login and:<br /></p>
                <ul>
                    <li style="color:#85C11B;"> Save your favorite research results</li>
                    <li style="color:#85C11B;"> Consult with a professional car buyer without obligation</li>
                    <li style="color:#85C11B;"> Get &quot;Firm Quotes&quot; on specific vehicles</li>
                    <li style="color:#85C11B;"> Place an order and let 1-800-vehicles.com find you the perfect vehicle</li>
                </ul>
                <p class="searchheaderblk" style="color: rgb(68, 68, 68);">A 1-800-vehicles.com representative may contact you<br />to see if you would  like free consulting.</p>
            </div>
            <h1 class="subhead" style="width: 250px;">Additional Information</h1>
            <div class="grideightgrey">
                <p class="blacktwelve" style="margin-top:-3px; color: rgb(68, 68, 68); font-weight: bold; font-size: 12px;">
                    <span style="font-size: 16px">&#42;&#42;&#42;&nbsp;</span>
                    None of the following fields are required
                </p>
                <p>
                    <label for="userimage" class="registera">Current Image:</label>
<?php
    if(strlen($inimage)>0) $srctext=$inimage;
    else $srctext="userimages/nopicture.jpg";
    $max_width = 160;
    $max_height = 160;
    echo '<img id="userimage" src="loadimage.php?image='.$srctext.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
?>
                </p>
                <p>
                    <label for="photoname" class="registera">Upload new photo</label>
                    <input type="file" name="photoname" id="photoname" value="<?php echo $inimage;?>" />
                    <!--button type="button" value="" class="small">BROWSE</button-->
                </p>
                <p>
                    <label for="company" class="registera">Employer</label>
                    <input type="text" name="company" value="<?php echo $incompany;?>" size="30" maxlength="50" />
                </p>
                <h4 class="subhead" style="width: 300px;"><span>A</span>ddress:</h4>
                <p>
                    <label for="address1" class="registera"><span>A</span>ddress 1</label>
                    <input type="text" name="address1" value="<?php echo $inaddr1;?>" size="30" maxlength="150" />
                </p>
                <p>
                    <label for="address2" class="registera"></label>
                    <input type="text" name="address2" value="<?php echo $inaddr2;?>" size="30" maxlength="150" />
                </p>
                <p>
                    <label for="city" class="registera">City</label>
                    <input type="text" name="city" value="<?php echo $incity;?>" size="30" maxlength="35" />
                </p>
                <p>
                    <label for="state" class="registera">State</label>
                    <input type="text" name="state" value="<?php echo $instate;?>" size="2" maxlength="2" />
                </p>
                <h4 class="subhead" style="width: 300px;">Alternate Phone Numbers:</h4>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <label for="addphone1" class="registera">Additional Number(s)</label>
                            <input name="addphone1" type="text" value="<?php echo $inaltnum[0];?>" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" />
                        </td>
                        <td>
                            <label for="addphone1ext" class="registera" style="width:20px;">ext</label>
                            <input name="addphone1ext" type="text" value="<?php echo $inaltnumext[0];?>" size="5" maxlength="10" />
                        </td>
                        <td>
                            <label for="addphone1type" class="registera" style="width:40px;">Notes</label>
                            <input name="addphone1type" type="text" value="<?php echo $inaltnuminfo[0];?>" size="8" maxlength="50" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <label for="addphone1text" class="register">Text Messaging?</label>
                            <input type="radio" name="addphone1text" value="Yes" <?php if($inaltnumtxt[0] == 1) echo 'checked="checked"';?> />
                            <label for="addphone1text" class="register">Yes</label>
                            <input type="radio" name="addphone1text" value="No" <?php if($inaltnumtxt[0] == 0) echo 'checked="checked"';?> />
                            <label for="addphone1text" class="register">No</label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <label for="addphone2" class="registera">&nbsp;</label>
                            <input name="addphone2" type="text" value="<?php echo $inaltnum[1];?>" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" />
                        </td>
                        <td>
                            <label for="addphone2ext" class="registera" style="width:20px;">ext</label>
                            <input name="addphone2ext" type="text" value="<?php echo $inaltnumext[1];?>" size="5" maxlength="10" />
                        </td>
                        <td>
                            <label for="addphone2type" class="registera" style="width:40px;">Notes</label>
                            <input name="addphone2type" type="text" value="<?php echo $inaltnuminfo[1];?>" size="8" maxlength="50" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <label for="addphone2text" class="register">Text Messaging?</label>
                            <input type="radio" name="addphone2text" value="Yes" <?php if($inaltnumtxt[1] == 1) echo 'checked="checked"';?> />
                            <label for="addphone2text" class="register">Yes</label>
                            <input type="radio" name="addphone2text" value="No" <?php if($inaltnumtxt[1] == 0) echo 'checked="checked"';?> />
                            <label for="addphone2text" class="register">No</label>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <p>
                    <label for="faxnum" class="registera">Fax</label>
                    <input name="faxnum" type="text" value="<?php echo $infax;?>" size="10" maxlength="10" onkeypress="javascript:return numbersonly(event);" />
                </p>
                <h4 class="subhead" style="width: 300px;">Alternate Email:</h4>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <label for="email2" class="registera">Email</label>
                            <input name="email2" type="text" value="<?php echo $inaltemail[0];?>" size="30" maxlength="200" />
                        </td>
                        <td>
                            <label for="email2type" class="registera" style="width:40px;">Notes</label>
                            <input name="email2type" type="text" value="<?php echo $inaltemailinfo[0];?>" size="8" maxlength="10" />
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td>
                            <label for="e2p2" class="registera">Email to Phone?</label>
                            <input type="radio" name="e2p2" value="Yes" <?php if($inaltemailetp[0] == 1) echo 'checked="checked"';?> />
                            <label for="e2p2" class="register">Yes</label>
                            <input type="radio" name="e2p2" value="No" <?php if($inaltemailetp[0] == 0) echo 'checked="checked"';?> />
                            <label for="e2p2" class="register">No</label>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td>
                            <label for="email3" class="registera">Email</label>
                            <input name="email3" type="text" value="<?php echo $inaltemail[1];?>" size="30" maxlength="200" />
                        </td>
                        <td>
                            <label for="email3type" class="registera" style="width:40px;">Notes</label>
                            <input name="email3type" type="text" value="<?php echo $inaltemailinfo[1];?>" size="8" maxlength="10" />
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td>
                            <label for="e2p3" class="registera">Email to Phone?</label>
                            <input type="radio" name="e2p3" value="Yes" <?php if($inaltemailetp[1] == 1) echo 'checked="checked"';?> />
                            <label for="e2p3" class="register">Yes</label>
                            <input type="radio" name="e2p3" value="No" <?php if($inaltemailetp[1] == 0) echo 'checked="checked"';?> />
                            <label for="e2p3" class="register">No</label>
                        </td>
                    </tr>
                </table>
                <p style="color:#85c11b; font-size:13px; margin-left:8px;">* Privacy Statement:  Customer information will not be shared or sold!</p>
                <p>
                    <span class="gridfour">
                        <button type="submit" value="" class="med">SUBMIT INFORMATION</button>
                    </span>
                </p>
            </div>
        </div><!-- grid eight container -->
    </form>
<?php
    }
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
