<?php
    require_once("globals.php");

    ini_set('display_errors', 'on');

    $_SESSION['state'] = 6;
    $_SESSION['substate'] = 25;
    $_SESSION['titleadd'] = 'Recent Customer List';

    // Get the list of customers in the system...
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select '".date_at_timezone('Y-m-d H:i:s', 'EST')."' from marketneeds where MarketNeedID=1";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $datenow = $row[0];
        }
        else $datenow = date();
        $onemonthback = date('Y-m-d H:i:s', (strtotime($datenow)-(30*24*60*60)));

        $query = "select u.firstname, u.lastname, u.email, u.created, u.lastupdated, u.referredby, u.repname, u.note, u.franchiseinterest, m.seenbysales, m.needscontact from users u, marketneeds m where m.userid=u.userid and u.lastupdated > '".date_at_timezone('Y-m-d','EST', $onemonthback)."' order by u.lastupdated desc";
        $result = mysql_query($query, $con);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $ifirst[$index] = $row[0];
            $ilast[$index] = $row[1];
            $iemail[$index] = $row[2];
            $icreated[$index] = $row[3];
            $iupdated[$index] = $row[4];
            $irefer[$index] = $row[5];
            $irep[$index] = $row[6];
            $inote[$index] = $row[7];
            $ifran[$index] = $row[8];
            $iseen[$index] = $row[9];
            $ineed[$index] = $row[10];
            $index++;
        }

        mysql_close($con);
    }
?>
<?php require_once(WEB_ROOT_PATH."headerstart.php"); ?>
<?php require_once(WEB_ROOT_PATH."header.php"); ?>
<?php require_once(WEB_ROOT_PATH."foursteps.php"); ?>
<?php require_once(WEB_ROOT_PATH."headerend.php"); ?>

<div id="content">
<?php
    echo '<h1 class="subhead">Recent Customers</h1>';
    echo '<b>Since:</b> '.date_at_timezone('m/d/Y','EST', $onemonthback).'<br/><br/>';
    echo '<table border="1"><thead>';
    echo '<tr>';
    echo '<th>First</th>';
    echo '<th>Last</th>';
    echo '<th>Email</th>';
    echo '<th>Created</th>';
    echo '<th>Updated</th>';
    echo '<th>Refer</th>';
    echo '<th>Rep</th>';
    echo '<th>Fran</th>';
    echo '<th>Seen</th>';
    echo '<th>Contact</th>';
    echo '<th>Note</th>';
    echo '</tr><tbody>';
    $count = count($ifirst);
    for($i=0;$i<$count;$i++)
    {
        echo '<tr>';
        echo '<td>'.$ifirst[$i].'</td>';
        echo '<td>'.$ilast[$i].'</td>';
        echo '<td>'.$iemail[$i].'</td>';
        echo '<td>'.date_at_timezone('m/d/Y H:i:s','EST', $icreated[$i]).'</td>';
        echo '<td>'.date_at_timezone('m/d/Y H:i:s','EST', $iupdated[$i]).'</td>';
        echo '<td>'.$irefer[$i].'</td>';
        echo '<td>'.$irep[$i].'</td>';
        echo '<td>'.$ifran[$i].'</td>';
        echo '<td>'.$iseen[$i].'</td>';
        echo '<td>'.$ineed[$i].'</td>';
        echo '<td>'.$inote[$i].'</td>';
        echo '</tr>';
    }
    echo '</tbody></table>';
?>
</div><!--end content-->

<?php require_once(WEB_ROOT_PATH."footerstart.php"); ?>
<?php require_once(WEB_ROOT_PATH."footer.php"); ?>
<?php require_once(WEB_ROOT_PATH."footerend.php"); ?>
