<div class="spread">
    
        <table>
            <tr><td width="900">
                <h1 class="subhead" style="width:275px;">Testimonials</h1>
                </td><td>
                <div style="float:right">
                    <a id="bbblink" class="sehzbus" href="http://www.bbb.org/nashville/business-reviews/franchising/1-800-vehiclescom-franchising-in-nashville-tn-37065588#bbblogo" title="1-800-Vehicles.com Franchising, Franchising, Nashville, TN" style="overflow: hidden; width: 100px; height: 38px; margin: -10px 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-nashville.bbb.org/logo/sehzbus/1-800-vehiclescom-franchising-37065588.png" width="200" height="38" alt="1-800-Vehicles.com Franchising, Franchising, Nashville, TN" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2F1-800-vehiclescom-franchising-37065588.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
                </div>
            </td></tr>
        </table>
    
    <div class="testimonial">
        <img src="common/layout/testimony1.jpg" class="custo" />
        "This is the only way to buy a car! The car is like brand new--a much better value than I would have ever expected. Great, friendly and honest!"
        <p>Jenny, <br /></p>
    </div>
    <div class="testimonial">
        <img src="common/layout/testimony2.jpg" class="custo" />
        "I hate shopping but this experience was great. I was able to get a fabulous car for a phenominal price. I am recommending 1-800-vehicles.com to all my friends and will use them to buy my wife a BMW!"
        <p>James, <br /></p>
    </div>
    <div class="testimonial">
        <img src="common/layout/testimony3.jpg" class="custo" />
        "We are sold on 1-800-vehicles.com! Great price, great service and a great car! We will definately be recommending them to everyone. Thank you!"
        <p>Dorothy, <br /></p>
    </div>
</div><!-- end spread -->
