<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
    <head>
        <title>1-800-Vehicles.com - Why Register</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="robots" content="" />
        <link href="style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="common/scripts/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="common/scripts/easySlider1.7.js"></script>
        <script type="text/javascript" src="common/scripts/master.js"></script>
        <link href="screen.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="common/scripts/jquery.hoverIntent.minified.js"></script>
    </head>
<body style="background-color:#FFF;">
    <h1 class="subhead">Why Register?</h1>
    <div class="gridtenpadded">
        <p style="font-size:14px; color:#85c11b; margin-top:5px;">Once registered you will be able to login and:</p>
        <ul class="greenlist">
            <li>Save your favorite research results</li>
            <li>Consult with a professional car buyer without obligation</li>
            <li>Get "Firm Quotes" on specific vehicles</li>
            <li>Place an order and let 1-800-vehicles.com find you the perfect vehicle</li>
        </ul>
    </div> <!-- endgrideightgrey -->
</body>
</html>
