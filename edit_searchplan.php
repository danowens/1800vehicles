<?php require("globals.php"); ?>   
<?php
//error_reporting(E_ALL);
//ini_set("display_errors", "on");
$_SESSION['state'] = 1; 
$_SESSION['substate'] = 23;
$_SESSION['titleadd'] = 'Add Price & Availability Study';
$userid = $_SESSION['userid'];
$marketneedid = $_SESSION['marketneedid'];

$SearchPlanDetailID = $_POST['SearchPlanDetailID'];
$total = 0;


$con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
if ($con) {
    mysql_select_db(DB_SERVER_DATABASE, $con);
    $vspquery = "select * from searchplandetails where SearchPlanDetailID = " . $SearchPlanDetailID;
    $vspresult = mysql_query($vspquery, $con);

    $vspdata = array();

    while ($vsprow = mysql_fetch_array($vspresult)) {
        $total = $total + 1;
        $vspdata[$total] = $vsprow;
    }


    mysql_close($con);
}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<script type="text/javascript">



    $(document).ready(function() {
        $("input:text").click(function() {
            var element = $(this);
            $("#notetext").val(element.val());
            $("#dialog").dialog({
                modal: true,
                title: " ",
                close: function(event, ui) {
                    element.val($("#notetext").val());
                    $("#dialog").dialog("destroy");
                }
            });
        });
        
         $(".grideightgrey .showrequirfield").bind('click', function(e) {
           
                        e.preventDefault();
			var id= $(this).attr('myhidevalue');
                    
                        $('#notshowfields'+id).toggle();
	}); 
    });

    function yearchanged(id) {
        var year = $("#yearlist" + id).val();
        $.ajax({
            type: "POST",
            url: 'ajaxallmakes.php',
            data: {year: year},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#makelist" + id).html('');
                $("#makelist" + id).append(data);

                makechanged(id);
            }
        });
    }

    function makechanged(id) {
        var year = $("#yearlist" + id).val();
        var make = $("#makelist" + id).val();
        $.ajax({
            type: "POST",
            url: 'ajaxallmodels.php',
            data: {year: year, make: make},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#modellist" + id).html('');
                $("#modellist" + id).append(data);

                modelchanged(id);
            }
        });
    }

    function modelchanged(id) {
        var year = $("#yearlist" + id).val();
        var make = $("#makelist" + id).val();
        var model = $("#modellist" + id).val();

        $.ajax({
            type: "POST",
            url: 'ajaxallstyles.php',
            data: {year: year, make: make, model: model},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#stylelist" + id).html('');
                $("#stylelist" + id).append(data);

                stylechanged(id);
            }
        });
    }

    function stylechanged(id) {
        var style = $("#stylelist" + id).val().split(";");
        $.ajax({
            type: "POST",
            url: 'ajaxcheckvehicletype.php',
            data: {id: style[0]},
            cache: false,
            async: false,
            dataType: 'html',
            success: function(data) {
                $("#vehicletype" + id).val(data);
                if (data == 'Auto') {
                    $("#drivetrain" + id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');

//Do you prefer section
                    $("#trfrontstype" + id).hide();
                    $("#trbedtype" + id).hide();
                    $("#trrearwindow" + id).hide();
                    $("#trbedliner" + id).hide();
                    $("#trentertainmentsystem" + id).hide();
                    $("#trthirdrs" + id).hide();
                    $("#trcrow" + id).hide();
                    $("#trprhatch" + id).hide();
                    $("#trbackupcamera" + id).hide();
                    $("#trtpackage" + id).hide();

                } else if (data == 'Minivan' || data == 'MiniVan') {
                    $("#drivetrain" + id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');

//Do you prefer section
                    $("#trfrontstype" + id).hide();
                    $("#trbedtype" + id).hide();
                    $("#trrearwindow" + id).hide();
                    $("#trbedliner" + id).hide();
                    $("#trentertainmentsystem" + id).show();
                    $("#trthirdrs" + id).hide();
                    $("#trcrow" + id).hide();
                    $("#trprhatch" + id).show();
                    $("#trbackupcamera" + id).show();
                    $("#trtpackage" + id).hide();

                } else if (data == 'SUV') {
                    $("#drivetrain" + id).html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');

//Do you prefer section
                    $("#trfrontstype" + id).hide();
                    $("#trbedtype" + id).hide();
                    $("#trrearwindow" + id).hide();
                    $("#trbedliner" + id).hide();
                    $("#trentertainmentsystem" + id).show();
                    $("#trthirdrs" + id).show();
                    $("#trcrow" + id).show();
                    $("#trprhatch" + id).show();
                    $("#trbackupcamera" + id).show();
                    $("#trtpackage" + id).show();

                } else if (data == 'Pickup') {
                    $("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');

//Do you prefer section
//@TODO make function with 2 parameter (Array of ids and event)
                    $("#trfrontstype" + id).show();
                    $("#trbedtype" + id).show();
                    $("#trrearwindow" + id).show();
                    $("#trbedliner" + id).show();
                    $("#trentertainmentsystem" + id).hide();
                    $("#trthirdrs" + id).hide();
                    $("#trcrow" + id).hide();
                    $("#trprhatch" + id).hide();
                    $("#trbackupcamera" + id).show();
                    $("#trtpackage" + id).show();
                }
            }
        });
    }

    function specific(id, val) {
        $("#specific" + id).val(val);
        if (val == 1) {
            $("#specificsection" + id).hide();
            $("#selectyear" + id).hide();
            $("#selectmake" + id).hide();
            $("#selectmodel" + id).hide();
            $("#selectstyle" + id).hide();

            $("#vehiclesection" + id).show();
            $("#textyear" + id).show();
            $("#textmake" + id).show();
            $("#textmodel" + id).show();
            $("#textstyle" + id).show();
        } else {
            $("#specificsection" + id).show();
            $("#selectyear" + id).show();
            $("#selectmake" + id).show();
            $("#selectmodel" + id).show();
            $("#selectstyle" + id).show();

            $("#vehiclesection" + id).hide();
            $("#textyear" + id).hide();
            $("#textmake" + id).hide();
            $("#textmodel" + id).hide();
            $("#textstyle" + id).hide();
        }
    }

</script>
 <div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">

        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">
            <!--Vehicle Specification--> 
            Edit Vehicle Search Plan
            <span style="float: right; margin-right: 10px; font-size: 16px;"><a href="searchplan.php" style="color:black"><img src="images/back.png"></a></span>
        </h1>
        <form action="searchplansave.php" onsubmit="javascript:return validateFormOnSubmit()" method="post" name="assessform">
            <?php
            for ($current = 1; $current <= $total; $current++) {
                $recon = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
                if ($recon) {
                    mysql_select_db(DB_SERVER_DATABASE, $recon);

//Get all vehicle years
                    $yquery = "select distinct v.Year from vehicles v order by v.Year desc";
                    $yresult = mysql_query($yquery, $recon);
                    $index = 0;
                    $allyears = array();

                    while ($yrow = mysql_fetch_array($yresult)) {
                        $allyears[$index] = $yrow[0];
                        $index++;
                    }

//Get all vehicle makes
                    $mquery = "select distinct m.MakeID, m.Name from vehicles v, makes m where m.MakeID = v.MakeID and v.Year = '" . $vspdata[$current]['Year'] . "' order by 2";
                    $result = mysql_query($mquery, $recon);

                    $allmakes = array();
                    while ($result && $mrow = mysql_fetch_array($result)) {
                        $allmakes[] = $mrow;
                    }

//Get all vehicle model
                    $modquery = "select distinct v.Model from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = " . $vspdata[$current]['Make'] . " and v.Year = '" . $vspdata[$current]['Year'] . "' order by 1";
                    $result = mysql_query($modquery, $recon);

                    $allmodels = array();
                    while ($result && $row = mysql_fetch_array($result)) {
                        $allmodels[] = $row[0];
                    }

//Get all vehicle Style
                    $styquery = "select distinct v.Style, v.VehicleID from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = " . $vspdata[$current]['Make'] . " and v.Year = '" . $vspdata[$current]['Year'] . "' and v.Model = '" . $vspdata[$current]['Model'] . "' order by 1";
                    $result = mysql_query($styquery, $recon);

                    $allstyles = array();
                    $k = 0;
                    while ($result && $strow = mysql_fetch_array($result)) {
                        if ($k == 0) {
                            $vehicleid = $strow['VehicleID'];
                        } else {
                            $k = 1;
                        }
                        $allstyles[] = $strow;
                    }

//Update type of the vehicle based on Vehicle ID
                    $query = "select Type from vehicles where VehicleID = '" . $vehicleid . "'";
                    $result = mysql_query($query, $recon);
                    while ($result && $row = mysql_fetch_array($result)) {
                        $vehicletype = $row['Type'];
                    }

                    $specific = $vspdata[$current]['Specific'];
                    $drivetrain = $vspdata[$current]['DriveTrain'];
                    $transmission = $vspdata[$current]['Transmission'];
                    $mileagefrom = $vspdata[$current]['MileageFrom'];
                    $mileageto = $vspdata[$current]['MileageTo'];
                    $mileageceiling = $vspdata[$current]['MileageCeiling'];
                    $extlike = $vspdata[$current]['ExtLike'];
                    $extdislike = $vspdata[$current]['ExtDislike'];
                    $intlike = $vspdata[$current]['IntLike'];
                    $intdislike = $vspdata[$current]['IntDisike'];
                    $budgetfrom = $vspdata[$current]['BudgetFrom'];
                    $budgetto = $vspdata[$current]['BudgetTo'];
                    $borrowmaxpayment = $vspdata[$current]['BorrowMaxPayment'];
                    $borrowdownpayment = $vspdata[$current]['BorrowDownPayment'];
                    $vehicleneed = $vspdata[$current]['VehicleNeed'];

//What do you prefer start
                    $frontstype = $vspdata[$current]['FrontSType'];
                    $bedtype = $vspdata[$current]['BedType'];
                    $leather = $vspdata[$current]['Leather'];
                    $heatedseat = $vspdata[$current]['HeatedSeat'];
                    $navigation = $vspdata[$current]['Navigation'];
                    $sunroof = $vspdata[$current]['SunRoof'];
                    $alloywheels = $vspdata[$current]['AlloyWheels'];
                    $rearwindow = $vspdata[$current]['RearWindow'];
                    $bedliner = $vspdata[$current]['BedLiner'];
                    $entertainmentsystem = $vspdata[$current]['EntertainmentSystem'];
                    $thirdrs = $vspdata[$current]['ThirdRD'];
                    $crow = $vspdata[$current]['CRow'];
                    $prhatch = $vspdata[$current]['PRHatch'];
                    $backupcamera = $vspdata[$current]['BackupCamera'];
                    $tpackage = $vspdata[$current]['TPackage'];
                    $adminflag = $vspdata[$current]['adminflag'];
//What do you prefer end
                    ?>	
                    <div class="grideightgrey" style="padding: 0px !important;">
                        <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;"></p>
                        <input type="hidden" id="vehicletype<?php echo $current ?>" name="vehicletype<?php echo $current ?>" value="<?php echo $vehicletype; ?>" />
                        <input type="hidden" id="spdetailid<?php echo $current ?>" name="spdetailid<?php echo $current ?>" value="<?php echo $vspdata[$current]['SearchPlanDetailID']; ?>" />
                        <input type="hidden" id="specific<?php echo $current ?>" name="specific<?php echo $current ?>" value="<?php echo $specific; ?>" />
                        <div class="grideight" style="width: 95%; margin-top:-5px; margin-bottom: 0px;">
                            <table class="table">
        <?php if ($adminflag == 1) { ?>
                                    <td colspan="2" align="center" class="assessment_insidsales"><strong class="assessment_inside"> Added By Sales Rep  </strong></td>
                                <?php } ?>
                                <tr id="selectyear<?php echo $current ?>" <?php if ($specific == 1) { ?>style="display:none;"<?php } ?>>
                                    <td style="width:30%"><strong>Year</strong></td>
                                    <td align="left" style="width:40%">
                                        <select id="yearlist<?php echo $current ?>" name="yearlist<?php echo $current ?>" onchange="yearchanged('<?php echo $current ?>')">
                                        <?php
                                        $count = count($allyears);
                                        for ($i = 0; $i < $count; $i++) {
                                            ?>
                                                <option value="<?php echo $allyears[$i] ?>" <?php if ($vspdata[$current]['Year'] == $allyears[$i]) { ?> selected="selected"<?php } ?>><?php echo $allyears[$i]; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="textyear<?php echo $current ?>" <?php if ($specific == 0) { ?>style="display:none;"<?php } ?>>
                                    <td style="width:30%"><strong>Year</strong></td>
                                    <td style="width:40%" >
                                        <input type="text" name="yeartext<?php echo $current; ?>" id="yeartext<?php echo $current; ?>" value="<?php echo $vspdata[$current]['TextYear']; ?>" />
                                    </td>
                                </tr>
                                <tr id="selectmake<?php echo $current ?>" <?php if ($specific == 1) { ?>style="display:none;"<?php } ?>>
                                    <td><strong>Make</strong></td>
                                    <td align="left" >
                                        <select id="makelist<?php echo $current ?>" name="makelist<?php echo $current ?>" onchange="makechanged('<?php echo $current ?>')">
        <?php
        $count = count($allmakes);
        for ($i = 0; $i < $count; $i++) {
            ?>
                                                <option value="<?php echo $allmakes[$i]['MakeID'] ?>" <?php if ($vspdata[$current]['Make'] == $allmakes[$i]['MakeID']) { ?> selected="selected"<?php } ?>><?php echo $allmakes[$i]['Name']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td> 
                                </tr>
                                <tr id="textmake<?php echo $current ?>" <?php if ($specific == 0) { ?>style="display:none;"<?php } ?>>
                                    <td ><strong>Make</strong></td>
                                    <td align="left" >
                                        <input type="text" name="maketext<?php echo $current; ?>" id="maketext<?php echo $current; ?>" value="<?php echo $vspdata[$current]['TextMake']; ?>" />
                                    </td>
                                </tr>
                                <tr id="selectmodel<?php echo $current ?>" <?php if ($specific == 1) { ?>style="display:none;"<?php } ?>>
                                    <td><strong>Model</strong></td>
                                    <td align="left" >
                                        <select id="modellist<?php echo $current ?>" name="modellist<?php echo $current ?>" onchange="modelchanged('<?php echo $current ?>')">
        <?php
        $count = count($allmodels);
        for ($i = 0; $i < $count; $i++) {
            ?>
                                                <option value="<?php echo $allmodels[$i] ?>" <?php if ($vspdata[$current]['Model'] == $allmodels[$i]) { ?> selected="selected"<?php } ?>><?php echo $allmodels[$i]; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="textmodel<?php echo $current ?>" <?php if ($specific == 0) { ?>style="display:none;"<?php } ?>>
                                    <td ><strong>Model</strong></td>
                                    <td align="left" >
                                        <input type="text" name="modeltext<?php echo $current; ?>" id="modeltext<?php echo $current; ?>" value="<?php echo $vspdata[$current]['TextModel']; ?>" />
                                    </td>
                                </tr>
                                <tr id="selectstyle<?php echo $current ?>" <?php if ($specific == 1) { ?>style="display:none;"<?php } ?>>
                                    <td><strong>Style</strong></td>
                                    <td align="left" >                                           
                                        <select id="stylelist<?php echo $current ?>" name="stylelist<?php echo $current ?>" onchange="stylechanged('<?php echo $current ?>')">
        <?php
        $count = count($allstyles);
        for ($i = 0; $i < $count; $i++) {
            ?>
                                                <option value="<?php echo $allstyles[$i]['VehicleID'] . ';' . $allstyles[$i]['Style']; ?>" <?php if ($vspdata[$current]['VehicleID'] . ';' . $vspdata[$current]['Style'] == $allstyles[$i]['VehicleID'] . ';' . $allstyles[$i]['Style']) { ?> selected="selected"<?php } ?>><?php echo $allstyles[$i]['Style']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="textstyle<?php echo $current ?>" <?php if ($specific == 0) { ?>style="display:none;"<?php } ?>>
                                    <td ><strong>Style</strong></td>
                                    <td align="left" >
                                        <input type="text" name="styletext<?php echo $current; ?>" id="styletext<?php echo $current; ?>" value="<?php echo $vspdata[$current]['TextStyle']; ?>" />
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr id="specificsection<?php echo $current ?>" <?php if ($specific == 1) { ?>style="display:none;"<?php } ?>><td colspan="2"><strong>Not seeing the vehicle you want? <span style="cursor:pointer;color:blue;" onclick="javascript: specific('<?php echo $current ?>', 1)" >Click here</span>.</strong></td></tr>
                                <tr id="vehiclesection<?php echo $current ?>" <?php if ($specific == 0) { ?>style="display:none;"<?php } ?>><td colspan="2"><strong>Want to select from the list? <span style="color:blue;cursor:pointer;" onclick="javascript: specific('<?php echo $current ?>', 0)" >Click here</span>.</strong></td></tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td><strong>Other styles (or packages) you will consider</strong></td>
                                    <td align="left" >
                                        <input type="text" name="otherstyle<?php echo $current ?>" id="otherstyle<?php echo $current ?>" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['OtherStyle']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Other year models you will consider:</strong></td>
                                    <td align="left" >
                                        <input type="text" name="otheryear<?php echo $current ?>" id="otheryear<?php echo $current ?>" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['OtherYear']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>How soon do you need a vehicle?</strong></td>
                                    <td align="left" >
                                        <select name="vehicleneed<?php echo $current ?>" id="vehicleneed<?php echo $current ?>">
                                            <option value="Now" <?php if ($vehicleneed == 'Now') { ?> selected="selected" <?php } ?>>Now!</option>
                                            <option value="1-2 weeks" <?php if ($vehicleneed == '1-2 weeks') { ?> selected="selected" <?php } ?>>1-2 weeks</option>
                                            <option value="3 weeks is ok" <?php if ($vehicleneed == '3 weeks is ok') { ?> selected="selected" <?php } ?>>3 weeks is ok</option>
                                            <option value="A month or more is ok" <?php if ($vehicleneed == 'A month or more is ok') { ?> selected="selected" <?php } ?>>A month or more is ok</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr><td colspan="2"><strong class="assessment_insideother">Mileage:</strong></td></tr>
                                <tr>
                                    <td><strong>Mileage you are hoping for</strong></td>
                                    <td align="left" width="10%">
                                        <select name="mileagefrom<?php echo $current ?>" id="mileagefrom<?php echo $current ?>" class="width103">
                                            <option value="Flexible"<?php if ($mileagefrom == 'Small') { ?> selected="selected" <?php } ?>>Flexible</option>
        <?php for ($i = 10000; $i <= 200000; $i = $i + 5000) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($i == $mileagefrom) { ?> selected="selected" <?php } ?>><?php echo number_format($i); ?></option>
                                            <?php } ?>
                                        </select> TO  
                                        <select name="mileageto<?php echo $current ?>" id="mileageto<?php echo $current ?>" class="width100">
                                            <option value="Flexible"<?php if ($mileageto == 'Small') { ?> selected="selected" <?php } ?>>Flexible</option>
        <?php for ($i = 10000; $i <= 200000; $i = $i + 5000) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($i == $mileageto) { ?> selected="selected" <?php } ?>><?php echo number_format($i); ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>

                                </tr>
                                <tr>
                                    <td><strong>Mileage Ceiling</strong></td>
                                    <td align="left" >
                                        <select name="mileageceiling<?php echo $current ?>" id="mileageceiling<?php echo $current ?>">
                                            <option value="Flexible"<?php if ($mileageceiling == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
        <?php for ($i = 10000; $i <= 200000; $i = $i + 5000) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($i == $mileageceiling) { ?> selected="selected" <?php } ?>><?php echo number_format($i); ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Transmission</strong></td>
                                    <td align="left" >
                                        <select name="transmission<?php echo $current ?>" id="transmission<?php echo $current ?>">
                                            <option value="Automatic"<?php if ($transmission == 'Automatic') { ?> selected="selected" <?php } ?>>Automatic</option>
                                            <option value="Manual"<?php if ($transmission == 'Manual') { ?> selected="selected" <?php } ?>>Manual</option>
                                            <option value="Flexible"<?php if ($transmission == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Drive Train</strong></td>
                                    <td align="left" >
                                        <select name="drivetrain<?php echo $current ?>" id="drivetrain<?php echo $current ?>">
        <?php if ($vehicletype == 'Auto') { ?>
                                                <option value="Front Wheel Drive"<?php if ($drivetrain == 'Front Wheel Drive') { ?> selected="selected" <?php } ?>>Front Wheel Drive</option>
                                                <option value="Rear Wheel Drive"<?php if ($drivetrain == 'Rear Wheel Drive') { ?> selected="selected" <?php } ?>>Rear Wheel Drive</option>
                                                <option value="Flexible"<?php if ($drivetrain == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
        <?php } elseif ($vehicletype == 'Minivan' || $vehicletype == 'MiniVan') { ?>
                                                <option value="Front Wheel Drive"<?php if ($drivetrain == 'Front Wheel Drive') { ?> selected="selected" <?php } ?>>Front Wheel Drive</option>
                                                <option value="4wd"<?php if ($drivetrain == '4wd') { ?> selected="selected" <?php } ?>>4wd</option>
                                                <option value="Flexible"<?php if ($drivetrain == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
        <?php } elseif ($vehicletype == 'SUV' || $vehicletype == 'Pickup') { ?>
                                                <option value="2wd"<?php if ($drivetrain == '2wd') { ?> selected="selected" <?php } ?>>2wd</option>
                                                <option value="4wd"<?php if ($drivetrain == '4wd') { ?> selected="selected" <?php } ?>>4wd</option>
                                                <option value="Flexible"<?php if ($drivetrain == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
        <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td><strong>Exterior colors you like</strong></td>
                                    <td align="left" >
                                        <input type="text" name="extlike<?php echo $current ?>" id="extlike<?php echo $current ?>" value="<?php echo $extlike ?>" placeholder="Text box">
                                    </td>

                                </tr>


                                <tr><td ><strong>Interior colors you like</strong></td>
                                    <td align="left" >
                                        <input type="text" name="intlike<?php echo $current ?>" id="intlike<?php echo $current ?>" value="<?php echo $intlike ?>" placeholder="Text box">
                                    </td>
                                </tr>				

        <!--                                    <tr><td colspan="2">&nbsp;</td></tr>
        <tr><td colspan="2"><strong class="assessment_insideother">If Borrowing:</strong></td></tr>
        <tr>
        <td><strong>Maximum payment is</strong></td>
        <td align="left" >$
        <select name="borrowmaxpayment<?php echo $current ?>" id="borrowmaxpayment<?php echo $current ?>" >
        <option value="Not Applicable" <?php if ($borrowmaxpayment == 'Not Applicable') { ?> selected="selected" <?php } ?>>Not Applicable</option>
        <option value="Not Borrowing" <?php if ($borrowmaxpayment == 'Not Borrowing') { ?> selected="selected" <?php } ?>>Not Borrowing</option>
        <option value="Not Sure"<?php if ($borrowmaxpayment == 'Not Sure') { ?> selected="selected" <?php } ?>>Not Sure</option>
        <?php for ($i = 100; $i <= 2000; $i = $i + 10) { ?>
            <option value="<?php echo $i; ?>" <?php if ($i == $borrowmaxpayment) { ?> selected="selected" <?php } ?>><?php echo number_format($i); ?></option>
                                <?php } ?>
        </select>
        </td>
        </tr>
        <tr>
        <td><strong>Down payment is</strong></td>
        <td align="left" >$
        <select name="borrowdownpayment<?php echo $current ?>" id="borrowdownpayment<?php echo $current ?>" >
        <option value="Not Applicable" <?php if ($borrowdownpayment == 'Not Applicable') { ?> selected="selected" <?php } ?>>Not Applicable</option>
        <option value="Not Borrowing" <?php if ($borrowdownpayment == 'Not Borrowing') { ?> selected="selected" <?php } ?>>Not Borrowing</option>
        <option value="Not Sure" <?php if ($borrowdownpayment == 'Not Sure') { ?> selected="selected" <?php } ?>>Not Sure</option>
        <?php for ($i = 100; $i <= 2000; $i = $i + 10) { ?>
            <option value="<?php echo $i; ?>" <?php if ($i == $borrowdownpayment) { ?> selected="selected" <?php } ?>><?php echo number_format($i); ?></option>
                                <?php } ?> 
        </select>
        </td>
        </tr>						
        <tr><td colspan="2">&nbsp;</td></tr>-->

                                <tr><td colspan="2"><strong class="assessment_insideother">Do you prefer:</strong></td></tr>
                                <tr id="trfrontstype<?php echo $current ?>" <?php if ($vehicletype != 'Pickup') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Front Seat Type</strong></td>
                                    <td align="left" >
                                        <select name="frontstype<?php echo $current ?>" id="frontstype<?php echo $current ?>">
                                            <option value="Standard Bench" <?php if ($frontstype == 'Standard Bench') { ?> selected="selected" <?php } ?>>Standard Bench</option>
                                            <option value="Bench Warmrest" <?php if ($frontstype == 'Bench Warmrest') { ?> selected="selected" <?php } ?>>Bench Warmrest</option>
                                            <option value="Bucket Seats" <?php if ($frontstype == 'Bucket Seats') { ?> selected="selected" <?php } ?>>Bucket Seats</option>
                                            <option value="Flexible" <?php if ($frontstype == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trbedtype<?php echo $current ?>" <?php if ($vehicletype != 'Pickup') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Bed Type</strong></td>
                                    <td align="left" >
                                        <select name="bedtype<?php echo $current ?>" id="bedtype<?php echo $current ?>">
                                            <option value="Short Bed (about 6 ft.)" <?php if ($bedtype == 'Short Bed (about 6 ft.)') { ?> selected="selected" <?php } ?>>Short Bed (about 6 ft.)</option>
                                            <option value="Long Bed (about 8 ft.)" <?php if ($bedtype == 'Long Bed (about 8 ft.)') { ?> selected="selected" <?php } ?>>Long Bed (about 8 ft.)</option>
                                            <option value="Extra Long Bed (about 10 ft.)" <?php if ($bedtype == 'Extra Long Bed (about 10 ft.)') { ?> selected="selected" <?php } ?>>Extra Long Bed (about 10 ft.)</option>
                                            <option value="Mini Bed (about 4 ft.)" <?php if ($bedtype == 'Mini Bed (about 4 ft.)') { ?> selected="selected" <?php } ?>>Mini Bed (about 4 ft.)</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Leather</strong></td>
                                    <td align="left" >
                                        <select name="leather<?php echo $current ?>" id="leather<?php echo $current ?>">
                                            <option value="Yes" <?php if ($leather == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($leather == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($leather == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($leather == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Heated Seats</strong></td>
                                    <td align="left" >
                                        <select name="heatedseat<?php echo $current ?>" id="heatedseat<?php echo $current ?>">
                                            <option value="Yes" <?php if ($heatedseat == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($heatedseat == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($heatedseat == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($heatedseat == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Navigation</strong></td>
                                    <td align="left" >
                                        <select name="navigation<?php echo $current ?>" id="navigation<?php echo $current ?>">
                                            <option value="Yes" <?php if ($navigation == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($navigation == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($navigation == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($navigation == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Sunroof</strong></td>
                                    <td align="left" >
                                        <select name="sunroof<?php echo $current ?>" id="sunroof<?php echo $current ?>">
                                            <option value="Yes" <?php if ($sunroof == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($sunroof == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($sunroof == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($sunroof == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Alloy Wheels</strong></td>
                                    <td align="left" >
                                        <select name="alloywheels<?php echo $current ?>" id="alloywheels<?php echo $current ?>">
                                            <option value="Yes" <?php if ($alloywheels == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($alloywheels == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($alloywheels == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($alloywheels == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trrearwindow<?php echo $current ?>" <?php if ($vehicletype != 'Pickup') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Rear Sliding Window</strong></td>
                                    <td align="left" >
                                        <select name="rearwindow<?php echo $current ?>" id="rearwindow<?php echo $current ?>">
                                            <option value="Yes" <?php if ($rearwindow == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($rearwindow == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($rearwindow == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($rearwindow == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trbedliner<?php echo $current ?>" <?php if ($vehicletype != 'Pickup') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Bed Liner</strong></td>
                                    <td align="left" >
                                        <select name="bedliner<?php echo $current ?>" id="bedliner<?php echo $current ?>">
                                            <option value="Yes" <?php if ($bedliner == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($bedliner == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($bedliner == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($bedliner == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trentertainmentsystem<?php echo $current ?>" <?php if ($vehicletype == 'Pickup' || $vehicletype == 'Auto') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Entertainment System</strong></td>
                                    <td align="left" >
                                        <select name="entertainmentsystem<?php echo $current ?>" id="entertainmentsystem<?php echo $current ?>">
                                            <option value="Yes" <?php if ($entertainmentsystem == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($entertainmentsystem == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($entertainmentsystem == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($entertainmentsystem == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trthirdrs<?php echo $current ?>" <?php if ($vehicletype != 'SUV') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Third Row Seating</strong></td>
                                    <td align="left" >
                                        <select name="thirdrs<?php echo $current ?>" id="thirdrs<?php echo $current ?>">
                                            <option value="Yes" <?php if ($thirdrs == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($thirdrs == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($thirdrs == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($thirdrs == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trcrow<?php echo $current ?>" <?php if ($vehicletype != 'SUV') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Captain chairs center row</strong></td>
                                    <td align="left" >
                                        <select name="crow<?php echo $current ?>" id="crow<?php echo $current ?>">
                                            <option value="Yes" <?php if ($crow == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($crow == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($crow == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($crow == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trprhatch<?php echo $current ?>" <?php if ($vehicletype == 'Pickup' || $vehicletype == 'Auto') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Power Rear Hatch</strong></td>
                                    <td align="left" >
                                        <select name="prhatch<?php echo $current ?>" id="prhatch<?php echo $current ?>">
                                            <option value="Yes" <?php if ($prhatch == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($prhatch == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($prhatch == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($prhatch == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trbackupcamera<?php echo $current ?>" <?php if ($vehicletype != 'Auto') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Backup Camera</strong></td>
                                    <td align="left" >
                                        <select name="backupcamera<?php echo $current ?>" id="backupcamera<?php echo $current ?>">
                                            <option value="Yes" <?php if ($backupcamera == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($backupcamera == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($backupcamera == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($backupcamera == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trtpackage<?php echo $current ?>" <?php if ($vehicletype == 'Minivan' || $vehicletype == 'MiniVan' || $vehicletype == 'Auto') { ?> style="display:none;" <?php } ?>>
                                    <td><strong>Tow package</strong></td>
                                    <td align="left" >
                                        <select name="tpackage<?php echo $current ?>" id="tpackage<?php echo $current ?>">
                                            <option value="Yes" <?php if ($tpackage == 'Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                            <option value="No" <?php if ($tpackage == 'No') { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="Would really like to have" <?php if ($tpackage == 'Would really like to have') { ?> selected="selected" <?php } ?>>Would really like to have</option>
                                            <option value="Flexible" <?php if ($tpackage == 'Flexible') { ?> selected="selected" <?php } ?>>Flexible</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td><strong>Notes from Client</strong></td>
                                    <td align="left" >
                                        <input type="text" name="clientnote<?php echo $current ?>" id="clientnote<?php echo $current ?>" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['ClientNote']; ?>">
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr><td colspan="2" align="center">
                                        <span style="width: 100%">
                                            <button type="submit" class="med"><nobr>SUBMIT THE REQUEST</nobr></button>
                                            &nbsp;<strong>OR</strong>&nbsp; 
                                            <button type="button" class="blueonwhitehideshow showrequirfield" myhidevalue="<?php echo $current ?>" id="showrequirfield"><nobr>Give us a little more detail</nobr></button>
                                        </span>
                                    </td></tr> 
                            </table>  
                            <table class="table" id="notshowfields<?php echo $current ?>" style="display: none;">


                                <tr><td colspan="2" class="assessment_insidetd_green" ><strong class="assessment_inside">NONE OF THESE FIELDS ARE REQUIRED</strong></td></tr>
                                <tr><td style="width:30%"><strong>Exterior colors you don't like </strong></td>
                                    <td align="left" style="width:40%" >

                                        <input type="text" name="extdislike<?php echo $current ?>" id="extdislike<?php echo $current ?>" value="<?php echo $extdislike ?>" placeholder="Text box">
                                    </td>

                                </tr>


                                <tr><td ><strong>Interior colors you don't like</strong></td>
                                    <td align="left" >	
                                        <input type="text" name="intdislike<?php echo $current ?>" id="intdislike<?php echo $current ?>" value="<?php echo $intdislike ?>" placeholder="Text box">
                                    </td>
                                </tr>




                                <tr>
                                    <td><strong>Must Have</strong></td>
                                    <td align="left" >
                                        <input type="text" name="musthave<?php echo $current ?>" id="musthave<?php echo $current ?>" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['MustHave']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Would really like to have</strong></td>
                                    <td align="left" >
                                        <input type="text" name="reallyhave<?php echo $current ?>" id="reallyhave<?php echo $current ?>" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['ReallyHave']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Flexible On</strong></td>
                                    <td align="left" >
                                        <input type="text" name="flexible<?php echo $current ?>" id="flexible<?php echo $current ?>" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['Flexible']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Do not want</strong></td>
                                    <td align="left" >
                                        <input type="text" name="notwant<?php echo $current ?>" id="notwant<?php echo $current ?>" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['NotWant']; ?>">
                                    </td>
                                </tr>

        <?php if (($vspdata[$current]['seller_review'] != "") && ($vspdata[$current]['plan_rating'] != "")) { ?>
                                    <td colspan="2" class="assessment_insidetd"><strong class="assessment_inside"> Sales Rep Review::  </strong></td>
                                    <?php if ($vspdata[$current]['seller_review']) { ?>
                                        <tr>
                                            <td><strong>Reiew</strong></td>
                                            <td align="left" >
                <?php echo $vspdata[$current]['seller_review']; ?>
                                            </td>
                                        </tr>
            <?php } ?>
                                    <?php if ($vspdata[$current]['plan_rating']) { ?>
                                        <tr>
                                            <td><strong>Plan Rating By Sale Person</strong></td>
                                            <td align="left" >
                <?php echo $vspdata[$current]['plan_rating']; ?>
                                            </td>
                                        </tr>
            <?php } ?>
                                <?php } ?>  
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr><td colspan="2" align="center">
                                        <span style="width: 100%">
                                            <button type="submit" class="med"><nobr>SUBMIT THE REQUEST</nobr></button>                                           
                                        </span>
                                </td></tr>                                         
                            </table>
                        </div><!-- end greyeight-->
                    </div><!-- grid eight container -->
                    <br clear="all" />
        <?php
        mysql_close($recon);
    }
}
?>
<!--            <div class="grideightgrey">
                <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;"></p>
                <div class="grideight" style="width: 95%; margin-top:-5px; margin-bottom: 0px;">                    
                    <p><button type="submit" class="med"><nobr>SUBMIT THE REQUEST</nobr></button></p>                    
                    <div id="dialog" title=" " style="display: none;">
                        <textarea name="notetext" id="notetext"  style="resize:vertical;width:100%;height:100%"></textarea>
                    </div>
                </div>
            </div>-->
 <input type="hidden" name="number" id="number" value="<?php echo $total; ?>" />
        </form>
    </div><!-- end grideightgrey-->

    <style>
        .assessment_insidetd{
            background: none repeat scroll 0% 0% gray; 
            color: white;
        }
        .assessment_insidsales{
            background: none repeat scroll 0% 0% #85c11b; 
            color: white;
        }


    </style>
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
