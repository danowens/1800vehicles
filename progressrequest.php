<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 4;
    $_SESSION['substate'] = 3;
    $_SESSION['titleadd'] = 'Progress Request';

    if(isset($_POST['PostBack']) && ($_POST['PostBack'] == 2))
    {
        $userid = $_SESSION['userid'];
        $marketneedid = $_SESSION['marketneedid'];
        $salesrep = getsalesrep($userid, $marketneedid);
        if($salesrep > 0)
        {
            posttodashboardnodb($userid, $salesrep, '<a href="'.WEB_SERVER_NAME.'salesrepactions.php?ForUserID='.$userid.'&MarketNeedID='.$marketneedid.'">'.$firstname.' '.$lastname.'</a> requested a Progress Update.');
            posttodashboardnodb($userid, $userid, 'requested a Progress Request.');
            //$con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            //if($con)
            //{
            //    mysql_select_db(DB_SERVER_DATABASE, $con);

            //    $query = "insert into messages (Created, Message, UserFromID, UserToID) values ('".date_at_timezone('Y-m-d H:i:s', 'EST')."','";
            //    $query .= '<a href="'.WEB_SERVER_NAME.'salesrepactions.php?ForUserID='.$userid.'&MarketNeedID='.$marketneedid.'">'.$firstname.'</a> requested a Progress Update.';
            //    $query .= "',".$userid.",".$salesrep.")";
            //    mysql_query($query, $con);

            //    mysql_close($con);
            //}
        }
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 300px;">Request Progress Update</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-left:10px; margin-top:0; width:590px;">
<?php
    if(isset($_POST['PostBack']) && ($_POST['PostBack'] == 1)) echo '<p style="color:#757575; font-weight:bold; font-size:14px;">Are you sure you want to requst a progress update?</p>';
    elseif(isset($_POST['PostBack']) && ($_POST['PostBack'] == 2)) echo '<p style="color:#757575; font-weight:bold; font-size:14px;">Your Sales Representative has been alerted and you should receive a response soon. Thanks!</p>';
    else echo '<p style="color:#757575; font-weight:bold; font-size:14px;">Your sales rep will be immediately notified that a progress update has been requested.  He or she will respond with an update in a timely manner. </p>';
?>
                <br clear="all" />
                <table border="0" width="236">
                    <tr>
                        <td width="222" align="left">
                            <form action="progressrequest.php" method="post">
<?php
    if(isset($_POST['PostBack']) && ($_POST['PostBack'] == 1)) echo '<input type="hidden" value="2" name="PostBack" />';
    else echo '<input type="hidden" value="1" name="PostBack" />';
    if(!isset($_POST['PostBack']) || ($_POST['PostBack'] == 1)) echo '<button type="submit" value="" class="med">REQUEST PROGRESS UPDATE</button>';
?>
                            </form>
                        </td>
                    </tr>
                </table>
                <br clear="all" />
                <br />
            </div><!-- end grid eight -->
        </div><!-- end grideightgrey-->
    </div><!-- end grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
