<?php 
    require("globals.php"); 
    require_once(WEB_ROOT_PATH.'common/functions/usernavfunctions.php');
    require_once(WEB_ROOT_PATH.'common/functions/franchiseefunctions.php');
    require_once(WEB_ROOT_PATH.'common/functions/string.php');

    if(ismorethancust($_SESSION['userid']) == 'true')
        $_SESSION['state'] = 5;
    else
    $_SESSION['state'] = currentstep();
    $_SESSION['substate'] = 99;
    $_SESSION['titleadd'] = 'My Dashboard';
  
    //print_r($_SESSION);
    
    // Get some of the state of the marketneed and user variables we will need...
    $state = $_SESSION['state'];
    $substate = $_SESSION['substate'];

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];
    if(isset($_POST['ChangeMarketNeedTo']))
    {
        $_SESSION['marketneedid'] = $_POST['ChangeMarketNeedTo'];
        $marketneedid = $_SESSION['marketneedid'];

        // Update Last Login Info...
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);
            $uquery = "update marketneeds set lastlogin = '".date_at_timezone('Y-m-d H:i:s','GMT')."' where marketneedid = ".$_SESSION['marketneedid'];
            mysql_query($uquery, $con);
            mysql_close($con);
        }
    }

    $ucurrentstep = currentstep();
    $usalesrep = getsalesrep($userid, $marketneedid);

    $ucust = getuserprofile($userid, 'Customer');

    $hasaccess = ismorethancust($userid);
    $uadmin = getuserprofile($userid, 'Administrator');
    $uterr  = getuserprofile($userid, 'Teritory Admin');
    $ufran  = getuserprofile($userid, 'Franchisee');
    $ugm    = getuserprofile($userid, 'General Manager');
    $uops   = getuserprofile($userid, 'Operations Manager');
    $ubuyer = getuserprofile($userid, 'Researcher');
    $usrep  = getuserprofile($userid, 'Sales Representative');

    if(isset($_SESSION['curadmintab']))
    {
        if(($_SESSION['curadmintab'] == 'admin') && !($uadmin == 'true'))
        {
            unset($_SESSION['curadmintab']);
        }
        if(($_SESSION['curadmintab'] == 'terrrep') && !(($uadmin == 'true') || ($uterr == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        if(($_SESSION['curadmintab'] == 'franchise') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'genman') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'opsman') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'authbuyer') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ubuyer == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'salesrep') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true') || ($usrep == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
    }

    if(isset($_SESSION['curadmintab']) && ($_SESSION['curadmintab'] != 'none')) $curadmintab = $_SESSION['curadmintab'];
    else
    {
        if($uadmin == 'true')
            $curadmintab = 'admin';
        elseif($uterr == 'true')
            $curadmintab = 'terrrep';
        elseif($ufran == 'true')
            $curadmintab = 'franchise';
        elseif($ugm == 'true')
            $curadmintab = 'genman';
        elseif($uops == 'true')
            $curadmintab = 'opsman';
        elseif($ubuyer == 'true')
            $curadmintab = 'authbuyer';
        elseif($usrep == 'true')
            $curadmintab = 'salesrep';
        else
            $curadmintab = 'none';

        $_SESSION['curadmintab'] = $curadmintab;
    }

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the current User Information...
        $query = 'select u.firstname, u.lastname, u.created, u.imagefile from users u where u.userid = '.$userid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $infirst = $row[0];
            $inlast = $row[1];
            $increated = $row[2];
            $inimage = $row[3];
        }

        // Get the Current Market Need Details...
        $query = 'select title, created from marketneeds where marketneedid = '.$marketneedid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $inmtitle = $row[0];
            $inmdate = $row[1];
        }

        // Get the Other Market Need Details...
        $query = 'select marketneedid, title, created, needscontact from marketneeds where active = 1 and showtouser = 1 and userid = '.$userid.' and marketneedid != '.$marketneedid.' order by created';
        $result = mysql_query($query, $con);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $inmnid[$index] = $row[0];
            $inmntitle[$index] = $row[1];
            $inmndate[$index] = $row[2];
            $inmnnc[$index] = $row[3];
            $index++;
        }

        // Get Messages to show on dashboard
        // TODO: limit messages to a reasonable number date descending
        //if(isset($marketneedid)) $query = "select created,message,fromid,msgid,fromtab from (select created 'created', message 'message', userfromid 'fromid', MessageID 'msgid',1 'fromtab' from messages where hidden=0 and ((usertoid = ".$userid." and marketneedid is null) or marketneedid = ".$marketneedid.") union all select created,updatetext,NULL, MarketUpdateID, 2 from marketupdates where hidden=0 and marketneedid = ".$marketneedid.") msglist order by 1 desc";
        //else $query = "select created 'created', message 'message', userfromid 'fromid', MessageID 'msgid',1 'fromtab' from messages where hidden=0 and usertoid = ".$userid." order by 1 desc";
        if(isset($marketneedid)) $query = "select created 'created', message 'message', userfromid 'fromid', messageid 'msgid',1 'fromtab' from messages where hidden=0 and ((usertoid = ".$userid." and marketneedid is null) or marketneedid = ".$marketneedid.") order by 1 desc";
        else $query = "select created 'created', message 'message', userfromid 'fromid', messageid 'msgid',1 'fromtab' from messages where hidden=0 and usertoid = ".$userid." order by 1 desc";
        $result = mysql_query($query, $con);
        $index = 0;
                $umsgcreated = array();
        while($result && $row = mysql_fetch_array($result))
        {
            $umsgcreated[$index] = $row[0];
            $umsgmessage[$index] = $row[1];
            $umsgfromid[$index] = $row[2];
            $umsgtabid[$index] = $row[3];
            $umsgfromtab[$index] = $row[4];
            $index++;
        }

        mysql_close($con);
    }

    // react to show all hidden button on salesrep dashboard
    if(isset($_POST['PostBack']) && isset($_POST['ShowAllHiddenCust']))
    {
        // list of market needs for this salesrep to show all of them to the rep all of them
        $needlist = getassignedmarketneeds($userid);
        if(strlen($needlist)>0)
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $sahquery = "update marketneeds set showtorep=1 where marketneedid in (".$needlist.")";
                mysql_query($sahquery, $con);

                mysql_close($con);
            }
        }
    }
    // react to show one hidden market need button on salesrep dashboard
    elseif(isset($_POST['PostBack']) && isset($_POST['ShowExperience']))
    {
        $needlist = $_POST['ShowExperience'];
        if(strlen($needlist)>0)
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $sahquery = "update marketneeds set showtorep=1 where marketneedid in (".$needlist.")";
                mysql_query($sahquery, $con);

                mysql_close($con);
            }
        }
    }
?>
<?php require("headerstart.php"); ?>
<script language="JavaScript">
/*
    var refreshinterval=30
    var displaycountdown="no"
    var starttime;
    var nowtime;
    var reloadseconds=0;
    var secondssinceloaded=0;

    function starttime()
    {
        starttime=new Date();
        starttime=starttime.getTime();
        countdown();
    }

    function countdown()
    {
        nowtime= new Date();
        nowtime=nowtime.getTime();
        secondssinceloaded=(nowtime-starttime)/1000;
        reloadseconds=Math.round(refreshinterval-secondssinceloaded);
        if(refreshinterval>=secondssinceloaded)
        {
            var timer=setTimeout("countdown()",1000);
            if(displaycountdown=="yes")
            {
                window.status="Page refreshing in "+reloadseconds+ " seconds";
            }
        }
        else
        {
            clearTimeout(timer);
            window.location.reload(true);
        }
    }

    window.onload=starttime;
*/

    function deleteempok()
    {
        var r=confirm("Are you sure you want to Remove the Employee from the Franchise?")
        if(r==true) return true;
        else return false;
    }

    // TODO: move to global functions
    // showhidediv = toggles a div open/closed and optionally changes the text of the activating element to match the next action
    // tdiv = the id of the div to show/hide
    // ttrigger = the id of the element triggering the action (optional)
    // Return = the converted string
    function showhidediv(tdiv, ttrigger)
    {
        var thediv = document.getElementById(tdiv);
        var thetrigger = document.getElementById(ttrigger);

        if (thediv.style.display == "block")
        {
            thediv.style.display = "none";
            thetrigger.innerHTML = "Show";
        }
        else
        {
            thediv.style.display = "block";
            thetrigger.innerHTML = "Hide";
        }
        return true;
    }

</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>


<div class="gridtwelve"></div>

<div id="content">
   
    
    <div class="grideightcontainer" style="width: 70%;"> 
       
<?php


    

    if($hasaccess == 'true')
    {
        echo '<div class="dashstrip" style="min-height: 0px; margin: 0 auto; padding-bottom: 10px;">';
        // Set up the Tab Menu for those with access to it...
        echo '<a name="admintab"></a><div id="droplinetabs" class="admintabs">';
        echo '<ul>';
        if($uadmin == 'true')
        {
            if($curadmintab == 'admin') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=admin"><span>Admin</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=admin"><span>Admin</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true'))
        {
            if($curadmintab == 'terrrep') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=terrrep"><span>Territory Rep</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=terrrep"><span>Territory Rep</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true'))
        {
            if($curadmintab == 'franchise') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=franchise"><span>Franchisee</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=franchise"><span>Franchisee</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true'))
        {
            if($curadmintab == 'genman') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=genman"><span>General Manager</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=genman"><span>General Manager</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true'))
        {
            if($curadmintab == 'opsman') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=opsman"><span>Operations Manager</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=opsman"><span>Operations Manager</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ubuyer == 'true'))
        {
            if($curadmintab == 'authbuyer') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=authbuyer"><span>Researcher</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=authbuyer"><span>Researcher</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true') || ($usrep == 'true'))
        {
            if($curadmintab == 'salesrep') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=salesrep"><span>Sales Rep</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=salesrep"><span>Sales Rep</span></a></li>';
        }
        echo '</ul>';
        echo '</div>';

        // Add the currently selected menu...
        if($curadmintab == 'admin')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight" style="width:930px;">';
            //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Watchlist Vehicles --</strong></p>';
            echo '<p>Coming Soon!</p>';
            echo '</div><!-- endgrideight-->';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'terrrep')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight">';
            //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Watchlist Vehicles --</strong></p>';
            //echo '<p>Coming Soon!</p>';
            // Get the ones not in a franchise yet for the Admin to see...
            if($uadmin == 'true')
            {
                $userlist = getnonfranunassignedleads($userid);
                if(strlen($userlist) > 0)
                {
                    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                    if($con)
                    {
                        mysql_select_db(DB_SERVER_DATABASE, $con);

                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '</div><!-- endgridfullgrey -->';
                        echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                        echo '<div class="grideight">';
                        echo '<h4 class="subhead">Unassigned No Franchisee Leads</h4>';
                        echo '<table class="table">';
                        echo '<tr style="color:#85c11b; font-size:15px;">';
                        echo '<td ><strong>Name (Email)</strong></td>';
                        echo '<td ><strong>Market Need</strong></td>';
                        echo '<td ><strong>Created</strong></td>';
                        echo '<td ><strong>Referred By</strong></td>';
                        echo '<td ><strong>Rep Name</strong></td>';
                        echo '<td ><strong>Actions</strong></td>';
                        echo '</tr>';

                        $query = "select u.userid, concat(u.firstname,' ',u.lastname,'<br/>(',u.email,')') AS fullname, u.referredby, u.repname, m.marketneedid, m.title, m.created from users u, marketneeds m where u.userid=m.userid and u.userid in (".$userlist.") order by u.lastupdated desc,2";
                        $result = mysql_query($query, $con);
                        while($result && $row = mysql_fetch_array($result))
                        {
                            $aquery = "select * from assignedreps where marketneedid = ".$row[4];
                            $aresult = mysql_query($aquery, $con);
                            if(!$arow = mysql_fetch_array($aresult))
                            {
                                echo '<tr valign="baseline">';
                                echo '<td><strong>'.$row[1].'</strong></td>';
                                echo '<td>'.$row[5].'</td>';
                                echo '<td>'.$row[6].'</td>';
                                echo '<td>'.$row[2].'</td>';
                                echo '<td>'.$row[3].'</td>';
                                echo '<td><form action="'.WEB_SERVER_NAME.'addassignedrep.php" method="post">';
                                echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                                echo '<input type="hidden" value="'.$row[4].'" name="AddMarketID" />';
                                echo '<input type="hidden" value="'.$row[0].'" name="ForUserID" />';
                                echo '<input type="hidden" value="'.$row[1].'" name="ForUserName" />';
                                echo '<input type="hidden" value="'.$row[5].'" name="ForUserNeed" />';
                                if(isset($row[2]) && strlen($row[2]) > 0) $iuserref = $row[2];
                                else $iuserref = 'None Specified';
                                echo '<input type="hidden" value="'.$iuserref.'" name="ForUserRef" />';
                                if(isset($row[3]) && strlen($row[3]) > 0) $iuserrep = $row[3];
                                else $iuserrep = 'None Specified';
                                echo '<input type="hidden" value="'.$iuserrep.'" name="ForUserRep" />';
                                echo '<button type="submit" value="" class="med">ASSIGN</button>';
                                echo '</form></td>';
                                echo '</tr>';
                            }
                        }

                        echo '</table>';
                        //echo '<br clear="all" />';
                        //echo '<button type="submit" value="" class="med" style="margin-top:10px;">DELETE</button>';
                        echo '</div><!-- endgrideight-->';
                    }

                    mysql_close($con);
                }
            }
            echo '</div><!-- endgrideight-->';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'franchise')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight" style="width:930px;">';

            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $query = "select fm.franchiseeid, f.name from franchiseemembers fm, franchisees f where userid=".$userid." and fm.franchiseeid=f.franchiseeid";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];
                    $ifranname = $row[1];

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>Franchise:'.$ifranname.'</strong></p>';
                    echo '<div class="grideight" style="width:930px;">';

                    // employees
                    echo '<h4 class="subhead">Employees</h4>';
                    //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Employees --</strong></p>';
                    echo '<table border="0" width="900" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:17px;">';
                    echo '<td width="300"><strong>Employee</strong></td>';
                    echo '<td width="300"><strong>Responsibilities</strong></td>';
                    echo '<td width="25" align="right"><strong>Actions</strong></td>';
                    echo '<td width="10" align="left"><strong>&nbsp;</strong></td>';
                    echo '</tr>';

                    // Profile Names
                    $iprofilename[1] = "Customer";
                    $iprofilename[2] = "Administrator";
                    $iprofilename[3] = "Territory&nbsp;Administrator";
                    $iprofilename[4] = "Franchisee";
                    $iprofilename[5] = "Operations&nbsp;Manager";
                    $iprofilename[6] = "General&nbsp;Manager";
                    $iprofilename[7] = "Authorized&nbsp;Buyer";
                    $iprofilename[8] = "Sales&nbsp;Representative";

                    $query = "select m.userid, concat(u.lastname,', ',u.firstname,' (',u.email,')') AS fullname, m.abletocontrol, group_concat(up.profileid)";
                    $query .= " from franchiseemembers m, users u, userprofiles up";
                    $query .= " where u.userid=m.userid and up.userid=u.userid and m.franchiseeid=".$ifranid." group by up.userid order by up.profileid, fullname";

                    //$query = "select m.UserID, concat(u.LastName,', ',u.FirstName,' (',u.Email,')') AS FullName from FranchiseeMembers m, Users u where u.UserId=m.UserID and m.FranchiseeID=".$ifranid." order by 2";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $ifuserid = $row[0];
                        $iffull = $row[1];
                        $ifcontrol = $row[2];
                        $ifprofiles = $row[3];

                        // TODO: roll this up into usernavfunctions.php (keep single code copy of the profiles table to avoid lookups)
                        $pieces = explode(',', $ifprofiles);
                        $count = count($pieces);
                        sort($pieces);
                        $ifroles = '';
                        for ($i = 0; $i < $count; $i++)
                        {
                            $ifroles .= $iprofilename[$pieces[$i]];
                            if ($i < ($count - 1)) $ifroles .= ', ';
                        }

                        {
                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$iffull.'</strong></td>';
                            echo '<td>'.$ifroles.'</td>';
                            echo '<td align="right"><form action="'.WEB_SERVER_NAME.'changeuserresp.php" method="post">';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="ChangeUserID" />';
                            echo '<input type="hidden" value="true" name="NoGMOption" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Employee Responsibilities" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form>';
                            echo '</td><td align="left">';
                            echo '<form action="'.WEB_SERVER_NAME.'delmember.php" method="post" onsubmit="javascript:return deleteempok();" >';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="MemberID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Employee from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }

                    echo '</table>';
                    echo '<form action="'.WEB_SERVER_NAME.'adduser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD NEW EMPLOYEE</button>';
                    echo '</form>';
                    echo '<form action="'.WEB_SERVER_NAME.'addreguser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD REGISTERED EMPLOYEE</button>';
                    echo '</form>';
                    echo '</div>';
                }
            }
/* 
            // employees
            echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Employees --</strong></p>';
            echo '<table border="0" width="900" cellpadding="3">';
            echo '<tr style="color:#85c11b; font-size:17px;">';
            echo '<td width="300"><strong>Employee</strong></td>';
            echo '<td width="300"><strong>Responsibilities</strong></td>';
            echo '<td width="25" align="right"><strong>Actions</strong></td>';
            echo '<td width="10" align="left"><strong>&nbsp;</strong></td>';
            echo '</tr>';
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            $ifranid = -1;
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                //$query = "select FranchiseeID from FranchiseeMembers where AbleToControl=1 and UserId=".$userid;
                $query = "select franchiseeid from franchiseemembers where userid=".$userid;
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];

                    $query = "select m.userid, concat(u.lastname,', ',u.firstname,' (',u.email,')') AS fullname from franchiseemembers m, users u where u.userid=m.userid and m.franchiseeid=".$ifranid." order by 2";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $ifuserid = $row[0];
                        $iffull = $row[1];

                        if($ifuserid != $userid)
                        {
                            $respstr = '';
                            if(getuserprofile($ifuserid, 'General Manager') == 'true') $respstr .= 'General Manager';
                            if(getuserprofile($ifuserid, 'Operations Manager') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Operations Manager';
                                else $respstr .= 'Operations Manager';
                            }
                            if(getuserprofile($ifuserid, 'Researcher') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Researcher';
                                else $respstr .= 'Researcher';
                            }
                            if(getuserprofile($ifuserid, 'Sales Representative') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Sales Representative';
                                else $respstr .= 'Sales Representative';
                            }
                            if(getuserprofile($ifuserid, 'Customer') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Customer';
                                else $respstr .= 'Customer';
                            }

                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$iffull.'</strong></td>';
                            echo '<td>'.$respstr.'</td>';
                            echo '<td align="right"><form action="changeuserresp.php" method="post">';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="ChangeUserID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Employee Responsibilities" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form></td align="left">';
                            echo '<td><form action="delmember.php" method="post">';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="MemberID" />';
                            echo '<input type="hidden" value="true" name="FromDash" />';
                            echo '<input type="hidden" value="pleasedo" name="runok" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Employee from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }
                }

                mysql_close($con);
            }
            echo '</table>';
            if($ifranid != -1)
            {
                echo '<form action="adduser.php" method="post">';
                echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD EMPLOYEE</button>';
                echo '</form>';
            }
            //echo $query;
*/
            echo '</div>';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'genman')
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $query = "select fm.franchiseeid, f.name from franchiseemembers fm, franchisees f where userid=".$userid." and fm.franchiseeid=f.franchiseeid";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];
                    $ifranname = $row[1];

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>Franchise:'.$ifranname.'</strong></p>';
                    echo '<div class="grideight" style="width:930px;">';

                    // employees
                    echo '<h4 class="subhead">Employees</h4>';
                    //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Employees --</strong></p>';
                    echo '<table border="0" width="900" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:17px;">';
                    echo '<td width="300"><strong>Employee</strong></td>';
                    echo '<td width="300"><strong>Responsibilities</strong></td>';
                    echo '<td width="25" align="right"><strong>Actions</strong></td>';
                    echo '<td width="10" align="left"><strong>&nbsp;</strong></td>';
                    echo '</tr>';

                    // Profile Names
                    $iprofilename[1] = "Customer";
                    $iprofilename[2] = "Administrator";
                    $iprofilename[3] = "Territory&nbsp;Administrator";
                    $iprofilename[4] = "Franchisee";
                    $iprofilename[5] = "Operations&nbsp;Manager";
                    $iprofilename[6] = "General&nbsp;Manager";
                    $iprofilename[7] = "Authorized&nbsp;Buyer";
                    $iprofilename[8] = "Sales&nbsp;Representative";

                    $query = "select m.userid, concat(u.lastname,', ',u.firstname,' (',u.email,')') AS fullname, m.abletocontrol, group_concat(up.profileid)";
                    $query .= " from franchiseemembers m, users u, userprofiles up";
                    $query .= " where u.userid=m.userid and up.userid=u.userid and m.franchiseeid=".$ifranid." group by up.userid order by up.profileid, fullname";

                    //$query = "select m.UserID, concat(u.LastName,', ',u.FirstName,' (',u.Email,')') AS FullName from FranchiseeMembers m, Users u where u.UserId=m.UserID and m.FranchiseeID=".$ifranid." order by 2";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $ifuserid = $row[0];
                        $iffull = $row[1];
                        $ifcontrol = $row[2];
                        $ifprofiles = $row[3];

                        // TODO: roll this up into usernavfunctions.php (keep single code copy of the profiles table to avoid lookups)
                        $pieces = explode(',', $ifprofiles);
                        $count = count($pieces);
                        sort($pieces);
                        $ifroles = '';
                        for ($i = 0; $i < $count; $i++)
                        {
                            $ifroles .= $iprofilename[$pieces[$i]];
                            if ($i < ($count - 1)) $ifroles .= ', ';
                        }

                        {
                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$iffull.'</strong></td>';
                            echo '<td>'.$ifroles.'</td>';
                            echo '<td align="right"><form action="'.WEB_SERVER_NAME.'changeuserresp.php" method="post">';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="ChangeUserID" />';
                            echo '<input type="hidden" value="true" name="NoGMOption" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Employee Responsibilities" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form>';
                            echo '</td><td align="left">';
                            echo '<form action="'.WEB_SERVER_NAME.'delmember.php" method="post" onsubmit="javascript:return deleteempok();" >';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="MemberID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Employee from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }

                    echo '</table>';
                    echo '<form action="'.WEB_SERVER_NAME.'adduser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD NEW EMPLOYEE</button>';
                    echo '</form>';
                    echo '<form action="'.WEB_SERVER_NAME.'addreguser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD REGISTERED EMPLOYEE</button>';
                    echo '</form>';
                    echo '</div>';
                    //echo '<br clear="all" />';

/*
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>My Employees</strong></p>';
                    echo '<table border="0" width="900" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:15px;">';
                    echo '<td width="45">&nbsp;</td>';
                    echo '<td width="126"><strong>NAME</strong></td>';
                    echo '<td width="153"><strong>ROLE</strong></td>';
                    echo '<td width="126"><strong>AUTO ASSIGN</strong></td>';
                    echo '<td width="232"><strong>ASSIGNMENT PRIVILEGES?</strong></td>';
                    echo '<td width="168"><strong>VIEW CUSTOMERS</strong></td>';
                    echo '</tr>';
                    echo '<tr valign="baseline">';
                    echo '<td width="45"><strong>';
                    echo '  <input name="" type="checkbox" value="" />';
                    echo '</strong></td>';
                    echo '<td width="126"><strong><a href="#">Esu Vee</a></strong></td>';
                    echo '<td width="153"><strong><a href="#">Sales Rep</a></strong></td>';
                    echo '<td width="126" align="center"><strong>';
                    echo '<input name="" type="radio" value="" />';
                    echo '</strong></td>';
                    echo '<td width="232" align="center"><strong>';
                    echo '<select name="AssignmentPrivileges">';
                    echo '<option value="AssignmentPrivileges">No</option>';
                    echo '</select>';
                    echo '</strong></td>';
                    echo '<td width="168" align="center"><strong><a href="#">View</a></strong></td>';
                    echo '</tr>';
                    echo '</table>';
                    echo '<br clear="all" />';
                    echo '<button type="submit" value="" class="med">SAVE CHANGES</button><br />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px;">DELETE</button>';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD EMPLOYEE/USER</button>';
                    echo '</div>';
                    echo '<br clear="all" />';
                    echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '</div><!-- endgrideight-->';
                    echo '</div><!-- endgridfullgrey -->';
*/
                }
                else
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<div class="grideight" style="width:930px;">';
                }
                mysql_close($con);
            }

            // unassigned leads in this franchise
            $userlist = getunassignedleads($userid, $ifranid);
            if(strlen($userlist) > 0)
            {
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    mysql_select_db(DB_SERVER_DATABASE, $con);

                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '</div><!-- endgridfullgrey -->';
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<h4 class="subhead">Unassigned Leads</h4>';
                    echo '<table width="900" border="0" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:15px;">';
                    echo '<td width="300"><strong>Name (Email)</strong></td>';
                    echo '<td width="300"><strong>Market Need</strong></td>';
                    echo '<td width="125"><strong>Referred By</strong></td>';
                    echo '<td width="125"><strong>Rep Name</strong></td>';
                    echo '<td width="10"><strong>Actions</strong></td>';
                    echo '</tr>';

                    $query = "select u.userid, concat(u.firstname,' ',u.lastname,' (',u.email,')') AS fullname, u.referredby, u.repname, m.marketneedid, m.title from users u, marketneeds m where u.userid=m.userid and u.userid in (".$userlist.") order by m.marketneedid desc";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $aquery = "select * from assignedreps where marketneedID = ".$row[4];
                        $aresult = mysql_query($aquery, $con);
                        if(!$aresult || !$arow = mysql_fetch_array($aresult))
                        {
                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$row[1].'</strong></td>';
                            echo '<td>'.$row[5].'</td>';
                            echo '<td>'.$row[2].'</td>';
                            echo '<td>'.$row[3].'</td>';
                            echo '<td><form action="'.WEB_SERVER_NAME.'addassignedrep.php" method="post">';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$row[4].'" name="AddMarketID" />';
                            echo '<input type="hidden" value="'.$row[0].'" name="ForUserID" />';
                            echo '<input type="hidden" value="'.$row[1].'" name="ForUserName" />';
                            echo '<input type="hidden" value="'.$row[5].'" name="ForUserNeed" />';
                            if(isset($row[2]) && strlen($row[2]) > 0) $iuserref = $row[2];
                            else $iuserref = 'None Specified';
                            echo '<input type="hidden" value="'.$iuserref.'" name="ForUserRef" />';
                            if(isset($row[3]) && strlen($row[3]) > 0) $iuserrep = $row[3];
                            else $iuserrep = 'None Specified';
                            echo '<input type="hidden" value="'.$iuserrep.'" name="ForUserRep" />';
                            echo '<button type="submit" value="" class="med">ASSIGN</button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }

                    echo '</table>';
                    //echo '<br clear="all" />';
                    //echo '<button type="submit" value="" class="med" style="margin-top:10px;">DELETE</button>';
                    echo '</div><!-- endgrideight-->';
                }

                mysql_close($con);
            }

            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'opsman')
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $query = "select fm.franchiseeid, f.name from franchiseemembers fm, franchisees f where userid=".$userid." and fm.franchiseeid=f.franchiseeid";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];
                    $ifranname = $row[1];

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>Franchise:'.$ifranname.'</strong></p>';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<h4 class="subhead">Purchased Vehicles In Process</h4>';

                    $query = "select count(*) from purchases p, specificvehicles s, assignedreps a, franchiseemembers f where p.completed=0 and s.specificvehicleid=p.specificvehicleid and s.marketneedid=a.marketneedid and a.userrepid=f.userid and f.franchiseeid=".$ifranid;
                    $result = mysql_query($query, $con);
                    if($result && $row = mysql_fetch_array($result)) $count = $row[0];
                    else $count = 0;

                    if($count > 0)
                    {
                        echo '<table width="900" border="0" cellpadding="3">';
                        echo '<tr style="color:#85c11b; font-size:15px;">';
                        echo '<td width="200"><strong>Year/Make/Model</strong></td>';
                        echo '<td width="200"><strong>Customer</strong></td>';
                        echo '<td width="200"><strong>Current Stage</strong></td>';
                        echo '<td width="200"><strong>Cost So Far</strong></td>';
                        echo '<td width="200"><strong>Bought On</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';

                        $query = "select v.year, m.name, v.model, u.firstname, u.lastname, p.purchaseid, p.boughton, p.status from purchases p, specificvehicles s, vehicledetails v, makes m, assignedreps a, franchiseemembers f, marketneeds n, users u where p.completed=0 and s.specificvehicleid=p.specificvehicleid and s.vehicledetailid = v.vehicledetailid and v.makeid = m.makeid and s.marketneedid = n.marketneedid and n.userid = u.userid and s.marketneedid = a.marketneedid and a.userrepid = f.userid and f.franchiseeid = ".$ifranid;
                        $result = mysql_query($query, $con);
                        while($result && $row = mysql_fetch_array($result))
                        {
                            echo '<tr style="color:#000000; font-size:15px;">';
                            echo '<td>'.$row[0].' '.$row[1].' '.$row[2].'</td>';
                            echo '<td>'.$row[3].' '.$row[4].'</td>';
                            echo '<td>';

                            $cquery = "select * from specificactions where purchaseid=".$row[5];
                            $cresult = mysql_query($cquery, $con);
                            if(!$crow = mysql_fetch_array($cresult)) echo 'Pending';
                            else
                            {
                                $sumquery = "select count(*) from specificactions sa, specificcategories sc, specificitems si where si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5];
                                $sumresult = mysql_query($sumquery);
                                if($sumresult && $sumrow = mysql_fetch_array($sumresult))
                                {
                                    $total = $sumrow[0];
                                    $sumquery = "select count(*) from specificactions sa, specificcategories sc, specificitems si where si.completed=1 and si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5];
                                    $sumresult = mysql_query($sumquery);
                                    if($sumrow = mysql_fetch_array($sumresult))
                                    {
                                        $completed = $sumrow[0];
                                        $sumquery = "select si.name from specificactions sa, specificcategories sc, specificitems si where si.completed=0 and si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5]." order by sc.displayorder asc, si.displayorder asc";
                                        $sumresult = mysql_query($sumquery);
                                        if($sumresult && $sumrow = mysql_fetch_array($sumresult))
                                        {
                                            $itemname = $sumrow[0];
                                            echo $itemname.' ['.$completed.' of '.$total.']';
                                        }
                                        else echo $completed.' of '.$total.' Completed';
                                    }
                                    else echo '0 of '.$total.' Completed';
                                }
                                else echo 'Pending';
                            }
                            echo '</td>';
                            echo '<td>';

                            $cquery = "select * from specificactions where purchaseid=".$row[5];
                            $cresult = mysql_query($cquery, $con);
                            if(!$crow = mysql_fetch_array($cresult)) echo '$0.00';
                            else
                            {
                                $sumquery = "select sum(si.amount) from specificactions sa, specificcategories sc, specificitems si where si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5];
                                $sumresult = mysql_query($sumquery);
                                if($sumresult && $sumrow = mysql_fetch_array($sumresult)) echo '$'.number_format($sumrow[0]);
                                else echo '$0.00';
                            }
                            echo '</td>';
                            if ($row[7] == 'Bought')
                                echo '<td>'.$row[6].'</td>';
                            else
                                echo '<td>'.$row[7].'</td>';

                            if($row[1] == 0)
                            {
                                echo '<td align="right">';
                                echo '<form action="'.WEB_SERVER_NAME.'opsmgractions.php#Details" method="post">';
                                echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                                echo '<input type="hidden" value="'.$row[5].'" name="PurchaseID" />';
                                echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Vehicle Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                echo '</form>';
                                echo '</td>';
                            }
                            else echo '<td>&nbsp;</td>';
                            echo '</tr>';
                        }
                        echo '</table>';
                    }
                    else echo '<p>No Purchased Vehicles To Work On</p>';

                    echo '</div><!-- endgrideight-->';
                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                }

                if($uadmin == 'true')
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<h4 class="subhead">Action Plans</h4>';
                    //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Action Plans --</strong></p>';

                    $query = "select count(*) from actionplans where visible=1";
                    $result = mysql_query($query, $con);
                    if($result && $row = mysql_fetch_array($result)) $count = $row[0];
                    else $count = 0;

                    if($count > 0)
                    {
                        echo '<table width="900" border="0" cellpadding="3">';
                        echo '<tr style="color:#85c11b; font-size:15px;">';
                        echo '<td width="300"><strong>Plan Name</strong></td>';
                        echo '<td width="300"><strong>Categories</strong></td>';
                        echo '<td width="280"><strong>Checklist Items</strong></td>';
                        echo '<td width="10" align="right"><strong>Actions</strong></td>';
                        echo '<td width="10"><strong>&nbsp</strong></td>';
                        echo '</tr>';

                        $query = "select name, corporateadded, actionplanid from actionplans where visible=1";
                        $result = mysql_query($query, $con);
                        while($result && $row = mysql_fetch_array($result))
                        {
                            echo '<tr style="color:#000000; font-size:15px;">';
                            echo '<td>'.$row[0].'</td>';
                            echo '<td>';

                            $cquery = "select count(*) from actioncategories where actionplanid=".$row[2];
                            $cresult = mysql_query($cquery, $con);
                            if($cresult && $crow = mysql_fetch_array($cresult)) echo $crow[0];
                            else echo 0;
                            echo '</td>';
                            echo '<td>';

                            $cquery = "select count(*) from actioncategories a, actionitems i where i.actioncategoryid=a.actioncategoryid and a.actionplanid=".$row[2];
                            $cresult = mysql_query($cquery, $con);
                            if($cresult && $crow = mysql_fetch_array($cresult)) echo $crow[0];
                            else echo 0;
                            echo '</td>';
                            echo '<td align="right">';
                            echo '<form action="'.WEB_SERVER_NAME.'actionplan.php" method="post">';
                            echo '<input type="hidden" value="'.$row[2].'" name="ActionPlanID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Action Plan" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form>';
                            echo '</td><td align="left">';
                            echo '<form action="'.WEB_SERVER_NAME.'actionplan.php" method="post">';
                            echo '<input type="hidden" value="'.$row[2].'" name="ActionPlanID" />';
                            echo '<input type="hidden" value="true" name="DeleteIt" />';
                            echo '<input type="hidden" value="'.WEB_SERVER_NAME.'mydashboard.php#admintab" name="ReturnTo" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Action Plan" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        echo '</table>';
                    }
                    else echo '<p>No Action Plans Available</p>';

                    echo '</div><!-- endgrideight-->';
                    echo '<table><tr><td>';
                    echo '<form action="'.WEB_SERVER_NAME.'actionplan.php" method="post">';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD ACTION PLAN</button>';
                    echo '</form></td><td>';
                    echo '<form action="'.WEB_SERVER_NAME.'copyactionplan.php" method="post">';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">COPY AN ACTION PLAN</button>';
                    echo '</form></td></tr></table>';
                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                }
                mysql_close($con);
            }
        }
        elseif($curadmintab == 'authbuyer')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight" style="width:930px;">';
            echo '<h4 class="subhead">Watchlist Vehicles</h4>';
           
            
            
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 3 -- </strong>';
                      //  echo '<h4 class="subhead"><button class="hideshow" id="step3button" onclick="javascript:showhidediv(\'step3\', \'step3button\');">Hide</button>';
                    //    echo '&nbsp;&nbsp;Customers in Step 3</h4>';
                        echo '<div style="display:block" name="step3" id="step3">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td ><strong>Customer</strong></td>';
                        echo '<td ><strong>Status</strong></td>';
                        echo '<td ><strong>Notes</strong></td>';
                        echo '<td ><strong>Registered</strong></td>';
                        echo '<td ><strong>Last Status</strong></td>';
                        echo '<td ><strong>FollowUp</strong></td>';
                        echo '<td ><strong>Actions</strong></td>';
                        echo '</tr>';
                        
                        $admin_id = get_admin_id();
			$needlist = getassignedmarketneeds($admin_id, true);
                       
			//echo $userid;
                     //  echo "needlist for $userid is<br><pre>", print_r($needlist, true), "</pre>\n";
            if(strlen($needlist)>0)
            {
                $numhidden = 0;

                // Get the basic user information for each one...
                //echo $con;
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    //echo $con + ' - ';
                    mysql_select_db(DB_SERVER_DATABASE, $con);
                    //echo $con + '<br/>';

                    // Get the hidden market needs and order by follow up date reversed...
                    $cuquery = "select m.marketneedid, concat(u.firstname,' ',u.lastname) AS fullname, m.title, m.created, m.followupdate, u.userid, m.state, u.note from users u, marketneeds m where u.userid=m.userid and m.showtorep=0 and m.marketneedid in (".$needlist.") order by m.followupdate desc, m.created desc";
                        //echo "query is <br><pre>" . htmlspecialchars($cuquery)."</pre><br>\n";
                    $curesult = mysql_query($cuquery, $con);
                    if($curesult)
                    {
                        while($curow = mysql_fetch_array($curesult))
                        {
                            $mnid[$numhidden] = $curow[0];
                            $mnname[$numhidden] = $curow[1];
                            $mntitle[$numhidden] = $curow[2];
                            $mncreated[$numhidden] = $curow[3];
                            $mnfollowup[$numhidden] = $curow[4];
                            $mnuserid[$numhidden] = $curow[5];
                            $mncestate[$numhidden] = $curow[6];
                            $mnnote[$numhidden] = $curow[7];
                            $numhidden++;
                        }
                    }

                    // Get details of all visible market needs to show to the rep
                    $query = "select u.userid, concat(u.firstname,' ',u.lastname) AS fullname, u.note, u.created, m.followupdate, m.marketneedid from users u, marketneeds m where u.userid=m.userid and m.showtorep=1 and m.marketneedid in (".$needlist.")";
                        //echo "query is <br><pre>" . htmlspecialchars($query)."</pre><br>\n";
                    $result = mysql_query($query, $con);
                    $index = 0;
                    if($result)
                    {
                        while($row = mysql_fetch_array($result))
                        {
                            $iuserid[$index] = $row[0];
                            $iusername[$index] = $row[1];
                            $iusernote[$index] = $row[2];
                            $iuserreg[$index] = $row[3];
                            $iuserfollow[$index] = $row[4];
                            $iuserneed[$index] = $row[5];
                            $istep[$index] = getuserstep($row[0], $row[5]);
                            $index++;
                        }
                    }

                    //mysql_close($con);
                }
			}
                        
                        
                        
                        $count = count($iuserid);
                        
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 3)
                            {
                                echo '<tr valign="top">';
                                echo '<td><strong>'.$iusername[$i].'</strong></td>';

                                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                                if($con)
                                {
                                    mysql_select_db(DB_SERVER_DATABASE, $con);

                                    $query = "select count(*) from specificvehicles where marketneedid =".$iuserneed[$i];
                                    $result = mysql_query($query, $con);
                                    if($result && $row = mysql_fetch_array($result))
                                    {
                                        $vehcount = $row[0];
                                        $purcount = 0;

                                        $query = "select specificvehicleid from specificvehicles where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        while($result && $row = mysql_fetch_array($result))
                                        {
                                            // Get the current Purchase Approval Information (if any)...
                                            $pquery = 'select count(*) from purchases where specificvehicleid = '.$row[0];
                                            $presult = mysql_query($pquery, $con);
                                            if($presult && $prow = mysql_fetch_array($presult)) $purcount += $prow[0];
                                        }
                                    }
                                    else $vehcount = 0;

                                    if($purcount > 0)
                                    {
                                        echo '<td>Specific Vehicle Chosen</td>';

                                        $query = "select max(p.lastupdated) from purchases p, specificvehicles s where p.specificvehicleid=s.specificvehicleid and s.marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }
                                    elseif($vehcount > 0)
                                    {
                                        echo '<td>'.$vehcount.' Specific Vehicle(s)</td>';

                                        $query = "select max(lastupdated) from specificvehicles where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }
                                    else
                                    {
                                        echo '<td>Needs Specific Vehicles</td>';

                                        $query = "select max(accepted) from (select o.accepted from orders o,orderfor of,firmquotes f,quoterequests q where o.orderid=of.orderid and f.firmquoteid=of.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=".$iuserneed[$i]." union all select w.accepted from watches w,watchfor wf,firmquotes f,quoterequests q where w.watchid=wf.watchid and f.firmquoteid=wf.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=".$iuserneed[$i].") data";
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }

                                    mysql_close($con);
                                }
                                else
                                {
                                    echo '<td>Pending</td>';
                                    $laststatus = 'Pending';
                                }
                                if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                else echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                echo '<td>'.$laststatus.'</td>';
                                if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                echo '<td><form action="'.WEB_SERVER_NAME.'salesresearcher.php" method="post">';
                                echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Customer Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                echo '</form></td>';

                                echo '</tr>';
                            }
                        }
                        echo '</table>';
                        echo '</div>'; // step3
                        echo '</div>';
            
            
            
            
            echo '</div><!-- endgrideight-->';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'salesrep')
        {
            // request only active leads
            // TODO: fix this function to return leads actually assigned to this salesrep not used to be assigned
            $needlist = getassignedmarketneeds($userid, true);
            // echo "needlist for $userid is<br><pre>", print_r($needlist, true), "</pre>\n";
            if(strlen($needlist)>0)
            {
                $numhidden = 0;

                // Get the basic user information for each one...
                //echo $con;
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    //echo $con + ' - ';
                    mysql_select_db(DB_SERVER_DATABASE, $con);
                    //echo $con + '<br/>';

                    // Get the hidden market needs and order by follow up date reversed...
                    $cuquery = "select m.marketneedid, concat(u.firstname,' ',u.lastname) AS fullname, m.title, m.created, m.followupdate, u.userid, m.state, u.note from users u, marketneeds m where u.userid=m.userid and m.showtorep=0 and m.marketneedid in (".$needlist.") order by m.followupdate desc, m.created desc";
                        //echo "query is <br><pre>" . htmlspecialchars($cuquery)."</pre><br>\n";
                    $curesult = mysql_query($cuquery, $con);
                    if($curesult)
                    {
                        while($curow = mysql_fetch_array($curesult))
                        {
                            $mnid[$numhidden] = $curow[0];
                            $mnname[$numhidden] = $curow[1];
                            $mntitle[$numhidden] = $curow[2];
                            $mncreated[$numhidden] = $curow[3];
                            $mnfollowup[$numhidden] = $curow[4];
                            $mnuserid[$numhidden] = $curow[5];
                            $mncestate[$numhidden] = $curow[6];
                            $mnnote[$numhidden] = $curow[7];
                            $numhidden++;
                        }
                    }

                    // Get details of all visible market needs to show to the rep
                    $query = "select u.userid, concat(u.firstname,' ',u.lastname) AS fullname, u.note, u.created, m.followupdate, m.marketneedid from users u, marketneeds m where u.userid=m.userid and m.showtorep=1 and m.marketneedid in (".$needlist.")";
                        //echo "query is <br><pre>" . htmlspecialchars($query)."</pre><br>\n";
                    $result = mysql_query($query, $con);
                    $index = 0;
                    if($result)
                    {
                        while($row = mysql_fetch_array($result))
                        {
                            $iuserid[$index] = $row[0];
                            $iusername[$index] = $row[1];
                            $iusernote[$index] = $row[2];
                            $iuserreg[$index] = $row[3];
                            $iuserfollow[$index] = $row[4];
                            $iuserneed[$index] = $row[5];
                            $istep[$index] = getuserstep($row[0], $row[5]);
                            $index++;
                        }
                    }

                    //mysql_close($con);
                }
                else $numhidden = 0;

if (0)
{
                if($numhidden > 0)
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 0px">';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<form action="'.WEB_SERVER_NAME.'mydashboard.php#admintab" method="post" style="float:right;" >';
                    echo '<input type="hidden" value="true" name="PostBack" />';
                    echo '<input type="hidden" value="true" name="ShowAllHiddenCust" />';
                    echo '<button type="submit" value="" class="blueongrey_dash">Show All Hidden Customer Experiences</button>';
                    echo '</form>';
                    echo '<form action="'.WEB_SERVER_NAME.'mydashboard.php#admintab" method="post">';
                    echo '<input type="hidden" value="true" name="PostBack" />';
                    echo '<select name="ShowExperience" id="ShowExperience" style="width: 350px;">';
                    for($i = 0; $i < $numhidden; $i++)
                    {
                        if (strlen($mnfollowup[$i]) > 0) // followups first
                            echo '<option value="'.$mnid[$i].'" >'.$mnname[$i].' - '.$mntitle[$i].' - Followup:'.date_at_timezone('m/d/Y','GMT',$mnfollowup[$i]).'</option>';
                        else
                            echo '<option value="'.$mnid[$i].'" >'.$mnname[$i].' - '.$mntitle[$i].' - Started:'.date_at_timezone('m/d/Y','GMT',$mncreated[$i]).'</option>';
                    }
                    echo '</select>';
                    echo '<button type="submit" value="" class="blueongrey_dash">Show Selected Customer Experience</button>';
                    echo '</form>';
                    echo '</div></div>';
                }
}
                // customers for this sales rep
                // NOTE = if a customer has multiple active market needs, all of them go to the salesrep of the latest market need record sales rep assignment
                $count = count($iuserid);
                if($count>0 || $numhidden > 0)
                {
                    $step1 = 0;
                    $step2 = 0;
                    $step3 = 0;
                    $step4 = 0;
                    for($i=0;$i<$count;$i++)
                    {
                        if($istep[$i] == 4) $step4++;
                        elseif($istep[$i] == 3) $step3++;
                        elseif($istep[$i] == 2) $step2++;
                        else $step1++;
                    }

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    if($step4 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 4 -- </strong>';
                        // echo '<button id="step4button" onclick="javascript:showhidediv(\'step4\', \'step4button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button class="hideshow" id="step4button" onclick="javascript:showhidediv(\'step4\', \'step4button\');">Hide</button>';
                        echo '&nbsp;&nbsp;Customers in Step 4</h4>';
                        echo '<div style="display:block" id="step4">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 4)
                            {
                                echo '<tr valign="top">';
                                echo '<td><strong>'.$iusername[$i].'</strong></td>';
                                echo '<td>Pending</td>';
                                echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                // TODO: is this just a duplicate?
                                echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                echo '</form></td>';

                                echo '</tr>';
                            }
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                        //echo '<br clear="all" />';
                    }
                    if($step3 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 3 -- </strong>';
                        echo '<h4 class="subhead"><button class="hideshow" id="step3button" onclick="javascript:showhidediv(\'step3\', \'step3button\');">Hide</button>';
                        echo '&nbsp;&nbsp;Customers in Step 3</h4>';
                        echo '<div style="display:block" name="step3" id="step3">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td ><strong>Customer</strong></td>';
                        echo '<td ><strong>Status</strong></td>';
                        echo '<td ><strong>Notes</strong></td>';
                        echo '<td ><strong>Registered</strong></td>';
                        echo '<td ><strong>Last Status</strong></td>';
                        echo '<td ><strong>FollowUp</strong></td>';
                        echo '<td ><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 3)
                            {
                                echo '<tr valign="top">';
                                echo '<td><strong>'.$iusername[$i].'</strong></td>';

                                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                                if($con)
                                {
                                    mysql_select_db(DB_SERVER_DATABASE, $con);

                                    $query = "select count(*) from specificvehicles where marketneedid =".$iuserneed[$i];
                                    $result = mysql_query($query, $con);
                                    if($result && $row = mysql_fetch_array($result))
                                    {
                                        $vehcount = $row[0];
                                        $purcount = 0;

                                        $query = "select specificvehicleid from specificvehicles where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        while($result && $row = mysql_fetch_array($result))
                                        {
                                            // Get the current Purchase Approval Information (if any)...
                                            $pquery = 'select count(*) from purchases where specificvehicleid = '.$row[0];
                                            $presult = mysql_query($pquery, $con);
                                            if($presult && $prow = mysql_fetch_array($presult)) $purcount += $prow[0];
                                        }
                                    }
                                    else $vehcount = 0;

                                    if($purcount > 0)
                                    {
                                        echo '<td>Specific Vehicle Chosen</td>';

                                        $query = "select max(p.lastupdated) from purchases p, specificvehicles s where p.specificvehicleid=s.specificvehicleid and s.marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }
                                    elseif($vehcount > 0)
                                    {
                                        echo '<td>'.$vehcount.' Specific Vehicle(s)</td>';

                                        $query = "select max(lastupdated) from specificvehicles where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }
                                    else
                                    {
                                        echo '<td>Needs Specific Vehicles</td>';

                                        $query = "select max(accepted) from (select o.accepted from orders o,orderfor of,firmquotes f,quoterequests q where o.orderid=of.orderid and f.firmquoteid=of.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=".$iuserneed[$i]." union all select w.accepted from watches w,watchfor wf,firmquotes f,quoterequests q where w.watchid=wf.watchid and f.firmquoteid=wf.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=".$iuserneed[$i].") data";
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }

                                    mysql_close($con);
                                }
                                else
                                {
                                    echo '<td>Pending</td>';
                                    $laststatus = 'Pending';
                                }
                                if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                else echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                echo '<td>'.$laststatus.'</td>';
                                if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Customer Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                echo '</form></td>';

                                echo '</tr>';
                            }
                        }
                        echo '</table>';
                        echo '</div>'; // step3
                        echo '</div>';
                        //echo '<br clear="all" />';
                    }
                    if($step2 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 2 -- </strong>';
                        // echo '<button id="step2button" onclick="javascript:showhidediv(\'step2\', \'step4button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button id="step2button" class="hideshow" onclick="javascript:showhidediv(\'step2\', \'step2button\');">Hide</button>';
                        echo ' Customers in Step 2</h4>';
                        echo '<div id="step2" style="display:block">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 2)
                            {
                                // Get the basic user information for each one...
                                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                                if($con)
                                {
                                    mysql_select_db(DB_SERVER_DATABASE, $con);

                                    echo '<tr valign="top">';
                                    echo '<td><strong>'.$iusername[$i].'</strong></td>';

                                    $query = "select count(*) from firmquotes f,quoterequests q where f.quoterequestid=q.quoterequestid and q.marketneedid =".$iuserneed[$i];
                                    $result = mysql_query($query, $con);
                                    if($result && $row = mysql_fetch_array($result)) $quotecount = $row[0];
                                    else $quotecount = 0;

                                    if($quotecount > 0)
                                    {
                                                                            $plural = pluralize_noun($quotecount, "Current Market Stud", "ies", "y");
                                        echo "<td> $quotecount $plural</td>";
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';

                                        $query = "select max(lastupdated) from firmquotes where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                        else echo '<td>Not Set</td>';
                                    }
                                    else
                                    {
                                        echo '<td>Needs Current Market Study</td>';
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';

                                        $query = "select max(lastupdated) from quoterequests where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result))
                                        {
                                            if(is_null($row[0]))
                                            {
                                                $query = "select max(lastupdated) from assessments where marketneedid =".$iuserneed[$i];
                                                $result = mysql_query($query, $con);
                                                if($result && $row = mysql_fetch_array($result))
                                                {
                                                    if(is_null($row[0]))
                                                    {
                                                        $query = "select max(lastupdated) from consultations where marketneedid =".$iuserneed[$i];
                                                        $result = mysql_query($query, $con);
                                                        if($result && $row = mysql_fetch_array($result))
                                                        {
                                                            if(is_null($row[0])) echo '<td>Not Set</td>';
                                                            else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</strong></td>';
                                                        }
                                                        else echo '<td>Not Set</td>';
                                                    }
                                                    else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                                }
                                                else
                                                {
                                                    $query = "select max(lastupdated) from consultations where marketneedid =".$iuserneed[$i];
                                                    $result = mysql_query($query, $con);
                                                    if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                                    else echo '<td>Not Set</td>';
                                                }
                                            }
                                            else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                        }
                                        else
                                        {
                                            $query = "select max(lastupdated) from assessments where marketneedid =".$iuserneed[$i];
                                            $result = mysql_query($query, $con);
                                            if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                            else
                                            {
                                                $query = "select max(lastupdated) from consultations where marketneedid =".$iuserneed[$i];
                                                $result = mysql_query($query, $con);
                                                if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                                else echo '<td>Not Set</td>';
                                            }
                                        }
                                    }
                                    if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                    else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                    echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                    echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                    echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                    echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                    echo '</form></td>';

                                    echo '</tr>';

                                    mysql_close($con);
                                }
                            }
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                        //echo '<br clear="all" />';
                    } 
                    if($step1 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 1 -- </strong>';
                        // echo '<button id="step1button" onclick="javascript:showhidediv(\'step1\', \'step1button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button id="step1button" class="hideshow" onclick="javascript:showhidediv(\'step1\', \'step1button\');">Hide</button>';
                        echo '&nbsp;&nbsp;Customers in Step 1</h4>';
                        echo '<div style="display:block" id="step1">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 1)
                            {
                                // Get the basic user information for each one...
                                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                                if($con)
                                {
                                    mysql_select_db(DB_SERVER_DATABASE, $con);

                                    echo '<tr valign="top">';
                                    echo '<td><strong>'.$iusername[$i].'</strong></td>';

                                    $query = "select count(*) from favorites where marketneedid =".$iuserneed[$i];
                                    $result = mysql_query($query, $con);
                                    if($result && $row = mysql_fetch_array($result)) $favcount = $row[0];
                                    else $favcount = 0;

                                    if($favcount > 0)
                                    {
                                        echo '<td>Stored '.$favcount.' Favorites</td>';
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</strong></td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';

                                        // Get the current User Information...
                                        $query = "select max(lastupdated) from favorites where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                        else echo '<td><strong>Not Set</strong></td>';
                                    }
                                    else
                                    {
                                        echo '<td>Registered Only</td>';
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</strong></td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                        echo '<td>Not Set</td>';
                                    }
                                    if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                    else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                    echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                    echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                    echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                    echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                    echo '</form></td>';

                                    echo '</tr>';

                                    mysql_close($con);
                                }
                            }
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                    }

                    // show hidden market needs under step 1
                    if($numhidden > 0)
                    {
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Followup Customers -- </strong>';
                        // echo '<button id="step0button" onclick="javascript:showhidediv(\'step0\', \'step0button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button id="step0button" class="hideshow" onclick="javascript:showhidediv(\'step0\', \'step0button\');">Hide</button>';
                        echo ' Followup Customers</h4>';
                        echo '<div style="display:block" id="step0">';
                        echo '<table width="900" style="color: rgb(51, 102, 204);" border="0" cellpadding="3">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$numhidden;$i++)
                        {
                            $mnstep[$i] = getuserstep($mnuserid[$i], $mnid[$i]);

                            echo '<tr valign="top">';
                            echo '<td><strong>'.$mnname[$i].'</strong></td>';
                            echo '<td><strong>Step '.$mnstep[$i].': '.$mntitle[$i].'</strong></td>';

                            if(is_null($mnnote[$i]) || (strlen(trim($mnnote[$i]))<1)) echo '<td><strong>{None}</strong></td>';
                            else echo '<td><strong>'.substr($mnnote[$i],0,16).'</strong></td>';
                            echo '<td><strong>'.date_at_timezone('m/d/Y', 'GMT', $mncreated[$i]).'</strong></td>';
                            echo '<td><strong>'.$mncestate[$i].'</strong></td>';
                            if(is_null($mnfollowup[$i])) echo '<td><strong>Not Set</strong></td>';
                            else echo '<td><strong>'.date_at_timezone('m/d/Y', 'GMT', $mnfollowup[$i]).'</strong></td>';

                            echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                            echo '<input type="hidden" value="'.$mnuserid[$i].'" name="ForUserID" />';
                            echo '<input type="hidden" value="'.$mnid[$i].'" name="MarketNeedID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Customer Experience" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                            echo '</form></td>';

                            echo '</tr>';
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                    }

                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                    //echo '<button type="submit" value="" class="med" style="margin-top: 10px;">ADD USER</button>';
                }
                else
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<div class="grideight" style="width:930px;">';
                    if($usrep == 'true') echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- All Customers Are Currently Hidden --</strong></p>';
                    else echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- You are not setup as a Sales Representative Personally --</strong></p>';
                    echo '</div><!-- endgrideight-->';
                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                    //echo '<button type="submit" value="" class="med" style="margin-top: 10px;">ADD USER</button>';
                }
            }
            else
            {
                echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                echo '<div class="grideight" style="width:930px;">';
                if($usrep == 'true') echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- No Customers Assigned Personally --</strong></p>';
                else echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- You are not setup as a Sales Representative Personally --</strong></p>';
                echo '</div><!-- endgrideight-->';
                echo '</div><!-- endgridfullgrey -->';
                //echo '<br clear="all" />';
                //echo '<button type="submit" value="" class="med" style="margin-top: 10px;">ADD USER</button>';
            }

            // Now if a higher level viewing this screen...show the other SalesReps Customers...
            if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true'))
            {
                echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                echo '<div class="grideight" style="width:930px;">';
                echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- View Other Representative\'s Customers --</strong></p>';
                echo '<p>Coming Soon!</p>';
                echo '</div><!-- endgrideight-->';
                echo '</div><!-- endgridfullgrey -->';
                //echo '<br clear="all" />';
            }
        }
        echo '</div>';
        //echo '<br clear="all" />';
    }

  


    echo '</div>';
?>
</div>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
