<style>
#popup_contact_us form {
	padding: 10px;
}
#popup_contact_us input[type=text], 
#popup_contact_us input[type=email],
#popup_contact_us textarea {
	padding-left: 15px;
	width: 100%;
	border: 1px solid #7BAE2B;
}
#popup_contact_us .red {
	color: red;
}
</style>
<div data-role="popup" id="popup_contact_us" data-theme="a" class="ui-corner-all">
	<h2 class="text-center">CONTACT US</h2>
	<form action="process_contact_form.php" method="POST">
		<div>
			<label for="Name" class="control-label">Name<span class="red">*</span></label>
			<p>
				<input type="text" class="form-control" name="name" id="name" required placeholder="Name">
			</p>
		</div>
		<div>
			<label for="email" class="control-label">Email address<span class="red">*</span></label>
			<p>
				<input type="email" class="form-control" name="email" id="email" required placeholder="Email">
			</p>			
		</div>
		<div>
			<label for="subject" class="control-label">Subject<span class="red">*</span></label>
			<p>
				<input type="text" class="form-control" name="subject" id="subject" required placeholder="Subject">
			</p>
		</div>	
		<div>
			<label for="message" class="control-label">Message<span class="red">*</span></label>
			<p>
				<textarea name="message" id="message" required placeholder="Enter message here..." rows="4"></textarea>
			</p>
		</div>
		<input type="submit" name="submit" class="btn btn-primary" value="Submit">
		<p style="text-align: center; margin: -30px auto 0; padding-left: 84px;" class="message"></p>
	</form>
</div>
<script>
jQuery(document).ready( function($) {
	jQuery('#popup_contact_us form').submit( function() {	
		
		//date_default_timezone_set('Etc/UTC');
		
		var $form = $(this);
		var $inputs = $form.find("input, select, button, textarea");		
		
		var url = 'process_contact_form.php';
		var data = $form.serialize();

		$inputs.prop("disabled", true);
		
		$.ajax({			
			url: url,
			type: "post",
			data: data,
			beforeSend: function() {
				$('#popup_contact_us .message').html("Sending. Please Wait...");	
			}
			
		}).done( function(data) {			
			
			$('#popup_contact_us .message').html(data);			
			
			$('#popup_contact_us input[type=text]').val('');
			$('#popup_contact_us input[type=email]').val('');
			$('#popup_contact_us textarea').val('');
		});	
		
		return false;
	});
	
	function date_default_timezone_set (tz) {
		// http://kevin.vanzonneveld.net
		// +   original by: Brett Zamir (http://brett-zamir.me)
		// -    depends on: timezone_abbreviations_list
		// %        note 1: Uses global: php_js to store the default timezone
		// *     example 1: date_default_timezone_set('unknown');
		// *     returns 1: 'unknown'
		var tal = {},
		abbr = '',
		i = 0;

		// BEGIN REDUNDANT
		this.php_js = this.php_js || {};
		// END REDUNDANT
		// PHP verifies that the timezone is valid and also sets this.php_js.currentTimezoneOffset and this.php_js.currentTimezoneDST if so
		tal = this.timezone_abbreviations_list();
		for (abbr in tal) {
			for (i = 0; i < tal[abbr].length; i++) {
			  if (tal[abbr][i].timezone_id === tz) {
				this.php_js.default_timezone = tz;
				return true;
			  }
			}
		}
		return false;
	}
});


</script>