<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 1056;
    $_SESSION['titleadd'] = 'Trade-In Quote Request'; 

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];

    $edititem = -1;    
    
    if (!isset($_SESSION['user'])) {
		unset($_SESSION['assessment_post']);
		unset($_SESSION['pos_post']);
		unset($_SESSION['searchplan_post']);
		unset($_SESSION['consult_post']);
		$_SESSION['request_trade_in_quote'] = 1;
		$_SESSION['submit_btn'] = "SUBMIT and continue to REQUEST TRADE IN QUOTE";
		echo "<script>window.location='register.php';</script>";
	} 

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    
	if($con) {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = 'select TradeInRequestID from tradeinrequests where MarketNeedID = '.$marketneedid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result)) $edititem = $row[0];

        mysql_close($con);
    }    
    
    if (!isset($_SESSION['user']) && isset($_POST['PostBack'])) {
		$_SESSION['PostBack11'] = $_POST;
		$_SESSION['images'] = $_FILES;
		echo "<script>window.location='register.php';</script>";
	} 

    if(isset($_POST['PostBack']) && isset($_SESSION['user']) ) {
       // echo'<pre>';print_r($_FILES);echo'</pre>';die('here');
        // Time to add it to the database...
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con) {
            
            mysql_select_db(DB_SERVER_DATABASE, $con);
            $erroronsave = 'false';
            if( $edititem == -1 ) {
                // We have to add a new one...
                $query = "insert into tradeinrequests (Created, LastUpdated, MarketNeedID, Description) values (";
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."', '".date_at_timezone('Y-m-d H:i:s', 'EST')."', ".$marketneedid.", '".escapestr($_POST["Description"])."')";

                if(!mysql_query($query, $con)) $erroronsave = 'Could Not Save Trade-In Request';
                else
                {
                    $edititem = mysql_insert_id($con);

                    $fileerror = 'false';

                    $count = $_POST['NumImages'];
                    for($current = 1; (($current <= $count) && ($fileerror == 'false')); $current++)
                    {
                        $curimage = 'image'.$current;

                        if(isset($_FILES[$curimage]['name']) && (strlen($_FILES[$curimage]['name']) > 0))
                        {
                            $ext = strtolower(end(explode(".", $_FILES[$curimage]['name'])));
                            $path = WEB_ROOT_PATH;
                            $ifilename = 'trimages/Veh'.$edititem.'-'.$current.'temp.'.$ext;
                            $target = $path.$ifilename;
                            if(!is_uploaded_file($_FILES[$curimage]['tmp_name'])) $fileerror = 'Transfer Error - usually due to files over '.ini_get('upload_max_filesize').' in size';
                            else
                            {
                                if(($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE"))
                                {
                                    if($_FILES[$curimage]["size"] > (20*1024*1024)) $fileerror = 'File is larger than 20M...'.number_format($_FILES[$curimage]["size"]/1024/1024).'M';
                                    else
                                    {
                                        if($_FILES[$curimage]["error"] > 0) $fileerror .= 'File Has Error '.$_FILES[$curimage]["error"];
                                        else
                                        {
                                            if(file_exists($target)) unlink($target);
                                            if(!move_uploaded_file($_FILES[$curimage]['tmp_name'], $target)) $fileerror = 'Image Invalid';
                                        }
                                    }
                                }
                                else $fileerror = 'File is not a supported Image format (PNG, GIF, JPG, JPEG, JPE)';
                            }
                        }
                        else $ifilename = '';

                        if($fileerror == 'false')
                        {
                            if($ifilename && (strlen($ifilename)>0))
                            {
                                $ext = strtolower(end(explode(".", $ifilename)));
                                $path = WEB_ROOT_PATH;
                                $ofilename = 'trimages/Veh'.$edititem.'-'.$current.'.'.$ext;
                                $target = $path.$ofilename;
                                if(file_exists($target)) unlink($target);
                                if(!ScaledImageFile($ifilename, 600, 600, $ofilename, 'false', 'true')) $fileerror = 'Could not create the image file!';
                                else
                                {
                                    unlink($path.$ifilename);

                                    $query = "insert into tradeinreqimages (TradeInRequestID, ImageFile) values (".$edititem.",'".escapestr($ofilename)."')";
                                    if(!mysql_query($query, $con)) $erroronsave = 'Could not add image file!';
                                }
                            }
                        }
                    }
                }
				
            } else {
			
                // We edit the one we have...
                $query = "update tradeinrequests set LastUpdated = '".date_at_timezone('Y-m-d H:i:s', 'EST')."', Description = '".escapestr($_POST["Description"])."' where TradeInRequestID = ".$edititem;

                if(!mysql_query($query, $con)) $erroronsave = 'Could Not Update Trade-In Request';
                else
                {
                    $fileerror = 'false';

                    // Determine how many are already stored...
                    $oquery = "select count(*) from tradeinreqimages where TradeInRequestID = ".$edititem;
                    $oresult = mysql_query($oquery, $con);
                    if($orow = mysql_fetch_array($oresult)) $startat = $row[0];
                    else $startat = 0;

                    $count = $_POST['NumImages'];
                    for($current = 1; (($current <= $count) && ($fileerror == 'false')); $current++)
                    {
                        $curimage = 'image'.$current;

                        if(isset($_FILES[$curimage]['name']) && (strlen($_FILES[$curimage]['name']) > 0))
                        {
                            $ext = strtolower(end(explode(".", $_FILES[$curimage]['name'])));
                            $path = WEB_ROOT_PATH;
                            $ifilename = 'trimages/Veh'.$edititem.'-'.($startat + $current).'temp.'.$ext;
                            $target = $path.$ifilename;
                            if(!is_uploaded_file($_FILES[$curimage]['tmp_name'])) $fileerror = 'Transfer Error - usually due to files over '.ini_get('upload_max_filesize').' in size';
                            else
                            {
                                if(($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE"))
                                {
                                    if($_FILES[$curimage]["size"] > (20*1024*1024)) $fileerror = 'File is larger than 20M...'.number_format($_FILES[$curimage]["size"]/1024/1024).'M';
                                    else
                                    {
                                        if($_FILES[$curimage]["error"] > 0) $fileerror .= 'File Has Error '.$_FILES[$curimage]["error"];
                                        else
                                        {
                                            if(file_exists($target)) unlink($target);
                                            if(!move_uploaded_file( $_FILES[$curimage]['tmp_name'], $target)) $fileerror = 'Image Invalid';
                                        }
                                    }
                                }
                                else $fileerror = 'File is not a supported Image format (PNG, GIF, JPG, JPEG, JPE)';
                            }
                        }
                        else $ifilename = '';

                        if($fileerror == 'false')
                        {
                            if($ifilename && (strlen($ifilename)>0))
                            {
                                $ext = strtolower(end(explode(".", $ifilename)));
                                $path = WEB_ROOT_PATH;
                                $ofilename = 'trimages/Veh'.$edititem.'-'.($startat + $current).'.'.$ext;
                                $target = $path.$ofilename;
                                if(file_exists($target)) unlink($target);
                                if(!ScaledImageFile($ifilename, 600, 600, $ofilename, 'false', 'true')) $fileerror = 'Could not create the image file!';
                                else
                                {
                                    unlink($path.$ifilename);

                                    $query = "insert into tradeinreqimages (TradeInRequestID, ImageFile) values (".$edititem.",'".escapestr($ofilename)."')";
                                    if(!mysql_query($query, $con)) $erroronsave = 'Could not add image file!';
                                }
                            }
                        }
                    }
                }
            }

            mysql_close($con);
        }
    }
    
    else if(isset($_SESSION['PostBack11']) && !isset($_POST['PostBack'])){
      
        // Time to add it to the database...
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $erroronsave = 'false';
            
            if($edititem == -1)
            {
                // We have to add a new one...
                $query = "insert into tradeinrequests (Created, LastUpdated, MarketNeedID, Description) values (";
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."', '".date_at_timezone('Y-m-d H:i:s', 'EST')."', ".$marketneedid.", '".escapestr($_SESSION['PostBack11']["Description"])."')";

                if(!mysql_query($query, $con)) $erroronsave = 'Could Not Save Trade-In Request';
                else
                {
                    $edititem = mysql_insert_id($con);

                    $fileerror = 'false';

                    $count = $_SESSION['PostBack11']['NumImages'];
                    $_FILES=$_SESSION['images'];
                    
                    for($current = 1; (($current <= $count) && ($fileerror == 'false')); $current++)
                    {
                        
                        $curimage = 'image'.$current;
                        //echo'<pre>';print_r($_SESSION['images'][$curimage]['tmp_name']);echo'</pre>';
                        if(isset($_FILES[$curimage]['name']) && (strlen($_FILES[$curimage]['name']) > 0))
                        {
                            
                           
                            $ext = strtolower(end(explode(".", $_FILES[$curimage]['name'])));
                            $path = WEB_ROOT_PATH;
                            $ifilename = 'trimages/Veh'.$edititem.'-'.$current.'temp.'.$ext;
                            $target = $path.$ifilename;
                          
                            if(0){
                               // echo'<pre>';print_r($_SESSION['images'][$curimage]);echo'</pre>';die('here');
                                $fileerror = 'Transfer Error - usually due to files over '.ini_get('upload_max_filesize').' in size';
                            }
                            else
                            {
                              
                                if(($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE"))
                                {
                                    if($_FILES[$curimage]["size"] > (20*1024*1024)) $fileerror = 'File is larger than 20M...'.number_format($_SESSION['images'][$curimage]["size"]/1024/1024).'M';
                                    else
                                    {
                                        if($_FILES[$curimage]["error"] > 0) $fileerror .= 'File Has Error '.$_FILES[$curimage]["error"];
                                        else
                                        {
                                            if(file_exists($target)) unlink($target);
                                            if(!move_uploaded_file($_FILES[$curimage]['tmp_name'], $target)) $fileerror = 'Image Invalid';
                                        }
                                    }
                                }
                                else $fileerror = 'File is not a supported Image format (PNG, GIF, JPG, JPEG, JPE)';
                            }
                           // echo'<pre>';print_r($_FILES[$curimage]['tmp_name']);echo'</pre>';die('here');
                        }
                        else{
                            $ifilename = '';
                           
                        }

                        if($fileerror == 'false')
                        {
                            if($ifilename && (strlen($ifilename)>0))
                            {
                                $ext = strtolower(end(explode(".", $ifilename)));
                                $path = WEB_ROOT_PATH;
                                $ofilename = 'trimages/Veh'.$edititem.'-'.$current.'.'.$ext;
                                $target = $path.$ofilename;
                                if(file_exists($target)) unlink($target);
                                if(!ScaledImageFile($ifilename, 600, 600, $ofilename, 'false', 'true')) $fileerror = 'Could not create the image file!';
                                else
                                {
                                    unlink($path.$ifilename);

                                    $query = "insert into tradeinreqimages (TradeInRequestID, ImageFile) values (".$edititem.",'".escapestr($ofilename)."')";
                                    if(!mysql_query($query, $con)) $erroronsave = 'Could not add image file!';
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // We edit the one we have...
                $query = "update tradeinrequests set LastUpdated = '".date_at_timezone('Y-m-d H:i:s', 'EST')."', Description = '".escapestr($_SESSION['PostBack11']["Description"])."' where TradeInRequestID = ".$edititem;

                if(!mysql_query($query, $con)) $erroronsave = 'Could Not Update Trade-In Request';
                else
                {
                    $fileerror = 'false';

                    // Determine how many are already stored...
                    $oquery = "select count(*) from tradeinreqimages where TradeInRequestID = ".$edititem;
                    $oresult = mysql_query($oquery, $con);
                    if($orow = mysql_fetch_array($oresult)) $startat = $row[0];
                    else $startat = 0;

                    $count = $_SESSION['PostBack11']['NumImages'];
                    for($current = 1; (($current <= $count) && ($fileerror == 'false')); $current++)
                    {
                        $curimage = 'image'.$current;

                        if(isset($_SESSION['images'][$curimage]['name']) && (strlen($_SESSION['images'][$curimage]['name']) > 0))
                        {
                            $ext = strtolower(end(explode(".", $_SESSION['images'][$curimage]['name'])));
                            $path = WEB_ROOT_PATH;
                            $ifilename = 'trimages/Veh'.$edititem.'-'.($startat + $current).'temp.'.$ext;
                            $target = $path.$ifilename;
                            if(!is_uploaded_file($_SESSION['images'][$curimage]['tmp_name'])) $fileerror = 'Transfer Error - usually due to files over '.ini_get('upload_max_filesize').' in size';
                            else
                            {
                                if(($ext == "png") || ($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($ext == "jpe") || ($ext == "PNG") || ($ext == "GIF") || ($ext == "JPEG") || ($ext == "JPG") || ($ext == "JPE"))
                                {
                                    if($_SESSION['images'][$curimage]["size"] > (20*1024*1024)) $fileerror = 'File is larger than 20M...'.number_format($_SESSION['images'][$curimage]["size"]/1024/1024).'M';
                                    else
                                    {
                                        if($_SESSION['images'][$curimage]["error"] > 0) $fileerror .= 'File Has Error '.$_SESSION['images'][$curimage]["error"];
                                        else
                                        {
                                            if(file_exists($target)) unlink($target);
                                            if(!move_uploaded_file($_SESSION['images'][$curimage]['tmp_name'], $target)) $fileerror = 'Image Invalid';
                                        }
                                    }
                                }
                                else $fileerror = 'File is not a supported Image format (PNG, GIF, JPG, JPEG, JPE)';
                            }
                        }
                        else $ifilename = '';

                        if($fileerror == 'false')
                        {
                            if($ifilename && (strlen($ifilename)>0))
                            {
                                $ext = strtolower(end(explode(".", $ifilename)));
                                $path = WEB_ROOT_PATH;
                                $ofilename = 'trimages/Veh'.$edititem.'-'.($startat + $current).'.'.$ext;
                                $target = $path.$ofilename;
                                if(file_exists($target)) unlink($target);
                                if(!ScaledImageFile($ifilename, 600, 600, $ofilename, 'false', 'true')) $fileerror = 'Could not create the image file!';
                                else
                                {
                                    unlink($path.$ifilename);

                                    $query = "insert into tradeinreqimages (TradeInRequestID, ImageFile) values (".$edititem.",'".escapestr($ofilename)."')";
                                    if(!mysql_query($query, $con)) $erroronsave = 'Could not add image file!';
                                }
                            }
                        }
                    }
                }
            }

            mysql_close($con);
        }
       unset($_SESSION['PostBack11']); 
       //echo'<pre>';print_r($fileerror);echo'</pre>';die('here');
    }

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = 'select Description, TradeInRequestID from tradeinrequests where MarketNeedID = '.$marketneedid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $tdesc = $row[0];
            $tid = $row[1];

            $query = "select ImageFile from tradeinreqimages where TradeInRequestID = ".$tid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $timage[$index] = $row[0];
                $index++;
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function trimAll(sString)
    {
        while(sString.substring(0,1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while(sString.substring(sString.length-1, sString.length) == ' ')
        {
            sString = sString.substring(0,sString.length-1);
        }
        return sString;
    }

    function validateFormOnSubmit()
    {
        var errorstr = "";
        var focusset = 0;

        var vdesc = document.getElementById("Description");
        var tString = trimAll(vdesc.value);
        if(tString.length == 0)
        {
            vdesc.style.background = '#ffcccc';
            errorstr += "Description is required."+'\n';
            vdesc.focus();
            focusset = 1;
        }
        else
        {
            vdesc.style.background = 'White';
        }

        var hiterror = 0;
        var hitcount = 0;
        var numberbox = document.getElementById("NumImages");
        for(var i = 0; i < parseInt(numberbox.value,10); i++)
        {
            var lastimage = "image"+(i+1);
            var vlast = document.getElementById(lastimage);
            var tString = trimAll(vlast.value);
            if(tString.length < 1)
            {
                hiterror = i+1;
                if(focusset == 0)
                {
                    vlast.focus();
                    focusset = 1;
                }
            }
            else hitcount++;
        }
        if(hiterror == 0)
        {
            for(var i = 0; ((i < parseInt(numberbox.value,10))&&(hiterror == 0)); i++)
            {
                var lastimage = "image"+(i+1);
                var vlast = document.getElementById(lastimage);
                for(var j = i+1; ((j < parseInt(numberbox.value,10)) && (hiterror == 0)); j++)
                {
                    var curimage = "image"+(j+1);
                    var vcur = document.getElementById(curimage);
                    if(vcur.value == vlast.value)
                    {
                        errorstr += "The Included Images must be unique and '"+vcur.value+"' was included twice."+'\n';
                        hiterror = 1;
                        if(focusset == 0)
                        {
                            vcur.focus();
                            focusset = 1;
                        }
                    }
                }
            }
        }
        //else
        //{
        //    errorstr += "All included Image lines must be filled in. Please remove any you are not using.  Number "+(hiterror)+" is one of the blank image lines."+'\n';
        //}

        if(errorstr != "")
        {
            alert("Errors that must be corrected:"+'\n'+errorstr+'\n');
            return false;
        }
        return true;
    }

    function addimagebox()
    {
        var numberbox = document.getElementById("NumImages");
        var nextone = parseInt(numberbox.value,10)+1;
        var x=document.getElementById('imagetable').insertRow(nextone-1);
        x.verticalAlign="baseline";
        var c1=x.insertCell(0);
        var c2=x.insertCell(1);
        var c3=x.insertCell(2);
        var c4=x.insertCell(3);
        c1.innerHTML="&nbsp;";
        c1.height = "30px";
        c2.innerHTML="&nbsp;";
        c2.width = "80px";
        c3.innerHTML='<input type="file" name="image'+nextone+'" id="image'+nextone+'" value="" />';
        c3.align="right";
        c4.innerHTML="&nbsp;";
        c4.width = "68px";
        numberbox.value = nextone;

        var rembox = document.getElementById("remimage");
        rembox.style.visibility = "visible";
    }

    function remimagebox()
    {
        var numberbox = document.getElementById("NumImages");
        var nextone = parseInt(numberbox.value,10)-1;
        if(nextone > -1)
        {
            document.getElementById('imagetable').deleteRow(nextone);
            numberbox.value = nextone;
            if(nextone == 0)
            {
                var rembox = document.getElementById("remimage");
                rembox.style.visibility = "hidden";
            }
        }
        else alert("Cannot Remove:"+'\n'+"All of the lines have already been deleted."+'\n'+'\n');
    }
</script>
<?php 
require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
<div id="content">
	
	<style>
		.welcome-message {
			text-align: center;
			color: #7bae2b;
		}
	</style>
	
	<?php if( @$_SESSION['welcome_message'] ) : ?>			
		<p class="welcome-message"><?php echo $_SESSION['welcome_message']; ?></p>
	<?php endif; ?>
	
	<?php 
		//unset $_SESSION for welcome message.
		unset( $_SESSION['welcome_message'] );
	?>
	
    <div class="grideightcontainer">
       <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Trade-In Quote Request</h1>
        <form action="requesttradeinquote.php" method="post" name="tradeinform" id="tradeinform" onsubmit="javascript:return validateFormOnSubmit();" enctype="multipart/form-data">
            <input type="hidden" value="true" name="PostBack" />
<?php
    $count = count($timage);
    echo '<input type="hidden" value="'.$count.'" id="OldImages" name="OldImages" />';
?>
            <input type="hidden" value="1" id="NumImages" name="NumImages" />
            <div class="grideightgrey">
                <div class="grideight">
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        The text box below is meant to give you a free form way of describing your trade-in vehicle details.
                        Once completed, your information will be saved and forwarded to your 1-800-vehicles.com representative immediately.
                        He or she will then contact you to discuss the value of your trade-in.
                    </p>
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        Please describe your vehicle details
                    </p>
                    <textarea style="width: 100%;height: 200px;" name="Description" id="Description" wrap="hard"><?php if(isset($tdesc) && (strlen($tdesc)>0)) echo $tdesc; ?></textarea>
                    <br clear="all" />
                    <br/>
                    If possible, please add at least one image of the vehicle (more are better and may be requested)<br/>
                    <br />
                    <table  id="imagetable" style="width: 60%;">
                        <tbody>
                            <tr valign="baseline">
                                <td height="30">
                                    <p style="margin: 0pt; font-size: 13px; font-weight: bold;">&nbsp;</p>
                                </td>
                                <td style="font-size: 11px; color: rgb(117, 117, 117);" width="80">Upload Pictures</td>
                                <td align="right">
                                    <input type="file" name="image1" id="image1" value="" />
                                </td>
                                <td width="68">
                                    <!--button value="" class="small"><nobr>BROWSE</nobr></button-->
                                    &nbsp;
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td height="30" align="right">&nbsp;</td>
                                <td valign="baseline" align="right">
                                    <button type="button" value="addimage" id="addimage" onclick="javascript:addimagebox();" class="small"><nobr>ADD PICTURE</nobr></button>
                                </td>
                                <td valign="baseline" align="left">
                                    <button type="button" value="remimage" id="remimage" onclick="javascript:remimagebox();" class="small"><nobr>REMOVE PICTURE</nobr></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br clear="all" />
<?php
    if($edititem == -1) echo '<p>Note: You must click the Submit Request Button below to save the picture(s) you add above.</p>';
    else echo '<p>Note: You must click the Save Requested Changes Button below to save the picture(s) you add above.</p>';
    if($count > 0)
    {
        echo 'Submitted Images<br/><center>';
        $max_width = 550;
        $max_height = 550;
        for($index = 0; $index < $count; $index++)
        {
            echo '<img class="img-responsive" src="loadimage.php?image='.$timage[$index].'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" /><br/>';
        }
        echo '</center><br clear="all" />';
    }
    if($edititem == -1) echo '<p><button type="submit" class="med"><nobr>SUBMIT REQUEST</nobr></button></p>';
    else echo '<p><button type="submit" class="med"><nobr>SAVE REQUEST CHANGES</nobr></button></p>';
?>
                </div><!-- end greyeight-->
            </div><!-- end greyeightgrey-->
        </form>
    </div><!-- end grideightgrey-->
<?php require("teaser.php"); ?>
</div><!--end content-->
<div style="display: none;">
	<pre>
		<?php print_r($_SESSION); ?>
	</pre>
</div>
<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
