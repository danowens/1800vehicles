<?php

if(isset($_SESSION['userid'])) $iscust = getuserprofile($_SESSION['userid'], 'Customer');
else $iscust = 'false';

if($iscust == 'true')
{
    $state = $_SESSION['state'];
    $substate = $_SESSION['substate'];
    $current = currentstep();
    if($current != $state)
    {
        if(!(($state == 0) && ($substate == 0)))
        {
            $state = $current;
            $substate = -1;
        }
    }
    switch($state)
    {
        case 1:
            switch($substate)
            {
                case 0:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li class="currenttab"><a href="learntheprocess.php"><span>Learn The Process</span></a></li>';
                    echo '<li><a href="researchvehicles.php"><span>Research Vehicles</span></a></li>';
                    echo '<li><a href="assessment.php"><span>Quick Assessment</span></a></li>';
                    echo '<li><a href="consult.php"><span>Request Consultation</span></a></li>';
                    echo '<li><a href="requestquote.php"><span>Request a Current Market Study</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="learntheprocess.php"><span>Learn The Process</span></a></li>';
                    echo '<li class="currenttab"><a href="researchvehicles.php"><span>Research Vehicles</span></a></li>';
                    echo '<li><a href="assessment.php"><span>Quick Assessment</span></a></li>';
                    echo '<li><a href="consult.php"><span>Request Consultation</span></a></li>';
                    echo '<li><a href="requestquote.php"><span>Request a Current Market Study</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 11:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="learntheprocess.php"><span>Learn The Process</span></a></li>';
                    echo '<li><a href="researchvehicles.php"><span>Research Vehicles</span></a></li>';
                    echo '<li class="currenttab"><a href="assessment.php"><span>Quick Assessment</span></a></li>';
                    echo '<li><a href="consult.php"><span>Request Consultation</span></a></li>';
                    echo '<li><a href="requestquote.php"><span>Request a Current Market Study</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 12:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="learntheprocess.php"><span>Learn The Process</span></a></li>';
                    echo '<li><a href="researchvehicles.php"><span>Research Vehicles</span></a></li>';
                    echo '<li><a href="assessment.php"><span>Quick Assessment</span></a></li>';
                    echo '<li class="currenttab"><a href="consult.php"><span>Request Consultation</span></a></li>';
                    echo '<li><a href="requestquote.php"><span>Request a Current Market Study</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 13:
                case 14:
                case 15:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="learntheprocess.php"><span>Learn The Process</span></a></li>';
                    echo '<li><a href="researchvehicles.php"><span>Research Vehicles</span></a></li>';
                    echo '<li><a href="assessment.php"><span>Quick Assessment</span></a></li>';
                    echo '<li><a href="consult.php"><span>Request Consultation</span></a></li>';
                    echo '<li class="currenttab"><a href="requestquote.php"><span>Request a Current Market Study</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 99:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="learntheprocess.php"><span>Learn The Process</span></a></li>';
                    echo '<li><a href="researchvehicles.php"><span>Research Vehicles</span></a></li>';
                    echo '<li><a href="assessment.php"><span>Quick Assessment</span></a></li>';
                    echo '<li><a href="consult.php"><span>Request Consultation</span></a></li>';
                    echo '<li><a href="requestquote.php"><span>Request a Current Market Study</span></a></li>';
                    echo '<li class="currenttab"><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                default:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="learntheprocess.php"><span>Learn The Process</span></a></li>';
                    echo '<li><a href="researchvehicles.php"><span>Research Vehicles</span></a></li>';
                    echo '<li><a href="assessment.php"><span>Quick Assessment</span></a></li>';
                    echo '<li><a href="consult.php"><span>Request Consultation</span></a></li>';
                    echo '<li><a href="requestquote.php"><span>Request a Current Market Study</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
            }
            break;
        case 2:
            switch($substate)
            {
                case 0:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li class="currenttab"><a href="allfirmquotes.php"><span>All Current Market Studies</span></a></li>';
                    echo '<li><a href="requesttradeinquote.php"><span>Request Trade-In Quote</span></a></li>';
                    echo '<li><a href="placeorder.php"><span>Place an Order</span></a></li>';
                    echo '<li><a href="posttogroup.php"><span>Post to the Group Watchlist</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 1:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="allfirmquotes.php"><span>All Current Market Studies</span></a></li>';
                    echo '<li class="currenttab"><a href="requesttradeinquote.php"><span>Request Trade-In Quote</span></a></li>';
                    echo '<li><a href="placeorder.php"><span>Place an Order</span></a></li>';
                    echo '<li><a href="posttogroup.php"><span>Post to the Group Watchlist</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 2:
                case 3:
                case 4:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="allfirmquotes.php"><span>All Current Market Studies</span></a></li>';
                    echo '<li><a href="requesttradeinquote.php"><span>Request Trade-In Quote</span></a></li>';
                    echo '<li class="currenttab"><a href="placeorder.php"><span>Place an Order</span></a></li>';
                    echo '<li><a href="posttogroup.php"><span>Post to the Group Watchlist</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 5:
                case 6:
                case 7:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="allfirmquotes.php"><span>All Current Market Studies</span></a></li>';
                    echo '<li><a href="requesttradeinquote.php"><span>Request Trade-In Quote</span></a></li>';
                    echo '<li><a href="placeorder.php"><span>Place an Order</span></a></li>';
                    echo '<li class="currenttab"><a href="posttogroup.php"><span>Post to the Group Watchlist</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 99:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="allfirmquotes.php"><span>All Current Market Studies</span></a></li>';
                    echo '<li><a href="requesttradeinquote.php"><span>Request Trade-In Quote</span></a></li>';
                    echo '<li><a href="placeorder.php"><span>Place an Order</span></a></li>';
                    echo '<li><a href="posttogroup.php"><span>Post to the Group Watchlist</span></a></li>';
                    echo '<li class="currenttab"><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                default:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="allfirmquotes.php"><span>All Current Market Studies</span></a></li>';
                    echo '<li><a href="requesttradeinquote.php"><span>Request Trade-In Quote</span></a></li>';
                    echo '<li><a href="placeorder.php"><span>Place an Order</span></a></li>';
                    echo '<li><a href="posttogroup.php"><span>Post to the Group Watchlist</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
            }
            break;
        case 3:
            switch($substate)
            {
                case 0:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li class="currenttab"><a href="orderdetails.php"><span>View My Order Details</span></a></li>';
                    echo '<li><a href="watchlist.php"><span>View My Watchlist Details</span></a></li>';
                    echo '<li><a href="svqlist.php"><span>All Specific Vehicle Quotes</span></a></li>';
                    echo '<li><a href="searchrequest.php"><span>Request a Search Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 1:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="orderdetails.php"><span>View My Order Details</span></a></li>';
                    echo '<li class="currenttab"><a href="watchlist.php"><span>View My Watchlist Details</span></a></li>';
                    echo '<li><a href="svqlist.php"><span>All Specific Vehicle Quotes</span></a></li>';
                    echo '<li><a href="searchrequest.php"><span>Request a Search Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 2:
                case 3:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="orderdetails.php"><span>View My Order Details</span></a></li>';
                    echo '<li><a href="watchlist.php"><span>View My Watchlist Details</span></a></li>';
                    echo '<li class="currenttab"><a href="svqlist.php"><span>All Specific Vehicle Quotes</span></a></li>';
                    echo '<li><a href="searchrequest.php"><span>Request a Search Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 4:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="orderdetails.php"><span>View My Order Details</span></a></li>';
                    echo '<li><a href="watchlist.php"><span>View My Watchlist Details</span></a></li>';
                    echo '<li><a href="svqlist.php"><span>All Specific Vehicle Quotes</span></a></li>';
                    echo '<li class="currenttab"><a href="searchrequest.php"><span>Request a Search Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 99:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="orderdetails.php"><span>View My Order Details</span></a></li>';
                    echo '<li><a href="watchlist.php"><span>View My Watchlist Details</span></a></li>';
                    echo '<li><a href="svqlist.php"><span>All Specific Vehicle Quotes</span></a></li>';
                    echo '<li><a href="searchrequest.php"><span>Request a Search Update</span></a></li>';
                    echo '<li class="currenttab"><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                default:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="orderdetails.php"><span>View My Order Details</span></a></li>';
                    echo '<li><a href="watchlist.php"><span>View My Watchlist Details</span></a></li>';
                    echo '<li><a href="svqlist.php"><span>All Specific Vehicle Quotes</span></a></li>';
                    echo '<li><a href="searchrequest.php"><span>Request a Search Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
            }
            break;
        case 4:
            switch($substate)
            {
                case 0:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li class="currenttab"><a href="vehicle.php"><span>Status of My Vehicle</span></a></li>';
                    echo '<li><a href="vehicleactionplan.php"><span>Action Plan Details</span></a></li>';
                    echo '<li><a href="vehicledelivery.php"><span>Vehicle Delivery</span></a></li>';
                    echo '<li><a href="progressrequest.php"><span>Request Progress Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 1:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="vehicle.php"><span>Status of My Vehicle</span></a></li>';
                    echo '<li class="currenttab"><a href="vehicleactionplan.php"><span>Action Plan Details</span></a></li>';
                    echo '<li><a href="vehicledelivery.php"><span>Vehicle Delivery</span></a></li>';
                    echo '<li><a href="progressrequest.php"><span>Request Progress Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 2:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="vehicle.php"><span>Status of My Vehicle</span></a></li>';
                    echo '<li><a href="vehicleactionplan.php"><span>Action Plan Details</span></a></li>';
                    echo '<li class="currenttab"><a href="vehicledelivery.php"><span>Vehicle Delivery</span></a></li>';
                    echo '<li><a href="progressrequest.php"><span>Request Progress Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 3:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="vehicle.php"><span>Status of My Vehicle</span></a></li>';
                    echo '<li><a href="vehicleactionplan.php"><span>Action Plan Details</span></a></li>';
                    echo '<li><a href="vehicledelivery.php"><span>Vehicle Delivery</span></a></li>';
                    echo '<li class="currenttab"><a href="progressrequest.php"><span>Request Progress Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                case 99:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="vehicle.php"><span>Status of My Vehicle</span></a></li>';
                    echo '<li><a href="vehicleactionplan.php"><span>Action Plan Details</span></a></li>';
                    echo '<li><a href="vehicledelivery.php"><span>Vehicle Delivery</span></a></li>';
                    echo '<li><a href="progressrequest.php"><span>Request Progress Update</span></a></li>';
                    echo '<li class="currenttab"><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
                default:
                    echo '<div class="gridtwelve"><div id="linetabs" class="droplinetabs"><ul>';
                    echo '<li><a href="vehicle.php"><span>Status of My Vehicle</span></a></li>';
                    echo '<li><a href="vehicleactionplan.php"><span>Action Plan Details</span></a></li>';
                    echo '<li><a href="vehicledelivery.php"><span>Vehicle Delivery</span></a></li>';
                    echo '<li><a href="progressrequest.php"><span>Request Progress Update</span></a></li>';
                    echo '<li><a href="mydashboard.php"><span>My Dashboard</span></a></li>';
                    echo '</ul></div><!--end linetabs--></div><!--end gridtwelve-->';
                    break;
            }
            break;
        default:
            break;
    }
}else{
	echo '<div class="gridtwelve"></div>';
}
?>
