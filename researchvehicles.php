<?php require("globals.php"); ?>
<?php

error_reporting(E_ALL);
ini_set("display_errors", "on");
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 1;
    $_SESSION['titleadd'] = 'Research Vehicles';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
         <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Research Vehicles
         <span style="float: right; margin-right: 10px; font-size: 16px;"> <a href="javascript:history.back()" style="color:black"><img src="images/back.png"></a></span></h1>
        <div class="grideightgrey">
            <p class="blackfourteen">
                Quickly find information on specific autos, SUVs and minivans that are 1 to 6 years old.
                <form action="researchspecific.php" method="post">
                    <button type="submit" value="" class="med"><nobr>RESEARCH SPECIFIC VEHICLES</nobr></button>
                </form>
            </p>
            <br/>
            <p class="blackfourteen">
                Quickly sort all autos, SUVs and minivans that are 1 to 6 years old to find out which ones fall within your price range.
                <form action="researchprice.php" method="post">
                    <button type="submit" value="" class="med"><nobr>RESEARCH BY PRICE RANGES</nobr></button>
                </form>
            </p>
            <br/>
            <p class="blackfourteen">
                These vehicles can be found in a shorter time frame with greater selection of colors, options and mileage.
                <form action="excellent.php" method="post">
                    <button type="submit" value="" class="med"><nobr>RESEARCH EXCELLENT AVAILABILITY</nobr></button>
                </form>
            </p>
        </div>
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
