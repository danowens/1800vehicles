<?php
    require("globals.php"); 
    require_once(WEB_ROOT_PATH.'common/functions/usernavfunctions.php');
    require_once(WEB_ROOT_PATH.'common/functions/franchiseefunctions.php');
    require_once(WEB_ROOT_PATH.'common/functions/string.php');

  
    
    
    if(ismorethancust($_SESSION['userid']) == 'true')
        $_SESSION['state'] = 5;
    else
        $_SESSION['state'] = currentstep();
    $_SESSION['substate'] = 99;
    $_SESSION['titleadd'] = 'My Dashboard';
  
    echo "<pre>";
    print_r($_SESSION);
    echo "</pre>";
    
    // Get some of the state of the marketneed and user variables we will need...
    $state = $_SESSION['state'];
    $substate = $_SESSION['substate'];

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];
    if(isset($_POST['ChangeMarketNeedTo']))
    {
        $_SESSION['marketneedid'] = $_POST['ChangeMarketNeedTo'];
        $marketneedid = $_SESSION['marketneedid'];

        // Update Last Login Info...
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);
            $uquery = "update marketneeds set lastlogin = '".date_at_timezone('Y-m-d H:i:s','GMT')."' where marketneedid = ".$_SESSION['marketneedid'];
            mysql_query($uquery, $con);
            mysql_close($con);
        }
    }

    $ucurrentstep = currentstep();
    $usalesrep = getsalesrep($userid, $marketneedid);

    $ucust = getuserprofile($userid, 'Customer');

    $hasaccess = ismorethancust($userid);
    $uadmin = getuserprofile($userid, 'Administrator');
    $uterr  = getuserprofile($userid, 'Teritory Admin');
    $ufran  = getuserprofile($userid, 'Franchisee');
    $ugm    = getuserprofile($userid, 'General Manager');
    $uops   = getuserprofile($userid, 'Operations Manager');
    $ubuyer = getuserprofile($userid, 'Researcher');
    $usrep  = getuserprofile($userid, 'Sales Representative');

    if(isset($_SESSION['curadmintab']))
    {
        if(($_SESSION['curadmintab'] == 'admin') && !($uadmin == 'true'))
        {
            unset($_SESSION['curadmintab']);
        }
        if(($_SESSION['curadmintab'] == 'terrrep') && !(($uadmin == 'true') || ($uterr == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        if(($_SESSION['curadmintab'] == 'franchise') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'genman') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'opsman') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'authbuyer') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ubuyer == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
        elseif(($_SESSION['curadmintab'] == 'salesrep') && !(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true') || ($usrep == 'true')))
        {
            unset($_SESSION['curadmintab']);
        }
    }

    if(isset($_SESSION['curadmintab']) && ($_SESSION['curadmintab'] != 'none')) $curadmintab = $_SESSION['curadmintab'];
    else
    {
        if($uadmin == 'true')
            $curadmintab = 'admin';
        elseif($uterr == 'true')
            $curadmintab = 'terrrep';
        elseif($ufran == 'true')
            $curadmintab = 'franchise';
        elseif($ugm == 'true')
            $curadmintab = 'genman';
        elseif($uops == 'true')
            $curadmintab = 'opsman';
        elseif($ubuyer == 'true')
            $curadmintab = 'authbuyer';
        elseif($usrep == 'true')
            $curadmintab = 'salesrep';
        else
            $curadmintab = 'none';

        $_SESSION['curadmintab'] = $curadmintab;
    }

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the current User Information...
        $query = 'select u.firstname, u.lastname, u.created, u.imagefile from users u where u.userid = '.$userid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $infirst = $row[0];
            $inlast = $row[1];
            $increated = $row[2];
            $inimage = $row[3];
        }

        // Get the Current Market Need Details...
        $query = 'select title, created from marketneeds where marketneedid = '.$marketneedid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $inmtitle = $row[0];
            $inmdate = $row[1];
        }

        // Get the Other Market Need Details...
        $query = 'select marketneedid, title, created, needscontact from marketneeds where active = 1 and showtouser = 1 and userid = '.$userid.' and marketneedid != '.$marketneedid.' order by created';
        $result = mysql_query($query, $con);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $inmnid[$index] = $row[0];
            $inmntitle[$index] = $row[1];
            $inmndate[$index] = $row[2];
            $inmnnc[$index] = $row[3];
            $index++;
        }

        // Get Messages to show on dashboard
        // TODO: limit messages to a reasonable number date descending
        //if(isset($marketneedid)) $query = "select created,message,fromid,msgid,fromtab from (select created 'created', message 'message', userfromid 'fromid', MessageID 'msgid',1 'fromtab' from messages where hidden=0 and ((usertoid = ".$userid." and marketneedid is null) or marketneedid = ".$marketneedid.") union all select created,updatetext,NULL, MarketUpdateID, 2 from marketupdates where hidden=0 and marketneedid = ".$marketneedid.") msglist order by 1 desc";
        //else $query = "select created 'created', message 'message', userfromid 'fromid', MessageID 'msgid',1 'fromtab' from messages where hidden=0 and usertoid = ".$userid." order by 1 desc";
        if(isset($marketneedid)) $query = "select created 'created', message 'message', userfromid 'fromid', messageid 'msgid',1 'fromtab' from messages where hidden=0 and ((usertoid = ".$userid." and marketneedid is null) or marketneedid = ".$marketneedid.") order by 1 desc";
        else $query = "select created 'created', message 'message', userfromid 'fromid', messageid 'msgid',1 'fromtab' from messages where hidden=0 and usertoid = ".$userid." order by 1 desc";
        $result = mysql_query($query, $con);
        $index = 0;
                $umsgcreated = array();
        while($result && $row = mysql_fetch_array($result))
        {
            $umsgcreated[$index] = $row[0];
            $umsgmessage[$index] = $row[1];
            $umsgfromid[$index] = $row[2];
            $umsgtabid[$index] = $row[3];
            $umsgfromtab[$index] = $row[4];
            $index++;
        }

        mysql_close($con);
    }

    // react to show all hidden button on salesrep dashboard
    if(isset($_POST['PostBack']) && isset($_POST['ShowAllHiddenCust']))
    {
        // list of market needs for this salesrep to show all of them to the rep all of them
        $needlist = getassignedmarketneeds($userid);
        if(strlen($needlist)>0)
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $sahquery = "update marketneeds set showtorep=1 where marketneedid in (".$needlist.")";
                mysql_query($sahquery, $con);

                mysql_close($con);
            }
        }
    }
    // react to show one hidden market need button on salesrep dashboard
    elseif(isset($_POST['PostBack']) && isset($_POST['ShowExperience']))
    {
        $needlist = $_POST['ShowExperience'];
        if(strlen($needlist)>0)
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $sahquery = "update marketneeds set showtorep=1 where marketneedid in (".$needlist.")";
                mysql_query($sahquery, $con);

                mysql_close($con);
            }
        }
    }
?>
<?php require("headerstart.php"); ?>
<script language="JavaScript">
/*
    var refreshinterval=30
    var displaycountdown="no"
    var starttime;
    var nowtime;
    var reloadseconds=0;
    var secondssinceloaded=0;

    function starttime()
    {
        starttime=new Date();
        starttime=starttime.getTime();
        countdown();
    }

    function countdown()
    {
        nowtime= new Date();
        nowtime=nowtime.getTime();
        secondssinceloaded=(nowtime-starttime)/1000;
        reloadseconds=Math.round(refreshinterval-secondssinceloaded);
        if(refreshinterval>=secondssinceloaded)
        {
            var timer=setTimeout("countdown()",1000);
            if(displaycountdown=="yes")
            {
                window.status="Page refreshing in "+reloadseconds+ " seconds";
            }
        }
        else
        {
            clearTimeout(timer);
            window.location.reload(true);
        }
    }

    window.onload=starttime;
*/

    function deleteempok()
    {
        var r=confirm("Are you sure you want to Remove the Employee from the Franchise?")
        if(r==true) return true;
        else return false;
    }

    // TODO: move to global functions
    // showhidediv = toggles a div open/closed and optionally changes the text of the activating element to match the next action
    // tdiv = the id of the div to show/hide
    // ttrigger = the id of the element triggering the action (optional)
    // Return = the converted string
    function showhidediv(tdiv, ttrigger)
    {
        var thediv = document.getElementById(tdiv);
        var thetrigger = document.getElementById(ttrigger);

        if (thediv.style.display == "block")
        {
            thediv.style.display = "none";
            thetrigger.innerHTML = "Show";
        }
        else
        {
            thediv.style.display = "block";
            thetrigger.innerHTML = "Hide";
        }
        return true;
    }

</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
        <table style="margin: 0px auto; width: 960px;">
            <tr><td width="900">
                <a name="dashtop"></a><h1 class="subhead">My Dashboard</h1>
                </td><td>
                <div style="float:right">
                    <a id="bbblink" class="sehzbus" href="http://www.bbb.org/nashville/business-reviews/franchising/1-800-vehiclescom-franchising-in-nashville-tn-37065588#bbblogo" title="1-800-Vehicles.com Franchising, Franchising, Nashville, TN" style="overflow: hidden; width: 100px; height: 38px; margin: -10px 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-nashville.bbb.org/logo/sehzbus/1-800-vehiclescom-franchising-37065588.png" width="200" height="38" alt="1-800-Vehicles.com Franchising, Franchising, Nashville, TN" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2F1-800-vehiclescom-franchising-37065588.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
                </div>
            </td></tr>
        </table>
<?php

    if($ucust == 'true')
    {
        echo '<div class="dashstrip" style="min-height: 310px;">';

        // Output the Basic Information...
        echo '<div class="gridfivedash">';
        echo '<h2 class="profile">BASIC INFORMATION</h2>';
        if(strlen($inimage)>0) $srctext=$inimage;
        else $srctext="userimages/nopicture.jpg";
        $max_width = 160;
        $max_height = 160;
        echo '<img class="profilepic" id="userimage" src="loadimage.php?image='.$srctext.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" />';
        echo '<h1 class="profilename">'.$infirst.' '.$inlast.'</h1>';
        echo '<p class="profiletitle">Member Since</p>';
        echo '<p class="profilestat" style="color:#000; font-size:12px;">'.date_at_timezone('F j, Y', 'GMT', $increated).'</p>';
        //if($usalesrep != -1) echo '<p class="profilestat" style="color:#000; font-size:12px;">Salesperson:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Salesperson:&nbsp;Unassigned</p>';
        //<p class="profilestat" style="color:#000; font-size:12px;">Salesperson: <a href="#">Dan Owens</a></p>
        //if($ucust == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">Customer:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Customer:&nbsp;No</p>';
        //if($uadmin == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">Admin:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Admin:&nbsp;No</p>';
        //if($uterr == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">Teritory Admin:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Teritory Admin:&nbsp;No</p>';
        //if($ufran == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">Franchisee:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Franchisee:&nbsp;No</p>';
        //if($uops == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">Operations Manager:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Operations Manager:&nbsp;No</p>';
        //if($ugm == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">General Manager:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">General Manager:&nbsp;No</p>';
        //if($ubuyer == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">Researcher:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Researcher:&nbsp;No</p>';
        //if($usrep == 'true') echo '<p class="profilestat" style="color:#000; font-size:12px;">Sales Representative:&nbsp;Yes</p>';
        //else echo '<p class="profilestat" style="color:#000; font-size:12px;">Sales Representative:&nbsp;No</p>';
        //echo $ucurrentstep;
        echo '<br clear="all" />';
        if(isset($inmnid))
        {
            $count = count($inmnid);
            if($count > 0)
            {
                echo '<p class="profiletitle">Current Experience:</p>';
                                if (isset($inmtitle) && isset($inmdate)) {
                                    echo '<p class="profilestat" style="color:#000; font-size:12px;">"'.$inmtitle.'" (Started: '.date_at_timezone('m/d/y', 'GMT', $inmdate).')</p>';
                                }
                echo '<p class="profiletitle">Other Experiences:</p>';
            }
            for($i = 0; $i < $count; $i++)
            {
                echo '<form action="'.WEB_SERVER_NAME.'mydashboard.php" method="post">';
                echo '<input type="hidden" value="'.$inmnid[$i].'" name="ChangeMarketNeedTo" />';
                echo '<p class="profilestat" style="color:#000; font-size:12px;">';
                echo '<button type="submit" value="" class="blueonwhite">"'.$inmntitle[$i].'" (Started: '.date_at_timezone('m/d/y', 'GMT', $inmndate[$i]).')</button>';
                echo '</p>';
                echo '</form>';
            }
        }
        //echo '<p class="profilestat" style="color:#000; font-size:12px;">To change the name of this vehicle purchase or to request another vehicle purchase prior to completing the current one, please contact your Sales Representative.</p>';
        echo '</div>';

        //Dashboard Step 1
        if($ucurrentstep < 2)
        {
            echo '<div class="gridthreepadded" style="float:right;">';
            //$margintop = 0;
            //require("teaser.php");
            echo '</div><!--end gridthreepadded -->';
        }
        //Dashboard Step 2
        elseif($ucurrentstep == 2)
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $query = 'select quoterequestid, year, make, model, style, created, quotetype from quoterequests where visible=1 and marketneedid='.$marketneedid;
                $result = mysql_query($query);
                $reqind = 0;
                $recind = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    $veh_qid = $row['quoterequestid'];

                    $firmquery = "select ordertype, pricepoint, firmquoteid from firmquotes where visible=1 and updatestatus = 'Completed' and quoterequestid = ".$veh_qid;
                    $firmresult = mysql_query($firmquery);
                    if($firmrow = mysql_fetch_array($firmresult))
                    {
                        $rec_qid[$recind] = $veh_qid;
                        $rec_year[$recind] = $row['year'];
                        $rec_make[$recind] = $row['make'];
                        $rec_model[$recind] = $row['model'];
                        $rec_style[$recind] = $row['style'];
                        $rec_status[$recind] = $row['updatestatus'];
                        $rec_cre[$recind] = $row['created'];
                        $rec_type[$recind] = $row['quotetype'];
                        $rec_order[$recind] = $firmrow['ordertype'];
                        $rec_price[$recind] = $firmrow['pricepoint'];
                        $rec_fid[$recind] = $firmrow['firmquoteid'];
                        $recind++;
                    }
                    else
                    {
                        $req_qid[$reqind] = $veh_qid;
                        $req_year[$reqind] = $row['year'];
                        $req_make[$reqind] = $row['make'];
                        $req_model[$reqind] = $row['model'];
                        $req_style[$reqind] = $row['style'];
                        $req_status[$reqind] = $row['updatestatus'];
                        $req_cre[$reqind] = $row['created'];
                        $req_type[$reqind] = $row['quotetype'];
                        $req_order[$reqind] = 'Pending';
                        $req_price[$reqind] = 0;
                        $req_fid[$reqind] = -1;
                        $reqind++;
                    }
                }

                mysql_close($con);
            }

            // Any Current Market Studies (Firm Quotes) Requested?
            $count = count($req_qid);
            if($count > 0)
            {
                echo '<div class="gridfourdashpadded" style="margin-top: 10px;border-right-style: none;">';
                                $plural = pluralize_noun($count, "Current Market Stud", "ies", "y");
                echo "<h3 class=\"profileinfo\">$plural</h3>";
                echo '<p class="profiletitle">Pending</p>';
                for($i=0;$i<$count;$i++)
                {
                    echo '<p class="profilestat">';
                    echo '<a href="'.WEB_SERVER_NAME.'quote.php?EditQuoteID='.$req_qid[$i].'">';
                    echo $req_year[$i].' '.$req_make[$i].' '.$req_model[$i].' '.$req_style[$i];
                    echo '</a> ';
                    echo '('.date_at_timezone('m/d/y', 'GMT', $req_cre[$i]).')';
                    echo '</p>';
                }
                echo '<form action="'.WEB_SERVER_NAME.'requestquote.php" method="post">';
                echo '<button type="submit" value="" class="small">NEW CURRENT MARKET STUDY</button>';
                echo '</form></div>';
            }
            else
            {
                echo '<div class="gridfourdashpadded" style="margin-top: 10px;border-right-style: none;">';
                echo '<h3 class="profileinfo">Current Market Studies Requested</h3>';
                echo '<form action="'.WEB_SERVER_NAME.'requestquote.php" method="post">';
                echo '<button type="submit" style="margin-top:10px;" class="small">REQUEST A CURRENT MARKET STUDY</button>';
                echo '</form></div>';
            }

            // Any Current Market Studies (Firm Quotes) Received Yet?
            $count = count($rec_qid);
            if($count > 0)
            {
                echo '<div class="gridfourdashpadded" style="margin-top: 10px;border-right-style: none;">';
                                $plural = pluralize_noun($count, "Current Market Stud", "ies", "y");
                echo "<h3 class=\"profileinfo\">$plural</h3>";
                echo '<p class="profiletitle">Completed</p>';
                $havegroup = 0;
                $haveorder = 0;
                for($i=0;$i<$count;$i++)
                {
                    echo '<p class="profilestat">';
                    echo '<a href="'.WEB_SERVER_NAME.'quotereceived.php?QuoteID='.$rec_qid[$i].'">';
                    echo $rec_year[$i].' '.$rec_make[$i].' '.$rec_model[$i].' '.$rec_style[$i].' $'.number_format($rec_price[$i]);
                    echo '</a> ';
                    echo '('.date_at_timezone('m/d/y', 'GMT', $rec_cre[$i]).')';
                    echo '</p>';
                    echo '<p class="profilestat">AVAILABILITY: ';
                    if($rec_order[$i] == 'Standard')
                    {
                        echo 'STANDARD ORDER';
                        $haveorder = 1;
                    }
                    elseif($rec_order[$i] == 'Special')
                    {
                        echo 'SELECT ORDER';
                        $haveorder = 1;
                    }
                    elseif($rec_order[$i] == 'Group')
                    {
                        echo 'GROUP WATCHLIST';
                        $havegroup = 1;
                    }
                    else echo 'NO AVAILABILITY';
                    //if($rec_order[$i] == 'Group')
                    //{
                    //    echo '<form action="posttogroupterms.php" method="post">';
                    //    echo '<input type="hidden" value="'.$rec_fid[$i].'" name="FirmQuoteID[]" />';
                    //    echo '<button type="submit" value="" class="small">POST TO WATCHLIST</button>';
                    //}
                    //elseif(($rec_order[$i] == 'Standard')||($rec_order[$i] == 'Special'))
                    //{
                    //    echo '<form action="placeorderterms.php" method="post">';
                    //    echo '<input type="hidden" value="'.$rec_fid[$i].'" name="FirmQuoteID[]" />';
                    //    echo '<button type="submit" value="" class="small">PLACE AN ORDER</button></form>';
                    //}
                    echo '</p>';
                }
                if($haveorder == 1)
                {
                    echo '<form action="'.WEB_SERVER_NAME.'placeorder.php" method="post" style="float:left;">';
                    //echo '<input type="hidden" value="'.$rec_fid[$i].'" name="FirmQuoteID[]" />';
                    echo '<button type="submit" value="" class="small">PLACE AN ORDER</button></form>';
                }
                if($havegroup == 1)
                {
                    echo '<form action="'.WEB_SERVER_NAME.'posttogroup.php" method="post" style="float:right;">';
                    //echo '<input type="hidden" value="'.$rec_fid[$i].'" name="FirmQuoteID[]" />';
                    echo '<button type="submit" value="" class="small">POST TO WATCHLIST</button></form>';
                }
                //echo '<p align="right" style="margin: 0; padding:0 15px 10px 0;"> <a href="#">More </a></p>';
                echo '</div><!--end gridfourdashpadded -->';
            }
            else
            {
                echo '<div class="gridfourdashpadded" style="margin-top: 10px;border-right-style: none;">';
                echo '<h3 class="profileinfo">Current Market Studies Received</h3>';
                echo '</div><!--end gridfourdashpadded -->';
            }
        }
        //Dashboard Step 3
        elseif($ucurrentstep == 3)
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);
                $query = 'select v.vehicledetailid, s.created, s.lastupdated, v.year, m.name, v.model, v.style, s.pricequoted, s.specificvehicleid from specificvehicles s, vehicledetails v, makes m where m.makeid=v.makeid and v.vehicledetailid=s.vehicledetailid and s.marketneedid='.$marketneedid;
                $result = mysql_query($query);
                $index = 0;
                $pindex = 0;
                while($result && $row = mysql_fetch_array($result))
                {
                    // Get the current Purchase Approval Information (if any)...
                    $pquery = 'select status, chosenon, boughton, salesreviewed from purchases where specificvehicleid = '.$row[10];
                    $presult = mysql_query($pquery, $con);
                    if($presult)
                    {
                        if($prow = mysql_fetch_array($presult))
                        {
                            $psvqstatus[$pindex] = $prow[0];
                            $psvqchosen[$pindex] = $prow[1];
                            $psvqbought[$pindex] = $prow[2];
                            $psvqid[$pindex] = $row[0];
                            $psvqcreated[$pindex] = $row[1];
                            $psvqupdated[$pindex] = $row[2];
                            $psvqyear[$pindex] = $row[3];
                            $psvqmake[$pindex] = $row[4];
                            $psvqmodel[$pindex] = $row[5];
                            $psvqstyle[$pindex] = $row[6];
                            $psvqprice[$pindex] = $row[7];
                            $psvqsid[$pindex] = $row[8];
                            $pindex++;
                        }
                        else
                        {
                            $svqstatus[$index] = 'Pending';
                            $svqid[$index] = $row[0];
                            $svqcreated[$index] = $row[1];
                            $svqupdated[$index] = $row[2];
                            $svqyear[$index] = $row[3];
                            $svqmake[$index] = $row[4];
                            $svqmodel[$index] = $row[5];
                            $svqstyle[$index] = $row[6];
                            $svqprice[$index] = $row[7];
                            $svqsid[$index] = $row[8];
                            $index++;
                        }
                    }
                    else
                    {
                        $svqstatus[$index] = 'Pending';
                        $svqid[$index] = $row[0];
                        $svqcreated[$index] = $row[1];
                        $svqupdated[$index] = $row[2];
                        $svqyear[$index] = $row[3];
                        $svqmake[$index] = $row[4];
                        $svqmodel[$index] = $row[5];
                        $svqstyle[$index] = $row[6];
                        $svqprice[$index] = $row[7];
                        $svqsid[$index] = $row[8];
                        $index++;
                    }
                }

                mysql_close($con);
            }
            // Specific Vehicles to consider?
            echo '<div class="gridfourdashpadded" style="margin-top: 10px;border-right-style: none;">';
            echo '<h3 class="profileinfo">Specific Vehicles to Consider</h3>';
            $count = count($svqstatus);
            if($count > 0)
            {
                for($i=0;$i<$count;$i++)
                {
                    echo '<p class="profilestat" style="padding-top:10px;">';
                    echo '<a href="'.WEB_SERVER_NAME.'specificquote.php?SVQID='.$svqsid[$i].'">';
                    echo $svqyear[$i].' '.$svqmake[$i].' '.$svqmodel[$i].' '.$svqstyle[$i].' $'.number_format($svqprice[$i]);
                    echo '</a> ';
                    echo '('.date_at_timezone('m/d/y', 'GMT', $svqupdated[$i]).')</p>';
                }
            }
            else echo '<p class="profilestat" style="font-size: 13px; padding-top: 10px; color:#85c11b;">No SVQs to Review</p>';
            echo '</div>';

            // Quotes on Specific Vehicles?
            echo '<div class="gridfourdashpadded" style="margin-top: 10px;border-right-style: none;">';
            echo '<h3 class="profileinfo">Recent Specific Vehicle Quotes</h3>';
            $count = count($psvqstatus);
            if($count > 0)
            {
                for($i=0;$i<$count;$i++)
                {
                    echo '<p class="profilestat" style="padding-top:10px;">';
                    echo '<a href="'.WEB_SERVER_NAME.'specificquote.php?SVQID='.$psvqsid[$i].'">';
                    echo $psvqyear[$i].' '.$psvqmake[$i].' '.$psvqmodel[$i].' '.$psvqstyle[$i].' $'.number_format($psvqprice[$i]);
                    echo '</a> ';
                    echo '('.date_at_timezone('m/d/y', 'GMT', $psvqupdated[$i]).')</p>';
                        $psvqstatus[$pindex] = 'Specific Vehicle Being Purchased';
                        $psvqchosen[$pindex] = $prow[1];
                        $psvqbought[$pindex] = $prow[2];
                    echo '<p class="profilestat" style="color:#85c11b;">Approved on ('.date_at_timezone('m/d/y', 'GMT', $psvqchosen[$i]).')</p>';
                    echo '<p class="profilestat" style="color:#85c11b;">';
                    if($psvqstatus[$pindex] == 'Not Bought') echo 'WHOLESALE PURCHASE PENDING</p>';
                    elseif($psvqstatus[$pindex] == 'Bought') echo 'WHOLESALE PURCHASE COMPLETED</p>';
                    else echo 'WHOLESALE PURCHASE NOT MADE</p>';
                }
                //echo '<p align="right" style="margin: 0; padding:0 0 10px 0;"> <a href="svqlist.php">More </a></p>';
            }
            else echo '<p class="profilestat" style="font-size: 13px; padding-top: 10px; color:#85c11b;">No Recent SVQs</p>';
            echo '</div><!--end gridfourdashpadded -->';
        }
        //Dashboard Step 4
        else
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);
                $query = "select v.vehicledetailid, s.created, s.lastupdated, v.year, m.name, v.model, v.style, s.pricequoted, s.specificvehicleid, p.boughton from purchases p, specificvehicles s, vehicledetails v, makes m where p.status='Bought' and s.specificvehicleid=p.specificvehicleid and m.makeid=v.makeid and v.vehicledetailid=s.vehicledetailid and s.marketneedid=".$marketneedid;
                $result = mysql_query($query);
                if($result && $row = mysql_fetch_array($result))
                {
                    $svqid = $row[0];
                    $svqcreated = $row[1];
                    $svqupdated = $row[2];
                    $svqyear = $row[3];
                    $svqmake = $row[4];
                    $svqmodel = $row[5];
                    $svqstyle = $row[6];
                    $svqprice = $row[7];
                    $svqsid = $row[8];
                    $psvqbought = $row[9];
                    $svqstatus = 'Bought';

                    // Look for images of the vehicle in question...
                    $query = "select count(*) from specificvehicles s, vehicleimages i where s.vehicledetailid=i.vehicledetailid and s.specificvehicleid=".$svqsid;
                    $result = mysql_query($query);
                    if($result && $row = mysql_fetch_array($result))
                    {
                        $svqimages = $row[0];
                    }
                    else $svqimages = 0;

                    if($svqimages > 0)
                    {
                        $query = "select i.imagefile from specificvehicles s, vehicleimages i where s.vehicledetailid=i.vehicledetailid and i.displayorder=1 and s.specificvehicleid=".$svqsid;
                        $result = mysql_query($query);
                        if($result && $row = mysql_fetch_array($result))
                        {
                            $svqimage = $row[0];
                        }
                    }

                    echo '<div class="gridfourdashpadded" style="margin-top: 10px; width:508px; border-right-style:none;">';
                    echo '<h3 class="profileinfo" style="color:#216dce; font-size:19px; margin-top:-5px;">CONGRATULATIONS!  YOUR VEHICLE IS ON ITS WAY!</h3>';
                    echo '<a href="'.WEB_SERVER_NAME.'specificordered.php?SVQID='.$svqsid.'">';
                    if(isset($svqimage))
                    {
                        $max_width = 225;
                        $max_height = 225;
                        echo '<img id="vehimage" src="loadimage.php?image='.$svqimage.'&mwidth='.$max_width.'&mheight='.$max_height.'" align="right" style="margin-top:10px;" />';
                    }
                    else echo '** Image not available **';
                    echo '</a>';
                    echo '<p style="font-size:15px;">';
                    echo '<a href="'.WEB_SERVER_NAME.'specificordered.php?SVQID='.$svqsid.'">';
                    echo $svqyear.' '.$svqmake.' '.$svqmodel.' '.$svqstyle;
                    echo '</a></p>';
                    echo '<table style="font-size:14px; margin-left:-3px; margin-bottom: 5px; margin-top: -10px;" border="0" cellspacing="3" align="left">';
                    echo '<tr>';
                    echo '<td width="175">Status Of My Vehicle</td>';
                    echo '<td width="75" style="color:#9a9a9a;">';
                    //echo 'In transit'; 
                    $pquery = "select purchaseid from purchases where specificvehicleid=".$svqsid;
                    $presult = mysql_query($pquery, $con);
                    if($prow = mysql_fetch_array($presult))
                    {
                        $cquery = "select * from specificactions where purchaseid=".$prow[0];
                        $cresult = mysql_query($cquery, $con);
                        if(!$crow = mysql_fetch_array($cresult)) echo 'Pending';
                        else
                        {
                            $sumquery = "select count(*) from specificactions sa, specificcategories sc, specificitems si where si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$prow[0];
                            $sumresult = mysql_query($sumquery);
                            if($sumrow = mysql_fetch_array($sumresult))
                            {
                                $total = $sumrow[0];
                                $sumquery = "select count(*) from specificactions sa, specificcategories sc, specificitems si where si.completed=1 and si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$prow[0];
                                $sumresult = mysql_query($sumquery);
                                if($sumrow = mysql_fetch_array($sumresult))
                                {
                                    $completed = $sumrow[0];
                                    $sumquery = "select si.name from specificactions sa, specificcategories sc, specificitems si where si.completed=0 and si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$prow[0]." order by sc.displayorder asc, si.displayorder asc";
                                    $sumresult = mysql_query($sumquery);
                                    if($sumrow = mysql_fetch_array($sumresult))
                                    {
                                        $itemname = $sumrow[0];
                                        echo $itemname.' ['.$completed.' of '.$total.']';
                                    }
                                    else echo $completed.' of '.$total.' Completed';
                                }
                                else echo '0 of '.$total.' Completed';
                            }
                            else echo 'Pending';
                        }
                    }
                    else echo 'Wholesale Purchase Made';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>Target Delivery Date: </td>';
                    echo '<td style="color:#9a9a9a;">';
                    echo 'Pending';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>Balance Due:</td>';
                    echo '<td style="color:#85c11b;">$'.number_format($svqprice-250).'</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>Temporary Tag Expiration:</td>';
                    echo '<td style="color:#9a9a9a;">';
                    //if(!is_null($svqtag)) echo date_at_timezone('m/d/y', 'GMT', $svqtag);
                    //else echo 'Pending';
                    echo 'Pending';
                    echo '</td>';
                    echo '</tr>';
                    echo '</table>';
                    //echo '<br clear="all" />';
                    echo '<a href="'.WEB_SERVER_NAME.'vehicleactionplan.php"><button type="submit" value="" class="med">VIEW DETAILS OF PROGRESS</button></a><br />';
                    echo '<a href="'.WEB_SERVER_NAME.'progressrequest.php"><button type="submit" value="" class="med" style="margin-top:10px;">REQUEST A PROGRESS UPDATE</button></a>';
                    echo '</div><!--end gridfourdashpadded -->';
                }

                mysql_close($con);
            }
        }

        echo '</div> <!-- end dashstrip -->';

        echo '<div class="dashnav2" style="clear: all;">';
        echo '<strong>Common Actions</strong> <a href="'.WEB_SERVER_NAME.'myaccount.php">Update My Account</a> | <a href="'.WEB_SERVER_NAME.'viewagreements.php">View Agreements</a>';
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);
            $query = 'select assessmentid from assessments where marketneedid = '.$marketneedid;
            $result = mysql_query($query, $con);
            if($result && $row = mysql_fetch_array($result)) echo ' | <a href="'.WEB_SERVER_NAME.'assessment.php">View My Quick Assessment</a>';
            else echo ' | <a href="'.WEB_SERVER_NAME.'assessment.php">Take the Quick Assessment</a>';
            mysql_close($con);
        }
        echo ' | <a href="'.WEB_SERVER_NAME.'favorites.php">View My Favorites</a>';
        if($ucurrentstep > 2) echo ' | <a href="'.WEB_SERVER_NAME.'allfirmquotes.php">All Current Market Studies</a>';
        if($ucurrentstep > 3) echo ' | <a href="'.WEB_SERVER_NAME.'svqlist.php">All Specific Vehicle Quotes</a>';
        echo '</div>';
    }
    if($hasaccess == 'true')  // Add the Backend Piece...
    {
        echo '<div class="dashnav2" style="clear: all;">';
        echo 'General: &raquo;<a href="'.WEB_SERVER_NAME.'myaccount.php">My Account</a>&nbsp;&nbsp;&nbsp;';
        echo '&raquo;<a href="'.WEB_SERVER_NAME.'groupwatch.php">Group Watchlist</a>';
        echo '<br/>Inventory: &raquo;<a href="'.WEB_SERVER_NAME.'mywholesale.php">My Wholesale Inventory</a>&nbsp;&nbsp;&nbsp;';
        echo '&raquo;<a href="'.WEB_SERVER_NAME.'allwholesale.php">All Wholesale Inventory</a>';
        if($uadmin == 'true')
        {
            echo '<br/>Agreements: &raquo;<a href="'.WEB_SERVER_NAME.'editagreement.php?AgreementName=Standard%20Order">Standard Agreement</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="'.WEB_SERVER_NAME.'editagreement.php?AgreementName=Special%20Order">Special Agreement</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="'.WEB_SERVER_NAME.'editagreement.php?AgreementName=Group%20Watchlist%20Posting">Group Posting Agreement</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="'.WEB_SERVER_NAME.'editagreement.php?AgreementName=Specific%20Vehicle%20Purchase">Purchase Agreement</a>';
            echo '<br/>Setup: &raquo;<a href="'.WEB_SERVER_NAME.'franchiseesetup.php">Franchisee Setup</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="'.WEB_SERVER_NAME.'userfunctions.php">User Setup</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="'.WEB_SERVER_NAME.'recentcustlist.php">Recent Customer List</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="http://1800vehicles.com/admin/emailexport.php">Email Export</a>';
            echo '<br/>Vehicles: &raquo;<a href="'.WEB_SERVER_NAME.'allvehicles.php">Vehicle Setup</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="'.WEB_SERVER_NAME.'virtualvehicles.php">Virtual Vehicle Mass Changes</a>&nbsp;&nbsp;&nbsp;';
            echo '&raquo;<a href="'.WEB_SERVER_NAME.'virtualvehiclegroups.php">Virtual Vehicle Groups</a>&nbsp;&nbsp;&nbsp;';
        }
        echo '</div> <!-- end dashstrip -->';
    }

    if($hasaccess == 'true')
    {
        echo '<div class="dashstrip" style="min-height: 0px; margin: 0 auto; padding-bottom: 10px;">';
        // Set up the Tab Menu for those with access to it...
        echo '<a name="admintab"></a><div id="droplinetabs" class="admintabs">';
        echo '<ul>';
        if($uadmin == 'true')
        {
            if($curadmintab == 'admin') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=admin"><span>Admin</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=admin"><span>Admin</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true'))
        {
            if($curadmintab == 'terrrep') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=terrrep"><span>Territory Rep</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=terrrep"><span>Territory Rep</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true'))
        {
            if($curadmintab == 'franchise') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=franchise"><span>Franchisee</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=franchise"><span>Franchisee</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true'))
        {
            if($curadmintab == 'genman') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=genman"><span>General Manager</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=genman"><span>General Manager</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true'))
        {
            if($curadmintab == 'opsman') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=opsman"><span>Operations Manager</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=opsman"><span>Operations Manager</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ubuyer == 'true'))
        {
            if($curadmintab == 'authbuyer') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=authbuyer"><span>Researcher</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=authbuyer"><span>Researcher</span></a></li>';
        }
        if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true') || ($usrep == 'true'))
        {
            if($curadmintab == 'salesrep') echo '<li class="admincurrent"><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=salesrep"><span>Sales Rep</span></a></li>';
            else echo '<li><a href="'.WEB_SERVER_NAME.'switchadmintab.php?toadmintab=salesrep"><span>Sales Rep</span></a></li>';
        }
        echo '</ul>';
        echo '</div>';

        // Add the currently selected menu...
        if($curadmintab == 'admin')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight" style="width:930px;">';
            //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Watchlist Vehicles --</strong></p>';
            echo '<p>Coming Soon!</p>';
            echo '</div><!-- endgrideight-->';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'terrrep')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight" style="width:930px;">';
            //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Watchlist Vehicles --</strong></p>';
            //echo '<p>Coming Soon!</p>';
            // Get the ones not in a franchise yet for the Admin to see...
            if($uadmin == 'true')
            {
                $userlist = getnonfranunassignedleads($userid);
                if(strlen($userlist) > 0)
                {
                    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                    if($con)
                    {
                        mysql_select_db(DB_SERVER_DATABASE, $con);

                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '</div><!-- endgridfullgrey -->';
                        echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                        echo '<div class="grideight" style="width:930px;">';
                        echo '<h4 class="subhead">Unassigned No Franchisee Leads</h4>';
                        echo '<table width="900" border="0" cellpadding="3">';
                        echo '<tr style="color:#85c11b; font-size:15px;">';
                        echo '<td width="300"><strong>Name (Email)</strong></td>';
                        echo '<td width="300"><strong>Market Need</strong></td>';
                        echo '<td width="300"><strong>Created</strong></td>';
                        echo '<td width="125"><strong>Referred By</strong></td>';
                        echo '<td width="125"><strong>Rep Name</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';

                        $query = "select u.userid, concat(u.firstname,' ',u.lastname,'<br/>(',u.email,')') AS fullname, u.referredby, u.repname, m.marketneedid, m.title, m.created from users u, marketneeds m where u.userid=m.userid and u.userid in (".$userlist.") order by u.lastupdated desc,2";
                        $result = mysql_query($query, $con);
                        while($result && $row = mysql_fetch_array($result))
                        {
                            $aquery = "select * from assignedreps where marketneedid = ".$row[4];
                            $aresult = mysql_query($aquery, $con);
                            if(!$arow = mysql_fetch_array($aresult))
                            {
                                echo '<tr valign="baseline">';
                                echo '<td><strong>'.$row[1].'</strong></td>';
                                echo '<td>'.$row[5].'</td>';
                                echo '<td>'.$row[6].'</td>';
                                echo '<td>'.$row[2].'</td>';
                                echo '<td>'.$row[3].'</td>';
                                echo '<td><form action="'.WEB_SERVER_NAME.'addassignedrep.php" method="post">';
                                echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                                echo '<input type="hidden" value="'.$row[4].'" name="AddMarketID" />';
                                echo '<input type="hidden" value="'.$row[0].'" name="ForUserID" />';
                                echo '<input type="hidden" value="'.$row[1].'" name="ForUserName" />';
                                echo '<input type="hidden" value="'.$row[5].'" name="ForUserNeed" />';
                                if(isset($row[2]) && strlen($row[2]) > 0) $iuserref = $row[2];
                                else $iuserref = 'None Specified';
                                echo '<input type="hidden" value="'.$iuserref.'" name="ForUserRef" />';
                                if(isset($row[3]) && strlen($row[3]) > 0) $iuserrep = $row[3];
                                else $iuserrep = 'None Specified';
                                echo '<input type="hidden" value="'.$iuserrep.'" name="ForUserRep" />';
                                echo '<button type="submit" value="" class="med">ASSIGN</button>';
                                echo '</form></td>';
                                echo '</tr>';
                            }
                        }

                        echo '</table>';
                        //echo '<br clear="all" />';
                        //echo '<button type="submit" value="" class="med" style="margin-top:10px;">DELETE</button>';
                        echo '</div><!-- endgrideight-->';
                    }

                    mysql_close($con);
                }
            }
            echo '</div><!-- endgrideight-->';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'franchise')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight" style="width:930px;">';

            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $query = "select fm.franchiseeid, f.name from franchiseemembers fm, franchisees f where userid=".$userid." and fm.franchiseeid=f.franchiseeid";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];
                    $ifranname = $row[1];

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>Franchise:'.$ifranname.'</strong></p>';
                    echo '<div class="grideight" style="width:930px;">';

                    // employees
                    echo '<h4 class="subhead">Employees</h4>';
                    //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Employees --</strong></p>';
                    echo '<table border="0" width="900" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:17px;">';
                    echo '<td width="300"><strong>Employee</strong></td>';
                    echo '<td width="300"><strong>Responsibilities</strong></td>';
                    echo '<td width="25" align="right"><strong>Actions</strong></td>';
                    echo '<td width="10" align="left"><strong>&nbsp;</strong></td>';
                    echo '</tr>';

                    // Profile Names
                    $iprofilename[1] = "Customer";
                    $iprofilename[2] = "Administrator";
                    $iprofilename[3] = "Territory&nbsp;Administrator";
                    $iprofilename[4] = "Franchisee";
                    $iprofilename[5] = "Operations&nbsp;Manager";
                    $iprofilename[6] = "General&nbsp;Manager";
                    $iprofilename[7] = "Authorized&nbsp;Buyer";
                    $iprofilename[8] = "Sales&nbsp;Representative";

                    $query = "select m.userid, concat(u.lastname,', ',u.firstname,' (',u.email,')') AS fullname, m.abletocontrol, group_concat(up.profileid)";
                    $query .= " from franchiseemembers m, users u, userprofiles up";
                    $query .= " where u.userid=m.userid and up.userid=u.userid and m.franchiseeid=".$ifranid." group by up.userid order by up.profileid, fullname";

                    //$query = "select m.UserID, concat(u.LastName,', ',u.FirstName,' (',u.Email,')') AS FullName from FranchiseeMembers m, Users u where u.UserId=m.UserID and m.FranchiseeID=".$ifranid." order by 2";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $ifuserid = $row[0];
                        $iffull = $row[1];
                        $ifcontrol = $row[2];
                        $ifprofiles = $row[3];

                        // TODO: roll this up into usernavfunctions.php (keep single code copy of the profiles table to avoid lookups)
                        $pieces = explode(',', $ifprofiles);
                        $count = count($pieces);
                        sort($pieces);
                        $ifroles = '';
                        for ($i = 0; $i < $count; $i++)
                        {
                            $ifroles .= $iprofilename[$pieces[$i]];
                            if ($i < ($count - 1)) $ifroles .= ', ';
                        }

                        {
                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$iffull.'</strong></td>';
                            echo '<td>'.$ifroles.'</td>';
                            echo '<td align="right"><form action="'.WEB_SERVER_NAME.'changeuserresp.php" method="post">';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="ChangeUserID" />';
                            echo '<input type="hidden" value="true" name="NoGMOption" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Employee Responsibilities" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form>';
                            echo '</td><td align="left">';
                            echo '<form action="'.WEB_SERVER_NAME.'delmember.php" method="post" onsubmit="javascript:return deleteempok();" >';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="MemberID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Employee from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }

                    echo '</table>';
                    echo '<form action="'.WEB_SERVER_NAME.'adduser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD NEW EMPLOYEE</button>';
                    echo '</form>';
                    echo '<form action="'.WEB_SERVER_NAME.'addreguser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD REGISTERED EMPLOYEE</button>';
                    echo '</form>';
                    echo '</div>';
                }
            }
/* 
            // employees
            echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Employees --</strong></p>';
            echo '<table border="0" width="900" cellpadding="3">';
            echo '<tr style="color:#85c11b; font-size:17px;">';
            echo '<td width="300"><strong>Employee</strong></td>';
            echo '<td width="300"><strong>Responsibilities</strong></td>';
            echo '<td width="25" align="right"><strong>Actions</strong></td>';
            echo '<td width="10" align="left"><strong>&nbsp;</strong></td>';
            echo '</tr>';
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            $ifranid = -1;
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                //$query = "select FranchiseeID from FranchiseeMembers where AbleToControl=1 and UserId=".$userid;
                $query = "select franchiseeid from franchiseemembers where userid=".$userid;
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];

                    $query = "select m.userid, concat(u.lastname,', ',u.firstname,' (',u.email,')') AS fullname from franchiseemembers m, users u where u.userid=m.userid and m.franchiseeid=".$ifranid." order by 2";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $ifuserid = $row[0];
                        $iffull = $row[1];

                        if($ifuserid != $userid)
                        {
                            $respstr = '';
                            if(getuserprofile($ifuserid, 'General Manager') == 'true') $respstr .= 'General Manager';
                            if(getuserprofile($ifuserid, 'Operations Manager') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Operations Manager';
                                else $respstr .= 'Operations Manager';
                            }
                            if(getuserprofile($ifuserid, 'Researcher') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Researcher';
                                else $respstr .= 'Researcher';
                            }
                            if(getuserprofile($ifuserid, 'Sales Representative') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Sales Representative';
                                else $respstr .= 'Sales Representative';
                            }
                            if(getuserprofile($ifuserid, 'Customer') == 'true')
                            {
                                if(strlen($respstr)>0) $respstr .= ', Customer';
                                else $respstr .= 'Customer';
                            }

                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$iffull.'</strong></td>';
                            echo '<td>'.$respstr.'</td>';
                            echo '<td align="right"><form action="changeuserresp.php" method="post">';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="ChangeUserID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Employee Responsibilities" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form></td align="left">';
                            echo '<td><form action="delmember.php" method="post">';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="MemberID" />';
                            echo '<input type="hidden" value="true" name="FromDash" />';
                            echo '<input type="hidden" value="pleasedo" name="runok" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Employee from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }
                }

                mysql_close($con);
            }
            echo '</table>';
            if($ifranid != -1)
            {
                echo '<form action="adduser.php" method="post">';
                echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD EMPLOYEE</button>';
                echo '</form>';
            }
            //echo $query;
*/
            echo '</div>';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'genman')
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $query = "select fm.franchiseeid, f.name from franchiseemembers fm, franchisees f where userid=".$userid." and fm.franchiseeid=f.franchiseeid";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];
                    $ifranname = $row[1];

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>Franchise:'.$ifranname.'</strong></p>';
                    echo '<div class="grideight" style="width:930px;">';

                    // employees
                    echo '<h4 class="subhead">Employees</h4>';
                    //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Employees --</strong></p>';
                    echo '<table border="0" width="900" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:17px;">';
                    echo '<td width="300"><strong>Employee</strong></td>';
                    echo '<td width="300"><strong>Responsibilities</strong></td>';
                    echo '<td width="25" align="right"><strong>Actions</strong></td>';
                    echo '<td width="10" align="left"><strong>&nbsp;</strong></td>';
                    echo '</tr>';

                    // Profile Names
                    $iprofilename[1] = "Customer";
                    $iprofilename[2] = "Administrator";
                    $iprofilename[3] = "Territory&nbsp;Administrator";
                    $iprofilename[4] = "Franchisee";
                    $iprofilename[5] = "Operations&nbsp;Manager";
                    $iprofilename[6] = "General&nbsp;Manager";
                    $iprofilename[7] = "Authorized&nbsp;Buyer";
                    $iprofilename[8] = "Sales&nbsp;Representative";

                    $query = "select m.userid, concat(u.lastname,', ',u.firstname,' (',u.email,')') AS fullname, m.abletocontrol, group_concat(up.profileid)";
                    $query .= " from franchiseemembers m, users u, userprofiles up";
                    $query .= " where u.userid=m.userid and up.userid=u.userid and m.franchiseeid=".$ifranid." group by up.userid order by up.profileid, fullname";

                    //$query = "select m.UserID, concat(u.LastName,', ',u.FirstName,' (',u.Email,')') AS FullName from FranchiseeMembers m, Users u where u.UserId=m.UserID and m.FranchiseeID=".$ifranid." order by 2";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $ifuserid = $row[0];
                        $iffull = $row[1];
                        $ifcontrol = $row[2];
                        $ifprofiles = $row[3];

                        // TODO: roll this up into usernavfunctions.php (keep single code copy of the profiles table to avoid lookups)
                        $pieces = explode(',', $ifprofiles);
                        $count = count($pieces);
                        sort($pieces);
                        $ifroles = '';
                        for ($i = 0; $i < $count; $i++)
                        {
                            $ifroles .= $iprofilename[$pieces[$i]];
                            if ($i < ($count - 1)) $ifroles .= ', ';
                        }

                        {
                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$iffull.'</strong></td>';
                            echo '<td>'.$ifroles.'</td>';
                            echo '<td align="right"><form action="'.WEB_SERVER_NAME.'changeuserresp.php" method="post">';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="ChangeUserID" />';
                            echo '<input type="hidden" value="true" name="NoGMOption" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Employee Responsibilities" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form>';
                            echo '</td><td align="left">';
                            echo '<form action="'.WEB_SERVER_NAME.'delmember.php" method="post" onsubmit="javascript:return deleteempok();" >';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$ifuserid.'" name="MemberID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Employee from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }

                    echo '</table>';
                    echo '<form action="'.WEB_SERVER_NAME.'adduser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD NEW EMPLOYEE</button>';
                    echo '</form>';
                    echo '<form action="'.WEB_SERVER_NAME.'addreguser.php" method="post" style="float: left;" >';
                    echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD REGISTERED EMPLOYEE</button>';
                    echo '</form>';
                    echo '</div>';
                    //echo '<br clear="all" />';

/*
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<p style="color:#757575; font-size:17px; margin-bottom:5px;"><strong>My Employees</strong></p>';
                    echo '<table border="0" width="900" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:15px;">';
                    echo '<td width="45">&nbsp;</td>';
                    echo '<td width="126"><strong>NAME</strong></td>';
                    echo '<td width="153"><strong>ROLE</strong></td>';
                    echo '<td width="126"><strong>AUTO ASSIGN</strong></td>';
                    echo '<td width="232"><strong>ASSIGNMENT PRIVILEGES?</strong></td>';
                    echo '<td width="168"><strong>VIEW CUSTOMERS</strong></td>';
                    echo '</tr>';
                    echo '<tr valign="baseline">';
                    echo '<td width="45"><strong>';
                    echo '  <input name="" type="checkbox" value="" />';
                    echo '</strong></td>';
                    echo '<td width="126"><strong><a href="#">Esu Vee</a></strong></td>';
                    echo '<td width="153"><strong><a href="#">Sales Rep</a></strong></td>';
                    echo '<td width="126" align="center"><strong>';
                    echo '<input name="" type="radio" value="" />';
                    echo '</strong></td>';
                    echo '<td width="232" align="center"><strong>';
                    echo '<select name="AssignmentPrivileges">';
                    echo '<option value="AssignmentPrivileges">No</option>';
                    echo '</select>';
                    echo '</strong></td>';
                    echo '<td width="168" align="center"><strong><a href="#">View</a></strong></td>';
                    echo '</tr>';
                    echo '</table>';
                    echo '<br clear="all" />';
                    echo '<button type="submit" value="" class="med">SAVE CHANGES</button><br />';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px;">DELETE</button>';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD EMPLOYEE/USER</button>';
                    echo '</div>';
                    echo '<br clear="all" />';
                    echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '</div><!-- endgrideight-->';
                    echo '</div><!-- endgridfullgrey -->';
*/
                }
                else
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<div class="grideight" style="width:930px;">';
                }
                mysql_close($con);
            }

            // unassigned leads in this franchise
            $userlist = getunassignedleads($userid, $ifranid);
            if(strlen($userlist) > 0)
            {
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    mysql_select_db(DB_SERVER_DATABASE, $con);

                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '</div><!-- endgridfullgrey -->';
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<h4 class="subhead">Unassigned Leads</h4>';
                    echo '<table width="900" border="0" cellpadding="3">';
                    echo '<tr style="color:#85c11b; font-size:15px;">';
                    echo '<td width="300"><strong>Name (Email)</strong></td>';
                    echo '<td width="300"><strong>Market Need</strong></td>';
                    echo '<td width="125"><strong>Referred By</strong></td>';
                    echo '<td width="125"><strong>Rep Name</strong></td>';
                    echo '<td width="10"><strong>Actions</strong></td>';
                    echo '</tr>';

                    $query = "select u.userid, concat(u.firstname,' ',u.lastname,' (',u.email,')') AS fullname, u.referredby, u.repname, m.marketneedid, m.title from users u, marketneeds m where u.userid=m.userid and u.userid in (".$userlist.") order by m.marketneedid desc";
                    $result = mysql_query($query, $con);
                    while($result && $row = mysql_fetch_array($result))
                    {
                        $aquery = "select * from assignedreps where marketneedID = ".$row[4];
                        $aresult = mysql_query($aquery, $con);
                        if(!$aresult || !$arow = mysql_fetch_array($aresult))
                        {
                            echo '<tr valign="baseline">';
                            echo '<td><strong>'.$row[1].'</strong></td>';
                            echo '<td>'.$row[5].'</td>';
                            echo '<td>'.$row[2].'</td>';
                            echo '<td>'.$row[3].'</td>';
                            echo '<td><form action="'.WEB_SERVER_NAME.'addassignedrep.php" method="post">';
                            echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                            echo '<input type="hidden" value="'.$row[4].'" name="AddMarketID" />';
                            echo '<input type="hidden" value="'.$row[0].'" name="ForUserID" />';
                            echo '<input type="hidden" value="'.$row[1].'" name="ForUserName" />';
                            echo '<input type="hidden" value="'.$row[5].'" name="ForUserNeed" />';
                            if(isset($row[2]) && strlen($row[2]) > 0) $iuserref = $row[2];
                            else $iuserref = 'None Specified';
                            echo '<input type="hidden" value="'.$iuserref.'" name="ForUserRef" />';
                            if(isset($row[3]) && strlen($row[3]) > 0) $iuserrep = $row[3];
                            else $iuserrep = 'None Specified';
                            echo '<input type="hidden" value="'.$iuserrep.'" name="ForUserRep" />';
                            echo '<button type="submit" value="" class="med">ASSIGN</button>';
                            echo '</form></td>';
                            echo '</tr>';
                        }
                    }

                    echo '</table>';
                    //echo '<br clear="all" />';
                    //echo '<button type="submit" value="" class="med" style="margin-top:10px;">DELETE</button>';
                    echo '</div><!-- endgrideight-->';
                }

                mysql_close($con);
            }

            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'opsman')
        {
            $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
            if($con)
            {
                mysql_select_db(DB_SERVER_DATABASE, $con);

                $query = "select fm.franchiseeid, f.name from franchiseemembers fm, franchisees f where userid=".$userid." and fm.franchiseeid=f.franchiseeid";
                $result = mysql_query($query, $con);
                if($result && $row = mysql_fetch_array($result))
                {
                    $ifranid = $row[0];
                    $ifranname = $row[1];

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>Franchise:'.$ifranname.'</strong></p>';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<h4 class="subhead">Purchased Vehicles In Process</h4>';

                    $query = "select count(*) from purchases p, specificvehicles s, assignedreps a, franchiseemembers f where p.completed=0 and s.specificvehicleid=p.specificvehicleid and s.marketneedid=a.marketneedid and a.userrepid=f.userid and f.franchiseeid=".$ifranid;
                    $result = mysql_query($query, $con);
                    if($result && $row = mysql_fetch_array($result)) $count = $row[0];
                    else $count = 0;

                    if($count > 0)
                    {
                        echo '<table width="900" border="0" cellpadding="3">';
                        echo '<tr style="color:#85c11b; font-size:15px;">';
                        echo '<td width="200"><strong>Year/Make/Model</strong></td>';
                        echo '<td width="200"><strong>Customer</strong></td>';
                        echo '<td width="200"><strong>Current Stage</strong></td>';
                        echo '<td width="200"><strong>Cost So Far</strong></td>';
                        echo '<td width="200"><strong>Bought On</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';

                        $query = "select v.year, m.name, v.model, u.firstname, u.lastname, p.purchaseid, p.boughton, p.status from purchases p, specificvehicles s, vehicledetails v, makes m, assignedreps a, franchiseemembers f, marketneeds n, users u where p.completed=0 and s.specificvehicleid=p.specificvehicleid and s.vehicledetailid = v.vehicledetailid and v.makeid = m.makeid and s.marketneedid = n.marketneedid and n.userid = u.userid and s.marketneedid = a.marketneedid and a.userrepid = f.userid and f.franchiseeid = ".$ifranid;
                        $result = mysql_query($query, $con);
                        while($result && $row = mysql_fetch_array($result))
                        {
                            echo '<tr style="color:#000000; font-size:15px;">';
                            echo '<td>'.$row[0].' '.$row[1].' '.$row[2].'</td>';
                            echo '<td>'.$row[3].' '.$row[4].'</td>';
                            echo '<td>';

                            $cquery = "select * from specificactions where purchaseid=".$row[5];
                            $cresult = mysql_query($cquery, $con);
                            if(!$crow = mysql_fetch_array($cresult)) echo 'Pending';
                            else
                            {
                                $sumquery = "select count(*) from specificactions sa, specificcategories sc, specificitems si where si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5];
                                $sumresult = mysql_query($sumquery);
                                if($sumresult && $sumrow = mysql_fetch_array($sumresult))
                                {
                                    $total = $sumrow[0];
                                    $sumquery = "select count(*) from specificactions sa, specificcategories sc, specificitems si where si.completed=1 and si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5];
                                    $sumresult = mysql_query($sumquery);
                                    if($sumrow = mysql_fetch_array($sumresult))
                                    {
                                        $completed = $sumrow[0];
                                        $sumquery = "select si.name from specificactions sa, specificcategories sc, specificitems si where si.completed=0 and si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5]." order by sc.displayorder asc, si.displayorder asc";
                                        $sumresult = mysql_query($sumquery);
                                        if($sumresult && $sumrow = mysql_fetch_array($sumresult))
                                        {
                                            $itemname = $sumrow[0];
                                            echo $itemname.' ['.$completed.' of '.$total.']';
                                        }
                                        else echo $completed.' of '.$total.' Completed';
                                    }
                                    else echo '0 of '.$total.' Completed';
                                }
                                else echo 'Pending';
                            }
                            echo '</td>';
                            echo '<td>';

                            $cquery = "select * from specificactions where purchaseid=".$row[5];
                            $cresult = mysql_query($cquery, $con);
                            if(!$crow = mysql_fetch_array($cresult)) echo '$0.00';
                            else
                            {
                                $sumquery = "select sum(si.amount) from specificactions sa, specificcategories sc, specificitems si where si.specificcategoryid=sc.specificcategoryid and sc.specificactionid=sa.specificactionid and sa.purchaseid=".$row[5];
                                $sumresult = mysql_query($sumquery);
                                if($sumresult && $sumrow = mysql_fetch_array($sumresult)) echo '$'.number_format($sumrow[0]);
                                else echo '$0.00';
                            }
                            echo '</td>';
                            if ($row[7] == 'Bought')
                                echo '<td>'.$row[6].'</td>';
                            else
                                echo '<td>'.$row[7].'</td>';

                            if($row[1] == 0)
                            {
                                echo '<td align="right">';
                                echo '<form action="'.WEB_SERVER_NAME.'opsmgractions.php#Details" method="post">';
                                echo '<input type="hidden" value="'.$ifranid.'" name="FranchiseeID" />';
                                echo '<input type="hidden" value="'.$row[5].'" name="PurchaseID" />';
                                echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Vehicle Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                echo '</form>';
                                echo '</td>';
                            }
                            else echo '<td>&nbsp;</td>';
                            echo '</tr>';
                        }
                        echo '</table>';
                    }
                    else echo '<p>No Purchased Vehicles To Work On</p>';

                    echo '</div><!-- endgrideight-->';
                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                }

                if($uadmin == 'true')
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<h4 class="subhead">Action Plans</h4>';
                    //echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Action Plans --</strong></p>';

                    $query = "select count(*) from actionplans where visible=1";
                    $result = mysql_query($query, $con);
                    if($result && $row = mysql_fetch_array($result)) $count = $row[0];
                    else $count = 0;

                    if($count > 0)
                    {
                        echo '<table width="900" border="0" cellpadding="3">';
                        echo '<tr style="color:#85c11b; font-size:15px;">';
                        echo '<td width="300"><strong>Plan Name</strong></td>';
                        echo '<td width="300"><strong>Categories</strong></td>';
                        echo '<td width="280"><strong>Checklist Items</strong></td>';
                        echo '<td width="10" align="right"><strong>Actions</strong></td>';
                        echo '<td width="10"><strong>&nbsp</strong></td>';
                        echo '</tr>';

                        $query = "select name, corporateadded, actionplanid from actionplans where visible=1";
                        $result = mysql_query($query, $con);
                        while($result && $row = mysql_fetch_array($result))
                        {
                            echo '<tr style="color:#000000; font-size:15px;">';
                            echo '<td>'.$row[0].'</td>';
                            echo '<td>';

                            $cquery = "select count(*) from actioncategories where actionplanid=".$row[2];
                            $cresult = mysql_query($cquery, $con);
                            if($cresult && $crow = mysql_fetch_array($cresult)) echo $crow[0];
                            else echo 0;
                            echo '</td>';
                            echo '<td>';

                            $cquery = "select count(*) from actioncategories a, actionitems i where i.actioncategoryid=a.actioncategoryid and a.actionplanid=".$row[2];
                            $cresult = mysql_query($cquery, $con);
                            if($cresult && $crow = mysql_fetch_array($cresult)) echo $crow[0];
                            else echo 0;
                            echo '</td>';
                            echo '<td align="right">';
                            echo '<form action="'.WEB_SERVER_NAME.'actionplan.php" method="post">';
                            echo '<input type="hidden" value="'.$row[2].'" name="ActionPlanID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Edit" title="Edit Action Plan" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                            echo '</form>';
                            echo '</td><td align="left">';
                            echo '<form action="'.WEB_SERVER_NAME.'actionplan.php" method="post">';
                            echo '<input type="hidden" value="'.$row[2].'" name="ActionPlanID" />';
                            echo '<input type="hidden" value="true" name="DeleteIt" />';
                            echo '<input type="hidden" value="'.WEB_SERVER_NAME.'mydashboard.php#admintab" name="ReturnTo" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Delete" title="Remove Action Plan" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                            echo '</form>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        echo '</table>';
                    }
                    else echo '<p>No Action Plans Available</p>';

                    echo '</div><!-- endgrideight-->';
                    echo '<table><tr><td>';
                    echo '<form action="'.WEB_SERVER_NAME.'actionplan.php" method="post">';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">ADD ACTION PLAN</button>';
                    echo '</form></td><td>';
                    echo '<form action="'.WEB_SERVER_NAME.'copyactionplan.php" method="post">';
                    echo '<button type="submit" value="" class="med" style="margin-top:10px; margin-left:30px;">COPY AN ACTION PLAN</button>';
                    echo '</form></td></tr></table>';
                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                }
                mysql_close($con);
            }
        }
        elseif($curadmintab == 'authbuyer')
        {
            echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
            //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
            echo '<div class="grideight" style="width:930px;">';
            echo '<h4 class="subhead">Watchlist Vehicles</h4>';
            echo '<p>Coming Soon!</p>';
            echo '</div><!-- endgrideight-->';
            echo '</div><!-- endgridfullgrey -->';
            //echo '<br clear="all" />';
        }
        elseif($curadmintab == 'salesrep')
        {
            // request only active leads
            // TODO: fix this function to return leads actually assigned to this salesrep not used to be assigned
            $needlist = getassignedmarketneeds($userid, true);
                        //echo "needlist for $userid is<br><pre>", print_r($needlist, true), "</pre>\n";
            if(strlen($needlist)>0)
            {
                $numhidden = 0;

                // Get the basic user information for each one...
                //echo $con;
                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                if($con)
                {
                    //echo $con + ' - ';
                    mysql_select_db(DB_SERVER_DATABASE, $con);
                    //echo $con + '<br/>';

                    // Get the hidden market needs and order by follow up date reversed...
                    $cuquery = "select m.marketneedid, concat(u.firstname,' ',u.lastname) AS fullname, m.title, m.created, m.followupdate, u.userid, m.state, u.note from users u, marketneeds m where u.userid=m.userid and m.showtorep=0 and m.marketneedid in (".$needlist.") order by m.followupdate desc, m.created desc";
                        //echo "query is <br><pre>" . htmlspecialchars($cuquery)."</pre><br>\n";
                    $curesult = mysql_query($cuquery, $con);
                    if($curesult)
                    {
                        while($curow = mysql_fetch_array($curesult))
                        {
                            $mnid[$numhidden] = $curow[0];
                            $mnname[$numhidden] = $curow[1];
                            $mntitle[$numhidden] = $curow[2];
                            $mncreated[$numhidden] = $curow[3];
                            $mnfollowup[$numhidden] = $curow[4];
                            $mnuserid[$numhidden] = $curow[5];
                            $mncestate[$numhidden] = $curow[6];
                            $mnnote[$numhidden] = $curow[7];
                            $numhidden++;
                        }
                    }

                    // Get details of all visible market needs to show to the rep
                    $query = "select u.userid, concat(u.firstname,' ',u.lastname) AS fullname, u.note, u.created, m.followupdate, m.marketneedid from users u, marketneeds m where u.userid=m.userid and m.showtorep=1 and m.marketneedid in (".$needlist.")";
                        //echo "query is <br><pre>" . htmlspecialchars($query)."</pre><br>\n";
                    $result = mysql_query($query, $con);
                    $index = 0;
                    if($result)
                    {
                        while($row = mysql_fetch_array($result))
                        {
                            $iuserid[$index] = $row[0];
                            $iusername[$index] = $row[1];
                            $iusernote[$index] = $row[2];
                            $iuserreg[$index] = $row[3];
                            $iuserfollow[$index] = $row[4];
                            $iuserneed[$index] = $row[5];
                            $istep[$index] = getuserstep($row[0], $row[5]);
                            $index++;
                        }
                    }

                    //mysql_close($con);
                }
                else $numhidden = 0;

if (0)
{
                if($numhidden > 0)
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 0px">';
                    echo '<div class="grideight" style="width:930px;">';
                    echo '<form action="'.WEB_SERVER_NAME.'mydashboard.php#admintab" method="post" style="float:right;" >';
                    echo '<input type="hidden" value="true" name="PostBack" />';
                    echo '<input type="hidden" value="true" name="ShowAllHiddenCust" />';
                    echo '<button type="submit" value="" class="blueongrey_dash">Show All Hidden Customer Experiences</button>';
                    echo '</form>';
                    echo '<form action="'.WEB_SERVER_NAME.'mydashboard.php#admintab" method="post">';
                    echo '<input type="hidden" value="true" name="PostBack" />';
                    echo '<select name="ShowExperience" id="ShowExperience" style="width: 350px;">';
                    for($i = 0; $i < $numhidden; $i++)
                    {
                        if (strlen($mnfollowup[$i]) > 0) // followups first
                            echo '<option value="'.$mnid[$i].'" >'.$mnname[$i].' - '.$mntitle[$i].' - Followup:'.date_at_timezone('m/d/Y','GMT',$mnfollowup[$i]).'</option>';
                        else
                            echo '<option value="'.$mnid[$i].'" >'.$mnname[$i].' - '.$mntitle[$i].' - Started:'.date_at_timezone('m/d/Y','GMT',$mncreated[$i]).'</option>';
                    }
                    echo '</select>';
                    echo '<button type="submit" value="" class="blueongrey_dash">Show Selected Customer Experience</button>';
                    echo '</form>';
                    echo '</div></div>';
                }
}
                // customers for this sales rep
                // NOTE = if a customer has multiple active market needs, all of them go to the salesrep of the latest market need record sales rep assignment
                $count = count($iuserid);
                if($count>0 || $numhidden > 0)
                {
                    $step1 = 0;
                    $step2 = 0;
                    $step3 = 0;
                    $step4 = 0;
                    for($i=0;$i<$count;$i++)
                    {
                        if($istep[$i] == 4) $step4++;
                        elseif($istep[$i] == 3) $step3++;
                        elseif($istep[$i] == 2) $step2++;
                        else $step1++;
                    }

                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    if($step4 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 4 -- </strong>';
                        // echo '<button id="step4button" onclick="javascript:showhidediv(\'step4\', \'step4button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button class="hideshow" id="step4button" onclick="javascript:showhidediv(\'step4\', \'step4button\');">Hide</button>';
                        echo '&nbsp;&nbsp;Customers in Step 4</h4>';
                        echo '<div style="display:block" id="step4">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 4)
                            {
                                echo '<tr valign="top">';
                                echo '<td><strong>'.$iusername[$i].'</strong></td>';
                                echo '<td>Pending</td>';
                                echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                // TODO: is this just a duplicate?
                                echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                echo '</form></td>';

                                echo '</tr>';
                            }
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                        //echo '<br clear="all" />';
                    }
                    if($step3 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 3 -- </strong>';
                        echo '<h4 class="subhead"><button class="hideshow" id="step3button" onclick="javascript:showhidediv(\'step3\', \'step3button\');">Hide</button>';
                        echo '&nbsp;&nbsp;Customers in Step 3</h4>';
                        echo '<div style="display:block" name="step3" id="step3">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td ><strong>Customer</strong></td>';
                        echo '<td ><strong>Status</strong></td>';
                        echo '<td ><strong>Notes</strong></td>';
                        echo '<td ><strong>Registered</strong></td>';
                        echo '<td ><strong>Last Status</strong></td>';
                        echo '<td ><strong>FollowUp</strong></td>';
                        echo '<td ><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 3)
                            {
                                echo '<tr valign="top">';
                                echo '<td><strong>'.$iusername[$i].'</strong></td>';

                                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                                if($con)
                                {
                                    mysql_select_db(DB_SERVER_DATABASE, $con);

                                    $query = "select count(*) from specificvehicles where marketneedid =".$iuserneed[$i];
                                    $result = mysql_query($query, $con);
                                    if($result && $row = mysql_fetch_array($result))
                                    {
                                        $vehcount = $row[0];
                                        $purcount = 0;

                                        $query = "select specificvehicleid from specificvehicles where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        while($result && $row = mysql_fetch_array($result))
                                        {
                                            // Get the current Purchase Approval Information (if any)...
                                            $pquery = 'select count(*) from purchases where specificvehicleid = '.$row[0];
                                            $presult = mysql_query($pquery, $con);
                                            if($presult && $prow = mysql_fetch_array($presult)) $purcount += $prow[0];
                                        }
                                    }
                                    else $vehcount = 0;

                                    if($purcount > 0)
                                    {
                                        echo '<td>Specific Vehicle Chosen</td>';

                                        $query = "select max(p.lastupdated) from purchases p, specificvehicles s where p.specificvehicleid=s.specificvehicleid and s.marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }
                                    elseif($vehcount > 0)
                                    {
                                        echo '<td>'.$vehcount.' Specific Vehicle(s)</td>';

                                        $query = "select max(lastupdated) from specificvehicles where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }
                                    else
                                    {
                                        echo '<td>Needs Specific Vehicles</td>';

                                        $query = "select max(accepted) from (select o.accepted from orders o,orderfor of,firmquotes f,quoterequests q where o.orderid=of.orderid and f.firmquoteid=of.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=".$iuserneed[$i]." union all select w.accepted from watches w,watchfor wf,firmquotes f,quoterequests q where w.watchid=wf.watchid and f.firmquoteid=wf.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=".$iuserneed[$i].") data";
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) $laststatus = date_at_timezone('m/d/Y', 'GMT', $row[0]);
                                        else $laststatus = 'Pending';
                                    }

                                    mysql_close($con);
                                }
                                else
                                {
                                    echo '<td>Pending</td>';
                                    $laststatus = 'Pending';
                                }
                                if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                else echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                echo '<td>'.$laststatus.'</td>';
                                if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Customer Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                echo '</form></td>';

                                echo '</tr>';
                            }
                        }
                        echo '</table>';
                        echo '</div>'; // step3
                        echo '</div>';
                        //echo '<br clear="all" />';
                    }
                    if($step2 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 2 -- </strong>';
                        // echo '<button id="step2button" onclick="javascript:showhidediv(\'step2\', \'step4button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button id="step2button" class="hideshow" onclick="javascript:showhidediv(\'step2\', \'step2button\');">Hide</button>';
                        echo ' Customers in Step 2</h4>';
                        echo '<div id="step2" style="display:block">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 2)
                            {
                                // Get the basic user information for each one...
                                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                                if($con)
                                {
                                    mysql_select_db(DB_SERVER_DATABASE, $con);

                                    echo '<tr valign="top">';
                                    echo '<td><strong>'.$iusername[$i].'</strong></td>';

                                    $query = "select count(*) from firmquotes f,quoterequests q where f.quoterequestid=q.quoterequestid and q.marketneedid =".$iuserneed[$i];
                                    $result = mysql_query($query, $con);
                                    if($result && $row = mysql_fetch_array($result)) $quotecount = $row[0];
                                    else $quotecount = 0;

                                    if($quotecount > 0)
                                    {
                                                                            $plural = pluralize_noun($quotecount, "Current Market Stud", "ies", "y");
                                        echo "<td> $quotecount $plural</td>";
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';

                                        $query = "select max(lastupdated) from firmquotes where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                        else echo '<td>Not Set</td>';
                                    }
                                    else
                                    {
                                        echo '<td>Needs Current Market Study</td>';
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';

                                        $query = "select max(lastupdated) from quoterequests where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result))
                                        {
                                            if(is_null($row[0]))
                                            {
                                                $query = "select max(lastupdated) from assessments where marketneedid =".$iuserneed[$i];
                                                $result = mysql_query($query, $con);
                                                if($result && $row = mysql_fetch_array($result))
                                                {
                                                    if(is_null($row[0]))
                                                    {
                                                        $query = "select max(lastupdated) from consultations where marketneedid =".$iuserneed[$i];
                                                        $result = mysql_query($query, $con);
                                                        if($result && $row = mysql_fetch_array($result))
                                                        {
                                                            if(is_null($row[0])) echo '<td>Not Set</td>';
                                                            else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</strong></td>';
                                                        }
                                                        else echo '<td>Not Set</td>';
                                                    }
                                                    else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                                }
                                                else
                                                {
                                                    $query = "select max(lastupdated) from consultations where marketneedid =".$iuserneed[$i];
                                                    $result = mysql_query($query, $con);
                                                    if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                                    else echo '<td>Not Set</td>';
                                                }
                                            }
                                            else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                        }
                                        else
                                        {
                                            $query = "select max(lastupdated) from assessments where marketneedid =".$iuserneed[$i];
                                            $result = mysql_query($query, $con);
                                            if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                            else
                                            {
                                                $query = "select max(lastupdated) from consultations where marketneedid =".$iuserneed[$i];
                                                $result = mysql_query($query, $con);
                                                if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                                else echo '<td>Not Set</td>';
                                            }
                                        }
                                    }
                                    if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                    else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                    echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                    echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                    echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                    echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                    echo '</form></td>';

                                    echo '</tr>';

                                    mysql_close($con);
                                }
                            }
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                        //echo '<br clear="all" />';
                    } 
                    if($step1 > 0)
                    {
                        //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Customers in Step 1 -- </strong>';
                        // echo '<button id="step1button" onclick="javascript:showhidediv(\'step1\', \'step1button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button id="step1button" class="hideshow" onclick="javascript:showhidediv(\'step1\', \'step1button\');">Hide</button>';
                        echo '&nbsp;&nbsp;Customers in Step 1</h4>';
                        echo '<div style="display:block" id="step1">';
                        echo '<table class="table table-bordered">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$count;$i++)
                        {
                            if($istep[$i] == 1)
                            {
                                // Get the basic user information for each one...
                                $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
                                if($con)
                                {
                                    mysql_select_db(DB_SERVER_DATABASE, $con);

                                    echo '<tr valign="top">';
                                    echo '<td><strong>'.$iusername[$i].'</strong></td>';

                                    $query = "select count(*) from favorites where marketneedid =".$iuserneed[$i];
                                    $result = mysql_query($query, $con);
                                    if($result && $row = mysql_fetch_array($result)) $favcount = $row[0];
                                    else $favcount = 0;

                                    if($favcount > 0)
                                    {
                                        echo '<td>Stored '.$favcount.' Favorites</td>';
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</strong></td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';

                                        // Get the current User Information...
                                        $query = "select max(lastupdated) from favorites where marketneedid =".$iuserneed[$i];
                                        $result = mysql_query($query, $con);
                                        if($result && $row = mysql_fetch_array($result)) echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $row[0]).'</td>';
                                        else echo '<td><strong>Not Set</strong></td>';
                                    }
                                    else
                                    {
                                        echo '<td>Registered Only</td>';
                                        if(is_null($iusernote[$i]) || (strlen(trim($iusernote[$i]))<1)) echo '<td>{None}</td>';
                                        else echo '<td>'.substr($iusernote[$i],0,16).'</strong></td>';
                                        echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserreg[$i]).'</td>';
                                        echo '<td>Not Set</td>';
                                    }
                                    if(is_null($iuserfollow[$i])) echo '<td>Not Set</td>';
                                    else echo '<td>'.date_at_timezone('m/d/Y', 'GMT', $iuserfollow[$i]).'</td>';

                                    echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                                    echo '<input type="hidden" value="'.$iuserid[$i].'" name="ForUserID" />';
                                    echo '<input type="hidden" value="'.$iuserneed[$i].'" name="MarketNeedID" />';
                                    echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                                    echo '</form></td>';

                                    echo '</tr>';

                                    mysql_close($con);
                                }
                            }
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                    }

                    // show hidden market needs under step 1
                    if($numhidden > 0)
                    {
                        echo '<div class="grideight" style="width:930px;margin-bottom:5px">';
                        // echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- Followup Customers -- </strong>';
                        // echo '<button id="step0button" onclick="javascript:showhidediv(\'step0\', \'step0button\');">Hide</button></p>';
                        echo '<h4 class="subhead"><button id="step0button" class="hideshow" onclick="javascript:showhidediv(\'step0\', \'step0button\');">Hide</button>';
                        echo ' Followup Customers</h4>';
                        echo '<div style="display:block" id="step0">';
                        echo '<table width="900" style="color: rgb(51, 102, 204);" border="0" cellpadding="3">';
                        echo '<tr style="color: rgb(133, 193, 27); font-size: 15px;" valign="top">';
                        echo '<td width="145"><strong>Customer</strong></td>';
                        echo '<td width="156"><strong>Status</strong></td>';
                        echo '<td width="111"><strong>Notes</strong></td>';
                        echo '<td width="80"><strong>Registered</strong></td>';
                        echo '<td width="80"><strong>Last Status</strong></td>';
                        echo '<td width="80"><strong>FollowUp</strong></td>';
                        echo '<td width="10"><strong>Actions</strong></td>';
                        echo '</tr>';
                        for($i=0;$i<$numhidden;$i++)
                        {
                            $mnstep[$i] = getuserstep($mnuserid[$i], $mnid[$i]);

                            echo '<tr valign="top">';
                            echo '<td><strong>'.$mnname[$i].'</strong></td>';
                            echo '<td><strong>Step '.$mnstep[$i].': '.$mntitle[$i].'</strong></td>';

                            if(is_null($mnnote[$i]) || (strlen(trim($mnnote[$i]))<1)) echo '<td><strong>{None}</strong></td>';
                            else echo '<td><strong>'.substr($mnnote[$i],0,16).'</strong></td>';
                            echo '<td><strong>'.date_at_timezone('m/d/Y', 'GMT', $mncreated[$i]).'</strong></td>';
                            echo '<td><strong>'.$mncestate[$i].'</strong></td>';
                            if(is_null($mnfollowup[$i])) echo '<td><strong>Not Set</strong></td>';
                            else echo '<td><strong>'.date_at_timezone('m/d/Y', 'GMT', $mnfollowup[$i]).'</strong></td>';

                            echo '<td><form action="'.WEB_SERVER_NAME.'salesrepactions.php" method="post">';
                            echo '<input type="hidden" value="'.$mnuserid[$i].'" name="ForUserID" />';
                            echo '<input type="hidden" value="'.$mnid[$i].'" name="MarketNeedID" />';
                            echo '<button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Customer Experience" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>';
                            echo '</form></td>';

                            echo '</tr>';
                        }
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                    }

                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                    //echo '<button type="submit" value="" class="med" style="margin-top: 10px;">ADD USER</button>';
                }
                else
                {
                    echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                    //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                    echo '<div class="grideight" style="width:930px;">';
                    if($usrep == 'true') echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- All Customers Are Currently Hidden --</strong></p>';
                    else echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- You are not setup as a Sales Representative Personally --</strong></p>';
                    echo '</div><!-- endgrideight-->';
                    echo '</div><!-- endgridfullgrey -->';
                    //echo '<br clear="all" />';
                    //echo '<button type="submit" value="" class="med" style="margin-top: 10px;">ADD USER</button>';
                }
            }
            else
            {
                echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                echo '<div class="grideight" style="width:930px;">';
                if($usrep == 'true') echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- No Customers Assigned Personally --</strong></p>';
                else echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- You are not setup as a Sales Representative Personally --</strong></p>';
                echo '</div><!-- endgrideight-->';
                echo '</div><!-- endgridfullgrey -->';
                //echo '<br clear="all" />';
                //echo '<button type="submit" value="" class="med" style="margin-top: 10px;">ADD USER</button>';
            }

            // Now if a higher level viewing this screen...show the other SalesReps Customers...
            if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true'))
            {
                echo '<div class="gridfullgrey" style="margin-bottom: 20px">';
                //echo '<center><img src="common/layout/grey-gradient-bar.jpg" /></center>';
                echo '<div class="grideight" style="width:930px;">';
                echo '<p style="color:#757575; font-size:17px; margin-top:0; margin-bottom:5px;"><strong>-- View Other Representative\'s Customers --</strong></p>';
                echo '<p>Coming Soon!</p>';
                echo '</div><!-- endgrideight-->';
                echo '</div><!-- endgridfullgrey -->';
                //echo '<br clear="all" />';
            }
        }
        echo '</div>';
        //echo '<br clear="all" />';
    }

    if($ucust == 'true')
    {
        //echo '<br clear="all" />';
        //echo '<div style="float: right">';
        //echo '<h1 class="subhead" style="float: right;">My Salesperson</h1>';
        //echo '<div class="gridtwelve" style="float: right">';
        echo '<div class="gridfour" style="float: right; margin-top: 0px;">';
        echo '<h1 class="subhead" style="width: 175px; margin-left: 0px; margin-top: 0px; margin-bottom: 10px;">My Salesperson</h1>';
        //echo '<p style="font-size: 24px; background-color: #7aac25; color:#FFFFFF; width: 175px; padding: 5px; margin-top: 0px;">My Salesperson</p>';
        if($usalesrep != -1)
        {
            $srepname = getuserfullname($usalesrep, 'false');
            //echo '<img src="'.getuserimage($usalesrep).'" alt="" width="79" height="98" class="profilepic" />';
            $srctext = getuserimage($usalesrep);
            $max_width = 100;
            $max_height = 150;
            echo '<img class="profilepic" id="userimage" src="loadimage.php?image='.$srctext.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="0" vspace="0" />';
            echo '<p class="salestitle">'.$srepname;
            //echo '<span class="filter">(is online now)</span>';
            echo '</p>';
            //echo '<button type="submit" value="" class="small">Send a Message</button>';
            //echo '<p class="blacktwelve">Sales Bio Inof.</p>';
            //echo '</div>';
            //echo '<div class="gridfour">';
            echo '<p class="salescontact">';
            $srepwork = getuserphone($usalesrep, 'Work');
            if(strlen($srepwork) > 0) echo 'Direct: '.$srepwork.'<br />';
            $srepcell = getuserphone($usalesrep, 'Cell');
            if(strlen($srepcell) > 0) echo 'Mobile: '.$srepcell.'<br />';
            $srepfax = getuserphone($usalesrep, 'Fax');
            if(strlen($srepfax) > 0) echo 'Fax: '.$srepfax.'<br />';
            echo '</p>';
            echo '<form action="'.WEB_SERVER_NAME.'sendmessage.php" method="post" target="_blank">';
            echo '<input type="hidden" value="'.$usalesrep.'" name="MessageToID" />';
            echo '<p class="salescontact">';
            echo 'Post a Message: <button type="submit" align="absmiddle" class="blueonwhite"><img alt="Send" title="Send a Message" align="absmiddle" border="0" src="common/layout/mailicon.gif" /></button>';
            //echo 'My email address:<br />';
            //echo '<a href="#">dowens@1800vehicles.com</a>';
            echo '</p>';
            echo '</form>';
            //echo '<p class="salescontact"><a href="#">View Dealership Info</a></p>';
            //echo '<p class="salescontact"><a href="#">Have a Suggestion?</a></p>';
        }
        else // No sales rep assigned yet...
        {
            $srctext = "userimages/nopicture.jpg";
            $max_width = 100;
            $max_height = 100;
            echo '<img class="profilepic" id="userimage" src="loadimage.php?image='.$srctext.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="0" vspace="0" />';
            echo '<p class="salestitle">Unassigned</p>';
            echo '<p class="blacktwelve">A 1-800-vehicles.com representative will be assigned to you for free consultation. Call 1-800-VEHICLES (834-4253) or request consultation from the Step 1 menu if you\'d like help now.</p>';
        }
        // inline message in hidden div that is toggled with mail icon in the top section
        echo '<div name="themessagediv" id="themessagediv" style="display:none">';
        echo '<br/><br/>';
        echo '<h1 class="subhead" style="width: 175px; margin-left: 0px; margin-top: 0px; margin-bottom: 10px;">My Message</h1>';
        {
            echo '<form name="theform" action="'.WEB_SERVER_NAME.'sendmessage.php" onsubmit="return validateFormOnSubmit();" method="post">';
                        echo '<input type="hidden" value="<?php echo $usertoID; ?>" name="MessageToID" />';
                        echo '<input type="hidden" value="true" name="PostBack" />';
                        echo '<p style="font-size:13px; color:#142c3c; margin-left:20px;"><strong>Message</strong></p>';
                        echo '<textarea id="themessage" name="themessage" cols="48" rows="8" style="margin-left:20px;"></textarea>';
                        echo '<br clear="all" /><br />';
                        echo '<div style="margin-left:70px;">';
                            echo '<button type="submit" value="send" class="med"><nobr>SEND</nobr></button>';
                            echo '<button type="button" value="clear" class="med" style="margin-left:10px;" onclick="javascript:clearclicked();" ><nobr>CLEAR</nobr></button>';
                            echo '<button type="button" value="cancel" class="med" style="margin-left:10px;" onclick="javascript:cancelclicked();" ><nobr>CANCEL</nobr></button>';
                        echo '</div>';
                    echo '</form>';
        }
        if (0) {
            $srepname = getuserfullname($usalesrep, 'false');
            $srctext = getuserimage($usalesrep);
            $max_width = 100;
            $max_height = 150;
            echo '<img class="profilepic" id="userimage" src="loadimage.php?image='.$srctext.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="0" vspace="0" />';
            echo '<p class="salestitle">'.$srepname;
            echo '</p>';
            echo '<p class="salescontact">';
            $srepwork = getuserphone($usalesrep, 'Work');
            if(strlen($srepwork) > 0) echo 'Direct: '.$srepwork.'<br />';
            $srepcell = getuserphone($usalesrep, 'Cell');
            if(strlen($srepcell) > 0) echo 'Mobile: '.$srepcell.'<br />';
            $srepfax = getuserphone($usalesrep, 'Fax');
            if(strlen($srepfax) > 0) echo 'Fax: '.$srepfax.'<br />';
            echo '</p>';
            echo '<form action="'.WEB_SERVER_NAME.'sendmessage.php" method="post" target="_blank">';
            echo '<input type="hidden" value="'.$usalesrep.'" name="MessageToID" />';
            echo '<p class="salescontact">';
            echo 'Post a Message: <button type="submit" align="absmiddle" class="blueonwhite"><img alt="Send" title="Send a Message" align="absmiddle" border="0" src="common/layout/mailicon.gif" /></button>';
            echo '</p>';
            echo '</form>';
        }
        echo '</div>';
        echo '</div><!--end gridfour -->';
        //if($ucurrentstep <= 2)
        //{
        //    $margintop = 0;
        //    require("teaser.php");    //Dashboard Page 2 Only...
        //}
        //echo '</div><!--end gridtwelve -->';
        //echo '</div><!--end floater -->';
    }

    // Output the Dashboard Messages...
    //echo '<br clear="all" />';  
   
    echo '<div class="grideight" style="clear: left; float: none; margin: 0 auto;  width: 50%;">';
    echo '<h1 class="subhead" style="clear: left;margin-left: 0;"><a name="messages"></a>Dashboard Updates</h1>';
    $nummsgs = count($umsgcreated);
    if($nummsgs < 1)
    {
        echo '<div class="updatelistfirst">';
        echo '<p style="font-size:12px;">There are currently no messages to display.</p>';
        echo '</div>';
    }
    else
    {
        for($i = 0; $i < $nummsgs; $i++)
        {
            if($i == 0) echo '<div class="updatelistfirst">';
            else echo '<div class="updatelist">';
            if(is_null($umsgfromid[$i]))
            {
                if(strlen($inimage)>0) $srctext=$inimage;
                else $srctext="userimages/nopicture.jpg";
                $max_width = 60;
                $max_height = 60;
                echo '<img class="updatethumb" id="userimage" src="loadimage.php?image='.$srctext.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="0" vspace="0" />';
            }
            else
            {
                $srctext = getuserimage($umsgfromid[$i]);
                $max_width = 60;
                $max_height = 60;
                echo '<img class="updatethumb" id="userimage" src="loadimage.php?image='.$srctext.'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="0" vspace="0" />';
            }
            echo '<p>';
            if(is_null($umsgfromid[$i]) || ($umsgfromid[$i] == $userid)) echo 'You';
            else echo getuserfullname($umsgfromid[$i], 'false');
            echo '<span style="float: right; margin-top: 0px;">';
            echo '<form action="'.WEB_SERVER_NAME.'delmessage.php" method="post" style="float: right; margin-top:0px;">';
            echo '<input type="hidden" value="'.$umsgtabid[$i].'" name="TableID" style="float: right; margin-top:0px;" />';
            echo '<input type="hidden" value="'.$umsgfromtab[$i].'" name="Table" style="float: right; margin-top:0px;" />';
            echo '<button type="submit" value="" class="blueonwhite" style="float: right; margin-top:0px;"><img style="float: right; margin-top:0px;" alt="Hide" title="Hide this Message" width="20" height="20" border="0" hspace="0" vspace="0" src="common/layout/close.gif" /></button>';
            echo '</form>';
            echo '</span>';
            echo '</p>';
            echo '<p class="greyeleven">'.timesincenow($umsgcreated[$i]).'</p>';
            echo '<p>'.$umsgmessage[$i].'</p>';
            echo '<br clear="all" />';
            echo '</div>';
        }
    }


    // echo '<div class="updatelistfirst"><img src="common/layout/profilethumb.jpg" class="updatethumb" />';
    // echo '<p>Me</p>';
    // echo '<p class="greyeleven">2 hours ago</p>';
    // echo '<p>Accepted delivery of the <a href="#">2006 Honda Odyssey (123456)</a> from <a href="#">Sky Ingram</a> and paid $15,432</p>';
    // echo '<br clear="all" /></div>';

    // echo '<div class="updatelist"><img src="common/layout/operations-sam-thumbnail.jpg" alt="" class="updatethumb" />';
    // echo '<p><a href="#">Operations Sam</a></p>';
    // echo '<p class="greyeleven">6 hours ago</p>';
    // echo '<p>Mechanic accepted the  <a href="#">2006 Honda Odyssey (123456)</a> and commented "only needs tires"</p>';
    // echo '<br clear="all" /></div>';

    // echo '<div class="updatelist"><img src="common/layout/profilethumb.jpg" class="updatethumb" />';
    // echo '<p>Me</p>';
    // echo '<p class="greyeleven">2 days ago</p>';
    // echo '<p>Accepted delivery of the <a href="#">2006 Honda Odyssey (123456)</a> from <a href="#">Sky Ingram</a> and paid $15,432</p>';
    // echo '<br clear="all" /></div>';


    echo '</div>';
?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
