<?php require("globals.php");
      require_once(WEB_ROOT_PATH.'common/functions/franchiseefunctions.php');  
?>
<?php 
require_once(WEB_ROOT_PATH . 'common/functions/string.php');
$_SESSION['state'] = 5;
$_SESSION['substate'] = 9;
$_SESSION['titleadd'] = 'Customer Review';

if (!isset($_REQUEST['ForUserID']) || !isset($_REQUEST['MarketNeedID'])) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Missing Parameters';
    header('Location: mydashboard.php#admintab1');
    exit();
}

$userid = $_SESSION['userid'];
if(!empty($userid))
$userid =get_admin_id();

if (!isset($userid)) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - UserID not set';
    header('Location: mydashboard.php#admintab2');
    exit();
}




$inuserid = $_REQUEST['ForUserID'];
$infranid = isset($_REQUEST['Franchisee']) ? $_REQUEST['Franchisee'] : null;
$inmneed = $_REQUEST['MarketNeedID'];
$instep = getuserstep($inuserid, $inmneed);

// Verify this user has access to the page...
$uadmin = getuserprofile($userid, 'Administrator');
$uterr = getuserprofile($userid, 'Teritory Admin');
$ufran = getuserprofile($userid, 'Franchisee');
$ugm = getuserprofile($userid, 'General Manager');
$usrep = getuserprofile($userid, 'Sales Representative');
if (!(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true') || ($usrep == 'true'))) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000508 - Permissions Error for ' . $userid;
    header('Location: mydashboard.php#admintab3');
    exit();
}

// Verify this user has access to this customer...  (ADMINS have rights to all customers)
if ($uadmin != 'true') {
    if (($usrep == 'true') && ($userid != getsalesrep($inuserid, $inmneed))) {
        $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Not Authorized to View Customer!';
        header('Location: mydashboard.php#admintab4');
        exit();
    }
}

// Check that the customer and market need match as well (they could have typed it on the line)...
if (isvalid($inuserid, $inmneed) == 0) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Invalid Customer Selection!  c=' . $inuserid . ' m=' . $inmneed;
    header('Location: mydashboard.php#admintab5');
    exit();
}

//$errormessage = 'false';
$con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
if ($con) {
    mysql_select_db(DB_SERVER_DATABASE, $con);

// Verify this user has access to the customer...   (ADMIN override)
    if ($uadmin != 'true') {
        $query = "select m.MarketNeedID from assignedreps a,marketneeds m where m.MarketNeedID=a.MarketNeedID and (('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between a.startdate and a.enddate) or (a.enddate is null)) and a.UserRepID=" . $userid . " and a.MarketNeedID = " . $inmneed;
        $result = mysql_query($query, $con);
        if (!$result || !$row = mysql_fetch_array($result)) {
            mysql_close($con);
            $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Failed authorization check';
            header('Location: mydashboard.php#admintab6');
            exit();
        }
    }

// query for Customer Details section
    $query = "select FirstName, LastName, Email, Note from users where userid=" . $inuserid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $ifname = $row[0];
        $ilname = $row[1];
        $iemail = $row[2];
        $inote = $row[3];
    }

// get details about current market need
    $query = "select title, showtouser, needscontact, active, completed, findby, depositrequired, followupdate, showtorep, max(LastLogin), created, state from marketneeds where marketneedid=" . $inmneed;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $ititle = $row[0];
        $ishowtouser = $row[1];
        $ineedscontact = $row[2];
        $iactive = $row[3];
        $icompleted = $row[4];
        $ifindby = $row[5];
        $idepreq = $row[6];
        $ifollowup = $row[7];
        $ishowrep = $row[8];
        $ilastlogin = $row[9];
        $icreated = $row[10];
        $icestate = $row[11];

// mark this as seen by sales unless the user is truly an admin
        if ($uadmin != 'true')
            $query = "update marketneeds set SeenBySales=1 where MarketNeedID=" . $inmneed;

        mysql_query($query, $con);
    }

// get all assignedrep records for this marketneedid
    if (1) {
        $index = 0;
        $query = "select a.AssignedRepID, a.UserRepID, concat(u.FirstName,' ',u.LastName,' (',u.Email,')') AS FullName, a.StartDate, a.EndDate from assignedreps a, users u where a.UserRepID = u.UserID and a.MarketNeedID=" . $inmneed . " order by a.StartDate desc";
//echo "query is <br><pre>" . htmlspecialchars($query)."</pre><br>\n";
        $result = mysql_query($query, $con);
        while ($result && $row = mysql_fetch_array($result)) {
            $asrepid[$index] = $row[0];
            $asurepid[$index] = $row[1];
            $asurepname[$index] = $row[2];
            $asstart[$index] = $row[3];
            $asend[$index] = $row[4];
            $index++;
        }
    }

// get customer worksheet information
    $query = "select TimeFrame, OrderType, Year, Make, Model, MileageCeiling, Colors, PriceQuoted, Packages, MustHave, FlexibleOn, DateOrdered, DateOrderEnds, MarketNeedID, Notes from worksheets where MarketNeedID=" . $inmneed;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $wtimeframe = $row[0];
        $wordertype = $row[1];
        $wyear = $row[2];
        $wmake = $row[3];
        $wmodel = $row[4];
        $wmileageceiling = $row[5];
        $wcolors = $row[6];
        $wpricequoted = $row[7];
        $wpackages = $row[8];
        $wmusthave = $row[9];
        $wflexibleon = $row[10];
        $wdateordered = $row[11];
        $wdateends = $row[12];
        $wid = $row[13];
        $wnotes = $row[14];
        $winsertedone = 0;
    } else {
        $query = "insert into worksheets (MarketNeedID) VALUES (" . $inmneed . ")";
        $result = mysql_query($query, $con);
        $winsertedone = 1;
//$werror = mysql_error();
//$_SESSION['ShowError'] = 'eo '.$werr2.' e1 '.$werror.' row '.$row[0].'err '.mysql_error();
    }

// get user login information
    $query = "select useemail, login from userlogin where userid=" . $inuserid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $iuseemail = $row[0];
        $ilogin = $row[1];
    }

// get user primary user address
    $query = "select ZIP,Address1,Address2,City,State,StartDate,EndDate from useraddresses where IsPrimary=1 and ((EndDate is null) or ('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between startdate and enddate)) and userid=" . $inuserid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $iaddzip = $row[0];
        $iaddaddr1 = $row[1];
        $iaddaddr2 = $row[2];
        $iaddcity = $row[3];
        $iaddstate = $row[4];
    }

// get the franchisee for the primary zip code
    $zipfranid = franchiseeofzipnodb($con, $iaddzip);
    $zipfranname = franchiseenamenodb($con, $zipfranid);

// get alternate emails
    $query = "select AlternateEmailID,AddInfo,Email,EmailToPhone,StartDate,EndDate from alternateemails where ((EndDate is null) or ('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between startdate and enddate)) and userid=" . $inuserid . " order by DisplayOrder";
    $result = mysql_query($query, $con);
    $index = 0;
    while ($result && $row = mysql_fetch_array($result)) {
        $iemailid[$index] = $row[0];
        $iemailtype[$index] = $row[1];
        $iemailaddr[$index] = $row[2];
        $iemaile2p[$index] = $row[3];
        $index++;
    }

// get phone numbers
    $query = "select n.UserNumberID, t.TypeName, n.PhoneNumber, n.Extension, n.AddInfo, n.Texting from usernumbers n, numbertypes t where ((n.EndDate is null) or ('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between n.startdate and n.enddate)) and n.NumberTypeID=t.NumberTypeID and userid=" . $inuserid . " order by n.DisplayOrder";
    $result = mysql_query($query, $con);
    $index = 0;
    while ($result && $row = mysql_fetch_array($result)) {
        $inumid[$index] = $row[0];
        $inumtype[$index] = $row[1];
        $inumnumber[$index] = $row[2];
        $inumext[$index] = $row[3];
        $inuminfo[$index] = $row[4];
        $inumtext[$index] = $row[5];
        $index++;
    }

// get trade in details
    $query = "select Description,LastUpdated,TradeInRequestID from tradeinrequests where MarketNeedID=" . $inmneed;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $itdesc = $row[0];
        $itlastup = $row[1];
        $itid = $row[2];

        $query = "select ImageFile from tradeinreqimages where TradeInRequestID=" . $itid;
        $result = mysql_query($query, $con);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $itimage[$index] = $row[0];
            $index++;
        }
    }

// handle the customer experience detail postback
// If anything was passed in, we are in a callback and should set the variables...
// TODO add error handling
// ADDED skip this if just switching the view
    if (isset($_REQUEST["InPostBack"]) && !isset($_REQUEST["Switching"])) {
        if (isset($_REQUEST["NeedTitle"]) && ($ititle != $_REQUEST["NeedTitle"])) {
            $ititle = $_REQUEST["NeedTitle"];
            $query = "update marketneeds set title='" . escapestr($ititle) . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["CEState"]) && ($icestate != $_REQUEST["CEState"])) {
            $icestate = $_REQUEST["CEState"];
            $query = "update marketneeds set state='" . $icestate . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["FollowUp"]) && ($ifollowup != $_REQUEST["FollowUp"])) {
            $afollowup = explode('/', $_REQUEST["FollowUp"]);
            $sfollowup = $afollowup[2] . '-' . $afollowup[0] . '-' . $afollowup[1] . ' 00:00:00';
            $ifollowup = $_REQUEST["FollowUp"];
            if (strlen($ifollowup) > 0)
                $query = "update marketneeds set followupdate='" . $sfollowup . "' where marketneedid=" . $inmneed;
            else
                $query = "update marketneeds set followupdate=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["FindBy"]) && ($ifindby != $_REQUEST["FindBy"])) {
            $afindby = explode('/', $_REQUEST["FindBy"]);
            $sfindby = $afindby[2] . '-' . $afindby[0] . '-' . $afindby[1] . ' 00:00:00';
            $ifindby = $_REQUEST["FindBy"];
            if (strlen($ifindby) > 0)
                $query = "update marketneeds set findby='" . $sfindby . "' where marketneedid=" . $inmneed;
            else
                $query = "update marketneeds set findby=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Deposit"]) && ($idepamt != $_REQUEST["Deposit"])) {
            $idepamt = $_REQUEST["Deposit"];
            if (strlen($idepamt) > 0) {
                if (substr($idepamt, 0, 1) == '$')
                    $idepamt = substr($idepamt, 1);
                $query = "update marketneeds set depositamount=" . $idepamt . " where marketneedid=" . $inmneed;
            } else
                $query = "update marketneeds set depositamount=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["DepReq"]) && ($idepreq == 0)) {
            $idepreq = 1;
            $query = "update marketneeds set depositrequired=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["DepReq"]) && ($idepreq == 1)) {
            $idepreq = 0;
            $query = "update marketneeds set depositrequired=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["ShowRep"]) && ($ishowrep == 0)) {
            $ishowrep = 1;
            $query = "update marketneeds set showtorep=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["ShowRep"]) && ($ishowrep == 1)) {
            $ishowrep = 0;
            $query = "update marketneeds set showtorep=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

//if(isset($_REQUEST["ShowUser"]) && ($ishowtouser == 0))
//{
//    $ishowtouser = 1;
//    $query = "update marketneeds set showtouser=1 where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//elseif($ishowtouser == 1)
//{
//    $ishowtouser = 0;
//    $query = "update marketneeds set showtouser=0 where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}

        if (isset($_REQUEST["NeedsContact"]) && ($ineedscontact == 0)) {
            $ineedscontact = 1;
            $query = "update marketneeds set needscontact=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["NeedsContact"]) && ($ineedscontact == 1)) {
            $ineedscontact = 0;
            $query = "update marketneeds set needscontact=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Active"]) && ($iactive == 0)) {
            $iactive = 1;
            $query = "update marketneeds set active=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["Active"]) && ($iactive == 1)) {
            $iactive = 0;
            $query = "update marketneeds set active=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Completed"]) && ($icompleted == 0)) {
            $icompleted = 1;
            $query = "update marketneeds set completed=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["Completed"]) && ($icompleted == 1)) {
            $icompleted = 0;
            $query = "update marketneeds set completed=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

//if(isset($_REQUEST["SecKey"]) && ($iseckey != $_REQUEST["SecKey"]))
//{
//    $iseckey = $_REQUEST["SecKey"];
//    if(strlen($iseckey) > 0)
//    {
//        if(substr($iseckey,0,1) == '$') $iseckey = substr($iseckey,1);
//        $query = "update marketneeds set SecondKeyLimit=".$iseckey." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set SecondKeyLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["NavDisc"]) && ($inavdisc != $_REQUEST["NavDisc"]))
//{
//    $inavdisc = $_REQUEST["NavDisc"];
//    if(strlen($inavdisc) > 0)
//    {
//        if(substr($inavdisc,0,1) == '$') $inavdisc = substr($inavdisc,1);
//        $query = "update marketneeds set NavDiscLimit=".$inavdisc." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set NavDiscLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["SelRep"]) && ($iselrep != $_REQUEST["SelRep"]))
//{
//    $iselrep = $_REQUEST["SelRep"];
//    if(strlen($iselrep) > 0)
//    {
//        if(substr($iselrep,0,1) == '$') $iselrep = substr($iselrep,1);
//        $query = "update marketneeds set SellerRepairLimit=".$iselrep." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set SellerRepairLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["PurRep"]) && ($ipurrep != $_REQUEST["PurRep"]))
//{
//    $ipurrep = $_REQUEST["PurRep"];
//    if(strlen($ipurrep) > 0)
//    {
//        if(substr($ipurrep,0,1) == '$') $ipurrep = substr($ipurrep,1);
//        $query = "update marketneeds set PurchaserRepairLimit=".$ipurrep." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set PurchaserRepairLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["PurDep"]) && ($ipurdep != $_REQUEST["PurDep"]))
//{
//    $ipurdep = $_REQUEST["PurDep"];
//    if(strlen($ipurdep) > 0)
//    {
//        if(substr($ipurdep,0,1) == '$') $ipurdep = substr($ipurdep,1);
//        $query = "update marketneeds set PurchaseDepositAmount=".$ipurdep." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set PurchaseDepositAmount=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}

        mysql_close($con);
//$_SESSION['ShowError'] = 'Completing the postback';
        header('Location: mydashboard.php#admintab');
        exit();
    }

// handle the customer experience worksheet postback
// If anything was passed in, we are in a callback and should set the variables...
// TODO add error handling
// ADDED skip this if just switching the view
    if (isset($_REQUEST["InPostBackWorksheet"]) && !isset($_REQUEST["Switching"])) {
        if (isset($_REQUEST["WNotes"]) && ($wnotes != $_REQUEST["WNotes"])) {
            $wnotes = $_REQUEST["WNotes"];
            $query = "update worksheets set notes='" . escapestr($wnotes) . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
//$_SESSION['ShowError'] = 'writing '.escapestr($wnotes).' for '.$inmneed.' err '.mysql_error();
        }

        if (isset($_REQUEST["TimeFrame"]) && ($wtimeframe != $_REQUEST["TimeFrame"])) {
            $wtimeframe = $_REQUEST["TimeFrame"];
            $query = "update worksheets set timeframe='" . $wtimeframe . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["OrderType"]) && ($wordertype != $_REQUEST["OrderType"])) {
            $wordertype = $_REQUEST["OrderType"];
            $query = "update worksheets set ordertype='" . $wordertype . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Year"]) && ($wyear != $_REQUEST["Year"])) {
            $wyear = $_REQUEST["Year"];
            $query = "update worksheets set year='" . $wyear . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Make"]) && ($wmake != $_REQUEST["Make"])) {
            $wmake = $_REQUEST["Make"];
            $query = "update worksheets set make='" . $wmake . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Model"]) && ($wmodel != $_REQUEST["Model"])) {
            $wmodel = $_REQUEST["Model"];
            $query = "update worksheets set model='" . $wmodel . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["MileageCeiling"]) && ($wmileageceiling != $_REQUEST["MileageCeiling"])) {
            $wmileageceiling = $_REQUEST["MileageCeiling"];
            $query = "update worksheets set mileageceiling='" . $wmileageceiling . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Colors"]) && ($wcolors != $_REQUEST["Colors"])) {
            $wcolors = $_REQUEST["Colors"];
            $query = "update worksheets set colors='" . $wcolors . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["PriceQuoted"]) && ($wpricequoted != $_REQUEST["PriceQuoted"])) {
            $wpricequoted = $_REQUEST["PriceQuoted"];
            $query = "update worksheets set PriceQuoted='" . $wpricequoted . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Packages"]) && ($wpackages != $_REQUEST["Packages"])) {
            $wpackages = $_REQUEST["Packages"];
            $query = "update worksheets set Packages='" . $wpackages . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["MustHave"]) && ($wmusthave != $_REQUEST["MustHave"])) {
            $wmusthave = $_REQUEST["MustHave"];
            $query = "update worksheets set MustHave='" . $wmusthave . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["FlexibleOn"]) && ($wflexibleon != $_REQUEST["FlexibleOn"])) {
            $wflexibleon = $_REQUEST["FlexibleOn"];
            $query = "update worksheets set FlexibleOn='" . $wflexibleon . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["DateOrdered"]) && ($wdateordered != $_REQUEST["DateOrdered"])) {
            $adateordered = explode('/', $_REQUEST["DateOrdered"]);
            $sdateordered = $adateordered[2] . '-' . $adateordered[0] . '-' . $adateordered[1] . ' 00:00:00';
            $wdateordered = $_REQUEST["DateOrdered"];
            if (strlen($wdateordered) > 0)
                $query = "update worksheets set dateordered='" . $sdateordered . "' where marketneedid=" . $inmneed;
            else
                $query = "update worksheets set dateordered=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["DateOrderEnds"]) && ($wdateends != $_REQUEST["DateOrderEnds"])) {
            $adateends = explode('/', $_REQUEST["DateOrderEnds"]);
            $sdateends = $adateends[2] . '-' . $adateends[0] . '-' . $adateends[1] . ' 00:00:00';
            $wdateends = $_REQUEST["DateOrderEnds"];
            if (strlen($ifindby) > 0)
                $query = "update worksheets set dateorderends='" . $sdateends . "' where marketneedid=" . $inmneed;
            else
                $query = "update worksheets set dateorderends=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        mysql_close($con);
//$_SESSION['ShowError'] = 'Completing the postback';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    if (isset($_REQUEST['AddMarketNeed'])) {
        unset($_REQUEST['AddMarketNeed']);
        $query = "insert into marketneeds (UserID,Created,LastLogin,Title) values (" . $inuserid . ",'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','New Vehicle Purchase')";
        if (mysql_query($query, $con)) {
            $lastid = mysql_insert_id($con);
            $query = "insert into assignedreps (MarketNeedID, UserRepID, StartDate) values (" . $lastid . "," . $userid . ",'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
            if (mysql_query($query, $con))
                $addedneed = 'true';
        }
    }
// update customer notes
    if (isset($_REQUEST['UpdateCustomerNotes'])) {
        unset($_REQUEST['UpdateCustomerNotes']);
        if (isset($_REQUEST["UserNote"]) && ($inote != $_REQUEST["UserNote"])) {
            $inote = $_REQUEST["UserNote"];
            $query = "update users set Note='" . escapestr($inote) . "' where userid=" . $inuserid;
            mysql_query($query, $con);
        }
    }
    if (isset($_REQUEST["UpdateWaitPeriod"])) {
        $inoid = $_REQUEST["OrderID"];
        $inowait = $_REQUEST["WaitPeriod"];
        $query = "update orders set WaitPeriod='" . $inowait . "' where orderid=" . $inoid;
        mysql_query($query, $con);
    }
    if (isset($_REQUEST["UpdateDeposit"])) {
        $inoid = $_REQUEST["OrderID"];
        $inodep = $_REQUEST["DepositAmount"];
        $query = "update orders set DepositReceived='" . $inodep . "' where orderid=" . $inoid;
        mysql_query($query, $con);
    }

// Fetch all market needs for this customer to populate the dropdowns
    $index = 0;
    $mncountactive = 0;
    $query = "select title, showtouser, needscontact, active, completed, findby, depositrequired, followupdate, showtorep, marketneedid, created, state from marketneeds where UserID=" . $inuserid;
    $result = mysql_query($query, $con);
    while ($result && $row = mysql_fetch_array($result)) {
        $mnid[$index] = $row[9];
        $mncreated[$index] = $row[10];    // TODO: use the created field
        $mntitle[$index] = $row[0];
        $mnshowtouser[$index] = $row[1];
        $mnneedscontact[$index] = $row[2];
        $mnactive[$index] = $row[3];
        $mncompleted[$index] = $row[4];
        $mnfindby[$index] = $row[5];
        $mndepreq[$index] = $row[6];
        $mnfollowup[$index] = $row[7];
        $mnshowrep[$index] = $row[8];
        $mncestate[$index] = $row[11];
// TODO: figure out why this aborts the rendering of the page after tradeins
// (probably because it closes the sql connection) - need to make a nodb version
//$mnstep[$index] = getuserstep($inuserid, $mnid[$index]);
        if ($mnactive[$index] != 0)
            $mncountactive++;
        $index++;
    }

// Get the stage specific details...
    if ($instep >= 1) {
// fetch favorites
        $index = 0;
        $query = 'select VehicleID, Information,Visible from favorites where MarketNeedID = ' . $inmneed . ' order by Visible desc, LastUpdated desc';
        $result = mysql_query($query);
        while ($result && $row = mysql_fetch_array($result)) {
            $vehid[$index] = $row['VehicleID'];
            $vnote[$index] = $row['Information'];
            $vvisible[$index] = $row['Visible'];

// Get the vehicle data for the favorite
            $vehquery = "select m.name 'Make', v.year 'Year', v.model 'Model', v.style 'Style', v.ImageFile 'Image', m.Origin 'Origin' from vehicles v, makes m where m.makeid=v.makeid and v.vehicleid=" . $vehid[$index];
            $vehresult = mysql_query($vehquery);
            if ($row = mysql_fetch_array($vehresult)) {
                $makedets[$index] = $row['Make'];
                $yeardets[$index] = $row['Year'];
                $modeldets[$index] = $row['Model'];
                $styledets[$index] = $row['Style'];
                $vimage[$index] = $row['Image'];
                $origin[$index] = $row['Origin'];

// TODO: how about a group by lowmiles and then order by lowmiles to do this in one shot?
// First we retrieve the details for the high mileage side...
                $highquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=0 and vehicleid = " . $vehid[$index];
                $highresult = mysql_query($highquery);
                if ($row = mysql_fetch_array($highresult)) {
                    $highbb[$index] = $row['bestbuy'];
                    $highmilestart[$index] = $row['mileagestart'];
                    $highmileend[$index] = $row['mileageend'];
                    $highpricestart[$index] = $row['pricestart'];
                    $highpriceend[$index] = $row['priceend'];
                }
// Next we retrieve the details for the low mileage side...
                $lowquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=1 and vehicleid = " . $vehid[$index];
                $lowresult = mysql_query($lowquery);
                if ($row = mysql_fetch_array($lowresult)) {
                    $lowbb[$index] = $row['bestbuy'];
                    $lowmilestart[$index] = $row['mileagestart'];
                    $lowmileend[$index] = $row['mileageend'];
                    $lowpricestart[$index] = $row['pricestart'];
                    $lowpriceend[$index] = $row['priceend'];
                }
            }
            $index++;
        }
    }
    
$queryvehiclespecification = "SELECT * FROM  `searchplans` sp, searchplandetails spd WHERE sp.SearchPlanID = spd.SearchPlanID AND sp.MarketNeedID =".$inmneed;
$resultvehiclespecification = mysql_query($queryvehiclespecification, $con);  

 $querypos = "SELECT * FROM  `pasplans` pp, pasplandetails ppd WHERE pp.SearchPlanID = ppd.SearchPlanID AND pp.MarketNeedID =".$inmneed;
$resultpos = mysql_query($querypos, $con);  

    
      
    if ($instep >= 2) {
// Get the Assessment information (if any)...
        $query = 'select * from assessments where MarketNeedID = ' . $inmneed;
        $result = mysql_query($query, $con);
        if ($result && $row = mysql_fetch_assoc($result)) {
            $ahaveassess = 1;
            $BudgetCeiling = $row['BudgetCeiling'];
            $CareGiven = $row['CareGiven'];
            $Comfort = $row['Comfort'];
            $Created = $row['Created'];
            $Dependability = $row['Dependability'];
            $Economy = $row['Economy'];
            $ForProfessionalUse = $row['ForProfessionalUse'];
            $FuelEfficiency = $row['FuelEfficiency'];
            $HowSoon = $row['HowSoon'];
            $KeepFor = $row['KeepFor'];
            $LastUpdated = $row['LastUpdated'];
            $Luxury = $row['Luxury'];
            $MarketNeedID_db = $row['MarketNeedID'];
            $MileageGoal = $row['MileageGoal'];
            $MostlyDrive = $row['MostlyDrive'];
            $NeedType = $row['NeedType'];
            $Origin = $row['Origin'];
            $Particular = $row['Particular'];
            $Quietness = $row['Quietness'];
            $Roominess = $row['Roominess'];
            $Safety = $row['Safety'];
            $Security = $row['Security'];
            $SeenBySales = $row['SeenBySales'];
            $VehicleType = $row['VehicleType'];
            $BodyType = $row['BodyType'];
            $VehicleSize = $row['VehicleSize'];
            $DriveTrain = $row['DriveTrain'];
            $Notes = $row['Notes'];
            $Transmission = $row['Transmission'];
            $MileageFrom = $row['MileageFrom'];
            $MileageTo = $row['MileageTo'];
            $MileageCeiling = $row['MileageCeiling'];
            $ExtLike = $row['ExtLike'];
            $ExtDislike = $row['ExtDislike'];
            $IntLike = $row['IntLike'];
            $IntDisike = $row['IntDisike'];
            $BudgetFrom = $row['BudgetFrom'];
            $BudgetTo = $row['BudgetTo'];
            $BorrowMaxPayment = $row['BorrowMaxPayment'];
            $BorrowDownPayment = $row['BorrowDownPayment'];
            $VehicleNeed = $row['VehicleNeed'];
            $BrandLike = $row['BrandLike'];
            $BrandDislike = $row['BrandDislike'];
            $Model = $row['Model'];
            $VehicleAge = $row['VehicleAge'];
            $RateFuelEfficiency = $row['RateFuelEfficiency'];
            $RateMintenanceCost = $row['RateMintenanceCost'];
            $RateReliability = $row['RateReliability'];
            $RateLuxury = $row['RateLuxury'];
            $RateSporty = $row['RateSporty'];
            $RateSafety = $row['RateSafety'];
            $FrontSType = $row['FrontSType'];
            $BedType = $row['BedType'];
            $Leather = $row['Leather'];
            $HeatedSeat = $row['HeatedSeat'];
            $Navigation = $row['Navigation'];
            $SunRoof = $row['SunRoof'];
            $AlloyWheels = $row['AlloyWheels'];
            $RearWindow = $row['RearWindow'];
            $BedLiner = $row['BedLiner'];
            $EntertainmentSystem = $row['EntertainmentSystem'];
            $ThirdRD = $row['ThirdRD'];
            $CRow = $row['CRow'];
            $PRHatch = $row['PRHatch'];
            $BackupCamera = $row['BackupCamera'];
            $TPackage = $row['TPackage'];
            $OtherPrefereces = $row['OtherPrefereces'];
            $OtherReallyHave = $row['OtherReallyHave'];
            $Sportiness = $row['Sportiness'];
            $StatusLevel = $row['StatusLevel'];
            $AdditionalInfo = $row['AdditionalInfo'];

            $query = "update assessments set SeenBySales=1 where AssessmentID=" . $row['AssessmentID'];
            mysql_query($query, $con);
        } else
            $ahaveassess = 0;

// Get the current Request for Consultation Information (if any)...
        $query = 'select Notes, ConsultationID from consultations where MarketNeedID = ' . $inmneed;
        $result = mysql_query($query, $con);
        if ($result && $row = mysql_fetch_array($result)) {
            $ahaveconsult = 1;
            $anote = $row[0];

            $query = "update consultations set SeenBySales=1 where ConsultationID=" . $row[1];
            mysql_query($query, $con);
        } else
            $ahaveconsult = 0;
    }

    if ($instep >= 2) {
// Get the current Quote Request Information (if any)...
        $query = "select QuoteRequestID, QuoteType, Year, Make, Model, Style, MileageCeiling, EngineCylinders, Transmission,
CabType, FrontSeatType, BedType, SeatMaterial, WheelDriveType, WheelCovers, RadioNeeded, RadioFM, RadioCassette, RadioCD, RadioSat, PowerDoors, PowerWindows,
PowerSeats, HeatedSeats, AirConditioning, RemoteEntry, TractionControl, SecuritySystem, CruiseControl, Navigation, RearSlidingWindow, BedLiner, TowPackage,
ColorCombination, SpecialRequests, Pickup, DeliverToCity, DeliverToState, LuggageRack, SunRoof, Visible from quoterequests where MarketNeedID=" . $inmneed . " order by Visible desc";
        $result = mysql_query($query);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $aquoteid[$index] = $row['QuoteRequestID'];
            $aquotetype[$index] = $row['QuoteType'];
            $ayear[$index] = $row['Year'];
            $amake[$index] = $row['Make'];
            $amodel[$index] = $row['Model'];
            $astyle[$index] = $row['Style'];
            $aqmileage[$index] = $row['MileageCeiling'];
            $aseattype[$index] = $row['SeatMaterial'];
            $aengine[$index] = $row['EngineCylinders'];
            $adrive[$index] = $row['WheelDriveType'];
            $atrans[$index] = $row['Transmission'];
            $awheels[$index] = $row['WheelCovers'];
            $acab[$index] = $row['CabType'];
            $afront[$index] = $row['FrontSeatType'];
            $abedtype[$index] = $row['BedType'];
            $aradflex[$index] = $row['RadioNeeded'];
            $aradfm[$index] = $row['RadioFM'];
            $aradcas[$index] = $row['RadioCassette'];
            $aradcd[$index] = $row['RadioCD'];
            $aradsat[$index] = $row['RadioSat'];
            $apdoor[$index] = $row['PowerDoors'];
            $apwin[$index] = $row['PowerWindows'];
            $apseat[$index] = $row['PowerSeats'];
            $aheat[$index] = $row['HeatedSeats'];
            $aair[$index] = $row['AirConditioning'];
            $aremote[$index] = $row['RemoteEntry'];
            $atraction[$index] = $row['TractionControl'];
            $asecure[$index] = $row['SecuritySystem'];
            $acruise[$index] = $row['CruiseControl'];
            $anavsys[$index] = $row['Navigation'];
            $arearwin[$index] = $row['RearSlidingWindow'];
            $abed[$index] = $row['BedLiner'];
            $atow[$index] = $row['TowPackage'];
            $acolors[$index] = $row['ColorCombination'];
            $anotes[$index] = $row['SpecialRequests'];
            $adelorpick[$index] = $row['Pickup'];
            $adelcity[$index] = $row['DeliverToCity'];
            $adelstate[$index] = $row['DeliverToState'];
            $aluggage[$index] = $row['LuggageRack'];
            $asunroof[$index] = $row['SunRoof'];
            $avisible[$index] = $row['Visible'];

// get current market studies (firm quotes)
            $fquery = "Select UpdateStatus,OrderType,Mileage,PricePoint,ImageFile,AdminNote,FirmQuoteID from firmquotes where QuoteRequestID=" . $aquoteid[$index];
            $fresult = mysql_query($fquery);
            if ($fresult && ($frow = mysql_fetch_array($fresult))) {
                if ($frow[0] == 'Researching')
                    $fstatus[$index] = 1;
                else
                    $fstatus[$index] = 0;
                $fordertype[$index] = $frow[1];
                $fmiles[$index] = $frow[2];
                $fprice[$index] = $frow[3];
                $fimage[$index] = $frow[4];
                $fnote[$index] = $frow[5];
                $fquoteid[$index] = $frow[6];
            }
            $index++;
        }
        if ($index > 0)
            $ahavequotes = 1;
        else
            $ahavequotes = 0;
    }

    if ($instep >= 3) {
// Get the Orders and Posts...
        $query = "select q.QuoteRequestID, q.QuoteType, q.Year, q.Make, q.Model, q.Style, f.OrderType, o.accepted, o.Expires, o.DepositReceived, o.WaitPeriod, o.orderid, f.AddDepositAmount from orders o,orderfor of,firmquotes f,quoterequests q where o.orderid=of.orderid and f.firmquoteid=of.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=" . $inmneed;
        $result = mysql_query($query);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $oquoteid[$index] = $row[0];
            $oquotetype[$index] = $row[1];
            $oyear[$index] = $row[2];
            $omake[$index] = $row[3];
            $omodel[$index] = $row[4];
            $ostyle[$index] = $row[5];
            $oordertype[$index] = $row[6];
            $oaccepted[$index] = $row[7];
            $oexpires[$index] = $row[8];
            $odeprec[$index] = $row[9];
            $owait[$index] = $row[10];
            $oorderid[$index] = $row[11];
            $odepneeded[$index] = $row[12] + 250.00;
            $odepreq[$index] = 1;
            $mnquery = "select depositrequired from marketneeds where marketneedid=" . $inmneed;
            $mnresult = mysql_query($mnquery);
            if ($mnrow = mysql_fetch_array($mnresult))
                if ($mnrow[0] != 1)
                    $odepreq[$index] = 0;

            $index++;
        }

// get watches
        $query = "select q.QuoteRequestID, q.QuoteType, q.Year, q.Make, q.Model, q.Style, f.OrderType, w.accepted, w.Expires, w.watchid from watches w,watchfor wf,firmquotes f,quoterequests q where w.watchid=wf.watchid and f.firmquoteid=wf.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=" . $inmneed;
        $result = mysql_query($query);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $wquoteid[$index] = $row[0];
            $wquotetype[$index] = $row[1];
            $wyear[$index] = $row[2];
            $wmake[$index] = $row[3];
            $wmodel[$index] = $row[4];
            $wstyle[$index] = $row[5];
            $wordertype[$index] = $row[6];
            $waccepted[$index] = $row[7];
            $wexpires[$index] = $row[8];
            $wwatchid[$index] = $row[9];
            $index++;
        }
    }

    mysql_close($con);
}

function makefind($make){

$con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
if($con)
{
mysql_select_db(DB_SERVER_DATABASE, $con);

$querymake = "SELECT Name FROM  `makes` WHERE  `MakeID` =".$make;
$resultmake = mysql_query($querymake, $con);
$makerow = mysql_fetch_assoc($resultmake);
return $makerow['Name'];
mysql_close($con);
}



}


?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function trimAll(sString)
    {
        while (sString.substring(0, 1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while (sString.substring(sString.length - 1, sString.length) == ' ')
        {
            sString = sString.substring(0, sString.length - 1);
        }
        return sString;
    }

    function validateDataOnSubmit()
    {
        var datechars = new RegExp("[^0123456789:/ -]");
        var pricechars = new RegExp("[^0123456789.]");

        var vtitle = document.getElementById("NeedTitle");
        var tString = trimAll(vtitle.value);
        if (tString.length < 1)
        {
            alert("Error that must be corrected:" + '\n' + "Title for Market Need must be filled in." + '\n');
            vtitle.focus();
            vtitle.style.background = '#ffcccc';
            return false;
        }
        else
            vtitle.style.background = 'White';

        vtitle = document.getElementById("FollowUp");
        tString = trimAll(vtitle.value);
        if ((tString.length > 0) && (datechars.test(tString)))
        {
            alert("Error that must be corrected:" + '\n' + "Follow Up Date can only contain digits, dashes, slashes and colons (preferred format YYYY-MM-DD HH:MM:SS)." + '\n');
            vtitle.focus();
            vtitle.style.background = '#ffcccc';
            return false;
        }
        else
            vtitle.style.background = 'White';

        vtitle = document.getElementById("FindBy");
        tString = trimAll(vtitle.value);
        if ((tString.length > 0) && (datechars.test(tString)))
        {
            alert("Error that must be corrected:" + '\n' + "Follow Up Date can only contain digits, dashes, slashes and colons (preferred format YYYY-MM-DD HH:MM:SS)." + '\n');
            vtitle.focus();
            vtitle.style.background = '#ffcccc';
            return false;
        }
        else
            vtitle.style.background = 'White';

        /*
         var dprice = document.getElementById("Deposit");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Deposit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else if(parseFloat(tString) < 250.00)
         {
         alert("Error that must be corrected:"+'\n'+"Deposit can not be less than $250."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else if(parseFloat(tString) > 500.00)
         {
         alert("Error that must be corrected:"+'\n'+"Deposit can not be greater than $500."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Deposit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("SecKey");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Second Key Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Second Key Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("NavDisc");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Nav Disc Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Nav Disc Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("SelRep");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Seller Repair Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Seller Repair Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("PurRep");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Purchaser Repair Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Purchaser Repair Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("PurDep");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Specific Vehicle Purchase Deposit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Specific Vehicle Purchase Deposit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         */

        return true;
    }

// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);

// control keys
        if ((key == null) || (key == 0) || (key == 8) ||
                (key == 9) || (key == 13) || (key == 27))
            return true;

// numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        else
            return false;
    }

    function addnewneed()
    {
        var r = confirm("Are you sure you want to Add a Customer Experience?")
        if (r == true)
        {
            alert("Please note, you will not see this new experience on this page.  Your customer will show up again on your dashboard in stage 1." + '\n')
            return true;
        }
        else
        {
            return false;
        }
    }

    function checkorder()
    {
        var dperiod = document.getElementById("WaitPeriod");
        tString = trimAll(dperiod.value);
        if (tString.length < 1)
        {
            alert("Error that must be corrected:" + '\n' + "Wait Period must be specified." + '\n');
            dperiod.focus();
            dperiod.style.background = '#ffcccc';
            return false;
        }
        else
            dperiod.style.background = 'White';

        return true;
    }

    function ceactivechanged(u)
    {
        var theselect = document.getElementById("ceactive");
        if (theselect.options[theselect.selectedIndex].value > 0) // support the 'choose one' item
        {
            var thelocation = 'salesrepactions.php?Switching=1&ForUserID=' + u + '&MarketNeedID=' + theselect.options[theselect.selectedIndex].value + '#mnid';
            self.location = thelocation;
        }
    }

    function ceprevchanged(u)
    {
        var theselect = document.getElementById("ceprev");
        if (theselect.options[theselect.selectedIndex].value > 0) // support the 'choose one' item
        {
            var thelocation = 'salesrepactions.php?Switching=1&ForUserID=' + u + '&MarketNeedID=' + theselect.options[theselect.selectedIndex].value + '#mnid';
            self.location = thelocation;
        }
    }

    $(function()
    {
        $('.date-pick').datePicker({clickInput: true});
    });

// when the cestate select is changed - update active, show, completed, archive reason appropriately
// supported states
// echo '<option value="0">Active</option>';
// echo '<option value="10">Followup: Search on Hold</option>';
// echo '<option value="11">Followup: Check In Later</option>';
// echo '<option value="20">Lost Interest</option>';
// echo '<option value="21">Disliked System</option>';
// echo '<option value="22">Disliked SalesRep</option>';
// echo '<option value="30">Spam Customer</option>';
// echo '<option value="31">Test or Demo Customer</option>';
// echo '<option value="100">Bought One!</option>';

    function cestatechanged()
    {
        var eactive = document.getElementById("Active");
        var ecompleted = document.getElementById("Completed");
        var eshowrep = document.getElementById("ShowRep");
        var econtact = document.getElementById("NeedsContact");
        var ear = document.getElementById("ArchiveReason");
        var estate = document.getElementById("CEState");

// set archive reason text box
        ear.value = estate.options[estate.selectedIndex].text;

// only one active state
        if (estate.options[estate.selectedIndex].value == 0) // active
        {
            eactive.checked = true;
            eshowrep.checked = true;
            ecompleted.checked = false;
        }

// only one completed state
        else if (estate.options[estate.selectedIndex].value == 100) // bought one
        {
            eactive.checked = false;
            eshowrep.checked = false;
            ecompleted.checked = true;
        }

// follow up states are active but not seen on dashboard until the follow up date is < 24 hours in the future (or in the past)
        else if (estate.options[estate.selectedIndex].value >= 10 &&
                estate.options[estate.selectedIndex].value < 20) // followup: *
        {
            eactive.checked = true;
            eshowrep.checked = false;
            ecompleted.checked = false;
        }

// all the rest are pure archive: not active, not shown, not completed
        else
        {
            eactive.checked = false;
            eshowrep.checked = false;
            ecompleted.checked = false;
        }

// needs contact checkbox could match active checkbox
// NOT YET since needs contact appears to be a reply needed bit that is set if customer does something
// so this should be shown obviously in the dashboard view
//econtact.checked = eactive.checked;
    }


</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php // require("headerend.php"); ?>

<!-- TODO: complete date picker insert from: http://www.kelvinluck.com/assets/jquery/datePicker/v2/demo/-->
<style type="text/css">@import "datepicker.css";</style>
<script type="text/javascript" src="common/scripts/date.js"></script>
<script type="text/javascript" src="common/scripts/jquery.datepicker.js"></script>
<script type="text/javascript" src="common/scripts/jquery.datePickerMultiMonth.js"></script>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;"> Customer Review</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: -5px;">
                <p class="blackeleven" style="margin: 0pt;"><a href="mydashboard.php#admintab">Go back to My Dashboard</a></p>
                <br clear="all" />

<?php
//echo '<p style="color: rgb(133, 193, 27); font-size: 16px; margin-top: 0pt; float: left;"><strong>Customer Details</strong></p>';

echo '<h4 class="subhead"><button class="hideshow" id="span_curtomer_details" onclick="javascript:showhidediv(\'curtomer_details\', \'span_curtomer_details\');"  >Hide</button>&nbsp;&nbsp;Customer Details</h4>';
echo '<span style="float: right;">';
// send message form
echo '<form action="sendmessage.php" method="post" target="_blank">';
echo '<input type="hidden" value="' . $inuserid . '" name="MessageToID" />';
echo '<input type="hidden" value="salesrepactions.php?ForUserID=' . $inuserid . '&MarketNeedID=' . $inmneed . '" name="ReturnTo" />';
echo '<p style="color: #fff;font-size: 12px; margin-top: -30px;">';
echo 'Send a Message: ';
echo '<button type="submit" value="" class="blueongrey"><img alt="Send" title="Send a Message" align="middle" border="0" src="common/layout/mailicon.gif" /></button>';
echo '</p>';
echo '</form></span>';
echo '<div id="curtomer_details" style="display:block">';
?>

<?php
// customer details section: name, email, login, address, phone, id, note
echo '<table  style="float: left; width:100%" border="0"><tbody>';
echo '<tr><td style="width: 33%;"><strong>';
echo 'ID';
echo '</strong></td><td  >';
echo $inuserid;
echo '</td></tr><tr><td><strong>';
echo 'Franchise';
echo '</strong></td><td >';
echo $zipfranname;
echo '</td></tr><tr><td><strong>';
echo 'Name';
echo '</strong></td><td>';
echo $ifname . ' ' . $ilname;
echo '</td></tr><tr><td><strong>';
echo 'Email';
echo '</strong></td><td>';
echo '<a href="mailto:' . $iemail . '">' . $iemail . '</a>';
echo '</td></tr><tr><td><strong>';
echo 'Login';
echo '</strong></td><td>';
if ($iuseemail == 1)
    echo '(Same as Email)';
else
    echo $ilogin;
echo '</td></tr><tr><td><strong>';
echo 'Last Login';
echo '</strong></td><td>';
echo $ilastlogin . ' <span >(' . timesincenow($ilastlogin) . ')</span>';
echo '</td></tr>';
echo '<td><td>&nbsp;</td></tr>';
echo '<tr><td valign="top"><strong>';
echo 'Primary Address';
echo '</strong></td><td>';
$theaddress = "";
if (isset($iaddzip)) {
    if (!is_null($iaddaddr1)) {
        $theaddress .= $iaddaddr1;
        if (!is_null($iaddaddr2)) {
            $theaddress .= ' ' . $iaddaddr2;
            if (!is_null($iaddcity)) {
                $theaddress .= ', ' . $iaddcity;
                if (!is_null($iaddstate)) {
                    $theaddress .= ', ' . $iaddstate;
                    $theaddress .= ', ' . $iaddzip;
                } else
                    $theaddress .= ', ' . $iaddzip;
            }
            elseif (!is_null($iaddstate)) {
                $theaddress .= ', ' . $iaddstate;
                $theaddress .= ', ' . $iaddzip;
            } else
                $theaddress .= ', ' . $iaddzip;
        }
        else {
            if (!is_null($iaddcity)) {
                $theaddress .= ' <br/>' . $iaddcity;
                if (!is_null($iaddstate)) {
                    $theaddress .= ', ' . $iaddstate;
                    $theaddress .= ', ' . $iaddzip;
                } else
                    $theaddress .= ', ' . $iaddzip;
            }
            elseif (!is_null($iaddstate)) {
                $theaddress .= ' <br/> ' . $iaddstate;
                $theaddress .= ', ' . $iaddzip;
            } else
                $theaddress .= ' <br/> ' . $iaddzip;
        }
    }
    else {
        if (!is_null($iaddcity)) {
            $theaddress .= $iaddcity;
            if (!is_null($iaddstate)) {
                $theaddress .= ', ' . $iaddstate;
                $theaddress .= ', ' . $iaddzip;
            } else
                $theaddress .= $iaddzip;
        }
        elseif (!is_null($iaddstate)) {
            $theaddress .= $iaddstate;
            $theaddress .= ', ' . $iaddzip;
        } else
            $theaddress .= $iaddzip;
    }
}
echo $theaddress;
echo '<a style="margin-left: 35%;" href="http://maps.google.com/maps?q=' . urlencode(strip_tags($theaddress)) . '">Map</a>';
echo '</td></tr>';
echo '<tr><td><strong>';
echo 'Phone Numbers';
echo '</strong></td>';

// show phone numbers if present
if (isset($inumid)) {
    $count = count($inumid);
    for ($i = 0; $i < $count; $i++) {
        if ($i == 0)
            echo '<td>';
        else
            echo '<tr><td>&nbsp;</td><td>';
        echo '(' . substr($inumnumber[$i], 0, 3) . ') ' . substr($inumnumber[$i], 3, 3) . '-' . substr($inumnumber[$i], 6, 4);
        if (isset($inumext[$i]))
            echo ' x ' . $inumext[$i];
        echo ' <span style="color: rgb(133, 193, 27);">[' . $inumtype[$i] . '] &nbsp;';
        if ($inumtext[$i] == 1)
            echo '[&#10003;Texting]';
//else echo '[No Texting]';
        echo '</span>';
        if (isset($inuminfo[$i]))
            echo ' Note: ' . $inuminfo[$i];
        echo '</td>';
    }
} else
    echo '<td>&nbsp;</td>';
echo '</tr>';

// show alternate emails if present
if (isset($iemailid)) {
    echo '<tr><td><strong>';
    echo 'Alternate Emails';
    echo '</strong></td>';
    $count = count($iemailid);
    for ($i = 0; $i < $count; $i++) {
        if ($i == 0)
            echo '<td style="color: rgb(117, 117, 117);">';
        else
            echo '<tr><td>&nbsp;</td><td style="color: rgb(117, 117, 117);">';
        echo $iemailtype[$i] . ' - ' . $iemailaddr[$i] . ' (On Phone - ';
        if ($iemaile2p[$i] == 1)
            echo 'Yes)';
        else
            echo 'No)';
        echo '</td>';
    }
    echo '</tr>';
}
?>
                <form action="salesrepactions.php" method="post" name="custnoteform" >
                    <tr>
                        <td><strong>Notes</strong></td>
                        <td> 
<?php
echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
echo '<input type="hidden" value="true" name="UpdateCustomerNotes" />';
echo  $inote ;
?>

                        </td>
                    </tr>    
 
                </form>
<?php
echo '</tbody></table>';
echo '</div>';

?>



<?php



if ($instep >= 2) { ?>

<!--    Fetch vehicle specifiction
developed by: ketan
date: 24-07-2015
-->
<?php
if($resultvehiclespecification){
?>


    <h4 class="subhead"> <button onclick="javascript:showhidediv('vehicle_specification', 'span_vehicle_specification');" id="span_vehicle_specification" class="hideshow">Hide</button>&nbsp;&nbsp;Vehicle Search Plan </h4>
                        <div style="display: block;" id="vehicle_specification">            
                          <table class="table table-bordered">
    <thead>
      <tr>
        <th>Make</th>
        <th>Model</th>
        <th>Style</th>
        <th>Year</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php
 
    
    while ($row1 = mysql_fetch_assoc($resultvehiclespecification)) { ?>
      <tr>
          <td><strong><?php echo ($row1['Specific'])? $row1['TextMake'] : makefind($row1['Make']);?></strong></td>
            <td><?php echo ($row1['Specific'])? $row1['TextModel'] :$row1['Model'] ?></td>
            <td><?php echo ($row1['Specific'])? $row1['TextStyle'] : $row1['Style']; ?></td>
            <td><?php echo ($row1['Specific'])? $row1['TextYear'] : $row1['Year']; ?></td>
        <td><form action="<?=WEB_SERVER_NAME?>edit_vehiclespecifiction.php" method="post"> 
        <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
        <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
         <input type="hidden" value="<?php echo $row1['SearchPlanDetailID'];?>" name="SearchPlanDetailID" />
             <button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>
         </form></td>
      </tr> 
    <?php } ?>
    </tbody>
  </table>
                        </div>
<?php } ?>
    
    
    <?php
        if($resultpos){
    ?>


    <h4 class="subhead"> <button onclick="javascript:showhidediv('vehicle_pas', 'span_vehicle_pas');" id="span_vehicle_pas" class="hideshow">Hide</button>&nbsp;&nbsp;Price & Availability Study</h4>
                        <div style="display: block;" id="vehicle_pas">            
                          <table class="table table-bordered">
    <thead>
      <tr>
        <th>Make</th>
        <th>Model</th>
        <th>Style</th>
        <th>Year</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
   <?php
    while ($row1 = mysql_fetch_assoc($resultpos)) { ?>
      <tr>
       
          <td><strong><?php echo ($row1['Specific'])? $row1['TextMake'] : makefind($row1['Make']);?></strong></td>
                        <td><?php echo ($row1['Specific'])? $row1['TextModel'] :$row1['Model'] ?></td>
                        <td><?php echo ($row1['Specific'])? $row1['TextStyle'] : $row1['Style']; ?></td>
                        <td><?php echo ($row1['Specific'])? $row1['TextYear'] : $row1['Year']; ?></td>
         
         
         <td><form action="<?=WEB_SERVER_NAME?>edit_vehiclepas.php" method="post"> 
         <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
         <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
         <input type="hidden" value="<?php echo $row1['SearchPlanDetailID'];?>" name="SearchPlanDetailID" />
         <button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>
         </form></td>
      </tr> 
    <?php } ?>
    </tbody>
  </table>
                        </div>
<?php } ?>

   <?php if ($ahaveconsult == 1) {
// echo '<br/><p style="color: rgb(133, 193, 27); font-size: 16px; margin-bottom: 5px;"><strong>';
// echo 'Requested Consultation';
// echo '</strong></p>';
        echo '<h4 class="subhead">Requested Consultation</h4>';
        echo '<p style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        echo $anote;
        echo '</p>';
    }
}
if ($instep >= 1) {
    $count = isset($vehid) ? count($vehid) : 0;
    if ($count > 0) {
// echo '<br/><p style="color: rgb(133, 193, 27); font-size: 16px; margin-bottom: 5px;"><strong>';
// echo 'Favorites';
// echo '</strong></p><br/>';
        echo '<h4 class="subhead">Favorites</h4>';
        echo '<br/>';
        for ($index = 0; $index < $count; $index++) {
            echo '<table style="margin-left: 10px;" align="left" border="0" cellpadding="0"><tbody><tr><td width="10">';
            if ($vvisible[$index] == 0)
                echo 'X';
            echo '</td><td width="178"><a href="details.php?vehid=' . $vehid[$index] . '" target="_blank">';
            if (!file_exists($vimage[$index]))
                $imagefile = '';
            else
                $imagefile = $vimage[$index];

            if ($imagefile) {
                $max_width = 170;
                $max_height = 170;
                echo '<img id="vehimage" src="loadimage.php?image=' . $imagefile . '&mwidth=' . $max_width . '&mheight=' . $max_height . '" border="0" hspace="10" vspace="10" />';
            } else
                echo '** Image currently not available **';
            echo '</a></td><td valign="top" width="278"><p class="blackfourteen" style="margin: 0pt;">';
            echo '<a href="details.php?vehid=' . $vehid[$index] . '" target="_blank"><strong>';
            echo $yeardets[$index] . ' ' . $makedets[$index] . ' ' . $modeldets[$index] . ' ' . $styledets[$index];
            echo '</strong></a></p>';
            echo '<p class="favorites"><span style="color: rgb(133, 193, 27);">Vehicle Type:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            echo $origin[$index] . '</span></p>';
            echo '<p class="favorites"><span style="color: rgb(133, 193, 27);">Low Mileage:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($lowmilestart[$index] < 1)
                echo 'Below ' . number_format($lowmileend[$index]);
            else
                echo number_format($lowmilestart[$index]) . ' - ' . number_format($lowmileend[$index]);
            echo '</span><br/><span style="color: rgb(133, 193, 27);">Price:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($lowpricestart[$index] < 5000)
                echo 'Below $' . number_format($lowpriceend[$index]);
            else
                echo '$' . number_format($lowpricestart[$index]) . ' to $' . number_format($lowpriceend[$index]);
            echo '</span></p>';
            if ($lowbb[$index] == 1)
                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="excellent" style="padding: 1px;">Excellent Availability!</span>';
            else
                echo '<br/>';
            echo '<p class="favorites"><span style="color: rgb(133, 193, 27);">Average Mileage:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($highmilestart[$index] < 1)
                echo 'Below ' . number_format($highmileend[$index]);
            else
                echo number_format($highmilestart[$index]) . ' - ' . number_format($highmileend[$index]);
            echo '</span><br/><span style="color: rgb(133, 193, 27);">Price:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($highpricestart[$index] < 5000)
                echo 'Below $' . number_format($highpriceend[$index]);
            else
                echo '$' . number_format($highpricestart[$index]) . ' to $' . number_format($highpriceend[$index]);
            echo '</span></p>';
            if ($highbb[$index] == 1)
                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="excellent" style="padding: 1px;">Excellent Availability!</span>';
            else
                echo '<br/>';
            echo '</td><td width="60"></td></tr>';
            echo '<tr><td colspan="3" style="color: rgb(117, 117, 117); font-weight: bold; font-size: 13px;">';
            echo 'Notes: <span style="color: rgb(133, 193, 27); font-size: 13px; font-weight: bold;" >';
            if (isset($vnote[$index]) && (strlen($vnote[$index]) > 0))
                echo $vnote[$index];
            echo '</span></td></tr></tbody></table>';
            if ($index < ($count - 1))
                echo '<center><img src="common/layout/short-bar.gif" style="padding: 10px;" border="0" /></center>';
        }
        echo '</p><br clear="all" />';
    }
}
?>
            </div><!-- grid eight -->
        </div><!-- grid eight grey -->
    </div><!-- grid eight container -->
</div><!--end content-->
<div id="dialog" title=" " style="display: none;">
    <textarea name="notetext" id="notetext" placeholder="let me grow and close the popup" style="resize:vertical;width:100%;height:100%"></textarea>
</div>
<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>


<script type="text/javascript">
    function showhidediv(tdiv, ttrigger)
    {
        var thediv = document.getElementById(tdiv);
        var thetrigger = document.getElementById(ttrigger);

        if (thediv.style.display == "block")
        {
            thediv.style.display = "none";
            thetrigger.innerHTML = "Show";
        }
        else
        {
            thediv.style.display = "block";
            thetrigger.innerHTML = "Hide";
        }
        return true;
    }

    $(document).ready(function() {
        $("input:text").click(function() {
            var element = $(this);
            $("#notetext").val(element.val());
            $("#dialog").dialog({
                modal: true,
                title: " ",
                close: function(event, ui) {
                    element.val($("#notetext").val());
                    $("#dialog").dialog("destroy");
                }
            });
        });
    });

</script>

<style>
    .assessment_insidetd{
        background: none repeat scroll 0% 0% gray; 
        color: white;
    }

    .assessment_inside{
        margin-left: 10px;
    }


    td, th {
        padding: 3px !important;
    }
    select, textarea, input[type='text'], input[type='password'] {
        border: 1px solid #aaaaaa;
        background: #fff;
        resize: none;
        color: #252525;
        cursor: text;
        padding: 3px;
        font-family: Arial, Helvetica, Sans Serif;
        font-size: 13px;
        width: 60%;
    }

  
</style>
