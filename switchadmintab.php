<?php require("globals.php"); ?>
<?php
    require_once("common/functions/usernavfunctions.php");

    $userid = $_SESSION['userid'];

    $ucust = getuserprofile($userid, 'Customer');
    $uadmin = getuserprofile($userid, 'Administrator');
    $uterr = getuserprofile($userid, 'Teritory Admin');
    $ufran = getuserprofile($userid, 'Franchisee');
    $uops = getuserprofile($userid, 'Operations Manager');
    $ugm = getuserprofile($userid, 'General Manager');
    $ubuyer = getuserprofile($userid, 'Researcher');
    $usrep = getuserprofile($userid, 'Sales Representative');

    if(isset($_REQUEST['toadmintab']))
    {
        $toadmintab = $_REQUEST['toadmintab'];
        switch($toadmintab)
        {
            case 'admin':
                if($uadmin == 'true')
                {
                    $_SESSION['curadmintab'] = 'admin';
                }
                break;
            case 'terrrep':
                if(($uadmin == 'true') || ($uterr == 'true'))
                {
                    $_SESSION['curadmintab'] = 'terrrep';
                }
                break;
            case 'franchise':
                if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true'))
                {
                    $_SESSION['curadmintab'] = 'franchise';
                }
                break;
            case 'genman':
                if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true'))
                {
                    $_SESSION['curadmintab'] = 'genman';
                }
                break;
            case 'opsman':
                if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($uops == 'true'))
                {
                    $_SESSION['curadmintab'] = 'opsman';
                }
                break;
            case 'authbuyer':
                if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ubuyer == 'true'))
                {
                    $_SESSION['curadmintab'] = 'authbuyer';
                }
                break;
            case 'salesrep':
                if(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true') || ($usrep == 'true'))
                {
                    $_SESSION['curadmintab'] = 'salesrep';
                }
                break;
            default:
                $_SESSION['curadmintab'] = 'none';
                break;
        }
    }
    header('Location: mydashboard.php#admintab');
?>
