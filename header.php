<?php require("globals.php"); ?>
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->    
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<link rel="shortcut icon" href="assets/img/ico/favicon.ico">
<link rel="apple-touch-icon" sizes="144x144" href="assets/img/ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/img/ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/img/ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" href="assets/img/ico/apple-touch-icon-57x57.png">

<!-- Bootstrap Core CSS -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,700' rel='stylesheet' type='text/css'>
<link href="assets/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="assets/css/pe-icons.css" rel="stylesheet">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- jQuery -->
<script src="assets/js/jquery.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="assets/css/colorbox.css" />
<script src="assets/js/jquery.colorbox.js"></script>
<!--<script src="assets/js/jquery.anystretch.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#signin").click(function() {
            $.colorbox({href: "login_pop.php", scrolling: false, maxWidth: "80%"});
        });

        $("#signup").click(function() {
			//$.colorbox({href:"register_pop.php",scrolling:false,maxWidth: "100%"});
            location.href = "<?php echo WEB_SERVER_NAME . 'register.php?request=create_account'; ?>";
        });
        $("#dashboard").click(function() {
            location.href = "<?php echo WEB_SERVER_NAME . 'dashboard.php'; ?>";
        });
        $("#logout").click(function() {
            location.href = "<?php echo WEB_SERVER_NAME . 'logout.php'; ?>";
        });
		
		//Contact Form Pop-up
		$(".contact-form-pop-up").click(function() {
			
			if( $( window ).width() > 767 ) { 				
				$.colorbox({
					href: "contact-form-popup.php", 
					scrolling: false, 
					maxWidth: "98%",
					width: "50%"
				});
			} else {
				$.colorbox({
					href: "contact-form-popup.php", 
					scrolling: false, 
					maxWidth: "98%",
					width: "100%"
				});
			}
            
        });
		
		//Chat Form Pop-up
		$(".chat-form-pop-up").click(function() {
			
			if( $( window ).width() > 767 ) {
				 $.colorbox({
					href: "chat-form-popup.php", 
					scrolling: false, 
					maxWidth: "98%",
					width: "50%"
				});
				
			} else {
				$.colorbox({
					href: "chat-form-popup.php", 
					scrolling: false, 
					maxWidth: "98%",
					width: "100%"
				});
			}
           
        });
		
		//Chat Form Pop-up
		$(".contact-by-phone-pop-up").click(function() {
			
			if( $( window ).width() > 767 ) {
				$.colorbox({
					href: "contact-by-phone-popup.php", 
					scrolling: false, 
					maxWidth: "98%",
					width: "50%"
				});
			} else {
				$.colorbox({
					href: "contact-by-phone-popup.php", 
					scrolling: false, 
					maxWidth: "98%",
					width: "100%"
				});
			}
			
            
        });
    });

    $(document).ready(function() {
        'use strict';
        /*jQuery('#headerwrap').backstretch([
            "assets/img/bg/bg1.png",
        ], {duration: 4000, fade: 500});*/
     /*$('#headerwrap').anystretch("assets/img/bg/bg1.png");*/
	 $("#bgdiv1").backstretch("assets/img/bg/imgo2-temp.jpg");
//         $(".abc").click(function(){
//             
//           window.location="<?php echo WEB_SERVER_NAME . '#how_work'; ?>";
//         });
    });
</script>
<style>
    .caret {
        border-left: 4px solid transparent;
        border-right: 4px solid transparent;
        border-top: 4px solid;
        border-top: medium none;
        height: 0;
        margin-left: 2px;
        vertical-align: middle;
        width: 0;
    }
</style>

</head>

<body id="page-top" class="index <?=isset($_SESSION['user'])?'logged-in':''?>">

 <div style="display: none">
   <pre>
   <?php print_r($_SESSION); ?>
   </pre>
 </div>
    
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top bounceInDown" data-wow-delay="2s">
                
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">            
            
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
         
            <?php //echo'<pre>';print_r($_SESSION);echo'</pre>';
            if($_SERVER[REQUEST_URI] == "/index.php" || $_SERVER[REQUEST_URI] == "/") : ?>
			<div class="header_new_menu">
				<a href="add_vehicle_spec.php" style="color: white">
					<span style="background-color: rgb(145, 214, 21) ! important; text-align: center; width: 48%; padding: 10px; font-size: 12px;">
						START THE SEARCH PROCESS
					</span>
                </a>                     
				
				<span style="margin-left: 5px;"></span>
                      
				<a href="get_price_and_expert_advice.php" style="color: white">     
					<span style="background-color: rgb(73, 94, 255) ! important; text-align: center; width: 48%; padding: 10px; font-size: 12px;">
                        GET HELP WITH WHAT TO BUY
					</span>
                </a>
            </div>
           <?php endif; ?>
            
            <div class="login_logout">              
                
            <div>
                <?php if (isset($_SESSION['user'])) {?>                       
                    <a href="javascript:void(0);"  id="dashboard" style="color: green;">My Dashboard</a>
                <?php } else {
                     if(strtolower($_SERVER[REQUEST_URI]) != "/register.php" && strtolower($_SERVER[REQUEST_URI]) != "/my_login.php"){
                    ?>
                    <a href="javascript:void(0);"  id="signin" style="color: green;">Login</a>                        
                     <?php }
                     } ?>                                                
            </div>
            <div>                
                
                <?php if (isset($_SESSION['user'])) { ?>                            
                    <a href="javascript:void(0);" id="logout" style="color: green;">Logout</a>                             
                <?php } else {
                    if(strtolower($_SERVER[REQUEST_URI]) != "/register.php" && strtolower($_SERVER[REQUEST_URI]) != "/my_login.php"){
                    ?>                              
                    <a href="javascript:void(0);" id="signup" style="color: green;">Create an Account</a>                      
                    <?php }
                    } ?>
            </div>
            <div>
            	<a href="tel:1-800-834-4253" class="toll-free-number"><i class="fa fa-phone-square"></i> 1-800-VEHICLES</a>
            </div> 
       </div>
            <a class="navbar-brand"    href="<?= WEB_SERVER_NAME ?>"><img  src="assets/img/logo/lgo.png" ></a>
            <div style="clear:both"></div>
        <!--<span class="home_number"> 
            <a href="tel:1-800-834-4253"> <img src="images/number.png"></a> </span></div>
        </div>-->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                 <!--<li class="dropdown" id="element1">                         
                    <a href="get_a_search_started.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">GET A SEARCH STARTED</a>                             
                </li>

                <li class="dropdown" id="element2">                         
                    <a href="dashboard.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">GET HELP WITH WHAT TO BUY</a>               
                </li>-->
                
                <!--<li class="dropdown">                         
                    <a href="franchise.php?page=how-it-works-for-customers" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">HOW IT WORKS</a>                             
                </li>-->
                
                <li class="dropdown">                         
                    <a href="add_vehicle_spec.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">START THE SEARCH PROCESS</a>                             
                </li>
                
                <li class="dropdown">                         
                    <a href="get_price_and_expert_advice.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">GET HELP WITH WHAT TO BUY</a>                             
                </li>

                <li class="dropdown">                         
                    <a href="#" class="dropdown-toggle abc" id="scroll-how-it-works">HOW IT WORKS</a>                             
                </li>                

               <li class="dropdown">                         
                    <a href="requesttradeinquote.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"> REQUEST TRADE IN QUOTE</a>                             
                </li>

                <li class="dropdown">                         
                    <a href="faq.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">FAQs</a>                             
                </li>

               <!-- <li class="dropdown">                         
                    <a href="researchvehicles.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">RESEARCH PRICES & AVAILABILITY</a>                             
                </li>-->
                <li class="dropdown">                         
                    <a href="consult.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">REQUEST CONSULTATION</a>                             
                </li>
                <li class="dropdown">                         
                    <a href="testimony.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">TESTIMONIALS</a>                             
                </li>
                <li class="dropdown">                         
                    <a href="franchise.php?page=what-is-1-800-vehicles-com" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">ABOUT US</a>                             
                </li>
                <li class="dropdown">                         
                    <a href="franchise.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">FRANCHISE OPPORTUNITIES</a>                             
                </li> 
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="search-wrapper">
        <button type="button" class="close">Ãƒâ€”</button>
        <form>
            <input type="search" value="" placeholder="type keyword(s) here" />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>
