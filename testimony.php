<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 2;
    $_SESSION['titleadd'] = 'Testimonials';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>

		<script src="assets/js/jquery.carouFredSel-6.1.0-packed.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {

				$('#carousel').carouFredSel({
					width: "100%",
					items: 1,
					scroll: 1,
					auto: {
						//duration: 2250,
						//timeoutDuration: 2500
						play:false
					},
					prev: '#prev',
					next: '#next',
					pagination: '#pager',
					responsive:true
				});
	
			});
		</script>
        <style type="text/css">
		
  #prev, #next {
				background: transparent url( assets/img/carousel_control.png ) no-repeat 0 0;
				text-indent: -999px;
				display: block;
				overflow: hidden;
				width: 15px;
				height: 21px;
				position: absolute;
				bottom: 5px;
			}
			#prev {
				background-position: 0 0;
				left: 40%;
			}
			#prev:hover {
				left: 39.5%;
			}			
			#next {
				background-position: -18px 0;
				right: 40%;
			}
			#next:hover {
				right: 39.5%;
			}				
			#pager {
				text-align: center;
				margin: 0 auto;
				padding-top: 20px;
			}
			#pager a {
				background: transparent url(assets/img/carousel_control.png) no-repeat -2px -32px;
				text-decoration: none;
				text-indent: -999px;
				display: inline-block;
				overflow: hidden;
				width: 8px;
				height: 8px;
				margin: 0 5px 0 0;
			}
			#pager a.selected {
				background: transparent url(assets/img/carousel_control.png) no-repeat -12px -32px;
				text-decoration: underline;				
			}  
			.testimonial-images {text-align: center;}
			.testimonial-images > img { width: 40%;}
			.slider-wrapper { background-color: #ffffff; padding-top: 30px; position: relative;}
			#content { margin: 40px 0 0;}
			
  </style>      
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Testimonials</h1>
        
        <div class="slider-wrapper">
           
			<div id="carousel">
				<div class="testimonial-images">
                 <img src="common/layout/testimonial-linda-stewart.jpg" /> 
                <p>
                    My husband wanted me to buy a Lexus, but I was uncomfortable with the high price tag. The dealer's price for
                    a 2006 model seemed way too high. My 1-800-vehicles.com dealer got me a 2007 model complete with All Wheel
                    Drive and half the mileage, for less than the dealer's price. Now I'm sold on Lexus and 1-800-vehicles.com!
                    - Linda.
                </p>
                </div>
				<div class="testimonial-images">
                 <img src="common/layout/testimonial-jenny-clough.jpg" />
                <p>
                    This is the only way to buy a car! The car is like brand new- a much better value than I would have ever
                    expected. Great, friendly and honest! - Jenny.
                </p>
                </div>
				<div class="testimonial-images">
                <img src="common/layout/testimonial-james-prewitt.jpg" />
				<p>
                    I hate shopping but this experience was great. I was able to get a fabulous car for a phenomenal price.
                    I am recommending 1-800-vehicles.com to all my friends and will use them to buy my wife a BMW! - James.
                </p>
                </div>
			</div>
			<a id="prev" href="#"></a>
			<a id="next" href="#"></a>
			<div id="pager"></div>
		</div>
        
        
        
        
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
