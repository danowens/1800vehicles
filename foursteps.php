<?php /* 
<div id="foursteps">
    <ul id="thesteps">
<?php
    require_once(WEB_ROOT_PATH."common/functions/usernavfunctions.php");

    echo '<li><a href="'.WEB_SERVER_NAME.'learntheprocess.php" class=';
    $current = currentstep();
    if(isset($_SESSION['userid']))
    {
        $userid = $_SESSION['userid'];
        $cust = getuserprofile($userid, 'Customer');
    }
    else $cust = 'false';
    if($cust == 'true')
    {
        if($current > 1) echo '"steponecheck"';
        elseif($current == 1) echo '"steponeselect"';
        else echo '"stepone"';
    }
    else echo '"stepone"';
    echo '>Step 1</a>';
?>
            <div class="sub">
                <ul>
<?php
                               echo '<li><a href="'.WEB_SERVER_NAME.'learntheprocess.php" id="desc_step1a_click">Learn The Process</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'researchvehicles.php" id="desc_step1b_click">Research Vehicles</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'assessment.php" id="desc_step1c_click">Take the Quick Assessment</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'consult.php" id="desc_step1d_click">Request Consultation</a></li>';
                                echo '<li><a href="'.WEB_SERVER_NAME.'requestquote.php" id="desc_step1e_click">Request a Current Market Study</a></li>';
?>
                        </ul>
                        <ul>
                               <li class="desc" id="desc_step1a" style="display:none">Learn how our revolutionary process will save you time, money and hassles.</li>
                               <li class="desc" id="desc_step1b" style="display:none">Get price ranges and basic information on specific vehicles. See a list of all 1 to 6 year old vehicles available within your budget, including some with &quot;Excellent Availability&quot;.</li>
                               <li class="desc" id="desc_step1c" style="display:none">Complete a short form describing your vehicle preferences.  This will help us make great recommendations to you.</li>
                               <li class="desc" id="desc_step1d" style="display:none">Get free consultation from a 1-800-vehicles.com representative who is a professional car buyer, not a pushy salesperson.</li>
                                <li class="desc" id="desc_step1e" style="display:none">Request a &quot;Current Market Study&quot; to get more specific pricing and availability based on your vehicle preferences. This does not obligate you in any way.</li>
                        </ul>
                </div>
        </li>
<?php
    echo '<li><a href="'.WEB_SERVER_NAME.'allfirmquotes.php" class=';
    if($cust == 'true')
    {
        if($current > 2) echo '"steptwocheck"';
        elseif($current == 2) echo '"steptwoselect"';
        else echo '"steptwo"';
    }
    else echo '"steptwo"';
    echo '>Step 2</a>';
?>
            <div class="sub">
                        <ul>
<?php
                            echo '<li><a href="'.WEB_SERVER_NAME.'allfirmquotes.php" id="desc_step2a_click">All Current Market Studies</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'requesttradeinquote.php" id="desc_step2b_click">Request Trade-In Quote</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'placeorder.php" id="desc_step2c_click">Place an Order</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'posttogroup.php" id="desc_step2d_click">Post to the Group Watchlist</a></li>';
?>
                        </ul>
                        <ul>
                                <li class="desc" id="desc_step2a" style="display:none">View all of your &quot;Current Market Studies&quot; in one place.</li>
                                <li class="desc" id="desc_step2b" style="display:none">Provide us information about your trade-in to see what it is worth.</li>
                                <li class="desc" id="desc_step2c" style="display:none">Select at least one "Current Market Study" to include and let your sales rep begin searching the national wholesale market for the perfect vehicle.</li>
                                <li class="desc" id="desc_step2d" style="display:none">When the vehicle you want is too scarce to accept as an order, you may post it here for all 1-800-vehicles.com dealers to see.</li>
                        </ul>
                </div>
        </li>
<?php
    echo '<li><a href="'.WEB_SERVER_NAME.'orderdetails.php" class=';
    if($cust == 'true')
    {
        if($current > 3) echo '"stepthreecheck"';
        elseif($current == 3) echo '"stepthreeselect"';
        else echo '"stepthree"';
    }
    else echo '"stepthree"';
    echo '>Step 3</a>';
?>
            <div class="sub">
                        <ul>
<?php
                                echo '<li><a href="'.WEB_SERVER_NAME.'orderdetails.php" id="desc_step3a_click">View My Order Details</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'watchlist.php" id="desc_step3b_click">View My Watchlist Details</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'svqlist.php" id="desc_step3c_click">All Specific Vehicle Quotes</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'searchrequest.php" id="desc_step3d_click">Request a Search Update</a></li>';
?>
                        </ul>
                        <ul>
                                <li class="desc" id="desc_step3a" style="display:none">View the details of your order including the date ordered, length of search period, and the &quot;Current Market Studies&quot; you have attached. </li>
                                <li class="desc" id="desc_step3b" style="display:none">View the details of your watchlist posting including the date posted and the "Current Market Studies" you have attached. </li>
                            <li class="desc" id="desc_step3c" style="display:none">View all quotes relating to specific vehicles that have been chosen for your consideration. </li>
                                <li class="desc" id="desc_step3d" style="display:none">Your sales rep is immediately notified that a search update has been requested.  He or she will respond with a quick update.</li>
                        </ul>
                </div>
        </li>
<?php
    echo '<li><a href="'.WEB_SERVER_NAME.'vehicle.php" class=';
    if($cust == 'true')
    {
        if($current > 4) echo '"stepfourcheck"';
        elseif($current == 4) echo '"stepfourselect"';
        else echo '"stepfour"';
    }
    else echo '"stepfour"';
    echo '>Step 4</a>';
?>
            <div class="sub">
                        <ul>
<?php
                                echo '<li><a href="'.WEB_SERVER_NAME.'vehicle.php" id="desc_step4a_click">Status of My Vehicle</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'vehicleactionplan.php" id="desc_step4b_click">Vehicle Action Plan</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'vehicledelivery.php" id="desc_step4c_click">Vehicle Moves</a></li>';
                            echo '<li><a href="'.WEB_SERVER_NAME.'progressrequest.php" id="desc_step4d_click">Request Progress Update</a></li>';
?>
                        </ul>
                        <ul>
                                <li class="desc" id="desc_step4a" style="display:none">View general information about the status of your vehicle including plan of action to make your car ready for delivery.</li>
                                <li class="desc" id="desc_step4b" style="display:none">View details about the steps of your vehicle will go through including parts ordered and appointments scheduled. </li>
                                <li class="desc" id="desc_step4c" style="display:none">View details about the moves your vehicle will go through in the current stage of the action plan including where it is in the delivery process. </li>
                                <li class="desc" id="desc_step4d" style="display:none">Your sales rep is immediately notified that a progress update has been requested.  He or she will respond with a quick update.</li>
                        </ul>
                </div>
        </li>
    </ul>
</div><!-- end foursteps-->
*/ ?>