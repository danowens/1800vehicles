<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = 1;
    $_SESSION['titleadd'] = "My Wholesale Inventory";

    $userid = $_SESSION['userid'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        if(isset($_POST['DeleteItem']) && isset($_POST['InStockID']) && isset($_POST['DelVehID']))
        {
            $diquery = "update instock set Available = 0 where instockid=".$_POST['InStockID'];
            if(mysql_query($diquery, $con))
            {
                $_SESSION['ShowError'] = 'Deletion Successful!';
            }
            else $_SESSION['ShowError'] = 'Deletion Failed!';
        }

        // Get the list franchise we are working with...
        $query = "select FranchiseeID from franchiseemembers where UserID=".$userid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $franid = $row[0];

            $query = "select i.InStockID, i.PricePaid, i.PriceWanted, v.VIN, v.Year, v.Model, m.Name, v.Mileage, v.VehicleDetailID from instock i,vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=i.VehicleDetailID and i.Available=1 and i.FranchiseeID=".$franid." order by 1";
            $result = mysql_query($query);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $svstockID[$index] = $row[0];
                $svprice[$index] = $row[1];
                $svwanted[$index] = $row[2];
                $svvin[$index] = $row[3];
                $svyear[$index] = $row[4];
                $svmodel[$index] = $row[5];
                $svmake[$index] = $row[6];
                $svmiles[$index] = $row[7];
                $svvehid[$index] = $row[8];
                $index++;
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function deleteok()
    {
        var r=confirm("Are you sure you want to Delete the Vehicle?")
        if(r==true) return true;
        else return false;
    }
</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width:275px;">My Wholesale Inventory</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: 0px;">
                <p class="blackeleven" style="margin: 0;"><a href="mydashboard.php">Go to MyDashboard</a></p>
<?php
    $count = count($svstockID);
    if($count > 0)
    {
?>
                <form action="mywholesale.php" method="post">
                    <input type="hidden" value="true" name="PostBack" />
                    <table border="0" width="300" cellspacing="10" style="margin-left:-10px;">
                        <tr valign="baseline">
                            <td width="15%" align="left"><label for="yearlist"><strong>Year</strong></label></td>
                            <td width="35%" align="left" colspan="3">
                                <select name="yearlist" id="yearlist">
                                    <option value="">All</option>
                                </select>
                            </td>
                            <!--td width="15%" align="left">&nbsp;</td>
                            <td width="35%" align="left">&nbsp;</td-->
                        </tr>
                        <tr valign="baseline">
                            <td align="left"><label for="makelist"><strong>Make</strong></label></td>
                            <td align="left" colspan="3">
                                <select name="makelist" id="makelist">
                                    <option value="">All</option>
                                </select>
                            </td>
                            <!--td align="left">&nbsp;</td>
                            <td align="left">&nbsp;</td-->
                        </tr>
                        <tr valign="baseline">
                            <td align="left"><label for="modellist"><strong>Model</strong></label></td>
                            <td align="left" colspan="3">
                                <select name="modellist" id="modellist">
                                    <option value="">All</option>
                                </select>
                            </td>
                            <!--td align="left">&nbsp;</td>
                            <td align="left">&nbsp;</td-->
                        </tr>
                        <tr valign="baseline">
                            <td align="left"><label for="milestart"><strong>Mileage</strong></label></td>
                            <td align="left">
                                <select name="milestart" id="milestart">
                                    <option value="">0</option>
                                </select>
                            </td>
                            <td align="left"><label for="mileend"><strong>To</strong></label></td>
                            <td align="left">
                                <select name="mileend" id="mileend">
                                    <option value="">0</option>
                                </select>
                            </td>
                        </tr>
                        <tr valign="baseline">
                            <td align="left"><label for="pricestart"><strong>Price</strong></label></td>
                            <td align="left">
                                <select name="pricestart" id="pricestart">
                                    <option value="">Any</option>
                                </select>
                            </td>
                            <td align="left"><label for="priceend"><strong>To</strong></label></td>
                            <td align="left">
                                <select name="priceend" id="priceend">
                                    <option value="">Any</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <button type="submit" value="" class="med">SEARCH CURRENT INVENTORY</button>
                    <br/>
                    <br/>
                </form>
<?php
    }
?>
                <form action="addinventory.php" method="post">
                    <input type="hidden" value="<?php echo $franid;?>" name="FranchiseeID" />
                    <input type="hidden" value="true" name="WholeSale" />
                    <button type="submit" value="" class="med">ADD TO INVENTORY</button>
                </form>
            </div>    <!-- endgrideight -->
        </div>  <!-- endgrideightgrey -->
<?php
    if(isset($_POST['PostBack']))
    {
        echo '<h1 class="subhead" style="width:125px;">Results</h1>';
        echo '<div class="grideightgrey">';
        echo '<div class="grideight" style="margin-top: 0px; width:580px;">';
        if($count < 1)
        {
            echo '<p class="blackeleven" style="margin: 0; float:right;">No results match the current criteria</p>';
        }
        else
        {
            echo '<table width="561" border="0" cellspacing="10" style="font-size:13px; color:#3366cc;">';
            echo '<tr style="font-size:15px;">';
            echo '<td width="200" align="center" style="color:#85c11b;"><strong>YEAR/MAKE/MODEL</strong></td>';
            echo '<td width="91" align="center" style="color:#85c11b;"><strong>MILEAGE</strong></td>';
            echo '<td width="162" align="center" style="color:#85c11b;"><strong>ASKING PRICE</strong></td>';
            echo '<td width="17" align="center">&nbsp;</td>';
            echo '<td width="21" align="center">&nbsp;</td>';
            echo '</tr>';
            for($i=0;$i<$count;$i++)
            {
                echo '<tr valign="baseline">';
                echo '<td><strong>';
                //echo '<a href="#">';
                echo $svyear[$i].' '.$svmake[$i].' '.$svmodel[$i];
                //echo '</a>';
                echo '</strong></td>';
                echo '<td align="center"><strong>';
                echo number_format($svmiles[$i]);
                echo '</strong></td>';
                echo '<td align="center"><strong>$';
                echo number_format($svwanted[$i]);
                if(!is_null($svprice[$i])) echo ' [Paid: $'.number_format($svprice[$i]).']';
                echo '</strong></td>';
                echo '<td align="center">';
                echo '<form action="addinventory.php" method="post">';
                echo '<input type="hidden" value="true" name="WholeSale" />';
                echo '<input type="hidden" value="'.$franid.'" name="FranchiseeID" />';
                echo '<input type="hidden" value="'.$svstockID[$i].'" name="InStockID" />';
                echo '<input type="hidden" value="'.$svvehid[$i].'" name="EditVehID" />';
                echo '<button type="submit" value="" class="blueongrey"><img alt="Edit" title="Edit Inventory Item" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>';
                echo '</form>';
                echo '</td>';
                echo '<td align="center">';
                echo '<form action="mywholesale.php" method="post" onsubmit="javascript:return deleteok();" >';
                echo '<input type="hidden" value="'.$svstockID[$i].'" name="InStockID" />';
                echo '<input type="hidden" value="'.$svvehid[$i].'" name="DelVehID" />';
                echo '<input type="hidden" value="true" name="DeleteItem" />';
                echo '<input type="hidden" value="true" name="PostBack" />';
                echo '<button type="submit" value="" class="blueongrey"><img alt="Delete" title="Remove Inventory from Franchise" width="20" height="20" border="0" src="common/layout/close.gif" /></button>';
                echo '</form>';
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
        echo '<br/>';
        echo '<form action="addinventory.php" method="post">';
        echo '<input type="hidden" value="'.$franid.'" name="FranchiseeID" />';
        echo '<input type="hidden" value="true" name="WholeSale" />';
        echo '<button type="submit" value="" class="med">ADD TO INVENTORY</button>';
        echo '</form>';
        echo '<br/><br/><p class="blackeleven" style="margin: 0; float:right;"><a href="mydashboard.php">Go to MyDashboard</a></p></div><!-- endgrideight --></div> <!-- endgrideightgrey -->';
    }
?>
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
