<?php require("globals.php"); ?>
<?php
require("common/functions/User.php");
require("common/functions/DB.php");
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 5;
    $_SESSION['titleadd'] = 'Lost Password';

    $loginname = $_REQUEST['loginname'];
    $db = DB::init();
    $user = User::getUserByLogin($db, $loginname);
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <form action="forgotpass3.php" onsubmit="return validateFormOnSubmit(this)" method="post">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 260px;">Forgot your password?</h1>
        <div class="grideightgrey">
            <div class="grideight" style="width:580px;">
<?php if (!empty($user) && isset(User::$SECURITY_QUESTIONS[$user['PWQuestion1']])) { ?>
            <p class="blackfourteen" style="margin-top:0; font-size: 13px; color:#142c3c;"><strong><?php echo htmlspecialchars($user['FirstName']); ?>, please answer the question you selected:</strong></p>
            <label for="answer" class="blackfourteen" style="font-size: 13px; color:#142c3c;"><strong><?php echo htmlspecialchars(User::$SECURITY_QUESTIONS[$user['PWQuestion1']]); ?></strong></label>
                <input name="answer" id="answer" type="text" size="30" style="margin-left:10px;" />
                <input name="loginname" type="hidden" size="30" value="<?php echo htmlspecialchars($loginname); ?>" />
                <br />
                <br />
                <button type="submit" value="" class="med"><nobr>CONTINUE</nobr></button>
<?php } else { ?>
<p class="blackfourteen" style="margin-top:0;"><strong>The login (or email) you entered was not found or you did not set a password question to answer.</strong></p><p class="blackfourteen" style="margin-top:0;"><strong>Contact your sales representative for more help or <a href="forgotpass1.php">return and try again</a></strong></p>
<?php } ?>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
