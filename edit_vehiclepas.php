<?php require("globals.php"); ?> 
<?php
require_once(WEB_ROOT_PATH . 'common/functions/string.php');
//error_reporting(E_ALL);
//	ini_set("display_errors", "on");
$_SESSION['state'] = 5;
$_SESSION['substate'] = 9;
$_SESSION['titleadd'] = 'Edit Price & Availability Study';

//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";

$userid = $_SESSION['userid'];
if (!isset($userid)) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - UserID not set';
    header('Location: mydashboard.php#admintab');
    exit();
}
        $inuserid = $_POST['ForUserID'];
        $inmneed = $_POST['MarketNeedID'];
        $SearchPlanDetailID = $_POST['SearchPlanDetailID'];
	$total			= 0;
	
	$con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con){
        mysql_select_db(DB_SERVER_DATABASE, $con);
			$vspquery	= "select * from pasplandetails where SearchPlanDetailID = ".$SearchPlanDetailID;
			$vspresult = mysql_query($vspquery, $con);
			
			$vspdata = array();
			
			while($vsprow = mysql_fetch_array($vspresult))
			{
				$total = $total+1;
				$vspdata[$total] = $vsprow;
			}
                        
                        
            $query = "select FirstName, LastName, Email, Note from users where userid=" . $_SESSION['userid'];
            $result = mysql_query($query, $con);
            if ($result && $row = mysql_fetch_array($result)) {
                $ifname = $row[0];
                $ilname = $row[1];
            }
            
         $assessments_query = "SELECT BudgetFrom,BudgetTo FROM  `assessments` WHERE  `MarketNeedID` =".$inmneed;
        
              
         $assessments_result = mysql_query($assessments_query, $con);
            if ($assessments_result && $row = mysql_fetch_array($assessments_result)) { 
                $ass_BudgetFrom = $row[0];
                $ass_BudgetTo = $row[1];
            }
		
		mysql_close($con);
	}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php // require("headerend.php"); ?>
<script type="text/javascript">



 $(document).ready(function() {
        $("input:text").click(function() {
            var element = $(this);
            $("#notetext").val(element.val());
            $("#dialog").dialog({
                modal: true,
                title: " ",
                close: function(event, ui) {
                    element.val($("#notetext").val());
                    $("#dialog").dialog("destroy");
                }
            });
        });
    });
	
	function yearchanged(id){
		var year = $("#yearlist"+id).val();
		$.ajax({
			type: "POST",
			url: 'ajaxallmakes.php',
			data: { year:year },
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#makelist"+id).html('');
				$("#makelist"+id).append(data);
				
				makechanged(id);
			}
		});
	}
	
	function makechanged(id){
		var year = $("#yearlist"+id).val();
		var make = $("#makelist"+id).val();
		$.ajax({
			type: "POST",
			url: 'ajaxallmodels.php',
			data: { year:year, make:make},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#modellist"+id).html('');
				$("#modellist"+id).append(data);
				
				modelchanged(id);
			}
		});
	}
	
	function modelchanged(id){
		var year	= $("#yearlist"+id).val();
		var make	= $("#makelist"+id).val();
		var model	= $("#modellist"+id).val();
		
		$.ajax({
			type: "POST",
			url: 'ajaxallstyles.php',
			data: { year:year, make:make, model:model},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#stylelist"+id).html('');
				$("#stylelist"+id).append(data);
				
				stylechanged(id);
			}
		});
	}
	
	function stylechanged(id){
		var style = $("#stylelist"+id).val().split(";");
		$.ajax({
			type: "POST",
			url: 'ajaxcheckvehicletype.php',
			data: { id:style[0]},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#vehicletype"+id).val(data);
				if(data=='Auto'){
					$("#drivetrain"+id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).hide();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).hide();
					$("#trbackupcamera"+id).hide();
					$("#trtpackage"+id).hide();
					
				}else if(data=='Minivan' || data=='MiniVan'){
					$("#drivetrain"+id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).show();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).show();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).hide();

				}else if(data=='SUV'){
					$("#drivetrain"+id).html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
				
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).show();
					$("#trthirdrs"+id).show();
					$("#trcrow"+id).show();
					$("#trprhatch"+id).show();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).show();

				}else if(data=='Pickup'){
					$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					//@TODO make function with 2 parameter (Array of ids and event)
					$("#trfrontstype"+id).show();
					$("#trbedtype"+id).show();
					$("#trrearwindow"+id).show();
					$("#trbedliner"+id).show();
					$("#trentertainmentsystem"+id).hide();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).hide();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).show();
				}
			}
		});
	}
	
	function specific(id, val){
		$("#specific"+id).val(val);
		if(val==1){
			$("#specificsection"+id).hide();
			$("#selectyear"+id).hide();
			$("#selectmake"+id).hide();
			$("#selectmodel"+id).hide();
			$("#selectstyle"+id).hide();
			
			$("#vehiclesection"+id).show();
			$("#textyear"+id).show();
			$("#textmake"+id).show();
			$("#textmodel"+id).show();
			$("#textstyle"+id).show();
		}else{
			$("#specificsection"+id).show();
			$("#selectyear"+id).show();
			$("#selectmake"+id).show();
			$("#selectmodel"+id).show();
			$("#selectstyle"+id).show();
			
			$("#vehiclesection"+id).hide();
			$("#textyear"+id).hide();
			$("#textmake"+id).hide();
			$("#textmodel"+id).hide();
			$("#textstyle"+id).hide();
		}
	}

</script>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;"> Price & Availability Study</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: -5px;">
   <form method="post" action="/salesrepactions.php">
        <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
        <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
        <button class="blueongrey_dash" value="" style="font-size: 11px;" type="submit">
            Go back to Customer Review
        </button>
    </form>
               <div id="dialog" title=" " style="display: none;">
    <textarea name="notetext" id="notetext" style="resize:vertical;width:100%;height:100%"></textarea>
</div> 
                
                <form action="save_sales_pas.php" onsubmit="javascript:return validateFormOnSubmit()" method="post" name="assessform">
			
				
				<?php
					//$vspdata['Year']
					for($current = 1; $current <= $total; $current++){
						$recon = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
						if($recon){
							mysql_select_db(DB_SERVER_DATABASE, $recon);
							
							//Get all vehicle years
							$yquery = "select distinct v.Year from vehicles v order by v.Year desc";
							$yresult = mysql_query($yquery, $recon);
							$index = 0;
							$allyears = array();
							
							while($yrow = mysql_fetch_array($yresult))
							{
								$allyears[$index] = $yrow[0];
								$index++;
							}
							
							//Get all vehicle makes
							$mquery = "select distinct m.MakeID, m.Name from vehicles v, makes m where m.MakeID = v.MakeID and v.Year = '".$vspdata[$current]['Year']."' order by 2";
							$result = mysql_query($mquery, $recon);
							
							$allmakes = array();
							while($result && $mrow = mysql_fetch_array($result))
							{
								$allmakes[] = $mrow;
							}
							
							//Get all vehicle model
							$modquery = "select distinct v.Model from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$vspdata[$current]['Make']." and v.Year = '".$vspdata[$current]['Year']."' order by 1";
							$result = mysql_query($modquery, $recon);
							
							$allmodels = array();
							while($result && $row = mysql_fetch_array($result))
							{
								$allmodels[] = $row[0];
							}
							
							//Get all vehicle Style
							$styquery = "select distinct v.Style, v.VehicleID from vehicles v, makes m where m.MakeID = v.MakeID and v.MakeID = ".$vspdata[$current]['Make']." and v.Year = '".$vspdata[$current]['Year']."' and v.Model = '".$vspdata[$current]['Model']."' order by 1";
							$result = mysql_query($styquery, $recon);
							
							$allstyles = array();
							$k = 0;
							while($result && $strow = mysql_fetch_array($result))
							{
								if($k==0){
									$vehicleid = $strow['VehicleID'];
								}else{
									$k = 1;
								}
								$allstyles[] = $strow;
							}
							
							//Update type of the vehicle based on Vehicle ID
							$query = "select Type from vehicles where VehicleID = '".$vehicleid."'";
							$result = mysql_query($query, $recon);
							while($result && $row = mysql_fetch_array($result)){
								$vehicletype = $row['Type'];
							}
                                                        

							
							$specific				= $vspdata[$current]['Specific'];
							$drivetrain				= $vspdata[$current]['DriveTrain'];
							$transmission			= $vspdata[$current]['Transmission'];
							$mileagefrom			= $vspdata[$current]['MileageFrom'];
							$mileageto				= $vspdata[$current]['MileageTo'];
							$mileageceiling			= $vspdata[$current]['MileageCeiling'];
							$extlike				= $vspdata[$current]['ExtLike'];
							$extdislike				= $vspdata[$current]['ExtDislike'];
							$intlike				= $vspdata[$current]['IntLike'];
							$intdislike				= $vspdata[$current]['IntDisike'];
							$budgetfrom				= $vspdata[$current]['BudgetFrom'];
							$budgetto				= $vspdata[$current]['BudgetTo'];
							$borrowmaxpayment		= $vspdata[$current]['BorrowMaxPayment'];
							$borrowdownpayment		= $vspdata[$current]['BorrowDownPayment'];
							$vehicleneed			= $vspdata[$current]['VehicleNeed'];

							//What do you prefer start
							$frontstype				= $vspdata[$current]['FrontSType'];
							$bedtype				= $vspdata[$current]['BedType'];
							$leather				= $vspdata[$current]['Leather'];
							$heatedseat				= $vspdata[$current]['HeatedSeat'];
							$navigation				= $vspdata[$current]['Navigation'];
							$sunroof				= $vspdata[$current]['SunRoof'];
							$alloywheels			= $vspdata[$current]['AlloyWheels'];
							$rearwindow				= $vspdata[$current]['RearWindow'];
							$bedliner				= $vspdata[$current]['BedLiner'];
							$entertainmentsystem	= $vspdata[$current]['EntertainmentSystem'];
							$thirdrs				= $vspdata[$current]['ThirdRD'];
							$crow					= $vspdata[$current]['CRow'];
							$prhatch				= $vspdata[$current]['PRHatch'];
							$backupcamera			= $vspdata[$current]['BackupCamera'];
							$tpackage				= $vspdata[$current]['TPackage'];
                                                        $plan_rating				= $vspdata[$current]['plan_rating'];                                                        
                                                        $SearchPlanDetailID=  $vspdata[$current]['SearchPlanDetailID'];
                                                         $SearchPlanID=  $vspdata[$current]['SearchPlanID'];
							//What do you prefer end
							
						
				?>	
							
								
								<input type="hidden" id="vehicletype" name="vehicletype" value="<?php echo $vehicletype;?>" />
                                                                <input type="hidden" id="ForUserID" name="ForUserID" value="<?php echo $inuserid;?>" />
                                                                <input type="hidden" id="MarketNeedID" name="MarketNeedID" value="<?php echo $inmneed;?>" />
                                                               
                                                                
                                                                
								<input type="hidden" id="spdetailid" name="spdetailid" value="<?php echo $vspdata[$current]['SearchPlanDetailID'];?>" />
								<input type="hidden" id="specific" name="specific" value="<?php echo $specific;?>" />
								
									<table class="table"  style="margin-top: 25px;">
										
                                                                            <tr >
											<td style="width: 40%;"><strong>Seller Name</strong></td>
											<td style="width: 60%;">
                                                                                            <strong><?=$ifname . ' ' . $ilname;?></strong>
											</td>
										</tr>
                                                                            
                                                                            <tr id="selectyear" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td ><strong>Year</strong></td>
											<td >
												<select id="yearlist" name="yearlist" onchange="yearchanged('')">
													<?php
														$count = count($allyears);
														for($i = 0; $i < $count; $i++){
													?>
															<option value="<?php echo $allyears[$i]?>" <?php if($vspdata[$current]['Year'] == $allyears[$i]){?> selected="selected"<?php }?>><?php echo $allyears[$i];?></option>
													<?php
														}
													?>
												</select>
											</td>
										</tr>
										<tr id="textyear" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Year</strong></td>
											<td >
												<input type="text" name="yeartext" id="yeartext" value="<?php echo $vspdata[$current]['TextYear'];?>" />
											</td>
										</tr>
										<tr id="selectmake" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td><strong>Make</strong></td>
											<td >
												<select id="makelist" name="makelist" onchange="makechanged('')">
													<?php
														$count = count($allmakes);
														for($i = 0; $i < $count; $i++){
													?>
															<option value="<?php echo $allmakes[$i]['MakeID']?>" <?php if($vspdata[$current]['Make'] == $allmakes[$i]['MakeID']){?> selected="selected"<?php }?>><?php echo $allmakes[$i]['Name'];?></option>
													<?php
														}
													?>
												</select>
											</td> 
										</tr>
										<tr id="textmake" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Make</strong></td>
											<td >
												<input type="text" name="maketext" id="maketext" value="<?php echo $vspdata[$current]['TextMake'];?>" />
											</td>
										</tr>
										<tr id="selectmodel" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td><strong>Model</strong></td>
											<td >
												<select id="modellist" name="modellist" onchange="modelchanged('')">
													<?php
														$count = count($allmodels);
														for($i = 0; $i < $count; $i++){
													?>
															<option value="<?php echo $allmodels[$i]?>" <?php if($vspdata[$current]['Model'] == $allmodels[$i]){?> selected="selected"<?php }?>><?php echo $allmodels[$i];?></option>
													<?php
														}
													?>
												</select>
											</td>
										</tr>
										<tr id="textmodel" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Model</strong></td>
											<td >
												<input type="text" name="modeltext" id="modeltext" value="<?php echo $vspdata[$current]['TextModel'];?>" />
											</td>
										</tr>
										<tr id="selectstyle" <?php if($specific == 1){ ?>style="display:none;"<?php }?>>
											<td><strong>Style</strong></td>
											<td >
												<select id="stylelist" name="stylelist" onchange="stylechanged('')">
													<?php
														$count = count($allstyles);
														for($i = 0; $i < $count; $i++){
													?>
															<option value="<?php echo $allstyles[$i]['VehicleID'].';'.$allstyles[$i]['Style'];?>" <?php if($vspdata[$current]['Style'] == $allstyles[$i]['VehicleID'].';'.$allstyles[$i]['Style']){?> selected="selected"<?php }?>><?php echo $allstyles[$i]['Style'];?></option>
													<?php
														}
													?>
												</select>
											</td>
										</tr>
										<tr id="textstyle" <?php if($specific == 0){ ?>style="display:none;"<?php }?>>
											<td ><strong>Style</strong></td>
											<td >
												<input type="text" name="styletext" id="styletext" value="<?php echo $vspdata[$current]['TextStyle'];?>" />
											</td>
										</tr>
										<tr><td colspan="2">&nbsp;</td></tr>										
										<tr><td colspan="2">&nbsp;</td></tr>
										<tr>
											<td><strong>Other styles (or packages) you will consider</strong></td>
											<td >
												<input type="text" name="otherstyle" id="otherstyle" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['OtherStyle'];?>">
											</td>
										</tr>
										<tr>
											<td><strong>Other year models you will consider:</strong></td>
											<td >
												<input type="text" name="otheryear" id="otheryear" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['OtherYear'];?>">
											</td>
										</tr>
										<tr>
											<td><strong>How soon do you need a vehicle?</strong></td>
											<td >
												<select name="vehicleneed" id="vehicleneed">
													<option value="Now" <?php if($vehicleneed == 'Now') {?> selected="selected" <?php }?>>Now!</option>
													<option value="1-2 weeks" <?php if($vehicleneed == '1-2 weeks') {?> selected="selected" <?php }?>>1-2 weeks</option>
													<option value="3 weeks is ok" <?php if($vehicleneed == '3 weeks is ok') {?> selected="selected" <?php }?>>3 weeks is ok</option>
													<option value="A month or more is ok" <?php if($vehicleneed == 'A month or more is ok') {?> selected="selected" <?php }?>>A month or more is ok</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="2">&nbsp;</td></tr>
										<tr><td colspan="2"><strong>Mileage:</strong></td></tr>
										<tr>
											<td><strong>Mileage you are hoping for</strong></td>
                                                                                        <td >
												<select name="mileagefrom" id="mileagefrom" class="width103">
													<option value="Flexible"<?php if($mileagefrom=='Small'){?> selected="selected" <?php }?>>Flexible</option>
													<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
														<option value="<?php echo $i;?>" <?php if($i==$mileagefrom){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
                                                                                                </select> &nbsp;&nbsp;TO&nbsp;&nbsp;
                                                                                                <select name="mileageto" id="mileageto" class="width100">
													<option value="Flexible"<?php if($mileageto=='Small'){?> selected="selected" <?php }?>>Flexible</option>
													<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
														<option value="<?php echo $i;?>" <?php if($i== $mileageto){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
												</select>
											</td>
											
										</tr>
										<tr>
											<td><strong>Mileage Ceiling</strong></td>
											<td >
												<select name="mileageceiling" id="mileageceiling">
													<option value="Flexible"<?php if($mileageceiling=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
													<?php for($i=10000; $i <= 200000; $i = $i+5000){?>
														<option value="<?php echo $i;?>" <?php if($i==$mileageceiling){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Transmission</strong></td>
											<td >
												<select name="transmission" id="transmission">
													<option value="Automatic"<?php if($transmission=='Automatic'){?> selected="selected" <?php }?>>Automatic</option>
													<option value="Manual"<?php if($transmission=='Manual'){?> selected="selected" <?php }?>>Manual</option>
													<option value="Flexible"<?php if($transmission=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Drive Train</strong></td>
											<td >
												<select name="drivetrain" id="drivetrain">
												<?php if($vehicletype=='Auto'){?>
													<option value="Front Wheel Drive"<?php if($drivetrain=='Front Wheel Drive'){?> selected="selected" <?php }?>>Front Wheel Drive</option>
													<option value="Rear Wheel Drive"<?php if($drivetrain=='Rear Wheel Drive'){?> selected="selected" <?php }?>>Rear Wheel Drive</option>
													<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
												<?php }elseif($vehicletype=='Minivan' || $vehicletype=='MiniVan'){?>
													<option value="Front Wheel Drive"<?php if($drivetrain=='Front Wheel Drive'){?> selected="selected" <?php }?>>Front Wheel Drive</option>
													<option value="4wd"<?php if($drivetrain=='4wd'){?> selected="selected" <?php }?>>4wd</option>
													<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
												<?php }elseif($vehicletype=='SUV' || $vehicletype == 'Pickup'){?>
													<option value="2wd"<?php if($drivetrain=='2wd'){?> selected="selected" <?php }?>>2wd</option>
													<option value="4wd"<?php if($drivetrain=='4wd'){?> selected="selected" <?php }?>>4wd</option>
													<option value="Flexible"<?php if($drivetrain=='Flexible'){?> selected="selected" <?php }?>>Flexible</option>
												<?php }?>
												</select>
											</td>
										</tr>
										<tr><td colspan="2">&nbsp;</td></tr>
										<tr><td colspan="2"><strong>Exterior Color Preferences:</strong></td></tr>
										<tr>
											<td><strong>Colors you like</strong></td>
											<td >
												<input type="text" name="extlike" id="extlike" value="<?php echo $extlike?>" placeholder="Text box">
											</td>
										</tr>
										<tr>
											<td><strong>Colors you dislike</strong></td>
											<td >
												<input type="text" name="extdislike" id="extdislike" value="<?php echo $extdislike?>" placeholder="Text box">
											</td>
										</tr>
										<tr><td colspan="2">&nbsp;</td></tr>
										<tr><td colspan="2"><strong>Interior Color Preferences:</strong></td></tr>
										<tr>
											<td><strong>Colors you like</strong></td>
											<td >
												<input type="text" name="intlike" id="intlike" value="<?php echo $intlike?>" placeholder="Text box">
											</td>
										</tr>
										<tr>
											<td><strong>Colors you dislike</strong></td>
											<td >
												<input type="text" name="intdislike" id="intdislike" value="<?php echo $intdislike?>" placeholder="Text box">
											</td>
										</tr>
										<tr><td colspan="2">&nbsp;</td></tr>	
                                                                                
                                                                               
                                                                                
										
										<tr><td colspan="2"><strong>If Borrowing:</strong></td></tr>
										<tr>
											<td><strong>Maximum payment is</strong></td>
											<td >$
												<select name="borrowmaxpayment" id="borrowmaxpayment" >
													<option value="Not Applicable" <?php if($borrowmaxpayment == 'Not Applicable') {?> selected="selected" <?php }?>>Not Applicable</option>
													<option value="Not Borrowing" <?php if($borrowmaxpayment == 'Not Borrowing') {?> selected="selected" <?php }?>>Not Borrowing</option>
													<option value="Not Sure"<?php if($borrowmaxpayment == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
													<?php for($i=100; $i <= 2000; $i=$i+10){?>
														<option value="<?php echo $i;?>" <?php if($i==$borrowmaxpayment){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Down payment is</strong></td>
											<td >$
												<select name="borrowdownpayment" id="borrowdownpayment" >
													<option value="Not Applicable" <?php if($borrowdownpayment == 'Not Applicable') {?> selected="selected" <?php }?>>Not Applicable</option>
													<option value="Not Borrowing" <?php if($borrowdownpayment == 'Not Borrowing') {?> selected="selected" <?php }?>>Not Borrowing</option>
													<option value="Not Sure" <?php if($borrowdownpayment == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
													<?php for($i=100; $i <= 2000; $i=$i+10){?>
														<option value="<?php echo $i;?>" <?php if($i==$borrowdownpayment){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
												</select>
											</td>
										</tr>						
										<tr><td colspan="2">&nbsp;</td></tr>
										<tr><td colspan="2"><strong>Do you prefer:</strong></td></tr>
										<tr id="trfrontstype" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
											<td><strong>Front Seat Type</strong></td>
											<td >
												<select name="frontstype" id="frontstype">
													<option value="Standard Bench" <?php if($frontstype == 'Standard Bench') {?> selected="selected" <?php }?>>Standard Bench</option>
													<option value="Bench Warmrest" <?php if($frontstype == 'Bench Warmrest') {?> selected="selected" <?php }?>>Bench Warmrest</option>
													<option value="Bucket Seats" <?php if($frontstype == 'Bucket Seats') {?> selected="selected" <?php }?>>Bucket Seats</option>
													<option value="Flexible" <?php if($frontstype == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trbedtype" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
											<td><strong>Bed Type</strong></td>
											<td >
												<select name="bedtype" id="bedtype">
													<option value="Short Bed (about 6 ft.)" <?php if($bedtype == 'Short Bed (about 6 ft.)') {?> selected="selected" <?php }?>>Short Bed (about 6 ft.)</option>
													<option value="Long Bed (about 8 ft.)" <?php if($bedtype == 'Long Bed (about 8 ft.)') {?> selected="selected" <?php }?>>Long Bed (about 8 ft.)</option>
													<option value="Extra Long Bed (about 10 ft.)" <?php if($bedtype == 'Extra Long Bed (about 10 ft.)') {?> selected="selected" <?php }?>>Extra Long Bed (about 10 ft.)</option>
													<option value="Mini Bed (about 4 ft.)" <?php if($bedtype == 'Mini Bed (about 4 ft.)') {?> selected="selected" <?php }?>>Mini Bed (about 4 ft.)</option>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Leather</strong></td>
											<td >
												<select name="leather" id="leather">
													<option value="Yes" <?php if($leather == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($leather == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($leather == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($leather == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Heated Seats</strong></td>
											<td >
												<select name="heatedseat" id="heatedseat">
													<option value="Yes" <?php if($heatedseat == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($heatedseat == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($heatedseat == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($heatedseat == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Navigation</strong></td>
											<td >
												<select name="navigation" id="navigation">
													<option value="Yes" <?php if($navigation == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($navigation == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($navigation == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($navigation == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Sunroof</strong></td>
											<td >
												<select name="sunroof" id="sunroof">
													<option value="Yes" <?php if($sunroof == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($sunroof == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($sunroof == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($sunroof == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr>
											<td><strong>Alloy Wheels</strong></td>
											<td >
												<select name="alloywheels" id="alloywheels">
													<option value="Yes" <?php if($alloywheels == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($alloywheels == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($alloywheels == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($alloywheels == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trrearwindow" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
											<td><strong>Rear Sliding Window</strong></td>
											<td >
												<select name="rearwindow" id="rearwindow">
													<option value="Yes" <?php if($rearwindow == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($rearwindow == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($rearwindow == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($rearwindow == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trbedliner" <?php if($vehicletype != 'Pickup'){ ?> style="display:none;" <?php }?>>
											<td><strong>Bed Liner</strong></td>
											<td >
												<select name="bedliner" id="bedliner">
													<option value="Yes" <?php if($bedliner == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($bedliner == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($bedliner == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($bedliner == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trentertainmentsystem" <?php if($vehicletype == 'Pickup' || $vehicletype == 'Auto'){ ?> style="display:none;" <?php }?>>
											<td><strong>Entertainment System</strong></td>
											<td >
												<select name="entertainmentsystem" id="entertainmentsystem">
													<option value="Yes" <?php if($entertainmentsystem == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($entertainmentsystem == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($entertainmentsystem == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($entertainmentsystem == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trthirdrs" <?php if($vehicletype != 'SUV'){ ?> style="display:none;" <?php }?>>
											<td><strong>Third Row Seating</strong></td>
											<td >
												<select name="thirdrs" id="thirdrs">
													<option value="Yes" <?php if($thirdrs == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($thirdrs == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($thirdrs == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($thirdrs == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trcrow" <?php if($vehicletype != 'SUV'){ ?> style="display:none;" <?php }?>>
											<td><strong>Captain chairs center row</strong></td>
											<td >
												<select name="crow" id="crow">
													<option value="Yes" <?php if($crow == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($crow == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($crow == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($crow == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trprhatch" <?php if($vehicletype == 'Pickup' || $vehicletype == 'Auto'){ ?> style="display:none;" <?php }?>>
											<td><strong>Power Rear Hatch</strong></td>
											<td >
												<select name="prhatch" id="prhatch">
													<option value="Yes" <?php if($prhatch == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($prhatch == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($prhatch == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($prhatch == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trbackupcamera" <?php if($vehicletype != 'Auto'){ ?> style="display:none;" <?php }?>>
											<td><strong>Backup Camera</strong></td>
											<td >
												<select name="backupcamera" id="backupcamera">
													<option value="Yes" <?php if($backupcamera == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($backupcamera == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($backupcamera == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($backupcamera == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr id="trtpackage" <?php if($vehicletype == 'Minivan' || $vehicletype == 'MiniVan' || $vehicletype == 'Auto'){ ?> style="display:none;" <?php }?>>
											<td><strong>Tow package</strong></td>
											<td >
												<select name="tpackage" id="tpackage">
													<option value="Yes" <?php if($tpackage == 'Yes') {?> selected="selected" <?php }?>>Yes</option>
													<option value="No" <?php if($tpackage == 'No') {?> selected="selected" <?php }?>>No</option>
													<option value="Would really like to have" <?php if($tpackage == 'Would really like to have') {?> selected="selected" <?php }?>>Would really like to have</option>
													<option value="Flexible" <?php if($tpackage == 'Flexible') {?> selected="selected" <?php }?>>Flexible</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="2">&nbsp;</td></tr>
										<tr>
											<td><strong>Must Have</strong></td>
											<td >
												<input type="text" name="musthave" id="musthave" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['MustHave'];?>">
											</td>
										</tr>
										<tr>
											<td><strong>Would really like to have</strong></td>
											<td >
												<input type="text" name="reallyhave" id="reallyhave" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['ReallyHave'];?>">
											</td>
										</tr>
										<tr>
											<td><strong>Flexible On</strong></td>
											<td >
												<input type="text" name="flexible" id="flexible" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['Flexible'];?>">
											</td>
										</tr>
										<tr>
											<td><strong>Do not want</strong></td>
											<td >
												<input type="text" name="notwant" id="notwant" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['NotWant'];?>">
											</td>
										</tr>
										<tr>
											<td><strong>Notes from Client</strong></td>
											<td >
												<input type="text" name="clientnote" id="clientnote" placeholder="Text box, i.e. open to mini van or suv" value="<?php echo $vspdata[$current]['ClientNote'];?>">
											</td>
										</tr>
                                                                                
                                                                               <td class="assessment_insidetd" colspan="2"><strong class="assessment_inside"> Sales Rep Review::  </strong></td>
                                                                                
                                                                               <tr>
											<td><strong>Budget Range (without tax) </strong></td>
                                                                                        
                                                                                        <?php		 
                                                                                        if(($budgetfrom!="") && ($budgetto !="")){ ?>
                                                                                             <td >$ 
												<select name="budgetfrom" id="budgetfrom" class="width85">
													<option value="Not Sure" <?php if($budgetfrom == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
													<?php for($i=10000; $i <= 100000; $i=$i+500){?>
														<option value="<?php echo $i;?>" <?php if($i==$budgetfrom){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
                                                                                                </select>&nbsp;&nbsp; TO&nbsp;&nbsp;
                                                                                                
                                                                                                $<select name="budgetto" id="budgetto" class="width92">
													<option value="Not Sure" <?php if($budgetto == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
													<?php for($i=10000; $i <= 100000; $i=$i+500){?>
														<option value="<?php echo $i;?>" <?php if($i==$budgetto){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
												</select>
											</td> 
                                                                                        
                                                                                      <?php } else { ?>  
                                                                                           <td >$ 
												<select name="budgetfrom" id="budgetfrom" class="width85">
													<option value="Not Sure" <?php if($ass_BudgetFrom == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
													<?php for($i=10000; $i <= 100000; $i=$i+500){?>
														<option value="<?php echo $i;?>" <?php if($i==$ass_BudgetFrom){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
                                                                                                </select>&nbsp;&nbsp; TO&nbsp;&nbsp;
                                                                                                
                                                                                                $<select name="budgetto" id="budgetto" class="width92">
													<option value="Not Sure" <?php if($ass_BudgetTo == 'Not Sure') {?> selected="selected" <?php }?>>Not Sure</option>
													<?php for($i=10000; $i <= 100000; $i=$i+500){?>
														<option value="<?php echo $i;?>" <?php if($i==$ass_BudgetTo){?> selected="selected" <?php }?>><?php echo number_format($i);?></option>
													<?php }?>
												</select>
											</td>   
                                                                                      <?php  } ?>
                                                                                        
                                                                                      
										</tr>
                                                                                
                                                                                <tr>
											<td><strong>Review</strong></td>
											<td >
												<input type="text" name="seller_review" id="seller_review" placeholder="Add Review of seller" value="<?php echo $vspdata[$current]['seller_review'];?>">
											</td>
										</tr>
                                                                                
                                                                                <tr>
                                                                                    <td><strong>Plan Rating By SALE Person</strong></td>
											<td >
												    
                                                                                                <select  name="plan_rating" id="plan_rating">
                                                                                                    <option value="A" <?php if($plan_rating == 'A') {?> selected="selected" <?php }?>>A</option>
                                                                                                    <option value="B" <?php if($plan_rating == 'B') {?> selected="selected" <?php }?>>B</option>
                                                                                                    <option value="C" <?php if($plan_rating == 'C') {?> selected="selected" <?php }?>>C</option>
                                                                                                    <option value="D" <?php if($plan_rating == 'D') {?> selected="selected" <?php }?>>D</option>
                                                                                                    <option value="A-" <?php if($plan_rating == 'A-') {?> selected="selected" <?php }?>>A-</option>
                                                                                                    <option value="B-" <?php if($plan_rating == 'B-') {?> selected="selected" <?php }?>>B-</option>
                                                                                                    <option value="C-" <?php if($plan_rating == 'C-') {?> selected="selected" <?php }?>>C-</option>
                                                                                                    <option value="D-" <?php if($plan_rating == 'D-') {?> selected="selected" <?php }?>>D-</option>
                                                                                                </select>
											</td>
										</tr>
                                                                               
                                                                                
                                                                                <tr>
											<td></td>
											<td>
												<button type="submit" class="med"><nobr>Save</nobr></button>
                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                
											</td>
										</tr>
                                                                                
										
									</table>
								
							
							<br clear="all" />
				<?php
							mysql_close($recon);
						}
					}
				?>
			
                            
                            
                       
				
				
			
        </form>
                
                 
          
        </div>
          
    </div><!-- end grideightgrey-->
    
       <div class="grideightgrey">
        <p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;"></p>
        <div class="grideight" style="width: 600px; margin-top:-5px; margin-bottom: 0px;">
            <form method="post" action="sales_addnew_pos.php">
                <input type="hidden" id="ForUserID" name="ForUserID" value="<?php echo $inuserid; ?>" />
                <input type="hidden" id="MarketNeedID" name="MarketNeedID" value="<?php echo $inmneed; ?>" />
                <input type="hidden" id="spdetailid" name="spdetailid" value="<?php echo $SearchPlanDetailID; ?>" />
                <input type="hidden" id="searchplanid" name="searchplanid" value="<?php echo $SearchPlanID; ?>" />
                <button type="submit" class="med"><nobr>Add New Price & Availability Study</nobr></button>
            </form>
    </div>
            </div>
    </div>
    
   
        <style>
            
             td, th {
        padding: 3px !important;
    }
    .assessment_insidetd{
        background: none repeat scroll 0% 0% gray; 
        color: white;
    }
    select, textarea, input[type='text'], input[type='password'] {
  border: 1px solid #aaaaaa;
  background: #fff;
  resize: none;
  color: #252525;
  cursor: text;
  padding: 3px;
  font-family: Arial, Helvetica, Sans Serif;
  font-size: 13px;
  width: 78%;
}
    </style>
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
