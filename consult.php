<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 12;
    $_SESSION['titleadd'] = 'Consultation';
	
	if (!isset($_SESSION['user'])) {
		unset($_SESSION['assessment_post']);
		unset($_SESSION['pos_post']);
		unset($_SESSION['searchplan_post']);		
		unset($_SESSION['request_trade_in_quote']);
		$_SESSION['consult_post'] = 1;	
		$_SESSION['submit_btn'] = "SUBMIT and continue to REQUEST CONSULTATION";
		echo "<script>window.location='register.php';</script>";
	}

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];
    $contactnow = 1;  // default to checked the first time through.  not truly sure why this is needed

    if (isset($_SESSION['user'])) {
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        // Get the current Request for Consultation Information (if any)...
        $query = 'select Notes,contact_type from consultations where MarketNeedID = '.$marketneedid;
        $result = mysql_query($query, $con);
        
        if($result && $row = mysql_fetch_array($result))
        {
            $anote = $row[0];
            $type = explode(",",$row['contact_type']);
        }
      
        $query = "select needscontact from marketneeds where MarketNeedID = ".$marketneedid;
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
          echo  $contactnow = $row[0];
        }

        mysql_close($con);
    }
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<style>
    .error {
    color: red;
    font-size: 13px;
}
</style>

<!--<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>-->

<script type="text/javascript" >

window.locaion = "";
(function ($, W, D)
{
var JQUERY4U = {};

JQUERY4U.UTIL =
{
setupFormValidation: function ()
{
//form validation rules
$("#consultform").validate({
     rules: {
notetext:{
required:true
}
},
messages: {
message: "Message is required!",
},
submitHandler: function (form) {
form.submit();
}
});
}
}

//when the dom has loaded setup form validation rules
$(D).ready(function ($) {
JQUERY4U.UTIL.setupFormValidation();
});

})(jQuery, window, document);

</script>


<div class="gridtwelve"></div>
<div id="content">
	<style>
		.welcome-message {
			text-align: center;
			color: #7bae2b;
		}
	</style>	
	<?php if( @$_SESSION['welcome_message'] ) : ?>			
		<p class="welcome-message"><?php echo $_SESSION['welcome_message']; ?></p>
	<?php endif; ?>
	<?php 
		//unset $_SESSION for welcome message.
		unset( $_SESSION['welcome_message'] );
	?>
	
    <div class="grideightcontainer">
       <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Request Consultation
       <a href="javascript:history.back()" style="color:black"><img src="images/back.png"></a></h1>
       <form action="consultsave.php" method="post" name="consultform" id="consultform">
            <!--<div class="grideightgrey">
                <div class="grideight">
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        The text box below is meant to give you a free form way of describing your vehicle needs and wants.
                        Once completed, your information will be saved and emailed to your 1-800-vehicles.com representative immediately.
                        He or she will then contact you to discuss your information and make recommendations.
                    </p>
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        Please describe your needs and wants here:
                    </p>
                    <textarea style="width: 100%; height: 200px;" name="notetext" id="notetext"  wrap="hard"><?php if(isset($anote)&&(strlen($anote)>0)) echo $anote; ?></textarea>
                    <br clear="all" />
                    <br/>
                    Don't have time to describe your needs at the moment, but want a call back right away?<br/>
                    You may check this box and submit the Request (or simply call 1-800-vehicles right away).<br/><br/>
                    <input type="checkbox" name="repcall" <?php if(isset($contactnow) && ($contactnow == 1)) echo 'checked="checked"'; ?> />
                    <span style="color:#142c3c;">Please have my 1-800-vehicles.com representative contact me for free vehicle consultation.</span>
                    <br />
                    <p><button type="submit" id="submit_consult" class="med">
                        <?php if(isset($anote) && (strlen($anote)>0)) echo 'UPDATE&nbsp;REQUEST'; else echo 'SUBMIT&nbsp;REQUEST';?>
                    </button></p>
                </div>
            </div>-->
            
            
            <div class="grideightgrey">
                <div class="grideight">
                     <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        How would you like for your representative to contact you?
                    </p>
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        By phone    <input style="margin-left: 20px;" type="checkbox" name="conatact_type[]" id ="conatact_type[]" value="Phone" <?php if(isset($type) && in_array("Phone", $type)) echo "checked"?>>
                    </p>
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        By text    <input style="margin-left: 32px;" type="checkbox" name="conatact_type[]" id ="conatact_type[]" value="Text" <?php if(isset($type) && in_array("Text", $type)) echo "checked"?>>
                    </p>
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        By email    <input style="margin-left: 26px;" type="checkbox" name="conatact_type[]" id ="conatact_type[]" value="Email" <?php if(isset($type) && in_array("Email", $type)) echo "checked"?>>
                    </p>
                    
                    <p class="blacktwelve" style="color: rgb(20, 44, 60);">
                        Notes:
                    </p>
                    <textarea style="width: 40%; height: 100px;" name="notetext" id="notetext"  wrap="hard"><?php if(isset($anote)&&(strlen($anote)>0)) echo $anote; ?></textarea>
                    <br clear="all" />
                    <br/>
                    <!--<input type="checkbox" name="repcall" <?php if(isset($contactnow) && ($contactnow == 1)) echo 'checked="checked"'; ?> />
                    <span style="color:#142c3c;">Please have my 1-800-vehicles.com representative contact me for free vehicle consultation.</span>
                    <br />-->
                    <p><button type="submit" id="submit_consult" class="med">
                        <?php if(isset($anote) && (strlen($anote)>0)) echo 'UPDATE&nbsp;REQUEST'; else echo 'SUBMIT&nbsp;REQUEST';?>
                    </button></p>
                    </div>
            </div>
        </form>
    </div><!-- end grideightgrey-->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
