<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 5;
    $_SESSION['substate'] = -1;
    $_SESSION['titleadd'] = 'Not a Customer';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 350px;">Customer Checkbox Required</h1>
        <div class="grideightgrey">
            <p class="blacktwelve">
                Sorry, in order to access that area of the site, you must be a customer on the site.<br/><br/>
                If you want to try this process and see the site as a customer does, please request your GM to set you up as a customer and you will be able to access this area.
            </p>
        </div><!-- end greyeightgrey-->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
