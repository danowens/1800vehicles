<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 5;
    $_SESSION['titleadd'] = 'Lost Password';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <form action="forgotpass2.php" onsubmit="return validateFormOnSubmit(this)" method="post">
    <div class="grideightcontainer">
        <h1 class="subhead" style="width: 260px;">Forgot your password?</h1>
        <div class="grideightgrey">
            <div class="grideight" style="width:580px;">
                <br />
                <label for="loginname" class="blackfourteen" style="font-size: 13px; color:#142c3c;"><strong>Enter your Email Address (or Login Name):</strong></label>
                <input name="loginname" id="loginname" type="text" size="30" style="margin-left:10px;" />
                <br />
                <br />
                <button type="submit" value="" class="med"><nobr>CONTINUE</nobr></button>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("why.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
