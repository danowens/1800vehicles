<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 5;
    $_SESSION['titleadd'] = 'Help';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Help</h1>
        <div class="grideightgrey">
            <div class="grideight">
                <p style="font-size:14px; margin-top:0px;"><strong>For questions concerning the process or the company:</strong></p>
                <p style="font-size:12px;"><a href=<?php echo 'mailto:'.PRIMARY_EMAIL_ADDR?>>Email the company</a></p>
                <br />
                <p style="font-size:14px;"><strong>For questions concerning technical problems:</strong></p>
                <p style="font-size:12px;"><a href=<?php echo 'mailto:'.PRIMARY_EMAIL_ADDR?>>Email the 1-800-vehicles.com site administrator</a></p>
                <br />
                <p style="font-size:14px;"><strong>Phone Number:</strong></p>
                <p style="font-size:12px; color:#216dce;">Toll Free: 1-800-VEHICLES<br />
                <span style="margin-left:48px;">(834-4253)</span></p>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
