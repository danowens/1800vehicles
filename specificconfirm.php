<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 3;
    $_SESSION['substate'] = 3;
    $_SESSION['titleadd'] = 'Specific Vehicle Quotes';

    if(!isset($_POST['SVQID']))
    {
        $_SESSION['ShowError'] = 'Internal Error - 0x000303';
        header('Location: svqlist.php');
        exit();
    }

    $svqid = $_POST['SVQID'];

    $errormessage = 'false';
    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select v.Year, m.Name, v.Model, v.Style, v.VIN, v.Mileage, v.Doors, v.ExteriorColor, v.InteriorColor, v.Transmission, v.WheelDriveType, v.EngineCylinders";
        $query .= ",v.SeatMaterial, v.WheelCovers, v.Sunroof, v.RadioFM, v.RadioCassette, v.RadioCD, v.PowerDoors, v.PowerWindows, v.PowerSeats, v.FrontHeatedSeats";
        $query .= ",v.AirConditioning, v.RemoteEntry, v.TractionControl, v.SecuritySystem, v.CruiseControl, v.Navigation, s.PriceQuoted, v.SpecialNotes, v.Information";
        $query .= ", v.ExtraFeatures, v.AudioFile, v.OnlineLink, v.VideoFile, s.AddDepositAmount, s.AddSecondKeyLimit, s.AddNavDiscLimit, s.AddSellerRepairLimit";
        $query .= ", s.AddPurchaserRepairLimit, s.AddTireCopay, s.AddDelivery";
        $query .= " from specificvehicles s, vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=s.VehicleDetailID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        if(!$result || !$row = mysql_fetch_array($result))
        {
            mysql_close($con);
            $_SESSION['ShowError'] = 'Internal Error - 0x000303';
            header('Location: svqlist.php');
            exit();
        }

        $svqyear = $row[0];
        $svqmake = $row[1];
        $svqmodel = $row[2];
        $svqstyle = $row[3];
        $svqvin = $row[4];
        $svqmiles = $row[5];
        $svqdoors = $row[6];
        $svqextcolor = $row[7];
        $svqintcolor = $row[8];
        $svqtrans = $row[9];
        $svqwdt = $row[10];
        $svqcyl = $row[11];
        $svqseat = $row[12];
        $svqwtype = $row[13];
        $svqroof = $row[14];
        $svqradfm = $row[15];
        $svqradcas = $row[16];
        $svqradcd = $row[17];
        $svqpdoor = $row[18];
        $svqpwin = $row[19];
        $svqpseat = $row[20];
        $svqhseat = $row[21];
        $svqair = $row[22];
        $svqremote = $row[23];
        $svqtraction = $row[24];
        $svqsecure = $row[25];
        $svqcruise = $row[26];
        $svqnav = $row[27];
        $svqprice = $row[28];
        $svqspecial = $row[29];
        $svqdesc = $row[30];
        $svqextra = $row[31];
        $svqaudio = $row[32];
        $svqonline = $row[33];
        $svqvideo = $row[34];
        $svqadddep = $row[35];
        $svqaddsec = $row[36];
        $svqaddnav = $row[37];
        $svqaddsel = $row[38];
        $svqaddpur = $row[39];
        $svqaddtir = $row[40];
        $svqadddel = $row[41];

        // Determine whether pickup or delivery...
        $svqpickup = 1;
        $svqcity = '';
        $svqstate = '';

        // Check the orders first...
        $query = "select q.Pickup, q.DeliverToCity, q.DeliverToState from specificvehicles s, orderfor o, firmquotes f, quoterequests q where q.QuoteRequestID=f.QuoteRequestID and f.FirmQuoteID=o.FirmQuoteID and o.OrderID=s.OrderID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        while($result && $row = mysql_fetch_array($result))
        {
            if($row[0] == 0)
            {
                $svqpickup = 0;
                $svqcity = $row[1];
                $svqstate = $row[2];
                break;
            }
        }

        // Check the watches next...
        if($svqpickup == 1)
        {
            $query = "select q.Pickup, q.DeliverToCity, q.DeliverToState from specificvehicles s, watchfor w, firmquotes f, quoterequests q where q.QuoteRequestID=f.QuoteRequestID and f.FirmQuoteID=w.FirmQuoteID and w.WatchID=s.WatchID and s.SpecificVehicleID=".$svqid;
            $result = mysql_query($query);
            while($result && $row = mysql_fetch_array($result))
            {
                if($row[0] == 0)
                {
                    $svqpickup = 0;
                    $svqcity = $row[1];
                    $svqstate = $row[2];
                    break;
                }
            }
        }

        // Look for images of the vehicle in question...
        $query = "select count(*) from specificvehicles s, vehicleimages i where s.VehicleDetailID=i.VehicleDetailID and s.SpecificVehicleID=".$svqid;
        $result = mysql_query($query);
        if($result && $row = mysql_fetch_array($result))
        {
            $svqimages = $row[0];
        }
        else $svqimages = 0;

        if($svqimages > 0)
        {
            $query = "select i.ImageFile from specificvehicles s, vehicleimages i where s.VehicleDetailID=i.VehicleDetailID and i.DisplayOrder=1 and s.SpecificVehicleID=".$svqid;
            $result = mysql_query($query);
            if($result && $row = mysql_fetch_array($result))
            {
                $svqimage = $row[0];
            }
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
    <div class="grideightcontainer">
        <div class="grideightgrey">
            <p style="font-size:15px; font-weight:bold; width:575px; margin: 10px auto;">By confirming this wholesale purchase you agree to be bound by the terms of the purchase agreement listed below. Once confirmed, 1-800-vehicles.com will make every effort to purchase this vehicle on your behalf. </p>
            <br />
            <center>
                <table>
                    <tr>
                        <td align="right">
                            <form action="specificordered.php" method="post">
                                <input type="hidden" value="<?php echo $svqid; ?>" name="SVQID" />
                                <button value="" class="med"><nobr>CONFIRM</nobr></button>
                            </form>
                        </td>
                        <td align="left">
                            <form action="svqlist.php" method="post">
                                <button value="" class="med" style="margin-left:10px;"><nobr>CANCEL</nobr></button>
                            </form>
                        </td>
                    </tr>
                </table>
            </center>
        </div><!--end grideightgrey -->
        <h1 class="subhead" style="width:300px;">Specific Vehicle Quote</h1>
        <div class="grideightgrey">
            <p class="blackeleven" style="margin: 0; margin-left: -5px;"><a href="svqlist.php">Go back to Specific Quote List</a></p>
<?php
    if(isset($svqimage))
    {
        $max_width = 365;
        $max_height = 365;
        echo '<img id="vehimage" src="loadimage.php?image='.$svqimage.'&mwidth='.$max_width.'&mheight='.$max_height.'" align="right" style="margin-top:10px;" />';
    }
    //else echo '** Image not available **';
?>
            <br />
            <p style="margin: 0; font-size: 19px; margin-left: -5px;"><?php echo $svqyear.' '.$svqmake.' '.$svqmodel; ?></p>
            <p class="blackfourteen" style="margin-top: 0; color:#757575; margin-left: -5px;"><?php echo $svqdoors; ?> Door <?php echo $svqstyle; ?></p>
            <table border="0" width="235" align="left" style="font-size:13px; margin-top:-20px; margin-left: -5px;">
                <tr>
                    <td><strong>Year</strong></td>
                    <td><?php echo $svqyear; ?></td>
                </tr>
                <tr>
                    <td><strong>Make</strong></td>
                    <td><?php echo $svqmake; ?></td>
                </tr>
                <tr>
                    <td><strong>Model</strong></td>
                    <td><?php echo $svqmodel; ?></td>
                </tr>
                <tr>
                    <td><strong>Style</strong></td>
                    <td><?php echo $svqstyle; ?></td>
                </tr>
                <tr>
                    <td><strong>Mileage</strong></td>
                    <td><?php echo number_format($svqmiles); ?></td>
                </tr>
                <tr>
                    <td><strong>VIN</strong></td>
                    <td style="font-size:10px;"><?php echo $svqvin; ?></td>
                </tr>
                <tr>
                    <td><strong>Exterior Color</strong></td>
                    <td><?php echo $svqextcolor; ?></td>
                </tr>
                <tr>
                    <td><strong>Interior Color</strong></td>
                    <td><?php echo $svqintcolor; ?></td>
                </tr>
                <tr>
                    <td><strong>Transmission</strong></td>
                    <td><?php echo $svqtrans; ?></td>
                </tr>
                <tr>
                    <td><strong>Wheel Drive</strong></td>
                    <td>
<?php
    if($svqwdt == 'All') echo 'AWD';
    elseif($svqwdt == 'Four') echo 'FWD';
    else echo '2WD';
?>
                    </td>
                </tr>
<?php
    if($svqcyl > 0) echo '<tr><td><strong>Transmission</strong></td><td>'.$svqcyl.' cylinders</td></tr>';
    if($svqseat != 'Unknown') echo '<tr><td><strong>Seat Material</strong></td><td>'.$svqseat.'</td></tr>';
    if(strlen($svqwtype) > 0) echo '<tr><td><strong>Wheel Type</strong></td><td>'.$svqwtype.'</td></tr>';
    if($svqroof != 'None') echo '<tr><td><strong>Sunroof</strong></td><td>'.$svqroof.'</td></tr>';
    if($svqradfm)
    {
        echo '<tr><td><strong>Stereo</strong></td><td>AM/FM';
        if($svqradcas) echo ', CAS';
        if($svqradcd) echo ', CD';
        echo '</td></tr>';
    }
    elseif($svqradcas)
    {
        echo '<tr><td><strong>Stereo</strong></td><td>CAS';
        if($svqradcd) echo ', CD';
        echo '</td></tr>';
    }
    elseif($svqradcd) echo '<tr><td><strong>Stereo</strong></td><td>CD</td></tr>';
    if($svqpdoor == 1) echo '<tr><td><strong>Power Door Locks</strong></td><td>Yes</td></tr>';
    if($svqpwin == 1) echo '<tr><td><strong>Power Windows</strong></td><td>Yes</td></tr>';
    if($svqpseat == 1) echo '<tr><td><strong>Power Seats</strong></td><td>Yes</td></tr>';
    if($svqhseat == 1) echo '<tr><td><strong>Heated Seats</strong></td><td>Yes</td></tr>';
    if($svqair == 1) echo '<tr><td><strong>Air Conditioning</strong></td><td>Yes</td></tr>';
    if($svqremote == 1) echo '<tr><td><strong>Remote Entry</strong></td><td>Yes</td></tr>';
    if($svqtraction == 1) echo '<tr><td><strong>Traction Control</strong></td><td>Yes</td></tr>';
    if($svqsecure == 1) echo '<tr><td><strong>Security System</strong></td><td>Yes</td></tr>';
    if($svqcruise == 1) echo '<tr><td><strong>Cruise Control</strong></td><td>Yes</td></tr>';
    if($svqnav == 1) echo '<tr><td><strong>Navigation</strong></td><td>Yes</td></tr>';
    if($svqadddel > 0)
    {
        echo '<tr><td><strong>Vehicle</strong></td><td style="text-align:right;">$'.number_format($svqprice).'</td></tr>';
        echo '<tr><td><strong>Delivery</strong></td><td style="text-align:right;">$'.number_format($svqadddel).'</td></tr>';
        echo '<tr><td><strong>Price</strong></td><td style="text-align:right; color:#85c11b; font-size:14px;"><strong>$'.number_format($svqadddel+$svqprice).'</strong></td></tr>';
    }
    else
    {
        echo '<tr><td><strong>Price</strong></td><td style="color:#85c11b; font-size:14px;"><strong>$'.number_format($svqprice).'</strong></td></tr>';
    }
?>
            </table>
            <div class="gridtenpadded" style="float:right;">
                <span style="font-size:17px; color:#85c11b;">Vehicle Delivery Information</span><br />
                <span style="font-size:13px;">
<?php
    if($svqpickup == 1) echo 'Client will pick up the vehicle at the 1-800-vehicles dealer location.';
    else echo '1-800-vehicles will deliver this vehicle to you in '.$svqcity.', '.$svqstate.'.';
?>
                </span>
                <br /><br />
<?php
    if($svqimages > 0)
    {
        echo '<span style="font-size:15px;">Picture(s) of the actual vehicle shown here.</span><br />';
        echo '<span style="font-size:12px;"><a href="specificimages.php?SVQID='.$svqid.'">View Picture Gallery ('.$svqimages.' pictures available)</a></span><br /><br />';
    }
    if(!is_null($svqaudio)) echo '<span style="font-size:12px;"><a href="'.$svqaudio.'" target="_blank">Listen to Audio about Vehicle</a></span><br /><br />';
    if(!is_null($svqvideo)) echo '<span style="font-size:12px;"><a href="'.$svqvideo.'" target="_blank">Watch a Video of the Vehicle</a></span><br/><br/>';
    //{
    //    echo '<OBJECT CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" WIDTH="176"HEIGHT="170" CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab">';
    //    echo '<PARAM name="SRC" VALUE="'.$svqvideo.'"> ';
    //    echo '<PARAM name="AUTOPLAY" VALUE="false"><param NAME="type" VALUE="video/quicktime"><PARAM name="CONTROLLER" VALUE="true">';
    //    echo '<EMBED SRC="'.$svqvideo.'" WIDTH="176" HEIGHT="170" AUTOPLAY="false" CONTROLLER="true" type="video/quicktime" PLUGINSPAGE="http://www.apple.com/quicktime/download/"></EMBED></OBJECT>';
    //}
    //if(!is_null($svqonline) && (strlen($svqonline) > 0)) echo '<span style="font-size:12px;"><a href="'.$svqonline.'" target="_blank">Review Vehicle Info on another website</a></span><br />';
    if(isset($svqdesc) && (strlen($svqdesc) > 0)) echo '<br /><br /><span style="font-size:17px; color:#85c11b;">Vehicle Description</span><br /><span style="font-size:13px;">'.$svqdesc.'</span>';
    if(isset($svqspecial) && (strlen($svqspecial) > 0)) echo '<br /><br /><span style="font-size:17px; color:#85c11b;">Special Notes</span><br /><span style="font-size:13px;">'.$svqspecial.'</span>';
    if(isset($svqextra) && (strlen($svqextra) > 0)) echo '<br /><br /><span style="font-size:17px; color:#85c11b;">Extra Features</span><br /><span style="font-size:13px;">'.$svqextra.'</span>';
?>
            </div> <!-- end gridtenpadded -->
        </div><!--end grideightgrey-->
<?php
    if(($svqadddep > 0) || ($svqaddtir > 0) || ($svqaddsec > 0) || ($svqaddnav > 0) || ($svqaddsel > 0) || ($svqaddpur > 0))
    {
?>
        <div class="grideightgrey">
            <h4 class="subhead" style="font-size:18px; width: 250px; margin-left: -10px; margin-top: -10px;">Additional Amounts & Co-Pays</h4>
            <p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">(in addition to amounts specified in Specific Vehicle Purchase Agreement)</p>
            <table border="0" width="400">
                <tr>
                    <td><strong>Deposit</strong></td>
                    <td><strong>Tire Copay</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($svqadddep)) echo '$'.number_format($svqadddep);
    else echo '$0.00';
?>
                    </p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($svqaddtir)) echo '$'.number_format($svqaddtir);
    else echo '$0.00';
?>
                    </p></td>
                </tr>
                <tr>
                    <td><strong>Second Key Limit</strong></td>
                    <td><strong>Nav Disc Limit</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($svqaddsec)) echo '$'.number_format($svqaddsec);
    else echo '$0.00';
?>
                    </p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($svqaddnav)) echo '$'.number_format($svqaddnav);
    else echo '$0.00';
?>
                    </p></td>
                </tr>
                <tr>
                    <td><strong>Seller Repair Limit</strong></td>
                    <td><strong>Purchaser Repair Limit</strong></td>
                </tr>
                <tr valign="top">
                    <td height="23"><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($svqaddsel)) echo '$'.number_format($svqaddsel);
    else echo '$0.00';
?>
                    </p></td>
                    <td><p class="blackeleven" style="margin-top: 1px; margin-bottom: 9px;">
<?php
    if(isset($svqaddpur)) echo '$'.number_format($svqaddpur);
    else echo '$0.00';
?>
                    </p></td>
                </tr>
            </table>
        </div><!--end grideightgrey-->
<?php
    }
?>
        <h2 class="subhead" style="width: 335px;">Specific Vehicle Purchase Agreement</h2>
        <div class="grideight" style="padding:5px;">
<?php
    $aname = 'Specific Vehicle Purchase';

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);

        $query = "select AgreementID from agreements where AgreementName='".$aname."'";
        $result = mysql_query($query, $con);
        if($result && $row = mysql_fetch_array($result))
        {
            $aid = $row[0];

            $query = "select SectionName, SectionText from agreementsections where AgreementID=".$aid;
            $result = mysql_query($query, $con);
            $index = 0;
            while($result && $row = mysql_fetch_array($result))
            {
                $secname[$index] = $row[0];
                $sectext[$index] = $row[1];
                $index++;
            }
        }

        mysql_close($con);
    }
    $count = count($secname);
    ini_set('display_errors','on');
    for($index = 0; $index < $count; $index++)
    {
        //$textdisp = replaceagreementtags($sectext[$index]);
        //echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($secname[$index]).'</strong> - '.$textdisp.'</p>';
        echo '<p class="blackfourteen" align="justify"><strong>'.strtoupper($secname[$index]).'</strong> - '.$sectext[$index].'</p>';
    }
?>
            <p class="blackfourteen" style="color:#85c11b;"><strong>THANK YOU FOR GIVING US AN OPPORTUNITY TO SERVE YOU! </strong></p>
        </div><!--end grideight-->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
