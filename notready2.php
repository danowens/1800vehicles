<?php require("globals.php"); ?>
<?php
    $_SESSION['state'] = 1;
    $_SESSION['substate'] = -1;
    $_SESSION['titleadd'] = 'Not in Step 2 Yet';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Getting Too Far Ahead</h1>
        <div class="grideightgrey">
            <div class="grideight">
                <p class="blackfourteen" style="margin-top:0;"><strong>You have not completed the actions required to reach step 2 yet.</strong></p>
                <p class="blackfourteen">
                    <strong>
                        To reach Step 2 in the process you will need to perform one of the following actions:<br/>
                        &nbsp;&nbsp;A) Fill out a <a href="assessment.php">Quick Assessment</a><br/>
                        &nbsp;&nbsp;B) Fill out a <a href="consult.php">Request for Consultation</a><br/>
                        &nbsp;&nbsp;C) Fill out a <a href="requestquote.php">Request for Quote</a><br/>
                    </strong>
                </p>
            </div><!-- endgrideight -->
        </div><!-- endgrideightgrey -->
    </div><!-- grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
