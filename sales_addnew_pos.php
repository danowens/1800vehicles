<?php require("globals.php"); ?> 
<?php
	//error_reporting(E_ALL);
	//ini_set("display_errors", "on");
$_SESSION['state'] = 5;
$_SESSION['substate'] = 9;
$_SESSION['titleadd'] = 'Add New Price & Availability Study';
$total			= 0;

 $inuserid = $_POST['ForUserID'];
 $inmneed = $_POST['MarketNeedID'];
 $SearchPlanDetailID = $_POST['spdetailid']; 
 $SearchPlanID = $_POST['searchplanid'];
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php // require("headerend.php"); ?>
<script type="text/javascript">
    var count = <?php echo $total;?>;
	$(document).ready(function () {
		$('body').on('click', 'input:text', function () {
			var element = $(this);
			$("#notetext").val(element.val());
			$( "#dialog" ).dialog({
				modal: true,
				title: " ",
				close: function( event, ui ) {
					element.val($("#notetext").val());
					$("#dialog").dialog("destroy");
				}
			});
		});
		if(count==0){
			addvehicle();
		}
	});
	
	function validateFormOnSubmit(){
        var reason = "";
        if(reason != "")
        {
            vbudget.focus();
            alert("Some fields need correction:"+'\n' + reason + '\n');
            return false;
        }
        return true;
    }
	
	function addvehicle(){
		id = window.count;
                //console.log(id);
		window.count = window.count+1; 
		//console.log($("#number").val());
		$("#number").val(window.count);
		//console.log("latest - "+$("#number").val());
		if(window.count==1){
			$.ajax({
				type: "POST",
				url: 'ajax_salesaddpas.php',
				data: { number:window.count,spdetailid:<?=$SearchPlanID;?> },
				cache: false,
				async: false,
				dataType: 'html',
				success: function(data) {
					$("#vehiclesplan").append(data);
					
					
				}
			});
		}else{
			yearlist			= $("#yearlist"+id).val();
			makelist			= $("#makelist"+id).val();
			modellist			= $("#modellist"+id).val();
			stylelist			= $("#stylelist"+id).val();
			otherstyle			= $("#otherstyle"+id).val();
			otheryear			= $("#otheryear"+id).val();
			vehicleneed 		= $("#vehicleneed"+id).val();
			mileagefrom 		= $("#mileagefrom"+id).val();
			mileageto			= $("#yearlist"+id).val();
			mileageceiling		= $("#mileageceiling"+id).val();
			transmission		= $("#transmission"+id).val();
			drivetrain			= $("#drivetrain"+id).val();
			extlike				= $("#extlike"+id).val();
			extdislike			= $("#extdislike"+id).val();
			intlike				= $("#intlike"+id).val();
			intdislike			= $("#intdislike"+id).val();
			budgetfrom			= $("#budgetfrom"+id).val();
			budgetto			= $("#budgetto"+id).val();
			borrowmaxpayment	= $("#borrowmaxpayment"+id).val();
			borrowdownpayment	= $("#borrowdownpayment"+id).val();
			//Do you Prefer section start
			frontstype			= $("#frontstype"+id).val();
			bedtype				= $("#bedtype"+id).val();
			leather				= $("#leather"+id).val();
			heatedseat			= $("#heatedseat"+id).val();
			navigation			= $("#navigation"+id).val();
			sunroof				= $("#sunroof"+id).val();
			alloywheels			= $("#alloywheels"+id).val();
			rearwindow			= $("#rearwindow"+id).val();
			bedliner			= $("#bedliner"+id).val();
			entertainmentsystem	= $("#entertainmentsystem"+id).val();
			thirdrs				= $("#thirdrs"+id).val();
			crow				= $("#crow"+id).val();
			prhatch				= $("#prhatch"+id).val();
			backupcamera		= $("#backupcamera"+id).val();
			tpackage			= $("#tpackage"+id).val();
			//Do you Prefer section end
			musthave			= $("#musthave"+id).val();
			reallyhave			= $("#reallyhave"+id).val();
			flexible			= $("#flexible"+id).val();
			notwant				= $("#notwant"+id).val();
			clientnote			= $("#clientnote"+id).val();
			
			
			$.ajax({
				type: "POST",
				url: 'ajax_salesaddpas.php',
				data: { number:window.count,yearlist:yearlist,makelist:makelist,modellist:modellist,stylelist:stylelist,otherstyle:otherstyle,otheryear:otheryear,vehicleneed:vehicleneed,mileagefrom:mileagefrom,mileageto:mileageto,mileageceiling:mileageceiling,transmission:transmission,drivetrain:drivetrain,extlike:extlike,extdislike:extdislike,intlike:intlike,intdislike:intdislike,budgetfrom:budgetfrom,budgetto:budgetto,borrowmaxpayment:borrowmaxpayment,borrowdownpayment:borrowdownpayment,frontstype:frontstype,bedtype:bedtype,leather:leather,heatedseat:heatedseat,navigation:navigation,sunroof:sunroof,alloywheels:alloywheels,rearwindow:rearwindow, bedliner:bedliner,entertainmentsystem:entertainmentsystem,thirdrs:thirdrs,crow:crow,prhatch:prhatch,backupcamera:backupcamera,tpackage:tpackage,musthave:musthave,reallyhave:reallyhave,flexible:flexible,notwant:notwant,clientnote:clientnote,spdetailid:<?=$SearchPlanID;?>},
				cache: false,
				async: false,
				dataType: 'html',
				success: function(data) {
					$("#vehiclesplan").append(data);
				} 
			});
		}
	}
	
	function yearchanged(id){
		var year = $("#yearlist"+id).val();
		$.ajax({
			type: "POST",
			url: 'ajaxallmakes.php',
			data: { year:year },
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#makelist"+id).html('');
				$("#makelist"+id).append(data);
				
				makechanged(id);
			}
		});
	}
	
	function makechanged(id){
		var year = $("#yearlist"+id).val();
		var make = $("#makelist"+id).val();
		$.ajax({
			type: "POST",
			url: 'ajaxallmodels.php',
			data: { year:year, make:make},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#modellist"+id).html('');
				$("#modellist"+id).append(data);
				
				modelchanged(id);
			}
		});
	}
	
	function modelchanged(id){
		var year	= $("#yearlist"+id).val();
		var make	= $("#makelist"+id).val();
		var model	= $("#modellist"+id).val();
		
		$.ajax({
			type: "POST",
			url: 'ajaxallstyles.php',
			data: { year:year, make:make, model:model},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#stylelist"+id).html('');
				$("#stylelist"+id).append(data);
				
				stylechanged(id);
			}
		});
	}
	
	function stylechanged(id){
		var style = $("#stylelist"+id).val().split(";");
		$.ajax({
			type: "POST",
			url: 'ajaxcheckvehicletype.php',
			data: { id:style[0]},
			cache: false,
			async: false,
			dataType: 'html',
			success: function(data) {
				$("#vehicletype"+id).val(data);
				if(data=='Auto'){
					$("#drivetrain"+id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="Rear Wheel Drive">Rear Wheel Drive</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).hide();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).hide();
					$("#trbackupcamera"+id).hide();
					$("#trtpackage"+id).hide();
					
				}else if(data=='Minivan' || data=='MiniVan'){
					$("#drivetrain"+id).html('<option value="Front Wheel Drive" selected="selected">Front Wheel Drive</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).show();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).show();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).hide();

				}else if(data=='SUV'){
					$("#drivetrain"+id).html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
				
					//Do you prefer section
					$("#trfrontstype"+id).hide();
					$("#trbedtype"+id).hide();
					$("#trrearwindow"+id).hide();
					$("#trbedliner"+id).hide();
					$("#trentertainmentsystem"+id).show();
					$("#trthirdrs"+id).show();
					$("#trcrow"+id).show();
					$("#trprhatch"+id).show();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).show();

				}else if(data=='Pickup'){
					$("#drivetrain").html('<option value="2wd" selected="selected">2wd</option><option value="4wd">4wd</option><option value="Flexible">Flexible</option>');
					
					//Do you prefer section
					//@TODO make function with 2 parameter (Array of ids and event)
					$("#trfrontstype"+id).show();
					$("#trbedtype"+id).show();
					$("#trrearwindow"+id).show();
					$("#trbedliner"+id).show();
					$("#trentertainmentsystem"+id).hide();
					$("#trthirdrs"+id).hide();
					$("#trcrow"+id).hide();
					$("#trprhatch"+id).hide();
					$("#trbackupcamera"+id).show();
					$("#trtpackage"+id).show();
				}
			}
		});
	}
	
	function specific(id, val){
		$("#specific"+id).val(val);
		if(val==1){
			$("#specificsection"+id).hide();
			$("#selectyear"+id).hide();
			$("#selectmake"+id).hide();
			$("#selectmodel"+id).hide();
			$("#selectstyle"+id).hide();
			
			$("#vehiclesection"+id).show();
			$("#textyear"+id).show();
			$("#textmake"+id).show();
			$("#textmodel"+id).show();
			$("#textstyle"+id).show();
		}else{
			$("#specificsection"+id).show();
			$("#selectyear"+id).show();
			$("#selectmake"+id).show();
			$("#selectmodel"+id).show();
			$("#selectstyle"+id).show();
			
			$("#vehiclesection"+id).hide();
			$("#textyear"+id).hide();
			$("#textmake"+id).hide();
			$("#textmodel"+id).hide();
			$("#textstyle"+id).hide();
		}
	}
</script>
 
<div class="gridtwelve"></div>
<div id="content">
	<div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;">Add New PRICE & AVAILABILITY STUDY</h1>
       
        <div class="grideightgrey">
        <form action="<?=WEB_SERVER_NAME?>edit_vehiclepas.php" method="post"> 
        <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
        <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
         <input type="hidden" value="<?php echo $SearchPlanDetailID;?>" name="SearchPlanDetailID" />
            <button class="blueongrey_dash" value="" style="font-size: 11px;" type="submit">
            Go back to Price & Availability Study
          </button>
         </form>
        </div>
	<form action="sale_pas_save.php" onsubmit="javascript:return validateFormOnSubmit()" method="post" name="assessform">
                    <div id="vehiclesplan"></div>
			<div class="grideightgrey">
				<p class="blacktwelve" style="margin-top:-3px; color:#142c3c; font-weight: bold; font-size: 12px;"></p>
				<div class="grideight" style="width: 600px; margin-top:-5px; margin-bottom: 0px;">
					<p><button type="button" class="med" onclick="addvehicle();"><nobr>Add another Price & Availability Study</nobr></button></p>
					<p><button type="submit" class="med"><nobr>SAVE</nobr></button></p>
					<input type="hidden" name="number" id="number" value="<?php echo $total;?>" />
					<div id="dialog" title=" " style="display: none;">
						<textarea name="notetext" id="notetext" placeholder="" style="resize:vertical;width:100%;height:100%"></textarea>
					</div>
				</div>
			</div>
        </form>
    </div><!-- end grideightgrey-->
    
    <style>
        .assessment_insidetd{
        background: none repeat scroll 0% 0% gray; 
        color: white;
    }
    </style>
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
