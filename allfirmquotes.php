<?php require("globals.php"); ?>
<?php
    require_once(WEB_ROOT_PATH.'common/functions/string.php');
    $_SESSION['state'] = 2;
    $_SESSION['substate'] = 0;
    $_SESSION['titleadd'] = 'Current Market Studies';

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];

    $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
    if($con)
    {
        mysql_select_db(DB_SERVER_DATABASE, $con);
        $query = 'select QuoteRequestID, Year, Make, Model, Style, Created, QuoteType from quoterequests where Visible=1 and MarketNeedID='.$marketneedid;
        $result = mysql_query($query);
        $index = 0;
        while($result && $row = mysql_fetch_array($result))
        {
            $quoterequestid[$index] = $row['QuoteRequestID'];
            $veh_year[$index] = $row['Year'];
            $veh_make[$index] = $row['Make'];
            $veh_model[$index] = $row['Model'];
            $veh_style[$index] = $row['Style'];
            $created[$index] = $row['Created'];
            $qtype[$index] = $row['QuoteType'];

            $firmquery = "select OrderType, PricePoint from firmquotes where Visible=1 and UpdateStatus = 'Completed' and QuoteRequestID = ".$quoterequestid[$index];
            $firmresult = mysql_query($firmquery);
            if($firmresult && $row = mysql_fetch_array($firmresult))
            {
                $ordertype[$index] = $row['OrderType'];
                if($ordertype[$index] == 'No Order or Posting Available') $pricepoint[$index] = 0;
                else $pricepoint[$index] = $row['PricePoint'];

                // Adjust the display to something other than what is stored in the database...
                if($ordertype[$index] == 'No Order or Posting Available') $ordertype[$index] = 'Cannot be Ordered or Posted';
                if($ordertype[$index] == 'Group') $ordertype[$index] = 'Group Watchlist';

                // Check if ordered...
                $oquery = "select o.Accepted from quoterequests q,firmquotes f,orderfor of,orders o where o.OrderID=of.OrderID and of.FirmQuoteID=f.FirmQuoteID and f.QuoteRequestID=q.QuoteRequestID and q.QuoteRequestID=".$quoterequestid[$index];
                $oresult = mysql_query($oquery);
                if($oresult && $orow = mysql_fetch_array($oresult)) $veh_status[$index] = 'Ordered';
                else
                {
                    // Check if ordered...
                    $wquery = "select w.Accepted from quoterequests q,firmquotes f,watchfor wf,watches w where w.WatchID=wf.WatchID and wf.FirmQuoteID=f.FirmQuoteID and f.QuoteRequestID=q.QuoteRequestID and q.QuoteRequestID=".$quoterequestid[$index];
                    $wresult = mysql_query($wquery);
                    if($wresult && $wrow = mysql_fetch_array($wresult)) $veh_status[$index] = 'Posted';
                    else $veh_status[$index] = 'Received';
                }
            }
            else
            {
                $ordertype[$index] = 'Unknown';
                $pricepoint[$index] = 0;

                $firmquery = "select OrderType, PricePoint from firmquotes where Visible=1 and UpdateStatus != 'Completed' and QuoteRequestID = ".$quoterequestid[$index];
                $firmresult = mysql_query($firmquery);
                if($firmresult && $firmrow = mysql_fetch_array($firmresult))
                {
                    $veh_status[$index] = 'Reviewing';
                }
                else $veh_status[$index] = 'Requested';
            }

            $index++;
        }

        mysql_close($con);
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
<?php //echo $query; ?>
    <div class="grideightcontainer">
        <h1 class="subhead"><nobr>Current Market Studies</nobr></h1>
        <div class="grideightgrey">
            <p class="blacktwelve">Can't wait for a Current Market Study? Go to &quot;<a href="researchspecific.php">Research Specific Vehicles</a>&quot; for quick estimates.</p>
            <p class="blacktwelve">Every effort will be made to complete your quote request within one business day. However, busy times can affect our ability to reach
                that goal. Please call our sales department at 1-800-VEHICLES (834-4253) if you have not received your quote in a timely fashion.
            </p>
            <ul>
                <li style="color: #000000;">Requesting a Current Market Study does not obligate you in any way.</li>
                <li style="color: #000000;">Current Market Studies are good for 30 days.</li>
            </ul>
            <p>
                PLEASE LIMIT YOU CURRENT MARKET STUDY REQUESTS TO A MAXIMUM OF THREE (3) AT A TIME.  This will allow your representative to be most accurate
                in the availability estimates and price quotations.
            </p>
            <br clear="all"/>
<?php
    $count = count($quoterequestid);
    if($count < 1)
    {
        echo '<p class="blackfourteen"><strong>*** No Active Quote Requests Found ***</strong></p>';
    }
    else
    {
        echo '<form action="deletespec.php" method="post">';
        echo '<table cellpadding="3" cellspacing="3"><tr valign="bottom"><td width="20"></td>';
        //echo '<td width="35" align="center"><h3 class="greensub">NO.</h3></td>';
        $plural = pluralize_noun($count, "CURRENT MARKET STUD", "IES", "Y");
        echo '<td width="165"><h3 class="greensub">'.$plural.'</h3></td>';
        echo '<td width="108" align="center"><h3 class="greensub">STATUS</h3></td>';
        echo '<td width="109" align="center"><h3 class="greensub">AVAILABILITY</h3></td>';
        echo '<td width="104" align="center"><h3 class="greensub">PRICE</h3></td></tr>';
        $anyboxes = 0;
        for($i = 0; $i < $count; $i++)
        {
            echo '<tr valign="baseline">';
            if(($veh_status[$i] == 'Requested') || ($veh_status[$i] == 'Reviewing'))
            {
                echo '<td><input name="QuoteNum[]" type="checkbox" value="'.$quoterequestid[$i].'" /></td>';
                $anyboxes++;
            }
            else echo '<td>&nbsp;</td>';
            //echo '<td align="center"><p class="greyeleven">'.str_pad($quoterequestid[$i],5,"0",STR_PAD_LEFT).'</p></td>';
            echo '<td>';
            echo '<p class="formbluetext">';
            if(($veh_status[$i] == 'Requested') || ($veh_status[$i] == 'Reviewing')) // We can let them edit it...
            {
                echo '<a href="quote.php?EditQuoteID='.$quoterequestid[$i].'">';
                echo $veh_year[$i].' '.$veh_make[$i].' '.$veh_model[$i].' '.$veh_style[$i];
                echo '</a>';
            }
            else  // They can no longer edit it...
            {
                echo '<a href="quotereceived.php?QuoteID='.$quoterequestid[$i].'">';
                echo $veh_year[$i].' '.$veh_make[$i].' '.$veh_model[$i].' '.$veh_style[$i];
                echo '</a>';
            }
            echo '</p><p class="greyeleven">'.date_at_timezone('m/d/Y H:i:s T', 'EST', $created[$i]).'</p>';
            echo '</td>';
            echo '<td align="center"><p class="greyeleven">'.$veh_status[$i].'</p></td>';
            echo '<td align="center"><p class="greyeleven">'.$ordertype[$i].'</p></td>';
            if($pricepoint[$i]==0) echo '<td align="center"><p class="greyeleven">?</p></td>';
            else echo '<td align="center"><p class="greyeleven">$'.number_format($pricepoint[$i]).'</p></td></tr>';
        }
        echo '</table><br /><table border="0" width="300" cellpadding="5"><tr>';
        if($anyboxes > 0) echo '<td><button type="submit" value="" class="med">DELETE SELECTED CURRENT MARKET STUDIES</button></td></tr>';
        echo '</table></form>';
    }
?>
        </div>
        <div class="grideight">
            <p class="blackfourteen">For 1-6 year old autos, SUVs, and minivans
            <form action="quote.php" method="post">
                <input type="hidden" value="Standard" name="QuoteType" />
                <button class="med"><nobr>MAKE A CURRENT MARKET STUDY</nobr></button>
            </form></p>
            <p class="blackfourteen">For special requests or exotic cars
            <form action="quote.php" method="post">
                <input type="hidden" value="Special" name="QuoteType" />
                <button type="submit" class="med"><nobr>MAKE A SPECIAL CURRENT MARKET STUDY</nobr></button>
            </form></p>
            <p class="blackfourteen">For pick-up trucks
            <form action="quote.php" method="post">
                <input type="hidden" value="Pickup" name="QuoteType" />
                <button type="submit" class="med"><nobr>MAKE A PICK-UP CURRENT MARKET STUDY</nobr></button>
            </form></p>
        </div>
    </div><!-- end grid eight container -->
<?php require("teaser.php"); ?>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
