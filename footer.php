<?php if(isset($_SESSION['ACTIVE_DASHBOARD'])){
    $dashboard_url = $_SESSION['ACTIVE_DASHBOARD'];
}else {
     $dashboard_url = 'dashboard.php';
}?>


<footer class="divider-wrapper-dark"> 
	<div class="container">
		<div class="row">
                    <div class="col-md-12 text-center" >
                            <span class="copyright wow fadeInUp">&copy;2015 1-800-Vehicles.com All rights reserved. <a href="<?php echo WEB_SERVER_NAME?>">Home</a> | <a href="mailto:vehicles@1800vehicles.com">Contact Us</a> </span>
			</div>
                    <?php if (isset($_SESSION['user'])) {?>    
                         <div class="col-md-12 text-center">                            
                             <span class="copyright wow fadeInUp">                                 
                                 <a href="<?php echo $dashboard_url;?>">DASHBOARD UPDATES</a>       
                             </span>                            
			</div>
                     <?php }?>
			<div class="col-md-12 text-center">
				<a href="#" id="back-to-top"><i class="pe-7s-angle-up fa-4x"></i></a>
			</div>
		</div>
	</div>
</footer>


</div><!--end footwrap-->

<!-- Bootstrap Core JavaScript -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>

<!-- Plugin JavaScript -->
<script src="assets/js/plugins.js"></script>

<!-- Plugins -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="assets/js/jqBootstrapValidation.js"></script>
<!---<script src="assets/js/contact_me.js"></script>-->

<!-- Custom JavaScript -->
<script src="assets/js/init.js"></script>
