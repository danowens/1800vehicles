<?php require("globals.php"); ?> <?php require("common/functions/emailfunctions.php"); ?> 
<?php
//error_reporting(E_ALL);
//ini_set("display_errors", "on");
$_SESSION['state'] = 0;
$_SESSION['substate'] = 100;
$_SESSION['titleadd'] = 'Save Vehicle Specification';
$userid = $_SESSION['userid'];
$marketneedid = $_SESSION['marketneedid'];
$srep = getsalesrep($userid, $marketneedid);
$errorinsave = 'false';
//@TODO: check for validation of the form data
if (!isset($_SESSION['user'])) {
    $_SESSION['searchplan_post'] = $_POST;
    $_SESSION['laststate'] = $_SESSION['state'];
    $_SESSION['lastsubstate'] = $_SESSION['substate'];
    echo "<script>window.location='register.php';</script>";	
} else {
    if( $errorinsave == 'false' ) {
        $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
        if( $con ) {
            mysql_select_db(DB_SERVER_DATABASE, $con);
			// See if they already have one...
            $query = "select * from searchplans where MarketNeedID = " . $marketneedid;
            $result = mysql_query($query, $con);
            $plandata = mysql_fetch_array($result);
            if (!isset($plandata['SearchPlanID'])) {
                // There is not one already saved, so add it...
                $query = "insert into searchplans (MarketNeedID, Created, LastUpdated, SeenBySales) values (" . $marketneedid . ",";                $query .= "'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','" . date_at_timezone('Y-m-d H:i:s', 'EST') . "',0)";                if (!mysql_query($query, $con))
                    $errorinsave = 'Could not add Vehicle Specification';
                $SVPid = mysql_insert_id($con);				
                if( $errorinsave == 'false' ) {				
                // Add number of vehicles in the search plan
                    if ($_SESSION['searchplan_post']) {
                        for ($i = 1; $i <= $_SESSION['searchplan_post']['number']; $i++) {
                            $style = explode(";", $_SESSION['searchplan_post']["stylelist$i"]);
                            if ($_SESSION['searchplan_post']["vehicletype$i"] == "Auto") {
                            //insert quert data
                                $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels';
                                $doyouprefervalues = "'" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "'";
                            } elseif ($_SESSION['searchplan_post']["vehicletype$i"] == "Minivan") {
                                //insert quert data
                                $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, PRHatch, BackupCamera';
                                $doyouprefervalues = "'" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "','" . $_SESSION['searchplan_post']["entertainmentsystem$i"] . "','" . $_SESSION['searchplan_post']["prhatch$i"] . "','" . $_SESSION['searchplan_post']["backupcamera$i"] . "'";
                            } elseif ($_SESSION['searchplan_post']["vehicletype$i"] == "SUV") {
                                $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, ThirdRD, CRow, PRHatch, BackupCamera, TPackage';
                                $doyouprefervalues = "'" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "','" . $_SESSION['searchplan_post']["entertainmentsystem$i"] . "','" . $_SESSION['searchplan_post']["thirdrs$i"] . "','" . $_SESSION['searchplan_post']["crow$i"] . "','" . $_SESSION['searchplan_post']["prhatch$i"] . "','" . $_SESSION['searchplan_post']["backupcamera$i"] . "','" . $_SESSION['searchplan_post']["tpackage$i"] . "'";
                            } elseif ($_SESSION['searchplan_post']["vehicletype$i"] == "Pickup") {

                                $doyouprefer = 'FrontSType, BedType, Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, RearWindow, BedLiner, BackupCamera, TPackage';
                                $doyouprefervalues = "'" . $_SESSION['searchplan_post']["frontstype$i"] . "','" . $_SESSION['searchplan_post']["bedtype$i"] . "','" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "','" . $_SESSION['searchplan_post']["rearwindow$i"] . "','" . $_SESSION['searchplan_post']["bedliner$i"] . "','" . $_SESSION['searchplan_post']["backupcamera$i"] . "','" . $_SESSION['searchplan_post']["tpackage$i"] . "'";
                            }

                            $query = "insert into searchplandetails (SearchPlanID, `Specific`, Year, Make, Model, Style, TextYear, TextMake, TextModel, TextStyle, OtherStyle, OtherYear, VehicleNeed, MileageFrom, MileageTo, MileageCeiling, Transmission, DriveTrain, ExtLike, ExtDislike, IntLike, IntDisike, BudgetFrom, BudgetTo, BorrowMaxPayment, BorrowDownPayment," . $doyouprefer . ", MustHave, ReallyHave, Flexible, NotWant, ClientNote, VehicleID) values (" . $SVPid . ",";
                            $query .= "'" . $_SESSION['searchplan_post']["specific$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["yearlist$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["makelist$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["modellist$i"] . "',";
                            $query .= "'" . $style[1] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["yeartext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["maketext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["modeltext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["styletext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["otherstyle$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["otheryear$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["vehicleneed$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["mileagefrom$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["mileageto$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["mileageceiling$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["transmission$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["drivetrain$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["extlike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["extdislike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["intlike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["intdislike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["budgetfrom$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["budgetto$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["borrowmaxpayment$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["borrowdownpayment$i"] . "',";
                            $query .= $doyouprefervalues . ",";
                            $query .= "'" . $_SESSION['searchplan_post']["musthave$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["reallyhave$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["flexible$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["notwant$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["clientnote$i"] . "',";
                            $query .= "'" . $style[0] . "'";
                            $query .= ")";
                            mysql_query($query, $con);
                        }
                        
                        if($_SESSION['searchplan_post']['reseller_status']==1){
                            $query_reseller ="UPDATE  `searchplans` SET  `researcher_flag` =  '1' WHERE  `searchplans`.`SearchPlanID` =".$SVPid;
                            mysql_query($query_reseller, $con);
                        }                       
                        unset($_SESSION['searchplan_post']);
                    } else if ($_POST) {   
                      
                        for ($i = 1; $i <= $_POST['number']; $i++) {
                            $style = explode(";", $_POST["stylelist$i"]);

                            if ($_POST["vehicletype$i"] == "Auto") {
                            //insert quert data
                                $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels';
                                $doyouprefervalues = "'" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "'";
                            } elseif ($_POST["vehicletype$i"] == "Minivan") {
                            //insert quert data
                                $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, PRHatch, BackupCamera';
                                $doyouprefervalues = "'" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "','" . $_POST["entertainmentsystem$i"] . "','" . $_POST["prhatch$i"] . "','" . $_POST["backupcamera$i"] . "'";
                            } elseif ($_POST["vehicletype$i"] == "SUV") {
                                $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, ThirdRD, CRow, PRHatch, BackupCamera, TPackage';
                                $doyouprefervalues = "'" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "','" . $_POST["entertainmentsystem$i"] . "','" . $_POST["thirdrs$i"] . "','" . $_POST["crow$i"] . "','" . $_POST["prhatch$i"] . "','" . $_POST["backupcamera$i"] . "','" . $_POST["tpackage$i"] . "'";
                            } elseif ($_POST["vehicletype$i"] == "Pickup") {
                                $doyouprefer = 'FrontSType, BedType, Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, RearWindow, BedLiner, BackupCamera, TPackage';
                                $doyouprefervalues = "'" . $_POST["frontstype$i"] . "','" . $_POST["bedtype$i"] . "','" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "','" . $_POST["rearwindow$i"] . "','" . $_POST["bedliner$i"] . "','" . $_POST["backupcamera$i"] . "','" . $_POST["tpackage$i"] . "'";
                            }
                            $query = "insert into searchplandetails (SearchPlanID, `Specific`, Year, Make, Model, Style, TextYear, TextMake, TextModel, TextStyle, OtherStyle, OtherYear, VehicleNeed, MileageFrom, MileageTo, MileageCeiling, Transmission, DriveTrain, ExtLike, ExtDislike, IntLike, IntDisike, BudgetFrom, BudgetTo, BorrowMaxPayment, BorrowDownPayment," . $doyouprefer . ", MustHave, ReallyHave, Flexible, NotWant, ClientNote, VehicleID) values (" . $SVPid . ",";
                            $query .= "'" . $_POST["specific$i"] . "',";
                            $query .= "'" . $_POST["yearlist$i"] . "',";
                            $query .= "'" . $_POST["makelist$i"] . "',";
                            $query .= "'" . $_POST["modellist$i"] . "',";
                            $query .= "'" . $style[1] . "',";
                            $query .= "'" . $_POST["yeartext$i"] . "',";
                            $query .= "'" . $_POST["maketext$i"] . "',";
                            $query .= "'" . $_POST["modeltext$i"] . "',";
                            $query .= "'" . $_POST["styletext$i"] . "',";
                            $query .= "'" . $_POST["otherstyle$i"] . "',";
                            $query .= "'" . $_POST["otheryear$i"] . "',";
                            $query .= "'" . $_POST["vehicleneed$i"] . "',";
                            $query .= "'" . $_POST["mileagefrom$i"] . "',";
                            $query .= "'" . $_POST["mileageto$i"] . "',";
                            $query .= "'" . $_POST["mileageceiling$i"] . "',";
                            $query .= "'" . $_POST["transmission$i"] . "',";
                            $query .= "'" . $_POST["drivetrain$i"] . "',";
                            $query .= "'" . $_POST["extlike$i"] . "',";
                            $query .= "'" . $_POST["extdislike$i"] . "',";
                            $query .= "'" . $_POST["intlike$i"] . "',";
                            $query .= "'" . $_POST["intdislike$i"] . "',";
                            $query .= "'" . $_POST["budgetfrom$i"] . "',";
                            $query .= "'" . $_POST["budgetto$i"] . "',";
                            $query .= "'" . $_POST["borrowmaxpayment$i"] . "',";
                            $query .= "'" . $_POST["borrowdownpayment$i"] . "',";
                            $query .= $doyouprefervalues . ",";
                            $query .= "'" . $_POST["musthave$i"] . "',";
                            $query .= "'" . $_POST["reallyhave$i"] . "',";
                            $query .= "'" . $_POST["flexible$i"] . "',";
                            $query .= "'" . $_POST["notwant$i"] . "',";
                            $query .= "'" . $_POST["clientnote$i"] . "',";
                            $query .= "'" . $style[0] . "'";
                            $query .= ")";   							echo $query;
                            mysql_query($query, $con);
                        }
                       if($_POST['reseller_status']==1){
                            $query_reseller ="UPDATE  `searchplans` SET  `researcher_flag` =  '1' WHERE  `searchplans`.`SearchPlanID` =".$SVPid;
                            mysql_query($query_reseller, $con);
                        } 
                       
                    }
					// Add a Message Update when this happens...
                    posttodashboard($con, $userid, $userid, 'completed a <a href="' . WEB_SERVER_NAME . 'searchplan.php">Vehicle Specification</a>.', $marketneedid);

                    if ($srep != -1) {
					// Add a Message when this happens...
                        posttodashboard($con, $userid, $srep, '<a href="' . WEB_SERVER_NAME . 'salesrepactions.php?ForUserID=' . $userid . '&MarketNeedID=' . $marketneedid . '">' . $firstname . ' ' . $lastname . '</a> added a Vehicle Specification.');
                        $message = 'Your customer has Added a Vehicle Specification...</br>';
                        $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                        sendemail(getuseremailnodb($con, $srep), 'VSP Received!', $message, 'true');

                        $message = 'Your VSP has been received.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, your sales representative will discuss options with you soon.';
                    } else {
                        $message = 'Your VSP has been received.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, a sales representative will be assigned to you soon to discuss your options.';
                    }

                    sendemail(getuseremailnodb($con, $userid), 'VSP Received', $message, 'false');

                    $message = 'A customer has Added a Vehicle Specification...</br>';
                    $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                    sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                }
            } else {


// There is already one saved, so update it...
                $query = "update searchplans set LastUpdated = '" . date_at_timezone('Y-m-d H:i:s', 'EST') . "', SeenBySales = 0 where MarketNeedID = " . $marketneedid;
                if ($_SESSION['searchplan_post']) {
                    for ($i = 1; $i <= $_SESSION['searchplan_post']['number']; $i++) {
                        $style = explode(";", $_SESSION['searchplan_post']["stylelist$i"]);

                        if ($_SESSION['searchplan_post']["vehicletype$i"] == "Auto") {
//insert quert data
                            $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels';
                            $doyouprefervalues = "'" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "'";
                        } elseif ($_SESSION['searchplan_post']["vehicletype$i"] == "Minivan") {
//insert quert data
                            $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, PRHatch, BackupCamera';
                            $doyouprefervalues = "'" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "','" . $_SESSION['searchplan_post']["entertainmentsystem$i"] . "','" . $_SESSION['searchplan_post']["prhatch$i"] . "','" . $_SESSION['searchplan_post']["backupcamera$i"] . "'";
                        } elseif ($_SESSION['searchplan_post']["vehicletype$i"] == "SUV") {
                            $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, ThirdRD, CRow, PRHatch, BackupCamera, TPackage';
                            $doyouprefervalues = "'" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "','" . $_SESSION['searchplan_post']["entertainmentsystem$i"] . "','" . $_SESSION['searchplan_post']["thirdrs$i"] . "','" . $_SESSION['searchplan_post']["crow$i"] . "','" . $_SESSION['searchplan_post']["prhatch$i"] . "','" . $_SESSION['searchplan_post']["backupcamera$i"] . "','" . $_SESSION['searchplan_post']["tpackage$i"] . "'";
                        } elseif ($_SESSION['searchplan_post']["vehicletype$i"] == "Pickup") {
                            $doyouprefer = 'FrontSType, BedType, Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, RearWindow, BedLiner, BackupCamera, TPackage';
                            $doyouprefervalues = "'" . $_SESSION['searchplan_post']["frontstype$i"] . "','" . $_SESSION['searchplan_post']["bedtype$i"] . "','" . $_SESSION['searchplan_post']["leather$i"] . "','" . $_SESSION['searchplan_post']["heatedseat$i"] . "','" . $_SESSION['searchplan_post']["navigation$i"] . "','" . $_SESSION['searchplan_post']["sunroof$i"] . "','" . $_SESSION['searchplan_post']["alloywheels$i"] . "','" . $_SESSION['searchplan_post']["rearwindow$i"] . "','" . $_SESSION['searchplan_post']["bedliner$i"] . "','" . $_SESSION['searchplan_post']["backupcamera$i"] . "','" . $_SESSION['searchplan_post']["tpackage$i"] . "'";
                        }

                        if (!isset($_SESSION['searchplan_post']["spdetailid$i"])) {
                            $query = "insert into searchplandetails (SearchPlanID, `Specific`, Year, Make, Model, Style, TextYear, TextMake, TextModel, TextStyle, OtherStyle, OtherYear, VehicleNeed, MileageFrom, MileageTo, MileageCeiling, Transmission, DriveTrain, ExtLike, ExtDislike, IntLike, IntDisike, BudgetFrom, BudgetTo, BorrowMaxPayment, BorrowDownPayment," . $doyouprefer . ", MustHave, ReallyHave, Flexible, NotWant, ClientNote, VehicleID) values (" . $plandata['SearchPlanID'] . ",";
                            $query .= "'" . $_SESSION['searchplan_post']["specific$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["yearlist$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["makelist$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["modellist$i"] . "',";
                            $query .= "'" . $style[1] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["yeartext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["maketext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["modeltext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["styletext$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["otherstyle$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["otheryear$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["vehicleneed$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["mileagefrom$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["mileageto$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["mileageceiling$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["transmission$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["drivetrain$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["extlike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["extdislike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["intlike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["intdislike$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["budgetfrom$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["budgetto$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["borrowmaxpayment$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["borrowdownpayment$i"] . "',";
                            $query .= $doyouprefervalues . ",";
                            $query .= "'" . $_SESSION['searchplan_post']["musthave$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["reallyhave$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["flexible$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["notwant$i"] . "',";
                            $query .= "'" . $_SESSION['searchplan_post']["clientnote$i"] . "',";
                            $query .= "'" . $style[0] . "'";
                            $query .= ")";
                        }
                        mysql_query($query, $con);
                    }
                      if($_SESSION['searchplan_post']['reseller_status']==1){
                            $query_reseller ="UPDATE  `searchplans` SET  `researcher_flag` =  '1' WHERE  `searchplans`.`SearchPlanID` =".$plandata['SearchPlanID'];
                            mysql_query($query_reseller, $con);
                        }               
                        unset($_SESSION['searchplan_post']);
                } else if ($_POST) {
                    if(isset($_POST['edit_mode']) && $_POST['edit_mode']==1){
                        $go_to_spec=1;
                    }

// Update/Add number of vehicles in the search plan
                    for ($i = 1; $i <= $_POST['number']; $i++) {
                        $style = explode(";", $_POST["stylelist$i"]);

                        if ($_POST["vehicletype$i"] == "Auto") {
//insert quert data
                            $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels';
                            $doyouprefervalues = "'" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "'";

//update query data
                            $updatedoyouprefer = ", Leather = '" . $_POST["leather$i"] . "'";
                            $updatedoyouprefer .= ", HeatedSeat = '" . $_POST["heatedseat$i"] . "'";
                            $updatedoyouprefer .= ", Navigation = '" . $_POST["navigation$i"] . "'";
                            $updatedoyouprefer .= ", SunRoof = '" . $_POST["sunroof$i"] . "'";
                            $updatedoyouprefer .= ", AlloyWheels = '" . $_POST["alloywheels$i"] . "'";
                        } elseif ($_POST["vehicletype$i"] == "Minivan") {
//insert quert data
                            $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, PRHatch, BackupCamera';
                            $doyouprefervalues = "'" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "','" . $_POST["entertainmentsystem$i"] . "','" . $_POST["prhatch$i"] . "','" . $_POST["backupcamera$i"] . "'";

//update query data
                            $updatedoyouprefer = ", Leather = '" . $_POST["leather$i"] . "'";
                            $updatedoyouprefer .= ", HeatedSeat = '" . $_POST["heatedseat$i"] . "'";
                            $updatedoyouprefer .= ", Navigation = '" . $_POST["navigation$i"] . "'";
                            $updatedoyouprefer .= ", SunRoof = '" . $_POST["sunroof$i"] . "'";
                            $updatedoyouprefer .= ", AlloyWheels = '" . $_POST["alloywheels$i"] . "'";
                            $updatedoyouprefer .= ", EntertainmentSystem = '" . $_POST["entertainmentsystem$i"] . "'";
                            $updatedoyouprefer .= ", PRHatch = '" . $_POST["prhatch$i"] . "'";
                            $updatedoyouprefer .= ", BackupCamera = '" . $_POST["backupcamera$i"] . "'";
                        } elseif ($_POST["vehicletype$i"] == "SUV") {
                            $doyouprefer = 'Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, EntertainmentSystem, ThirdRD, CRow, PRHatch, BackupCamera, TPackage';
                            $doyouprefervalues = "'" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "','" . $_POST["entertainmentsystem$i"] . "','" . $_POST["thirdrs$i"] . "','" . $_POST["crow$i"] . "','" . $_POST["prhatch$i"] . "','" . $_POST["backupcamera$i"] . "','" . $_POST["tpackage$i"] . "'";

//update query data
                            $updatedoyouprefer = ", Leather = '" . $_POST["leather$i"] . "'";
                            $updatedoyouprefer .= ", HeatedSeat = '" . $_POST["heatedseat$i"] . "'";
                            $updatedoyouprefer .= ", Navigation = '" . $_POST["navigation$i"] . "'";
                            $updatedoyouprefer .= ", SunRoof = '" . $_POST["sunroof$i"] . "'";
                            $updatedoyouprefer .= ", AlloyWheels = '" . $_POST["alloywheels$i"] . "'";
                            $updatedoyouprefer .= ", EntertainmentSystem = '" . $_POST["entertainmentsystem$i"] . "'";
                            $updatedoyouprefer .= ", ThirdRD = '" . $_POST["thirdrs$i"] . "'";
                            $updatedoyouprefer .= ", CRow = '" . $_POST["crow$i"] . "'";
                            $updatedoyouprefer .= ", PRHatch = '" . $_POST["prhatch$i"] . "'";
                            $updatedoyouprefer .= ", BackupCamera = '" . $_POST["backupcamera$i"] . "'";
                            $updatedoyouprefer .= ", TPackage = '" . $_POST["tpackage$i"] . "'";
                        } elseif ($_POST["vehicletype$i"] == "Pickup") {

                            $doyouprefer = 'FrontSType, BedType, Leather, HeatedSeat, Navigation, SunRoof, AlloyWheels, RearWindow, BedLiner, BackupCamera, TPackage';
                            $doyouprefervalues = "'" . $_POST["frontstype$i"] . "','" . $_POST["bedtype$i"] . "','" . $_POST["leather$i"] . "','" . $_POST["heatedseat$i"] . "','" . $_POST["navigation$i"] . "','" . $_POST["sunroof$i"] . "','" . $_POST["alloywheels$i"] . "','" . $_POST["rearwindow$i"] . "','" . $_POST["bedliner$i"] . "','" . $_POST["backupcamera$i"] . "','" . $_POST["tpackage$i"] . "'";

//update query data
                            $updatedoyouprefer = ", FrontSType = '" . $_POST["frontstype$i"] . "'";
                            $updatedoyouprefer .= ", BedType = '" . $_POST["bedtype$i"] . "'";
                            $updatedoyouprefer .= ", Leather = '" . $_POST["leather$i"] . "'";
                            $updatedoyouprefer .= ", HeatedSeat = '" . $_POST["heatedseat$i"] . "'";
                            $updatedoyouprefer .= ", Navigation = '" . $_POST["navigation$i"] . "'";
                            $updatedoyouprefer .= ", SunRoof = '" . $_POST["sunroof$i"] . "'";
                            $updatedoyouprefer .= ", AlloyWheels = '" . $_POST["alloywheels$i"] . "'";
                            $updatedoyouprefer .= ", RearWindow = '" . $_POST["rearwindow$i"] . "'";
                            $updatedoyouprefer .= ", BedLiner = '" . $_POST["bedliner$i"] . "'";
                            $updatedoyouprefer .= ", BackupCamera = '" . $_POST["backupcamera$i"] . "'";
                            $updatedoyouprefer .= ", TPackage = '" . $_POST["tpackage$i"] . "'";
                        }

                        if (!isset($_POST["spdetailid$i"])) {
                            $query = "insert into searchplandetails (SearchPlanID, `Specific`, Year, Make, Model, Style, TextYear, TextMake, TextModel, TextStyle, OtherStyle, OtherYear, VehicleNeed, MileageFrom, MileageTo, MileageCeiling, Transmission, DriveTrain, ExtLike, ExtDislike, IntLike, IntDisike, BudgetFrom, BudgetTo, BorrowMaxPayment, BorrowDownPayment," . $doyouprefer . ", MustHave, ReallyHave, Flexible, NotWant, ClientNote, VehicleID) values (" . $plandata['SearchPlanID'] . ",";
                            $query .= "'" . $_POST["specific$i"] . "',";
                            $query .= "'" . $_POST["yearlist$i"] . "',";
                            $query .= "'" . $_POST["makelist$i"] . "',";
                            $query .= "'" . $_POST["modellist$i"] . "',";
                            $query .= "'" . $style[1] . "',";
                            $query .= "'" . $_POST["yeartext$i"] . "',";
                            $query .= "'" . $_POST["maketext$i"] . "',";
                            $query .= "'" . $_POST["modeltext$i"] . "',";
                            $query .= "'" . $_POST["styletext$i"] . "',";
                            $query .= "'" . $_POST["otherstyle$i"] . "',";
                            $query .= "'" . $_POST["otheryear$i"] . "',";
                            $query .= "'" . $_POST["vehicleneed$i"] . "',";
                            $query .= "'" . $_POST["mileagefrom$i"] . "',";
                            $query .= "'" . $_POST["mileageto$i"] . "',";
                            $query .= "'" . $_POST["mileageceiling$i"] . "',";
                            $query .= "'" . $_POST["transmission$i"] . "',";
                            $query .= "'" . $_POST["drivetrain$i"] . "',";
                            $query .= "'" . $_POST["extlike$i"] . "',";
                            $query .= "'" . $_POST["extdislike$i"] . "',";
                            $query .= "'" . $_POST["intlike$i"] . "',";
                            $query .= "'" . $_POST["intdislike$i"] . "',";
                            $query .= "'" . $_POST["budgetfrom$i"] . "',";
                            $query .= "'" . $_POST["budgetto$i"] . "',";
                            $query .= "'" . $_POST["borrowmaxpayment$i"] . "',";
                            $query .= "'" . $_POST["borrowdownpayment$i"] . "',";
                            $query .= $doyouprefervalues . ",";
                            $query .= "'" . $_POST["musthave$i"] . "',";
                            $query .= "'" . $_POST["reallyhave$i"] . "',";
                            $query .= "'" . $_POST["flexible$i"] . "',";
                            $query .= "'" . $_POST["notwant$i"] . "',";
                            $query .= "'" . $_POST["clientnote$i"] . "',";
                            $query .= "'" . $style[0] . "'";
                            $query .= ")";
                        } else {
                            $query = "update searchplandetails set `Specific` = '" . $_POST["specific$i"] . "'";
                            $query .= ", Year = '" . $_POST["yearlist$i"] . "'";
                            $query .= ", Make = '" . $_POST["makelist$i"] . "'";
                            $query .= ", Model = '" . $_POST["modellist$i"] . "'";
                            $query .= ", Style = '" . $style[1] . "'";
                            $query .= ", TextYear = '" . $_POST["yeartext$i"] . "'";
                            $query .= ", TextMake = '" . $_POST["maketext$i"] . "'";
                            $query .= ", TextModel = '" . $_POST["modeltext$i"] . "'";
                            $query .= ", TextStyle = '" . $_POST["styletext$i"] . "'";
                            $query .= ", OtherStyle = '" . $_POST["otherstyle$i"] . "'";
                            $query .= ", OtherYear = '" . $_POST["otheryear$i"] . "'";
                            $query .= ", VehicleNeed = '" . $_POST["vehicleneed$i"] . "'";
                            $query .= ", MileageFrom = '" . $_POST["mileagefrom$i"] . "'";
                            $query .= ", MileageTo = '" . $_POST["mileageto$i"] . "'";
                            $query .= ", MileageCeiling = '" . $_POST["mileageceiling$i"] . "'";
                            $query .= ", Transmission = '" . $_POST["transmission$i"] . "'";
                            $query .= ", DriveTrain = '" . $_POST["drivetrain$i"] . "'";
                            $query .= ", ExtLike = '" . $_POST["extlike$i"] . "'";
                            $query .= ", ExtDislike = '" . $_POST["extdislike$i"] . "'";
                            $query .= ", IntLike = '" . $_POST["intlike$i"] . "'";
                            $query .= ", IntDisike = '" . $_POST["intdislike$i"] . "'";
                            $query .= ", BudgetFrom = '" . $_POST["budgetfrom$i"] . "'";
                            $query .= ", BudgetTo = '" . $_POST["budgetto$i"] . "'";
                            $query .= ", BorrowMaxPayment = '" . $_POST["borrowmaxpayment$i"] . "'";
                            $query .= ", BorrowDownPayment = '" . $_POST["borrowdownpayment$i"] . "'";
                            $query .= $updatedoyouprefer;
                            $query .= ", MustHave = '" . $_POST["musthave$i"] . "'";
                            $query .= ", ReallyHave = '" . $_POST["reallyhave$i"] . "'";
                            $query .= ", Flexible = '" . $_POST["flexible$i"] . "'";
                            $query .= ", NotWant = '" . $_POST["notwant$i"] . "'";
                            $query .= ", ClientNote = '" . $_POST["clientnote$i"] . "'";
                            $query .= ", VehicleID = '" . $style[0] . "'";
                            $query .= " where SearchPlanDetailID = " . $_POST["spdetailid$i"];
                        }
//echo '<br>'.$query.'<br><br><br>';
                        mysql_query($query, $con);
                        
                        if($_POST['reseller_status']==1){
                            $query_reseller ="UPDATE  `searchplans` SET  `researcher_flag` =  '1' WHERE  `searchplans`.`SearchPlanID` =".$plandata['SearchPlanID'];
                            mysql_query($query_reseller, $con);
                        }
                    }
                }
// Add a Message Update when this happens...
                posttodashboard($con, $userid, $userid, 'updated their <a href="' . WEB_SERVER_NAME . 'searchplan.php">Vehicle Specification.</a>', $marketneedid);

                if ($srep != -1) {
// Add a Message when this happens...
                    posttodashboard($con, $userid, $srep, '<a href="' . WEB_SERVER_NAME . 'salesrepactions.php?ForUserID=' . $userid . '&MarketNeedID=' . $marketneedid . '">' . $firstname . ' ' . $lastname . '</a> updated their Vehicle Specification Information.');

                    $message = 'Your customer has Updated their VSP Information...</br>';
                    $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                    sendemail(getuseremailnodb($con, $srep), 'VSP Updated!', $message, 'true');
                }

                $message = 'A customer has Updated their VSP...</br>';
                $message .= '&nbsp;&nbsp;Customer: ' . getuserfullnamenodb($con, $userid, 'false') . '</br>';
                $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;' . getuserphonenodb($con, $userid) . '</br>';
                sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
            }

            mysql_close($con);
        } else
            $errorinsave = 'Could not connect to the database';
    }
    ?>
    <?php 
    require("headerstart.php"); ?>
    <?php require("header.php"); ?>
    <?php require("foursteps.php"); 
    if(isset($go_to_spec) && $go_to_spec==1){
        echo "<script>window.location='vehiclespec.php';</script>";
    }
    
    ?>

   <!-- <div class="gridtwelve"></div>-->
    <div id="content">
        <div class="grideightcontainer" style='text-align: center;'>
    <?php
    if ($errorinsave != 'false') {
        echo '<h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">VSP Issue!</h1>';
    } else {
        echo '<h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">WELCOME TO 1-800-VEHICLES.COM!</h1>';
    }
    ?>
            <div class="grideightgrey vehiclespec-background">
                <div class="grideight">
    <?php
    if ($errorinsave != 'false') {
        echo '<p class="blacktwelve">Sorry!  There was an error processing your VSP.</p>';
        echo '<p class="blacktwelve">Please contact a representative at 1-800-vehicles (834-4253) for help with this issue.</p>';
        if ($errorinsave != 'true')
            echo $errorinsave . '<br/>';
        echo '<p class="blacktwelve">Use your browser <a href="javascript:history.back()">back</a> button to add missing fields or to start over click <a href="' . WEB_SERVER_NAME . 'searchplan.php">here</a>.</p>';
//echo $query.'<br/>';
    }
    else {
        echo '
		<div class="vehiclespec-save">
		
		 <div class="col-xs-6 col-sm-6 vehiclespec-save-mobile" style="padding:10px 0px;">
		
		<p class="blacktwelve vehiclespec-title">A 1-800-vehicles.com representative will be assigned to you shortly</p>
		<p class="blacktwelve"></br></br><a href="add_vehicle_to_searchplan.php" style="color: white">
               
                CONTINUE MAKING A SEARCH PLAN
            </a></br></br> <a href="dashboard.php" style="color: white">
             
                GO TO YOUR DASHBOARD
              </a>      </p>
		
		
		</div>	
		<div class="clear"></div>
			
		</div>
		
		';
		
		
		
		// echo '<p class="blacktwelve">A 1-800-vehicles.com representative will be assigned to shortly. You will receive an introductory email.</p>';
        
       //  echo '<p class="blacktwelve"><h3>PLEASE:<h3></p>';
      //   echo '<p class="blacktwelve"><a href="add_vehicle_to_searchplan.php" style="color: white">
     //          <span style="background-color: rgb(145, 214, 21) ! important; text-align: center; width: 48%; padding: 10px; font-size: 12px;">
      //           CONTINUE BUILDING YOUR SEARCH
       //        </span>
      //                    </a></p>';
     //    echo '<p class="blacktwelve">or</p>';
         
       //  echo '<p class="blacktwelve"><a href="dashboard.php" style="color: white">
        //       <span style="background-color: rgb(145, 214, 21) ! important; text-align: center; width: 48%; padding: 10px; font-size: 12px;">
         //         GO TO YOUR DASHBOARD
         //      </span>
        //                  </a></p>';
          
        // echo '<p class="blacktwelve">To change your Vehicle Spec, you can revisit the <a href="' . WEB_SERVER_NAME . 'vehiclespec.php">Vehicle Spec</a> page at any time.</p>';
//if($errorinsave != 'true') echo $errorinsave.'<br/>';
//echo $query.'<br/>';
    }
    ?>
                    
                    
                    
                    
                </div><!-- end greyeight-->
            </div><!-- grid eight container -->
        </div><!-- end grideightgrey-->
        
        
      
    <?php require("teaser.php"); ?>
    </div><!--end content--><div class="query" style="display: none;"><?php echo $query; ?></div>
<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); } ?>
