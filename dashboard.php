<?php require("globals.php"); ?>
<?php

$_SESSION['state'] = 0;
$_SESSION['substate'] = 100;
$_SESSION['titleadd'] = 'Dashboard';

unset($_SESSION['ACTIVE_DASHBOARD']);


$userid = $_SESSION['userid'];
$marketneedid = $_SESSION['marketneedid'];

$ucurrentstep = currentstep();
$usalesrep = getsalesrep($userid, $marketneedid);
$ucust = getuserprofile($userid, 'Customer');
$hasaccess = ismorethancust($userid);
$uadmin = getuserprofile($userid, 'Administrator');
$uterr = getuserprofile($userid, 'Teritory Admin');
$ufran = getuserprofile($userid, 'Franchisee');
$ugm = getuserprofile($userid, 'General Manager');
$uops = getuserprofile($userid, 'Operations Manager');
$ubuyer = getuserprofile($userid, 'Researcher');
$usrep = getuserprofile($userid, 'Sales Representative');

// echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";
//    
$con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
if ($con) {
    mysql_select_db(DB_SERVER_DATABASE, $con);

    // Get the current User Information...
    $query = 'select u.firstname, u.lastname, u.created, u.imagefile from users u where u.userid = ' . $userid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $infirst = $row[0];
        $inlast = $row[1];
        $increated = $row[2];
        $inimage = $row[3];
    }
    $have_vehicle_spec=0;
    $have_vehicle_assiamsnt=0;
    
    $chkqueryvehiclespec = "select SearchPlanID from searchplans where MarketNeedID = " . $marketneedid;
    $resultVehicleSpec = mysql_query($chkqueryvehiclespec, $con);
    $countVehicleSpec = mysql_num_rows($resultVehicleSpec);
    if($countVehicleSpec){
        $have_vehicle_spec=1;
    }

    $chkqueryvehicleassisment = 'select * from assessments where MarketNeedID = ' . $marketneedid;
    $resultVehicleAssis = mysql_query($chkqueryvehicleassisment, $con);
    $countVehicleAssis = mysql_num_rows($resultVehicleAssis);
    if($countVehicleAssis){
        $have_vehicle_assiamsnt=1;
    }


    $queryMyBestVehicleAssessment = 'SELECT MarketNeedID  FROM `assessments` WHERE `MarketNeedID` =' . $marketneedid;
    $resultMyBestVehicleAssessment = mysql_query($queryMyBestVehicleAssessment, $con);
    $MyBestVehicleAssessment = mysql_num_rows($resultMyBestVehicleAssessment);


    $query_MypriceAvailabilityStudy = "select SearchPlanID from pasplans where MarketNeedID = " . $marketneedid;
    $result_MypriceAvailabilityStudy = mysql_query($query_MypriceAvailabilityStudy, $con);
    $MypriceAvailabilityStudy = mysql_num_rows($result_MypriceAvailabilityStudy);


    $query_MyFavorites = "select FavoriteID from favorites where MarketNeedID =" . $marketneedid;
    $result_MyFavorites = mysql_query($query_MyFavorites, $con);
    $MyFavorites = mysql_num_rows($result_MyFavorites);


    $query_MyTradeinQuote = "SELECT MarketNeedID  FROM `tradeinrequests` WHERE `MarketNeedID` =" . $marketneedid;
    $result_MyTradeinQuote = mysql_query($query_MyTradeinQuote, $con);
    $MyTradeinQuote = mysql_num_rows($result_MyTradeinQuote);



    $chkquery = "select SearchPlanID, notes, search_post from searchplans where MarketNeedID = " . $marketneedid;
    $planresult = mysql_query($chkquery, $con);

    $row11 = mysql_fetch_array($planresult);
    $have_search_plan = 0;

    if (isset($row11['SearchPlanID']) && $row11['SearchPlanID'] > 0) {
        if ($row11['search_post'] == 1) {
            $have_search_plan = 1;
        }
    }
}
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<style>
    table { border: none; }
    td a {
        display: inline-block;
        height:100%;
        width:100%;
    }
    .sub-header {
        padding-bottom: 10px;
        border-bottom: 1px solid #eee;
    }

    .grideightcontainer table a {
        color: #fff;
        font-weight: normal;
    }


    .nav.navbar-nav.navbar-right {
        background-color: transparent;
    }

    #tableother-link a{
        color: #5FB796;
    }

    .dashboardmenu a{
        color: #00000;
    }

    .tdactive {
        background-color: #92d050;
    }
    .header-menu a{
        width: 100%;
        color: #5FB796;
    }
    /*td, th {
      border: solid 1px black;
    }*/


</style>

<script type="text/javascript">

    $(document).ready(function() {
        $("#youtube1").click(function() {
            $.colorbox({href: "dashboard_video.php", scrolling: false, width: "90%"});
        });
    });
</script>

<!--<div class="gridtwelve"></div>-->

<div id="content" class="dashboard-content">


    <div class="grideightcontainer"> 


        <!--  <div class="col-xs-12 col-sm-12"> 
                
                 <div class="col-xs-6 col-sm-6">
                     <img src="images/number.png">
                 </div> -->

        <!--    <div class="col-xs-12 col-sm-12" style="margin-bottom: 10px;">
<?php
if (isset($_SESSION['user'])) {
    ?>  
            <div  class="header-menu">              
                <div style="float: right">
                    <a href="#">My Dashboard <span>|</span></a>
                    <a href="myaccount.php">My Account <span>|</span></a>
                    <a href="logout.php" id="logout">Logout </a>
                </div> 
            </div>
        <?php } ?>
             </div>
         </div>-->
        <!--<div class="breaker"></div>
       

       <br>
       <br>-->

        <div class="col-xs-12 col-sm-12 dashboard-title">
            <h3>My Dashboard</h3>
        </div> 

        <div style="width: 100%">              
            <table class="table secondmenu-table">
                <thead>
                    <tr>


                        <td align="center" class="tdactive"><a href="dashboard.php">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <span class="fa-stack-lg step-text">1</span>
                                </span><?php if ($countVehicleSpec) {
            echo "GET A SEARCH STARTED";
        } else {
            echo "GET HELP WITH WHAT TO BUY";
        } ?>
                            </a></td>



                        <!--<?php if ($_SERVER[REQUEST_URI] != "/dashboard.php") {
            ?>
                            <span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <span class="fa-stack-lg step-text">1</span>
                            </span>-->

                            <!--<span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-lg"></i>
                              <span class="fa-stack-1x step-text">2</span>
                            </span>-->


                            <!--START THE SEARCH PROCESS</a></td>
<?php
} else {
    $_SESSION['ACTIVE_DASHBOARD'] = "dashboard.php";
}
?>  -->      

                        <!--<td align="center"><a href="get_a_search_started.php">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <span class="fa-stack-lg step-text">2</span>
                        </span>
                        GET A SEARCH STARTED</a></td>-->

                        <td align="center"><a href="consider_and_approve.php">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <span class="fa-stack-lg step-text">2</span>
                                </span>
                                CONSIDER & APPROVE</a></td>


                        <td align="center"><a href="inspect_and_takedelivery.php">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <span class="fa-stack-lg step-text">3</span>
                                </span>
                                INSPECT & TAKE DELIVERY</a></td>


                    </tr>
                </thead>   
            </table>
        </div>





        <!--<?php
if (strlen($inimage) > 0)
    $srctext = $inimage;
else
    $srctext = "userimages/nopicture.jpg";
?>   -->         

        <!--<div class="col-xs-12 col-sm-12">-->
            <!--<a href="#" id="youtube1"><img src="images/play.png"/></a>
            <div class="breaker"></div>-->

        <!--</div> -->

        <!--  <div class="col-xs-12 col-sm-12">
<?php
if (isset($_SESSION['user'])) {
    ?> 
                 <div style="float: right;"> 
                  <img class="img-responsive" style="border-radius: 14px; width: 60px;" src="<?= $srctext ?>">
                  <div class="breaker"></div>
                  <span ><?= $infirst . ' ' . $inlast ?></span> 
                 </div>
<?php } ?>
            
              </div>
      </div>-->


        <div class="col-xs-12 col-sm-12 about-this-video dashboard-video" style="margin-bottom: 20px;">
             <!--<a href="#" id="youtube1"><img src="images/play.png"/><span>About this Step</span></a>
                
            --> 


        </div>


        <div class="col-xs-12 col-sm-12 user-avatar-container" style="margin-bottom: 20px;">
            <span class="dashboard-title-mobile pull-left">MY DASHBOARD</span>
            <span>
<?php if (isset($_SESSION['user'])) { ?> 
                    <div class="user-info user-avatar pull-right"> 
                        <img class="user-img img-responsive pull-right" src="<?= $srctext ?>">
                        <span class="pull-right"><?= $infirst . ' ' . $inlast ?></span> 
                    </div>
<?php } ?>
            </span>
        </div>




        <div style="clear: both;"></div>
<?php if ($hasaccess == "true") { ?>
            <div class="dashboardmenu">
                <div class="col-sm-12">          
                    <div class="row placeholders">                                                
                        <div class="col-xs-4 col-sm-3 placeholder">
                            <a href="<?php echo WEB_SERVER_NAME . 'mydashboard.php'; ?>"> <div class="divcenter">ACTION ITEM</div> </a>
                        </div>
                        <div class="col-xs-4 col-sm-3 placeholder">
                            <a href="<?php echo WEB_SERVER_NAME . 'myaccount.php'; ?>"> <div class="divcenter">My Account</div> </a>                   
                        </div>
                        <div class="col-xs-4 col-sm-3 placeholder">
                            <a href="<?php echo WEB_SERVER_NAME . '/groupwatch.php'; ?>"> <div class="divcenter">Group Watchlist</div> </a>                   
                        </div>
                        <div class="col-xs-4 col-sm-3 placeholder">                   
                            <a href="<?php echo WEB_SERVER_NAME . 'mywholesale.php'; ?>"> <div class="divcenter">My Wholesale Inventory</div> </a>
                        </div>
                        <div class="col-xs-4 col-sm-3 placeholder">               
                            <a href="<?php echo WEB_SERVER_NAME . 'allwholesale.php'; ?>"> <div class="divcenter">All Wholesale Inventory</div> </a>
                        </div>



    <?php if ($uadmin == "true") { ?>

                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'franchiseesetup.php'; ?>"> <div class="divcenter">Franchisee Setup</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'userfunctions.php'; ?>"> <div class="divcenter">User Setup</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'recentcustlist.php'; ?>"> <div class="divcenter">Recent Customer List</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . ''; ?>"> <div class="divcenter">Email Export</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'allvehicles.php'; ?>"> <div class="divcenter">Vehicle Setup</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'virtualvehicles.php'; ?>"> <div class="divcenter">Virtual Vehicle Mass Changes</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'virtualvehiclegroups.php'; ?>"> <div class="divcenter">Virtual Vehicle Groups</div> </a>
                            </div>
    <?php } else if ($ufran == "true" && $ugm == "false") { ?>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'myaccount.php'; ?>"> <div class="divcenter">Update My Account</div> </a>

                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">                   
                                <a href="<?php echo WEB_SERVER_NAME . 'viewagreements.php'; ?>"> <div class="divcenter">View Agreements</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">                   
                                <a href="<?php echo WEB_SERVER_NAME . 'assessment.php'; ?>"> <div class="divcenter">View My Quick Assessment</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">                   
                                <a href="<?php echo WEB_SERVER_NAME . 'favorites.php'; ?>"> <div class="divcenter">View My Favorites</div> </a>
                            </div>                    

    <?php } else if ($usrep == "true" && $ugm == "false") { ?>
                            <div class="col-xs-4 col-sm-3 placeholder">
                                <a href="<?php echo WEB_SERVER_NAME . 'myaccount.php'; ?>"> <div class="divcenter">Update My Account</div> </a>

                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">                   
                                <a href="<?php echo WEB_SERVER_NAME . 'viewagreements.php'; ?>"> <div class="divcenter">View Agreements</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">                   
                                <a href="<?php echo WEB_SERVER_NAME . 'assessment.php'; ?>"> <div class="divcenter">View My Quick Assessment</div> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder">                   
                                <a href="<?php echo WEB_SERVER_NAME . 'favorites.php'; ?>"> <div class="divcenter">View My Favorites</div> </a>
                            </div>                    
    <?php } ?>

                    </div>
                </div>
            </div> 



<?php
} else {
    
    if($have_vehicle_spec==0 && $have_vehicle_assiamsnt==0){
         echo "<script>window.location='add_vehicle_spec.php';</script>";
    }
    
    if($have_search_plan==1){
        echo "<script>window.location='consider_and_approve.php';</script>";
    }
    $user_step='1a';
    if($have_vehicle_spec==1 && $have_vehicle_assiamsnt==1){
        $user_step='1b';
    }else if($have_vehicle_spec==0 && $have_vehicle_assiamsnt==1){
         $user_step='1b';
    }else if($have_vehicle_spec==1 && $have_vehicle_assiamsnt==0){
         $user_step='1a';
    }
    
    ?>
            <div class="dashboardmenu">
                <div class="col-sm-12">  
                    <div class="row placeholders">  

            <?php if ($user_step=='1a') { ?>
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'add_vehicle_spec.php'; ?>"> <img class="img-responsive"  alt="" src="images/enter_another_vehicle_specification.png"> </a>
                            </div>

                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a  id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'vehiclespec.php'; ?>"> <img class="img-responsive"  alt="" src="images/all_vehicle_specifications_&_quotes.png"> </a>
                            </div>


                            <!--  <div class="col-xs-4 col-sm-3 placeholder">
                                  <a href="<?php echo WEB_SERVER_NAME . 'mysearchplan.php'; ?>"> <div class="divcenter">EDIT MY SEARCH PLAN </div> </a>
                              </div>-->
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a  id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'mysearchplan.php'; ?>"> <img class="img-responsive"  alt="" src="images/edit_my_search_plan.png"> </a>
                            </div>



                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="consult.php"> 
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/request.png">
                                </a>
                            </div> 
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="support.php">  
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/support.png">
                                </a>
                            </div> 


    <?php } else if ($user_step=='1b') { ?>
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'assessment.php'; ?>"> <img class="img-responsive"  alt="" src="images/edit_my_vehicle_needs_assessment.png"> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'mybestvehicles.php'; ?>"> <img class="img-responsive"  alt="" src="images/view_our_best_vehicle_recommendations.png"> </a>
                            </div>
                            <!--<div class="col-xs-4 col-sm-3 placeholder">
                               <a href="<?php echo WEB_SERVER_NAME . 'mysearchplan.php'; ?>"> <div class="divcenter">EDIT MY SEARCH PLAN </div> </a>
                           </div>-->
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a  id="dashboardimage" class="img-responsive" href="<?php echo WEB_SERVER_NAME . 'mysearchplan.php'; ?>"> <img class="img-responsive"  alt="" src="images/edit_my_search_plan.png"> </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="researchvehicles.php"> 
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/research_prices_&_availability.png">
                                </a>
                            </div> 

                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="consult.php"> 
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/request.png">
                                </a>
                            </div> 
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="support.php">  
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/support.png">
                                </a>
                            </div> 

    <?php } else { ?>

                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="<?php echo WEB_SERVER_NAME . 'assessment.php'; ?>"> 
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/define.png">
                                </a>
                            </div>  
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="consult.php"> 
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/request.png">
                                </a>
                            </div> 
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a title="Newsweek"  href="researchvehicles.php">
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/research.png">
                                </a>
                            </div>
                            <!--<div class="dashboardcrear"></div>-->
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="<?php if (!isset($_SESSION['user'])) {
            echo "add_pas.php";
        } else {
            echo "pas.php";
        } ?>">  
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/study.png">
                                </a>
                            </div>    

                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="<?php echo WEB_SERVER_NAME . 'requesttradeinquote.php'; ?>">
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/quote.png">
                                </a>
                            </div>

                            <!--<?php if (isset($_SESSION['user'])) { ?>
                                <div class="col-xs-6 col-md-4 placeholder">
                                     <a href="searchplan.php"> <div style=" " class="divcenter">DESCRIBE THE VEHICLE YOU WANT</div> </a>
                                </div>
                
                                 <div class="col-xs-6 col-md-4 placeholder">
                                      <a href="add_searchplan.php"> <div style=" " class="divcenter">POST A VEHICLE SEARCH</div> </a>
                                 </div> 
        <?php } ?>-->
                            <div class="col-xs-4 col-sm-3 placeholder dashboard-icon">
                                <a href="#">  
                                    <img id="dashboardimage" class="img-responsive"  alt="" src="images/support.png">
                                </a>
                            </div> 
    <?php } ?>
                    </div>
                </div>
            </div>
<?php } ?>



<?php
if (isset($_SESSION['user']) && $hasaccess == "false") {
    ?>
            <!--<div class="row" style="margin-left: 1%;text-align: center; width: 98%;">
             <div class="col-xs-12" style="background-color: #404040;padding: 8px;">
                <strong style="color: #fff;">VIEW:</strong>
             </div> 
    <?php if ($MyBestVehicleAssessment) { ?>
                  <div class="col-xs-6 col-xs-height common_action" >
                    <a href="assessment.php"> My Best Vehicle Assessment </a>
                 </div>
    <?php } ?>  
                
            <?php if ($countVehicleAssis) { ?>
                 <div class="col-xs-6 col-xs-height common_action" >
                     <a href="pas.php">  Our Recommendations for you </a>
                 </div>
    <?php } ?>
            <?php if ($MypriceAvailabilityStudy) { ?>            
                 <div class="col-xs-6 col-xs-height common_action">
                      <a href="pas.php"> My  Price & Availability Studies </a>
                 </div>
    <?php } ?> 
                
            <?php if ($MyTradeinQuote) { ?>  
                  <div class="col-xs-6 col-xs-height common_action">
                      <a href="requesttradeinquote.php">  My Trade in Quote </a>
                 </div>
    <?php } ?> 
                
            <?php if ($MyFavorites) { ?>  
                 <div class="col-xs-6 col-xs-height common_action">
                     <a href="favorites.php"> My Favorites </a>
                 </div>
    <?php } ?> 
               <div class="col-xs-6 col-xs-height common_action">
                  <a href="viewagreements.php">  View Our Guarantee </a>
             </div>
                
    <?php if ($MyVehicleSpecifications) { ?>  
                 <div class="col-xs-6 col-xs-height common_action">
                      <a href="searchplan.php"> My Vehicle Specifications </a>
                 </div>
    <?php } ?> 
           </div>-->

    <?php
}
?>

        <?php if (isset($_SESSION['user']) && $user_step=='1a') { ?> 
            <div class="row view-dashboard" style="margin-left: 1%;text-align: center; width: 98%;">
                <div class="col-xs-12" style="background-color: #404040;padding: 8px;">
                    <strong style="color: #fff;">VIEW:</strong>
                </div> 
                <!--<div class="col-xs-6 col-xs-height common_action">
                    <a href="requesttradeinquote.php">  My Trade in Quote </a>
                </div>-->
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="favorites.php"> My Favorites </a>
                </div>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="viewagreements.php">  View Our Guarantee </a>
                </div>
                <div class="col-xs-6 col-xs-height common_action">
                    <a href="myaccount.php" id="myaccount">My Account</a>
                </div> 
<?php } else if (isset($_SESSION['user']) && $user_step=='1b') { ?>
                <div class="row view-dashboard" style="margin-left: 1%;text-align: center; width: 98%;">
                    <div class="col-xs-12" style="background-color: #404040;padding: 8px;">
                        <strong style="color: #fff;">VIEW:</strong>
                    </div> 
                    <div class="col-xs-6 col-xs-height common_action">
                        <a href="assessment.php">  My Vehicle Needs Assessment </a>
                    </div>
                    <div class="col-xs-6 col-xs-height common_action">
                        <a href="vehiclespec.php">All Vehicle Specifications & Quotes</a>
                    </div>
                   <!-- <div class="col-xs-6 col-xs-height common_action">
                        <a href="requesttradeinquote.php">  My Trade in Quote </a>
                    </div>-->
                    <div class="col-xs-6 col-xs-height common_action">
                        <a href="favorites.php"> My Favorites </a>
                    </div>
                    <div class="col-xs-6 col-xs-height common_action">
                        <a href="viewagreements.php">  View Our Guarantee </a>
                    </div>
                    <div class="col-xs-6 col-xs-height common_action">
                        <a href="myaccount.php" id="myaccount">My Account</a>
                    </div>


<?php } ?>
            </div>
                <?php require("teaser.php"); ?>
        </div><!--end content-->


<?php require("footerstart.php"); ?>
        <?php require("footer.php"); ?>
        <?php require("footerend.php"); ?>
