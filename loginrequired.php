<?php require("globals.php"); ?>
<?php
    $_SESSION['laststate'] = $_SESSION['state'];
    $_SESSION['lastsubstate'] = $_SESSION['substate'];
    $_SESSION['state'] = 0;
    $_SESSION['substate'] = 7;
    $_SESSION['titleadd'] = 'Login Required';
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>


<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="  text-align: left;width: 100%;margin-left: 0;">Login Required</h1>
        <div class="grideightgrey">
            <p class="blacktwelve" style="min-height:300px;font-size: 14px">
                <b> In order to access that area of the site, you must be a registered user.</b> <br>
                <b>Click <a href="my_login.php">here</a> to Login.</b>
            </p>
        </div><!-- end greyeightgrey-->
    </div><!-- grid eight container -->
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
