
<?php require("globals.php"); ?>
<?php 
require_once(WEB_ROOT_PATH . 'common/functions/string.php');
$_SESSION['state'] = 5;
$_SESSION['substate'] = 9;
$_SESSION['titleadd'] = 'Customer Review';

if (!isset($_REQUEST['ForUserID']) || !isset($_REQUEST['MarketNeedID'])) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Missing Parameters';
    header('Location: mydashboard.php#admintab');
    exit();
}

$userid = $_SESSION['userid'];
if (!isset($userid)) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - UserID not set';
    header('Location: mydashboard.php#admintab');
    exit();
}




$inuserid = $_REQUEST['ForUserID'];
$infranid = isset($_REQUEST['Franchisee']) ? $_REQUEST['Franchisee'] : null;
$inmneed = $_REQUEST['MarketNeedID'];
$instep = getuserstep($inuserid, $inmneed);

// Verify this user has access to the page...
$uadmin = getuserprofile($userid, 'Administrator');
$uterr = getuserprofile($userid, 'Teritory Admin');
$ufran = getuserprofile($userid, 'Franchisee');
$ugm = getuserprofile($userid, 'General Manager');
$usrep = getuserprofile($userid, 'Sales Representative');
if (!(($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true') || ($usrep == 'true'))) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000508 - Permissions Error for ' . $userid;
    header('Location: mydashboard.php#admintab');
    exit();
}

// Verify this user has access to this customer...  (ADMINS have rights to all customers)
if ($uadmin != 'true') {
    if (($usrep == 'true') && ($userid != getsalesrep($inuserid, $inmneed))) {
        $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Not Authorized to View Customer!';
        header('Location: mydashboard.php#admintab');
        exit();
    }
}

// Check that the customer and market need match as well (they could have typed it on the line)...
if (isvalid($inuserid, $inmneed) == 0) {
    $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Invalid Customer Selection!  c=' . $inuserid . ' m=' . $inmneed;
    header('Location: mydashboard.php#admintab');
    exit();
}

//$errormessage = 'false';
$con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
if ($con) {
    mysql_select_db(DB_SERVER_DATABASE, $con);

// Verify this user has access to the customer...   (ADMIN override)
    if ($uadmin != 'true') {
        $query = "select m.MarketNeedID from assignedreps a,marketneeds m where m.MarketNeedID=a.MarketNeedID and (('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between a.startdate and a.enddate) or (a.enddate is null)) and a.UserRepID=" . $userid . " and a.MarketNeedID = " . $inmneed;
        $result = mysql_query($query, $con);
        if (!$result || !$row = mysql_fetch_array($result)) {
            mysql_close($con);
            $_SESSION['ShowError'] = 'Internal Error - 0x000509 - Failed authorization check';
            header('Location: mydashboard.php#admintab');
            exit();
        }
    }

// query for Customer Details section
    $query = "select FirstName, LastName, Email, Note from users where userid=" . $inuserid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $ifname = $row[0];
        $ilname = $row[1];
        $iemail = $row[2];
        $inote = $row[3];
    }

// get details about current market need
    $query = "select title, showtouser, needscontact, active, completed, findby, depositrequired, followupdate, showtorep, max(LastLogin), created, state from marketneeds where marketneedid=" . $inmneed;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $ititle = $row[0];
        $ishowtouser = $row[1];
        $ineedscontact = $row[2];
        $iactive = $row[3];
        $icompleted = $row[4];
        $ifindby = $row[5];
        $idepreq = $row[6];
        $ifollowup = $row[7];
        $ishowrep = $row[8];
        $ilastlogin = $row[9];
        $icreated = $row[10];
        $icestate = $row[11];

// mark this as seen by sales unless the user is truly an admin
        if ($uadmin != 'true')
            $query = "update marketneeds set SeenBySales=1 where MarketNeedID=" . $inmneed;

        mysql_query($query, $con);
    }

// get all assignedrep records for this marketneedid
    if (1) {
        $index = 0;
        $query = "select a.AssignedRepID, a.UserRepID, concat(u.FirstName,' ',u.LastName,' (',u.Email,')') AS FullName, a.StartDate, a.EndDate from assignedreps a, users u where a.UserRepID = u.UserID and a.MarketNeedID=" . $inmneed . " order by a.StartDate desc";
//echo "query is <br><pre>" . htmlspecialchars($query)."</pre><br>\n";
        $result = mysql_query($query, $con);
        while ($result && $row = mysql_fetch_array($result)) {
            $asrepid[$index] = $row[0];
            $asurepid[$index] = $row[1];
            $asurepname[$index] = $row[2];
            $asstart[$index] = $row[3];
            $asend[$index] = $row[4];
            $index++;
        }
    }

// get customer worksheet information
    $query = "select TimeFrame, OrderType, Year, Make, Model, MileageCeiling, Colors, PriceQuoted, Packages, MustHave, FlexibleOn, DateOrdered, DateOrderEnds, MarketNeedID, Notes from worksheets where MarketNeedID=" . $inmneed;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $wtimeframe = $row[0];
        $wordertype = $row[1];
        $wyear = $row[2];
        $wmake = $row[3];
        $wmodel = $row[4];
        $wmileageceiling = $row[5];
        $wcolors = $row[6];
        $wpricequoted = $row[7];
        $wpackages = $row[8];
        $wmusthave = $row[9];
        $wflexibleon = $row[10];
        $wdateordered = $row[11];
        $wdateends = $row[12];
        $wid = $row[13];
        $wnotes = $row[14];
        $winsertedone = 0;
    } else {
        $query = "insert into worksheets (MarketNeedID) VALUES (" . $inmneed . ")";
        $result = mysql_query($query, $con);
        $winsertedone = 1;
//$werror = mysql_error();
//$_SESSION['ShowError'] = 'eo '.$werr2.' e1 '.$werror.' row '.$row[0].'err '.mysql_error();
    }

// get user login information
    $query = "select useemail, login from userlogin where userid=" . $inuserid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $iuseemail = $row[0];
        $ilogin = $row[1];
    }

// get user primary user address
    $query = "select ZIP,Address1,Address2,City,State,StartDate,EndDate from useraddresses where IsPrimary=1 and ((EndDate is null) or ('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between startdate and enddate)) and userid=" . $inuserid;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $iaddzip = $row[0];
        $iaddaddr1 = $row[1];
        $iaddaddr2 = $row[2];
        $iaddcity = $row[3];
        $iaddstate = $row[4];
    }

// get the franchisee for the primary zip code
    $zipfranid = franchiseeofzipnodb($con, $iaddzip);
    $zipfranname = franchiseenamenodb($con, $zipfranid);

// get alternate emails
    $query = "select AlternateEmailID,AddInfo,Email,EmailToPhone,StartDate,EndDate from alternateemails where ((EndDate is null) or ('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between startdate and enddate)) and userid=" . $inuserid . " order by DisplayOrder";
    $result = mysql_query($query, $con);
    $index = 0;
    while ($result && $row = mysql_fetch_array($result)) {
        $iemailid[$index] = $row[0];
        $iemailtype[$index] = $row[1];
        $iemailaddr[$index] = $row[2];
        $iemaile2p[$index] = $row[3];
        $index++;
    }

// get phone numbers
    $query = "select n.UserNumberID, t.TypeName, n.PhoneNumber, n.Extension, n.AddInfo, n.Texting from usernumbers n, numbertypes t where ((n.EndDate is null) or ('" . date_at_timezone('Y-m-d H:i:s', 'EST') . "' between n.startdate and n.enddate)) and n.NumberTypeID=t.NumberTypeID and userid=" . $inuserid . " order by n.DisplayOrder";
    $result = mysql_query($query, $con);
    $index = 0;
    while ($result && $row = mysql_fetch_array($result)) {
        $inumid[$index] = $row[0];
        $inumtype[$index] = $row[1];
        $inumnumber[$index] = $row[2];
        $inumext[$index] = $row[3];
        $inuminfo[$index] = $row[4];
        $inumtext[$index] = $row[5];
        $index++;
    }

// get trade in details
    $query = "select Description,LastUpdated,TradeInRequestID from tradeinrequests where MarketNeedID=" . $inmneed;
    $result = mysql_query($query, $con);
    if ($result && $row = mysql_fetch_array($result)) {
        $itdesc = $row[0];
        $itlastup = $row[1];
        $itid = $row[2];

        $query = "select ImageFile from tradeinreqimages where TradeInRequestID=" . $itid;
        $result = mysql_query($query, $con);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $itimage[$index] = $row[0];
            $index++;
        }
    }

// handle the customer experience detail postback
// If anything was passed in, we are in a callback and should set the variables...
// TODO add error handling
// ADDED skip this if just switching the view
    if (isset($_REQUEST["InPostBack"]) && !isset($_REQUEST["Switching"])) {
        if (isset($_REQUEST["NeedTitle"]) && ($ititle != $_REQUEST["NeedTitle"])) {
            $ititle = $_REQUEST["NeedTitle"];
            $query = "update marketneeds set title='" . escapestr($ititle) . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["CEState"]) && ($icestate != $_REQUEST["CEState"])) {
            $icestate = $_REQUEST["CEState"];
            $query = "update marketneeds set state='" . $icestate . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["FollowUp"]) && ($ifollowup != $_REQUEST["FollowUp"])) {
            $afollowup = explode('/', $_REQUEST["FollowUp"]);
            $sfollowup = $afollowup[2] . '-' . $afollowup[0] . '-' . $afollowup[1] . ' 00:00:00';
            $ifollowup = $_REQUEST["FollowUp"];
            if (strlen($ifollowup) > 0)
                $query = "update marketneeds set followupdate='" . $sfollowup . "' where marketneedid=" . $inmneed;
            else
                $query = "update marketneeds set followupdate=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["FindBy"]) && ($ifindby != $_REQUEST["FindBy"])) {
            $afindby = explode('/', $_REQUEST["FindBy"]);
            $sfindby = $afindby[2] . '-' . $afindby[0] . '-' . $afindby[1] . ' 00:00:00';
            $ifindby = $_REQUEST["FindBy"];
            if (strlen($ifindby) > 0)
                $query = "update marketneeds set findby='" . $sfindby . "' where marketneedid=" . $inmneed;
            else
                $query = "update marketneeds set findby=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Deposit"]) && ($idepamt != $_REQUEST["Deposit"])) {
            $idepamt = $_REQUEST["Deposit"];
            if (strlen($idepamt) > 0) {
                if (substr($idepamt, 0, 1) == '$')
                    $idepamt = substr($idepamt, 1);
                $query = "update marketneeds set depositamount=" . $idepamt . " where marketneedid=" . $inmneed;
            } else
                $query = "update marketneeds set depositamount=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["DepReq"]) && ($idepreq == 0)) {
            $idepreq = 1;
            $query = "update marketneeds set depositrequired=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["DepReq"]) && ($idepreq == 1)) {
            $idepreq = 0;
            $query = "update marketneeds set depositrequired=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["ShowRep"]) && ($ishowrep == 0)) {
            $ishowrep = 1;
            $query = "update marketneeds set showtorep=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["ShowRep"]) && ($ishowrep == 1)) {
            $ishowrep = 0;
            $query = "update marketneeds set showtorep=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

//if(isset($_REQUEST["ShowUser"]) && ($ishowtouser == 0))
//{
//    $ishowtouser = 1;
//    $query = "update marketneeds set showtouser=1 where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//elseif($ishowtouser == 1)
//{
//    $ishowtouser = 0;
//    $query = "update marketneeds set showtouser=0 where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}

        if (isset($_REQUEST["NeedsContact"]) && ($ineedscontact == 0)) {
            $ineedscontact = 1;
            $query = "update marketneeds set needscontact=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["NeedsContact"]) && ($ineedscontact == 1)) {
            $ineedscontact = 0;
            $query = "update marketneeds set needscontact=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Active"]) && ($iactive == 0)) {
            $iactive = 1;
            $query = "update marketneeds set active=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["Active"]) && ($iactive == 1)) {
            $iactive = 0;
            $query = "update marketneeds set active=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Completed"]) && ($icompleted == 0)) {
            $icompleted = 1;
            $query = "update marketneeds set completed=1 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        } elseif (!isset($_REQUEST["Completed"]) && ($icompleted == 1)) {
            $icompleted = 0;
            $query = "update marketneeds set completed=0 where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

//if(isset($_REQUEST["SecKey"]) && ($iseckey != $_REQUEST["SecKey"]))
//{
//    $iseckey = $_REQUEST["SecKey"];
//    if(strlen($iseckey) > 0)
//    {
//        if(substr($iseckey,0,1) == '$') $iseckey = substr($iseckey,1);
//        $query = "update marketneeds set SecondKeyLimit=".$iseckey." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set SecondKeyLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["NavDisc"]) && ($inavdisc != $_REQUEST["NavDisc"]))
//{
//    $inavdisc = $_REQUEST["NavDisc"];
//    if(strlen($inavdisc) > 0)
//    {
//        if(substr($inavdisc,0,1) == '$') $inavdisc = substr($inavdisc,1);
//        $query = "update marketneeds set NavDiscLimit=".$inavdisc." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set NavDiscLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["SelRep"]) && ($iselrep != $_REQUEST["SelRep"]))
//{
//    $iselrep = $_REQUEST["SelRep"];
//    if(strlen($iselrep) > 0)
//    {
//        if(substr($iselrep,0,1) == '$') $iselrep = substr($iselrep,1);
//        $query = "update marketneeds set SellerRepairLimit=".$iselrep." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set SellerRepairLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["PurRep"]) && ($ipurrep != $_REQUEST["PurRep"]))
//{
//    $ipurrep = $_REQUEST["PurRep"];
//    if(strlen($ipurrep) > 0)
//    {
//        if(substr($ipurrep,0,1) == '$') $ipurrep = substr($ipurrep,1);
//        $query = "update marketneeds set PurchaserRepairLimit=".$ipurrep." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set PurchaserRepairLimit=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}
//if(isset($_REQUEST["PurDep"]) && ($ipurdep != $_REQUEST["PurDep"]))
//{
//    $ipurdep = $_REQUEST["PurDep"];
//    if(strlen($ipurdep) > 0)
//    {
//        if(substr($ipurdep,0,1) == '$') $ipurdep = substr($ipurdep,1);
//        $query = "update marketneeds set PurchaseDepositAmount=".$ipurdep." where marketneedid=".$inmneed;
//    }
//    else $query = "update marketneeds set PurchaseDepositAmount=NULL where marketneedid=".$inmneed;
//    mysql_query($query, $con);
//}

        mysql_close($con);
//$_SESSION['ShowError'] = 'Completing the postback';
        header('Location: mydashboard.php#admintab');
        exit();
    }

// handle the customer experience worksheet postback
// If anything was passed in, we are in a callback and should set the variables...
// TODO add error handling
// ADDED skip this if just switching the view
    if (isset($_REQUEST["InPostBackWorksheet"]) && !isset($_REQUEST["Switching"])) {
        if (isset($_REQUEST["WNotes"]) && ($wnotes != $_REQUEST["WNotes"])) {
            $wnotes = $_REQUEST["WNotes"];
            $query = "update worksheets set notes='" . escapestr($wnotes) . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
//$_SESSION['ShowError'] = 'writing '.escapestr($wnotes).' for '.$inmneed.' err '.mysql_error();
        }

        if (isset($_REQUEST["TimeFrame"]) && ($wtimeframe != $_REQUEST["TimeFrame"])) {
            $wtimeframe = $_REQUEST["TimeFrame"];
            $query = "update worksheets set timeframe='" . $wtimeframe . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["OrderType"]) && ($wordertype != $_REQUEST["OrderType"])) {
            $wordertype = $_REQUEST["OrderType"];
            $query = "update worksheets set ordertype='" . $wordertype . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Year"]) && ($wyear != $_REQUEST["Year"])) {
            $wyear = $_REQUEST["Year"];
            $query = "update worksheets set year='" . $wyear . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Make"]) && ($wmake != $_REQUEST["Make"])) {
            $wmake = $_REQUEST["Make"];
            $query = "update worksheets set make='" . $wmake . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Model"]) && ($wmodel != $_REQUEST["Model"])) {
            $wmodel = $_REQUEST["Model"];
            $query = "update worksheets set model='" . $wmodel . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["MileageCeiling"]) && ($wmileageceiling != $_REQUEST["MileageCeiling"])) {
            $wmileageceiling = $_REQUEST["MileageCeiling"];
            $query = "update worksheets set mileageceiling='" . $wmileageceiling . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Colors"]) && ($wcolors != $_REQUEST["Colors"])) {
            $wcolors = $_REQUEST["Colors"];
            $query = "update worksheets set colors='" . $wcolors . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["PriceQuoted"]) && ($wpricequoted != $_REQUEST["PriceQuoted"])) {
            $wpricequoted = $_REQUEST["PriceQuoted"];
            $query = "update worksheets set PriceQuoted='" . $wpricequoted . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["Packages"]) && ($wpackages != $_REQUEST["Packages"])) {
            $wpackages = $_REQUEST["Packages"];
            $query = "update worksheets set Packages='" . $wpackages . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["MustHave"]) && ($wmusthave != $_REQUEST["MustHave"])) {
            $wmusthave = $_REQUEST["MustHave"];
            $query = "update worksheets set MustHave='" . $wmusthave . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["FlexibleOn"]) && ($wflexibleon != $_REQUEST["FlexibleOn"])) {
            $wflexibleon = $_REQUEST["FlexibleOn"];
            $query = "update worksheets set FlexibleOn='" . $wflexibleon . "' where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["DateOrdered"]) && ($wdateordered != $_REQUEST["DateOrdered"])) {
            $adateordered = explode('/', $_REQUEST["DateOrdered"]);
            $sdateordered = $adateordered[2] . '-' . $adateordered[0] . '-' . $adateordered[1] . ' 00:00:00';
            $wdateordered = $_REQUEST["DateOrdered"];
            if (strlen($wdateordered) > 0)
                $query = "update worksheets set dateordered='" . $sdateordered . "' where marketneedid=" . $inmneed;
            else
                $query = "update worksheets set dateordered=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        if (isset($_REQUEST["DateOrderEnds"]) && ($wdateends != $_REQUEST["DateOrderEnds"])) {
            $adateends = explode('/', $_REQUEST["DateOrderEnds"]);
            $sdateends = $adateends[2] . '-' . $adateends[0] . '-' . $adateends[1] . ' 00:00:00';
            $wdateends = $_REQUEST["DateOrderEnds"];
            if (strlen($ifindby) > 0)
                $query = "update worksheets set dateorderends='" . $sdateends . "' where marketneedid=" . $inmneed;
            else
                $query = "update worksheets set dateorderends=NULL where marketneedid=" . $inmneed;
            mysql_query($query, $con);
        }

        mysql_close($con);
//$_SESSION['ShowError'] = 'Completing the postback';
        header('Location: mydashboard.php#admintab');
        exit();
    }

    if (isset($_REQUEST['AddMarketNeed'])) {
        unset($_REQUEST['AddMarketNeed']);
        $query = "insert into marketneeds (UserID,Created,LastLogin,Title) values (" . $inuserid . ",'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','" . date_at_timezone('Y-m-d H:i:s', 'EST') . "','New Vehicle Purchase')";
        if (mysql_query($query, $con)) {
            $lastid = mysql_insert_id($con);
            $query = "insert into assignedreps (MarketNeedID, UserRepID, StartDate) values (" . $lastid . "," . $userid . ",'" . date_at_timezone('Y-m-d H:i:s', 'EST') . "')";
            if (mysql_query($query, $con))
                $addedneed = 'true';
        }
    }
// update customer notes
    if (isset($_REQUEST['UpdateCustomerNotes'])) {
        unset($_REQUEST['UpdateCustomerNotes']);
        if (isset($_REQUEST["UserNote"]) && ($inote != $_REQUEST["UserNote"])) {
            $inote = $_REQUEST["UserNote"];
            $query = "update users set Note='" . escapestr($inote) . "' where userid=" . $inuserid;
            mysql_query($query, $con);
        }
    }
    if (isset($_REQUEST["UpdateWaitPeriod"])) {
        $inoid = $_REQUEST["OrderID"];
        $inowait = $_REQUEST["WaitPeriod"];
        $query = "update orders set WaitPeriod='" . $inowait . "' where orderid=" . $inoid;
        mysql_query($query, $con);
    }
    if (isset($_REQUEST["UpdateDeposit"])) {
        $inoid = $_REQUEST["OrderID"];
        $inodep = $_REQUEST["DepositAmount"];
        $query = "update orders set DepositReceived='" . $inodep . "' where orderid=" . $inoid;
        mysql_query($query, $con);
    }

// Fetch all market needs for this customer to populate the dropdowns
    $index = 0;
    $mncountactive = 0;
    $query = "select title, showtouser, needscontact, active, completed, findby, depositrequired, followupdate, showtorep, marketneedid, created, state from marketneeds where UserID=" . $inuserid;
    $result = mysql_query($query, $con);
    while ($result && $row = mysql_fetch_array($result)) {
        $mnid[$index] = $row[9];
        $mncreated[$index] = $row[10];    // TODO: use the created field
        $mntitle[$index] = $row[0];
        $mnshowtouser[$index] = $row[1];
        $mnneedscontact[$index] = $row[2];
        $mnactive[$index] = $row[3];
        $mncompleted[$index] = $row[4];
        $mnfindby[$index] = $row[5];
        $mndepreq[$index] = $row[6];
        $mnfollowup[$index] = $row[7];
        $mnshowrep[$index] = $row[8];
        $mncestate[$index] = $row[11];
// TODO: figure out why this aborts the rendering of the page after tradeins
// (probably because it closes the sql connection) - need to make a nodb version
//$mnstep[$index] = getuserstep($inuserid, $mnid[$index]);
        if ($mnactive[$index] != 0)
            $mncountactive++;
        $index++;
    }

// Get the stage specific details...
    if ($instep >= 1) {
// fetch favorites
        $index = 0;
        $query = 'select VehicleID, Information,Visible from favorites where MarketNeedID = ' . $inmneed . ' order by Visible desc, LastUpdated desc';
        $result = mysql_query($query);
        while ($result && $row = mysql_fetch_array($result)) {
            $vehid[$index] = $row['VehicleID'];
            $vnote[$index] = $row['Information'];
            $vvisible[$index] = $row['Visible'];

// Get the vehicle data for the favorite
            $vehquery = "select m.name 'Make', v.year 'Year', v.model 'Model', v.style 'Style', v.ImageFile 'Image', m.Origin 'Origin' from vehicles v, makes m where m.makeid=v.makeid and v.vehicleid=" . $vehid[$index];
            $vehresult = mysql_query($vehquery);
            if ($row = mysql_fetch_array($vehresult)) {
                $makedets[$index] = $row['Make'];
                $yeardets[$index] = $row['Year'];
                $modeldets[$index] = $row['Model'];
                $styledets[$index] = $row['Style'];
                $vimage[$index] = $row['Image'];
                $origin[$index] = $row['Origin'];

// TODO: how about a group by lowmiles and then order by lowmiles to do this in one shot?
// First we retrieve the details for the high mileage side...
                $highquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=0 and vehicleid = " . $vehid[$index];
                $highresult = mysql_query($highquery);
                if ($row = mysql_fetch_array($highresult)) {
                    $highbb[$index] = $row['bestbuy'];
                    $highmilestart[$index] = $row['mileagestart'];
                    $highmileend[$index] = $row['mileageend'];
                    $highpricestart[$index] = $row['pricestart'];
                    $highpriceend[$index] = $row['priceend'];
                }
// Next we retrieve the details for the low mileage side...
                $lowquery = "select bestbuy, mileagestart, mileageend, pricestart, priceend from vehicledata where lowmiles=1 and vehicleid = " . $vehid[$index];
                $lowresult = mysql_query($lowquery);
                if ($row = mysql_fetch_array($lowresult)) {
                    $lowbb[$index] = $row['bestbuy'];
                    $lowmilestart[$index] = $row['mileagestart'];
                    $lowmileend[$index] = $row['mileageend'];
                    $lowpricestart[$index] = $row['pricestart'];
                    $lowpriceend[$index] = $row['priceend'];
                }
            }
            $index++;
        }
    }
    
$queryvehiclespecification = "SELECT * FROM  `searchplans` sp, searchplandetails spd WHERE sp.SearchPlanID = spd.SearchPlanID AND sp.MarketNeedID =".$inmneed;
$resultvehiclespecification = mysql_query($queryvehiclespecification, $con);  


$queryvehiclesearchplan = "SELECT * FROM  `searchplans` sp, searchplandetails spd WHERE sp.SearchPlanID = spd.SearchPlanID AND sp.MarketNeedID =".$inmneed." AND spd.add_in_new_searn_plan = 1" ;
$resultvehiclesearchplan = mysql_query($queryvehiclesearchplan, $con);  

 $querypos = "SELECT * FROM  `pasplans` pp, pasplandetails ppd WHERE pp.SearchPlanID = ppd.SearchPlanID AND pp.MarketNeedID =".$inmneed;
$resultpos = mysql_query($querypos, $con);  

    
      
    if ($instep >= 2) {
// Get the Assessment information (if any)...
        $query = 'select * from assessments where MarketNeedID = ' . $inmneed;
        $result = mysql_query($query, $con);
        if ($result && $row = mysql_fetch_assoc($result)) {
            $ahaveassess = 1;
            $BudgetCeiling = $row['BudgetCeiling'];
            $CareGiven = $row['CareGiven'];
            $Comfort = $row['Comfort'];
            $Created = $row['Created'];
            $Dependability = $row['Dependability'];
            $Economy = $row['Economy'];
            $ForProfessionalUse = $row['ForProfessionalUse'];
            $FuelEfficiency = $row['FuelEfficiency'];
            $HowSoon = $row['HowSoon'];
            $KeepFor = $row['KeepFor'];
            $LastUpdated = $row['LastUpdated'];
            $Luxury = $row['Luxury'];
            $MarketNeedID_db = $row['MarketNeedID'];
            $MileageGoal = $row['MileageGoal'];
            $MostlyDrive = $row['MostlyDrive'];
            $NeedType = $row['NeedType'];
            $Origin = $row['Origin'];
            $Particular = $row['Particular'];
            $Quietness = $row['Quietness'];
            $Roominess = $row['Roominess'];
            $Safety = $row['Safety'];
            $Security = $row['Security'];
            $SeenBySales = $row['SeenBySales'];
            $VehicleType = $row['VehicleType'];
            $BodyType = $row['BodyType'];
            $VehicleSize = $row['VehicleSize'];
            $DriveTrain = $row['DriveTrain'];
            $Notes = $row['Notes'];
            $Transmission = $row['Transmission'];
            $MileageFrom = $row['MileageFrom'];
            $MileageTo = $row['MileageTo'];
            $MileageCeiling = $row['MileageCeiling'];
            $ExtLike = $row['ExtLike'];
            $ExtDislike = $row['ExtDislike'];
            $IntLike = $row['IntLike'];
            $IntDisike = $row['IntDisike'];
            $BudgetFrom = $row['BudgetFrom'];
            $BudgetTo = $row['BudgetTo'];
            $BorrowMaxPayment = $row['BorrowMaxPayment'];
            $BorrowDownPayment = $row['BorrowDownPayment'];
            $VehicleNeed = $row['VehicleNeed'];
            $BrandLike = $row['BrandLike'];
            $BrandDislike = $row['BrandDislike'];
            $Model = $row['Model'];
            $VehicleAge = $row['VehicleAge'];
            $RateFuelEfficiency = $row['RateFuelEfficiency'];
            $RateMintenanceCost = $row['RateMintenanceCost'];
            $RateReliability = $row['RateReliability'];
            $RateLuxury = $row['RateLuxury'];
            $RateSporty = $row['RateSporty'];
            $RateSafety = $row['RateSafety'];
            $FrontSType = $row['FrontSType'];
            $BedType = $row['BedType'];
            $Leather = $row['Leather'];
            $HeatedSeat = $row['HeatedSeat'];
            $Navigation = $row['Navigation'];
            $SunRoof = $row['SunRoof'];
            $AlloyWheels = $row['AlloyWheels'];
            $RearWindow = $row['RearWindow'];
            $BedLiner = $row['BedLiner'];
            $EntertainmentSystem = $row['EntertainmentSystem'];
            $ThirdRD = $row['ThirdRD'];
            $CRow = $row['CRow'];
            $PRHatch = $row['PRHatch'];
            $BackupCamera = $row['BackupCamera'];
            $TPackage = $row['TPackage'];
            $OtherPrefereces = $row['OtherPrefereces'];
            $OtherReallyHave = $row['OtherReallyHave'];
            $Sportiness = $row['Sportiness'];
            $StatusLevel = $row['StatusLevel'];
            $AdditionalInfo = $row['AdditionalInfo'];

            $query = "update assessments set SeenBySales=1 where AssessmentID=" . $row['AssessmentID'];
            mysql_query($query, $con);
        } else
            $ahaveassess = 0;

// Get the current Request for Consultation Information (if any)...
        $query = 'select Notes, ConsultationID from consultations where MarketNeedID = ' . $inmneed;
        $result = mysql_query($query, $con);
        if ($result && $row = mysql_fetch_array($result)) {
            $ahaveconsult = 1;
            $anote = $row[0];

            $query = "update consultations set SeenBySales=1 where ConsultationID=" . $row[1];
            mysql_query($query, $con);
        } else
            $ahaveconsult = 0;
    }

    if ($instep >= 2) {
// Get the current Quote Request Information (if any)...
        $query = "select QuoteRequestID, QuoteType, Year, Make, Model, Style, MileageCeiling, EngineCylinders, Transmission,
CabType, FrontSeatType, BedType, SeatMaterial, WheelDriveType, WheelCovers, RadioNeeded, RadioFM, RadioCassette, RadioCD, RadioSat, PowerDoors, PowerWindows,
PowerSeats, HeatedSeats, AirConditioning, RemoteEntry, TractionControl, SecuritySystem, CruiseControl, Navigation, RearSlidingWindow, BedLiner, TowPackage,
ColorCombination, SpecialRequests, Pickup, DeliverToCity, DeliverToState, LuggageRack, SunRoof, Visible from quoterequests where MarketNeedID=" . $inmneed . " order by Visible desc";
        $result = mysql_query($query);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $aquoteid[$index] = $row['QuoteRequestID'];
            $aquotetype[$index] = $row['QuoteType'];
            $ayear[$index] = $row['Year'];
            $amake[$index] = $row['Make'];
            $amodel[$index] = $row['Model'];
            $astyle[$index] = $row['Style'];
            $aqmileage[$index] = $row['MileageCeiling'];
            $aseattype[$index] = $row['SeatMaterial'];
            $aengine[$index] = $row['EngineCylinders'];
            $adrive[$index] = $row['WheelDriveType'];
            $atrans[$index] = $row['Transmission'];
            $awheels[$index] = $row['WheelCovers'];
            $acab[$index] = $row['CabType'];
            $afront[$index] = $row['FrontSeatType'];
            $abedtype[$index] = $row['BedType'];
            $aradflex[$index] = $row['RadioNeeded'];
            $aradfm[$index] = $row['RadioFM'];
            $aradcas[$index] = $row['RadioCassette'];
            $aradcd[$index] = $row['RadioCD'];
            $aradsat[$index] = $row['RadioSat'];
            $apdoor[$index] = $row['PowerDoors'];
            $apwin[$index] = $row['PowerWindows'];
            $apseat[$index] = $row['PowerSeats'];
            $aheat[$index] = $row['HeatedSeats'];
            $aair[$index] = $row['AirConditioning'];
            $aremote[$index] = $row['RemoteEntry'];
            $atraction[$index] = $row['TractionControl'];
            $asecure[$index] = $row['SecuritySystem'];
            $acruise[$index] = $row['CruiseControl'];
            $anavsys[$index] = $row['Navigation'];
            $arearwin[$index] = $row['RearSlidingWindow'];
            $abed[$index] = $row['BedLiner'];
            $atow[$index] = $row['TowPackage'];
            $acolors[$index] = $row['ColorCombination'];
            $anotes[$index] = $row['SpecialRequests'];
            $adelorpick[$index] = $row['Pickup'];
            $adelcity[$index] = $row['DeliverToCity'];
            $adelstate[$index] = $row['DeliverToState'];
            $aluggage[$index] = $row['LuggageRack'];
            $asunroof[$index] = $row['SunRoof'];
            $avisible[$index] = $row['Visible'];

// get current market studies (firm quotes)
            $fquery = "Select UpdateStatus,OrderType,Mileage,PricePoint,ImageFile,AdminNote,FirmQuoteID from firmquotes where QuoteRequestID=" . $aquoteid[$index];
            $fresult = mysql_query($fquery);
            if ($fresult && ($frow = mysql_fetch_array($fresult))) {
                if ($frow[0] == 'Researching')
                    $fstatus[$index] = 1;
                else
                    $fstatus[$index] = 0;
                $fordertype[$index] = $frow[1];
                $fmiles[$index] = $frow[2];
                $fprice[$index] = $frow[3];
                $fimage[$index] = $frow[4];
                $fnote[$index] = $frow[5];
                $fquoteid[$index] = $frow[6];
            }
            $index++;
        }
        if ($index > 0)
            $ahavequotes = 1;
        else
            $ahavequotes = 0;
    }

    if ($instep >= 3) {
// Get the Orders and Posts...
        $query = "select q.QuoteRequestID, q.QuoteType, q.Year, q.Make, q.Model, q.Style, f.OrderType, o.accepted, o.Expires, o.DepositReceived, o.WaitPeriod, o.orderid, f.AddDepositAmount from orders o,orderfor of,firmquotes f,quoterequests q where o.orderid=of.orderid and f.firmquoteid=of.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=" . $inmneed;
        $result = mysql_query($query);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $oquoteid[$index] = $row[0];
            $oquotetype[$index] = $row[1];
            $oyear[$index] = $row[2];
            $omake[$index] = $row[3];
            $omodel[$index] = $row[4];
            $ostyle[$index] = $row[5];
            $oordertype[$index] = $row[6];
            $oaccepted[$index] = $row[7];
            $oexpires[$index] = $row[8];
            $odeprec[$index] = $row[9];
            $owait[$index] = $row[10];
            $oorderid[$index] = $row[11];
            $odepneeded[$index] = $row[12] + 250.00;
            $odepreq[$index] = 1;
            $mnquery = "select depositrequired from marketneeds where marketneedid=" . $inmneed;
            $mnresult = mysql_query($mnquery);
            if ($mnrow = mysql_fetch_array($mnresult))
                if ($mnrow[0] != 1)
                    $odepreq[$index] = 0;

            $index++;
        }

// get watches
        $query = "select q.QuoteRequestID, q.QuoteType, q.Year, q.Make, q.Model, q.Style, f.OrderType, w.accepted, w.Expires, w.watchid from watches w,watchfor wf,firmquotes f,quoterequests q where w.watchid=wf.watchid and f.firmquoteid=wf.firmquoteid and f.quoterequestid=q.quoterequestid and q.marketneedid=" . $inmneed;
        $result = mysql_query($query);
        $index = 0;
        while ($result && $row = mysql_fetch_array($result)) {
            $wquoteid[$index] = $row[0];
            $wquotetype[$index] = $row[1];
            $wyear[$index] = $row[2];
            $wmake[$index] = $row[3];
            $wmodel[$index] = $row[4];
            $wstyle[$index] = $row[5];
            $wordertype[$index] = $row[6];
            $waccepted[$index] = $row[7];
            $wexpires[$index] = $row[8];
            $wwatchid[$index] = $row[9];
            $index++;
        }
    }

    mysql_close($con);
}

function makefind($make){

$con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
if($con)
{
mysql_select_db(DB_SERVER_DATABASE, $con);

$querymake = "SELECT Name FROM  `makes` WHERE  `MakeID` =".$make;
$resultmake = mysql_query($querymake, $con);
$makerow = mysql_fetch_assoc($resultmake);
return $makerow['Name'];
mysql_close($con);
}



}


?>
<?php require("headerstart.php"); ?>
<script type="text/javascript">
    function trimAll(sString)
    {
        while (sString.substring(0, 1) == ' ')
        {
            sString = sString.substring(1, sString.length);
        }

        while (sString.substring(sString.length - 1, sString.length) == ' ')
        {
            sString = sString.substring(0, sString.length - 1);
        }
        return sString;
    }

    function validateDataOnSubmit()
    {
        var datechars = new RegExp("[^0123456789:/ -]");
        var pricechars = new RegExp("[^0123456789.]");

        var vtitle = document.getElementById("NeedTitle");
        var tString = trimAll(vtitle.value);
        if (tString.length < 1)
        {
            alert("Error that must be corrected:" + '\n' + "Title for Market Need must be filled in." + '\n');
            vtitle.focus();
            vtitle.style.background = '#ffcccc';
            return false;
        }
        else
            vtitle.style.background = 'White';

        vtitle = document.getElementById("FollowUp");
        tString = trimAll(vtitle.value);
        if ((tString.length > 0) && (datechars.test(tString)))
        {
            alert("Error that must be corrected:" + '\n' + "Follow Up Date can only contain digits, dashes, slashes and colons (preferred format YYYY-MM-DD HH:MM:SS)." + '\n');
            vtitle.focus();
            vtitle.style.background = '#ffcccc';
            return false;
        }
        else
            vtitle.style.background = 'White';

        vtitle = document.getElementById("FindBy");
        tString = trimAll(vtitle.value);
        if ((tString.length > 0) && (datechars.test(tString)))
        {
            alert("Error that must be corrected:" + '\n' + "Follow Up Date can only contain digits, dashes, slashes and colons (preferred format YYYY-MM-DD HH:MM:SS)." + '\n');
            vtitle.focus();
            vtitle.style.background = '#ffcccc';
            return false;
        }
        else
            vtitle.style.background = 'White';

        /*
         var dprice = document.getElementById("Deposit");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Deposit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else if(parseFloat(tString) < 250.00)
         {
         alert("Error that must be corrected:"+'\n'+"Deposit can not be less than $250."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else if(parseFloat(tString) > 500.00)
         {
         alert("Error that must be corrected:"+'\n'+"Deposit can not be greater than $500."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Deposit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("SecKey");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Second Key Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Second Key Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("NavDisc");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Nav Disc Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Nav Disc Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("SelRep");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Seller Repair Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Seller Repair Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("PurRep");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Purchaser Repair Limit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Purchaser Repair Limit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         
         var dprice = document.getElementById("PurDep");
         tString = trimAll(dprice.value);
         if(tString.length > 0)
         {
         if(pricechars.test(tString))
         {
         alert("Error that must be corrected:"+'\n'+"Specific Vehicle Purchase Deposit can only contain digits and a decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else
         {
         if(tString.indexOf(".") != -1)
         {
         if(tString.indexOf(".") != tString.lastIndexOf("."))
         {
         alert("Error that must be corrected:"+'\n'+"Specific Vehicle Purchase Deposit is not valid with more than one decimal (preferred format XX.XX)."+'\n');
         dprice.focus();
         dprice.style.background = '#ffcccc';
         return false;
         }
         else dprice.style.background = 'White';
         }
         else dprice.style.background = 'White';
         }
         }
         else dprice.style.background = 'White';
         */

        return true;
    }

// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
    function numbersonly(e)
    {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);

// control keys
        if ((key == null) || (key == 0) || (key == 8) ||
                (key == 9) || (key == 13) || (key == 27))
            return true;

// numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        else
            return false;
    }

    function addnewneed()
    {
        var r = confirm("Are you sure you want to Add a Customer Experience?")
        if (r == true)
        {
            alert("Please note, you will not see this new experience on this page.  Your customer will show up again on your dashboard in stage 1." + '\n')
            return true;
        }
        else
        {
            return false;
        }
    }

    function checkorder()
    {
        var dperiod = document.getElementById("WaitPeriod");
        tString = trimAll(dperiod.value);
        if (tString.length < 1)
        {
            alert("Error that must be corrected:" + '\n' + "Wait Period must be specified." + '\n');
            dperiod.focus();
            dperiod.style.background = '#ffcccc';
            return false;
        }
        else
            dperiod.style.background = 'White';

        return true;
    }

    function ceactivechanged(u)
    {
        var theselect = document.getElementById("ceactive");
        if (theselect.options[theselect.selectedIndex].value > 0) // support the 'choose one' item
        {
            var thelocation = 'salesrepactions.php?Switching=1&ForUserID=' + u + '&MarketNeedID=' + theselect.options[theselect.selectedIndex].value + '#mnid';
            self.location = thelocation;
        }
    }

    function ceprevchanged(u)
    {
        var theselect = document.getElementById("ceprev");
        if (theselect.options[theselect.selectedIndex].value > 0) // support the 'choose one' item
        {
            var thelocation = 'salesrepactions.php?Switching=1&ForUserID=' + u + '&MarketNeedID=' + theselect.options[theselect.selectedIndex].value + '#mnid';
            self.location = thelocation;
        }
    }

    $(function()
    {
        $('.date-pick').datePicker({clickInput: true});
    });

// when the cestate select is changed - update active, show, completed, archive reason appropriately
// supported states
// echo '<option value="0">Active</option>';
// echo '<option value="10">Followup: Search on Hold</option>';
// echo '<option value="11">Followup: Check In Later</option>';
// echo '<option value="20">Lost Interest</option>';
// echo '<option value="21">Disliked System</option>';
// echo '<option value="22">Disliked SalesRep</option>';
// echo '<option value="30">Spam Customer</option>';
// echo '<option value="31">Test or Demo Customer</option>';
// echo '<option value="100">Bought One!</option>';

    function cestatechanged()
    {
        var eactive = document.getElementById("Active");
        var ecompleted = document.getElementById("Completed");
        var eshowrep = document.getElementById("ShowRep");
        var econtact = document.getElementById("NeedsContact");
        var ear = document.getElementById("ArchiveReason");
        var estate = document.getElementById("CEState");

// set archive reason text box
        ear.value = estate.options[estate.selectedIndex].text;

// only one active state
        if (estate.options[estate.selectedIndex].value == 0) // active
        {
            eactive.checked = true;
            eshowrep.checked = true;
            ecompleted.checked = false;
        }

// only one completed state
        else if (estate.options[estate.selectedIndex].value == 100) // bought one
        {
            eactive.checked = false;
            eshowrep.checked = false;
            ecompleted.checked = true;
        }

// follow up states are active but not seen on dashboard until the follow up date is < 24 hours in the future (or in the past)
        else if (estate.options[estate.selectedIndex].value >= 10 &&
                estate.options[estate.selectedIndex].value < 20) // followup: *
        {
            eactive.checked = true;
            eshowrep.checked = false;
            ecompleted.checked = false;
        }

// all the rest are pure archive: not active, not shown, not completed
        else
        {
            eactive.checked = false;
            eshowrep.checked = false;
            ecompleted.checked = false;
        }

// needs contact checkbox could match active checkbox
// NOT YET since needs contact appears to be a reply needed bit that is set if customer does something
// so this should be shown obviously in the dashboard view
//econtact.checked = eactive.checked;
    }


</script>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php // require("headerend.php"); ?>

<!-- TODO: complete date picker insert from: http://www.kelvinluck.com/assets/jquery/datePicker/v2/demo/-->
<style type="text/css">@import "datepicker.css";</style>
<script type="text/javascript" src="common/scripts/date.js"></script>
<script type="text/javascript" src="common/scripts/jquery.datepicker.js"></script>
<script type="text/javascript" src="common/scripts/jquery.datePickerMultiMonth.js"></script>
<div class="gridtwelve"></div>
<div id="content">
    <div class="grideightcontainer">
        <h1 class="subhead" style="text-align: left;width: 100%;margin-left: 0;"> Customer Review</h1>
        <div class="grideightgrey">
            <div class="grideight" style="margin-top: -5px;">
                <p class="blackeleven" style="margin: 0pt;"><a href="mydashboard.php#admintab">Go back to My Dashboard</a></p>
                <br clear="all" />

<?php
//echo '<p style="color: rgb(133, 193, 27); font-size: 16px; margin-top: 0pt; float: left;"><strong>Customer Details</strong></p>';

echo '<h4 class="subhead"><button class="hideshow" id="span_curtomer_details" onclick="javascript:showhidediv(\'curtomer_details\', \'span_curtomer_details\');"  >Hide</button>&nbsp;&nbsp;Customer Details</h4>';
echo '<span style="float: right;">';
// send message form
echo '<form action="sendmessage.php" method="post" target="_blank">';
echo '<input type="hidden" value="' . $inuserid . '" name="MessageToID" />';
echo '<input type="hidden" value="salesrepactions.php?ForUserID=' . $inuserid . '&MarketNeedID=' . $inmneed . '" name="ReturnTo" />';
echo '<p style="color: #fff;font-size: 12px; margin-top: -30px;">';
echo 'Send a Message: ';
echo '<button type="submit" value="" class="blueongrey"><img alt="Send" title="Send a Message" align="middle" border="0" src="common/layout/mailicon.gif" /></button>';
echo '</p>';
echo '</form></span>';
echo '<div id="curtomer_details" style="display:block">';
?>

<?php
// customer details section: name, email, login, address, phone, id, note
echo '<table  style="float: left; width:100%" border="0"><tbody>';
echo '<tr><td style="width: 33%;"><strong>';
echo 'ID';
echo '</strong></td><td  >';
echo $inuserid;
echo '</td></tr><tr><td><strong>';
echo 'Franchise';
echo '</strong></td><td >';
echo $zipfranname;
echo '</td></tr><tr><td><strong>';
echo 'Name';
echo '</strong></td><td>';
echo $ifname . ' ' . $ilname;
echo '</td></tr><tr><td><strong>';
echo 'Email';
echo '</strong></td><td>';
echo '<a href="mailto:' . $iemail . '">' . $iemail . '</a>';
echo '</td></tr><tr><td><strong>';
echo 'Login';
echo '</strong></td><td>';
if ($iuseemail == 1)
    echo '(Same as Email)';
else
    echo $ilogin;
echo '</td></tr><tr><td><strong>';
echo 'Last Login';
echo '</strong></td><td>';
echo $ilastlogin . ' <span >(' . timesincenow($ilastlogin) . ')</span>';
echo '</td></tr>';
echo '<td><td>&nbsp;</td></tr>';
echo '<tr><td valign="top"><strong>';
echo 'Primary Address';
echo '</strong></td><td>';
$theaddress = "";
if (isset($iaddzip)) {
    if (!is_null($iaddaddr1)) {
        $theaddress .= $iaddaddr1;
        if (!is_null($iaddaddr2)) {
            $theaddress .= ' ' . $iaddaddr2;
            if (!is_null($iaddcity)) {
                $theaddress .= ', ' . $iaddcity;
                if (!is_null($iaddstate)) {
                    $theaddress .= ', ' . $iaddstate;
                    $theaddress .= ', ' . $iaddzip;
                } else
                    $theaddress .= ', ' . $iaddzip;
            }
            elseif (!is_null($iaddstate)) {
                $theaddress .= ', ' . $iaddstate;
                $theaddress .= ', ' . $iaddzip;
            } else
                $theaddress .= ', ' . $iaddzip;
        }
        else {
            if (!is_null($iaddcity)) {
                $theaddress .= ' <br/>' . $iaddcity;
                if (!is_null($iaddstate)) {
                    $theaddress .= ', ' . $iaddstate;
                    $theaddress .= ', ' . $iaddzip;
                } else
                    $theaddress .= ', ' . $iaddzip;
            }
            elseif (!is_null($iaddstate)) {
                $theaddress .= ' <br/> ' . $iaddstate;
                $theaddress .= ', ' . $iaddzip;
            } else
                $theaddress .= ' <br/> ' . $iaddzip;
        }
    }
    else {
        if (!is_null($iaddcity)) {
            $theaddress .= $iaddcity;
            if (!is_null($iaddstate)) {
                $theaddress .= ', ' . $iaddstate;
                $theaddress .= ', ' . $iaddzip;
            } else
                $theaddress .= $iaddzip;
        }
        elseif (!is_null($iaddstate)) {
            $theaddress .= $iaddstate;
            $theaddress .= ', ' . $iaddzip;
        } else
            $theaddress .= $iaddzip;
    }
}
echo $theaddress;
echo '<a style="margin-left: 35%;" href="http://maps.google.com/maps?q=' . urlencode(strip_tags($theaddress)) . '">Map</a>';
echo '</td></tr>';
echo '<tr><td><strong>';
echo 'Phone Numbers';
echo '</strong></td>';

// show phone numbers if present
if (isset($inumid)) {
    $count = count($inumid);
    for ($i = 0; $i < $count; $i++) {
        if ($i == 0)
            echo '<td>';
        else
            echo '<tr><td>&nbsp;</td><td>';
        echo '(' . substr($inumnumber[$i], 0, 3) . ') ' . substr($inumnumber[$i], 3, 3) . '-' . substr($inumnumber[$i], 6, 4);
        if (isset($inumext[$i]))
            echo ' x ' . $inumext[$i];
        echo ' <span style="color: rgb(133, 193, 27);">[' . $inumtype[$i] . '] &nbsp;';
        if ($inumtext[$i] == 1)
            echo '[&#10003;Texting]';
//else echo '[No Texting]';
        echo '</span>';
        if (isset($inuminfo[$i]))
            echo ' Note: ' . $inuminfo[$i];
        echo '</td>';
    }
} else
    echo '<td>&nbsp;</td>';
echo '</tr>';

// show alternate emails if present
if (isset($iemailid)) {
    echo '<tr><td><strong>';
    echo 'Alternate Emails';
    echo '</strong></td>';
    $count = count($iemailid);
    for ($i = 0; $i < $count; $i++) {
        if ($i == 0)
            echo '<td style="color: rgb(117, 117, 117);">';
        else
            echo '<tr><td>&nbsp;</td><td style="color: rgb(117, 117, 117);">';
        echo $iemailtype[$i] . ' - ' . $iemailaddr[$i] . ' (On Phone - ';
        if ($iemaile2p[$i] == 1)
            echo 'Yes)';
        else
            echo 'No)';
        echo '</td>';
    }
    echo '</tr>';
}
?>
                <form action="salesrepactions.php" method="post" name="custnoteform" >
                    <tr>
                        <td><strong>Notes</strong></td>
                        <td> 
<?php
echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
echo '<input type="hidden" value="true" name="UpdateCustomerNotes" />';
echo '<input type="text" id="UserNote" name="UserNote" value="' . $inote . '"><br/>';
?>

                        </td>
                    </tr>    
                    <tr>
                        <td></td>
                        <td> 
<?php
echo '<button type="submit" value="" class="med">UPDATE CUSTOMER NOTES</button>';
?>                    
                        </td>
                    </tr>    
                </form>
<?php
echo '</tbody></table>';
echo '</div>';



// list of customer experiences to choose from show state in title span_customer_experiences
echo '<h4 class="subhead"> <button class="hideshow"  id="span_customer_experiences" onclick="javascript:showhidediv(\'customer_experiences\', \'span_customer_experiences\');"  >Hide</button>&nbsp;&nbsp;Customer Experiences (' . count($mntitle) . ')</h4>';

// TODO: add real ce search to the database section at top
// list format: 'id: name'
// add customer experience form       
?> 
                <div id="customer_experiences" style="display:block">            
                    <table border="0" style="float: left; color: rgb(20, 44, 60); width:100%">
                        <tbody>

                            <tr>
                                <td style="width: 33%;"><strong>Active Experiences</strong></td>
                                <td style="width: 65%;"> 
<?php
echo '<select name="ceactive" id="ceactive" onchange="javascript:ceactivechanged(' . $inuserid . ')">';

$count = count($mntitle);
$options = 0;
if ($count > 0) {
    for ($i = 0; $i < $count; $i++) {
        if ($mnactive[$i] != 0) {
            if ($options == 0)
                echo '<option value="0">Choose One</option>';
            $dayparts = explode(' ', $mncreated[$i]);

            echo '<option value="' . $mnid[$i] . '"';
            if ($inmneed == $mnid[$i])
                echo ' selected="selected" ';
            echo '>' . $mnid[$i] . ': ' . $mntitle[$i] . ' (' . $dayparts[0] . ')</option>';
            $options++;
        }
    }
}
if ($count == 0 || $options == 0) {
    echo '<option value="" disabled>No Active Experiences</option>';
}
echo '</select>';
?>
                                </td>
                            </tr>    
                            <tr>
                                <td><strong>Previous Experiences</strong></td>
                                <td> 
<?php
echo '<select name="ceprev" id="ceprev" onchange="javascript:ceprevchanged(' . $inuserid . ')">';

$count = count($mntitle);
$options = 0;
if ($count > 0) {
    for ($i = 0; $i < $count; $i++) {
        if ($mnactive[$i] == 0) {
            if ($options == 0)
                echo '<option value="0">Choose One</option>';
            $dayparts = explode(' ', $mncreated[$i]);

            echo '<option value="' . $mnid[$i] . '"';
            if ($inmneed == $mnid[$i])
                echo ' selected="selected" ';
            echo '>' . $mnid[$i] . ': ' . $mntitle[$i] . ' (' . $dayparts[0] . ')</option>';
            $options++;
        }
    }
}
if ($count == 0 || $options == 0) {
    echo '<option value="" disabled>No Previous Experiences</option>';
}
echo '</select>';
?>
                                </td>
                            </tr>

                            <tr>
                                <td>

                                </td>
                                <td>
<?php
echo '<form action="salesrepactions.php" method="post" name="newneed" onsubmit="javascript:return addnewneed();" >';
echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
echo '<input type="hidden" value="true" name="AddMarketNeed" />';
echo '<button type="submit" value="" class="med">ADD CUSTOMER EXPERIENCE</button>';
if (isset($addedneed))
    echo '<br/>New Customer Experience Added.';
echo '</form>';
?>
                                </td>
                            </tr>

                        </tbody></table>
                </div>


<?php
// Customer Experience section  
echo '<h4 class="subhead"><button class="hideshow"  id="span_customer_experience_section" onclick="javascript:showhidediv(\'customer_experience_section\', \'span_customer_experience_section\');"  >Hide</button>&nbsp;&nbsp;Customer Experience Details</h4>';
echo '<div id="customer_experience_section" style="display:block">';
echo '<form action="salesrepactions.php" method="post" name="repactionform" onsubmit="javascript:return validateDataOnSubmit();" >';
echo '<table border="0" style="float: left; color: rgb(20, 44, 60); width:100%" ><tbody>';
echo '<tr><td style="width:33%"><strong>';
echo 'Created';
echo '</strong></td><td colspan="2" style="width:65%">';
echo $icreated . ' <span style="color: rgb(133, 193, 27);">(' . timesincenow($icreated) . ')</span>';
echo '<tr><td ><strong>';
echo 'Sales Rep';
echo '</strong></td><td colspan="2">';
// if ($asurerepid[0] = $inuserid) echo 'Me';
// else echo $asurepname[0];
if (!empty($asurepname))
    echo $asurepname[0];
echo '</td></tr><tr><td><strong>';
echo 'Experience ID';
echo '</strong></td><td>';
echo $inmneed;
echo '</td><td>';
echo '<label style="display:none"><input name="Active" id="Active" type="checkbox" value="" ';
if (isset($iactive) && ($iactive == 1))
    echo 'checked="checked"';
echo ' /> Active?</label>';
echo '</td></tr><tr><td ><strong>';
echo 'Title';
echo '</strong></td><td>';
echo '<input name="NeedTitle" id="NeedTitle" type="text" style="width: 94%;" maxlength="75" value="' . $ititle . '" /> ';
echo '</td><td><label style="display:none"><input name="ShowRep" id="ShowRep" type="checkbox" value="" ';
if (isset($ishowrep) && ($ishowrep == 1))
    echo 'checked="checked"';
echo ' /> Seen On Dashboard?</label>';
echo '</td></tr><tr><td><strong>';
echo 'Follow Up';
echo '</strong></td><td>';
echo '<input name="FollowUp" id="FollowUp" class="date-pick" type="text"  style="width: 94%;" size="10" maxlength="10" value="';
if (!is_null($ifollowup))
    echo date_at_timezone('m/d/Y', 'GMT', $ifollowup);
echo '" />';
// if(!is_null($ifollowup)) echo '<span id="followupsince" style="color: rgb(133, 193, 27);"> ('.timesincenow($ifollowup, 1).')</span>';
echo '</td><td><label><input name="NeedsContact" id="NeedsContact" type="checkbox" value="" ';
if (isset($ineedscontact) && ($ineedscontact == 1))
    echo 'checked="checked"';
echo ' /> Needs Contact?</label>';
echo '</td></tr><tr><td><strong>';
echo 'Find It By';
echo '</strong></td><td>';
echo '<input name="FindBy" id="FindBy" class="date-pick" type="text" style="width: 94%;" size="10" maxlength="10" value="';
if (!is_null($ifindby))
    echo date_at_timezone('m/d/Y', 'GMT', $ifindby);
echo '" /> ';
// if(!is_null($ifindby))     echo '<span style="color: rgb(133, 193, 27);"> ('.timesincenow($ifindby, 1).')</span>';
echo '<td><label><input name="DepReq" type="checkbox" value="" ';
if (isset($idepreq) && ($idepreq == 1))
    echo 'checked="checked"';
echo ' /> Deposit Required?</label>';
echo '</td></tr><tr>';


//echo '<strong>';
//echo 'Deposit Amount';
//echo '</strong></td><td>';
//echo '<input name="Deposit" id="Deposit" type="text" size="10" maxlength="8" value="';
//if(!is_null($idepamt)) echo $idepamt;
//echo '" /> ';
echo '</tr>';
//echo '<tr><td><strong>';
//echo 'Sec Key/Remote Max';
//echo '</strong></td><td style="color: rgb(117, 117, 117);">';
//echo '<input name="SecKey" id="SecKey" type="text" size="10" maxlength="8" value="';
//if(!is_null($iseckey)) echo $iseckey;
//echo '" /> ';
//echo '</td></tr>';
//echo '</td></tr><tr><td><strong>';
//echo 'Nav Disc Max';
//echo '</strong></td><td style="color: rgb(117, 117, 117);">';
//echo '<input name="NavDisc" id="NavDisc" type="text" size="10" maxlength="8" value="';
//if(!is_null($inavdisc)) echo $inavdisc;
//echo '" /> ';
//echo '</td></tr>';
//echo '</td></tr><tr><td><strong>';
//echo 'Seller Repair Max Max';
//echo '</strong></td><td style="color: rgb(117, 117, 117);">';
//echo '<input name="SelRep" id="SelRep" type="text" size="10" maxlength="8" value="';
//if(!is_null($iselrep)) echo $iselrep;
//echo '" /> ';
//echo '</td></tr>';
//echo '</td></tr><tr><td><strong>';
//echo 'Purchaser Repair Max';
//echo '</strong></td><td style="color: rgb(117, 117, 117);">';
//echo '<input name="PurRep" id="PurRep" type="text" size="10" maxlength="8" value="';
//if(!is_null($ipurrep)) echo $ipurrep;
//echo '" /> ';
//echo '</td></tr>';
//echo '</td></tr><tr><td><strong>';
//echo 'Deposit for Purchase';
//echo '</strong></td><td style="color: rgb(117, 117, 117);">';
//echo '<input name="PurDep" id="PurDep" type="text" size="10" maxlength="8" value="';
//if(!is_null($ipurdep)) echo $ipurdep;
//echo '" /> ';
//echo '</td></tr>';
// add the customer state functions
echo '<tr><td><strong>Experience State</strong>';
echo '</td><td><select id="CEState" name="CEState" style="width: 94%;" onchange="javascript:cestatechanged();" onload="javascript:cestatechanged();">';
echo '<option value="0"';
if ($icestate == 0)
    echo 'selected="selected"';
echo '>Active Customer</option>';
echo '<option value="100" ';
if ($icestate == 100)
    echo 'selected="selected"';
echo '>Bought One!</option>';
echo '<option value="10"';
if ($icestate == 10)
    echo 'selected="selected"';
echo '>Followup: Search on Hold</option>';
echo '<option value="11"';
if ($icestate == 11)
    echo 'selected="selected"';
echo '>Followup: Check In Later</option>';
echo '<option value="20"';
if ($icestate == 20)
    echo 'selected="selected"';
echo '>Lost Interest</option>';
echo '<option value="21"';
if ($icestate == 21)
    echo 'selected="selected"';
echo '>Disliked System</option>';
echo '<option value="22"';
if ($icestate == 22)
    echo 'selected="selected"';
echo '>Disliked SalesRep</option>';
echo '<option value="30"';
if ($icestate == 30)
    echo 'selected="selected"';
echo '>Spam Customer</option>';
echo '<option value="31"';
if ($icestate == 31)
    echo 'selected="selected"';
echo '>Test or Demo Customer</option>';
echo '</select>';
echo '</td><td>';
echo '<label style="display:none"><input name="Completed" id="Completed" type="checkbox" value="" ';
if (isset($icompleted) && ($icompleted == 1))
    echo 'checked="checked"';
echo ' /> Completed?</label>';
echo '</td></tr>';


echo '<tr><td></td><td>';
//echo 'Archive Reason:';
if (!isset($iarchivereason))
    $iarchivereason = '';
echo '<input type="hidden" name="ArchiveReason" id="ArchiveReason" disabled value="' . $iarchivereason . '"/>';
echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
echo '<input type="hidden" value="true" name="InPostBack" />';
echo '<button type="submit" value="" class="med">UPDATE CUSTOMER EXPERIENCE</button>';
echo '</form>';
echo '</td></tr>';
echo '</tbody></table>';
echo '</div>';



// currently in step section  
// echo '<br /><p style="color: rgb(133, 193, 27); font-size: 16px;"><strong>Current Step: '.$instep;.'</strong></p>';
echo '<h4 class="subhead"><button class="hideshow"  id="span_currently_in_step" onclick="javascript:showhidediv(\'currently_in_step\', \'span_currently_in_step\');"  >Hide</button>&nbsp;&nbsp;Currently in Step ' . $instep . '</h4>';
echo '<div id="currently_in_step" style="display:block">';
echo 'Coming Soon: key dates and facts - what would you like to know here?';
echo '</div>';
// admin or general manager - show the entire history of assignedreps
if (($uadmin == 'true') || ($uterr == 'true') || ($ufran == 'true') || ($ugm == 'true')) {
    echo '<h4 class="subhead"><button class="hideshow"  id="span_sales_rep_history" onclick="javascript:showhidediv(\'sales_rep_history\', \'span_sales_rep_history\');"  >Hide</button>&nbsp;&nbsp;Sales Rep History</h4>';

    echo '<div id="sales_rep_history" style="display:block">';
    $count = isset($asrepid) ? count($asrepid) : 0;
    for ($i = 0; $i < $count; $i++) {
        if ($i == 0)
            echo 'Rep is ';
        else
            echo 'Rep was ';

        echo $asurepname[$i] . ' as of ' . $asstart[$i];

        if (strlen($asend[$i]))
            echo 'to ' . $asend[$i];

        echo '<br/>';
    }

// assign buton
    echo '<form action="addassignedrep.php" method="post">';
    echo '<input type="hidden" value="' . $zipfranid . '" name="FranchiseeID" />';
    echo '<input type="hidden" value="' . $inmneed . '" name="AddMarketID" />';
    echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
    echo '<input type="hidden" value="' . $ifname . ' ' . $ilname . '" name="ForUserName" />';
    echo '<input type="hidden" value="' . $ititle . '" name="ForUserNeed" />';
    echo '<input type="hidden" value="Not Available" name="ForUserRef" />';
    if (!empty($asurepname))
        echo '<input type="hidden" value="' . $asurepname[0] . '" name="ForUserRep" />';
    echo '<button type="submit" value="" class="med">ASSIGN REP</button>';
    echo '</form></td>';
    echo '</div>';
}

if ($instep >= 3) {
    $count = count($oaccepted);
    $i = 0;
    $haveorders = 0;
    if ($count > 0) {
        echo '<br clear="all" /><br />';
// echo '<p style="color: rgb(133, 193, 27); font-size: 16px; margin-bottom: 5px;"><strong>';
// echo 'Orders and Watches';
// echo '</strong></p>';
        echo '<h4 class="subhead">Orders and Watches</h4>';
        $haveorders = 1;
        echo '<p style="color: rgb(133, 193, 27); font-size: 13px;"><strong>';
        echo 'Order';
        echo '</strong></p><table style="font-weight: bold; font-size: 13px; color: rgb(20, 44, 60); margin-left: 10px;" border="0" width="500"><tbody><tr><td width="174">';
        echo 'Order Type';
        echo '</td><td style="color: rgb(133, 193, 27);" width="316">';
        echo $oordertype[$i] . ' Order';
        echo '</td></tr><tr><td>';
        echo 'Date Ordered';
        echo '</td><td style="color: rgb(133, 193, 27);">';
        echo date_at_timezone('m/d/Y', 'EST', $oaccepted[$i]);
        echo '</td></tr><tr><td>';
        echo 'Exclusive Search Period';
        echo '</td><td style="color: rgb(133, 193, 27);">';

        echo '<form action="salesrepactions.php" method="post" name="orderchanges" onsubmit="javascript:return checkorder();" >';
//echo $owait[$i].' Days';
        echo '<input name="WaitPeriod" id="WaitPeriod" type="text" size="4" maxlength="3" value="';
        echo $owait[$i];
        echo '" onkeypress="javascript:return numbersonly(event);"/> Days';
//echo '<input type="hidden" value="true" name="InPostBack" />';
        echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
        echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
        echo '<input type="hidden" value="true" name="UpdateWaitPeriod" />';
        echo '<input type="hidden" value="' . $oorderid[0] . '" name="OrderID" />';
        echo '<button type="submit" value="" class="med" style="float:right;">UPDATE WAIT PERIOD</button>';
        echo '</form>';

        echo '</td></tr><tr><td>';
        echo 'Search Period Remaining';
        echo '</td><td style="color: rgb(133, 193, 27);">';
        $sec = time_diff(date_at_timezone('Y-m-d H:i:s', 'EST'), date_at_timezone('Y-m-d H:i:s', 'EST', $oaccepted[$i]));
        $days = $owait[$i] - round($sec / (60 * 60 * 24));
//$days = $owait[$i] - round((strtotime(date_at_timezone('m/d/Y H:i:s T', 'EST')) - strtotime(date_at_timezone('m/d/Y', 'EST', $oaccepted[$i]))) / (60 * 60 * 24));
        if ($days < 0)
            echo 'Expired';
        else
            echo $days . ' Days';
        echo '</td></tr><tr><td>';
        echo 'Expiration Date';
        echo '</td><td style="color: rgb(133, 193, 27);">';
        if (isset($oexpires[$i]))
            echo date_at_timezone('m/d/Y', 'EST', $oexpires[$i]);
        else
            echo 'Not Set';
        echo '</td></tr><tr><td>';
        $plural = pluralize_noun($count, "Current Market Stud", "ies", "y");
        echo "$plural Attached";
        echo '</td><td style="color: rgb(133, 193, 27);">';
        echo '[' . str_pad($aquoteid[$index], 5, "0", STR_PAD_LEFT) . '] ' . $oyear[$i] . ' ' . $omake[$i] . ' ' . $omodel[$i] . ' ' . $ostyle[$i];
        echo '</td></tr>';
        for ($i = 1; $i < $count; $i++) {
            echo '<tr><td>&nbsp;</td><td style="color: rgb(133, 193, 27);">';
            echo $oyear[$i] . ' ' . $omake[$i] . ' ' . $omodel[$i] . ' ' . $ostyle[$i];
            echo '</td></tr>';
        }
        if ($odepreq[0] == 1) {
            echo '<tr style="color: rgb(20, 44, 60);"><td>';
            echo 'Deposit Amount';
            echo '</td><td style="color: rgb(133, 193, 27);">$';
            echo number_format($odepneeded[0], 0);
            echo '</td></tr><tr><td>';
            echo 'Deposit Received';
            echo '</td><td valign="baseline">';

            echo '<form action="salesrepactions.php" method="post" name="depositchanges" >';
            echo '<input name="DepositAmount" id="DepositAmount" type="text" size="5" maxlength="4" value="';
//echo number_format($odeprec[0],0);
            echo substr($odeprec[0], 0, strpos($odeprec[0], '.'));
            echo '" onkeypress="javascript:return numbersonly(event);" />';
            echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
            echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
            echo '<input type="hidden" value="true" name="UpdateDeposit" />';
            echo '<input type="hidden" value="' . $oorderid[0] . '" name="OrderID" />';
            echo '<button type="submit" value="" class="med" style="float:right;">UPDATE DEPOSIT RECEIVED</button>';
            echo '</form>';
//if($odeprec[0] > 0.00) echo '$'.number_format($odeprec[0],2);
//else echo 'No';
            echo '</td></tr>';
        }
        echo '</tbody></table><br clear="all" />';
//echo '<button type="submit" value="" class="med" style="margin-top:15px;">MARK AS RECEIVED</button>';

        $addsvqok = 'true';
        $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
        if ($con) {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $query = "select v.Year, m.Name, v.Model, v.Style, s.SpecificVehicleID from specificvehicles s, vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=s.VehicleDetailID and s.MarketNeedID =" . $inmneed . " and s.OrderID=" . $oorderid[0];
            $result = mysql_query($query, $con);
            if ($result && $row = mysql_fetch_array($result)) {
                echo '<table style="font-weight: bold; font-size: 13px; color: rgb(20, 44, 60); margin-left: 10px;" border="0" width="500"><tbody>';
                echo '<tr>';
                echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Specific<br />Vehicle Quote(s) Attached</td>';
                echo '<td valign="bottom" style="color:#757575; font-size:12px;">';
                echo '<a href="invdetails.php?SVQID=' . $row[4] . '&ForUserID=' . $inuserid . '&MarketNeedID=' . $inmneed . '">';
                echo $row[0] . ' ' . $row[1] . ' ' . $row[2] . ' ' . $row[3];
                echo '</a>';
                echo '</td>';
                echo '</tr>';

// Get the current Purchase Approval Information (if any)...
                $pquery = 'select Status, ChosenOn, BoughtOn from purchases where SpecificVehicleID = ' . $row[4];
                $presult = mysql_query($pquery, $con);
                if ($prow = mysql_fetch_array($presult)) {
                    echo '<tr>';
                    echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Approved</td>';
                    echo '<td style="color:#757575; font-size:12px;">' . date_at_timezone('m/d/Y', 'EST', $prow[1]) . '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp; </td>';
                    if ($prow[0] != 'Bought')
                        echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE NOT MADE</td>';
                    else {
                        echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE MADE</td>';
                        $addsvqok = 'false';
                    }
                    echo '</tr>';

                    $query = "update purchases set SalesReviewed=1,SalesRepID=" . $userid . " where SpecificVehicleID=" . $row[4];
                    mysql_query($query, $con);
                }

                while ($row = mysql_fetch_array($result)) {
                    echo '<tr>';
                    echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp;</td>';
                    echo '<td valign="bottom" style="color:#757575; font-size:12px;">';
                    echo '<a href="invdetails.php?SVQID=' . $row[4] . '&ForUserID=' . $inuserid . '&MarketNeedID=' . $inmneed . '">';
                    echo $row[0] . ' ' . $row[1] . ' ' . $row[2] . ' ' . $row[3];
                    echo '</a>';
                    echo '</td>';
                    echo '</tr>';

// Get the current Purchase Approval Information (if any)...
                    $pquery = 'select Status, ChosenOn, BoughtOn from purchases where SpecificVehicleID = ' . $row[4];
                    $presult = mysql_query($pquery, $con);
                    if ($prow = mysql_fetch_array($presult)) {
                        echo '<tr>';
                        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Approved</td>';
                        echo '<td style="color:#757575; font-size:12px;">' . date_at_timezone('m/d/Y', 'EST', $prow[1]) . '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp; </td>';
                        if ($prow[0] != 'Bought')
                            echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE NOT MADE</td>';
                        else {
                            echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE MADE</td>';
                            $addsvqok = 'false';
                        }
                        echo '</tr>';

                        $query = "update purchases set SalesReviewed=1,SalesRepID=" . $userid . " where SpecificVehicleID=" . $row[4];
                        mysql_query($query, $con);
                    }
                }
                echo '</tbody></table>';
            }

            mysql_close($con);
        }
        echo '<br clear="all" /><br />';
        if ($addsvqok == 'true') {
            echo '<form action="addsvq.php" method="post">';
            echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
            echo '<input type="hidden" value="' . $inmneed . '" name="ForNeedID" />';
            echo '<input type="hidden" value="' . $oorderid[0] . '" name="ForOrderID" />';
            echo '<button type="submit" value="" class="med">ADD SVQ TO ORDER</button>';
            echo '</form>';
            echo '<br clear="all" /><br />';
        }
    }

    $count = count($waccepted);
    $i = 0;
    if ($count > 0) {
        if ($haveorders == 0) {
            echo '<br clear="all" /><br />';
// echo '<p style="color: rgb(133, 193, 27); font-size: 16px; margin-bottom: 5px;"><strong>';
// echo 'Orders and Watches';
// echo '</strong></p>';
            echo '<h4 class="subhead"><h4 class="subhead">Orders and Watches</h4></h4>';
        }
// echo '<p style="color: rgb(133, 193, 27); font-size: 13px;"><strong>';
// echo 'Watch List';
// echo '</strong></p>';
        echo '<h4 class="subhead">Watch List</h4>';
        echo '<table style="font-weight: bold; font-size: 13px; color: rgb(20, 44, 60); margin-left: 10px;" border="0" width="500"><tbody><tr><td width="174">';
        echo 'Order Type';
        echo '</td><td style="color: rgb(133, 193, 27);" width="316">';
        echo $wordertype[0] . ' Order';
        echo '</td></tr><tr><td>';
        echo 'Date Ordered';
        echo '</td><td style="color: rgb(133, 193, 27);">';
        echo date_at_timezone('m/d/Y', 'EST', $waccepted[0]);
        echo '</td></tr><tr><td>';
        echo 'Expiration Date';
        echo '</td><td style="color: rgb(133, 193, 27);">';
        if (isset($wexpires[0]))
            echo date_at_timezone('m/d/Y', 'EST', $wexpires[0]);
        else
            echo 'Not Set';
        echo '</td></tr>';
        echo '<tr><td>';
        $plural = pluralize_noun($count, "Current Market Stud", "ies", "y");
        echo "$plural Attached";
        echo '</td><td style="color: rgb(133, 193, 27);">';
        echo $wyear[$i] . ' ' . $wmake[$i] . ' ' . $wmodel[$i] . ' ' . $wstyle[$i];
        echo '</td></tr>';
        for ($i = 1; $i < $count; $i++) {
            echo '<tr><td>&nbsp;</td><td style="color: rgb(133, 193, 27);">' . $wyear[$i] . ' ' . $wmake[$i] . ' ' . $wmodel[$i] . ' ' . $wstyle[$i] . '</td></tr>';
        }
        echo '</tbody></table><br clear="all" />';

        $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
        if ($con) {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $query = "select v.Year, m.Name, v.Model, v.Style, s.SpecificVehicleID from specificvehicles s, vehicledetails v, makes m where m.MakeID=v.MakeID and v.VehicleDetailID=s.VehicleDetailID and s.MarketNeedID =" . $inmneed . " and s.WatchID=" . $wwatchid[0];
            $result = mysql_query($query, $con);
            if ($result && $row = mysql_fetch_array($result)) {
                echo '<table style="font-weight: bold; font-size: 13px; color: rgb(20, 44, 60); margin-left: 10px;" border="0" width="500"><tbody>';
                echo '<tr>';
                echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Specific<br />Vehicle Quote(s) Attached</td>';
                echo '<td valign="bottom" style="color:#757575; font-size:12px;">';
                echo '<a href="invdetails.php?SVQID=' . $row[4] . '&ForUserID=' . $inuserid . '&MarketNeedID=' . $inmneed . '">';
                echo $row[0] . ' ' . $row[1] . ' ' . $row[2] . ' ' . $row[3];
                echo '</a>';
                echo '</td>';
                echo '</tr>';

// Get the current Purchase Approval Information (if any)...
                $pquery = 'select Status, ChosenOn, BoughtOn from purchases where SpecificVehicleID = ' . $row[4];
                $presult = mysql_query($pquery, $con);
                if ($prow = mysql_fetch_array($presult)) {
                    echo '<tr>';
                    echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Approved</td>';
                    echo '<td style="color:#757575; font-size:12px;">' . date_at_timezone('m/d/Y', 'EST', $prow[1]) . '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp; </td>';
                    if ($prow[0] != 'Bought')
                        echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE NOT MADE</td>';
                    else
                        echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE MADE</td>';
                    echo '</tr>';

                    $query = "update purchases set SalesReviewed=1,SalesRepID=" . $userid . " where SpecificVehicleID=" . $row[4];
                    mysql_query($query, $con);
                }

                while ($row = mysql_fetch_array($result)) {
                    echo '<tr>';
                    echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp;</td>';
                    echo '<td valign="bottom" style="color:#757575; font-size:12px;">';
                    echo '<a href="invdetails.php?SVQID=' . $row[4] . '&ForUserID=' . $inuserid . '&MarketNeedID=' . $inmneed . '">';
                    echo $row[0] . ' ' . $row[1] . ' ' . $row[2] . ' ' . $row[3];
                    echo '</a>';
                    echo '</td>';
                    echo '</tr>';

// Get the current Purchase Approval Information (if any)...
                    $pquery = 'select Status, ChosenOn, BoughtOn from purchases where SpecificVehicleID = ' . $row[4];
                    $presult = mysql_query($pquery, $con);
                    if ($prow = mysql_fetch_array($presult)) {
                        echo '<tr>';
                        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">Approved</td>';
                        echo '<td style="color:#757575; font-size:12px;">' . date_at_timezone('m/d/Y', 'EST', $prow[1]) . '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp; </td>';
                        if ($prow[0] != 'Bought')
                            echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE NOT MADE</td>';
                        else
                            echo '<td style="color:#85c11b; font-size:12px; font-weight:bold;">WHOLESALE PURCHASE MADE</td>';
                        echo '</tr>';

                        $query = "update purchases set SalesReviewed=1,SalesRepID=" . $userid . " where SpecificVehicleID=" . $row[4];
                        mysql_query($query, $con);
                    }
                }
                echo '</tbody></table><br clear="all" />';
            }

            mysql_close($con);
        }
        echo '<form action="addsvq.php" method="post">';
        echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
        echo '<input type="hidden" value="' . $inmneed . '" name="ForNeedID" />';
        echo '<input type="hidden" value="' . $wwatchid[0] . '" name="ForGroupID" />';
        echo '<button type="submit" value="" class="med">ADD SVQ TO WATCH</button>';
        echo '</form>';
        echo '<br clear="all" /><br />';
    }
}
if ($instep >= 1) {
    echo '<br clear="all" /><br />';
// echo '<p style="color: rgb(133, 193, 27); font-size: 16px; margin-bottom: 5px;"><strong>';
// echo 'Quote Requests';
// echo '</strong></p>';
    echo '<h4 class="subhead">Quote Requests</h4>';
    echo '<form action="addfirmquote.php" method="post">';
    echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
    echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
    echo '<input type="hidden" value="-1" name="ForQuoteRequestID" />';
    echo '<button type="submit" value="" class="med">ADD NEW QUOTE REQUEST AND FIRM QUOTE (TODO: what is a Quote Request versus a Firm Quote?)</button>';
    echo '</form><br/>';
}
if ($instep >= 2) {
    if ($ahavequotes == 1) {
        echo '<center><img src="common/layout/short-bar.gif" style="margin-left: -30px; padding: 10px;" border="0" /></center><br/>';
        $count = count($aquoteid);
        for ($index = 0; $index < $count; $index++) {
            echo '<table style="font-size: 13px; font-weight: bold; color: rgb(20, 44, 60);" border="0" cellpadding="3" width="596"><tbody>';
            echo '<tr><td width="112" style="color: rgb(133, 193, 27);"><strong>';
            echo 'Quote #';
            echo '</strong></td><td style="color: rgb(0, 0, 0);" width="253"><strong>';
            echo str_pad($aquoteid[$index], 5, "0", STR_PAD_LEFT);
            echo '</strong></td><td width="117">&nbsp</td><td width="104">&nbsp</td></tr>';
            echo '<tr><td>';
            echo 'Quote Type';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            switch ($aquotetype[$index]) {
                case 'Standard':
                    echo 'Current Market Study Request<br/>';
                    break;
                case 'Special':
                    echo 'Special Current Market Study Request<br/>';
                    break;
                case 'Pickup':
                    echo 'Pick-up Current Market Study Request<br/>';
                    break;
            }
            echo '</td><td>';
            echo 'Power Doors';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $apdoor[$index];
            echo '</td></tr><tr><td>';
            echo 'Vehicle';
            echo '</td><td style="color: rgb(133, 193, 27);">';
//if($aquotetype[$index] == 'Standard') echo '<a href="#">';
            echo $ayear[$index] . ' ' . $amake[$index] . ' ' . $amodel[$index] . ' ' . $astyle[$index];
//if($aquotetype[$index] == 'Standard') echo '</a>';
            echo '</td><td>';
            echo 'Power Windows';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $apwin[$index];
            echo '</td></tr><tr><td>';
            echo 'Visible';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            if ($avisible[$index] == 1)
                echo 'Yes';
            else
                echo 'No';
            echo '</td><td>';
            echo 'Power Seats';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $apseat[$index];
            echo '</td></tr><tr><td>';
            echo 'Mileage Ceiling';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo number_format($aqmileage[$index]);
            echo '</td><td>';
            echo 'Heated Seats';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $aheat[$index];
            echo '</td></tr><tr><td>';
            echo 'Seat Material';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $aseattype[$index];
            echo '</td><td>';
            echo 'Air Conditioning';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $aair[$index];
            echo '</td></tr><tr><td>';
            echo 'Engine Cylinders';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $aengine[$index];
            echo '</td><td>';
            echo 'Remote Entry';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $aremote[$index];
            echo '</td></tr><tr><td>';
            echo 'Wheel Drive';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $adrive[$index];
            echo '</td><td>';
            echo 'Traction Control';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $atraction[$index];
            echo '</td></tr><tr><td>';
            echo 'Transmission';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $atrans[$index];
            echo '</td><td>';
            echo 'Security System';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $asecure[$index];
            echo '</td></tr><tr><td>';
            echo 'Wheel Covers';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $awheels[$index];
            echo '</td><td>';
            echo 'Cruise Control';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $acruise[$index];
            echo '</td></tr><tr><td>';
            if ($aquotetype[$index] == 'Pickup') {
                echo 'Cab Type';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $acab[$index];
            } else {
                echo 'Sunroof';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $asunroof[$index];
            }
            echo '</td><td>';
            echo 'Navigation';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            echo $anavsys[$index];
            echo '</td></tr><tr><td>';
            echo 'Radio Needed';
            echo '</td><td style="color: rgb(133, 193, 27);">';
            if ($aradflex[$index] == 'No')
                echo 'No';
            else {
                echo $aradflex[$index];
                if ($aradfm[$index] == 1)
                    echo ' (AM/FM';
                if ($aradcas[$index] == 1) {
                    if ($aradfm[$index] == 1)
                        echo ',CAS';
                    else
                        echo ' (CAS';
                }
                if ($aradcd[$index] == 1) {
                    if (($aradfm[$index] == 1) || ($aradcas[$index] == 1))
                        echo ',CD';
                    else
                        echo ' (CD';
                }
                if (($aradfm[$index] == 1) || ($aradcas[$index] == 1) || ($aradcd[$index] == 1))
                    echo ')';
            }
            echo '</td><td>';
            if ($aquotetype[$index] == 'Pickup') {
                echo 'Rear Sliding';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $arearwin[$index];
            } else {
                echo 'Luggage Rack';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $aluggage[$index];
            }
            echo '</td></tr>';
            if ($aquotetype[$index] == 'Pickup') {
                echo '<tr><td>';
                echo 'Front Seat Type';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $afront[$index];
                echo '</td><td>';
                echo 'Bed Type';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $abedtype[$index];
                echo '</td></tr><tr><td>';
                echo 'Bed Liner';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $abed[$index];
                echo '</td><td>';
                echo 'Tow Package';
                echo '</td><td style="color: rgb(133, 193, 27);">';
                echo $atow[$index];
                echo '</td></tr>';
            }
            echo '<tr><td>&nbsp;</td><td style="color: rgb(133, 193, 27);">&nbsp;</td><td>&nbsp;</td><td style="color: rgb(133, 193, 27);">&nbsp;</td></tr>';
            echo '<tr><td colspan="2">';
            echo 'Color Combinations';
            echo '</td><td colspan="2">';
            echo 'Special Requests';
            echo '</td></tr><tr><td colspan="2" style="color: rgb(133, 193, 27);" valign="top">';
            echo $acolors[$index];
            echo '</td><td colspan="2" style="color: rgb(133, 193, 27);" valign="top">';
            echo $anotes[$index];
            echo '</td></tr>';
            echo '<tr><td colspan="2" valign="top">';
            echo 'Deliver to ';
            echo '<span style="color: rgb(133, 193, 27);">';
            if ($adelorpick[$index] == 1)
                echo 'Dealership';
            else
                echo $adelcity[$index] . ', ' . $adelstate[$index];
            echo '</span></td><td colspan="2" style="color: rgb(133, 193, 27);" valign="top">&nbsp;</td></tr>';
            echo '</tbody></table><br clear="all" />';
            if (isset($fquoteid[$index])) {
                echo '<br/><strong>Current Market Study Data:</strong><br/>';
                echo 'Status: ';
                if ($fstatus[$index] == 1)
                    echo 'Researching<br/>';
                else
                    echo 'Completed<br/>';
                echo 'Order Type: ' . $fordertype[$index] . '<br/>';
                echo 'Estimated Miles: ' . number_format($fmiles[$index]) . '<br/>';
//echo 'High Miles: '.number_format($fhighmiles[$index]).'<br/>';
                echo 'Price Point: $' . number_format($fprice[$index]) . '<br/>';
                $imagefile = '';
                if (isset($fimage[$index])) {
                    if (!file_exists($fimage[$index]))
                        $imagefile = '';
                    else
                        $imagefile = $fimage[$index];
                }

                if (!isset($imagefile) || (strlen($imagefile) < 1)) {
                    if ($aquotetype[$index] == 'Standard') {
                        $con = mysql_connect(DB_SERVER_HOSTNAME, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
                        if ($con) {
// First fill the list of Makes...
                            mysql_select_db(DB_SERVER_DATABASE, $con);

                            $result = mysql_query("select v.ImageFile from vehicles v, makes m where m.makeid=v.makeid and m.name='" . $amake[$index] . "' and v.year=" . $ayear[$index] . " and v.model='" . $amodel[$index] . "' and v.style='" . $astyle[$index] . "'");
                            if ($result && $row = mysql_fetch_array($result)) {
                                if (strlen($row[0]) > 0)
                                    $imagefile = $row[0];
                            } else
                                $imagefile = '';
                            mysql_close($con);
                        } else
                            $imagefile = '';
                    }
                }

                if (isset($imagefile) && (strlen($imagefile) > 0)) {
                    echo 'Current Image:<br/>';
                    $max_width = 300;
                    $max_height = 300;
                    echo '<img id="vehimage" src="loadimage.php?image=' . $imagefile . '&mwidth=' . $max_width . '&mheight=' . $max_height . '" border="0" hspace="10" vspace="10" />';
                }

                if (isset($fnote[$index]) && (strlen($fnote[$index]) > 0))
                    echo '<br/>Notes: ' . $fnote[$index] . '<br/>';
            }
            if ($avisible[$index] == 1) {
                echo '<form action="addfirmquote.php" method="post">';
                echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
                echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
                if (isset($fquoteid[$index]))
                    echo '<input type="hidden" value="' . $fquoteid[$index] . '" name="FirmQuoteID" />';
                echo '<input type="hidden" value="' . $aquoteid[$index] . '" name="ForQuoteRequestID" />';
                echo '<button type="submit" value="" class="med">COMPLETE CURRENT MARKET STUDY</button>';
                echo '</form>';
            }
            if ($index < ($count - 1))
                echo '<center><img src="common/layout/short-bar.gif" style="margin-left: -30px; padding: 10px;" border="0" /></center>';
        }
    }
}
// trade in section
if (isset($itdesc)) {
// echo '<p style="color: rgb(133, 193, 27); font-size: 16px;"><strong>Trade In Details:</strong></p>';
    echo '<h4 class="subhead">Trade In Details</h4>';
    echo '<table align="left" border="0" cellpadding="0"><tbody>';
    echo '<tr><td style="color: rgb(20, 44, 60); font-weight: bold; font-size: 13px;">';
    echo 'Decription';
    echo '</td><td style="color: rgb(133, 193, 27); font-size: 13px; font-weight: bold;">';
    echo $itdesc;
    echo '</td></tr>';
    echo '<tr><td style="color: rgb(20, 44, 60); font-weight: bold; font-size: 13px;">';
    echo 'Updated';
    echo '</td><td style="color: rgb(133, 193, 27); font-size: 13px; font-weight: bold;">';
    echo date_at_timezone('m/d/Y H:i:s T', 'EST', $itlastup);
    echo '</td></tr>';
    $count = count($itimage);
    $max_width = 550;
    $max_height = 550;
    for ($index = 0; $index < $count; $index++) {
// TODO show three across and clicking on the picture zooms in.
        echo '<tr><td colspan="2" align="center">';
        echo '<img src="loadimage.php?image=' . $itimage[$index] . '&mwidth=' . $max_width . '&mheight=' . $max_height . '" border="0" hspace="10" vspace="10" />';
        echo '</td></tr>';
    }
    echo '</tbody></table>';
    echo '<form action="addinventory.php" method="post">';
    echo '<input type="hidden" value="' . $inmneed . '" name="MarketNeedID" />';
    echo '<input type="hidden" value="' . $inuserid . '" name="ForUserID" />';
    echo '<input type="hidden" value="true" name="TradeIn" />';
    echo '<button type="submit" value="" class="med">ADD TRADE-IN DETAILS TO SYSTEM</button>';
    echo '</form>';
}

if ($instep >= 2) {
    if ($ahaveassess == 1) {
        ?>
                        <h4 class="subhead"> <button onclick="javascript:showhidediv('assessment', 'span_assessment');" id="span_assessment" class="hideshow">Hide</button>&nbsp;&nbsp;Assessment</h4>
                        <div style="display: block;" id="assessment">            
                            <table border="0" style="float: left; color: rgb(20, 44, 60); width:100%">
                                <tbody>
        <?php if ($VehicleType) { ?>
                                        <tr>
                                            <td style="width: 33%;"><strong> Vehicle Type  </strong></td>
                                            <td style="width: 65%;"> 
                                        <?= $VehicleType ?>  </td>
                                        </tr>  
        <?php } ?>
                                            <?php if ($BodyType) { ?>
                                        <tr>
                                            <td><strong> Vehicle Type  </strong></td>
                                            <td> 
                                        <?= $BodyType ?>  </td>
                                        </tr>  
        <?php } ?>                                    
                                            <?php if ($VehicleSize) { ?>
                                        <tr>
                                            <td ><strong> Vehicle Size  </strong></td>
                                            <td > 
                                        <?= $VehicleSize ?>  </td>
                                        </tr>  
        <?php } ?>
                                            <?php if ($DriveTrain) { ?>
                                        <tr>
                                            <td ><strong> Drive Train  </strong></td>
                                            <td > 
                                        <?= $DriveTrain ?>  </td>
                                        </tr>  
        <?php } ?>
                                            <?php if ($Notes) { ?>
                                        <tr>
                                            <td ><strong> Notes  </strong></td>
                                            <td > 
                                        <?= $Notes ?>  </td>
                                        </tr>  
        <?php } ?>
                                            <?php if ($Origin) { ?>
                                        <tr>
                                            <td ><strong> Origin  </strong></td>
                                            <td > 
                                        <?= $Origin ?>  </td>
                                        </tr>  
        <?php } ?>
                                            <?php if ($Transmission) { ?>
                                        <tr>
                                            <td ><strong> Transmission  </strong></td>
                                            <td > 
                                        <?= $Transmission ?>  </td>
                                        </tr>  
        <?php } ?>
                                            <?php if ($MileageFrom && $MileageTo) { ?>
                                        <tr>
                                            <td ><strong> Mileage you are hoping for </strong></td>
                                            <td > 
                                        <?= $MileageFrom . ' To ' . $MileageTo ?>  </td>
                                        </tr>  
        <?php } ?>
                                            <?php if ($MileageCeiling) { ?>
                                        <tr>
                                            <td ><strong> Mileage Ceiling  </strong></td>
                                            <td > 
                                        <?= $MileageCeiling ?>  </td>
                                        </tr> 
        <?php } ?>

                                    <tr>
                                        <td colspan="2" class="assessment_insidetd"><strong class="assessment_inside"> Exterior Color Preferences::  </strong></td>                                        
                                    </tr>  

        <?php if ($ExtLike) { ?>
                                        <tr>
                                            <td ><strong> Colors you like  </strong></td>
                                            <td > 
                                        <?= $ExtLike ?>  </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($ExtDislike) { ?>
                                        <tr>
                                            <td ><strong> Colors you dislike  </strong></td>
                                            <td > 
                                        <?= $ExtDislike ?>  </td>
                                        </tr> 
        <?php } ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="assessment_insidetd"><strong class="assessment_inside"> Interior Color Preferences::  </strong></td>                                        
                                    </tr>
        <?php if ($IntLike) { ?>
                                        <tr>
                                            <td ><strong> Colors you like  </strong></td>
                                            <td > 
                                        <?= $IntLike ?>  </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($IntDisike) { ?>
                                        <tr>
                                            <td ><strong> Colors you dislike  </strong></td>
                                            <td > 
                                        <?= $IntDisike ?> </td>
                                        </tr> 
        <?php } ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
        <?php if ($BudgetFrom && $BudgetTo) { ?>
                                        <tr>
                                            <td ><strong> Budget Range(without tax)  </strong></td>
                                            <td > 
                                        <?= '$ '.$BudgetFrom . ' T0 $ ' . $BudgetTo; ?> </td>
                                        </tr> 
        <?php } ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="assessment_insidetd"><strong class="assessment_inside"> If Borrowing:: </strong></td>                                        
                                    </tr>
        <?php if ($BorrowMaxPayment) { ?>
                                        <tr>
                                            <td ><strong> Maximum payment is </strong></td>
                                            <td > 
                                        <?= '$ '.$BorrowMaxPayment ?> </td>
                                        </tr> 
        <?php } ?>

                                            <?php if ($BorrowDownPayment) { ?>
                                        <tr>
                                            <td ><strong> Down payment is </strong></td>
                                            <td > 
                                        <?= '$ '.$BorrowDownPayment ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($VehicleNeed) { ?>
                                        <tr>
                                            <td ><strong> How soon do you need a vehicle? </strong></td>
                                            <td > 
                                        <?= $VehicleNeed ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($BrandLike) { ?>
                                        <tr>
                                            <td ><strong> Brands you like </strong></td>
                                            <td > 
                                        <?= $BrandLike ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($BrandDislike) { ?>
                                        <tr>
                                            <td ><strong> Brands you dislike </strong></td>
                                            <td > 
                                        <?= $BrandDislike ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($Model) { ?>
                                        <tr>
                                            <td ><strong> Makes & Models you would consider </strong></td>
                                            <td > 
                                        <?= $Model ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($VehicleAge) { ?>
                                        <tr>
                                            <td ><strong> Age of vehicle? </strong></td>
                                            <td > 
                                        <?= $VehicleAge ?> </td>
                                        </tr> 
        <?php } ?>
                      

                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="assessment_insidetd"><strong class="assessment_inside"> Rate your need for:: </strong></td>                                        
                                    </tr>
        <?php if ($RateFuelEfficiency) { ?>
                                        <tr>
                                            <td ><strong> Fuel efficiency? </strong></td>
                                            <td > 
                                        <?= $RateFuelEfficiency ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($RateMintenanceCost) { ?>
                                        <tr>
                                            <td ><strong> Low maintenance costs </strong></td>
                                            <td > 
                                        <?= $RateMintenanceCost ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($RateReliability) { ?>
                                        <tr>
                                            <td ><strong> Reliability </strong></td>
                                            <td > 
                                        <?= $RateReliability ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($RateLuxury) { ?>
                                        <tr>
                                            <td ><strong> Luxury "stuff" </strong></td>
                                            <td > 
                                        <?= $RateLuxury ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($RateSporty) { ?>
                                        <tr>
                                            <td ><strong> Sporty "stuff" </strong></td>
                                            <td > 
                                        <?= $RateSporty ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($RateSafety) { ?>
                                        <tr>
                                            <td ><strong> Safety </strong></td>
                                            <td > 
                                        <?= $RateSafety ?> </td>
                                        </tr> 
        <?php } ?>
                     
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="assessment_insidetd"><strong class="assessment_inside"> Do you prefer:: </strong></td>                                        
                                    </tr>

        <?php if ($VehicleType == "Auto") { ?>
            <?php if ($Leather) { ?>
                                            <tr>
                                                <td ><strong> Leather </strong></td>
                                                <td > 
                                            <?= $Leather ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($HeatedSeat) { ?>
                                            <tr>
                                                <td ><strong> Heated Seats </strong></td>
                                                <td > 
                                            <?= $HeatedSeat ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($Navigation) { ?>
                                            <tr>
                                                <td ><strong> Navigation </strong></td>
                                                <td > 
                                            <?= $Navigation ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($SunRoof) { ?>
                                            <tr>
                                                <td ><strong> Sunroof </strong></td>
                                                <td > 
                                            <?= $SunRoof ?> </td>
                                            </tr> 
            <?php } ?>

                                                <?php if ($AlloyWheels) { ?>
                                            <tr>
                                                <td ><strong> Alloy Wheels </strong></td>
                                                <td > 
                                            <?= $AlloyWheels ?> </td>
                                            </tr> 
            <?php } ?>

                                            <?php } else if ($VehicleType == 'Minivan') { ?>
                                        <?php if ($Leather) { ?>
                                            <tr>
                                                <td ><strong> Leather </strong></td>
                                                <td > 
                                            <?= $Leather ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($HeatedSeat) { ?>
                                            <tr>
                                                <td ><strong> Heated Seats </strong></td>
                                                <td > 
                                            <?= $HeatedSeat ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($Navigation) { ?>
                                            <tr>
                                                <td ><strong> Navigation </strong></td>
                                                <td > 
                                            <?= $Navigation ?> </td>    
                                            </tr> 
            <?php } ?>
                                                <?php if ($SunRoof) { ?>
                                            <tr>
                                                <td ><strong> Sunroof </strong></td>
                                                <td > 
                                            <?= $SunRoof ?> </td>
                                            </tr> 
            <?php } ?>

                                                <?php if ($AlloyWheels) { ?>
                                            <tr>
                                                <td ><strong> Alloy Wheels </strong></td>
                                                <td > 
                                            <?= $AlloyWheels ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($EntertainmentSystem) { ?>
                                            <tr>
                                                <td ><strong> Entertainment System </strong></td>
                                                <td > 
                                            <?= $EntertainmentSystem ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($PRHatch) { ?>
                                            <tr>
                                                <td ><strong> Power Rear Hatch </strong></td>
                                                <td > 
                                            <?= $PRHatch ?> </td>
                                            </tr> 
            <?php } ?>

                                                <?php if ($BackupCamera) { ?>
                                            <tr>
                                                <td ><strong> Backup Camera </strong></td>
                                                <td > 
                                            <?= $BackupCamera ?> </td>
                                            </tr> 
            <?php } ?>



                                    <?php } else if ($VehicleType == 'SUV') { ?>
            <?php if ($Leather) { ?>
                                            <tr>
                                                <td ><strong> Leather </strong></td>
                                                <td > 
                                            <?= $Leather ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($HeatedSeat) { ?>
                                            <tr>
                                                <td ><strong> Heated Seats </strong></td>
                                                <td > 
                                            <?= $HeatedSeat ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($SunRoof) { ?>
                                            <tr>
                                                <td ><strong> Navigation </strong></td>
                                                <td > 
                                            <?= $SunRoof ?> </td>
                                            </tr> 
            <?php } ?>


                                        <?php if ($AlloyWheels) { ?>
                                            <tr>
                                                <td ><strong> Alloy Wheels </strong></td>
                                                <td > 
                                            <?= $AlloyWheels ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($EntertainmentSystem) { ?>
                                            <tr>
                                                <td ><strong> Entertainment System </strong></td>
                                                <td > 
                                            <?= $EntertainmentSystem ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($ThirdRD) { ?>
                                            <tr>
                                                <td ><strong> Third Row Seating </strong></td>
                                                <td > 
                                            <?= $ThirdRD ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($CRow) { ?>
                                            <tr>
                                                <td ><strong> Captain chairs center row </strong></td>
                                                <td > 
                                            <?= $CRow ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($PRHatch) { ?>
                                            <tr>
                                                <td ><strong> Power Rear Hatch </strong></td>
                                                <td > 
                                            <?= $PRHatch ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($BackupCamera) { ?>
                                            <tr>
                                                <td ><strong> Backup Camera </strong></td>
                                                <td > 
                                            <?= $BackupCamera ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($TPackage) { ?>
                                            <tr>
                                                <td ><strong> Tow package </strong></td>
                                                <td > 
                                            <?= $TPackage ?> </td>
                                            </tr> 
            <?php } ?>


                                    <?php } else if ($VehicleType == 'Pickup') { ?>
                                        <?php if ($FrontSType) { ?>
                                            <tr>
                                                <td ><strong> Front Seat Type </strong></td>
                                                <td > 
                                            <?= $FrontSType ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($BedType) { ?>
                                            <tr>
                                                <td ><strong> Bed Type </strong></td>
                                                <td > 
                                            <?= $BedType ?> </td>
                                            </tr> 
            <?php } ?>

                                                <?php if ($Leather) { ?>
                                            <tr>
                                                <td ><strong> Leather </strong></td>
                                                <td > 
                                            <?= $Leather ?> </td>
                                            </tr> 
            <?php } ?>

                                                <?php if ($HeatedSeat) { ?>
                                            <tr>
                                                <td ><strong> Heated Seats </strong></td>
                                                <td > 
                                            <?= $HeatedSeat ?> </td>
                                            </tr> 
            <?php } ?>

                                                <?php if ($Navigation) { ?>
                                            <tr>
                                                <td ><strong> Navigation </strong></td>
                                                <td > 
                                            <?= $Navigation ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($SunRoof) { ?>
                                            <tr>
                                                <td ><strong> Sunroof </strong></td>
                                                <td > 
                                            <?= $SunRoof ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($AlloyWheels) { ?>
                                            <tr>
                                                <td ><strong> Alloy Wheels </strong></td>
                                                <td > 
                                            <?= $AlloyWheels ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($RearWindow) { ?>
                                            <tr>
                                                <td ><strong> Rear Sliding Window </strong></td>
                                                <td > 
                                            <?= $RearWindow ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($BedLiner) { ?>
                                            <tr>
                                                <td ><strong> Bed Liner </strong></td>
                                                <td > 
                                            <?= $BedLiner ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($BackupCamera) { ?>
                                            <tr>
                                                <td ><strong> Backup Camera </strong></td>
                                                <td > 
                                            <?= $BackupCamera ?> </td>
                                            </tr> 
            <?php } ?>
                                                <?php if ($TPackage) { ?>
                                            <tr>
                                                <td ><strong> Tow package </strong></td>
                                                <td > 
                                            <?= $TPackage ?> </td>
                                            </tr> 
            <?php } ?>

                                            <?php } ?>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
        <?php if ($OtherPrefereces) { ?>
                                        <tr>
                                            <td ><strong> Other things I must have </strong></td>
                                            <td > 
                                        <?= $OtherPrefereces ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($OtherReallyHave) { ?>
                                        <tr>
                                            <td ><strong> Other things I would really like to have </strong></td>
                                            <td > 
                                        <?= $OtherReallyHave ?> </td>
                                        </tr> 
        <?php } ?>
                                            <?php if ($AdditionalInfo) { ?>
                                        <tr>
                                            <td ><strong> Additional Information </strong></td>
                                            <td > 
                                        <?= $AdditionalInfo ?> </td>
                                        </tr> 
        <?php } ?>

                                        
                                        <td align="center" colspan="2">
                                           
                                        <form action="<?=WEB_SERVER_NAME?>editsales_assessment.php" method="post"> 
                                             <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
                                            <input type="hidden" name="marketneedid" value="<?=$MarketNeedID_db?>">
                                                        <button class="med" type="submit"><nobr>Edit ASSESSMENT</nobr></button>           
                                             </form>
                                            
                                            <form action="<?=WEB_SERVER_NAME?>make_recommendation.php" method="post"> 
                                             <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
                                            <input type="hidden" name="MarketNeedID" value="<?=$MarketNeedID_db?>">
                                                        <button class="med" type="submit"><nobr>Make Recommendation</nobr></button>           
                                             </form>
                                        </td>
                                </tbody></table>
                        </div>





        <?php } ?>
<!--    Fetch vehicle specifiction
developed by: ketan
date: 24-07-2015
-->
<?php
if($resultvehiclespecification){
?>


    <h4 class="subhead"> <button onclick="javascript:showhidediv('vehicle_specification', 'span_vehicle_specification');" id="span_vehicle_specification" class="hideshow">Hide</button>&nbsp;&nbsp;Vehicle Spec </h4>
                        <div style="display: block;" id="vehicle_specification">            
                          <table class="table table-bordered">
    <thead>
      <tr>
        <th>Make</th>
        <th>Model</th>
        <th>Style</th>
        <th>Year</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php
 
    
    while ($row1 = mysql_fetch_assoc($resultvehiclespecification)) { ?>
      <tr>
          <td><strong><?php echo ($row1['Specific'])? $row1['TextMake'] : makefind($row1['Make']);?></strong></td>
            <td><?php echo ($row1['Specific'])? $row1['TextModel'] :$row1['Model'] ?></td>
            <td><?php echo ($row1['Specific'])? $row1['TextStyle'] : $row1['Style']; ?></td>
            <td><?php echo ($row1['Specific'])? $row1['TextYear'] : $row1['Year']; ?></td>
        <td><form style="float:left;" action="<?=WEB_SERVER_NAME?>edit_vehiclespecifiction.php" method="post"> 
        <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
        <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
         <input type="hidden" value="<?php echo $row1['SearchPlanDetailID'];?>" name="SearchPlanDetailID" />
             <button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>
         </form>
        
        </td>
      </tr> 
    <?php } ?>
    </tbody>
  </table>
                        </div>
<?php } ?>
    
    <?php
if($resultvehiclesearchplan){
?>


    <h4 class="subhead"> <button onclick="javascript:showhidediv('vehicle_search_plan', 'span_vehicle_search_plan');" id="span_vehicle_search_plan" class="hideshow">Hide</button>&nbsp;&nbsp;Vehicle Search Plan </h4>
                        <div style="display: block;" id="vehicle_search_plan">            
                          <table class="table table-bordered">
    <thead>
      <tr>
        <th>Make</th>
        <th>Model</th>
        <th>Style</th>
        <th>Year</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php
 
    
    while ($row1 = mysql_fetch_assoc($resultvehiclesearchplan)) { ?>
      <tr>
          <td><strong><?php echo ($row1['Specific'])? $row1['TextMake'] : makefind($row1['Make']);?></strong></td>
            <td><?php echo ($row1['Specific'])? $row1['TextModel'] :$row1['Model'] ?></td>
            <td><?php echo ($row1['Specific'])? $row1['TextStyle'] : $row1['Style']; ?></td>
            <td><?php echo ($row1['Specific'])? $row1['TextYear'] : $row1['Year']; ?></td>
        <td><form style="float:left;" action="<?=WEB_SERVER_NAME?>edit_vehiclespecifiction.php" method="post"> 
        <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
        <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
         <input type="hidden" value="<?php echo $row1['SearchPlanDetailID'];?>" name="SearchPlanDetailID" />
             <button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>
         </form>
        <form action="<?=WEB_SERVER_NAME?>add_preview.php" method="post"> 
        <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
        <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
         <input type="hidden" value="<?php echo $row1['SearchPlanDetailID'];?>" name="SearchPlanDetailID" />
             <button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/edit.gif" /></button>
         </form>
        </td>
      </tr> 
    <?php } ?>
    </tbody>
  </table>
                        </div>
<?php } ?>
    
    
    <?php
        if($resultpos){
    ?>


    <h4 class="subhead"> <button onclick="javascript:showhidediv('vehicle_pas', 'span_vehicle_pas');" id="span_vehicle_pas" class="hideshow">Hide</button>&nbsp;&nbsp;Price & Availability Study</h4>
                        <div style="display: block;" id="vehicle_pas">            
                          <table class="table table-bordered">
    <thead>
      <tr>
        <th>Make</th>
        <th>Model</th>
        <th>Style</th>
        <th>Year</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
   <?php
    while ($row1 = mysql_fetch_assoc($resultpos)) { ?>
      <tr>
       
          <td><strong><?php echo ($row1['Specific'])? $row1['TextMake'] : makefind($row1['Make']);?></strong></td>
                        <td><?php echo ($row1['Specific'])? $row1['TextModel'] :$row1['Model'] ?></td>
                        <td><?php echo ($row1['Specific'])? $row1['TextStyle'] : $row1['Style']; ?></td>
                        <td><?php echo ($row1['Specific'])? $row1['TextYear'] : $row1['Year']; ?></td>
         
         
         <td><form action="<?=WEB_SERVER_NAME?>edit_vehiclepas.php" method="post"> 
         <input type="hidden" name="ForUserID" value="<?=$inuserid?>">
         <input type="hidden" name="MarketNeedID" value="<?=$inmneed?>">
         <input type="hidden" value="<?php echo $row1['SearchPlanDetailID'];?>" name="SearchPlanDetailID" />
         <button type="submit" value="" class="blueongrey_dash"><img alt="Review" title="Review and Perform Actions on Employee Record" width="15" height="15" border="0" src="common/layout/magglass.gif" /></button>
         </form></td>
      </tr> 
    <?php } ?>
    </tbody>
  </table>
                        </div>
<?php } ?>

   <?php if ($ahaveconsult == 1) {
// echo '<br/><p style="color: rgb(133, 193, 27); font-size: 16px; margin-bottom: 5px;"><strong>';
// echo 'Requested Consultation';
// echo '</strong></p>';
        echo '<h4 class="subhead">Requested Consultation</h4>';
        echo '<p style="color:#142c3c; font-weight:bold; font-size:13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        echo $anote;
        echo '</p>';
    }
}
if ($instep >= 1) {
    $count = isset($vehid) ? count($vehid) : 0;
    if ($count > 0) {
// echo '<br/><p style="color: rgb(133, 193, 27); font-size: 16px; margin-bottom: 5px;"><strong>';
// echo 'Favorites';
// echo '</strong></p><br/>';
        echo '<h4 class="subhead">Favorites</h4>';
        echo '<br/>';
        for ($index = 0; $index < $count; $index++) {
            echo '<table style="margin-left: 10px;" align="left" border="0" cellpadding="0"><tbody><tr><td width="10">';
            if ($vvisible[$index] == 0)
                echo 'X';
            echo '</td><td width="178"><a href="details.php?vehid=' . $vehid[$index] . '" target="_blank">';
            if (!file_exists($vimage[$index]))
                $imagefile = '';
            else
                $imagefile = $vimage[$index];

            if ($imagefile) {
                $max_width = 170;
                $max_height = 170;
                echo '<img id="vehimage" src="loadimage.php?image=' . $imagefile . '&mwidth=' . $max_width . '&mheight=' . $max_height . '" border="0" hspace="10" vspace="10" />';
            } else
                echo '** Image currently not available **';
            echo '</a></td><td valign="top" width="278"><p class="blackfourteen" style="margin: 0pt;">';
            echo '<a href="details.php?vehid=' . $vehid[$index] . '" target="_blank"><strong>';
            echo $yeardets[$index] . ' ' . $makedets[$index] . ' ' . $modeldets[$index] . ' ' . $styledets[$index];
            echo '</strong></a></p>';
            echo '<p class="favorites"><span style="color: rgb(133, 193, 27);">Vehicle Type:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            echo $origin[$index] . '</span></p>';
            echo '<p class="favorites"><span style="color: rgb(133, 193, 27);">Low Mileage:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($lowmilestart[$index] < 1)
                echo 'Below ' . number_format($lowmileend[$index]);
            else
                echo number_format($lowmilestart[$index]) . ' - ' . number_format($lowmileend[$index]);
            echo '</span><br/><span style="color: rgb(133, 193, 27);">Price:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($lowpricestart[$index] < 5000)
                echo 'Below $' . number_format($lowpriceend[$index]);
            else
                echo '$' . number_format($lowpricestart[$index]) . ' to $' . number_format($lowpriceend[$index]);
            echo '</span></p>';
            if ($lowbb[$index] == 1)
                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="excellent" style="padding: 1px;">Excellent Availability!</span>';
            else
                echo '<br/>';
            echo '<p class="favorites"><span style="color: rgb(133, 193, 27);">Average Mileage:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($highmilestart[$index] < 1)
                echo 'Below ' . number_format($highmileend[$index]);
            else
                echo number_format($highmilestart[$index]) . ' - ' . number_format($highmileend[$index]);
            echo '</span><br/><span style="color: rgb(133, 193, 27);">Price:&nbsp;</span><span style="color: rgb(117, 117, 117);">';
            if ($highpricestart[$index] < 5000)
                echo 'Below $' . number_format($highpriceend[$index]);
            else
                echo '$' . number_format($highpricestart[$index]) . ' to $' . number_format($highpriceend[$index]);
            echo '</span></p>';
            if ($highbb[$index] == 1)
                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="excellent" style="padding: 1px;">Excellent Availability!</span>';
            else
                echo '<br/>';
            echo '</td><td width="60"></td></tr>';
            echo '<tr><td colspan="3" style="color: rgb(117, 117, 117); font-weight: bold; font-size: 13px;">';
            echo 'Notes: <span style="color: rgb(133, 193, 27); font-size: 13px; font-weight: bold;" >';
            if (isset($vnote[$index]) && (strlen($vnote[$index]) > 0))
                echo $vnote[$index];
            echo '</span></td></tr></tbody></table>';
            if ($index < ($count - 1))
                echo '<center><img src="common/layout/short-bar.gif" style="padding: 10px;" border="0" /></center>';
        }
        echo '</p><br clear="all" />';
    }
}
?>
            </div><!-- grid eight -->
        </div><!-- grid eight grey -->
    </div><!-- grid eight container -->
</div><!--end content-->
<div id="dialog" title=" " style="display: none;">
    <textarea name="notetext" id="notetext" placeholder="let me grow and close the popup" style="resize:vertical;width:100%;height:100%"></textarea>
</div>
<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>


<script type="text/javascript">
    function showhidediv(tdiv, ttrigger)
    {
        var thediv = document.getElementById(tdiv);
        var thetrigger = document.getElementById(ttrigger);

        if (thediv.style.display == "block")
        {
            thediv.style.display = "none";
            thetrigger.innerHTML = "Show";
        }
        else
        {
            thediv.style.display = "block";
            thetrigger.innerHTML = "Hide";
        }
        return true;
    }

    $(document).ready(function() {
        $("input:text").click(function() {
            var element = $(this);
            $("#notetext").val(element.val());
            $("#dialog").dialog({
                modal: true,
                title: " ",
                close: function(event, ui) {
                    element.val($("#notetext").val());
                    $("#dialog").dialog("destroy");
                }
            });
        });
    });

</script>

<style>
    .assessment_insidetd{
        background: none repeat scroll 0% 0% gray; 
        color: white;
    }

    .assessment_inside{
        margin-left: 10px;
    }


    td, th {
        padding: 3px !important;
    }
    select, textarea, input[type='text'], input[type='password'] {
        border: 1px solid #aaaaaa;
        background: #fff;
        resize: none;
        color: #252525;
        cursor: text;
        padding: 3px;
        font-family: Arial, Helvetica, Sans Serif;
        font-size: 13px;
        width: 60%;
    }

  
</style>
