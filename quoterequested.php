<?php require("globals.php"); ?>
<?php
    require_once(WEB_ROOT_PATH.'common/functions/globalfunctions.php');

    $_SESSION['state'] = 1;
    $_SESSION['substate'] = 15;

    // See what was passed in...
    $iquotetype = $_POST['QuoteType'];
    $iquoteid = $_POST['QuoteID'];
    $iyear = $_POST['Year'];
    $imake = $_POST['Make'];
    $imodel = $_POST['Model'];
    $istyle = $_POST['Style'];
    $imileage = $_POST['selectmileage'];
    $iseattype = $_POST['selectseat'];
    $iengine = $_POST['selectengine'];
    $idrive = $_POST['selectdrive'];
    $itrans = $_POST['selecttrans'];
    $iwheels = $_POST['selectwheels'];
    $icab = isset($_POST['selectcab']) ? $_POST['selectcab'] : null;
    $ifront = isset($_POST['selectfrontseat']) ? $_POST['selectfrontseat'] : null;
    $ibedtype = isset($_POST['selectbedtype']) ? $_POST['selectbedtype'] : null;
    $isunroof = $_POST['selectsunroof'];
    $iradflex = isset($_POST['RadFlex']) ? $_POST['RadFlex'] : null;
    $iradfm = $_POST['RadFM'];
    $iradcas = isset($_POST['RadCas']) ? $_POST['RadCas'] : null;
    $iradcd = $_POST['RadCD'];
    $ipdoor = $_POST['PowerDoorLocks'];
    $ipwin = $_POST['PowerWindows'];
    $ipseat = $_POST['PowerSeats'];
    $iheat = $_POST['HeatedSeats'];
    $iair = $_POST['AirConditioning'];
    $iremote = $_POST['RemoteEntry'];
    $itraction = $_POST['TractionControl'];
    $isecure = $_POST['SecuritySystem'];
    $icruise = $_POST['CruiseControl'];
    $inavsys = $_POST['Navigation'];
    $irearwin = isset($_POST['RearSlidingWindow']) ? $_POST['RearSlidingWindow'] : null;
    $ibed = isset($_POST['BedLiner']) ? $_POST['BedLiner'] : null;
    $itow = isset($_POST['TowPackage']) ? $_POST['TowPackage'] : null;
    $iluggage = $_POST['LuggageRack'];
    $icolors = $_POST['colorcombo'];
    $inotes = $_POST['notes'];
    $idelorpick = $_POST['DeliveryPickup'];
    $idelcity = $_POST['DeliveryCity'];
    $idelstate = $_POST['DeliveryState'];

    $userid = $_SESSION['userid'];
    $marketneedid = $_SESSION['marketneedid'];
    $srep = getsalesrep($userid, $marketneedid);

    switch($iquotetype)
    {
        case 'Standard':
            $_SESSION['titleadd'] = 'Current Market Study';
            break;
        case 'Special':
            $_SESSION['titleadd'] = 'Special Current Market Study';
            break;
        case 'Pickup':
            $_SESSION['titleadd'] = 'Pick-up Current Market Study';
            break;
        default:
            $iquotetype = 'Standard';
            $_SESSION['titleadd'] = 'Current Market Study';
            break;
    }

    $errorinsave = 'false';

    if(!isset($iquotetype) || !isset($iyear) || !isset($imake) || !isset($imodel) || !isset($istyle) || !isset($imileage) || !isset($iseattype) || !isset($iengine)
        || !isset($idrive) || !isset($itrans) || !isset($iwheels) || !isset($ipdoor) || !isset($ipwin) || !isset($ipseat) || !isset($iheat) || !isset($iair)
        || !isset($iremote) || !isset($itraction) || !isset($isecure) || !isset($icruise) || !isset($inavsys) || !isset($idelorpick)) $errorinsave = 'Not all fields were supplied!';
    if(($iquotetype == 'Pickup') && (!isset($irearwin) || !isset($ibed) || !isset($itow) || !isset($icab) || !isset($ifront) || !isset($ibedtype))) $errorinsave = 'Not all fields were supplied!';
    elseif(($iquotetype != 'Pickup') && (!isset($iluggage) || !isset($isunroof))) $errorinsave = 'Not all fields were supplied!';

    if($errorinsave == 'false')
    {
        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            mysql_select_db(DB_SERVER_DATABASE, $con);

            if(isset($iquoteid) && ($iquoteid != -1))
            {
                $query = "update quoterequests set ";
                $query .= "LastUpdated = '".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                $query .= "QuoteType = '".$iquotetype."',";
                $query .= "Year = ".escapestr($iyear).",";
                $query .= "Make = '".escapestr($imake)."',";
                $query .= "Model = '".escapestr($imodel)."',";
                $query .= "Style = '".escapestr($istyle)."',";
                $query .= "MileageCeiling = ".$imileage.",";
                $query .= "EngineCylinders = ".$iengine.",";
                $query .= "Transmission = '".$itrans."',";
                if(isset($icab)) $query .= "CabType = '".$icab."',";
                else $query .= "CabType = 'Flexible',";
                if(isset($ifront)) $query .= "FrontSeatType = '".$ifront."',";
                else $query .= "FrontSeatType = 'Flexible',";
                if(isset($ibedtype)) $query .= "BedType = '".$ibedtype."',";
                else $query .= "BedType = 'Flexible',";
                if(isset($isunroof)) $query .= "SunRoof = '".$isunroof."',";
                else $query .= "SunRoof = 'Flexible',";
                $query .= "SeatMaterial = '".$iseattype."',";
                $query .= "WheelDriveType = '".$idrive."',";
                if($idrive == 'Flexible') $query .= "WheelDrive = 'Flexible',";
                elseif($idrive == 'Four') $query .= "WheelDrive = '4',";
                else $query .= "WheelDrive = '2',";
                $query .= "WheelCovers = '".$iwheels."',";
                if(isset($iradflex)) $query .= "RadioNeeded = 'Flexible',";
                elseif(isset($iradfm) || isset($iradcas) || isset($iradcd)) $query .= "RadioNeeded = 'Yes',";
                else $query .= "RadioNeeded = 'No',";
                if(isset($iradfm)) $query .= "RadioFM = 1,";
                else $query .= "RadioFM = 0,";
                if(isset($iradcas)) $query .= "RadioCassette = 1,";
                else $query .= "RadioCassette = 0,";
                if(isset($iradcd)) $query .= "RadioCD = 1,";
                else $query .= "RadioCD = 0,";
                $query .= "RadioSat = 0,";
                $query .= "PowerDoors = '".$ipdoor."',";
                $query .= "PowerWindows = '".$ipwin."',";
                $query .= "PowerSeats = '".$ipseat."',";
                $query .= "HeatedSeats = '".$iheat."',";
                $query .= "AirConditioning = '".$iair."',";
                $query .= "RemoteEntry = '".$iremote."',";
                $query .= "TractionControl = '".$itraction."',";
                $query .= "SecuritySystem = '".$isecure."',";
                $query .= "CruiseControl = '".$icruise."',";
                $query .= "Navigation = '".$inavsys."',";
                if(isset($irearwin)) $query .= "RearSlidingWindow = '".$irearwin."',";
                else $query .= "RearSlidingWindow = 'Flexible',";
                if(isset($ibed)) $query .= "BedLiner = '".$ibed."',";
                else $query .= "BedLiner = 'Flexible',";
                if(isset($itow)) $query .= "TowPackage = '".$itow."',";
                else $query .= "TowPackage = 'Flexible',";
                if(isset($iluggage)) $query .= "LuggageRack = '".$iluggage."',";
                else $query .= "LuggageRack = 'Flexible',";
                $query .= "Convertible = 'Flexible',";
                if(isset($icolors) && (strlen($icolors) > 0)) $query .= "ColorCombination = '".escapestr($icolors)."',";
                else $query .= "ColorCombination = NULL,";
                if(isset($inotes) && (strlen($inotes) > 0)) $query .= "SpecialRequests = '".escapestr($inotes)."',";
                else $query .= "SpecialRequests = NULL,";
                if($idelorpick == 'Deliver')
                {
                    $query .= "Pickup = 0,";
                    if(isset($idelcity)) $query .= "DeliverToCity = '".$idelcity."',";
                    else $query .= "DeliverToCity = NULL,";
                    if(isset($idelstate)) $query .= "DeliverToState = '".$idelstate."'";
                    else $query .= "DeliverToState = NULL";
                }
                else $query .= "Pickup = 1,DeliverToCity = NULL,DeliverToState = NULL";
                $query .= ",Visible=1 where QuoteRequestID = ".$iquoteid;
                if(!mysql_query($query, $con)) $errorinsave = 'Could not update Request';

                if($errorinsave == 'false')
                {
                    // Add a Message Update when this happens...
                    posttodashboard($con, $userid, $userid, 'updated the <a href="'.WEB_SERVER_NAME.'quote.php?EditQuoteID='.$iquoteid.'">Current Market Study Request</a> for a '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.',$marketneedid);

                    if($srep != -1)
                    {
                        // Add a Message when this happens...
                        posttodashboard($con, $userid, $srep, '<a href="'.WEB_SERVER_NAME.'salesrepactions.php?ForUserID='.$userid.'&MarketNeedID='.$marketneedid.'">'.$firstname.' '.$lastname.'</a> updated their Quote Request for a '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.');

                        $message = 'Your customer has Updated a Current Market Study Request...</br>';
                        $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $userid, 'false').'</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $userid).'</br>';
                        $message .= '&nbsp;&nbsp;Vehicle: &nbsp;'.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'</br>';
                        sendemail(getuseremailnodb($con, $srep), 'Updated Current Market Study Request Received', $message, 'true');

                        $message = 'Your Current Market Study request update has been received for the '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, your sales representative will update you on the progress soon.';
                    }
                    else
                    {
                        $message = 'Your Current Market Study request update has been received for the '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, a sales representative will be assigned to help you very soon.';
                    }

                    sendemail(getuseremailnodb($con, $userid), 'Updated Current Market Study Request Received', $message, 'false');

                    $message = 'A customer has Updated a Current Market Study Request...</br>';
                    $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $userid, 'false').'</br>';
                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $userid).'</br>';
                    $message .= '&nbsp;&nbsp;Vehicle: &nbsp;'.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'</br>';
                    sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                }
            }
            else
            {
                $query = "insert into quoterequests (MarketNeedID, Created, LastUpdated, QuoteType, Year, Make, Model, Style, MileageCeiling, EngineCylinders, Transmission,
                    CabType, FrontSeatType, BedType, SunRoof, SeatMaterial, WheelDriveType, WheelDrive, WheelCovers, RadioNeeded, RadioFM, RadioCassette, RadioCD, RadioSat,
                    PowerDoors, PowerWindows, PowerSeats, HeatedSeats, AirConditioning, RemoteEntry, TractionControl, SecuritySystem, CruiseControl, Navigation, RearSlidingWindow,
                    BedLiner, TowPackage, LuggageRack, Convertible, ColorCombination, SpecialRequests, Pickup, DeliverToCity, DeliverToState) values (".$marketneedid.",";
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                $query .= "'".date_at_timezone('Y-m-d H:i:s', 'EST')."',";
                $query .= "'".$iquotetype."',";
                $query .= escapestr($iyear).",";
                $query .= "'".escapestr($imake)."',";
                $query .= "'".escapestr($imodel)."',";
                $query .= "'".escapestr($istyle)."',";
                $query .= $imileage.",";
                $query .= $iengine.",";
                $query .= "'".$itrans."',";
                if(isset($icab)) $query .= "'".$icab."',";
                else $query .= "'Flexible',";
                if(isset($ifront)) $query .= "'".$ifront."',";
                else $query .= "'Flexible',";
                if(isset($ibedtype)) $query .= "'".$ibedtype."',";
                else $query .= "'Flexible',";
                if(isset($isunroof)) $query .= "'".$isunroof."',";
                else $query .= "'Flexible',";
                $query .= "'".$iseattype."',";
                $query .= "'".$idrive."',";
                if($idrive == 'Flexible') $query .= "'Flexible',";
                elseif($idrive == 'Four') $query .= "'4',";
                else $query .= "'2',";
                $query .= "'".$iwheels."',";
                if(isset($iradflex)) $query .= "'Flexible',";
                elseif(isset($iradfm) || isset($iradcas) || isset($iradcd)) $query .= "'Yes',";
                else $query .= "'No',";
                if(isset($iradfm)) $query .= "1,";
                else $query .= "0,";
                if(isset($iradcas)) $query .= "1,";
                else $query .= "0,";
                if(isset($iradcd)) $query .= "1,";
                else $query .= "0,";
                $query .= "0,";
                $query .= "'".$ipdoor."',";
                $query .= "'".$ipwin."',";
                $query .= "'".$ipseat."',";
                $query .= "'".$iheat."',";
                $query .= "'".$iair."',";
                $query .= "'".$iremote."',";
                $query .= "'".$itraction."',";
                $query .= "'".$isecure."',";
                $query .= "'".$icruise."',";
                $query .= "'".$inavsys."',";
                if(isset($irearwin)) $query .= "'".$irearwin."',";
                else $query .= "'Flexible',";
                if(isset($ibed)) $query .= "'".$ibed."',";
                else $query .= "'Flexible',";
                if(isset($itow)) $query .= "'".$itow."',";
                else $query .= "'Flexible',";
                if(isset($iluggage)) $query .= "'".$iluggage."',";
                else $query .= "'Flexible',";
                $query .= "'Flexible',";
                if(isset($icolors) && (strlen($icolors) > 0)) $query .= "'".escapestr($icolors)."',";
                else $query .= "NULL,";
                if(isset($inotes) && (strlen($inotes) > 0)) $query .= "'".escapestr($inotes)."',";
                else $query .= "NULL,";
                if($idelorpick == 'Deliver')
                {
                    $query .= "0,";
                    if(isset($idelcity)) $query .= "'".$idelcity."',";
                    else $query .= "NULL,";
                    if(isset($idelstate)) $query .= "'".$idelstate."'";
                    else $query .= "NULL";
                }
                else $query .= "1,NULL,NULL";
                $query .= ")";
                if(!mysql_query($query, $con)) $errorinsave = 'Could not add Request';
                $newquoteid = mysql_insert_id($con);

                if($errorinsave == 'false')
                {
                    // Add a Message Update when this happens...
                    posttodashboard($con, $userid, $userid, 'made a <a href="'.WEB_SERVER_NAME.'quote.php?EditQuoteID='.$newquoteid.'">Current Market Study Request</a> for a '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.',$marketneedid);

                    if($srep != -1)
                    {
                        // Add a Message when this happens...
                        posttodashboard($con, $userid, $srep, '<a href="'.WEB_SERVER_NAME.'salesrepactions.php?ForUserID='.$userid.'&MarketNeedID='.$marketneedid.'">'.$firstname.' '.$lastname.'</a> made a Current Market Study Request for a '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.');

                        $message = 'Your customer has Requested a Current Market Study...</br>';
                        $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $userid, 'false').'</br>';
                        $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $userid).'</br>';
                        $message .= '&nbsp;&nbsp;Vehicle: &nbsp;'.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'</br>';
                        sendemail(getuseremailnodb($con, $srep), 'Current Market Study Request Received!', $message, 'true');

                        $message = 'Your Current Market Study request has been received for the '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, your sales representative will update you on the progress soon.';
                    }
                    else
                    {
                        $message = 'Your Current Market Study request has been received for the '.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'.</br>Thank you for using <a href="http://www.1800vehicles.com">1800vehicles.com</a>, a sales representative will be assigned to help you very soon.';
                    }

                    sendemail(getuseremailnodb($con, $userid), 'Current Market Study Request Received', $message, 'false');

                    $message = 'A customer has Requested a Current Market Study...</br>';
                    $message .= '&nbsp;&nbsp;Customer: '.getuserfullnamenodb($con, $userid, 'false').'</br>';
                    $message .= '&nbsp;&nbsp;Phone: &nbsp;&nbsp;&nbsp;'.getuserphonenodb($con, $userid).'</br>';
                    $message .= '&nbsp;&nbsp;Vehicle: &nbsp;'.escapestr($iyear).' '.escapestr($imake).' '.escapestr($imodel).' '.escapestr($istyle).'</br>';
                    sendtoadmins($con, $message, '1800vehicles.com Admin Alert', 'true');
                }
            }

            mysql_close($con);
        }
        else $errorinsave = 'Could not connect to the database';
    }
?>
<?php require("headerstart.php"); ?>
<?php require("header.php"); ?>
<?php require("foursteps.php"); ?>
<?php require("headerend.php"); ?>

<div id="content">
<?php
    echo '<div class="grideightcontainer">';
    if($errorinsave != 'false')
    {
        echo '<h1 class="subhead">Request Error</h1>';
        echo '<div class="grideightgrey">';
        echo '<p class="blackeleven" style="margin: 0;"><a href="allfirmquotes.php">Go to existing Current Market Studys</a></p>';
        echo '<br />';
        echo '<p class="blacktwelve">There was an error processing your request for a Current Market Study.</p>';
        echo '<p class="blacktwelve">Error: '.$errorinsave.'</p>';
        //echo '<p class="blacktwelve">Query: '.$query.'</p>';
        echo '<p class="blacktwelve">Please use your browser <a href="javascript:history.back()">back</a> button to correct any errors or start over by clicking <a href="pickupquote.php">here</a>.</p>';
        echo '</div><!--end grideightgrey-->';
    }
    else
    {
        echo '<h1 class="subhead" style="width: ';
        switch($iquotetype)
        {
            case 'Standard':
                echo '275';
                break;
            case 'Special':
                echo '325';
                break;
            case 'Pickup':
                echo '400';
                break;
        }
        echo 'px;" >';
        switch($iquotetype)
        {
            case 'Standard':
                echo 'Current Market Study Requested';
                break;
            case 'Special':
                echo 'Special Current Market Study Requested';
                break;
            case 'Pickup':
                echo 'Pick-up Truck Current Market Study Requested';
                break;
        }
        echo '</h1>';
        echo '<div class="grideightgrey">';
        echo '<p class="blackeleven" style="margin: 0;"><a href="allfirmquotes.php">Go to existing Current Market Studies</a></p>';
        if(isset($iquoteid) && ($iquoteid != -1)) echo '<p class="blacktwelve">Thank you for updating your request for a Current Market Study!  We appreciate this opportunity to earn your business. ';
        else echo '<p class="blacktwelve">Thank you for requesting a Current Market Study!  We appreciate this opportunity to earn your business. ';
        echo 'An email will be sent to you when your current market study has been completed.  Feel free to call us at 1-800-VEHICLES (834-4253) if you need a faster response.</p>';

        $con = mysql_connect(DB_SERVER_HOSTNAME,DB_SERVER_USERNAME,DB_SERVER_PASSWORD);
        if($con)
        {
            // First fill the list of Makes...
            mysql_select_db(DB_SERVER_DATABASE, $con);

            $result = mysql_query("select v.ImageFile from vehicles v, makes m where m.makeid=v.makeid and m.name='".$imake."' and v.year=".$iyear." and v.model='".$imodel."' and v.style='".$istyle."'");
            if($result && $row = mysql_fetch_array($result))
            {
                if(strlen($row[0]) > 0)
                {
                    $max_width = 550;
                    $max_height = 425;
                    echo '<center><img id="vehimage" src="loadimage.php?image='.$row[0].'&mwidth='.$max_width.'&mheight='.$max_height.'" border="0" hspace="10" vspace="10" /></center>';
                    echo '<center><span style="font-size:11px;" >The image is a representation of the selections you chose, not the actual vehicle.</span></center>';
                }
            }
            mysql_close($con);
        }

        //echo '<p class="blacktwelve">Query: '.$query.'</p>';
        echo '</div><!--end grideightgrey-->';
    }
    echo '</div><!-- grid eight container -->';

/*
<h2 class="subhead">Vehicle Preferences</h2>
<div class="grideightgrey">
<table border="0" width="300" align="left">
<tr>
<td><strong>Mileage Ceiling</strong></td>
<td><strong>Seat Material</strong></td>
</tr>
<tr valign="top">
<td height="23"><p class="greyeleven" style="margin-top: 0;">60,000</p></td>
<td><p class="greyeleven" style="margin-top: 0;">Leather</p></td>
</tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td><strong>Engine</strong></td>
<td><strong>Wheel Drive</strong></td>
</tr>
<tr valign="top">
<td height="23"><p class="greyeleven" style="margin-top: 0;">Flexible</p></td>
<td><p class="greyeleven" style="margin-top: 0;">Rear</p></td>
</tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td><strong>Transmission</strong></td>
<td><strong>Wheel Type</strong></td>
</tr>
<tr valign="top">
<td height="23"><p class="greyeleven" style="margin-top: 0;">Automatic</p></td>
<td><p class="greyeleven" style="margin-top: 0;">Flexible</p></td>
</tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td><strong>Cab Type</strong></td>
<td><strong>Stereo</strong></td>
</tr>
<tr valign="top">
<td><p class="greyeleven" style="margin-top: 0;">Regular</p></td>
<td><p class="greyeleven" style="margin-top: 0;">AM/FM, CD, Satellite</p></td>
</tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td><strong>Front Seat Type</strong></td>
<td><strong> </strong></td>
</tr>
<tr valign="top">
<td><p class="greyeleven" style="margin-top: 0;">Bench with armrest</p></td>
<td><p class="greyeleven" style="margin-top: 0;">&nbsp; </p></td>
</tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td><strong>Bed Type</strong></td>
<td><strong> </strong></td>
</tr>
<tr valign="top">
<td><p class="greyeleven" style="margin-top: 0;">Short Bed (about 6 ft)</p></td>
<td><p class="greyeleven" style="margin-top: 0;">&nbsp; </p></td>
</tr>
</table>
<table border="0" width="280" align="right" valign="top" cellspacing="5">
<tr>
<td width="147">Power Door Locks</td>
<td width="114"><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Power Windows</td>
<td><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Power Seats</td>
<td><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Heated Seats</td>
<td><span style="color: #06C;">Flexible</span></td>
</tr>
<tr>
<td width="147">Air Conditioning</td>
<td width="114"><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Remote Entry</td>
<td><span style="color: #06C;">Flexible</span></td>
</tr>
<tr>
<td>Traction Conrol</td>
<td><span style="color: #06C;">Flexible</span></td>
</tr>
<tr>
<td>Security System</td>
<td><span style="color: #06C;">Flexible</span></td>
</tr>
<tr>
<td width="147">Cruise Control</td>
<td width="114"><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Navigation</td>
<td><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Rear Sliding Window</td>
<td><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Bed Liner</td>
<td><span style="color: #06C;">Yes</span></td>
</tr>
<tr>
<td>Tow Package</td>
<td><span style="color: #06C;">Flexible</span></td>
</tr>
</table>
<br clear="all" />
<br />
<h4 class="subhead" style="font-size:18px;">Additional Information</h4>
<br />
<table border="0" width="300" align="left">
<tr>
<td><strong>Preferred color combinations:</strong></td>
</tr>
<tr>
<td>Anything but white.  Like dark colors for exterior.  Preferably gray interior.  No beige.</td>
</tr>
</table>
<table border="0" width="300" align="right">
<tr>
<td><strong>Special notes:</strong></td>
</tr>
<tr>
<td>Must be non smoker.  Would like to have DVD player and rear spoiler.</td>
</tr>
</table>
<br clear="all" />
<br />
<h4 class="subhead" style="font-size:18px;">Delivery</h4>
<p>I will pick the vehicle up at my 1-800-vehicles dealer location.</p>
*/
?>
<?php require("teaser.php"); ?>
</div>
</div><!--end content-->

<?php require("footerstart.php"); ?>
<?php require("footer.php"); ?>
<?php require("footerend.php"); ?>
